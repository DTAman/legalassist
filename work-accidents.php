<?php
include("includes/head.php");
?>

<body class="" contenteditable="false">
   <!-- Page-->
   <div class="page">
      <!-- Panel Thumbnail-->
      <!-- Template panel-->
      <div class="layout-panel-wrap">
         <div class="layout-panel">
         </div>
      </div>
      <?php
		include("includes/header.php");
	  ?>

      <section class="detail">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6">
                  <div class="contentee">
                     <h4 class="heading-decorated">Accidents at work</h4>
                     <p>Work injuries often happen. Many are avoidable. If yours feels wrong and you think it shouldn't
                        have happened, we can help you make things right.</p>
                     <a class="button button-primary mt-4" href="contact-us.php">Contact us</a>
                  </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="sImage">
                     <img src="images/s3.png" alt="" class="img-fluid">
                  </div>
               </div>
            </div>
         </div>
         <div class="bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <p><b>It’s not just about your physical suffering and pain: there are numerous other compensation
                           areas that your solicitor will include:</b></p>
                     <ol>
                        <li>
                           <p>Any future loss of income</p>
                        </li>
                        <li>
                           <p>Any time off work and lost earnings.</p>
                        </li>
                        <li>
                           <p>Psychological trauma.</p>
                        </li>
                        <li>
                           <p>Any changes to your ability to work.</p>
                        </li>
                        <li>
                           <p>Paid medical treatments, accommodation costs and travel to treatment.</p>
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-12 mt-4">
                  <div class="content">
                     <h3>Can I make a work injury claim?</h3>
                     <p>If you've been injured at work in the last three years and it wasn't your fault, you may be able
                        to claim compensation. Injuries and work-related illnesses are often caused by employers and
                        managers failing to follow health and safety rules.</p>
                     <p>Common reasons include:</p>
                     <ol>
                        <li>Dangerous working practices.</li>
                        <li>Poor or non-existent personal protective equipment (PPE).</li>
                        <li>Weak risk assessments.</li>
                        <li>Preventable spillages.</li>
                        <li>Unsatisfactory or poorly enforced safety procedures.</li>
                        <li>Badly-maintained equipment. </li>
                     </ol>
                     <p>Most of us depend on our income. That's why being injured at work and having to take time off
                        for recovery can be even more stressful for the individual and their family. You may have been
                        off work or perhaps even had to quit your job entirely as a result of your accident. Lost
                        earnings may have left you unable to pay household bills, care for your family or meet other
                        commitments.</p>
                     <p>You don't deserve to feel anxious about your health, job or money after an accident at work.
                        That's why we're here to help you make it right with a work injury claim. We're here to give you
                        the right advice and the right help - right when you need it the most.</p>
                     <h3>Did you know?</h3>
                     <p>There were nearly 310,000 accidents at work in 2015/16,
                        according to the Health and Safety Survey.
                        That's an average of nearly 1,500 every working day of the year.
                     </p>
                     <h3>How much compensation could I claim?</h3>
                     <p>The amount of work injury compensation you could win depends on your injury or illness, its
                        severity and the effect it's had on your life. When you make a claim with us, your solicitor
                        will consider the full impact of what you have suffered. It's important to us that you're
                        properly compensated.</p>
                     <p>It's not just about your pain and physical suffering. There are several other compensation areas
                        your solicitor will include:</p>
                     <ol>
                        <li>Any time off work and lost earnings.</li>
                        <li>Any future loss of income.</li>
                        <li>Any changes to your ability to work.</li>
                        <li>Psychological trauma.</li>
                        <li>Care or support you have needed, even if given free by family and friends.</li>
                        <li>Any changes you might need to your home or car.</li>
                        <li>Paid medical treatments, travel costs for treatment and accommodation costs.</li>
                     </ol>
                     <p>You won't have an estimate of the potential amount of work injury compensation you may receive
                        until your solicitor has started negotiating with your employer's insurers. However, we have 25
                        years' worth of experience handling accident at work claims.</p>
                     <h3>What are the typical causes of injury or illness at work?</h3>
                     <p>There's a school of thought that believes that every accident in the workplace is preventable.
                        In a perfect world perhaps, but in reality, some are unavoidable. For instance, construction,
                        energy and steel production are fundamentally high-risk working environments.</p>
                     <p>But most accidents at work could still be prevented. Usually by better, more thorough training
                        and HSE-compliant safety measures and maintenance.
                        For example, specific injuries could happen as a result of:
                     </p>
                     <ol>
                        <li>Poorly-maintained equipment.</li>
                        <li>Support structures assembled incorrectly, such as</li>
                        <li> scaffolding or warehouse racking.
                           Forklift trucks being badly maintained or handled dangerously.
                        </li>
                        <li>Uncleared liquid spillages that cause</li>
                        <li>Trips, slips or falls.</li>
                        <li>Poor ventilation or air conditioning (HVAC) and the insecure storage of chemicals.</li>
                        <li>Exposure to toxic chemicals or biological materials.</li>
                     </ol>
                     <br>
                     <h3>What are the time limits for claiming?</h3>
                     <p>Work accidents can cause serious injuries. This is because most working environments have a high
                        accident potential when not maintained - or if staff haven't been trained properly.</p>
                     <p>You typically have three years from the date of the accident or diagnosis of the medical
                        condition to make an injury at work claim.
                        There are certain things a solicitor has to do to get a compensation claim moving. If you're
                        approaching three years since the injury or illness diagnosis, then contact us as soon as you
                        can.
                     </p>
                     <p>There are a few exceptions to the three-year limit:</p>
                     <ol>
                        <li>Psychological trauma: if the claimant suffered a serious brain injury and can't make the
                           claim personally. In these circumstances there is no time limit for making a compensation
                           claim.</li>
                        <li>Manufacturing or design fault: if the equipment you were using had a fundamental defect, the
                           time limit may change. Talk to us; we can tell you if we can help.</li>
                        <li>Overseas work accidents: the time limit may be shorter depending on the circumstances.</li>
                     </ol>
                     <br>
                     <h5>Are you thinking of contacting us on behalf of a loved one?</h5>
                     <h3>How long does it take to make an injury at work claim?</h3>
                     <p>Finding out if you have a potential claim is simple. Our trained advisors will typically be able
                        to tell whether or not you have a claim in the space of one telephone call. How long a claim
                        takes to process depends on the nature and severity of your injuries.</p>
                     <ol>
                        <li>A clear-cut claim where injuries are minor, and your employer admits liability, can be
                           settled in six to nine months.</li>
                        <li>Complex cases possibly involving serious injury and a Health and Safety Executive
                           investigation naturally take longer. One to two years - or possibly more - are the norm.
                           However, no two cases are the same. The solicitor we appoint for you will be able to give you
                           a more accurate estimate when they've reviewed all the details.
                        </li>
                     </ol>
                     <br>
                     <h3>Do I have grounds for a work-based compensation claim?</h3>
                     <p>Most workplaces have a high potential for causing serious and long-lasting injuries when they're
                        not maintained safely. This is particularly true if people are poorly trained or lack the right
                        personal protective equipment (PPE) for the job.</p>
                     <p>For instance, an estimated 300,000 people in Australia suffer from back pain due to work-related
                        manual handling accidents, according to Unison.</p>
                     <p>You may not think you've suffered a serious injury. However, you'd be surprised how many people
                        call us who have been hurt badly without realising.</p>
                     <p>If you're unsure about your injury, ask yourself the questions below. If you answer yes to any,
                        then get in touch. You may have suffered a workplace injury worth investigating:</p>
                     <ol>
                        <li>Am I still receiving medical treatment for an old injury or illness picked up at work?</li>
                        <li>Have I been making repeat hospital visits to treat the same condition?</li>
                        <li>Have I taken time off work to recover from an injury or other condition?</li>
                        <li>Have my injuries or condition stopped me returning to work in the same role or doing the
                           same hours?</li>
                     </ol>
                     <br>
                     <p>Compensation can help pay for your recovery and any changes you have to make to your life -
                        either historically, now or in the future. In some cases, your solicitor may be able to arrange
                        an interim payment. This is a portion of your compensation that takes care of your immediate
                        needs before the final settlement comes through.</p>
                     <p>If you are in any doubt, then it is always worth speaking to one of our trained advisors. We're
                        open seven days a week.</p>
                     <h3>What types of accident at work injury can I claim for?</h3>
                     <p>We have 25 years' experience helping people make things right after an accident at work. During
                        that time, we've learnt that there's no such thing as a standard workplace accident.</p>
                     <p>Below we've linked to some useful information on certain types of work accident, which may help
                        you:</p>
                     <ol>
                        <li>Construction site accidents.</li>
                        <li>Accidents in a factory or warehouse.</li>
                        <li>Injuries while serving in the military.</li>
                        <li>Unsafe industrial sites.</li>
                        <li>Accidents while working in an office.</li>
                        <li>Agricultural and farming accidents</li>
                     </ol>
                     <br>
                     <p>All employers have a ‘duty of care' towards their employees - they're required by law to protect
                        you.</p>
                     <p>Your employer must take steps to make sure your working environment is safe for you and your
                        colleagues to work in.</p>
                     <p>If an employer doesn't follow the general health and safety regulations, such as The Health and
                        Safety at Work Act 1974, then they've put you at risk and they can be held responsible for your
                        accident.</p>
                     <h3>Can I claim for occupational illness or disease?</h3>
                     <p>Yes, you can. Thousands of people every year are affected by work-related illnesses - or
                        contract diseases during employment. Sometimes the working environment can aggravate the
                        symptoms of a pre-existing condition.</p>
                     <p>The law requires employers to provide satisfactory protection and safe work processes for their
                        people. Good health and safety practices must be in place to minimise the risk of occupational
                        illness.</p>
                     <p>These are some of the common medical conditions contracted in Australian workplaces.</p>
                     <ol>
                        <li>Noise-induced hearing loss: partial or full deafness caused by sudden or persistent exposure
                           to loud or highly penetrative sounds.</li>
                        <li>Vibration injuries: limbs and joints damaged by heavy use of vibrating power tools or
                           equipment.</li>
                        <li>Respiratory and lung diseases: exposure to harmful substances like fumes, dust, silica or
                           asbestos.</li>
                        <li>Industrial dermatitis: inflammation, irritation and hypersensitivity of the skin from
                           over-exposure to dust, chemicals and adhesives - even water.</li>
                        <li>Occupational cancer: usually lung cancer - mesothelioma - caused by exposure to asbestos.
                        </li>
                        <li>Musculoskeletal disorders: muscle, tendon and nerve conditions caused by things like
                           repetition, constrained body position or cumulative trauma.</li>
                     </ol>
                     <br>
                     <p>If you believe you've fallen ill because of a duty of care failure, you may be able to make an
                        industrial disease claim. The three-year time limit for making a claim begins when you first
                        notice the symptoms or the condition is diagnosed.</p>
                     <h3>What happens after a workplace accident?</h3>
                     <p>There are several things you should do after an accident in the workplace. First and most
                        importantly is to make sure you seek immediate medical attention.</p>
                     <ol>
                        <li>Ensure you - or a colleague - reports the accident to your manager.</li>
                        <li>Make sure that your employer records the accident in the company accident book.</li>
                        <li>Tell your trade union rep if you're a member - they're there to help you.</li>
                        <li>Get witness statements from colleagues if you can. Ask a good workmate or your union rep to
                           do so if you're incapacitated or off work.</li>
                        <li>Your employer must report certain accidents and injuries to the Health and Safety Executive
                           (HSE) under RIDDOR. These are the Reporting of Injuries, Diseases and Dangerous Occurrences
                           Regulations.</li>
                     </ol>
                     <br>
                     <p>If you've been seriously injured and are hospitalised, a colleague or manager will usually
                        arrange these things for you.</p>
                     <ol>
                        <li>Should you need time off work to recover, you should be entitled to statutory sick pay.</li>
                        <li>Serious injury with long-term or permanent effects means you may be eligible for industrial
                           injuries disablement benefit.</li>
                        <li>Make sure you talk to your employer about your employment rights after injury.</li>
                     </ol>
                     <br>
                     <p>The Health and Safety Executive investigate all reported cases of serious injury and
                        occupational disease.
                        What happens during a workplace accident claim?
                     </p>
                     <p>If you think you may have grounds for a work injury compensation claim, first seek expert
                        advice. Talk to us for free and in confidence. Tell us your story, your way and at your pace.
                        We'll let you know if we think you may be eligible to make a claim.</p>
                     <p>If we think you are and you decide you'd like to go ahead, we'll pair you with one of our expert
                        workplace accident solicitors. We can usually do this on the same call to get things moving for
                        you.</p>
                     <p>We also understand that you may have natural reservations about making a claim against your
                        employer. That's why we'll never pressure you or leave you feeling rushed into a decision.</p>
                     <p>Should you proceed with the claim, there are several things you can do to support your claim.
                     </p>
                     <ol>
                        <li>Gather photographic evidence: it may not always be possible but clear pictures of the
                           accident area are really helpful.</li>
                        <li>Gather any witness statements: these will also help your solicitor build your claim.</li>
                        <li>Record your injury details and symptoms: a regular injury diary will support the medical
                           evidence and show the pace of your recovery.</li>
                        <li>Record any financial losses: this comprises all the financial expenditure you've made
                           personally as a direct result of your injury.</li>
                        <li>Ask a colleague to help: a trusted co-worker - or your trades union rep - can help you sort
                           things out.</li>
                     </ol>
                     <br>
                     <p>Once your solicitor has all the evidence to hand, they'll submit the claims notification form to
                        your employer's insurance company. Your solicitor will also arrange a medical assessment where
                        an independent specialist will examine your injuries. The report they submit will form a key
                        part of your evidence.</p>
                     <p>If your employer accepts liability for your workplace injury, your solicitor will negotiate a
                        settlement figure with their insurers.</p>
                     <ol>
                        <li>If a figure cannot be agreed or your employer denies liability, your solicitor will begin
                           court proceedings.</li>
                        <li>The negotiations will continue and often, an agreement will still be reached.</li>
                        <li>Accident at work claims rarely get to the stage where a court hearing before a judge is
                           needed.</li>
                     </ol>
                     <br>
                     <p>Sometimes workplace accidents are serious enough to warrant automatic investigation by the HSE.
                        Investigations can take time, but you don't need to wait for the outcome before starting a
                        compensation claim.</p>
                     <h3>What do I need to prove in an accident at work claim?</h3>
                     <p>First, you need to prove that the accident that caused your injury (or medical condition) was
                        not your fault. Second, the evidence must demonstrate that the accident was caused by the
                        negligent action - or inaction - of your employer.</p>
                     <p>Your solicitor will base your accident at work claim on the following evidence:</p>
                     <ol>
                        <li>The accident at work was officially recorded in the company accident book.</li>
                        <li>A record of medical treatment received afterwards.</li>
                        <li>An independent medical assessment of your injuries.</li>
                        <li>Photographs of the location and injury hazard.</li>
                        <li>Witness statements.</li>
                        <li>A record of any/all financial losses.</li>
                     </ol>
                     <br>
                     <h3>What is contributory negligence?</h3>
                     <p>Even if you were partially responsible for being injured at work, you can still make a claim
                        under contributory negligence. This is also called split liability. It's where both sides in the
                        claim agree on a share of the blame. For example, this could be any ratio such as 50:50, 40:60
                        or 25:75.</p>
                     <p>Example: in a claim worth AUD10,000, the claimant is judged 25% responsible and the employer 75%
                        responsible (25:75). The gross compensation payment for the claimant, before deduction of the
                        relevant legal success fee and costs, would be AUD7,500.</p>
                     <h3>How do I pay for a work accident or illness claim?</h3>
                     <p>A no win no fee arrangement is the contract between you and the personal injury solicitor we
                        select for you. If your lawyer wins the claim, you pay them.</p>
                     <ol>
                        <li>Usually this is 25% of the compensation secured.</li>
                        <li>This figure can vary slightly either way depending on the claim circumstances.</li>
                        <li>It will only be deducted when your compensation is paid.</li>
                        <li>There are no upfront costs and no hidden charges.</li>
                     </ol>
                     <br>
                     <p>Your lawyer may need to arrange insurance for you to ensure claiming is risk-free. If your
                        lawyer loses the claim, you pay nothing to anyone. It's as simple as that.
                        There are variations on no win no fee claim funding. For instance, you may have an existing
                        legal protection insurance policy. These sometimes come with home or motor insurance - or as a
                        credit card benefit. If you do, your solicitor will check to see if it offers appropriate cover
                        for a no win no fee accident at work claim (not all do).
                        Similarly, if you're a member of a trade union, membership benefits may include legal protection
                        insurance. Again, your solicitor will check this out for you before starting work on your claim.
                     </p>
                     <p>You'll find detailed information about funding your claim on our no win no fee page.</p>
                     <p>Work-related injury or illness, the law … and you
                        All employers have a clear legal responsibility to make sure you and your colleagues are safe in
                        the workplace. This includes providing a well-maintained working environment, sound training and
                        satisfactory safety gear for the job.
                     </p>
                     <p>It's called a duty of care. If your employer has failed in theirs and you've been injured at
                        work - or become ill as a result - you're entitled to seek full work injury compensation.</p>
                     <p>By law, this cannot compromise the employer-employee relationship. The law recognises this
                        concern too.</p>
                     <ol>
                        <li>If your employer attempts to sack you for making - or thinking about making - a compensation
                           claim, you may have a case for unfair dismissal.</li>
                        <li>Equally, if they make your life at work so unbearable you end up quitting, you may have a
                           case for constructive dismissal.</li>
                     </ol>
                     <br>
                     <p>In reality, we understand that you may worry about making a claim against your employer. It's a
                        valid concern and we're here to help.
                        We're a trusted national helpline with 25 years' experience helping people win the compensation
                        they deserve. We'll give you the free, impartial and confidential advice you need to explore a
                        personal injury at work claim. In your own time and at your own pace. No rush. No pressure. No
                        problem.
                     </p>
                     <p>If you do decide to pursue your claim, we'll guide you through those initial first steps. We'll
                        then introduce you to one of our approved personal injury solicitors. Contact us today to find
                        out more.</p>
                     <h3>Can I claim for an accident at work if I'm a self-employed contractor?</h3>
                     <p>Yes, you can. Self-employed contractors hired by a third-party business have the same health and
                        safety protections as employees. Many sectors employ contractors. It's a common practice in
                        areas like construction, financial services, retail, healthcare and information technology.</p>
                     <h3>What are the most common workplace accidents and injuries?</h3>
                     <p>The Health and Safety Executive conducts regular and detailed research into injury and illness
                        in Australian workplaces. Let's have a look at the seven most common causes.</p>
                     <ol>
                        <li>Slips, trips or fall on the same level: the most common cause of injury with bruising,
                           strains, sprains, fractures and breaks.</li>
                        <li>Lifting and handling: soft tissue, spinal strains and bone breaks.</li>
                        <li>Being struck by a falling object: often a problem in warehouse-style environments where
                           stock or materials are stored at height.</li>
                        <li>Falling from a height: ladder, roof, scaffolding and exterior work.</li>
                        <li>Workplace violence: threats, assaults, cuts, scratches and bruising.</li>
                        <li>Contact with moving machinery: typically causing lacerations and cuts.</li>
                        <li>Collision: walking into a stationary object when not paying attention.</li>
                     </ol>
                     <br>
                     <p>According to HSE figures for 2013-2018, 26% of all fatal injuries were falls from a height.</p>
                     <p>Other risks to health at work include:</p>
                     <ol>
                        <li>Toxic fumes: inhalation that causes illness or respiratory damage.</li>
                        <li>Industrial deafness: exposure to loud, persistent noise with poor protection.</li>
                        <li>Repetitive strain injury (RSI): from keyboards to pneumatic equipment.</li>
                     </ol>
                     <br>
                     <h3>When does the Health and Safety Executive (HSE) investigate workplace accidents or occupational
                        illnesses?</h3>
                     <p>The HSE aims to prevent workplace fatalities, injuries and ill-health. It does this by providing
                        advice, licensing business activity in hazardous environments, inspections, incident
                        investigation and enforcing legislation.</p>
                     <p>Employers are bound by law to report all workplace injuries and illnesses to the HSE under the
                        RIDDOR regulations we mentioned earlier. Firms can face heavy fines for not doing so. The HSE
                        doesn't investigate all reports, just the ones it defines as serious.</p>
                     <ol>
                        <li>Fatalities: including accidents and all other deaths resulting from a preventable
                           work-related cause.</li>
                        <li>Serious injury: including multiple fractures, blinding, crush injuries, head trauma, burns
                           and amputation.</li>
                        <li>Occupational diseases: including carpal tunnel syndrome, hand arm vibration, tendonitis,
                           dermatitis, asthma, cancer and toxic agent exposure.</li>
                        <li>Any incident that indicates a potentially serious breach of health and safety law.</li>
                     </ol>
                     <br>
                     <p>Remember, if the HSE is investigating your accident, you do not have to wait for it to conclude
                        before making a workplace injury claim.
                        If you've been in an accident that feels wrong, get in touch with us for free, impartial advice
                        on injuries at work.
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Call to Action-->
      <section class="section section-sm context-dark bg-gray-dark section-cta">
         <div class="container">
            <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
               <div class="col-xl-8 text-xl-left">
                  <h4><span class="font-weight-bold">Extrafast</span><span class="font-weight-normal">offers flexible
                        solutions with lots of advantages</span></h4>
               </div>
               <div class="col-xl-2 text-xl-right"><a class="button button-primary" target="_blank" data-toggle="modal"
                     data-target="#modalLogin">Get in touch</a></div>
            </div>
         </div>
      </section>
        <?php
	  include("includes/footer_one.php");
	  ?>
   </div>
    <?php
	  include("includes/footer_two.php");
	  ?>
</body>
<!-- Google Tag Manager -->

</html>