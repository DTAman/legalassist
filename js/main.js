"use strict";
//Wrapping all JavaScript code into a IIFE function for prevent global variables creation
(function($){

    var $body = $('body');
    var $window = $(window);

//hidding menu elements that do not fit in menu width
//processing center logo

     
 
 

    function initGoogleMap() {
        //Google Map script
        var $googleMaps = $('#map, .page_map');
        if ( $googleMaps.length ) {
            $googleMaps.each(function() {
                var $map = $(this);

                var lat;
                var lng;
                var map;
                // light style
                var styles = [{"featureType": "water","elementType": "geometry","stylers": [{"color": "#e9e9e9"},{"lightness": 17}]},{"featureType": "landscape","elementType": "geometry","stylers": [{"color": "#f5f5f5"},{"lightness": 20}]},{"featureType": "road.highway","elementType": "geometry.fill","stylers": [{"color": "#ffffff"},{"lightness": 17}]},{"featureType": "road.highway","elementType": "geometry.stroke","stylers": [{"color": "#ffffff"},{"lightness": 29},{"weight": 0.2}]},{"featureType": "road.arterial","elementType": "geometry","stylers": [{"color": "#ffffff"},{"lightness": 18}]},{"featureType": "road.local","elementType": "geometry","stylers": [{"color": "#ffffff"},{"lightness": 16}]},{"featureType": "poi","elementType": "geometry","stylers": [{"color": "#f5f5f5"},{"lightness": 21}]},{"featureType": "poi.park","elementType": "geometry","stylers": [{"color": "#dedede"},{"lightness": 21}]},{"elementType": "labels.text.stroke","stylers": [{"visibility": "on"},{"color": "#ffffff"},{"lightness": 16}]},{"elementType": "labels.text.fill","stylers": [{"saturation": 36},{"color": "#333333"},{"lightness": 40}]},{"elementType": "labels.icon","stylers": [{"visibility": "off"}]},{"featureType": "transit","elementType": "geometry","stylers": [{"color": "#f2f2f2"},{"lightness": 19}]},{"featureType": "administrative","elementType": "geometry.fill","stylers": [{"color": "#fefefe"},{"lightness": 20}]},{"featureType": "administrative","elementType": "geometry.stroke","stylers": [{"color": "#fefefe"},{"lightness": 17},{"weight": 1.2}]}];

                //markers
                var $markers = $map.find('.marker');

                //map settings
                var address = $markers.first().find('.marker-address').text() ? $markers.first().find('.marker-address').text() : 'london, baker street, 221b';
                var geocoder = new google.maps.Geocoder();


                var draggable = $map.data('draggable') ? $map.data('draggable') : true;
                var scrollwheel = $map.data('scrollwheel') ? $map.data('scrollwheel') : false;

                //type your address after "address="
                geocoder.geocode({
                    address: address
                }, function(data){

                    lat = data[0].geometry.location.lat();
                    lng = data[0].geometry.location.lng();

                    var center = new google.maps.LatLng(lat, lng);
                    var settings = {
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        zoom: 4,
                        draggable: draggable,
                        scrollwheel: scrollwheel,
                        center: center,
                        styles: styles
                    };
                    map = new google.maps.Map($map[0], settings);

                    var infoWindows = [];

                    $($markers).each(function(index) {

                        var $marker = $(this);
                        var markerTitle = $marker.find('.marker-title').text();

                        //building info widnow HTML code
                        var markerDescription = '';
                        markerDescription += markerTitle ? '<h3 class="makret-title">' + markerTitle + '</h3>' : '';
                        markerDescription += $marker.find('.marker-description').html() ? '<div class="marker-description">' + $marker.find('.marker-description').html() + '</div>' : '';
                        if(markerDescription) {
                            markerDescription = '<div class="map_marker_description">' + markerDescription + '</div>';
                        }

                        geocoder.geocode({
                            address: $marker.find('.marker-address').text()
                        }, function(data){
                            var iconSrc = $marker.find('.marker-icon').attr('src');

                            var lat = data[0].geometry.location.lat();
                            var lng = data[0].geometry.location.lng();

                            var center = new google.maps.LatLng(lat, lng);

                            var marker = new google.maps.Marker({
                                position: center,
                                title: markerTitle,
                                map: map,
                                icon: iconSrc
                            });

                            var infowindow = new google.maps.InfoWindow({
                                content: markerDescription
                            });

                            infoWindows.push(infowindow);

                            google.maps.event.addListener(marker, 'click', function() {
                                for (var i=0;i<infoWindows.length;i++) {
                                    infoWindows[i].close();
                                }
                                infowindow.open(map,marker);
                            });
                        });
                    });
                });
            }); //each Google map
        }//google map length
    }
    window.initGoogleMap=initGoogleMap;



//end of IIFE function
})(jQuery);