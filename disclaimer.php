<?php
include("includes/head.php");
?>

<body class="" contenteditable="false">
   <!-- Page-->
   <div class="page">
      <!-- Panel Thumbnail-->
      <!-- Template panel-->
      <div class="layout-panel-wrap">
         <div class="layout-panel">
         </div>
      </div>
      <?php
		include("includes/header.php");
	  ?>

        <section class="detail pt-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h4 class="heading-decorated">Disclaimer</h4>
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-12 col-lg-8 order-1 order-lg-0 mt-4"> -->
                    <div class="col-12 col-lg-12 order-1 order-lg-0 mt-4">
                        <div class="content">

                            <p>Injury Assist Helpline offers services through our network of partners to Australian
                                residents that have unfortunately found themselves to be involved in a not at fault
                                accident. </p>

                            <p>To avoid any confusion, we want to make it clear that we operate separately to any
                                insurance company or broker. </p>

                            <p>We are not affiliated in any way or endorsed by any insurer or broker and nor do we
                                purport to be. For this reason, we ensure all calls are recorded and we have a
                                compliance team that listen in to calls daily to ensure all our agents are following our
                                strict company protocols. </p>

                            <p>We operate numerous marketing campaigns to try and spread awareness of our services as
                                most Australian motorists in a not at fault accident are unaware that they have a
                                choice. As can be found across our website we explain the reasons why our service rivals
                                any standard motor insurance company offering when it comes to making a claim. Therefore
                                we may share marketing space with insurance providers to ensure that you are aware of
                                your choice before its too late to make one. </p>

                            <p>If you have any queries in relation to a call you have made to us, then please feel free
                                to email our compliance team directly. Their email address is
                                info@injuryassisthelpline.com and we aim to respond to all queries within a maximum of
                                90 working days. </p>



                        </div>
                    </div>
                    <!--
					<div class="col-12 col-lg-4 order-0 order-lg-1">
                        <div class="imgg4"></div>
                    </div>
					-->
                </div>
            </div>
        </section>
         <?php
	  include("includes/footer_one.php");
	  ?>
   </div>
    <?php
	  include("includes/footer_two.php");
	  ?>
</body>
<!-- Google Tag Manager -->

</html>