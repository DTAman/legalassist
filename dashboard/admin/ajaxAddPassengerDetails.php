<?php
include('../includes/basic_auth.php');

if(isset($_POST['passenger_array']) && isset($_POST["hfLeadId"]) && trim($_POST['hfLeadId'])!=0 && isset($_POST['hfLeadAccidentId']) && trim($_POST['hfLeadAccidentId'])!=0)
{
	$lead_id = $_POST['hfLeadId'];
	$hfSomethingDoneInPassenger = $_POST['hfSomethingDoneInPassenger'];
	$hfLeadAccidentId = $_POST['hfLeadAccidentId'];
	
	if($lead_id>0 && $hfSomethingDoneInPassenger!=0)
	{
		$passenger_array = array();
		$passenger_array = json_decode($_POST['passenger_array']);
		
		$loginmaster = $mysqli->prepare('delete from lead_passenger_details where lead_accident_id='.$hfLeadAccidentId);
		$loginmaster->execute();
		$loginmaster->close();
		
		foreach($passenger_array as $my_array)
		{
			if(isset($my_array->txtPassenger) && trim($my_array->txtPassenger)!=null)
			{
				$loginmaster = $mysqli->prepare("insert into lead_passenger_details(lead_id, passenger_details, lead_accident_id, is_driver) values(?,?,?,?)");
				$loginmaster->bind_param("isii",$lead_id,$my_array->txtPassenger,$hfLeadAccidentId,$my_array->txtPassengerDriver);	
				$loginmaster->execute();
				$loginmaster->close();
			}
		}
	}
}
?>