<div id="logsModal" class="modal fade" role="dialog">
	<div class="modal-dialog" style="max-width:1000px!important">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">View Logs</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 col-sm-12 showLeadLogs"></div>
				</div>
			</div>
		</div>
	</div>
</div>