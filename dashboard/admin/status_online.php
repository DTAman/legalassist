<?php
include('../includes/basic_auth.php');

if(isset($_POST["sendval"]))
{
	if(isset($_POST["sub_user"]) && trim($_POST["sub_user"])!=null)
	{
		$loginmaster = $mysqli->prepare("update sub_clients SET online_status = ? where id=?");
		$loginmaster->bind_param("ii",$_POST["sendval"],$_POST["sub_user"]);
		$loginmaster->execute();
		$loginmaster->close();
		
		$_SESSION["userIsOnline"] = $_POST["sendval"];
	}
	else
	{
		$loginmaster = $mysqli->prepare("update login_master SET is_online = ? where uid=?");
		$loginmaster->bind_param("ii",$_POST["sendval"],$_SESSION['userId']);
		$loginmaster->execute();
		$loginmaster->close();
	}
	
	$_SESSION["userIsOnline"] = $_POST["sendval"];
}
?>