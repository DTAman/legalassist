<?php
include('../includes/basic_auth.php'); 

$cnt = 0;
if(isset($_POST['hfLeadId']) && trim($_POST['hfLeadId'])!=0)
{
	$loginmaster = $mysqli->prepare("select count(*) from lead_callbacks where lead_id = ? and added_by = ? and status = 'AC'");
	$loginmaster->bind_param("ii",$_POST['hfLeadId'],$_SESSION['userId']);
	$loginmaster->execute();
	$loginmaster->bind_result($cnt);
	$loginmaster->fetch();
	$loginmaster->close();
}

echo $cnt;
?>