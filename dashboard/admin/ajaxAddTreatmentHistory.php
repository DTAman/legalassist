<?php

include('../includes/basic_auth.php'); 

function return_nulls($val)
{
	if(trim(strtolower($val))=='yes' || trim(strtolower($val)=='no'))
	{
		return trim(strtolower($val))=='yes'?1:0;
	}
	else if(trim(strtolower($val))==null)
	{
		return null;
	}
}

if(isset($_POST["hfLeadId"]) && isset($_POST["rbtnAmbulanceComeScene"]) && isset($_POST["rbtnAnyPainRelief"]) && isset($_POST["rbtnRecommendedPhysioChrio"]) && isset($_POST["IsPhysio"]) && isset($_POST["affordPhysio"]) && isset($_POST["txtPhysioVisits"]) && isset($_POST["affordChiro"]) && isset($_POST["txtChirioVisits"]) && isset($_POST["rbtnXrayScan"]) && isset($_POST["txtScanResult"]) && isset($_POST["txtNoScan"]) && isset($_POST["txtPreExistingInjuries"]) && isset($_POST["txtAnyOther"]) && isset($_POST["rbtnVisitGP"]) && isset($_POST["txtGPLastVisit"]) && isset($_POST["txtGPName"]) && isset($_POST["txtGPSurgeryName"]) && isset($_POST["txtGPVisits"]) && isset($_POST["rbtnGoToHospital"]) && isset($_POST["txtAmbulanceHospitalDetails"]) && isset($_POST["txtFriendHospitalDetails"]) && isset($_POST["ambulanceTreatScene"]) && isset($_POST["txtWhatWasTreatment"]) && isset($_POST["rbtnAmbulanceTakenHospital"]) && isset($_POST["txtWhatWasHospitalTreatment"]) && trim($_POST['hfLeadId'])!=0 && isset($_POST['hfLeadAccidentId']) && trim($_POST['hfLeadAccidentId'])!=0)
{
	$lead_id = $_POST['hfLeadId'];
	$hfLeadAccidentId = $_POST['hfLeadAccidentId'];
	$dizziness = $nausea = $post_traumatic_stress_disorder = $insomnia = $vertigo = $driving_difficulty_post_accident = $fear_of_being_hit = 0;	
	
	//For radio Buttons
	$rbtnAnyPainRelief = return_nulls($_POST['rbtnAnyPainRelief']); 
	$rbtnAmbulanceComeScene = return_nulls($_POST['rbtnAmbulanceComeScene']); 
	$rbtnRecommendedPhysioChrio = return_nulls($_POST['rbtnRecommendedPhysioChrio']); 
	$IsPhysio = return_nulls($_POST['IsPhysio']); 
	$affordPhysio = return_nulls($_POST['affordPhysio']); 
	$affordChiro = return_nulls($_POST['affordChiro']); 
	$rbtnXrayScan = return_nulls($_POST['rbtnXrayScan']); 
	$rbtnVisitGP = return_nulls($_POST['rbtnVisitGP']); 
	$rbtnGoToHospital = return_nulls($_POST['rbtnGoToHospital']); 
	$ambulanceTreatScene = return_nulls($_POST['ambulanceTreatScene']);
	$rbtnAmbulanceTakenHospital = return_nulls($_POST['rbtnAmbulanceTakenHospital']);
	$PhysioChiro = return_nulls($_POST['PhysioChiro']);
	
	//For Date Format 
	// $txtGPLastVisit = trim($_POST['txtGPLastVisit'])!=null?date('Y-m-d',strtotime($_POST['txtGPLastVisit'])):null;
	$txtGPLastVisit = trim($_POST['txtGPLastVisit']);
	
	//For Textboxes and Textareas
	$txtPhysioVisits = $_POST['txtPhysioVisits']; 
	$txtChirioVisits = $_POST['txtChirioVisits']; 
	$txtScanResult = $_POST['txtScanResult']; 
	$txtNoScan = $_POST['txtNoScan']; 
	$txtPreExistingInjuries = $_POST['txtPreExistingInjuries']; 
	$txtWhatWasTreatment = $_POST['txtWhatWasTreatment']; 
	$txtAmbulanceHospitalDetails = $_POST['txtAmbulanceHospitalDetails']; 
	$txtFriendHospitalDetails = $_POST['txtFriendHospitalDetails']; 
	$txtAnyOther = $_POST['txtAnyOther']; 
	$txtGPName = $_POST['txtGPName']; 
	$txtGPSurgeryName = $_POST['txtGPSurgeryName']; 
	$txtGPVisits = $_POST['txtGPVisits']; 
	$txtWhatWasHospitalTreatment = $_POST['txtWhatWasHospitalTreatment']; 
	
	$is_chiro_recommended = null;
	$is_physio_recommended = null;
	
	$physiosessions = null;
	$chiriosessions = null;
	$physiotakingsessions = null;
	$chiriotakingsessions = null;

	if($rbtnRecommendedPhysioChrio==1)
	{
		if(trim($IsPhysio)!=null && ($IsPhysio ==1 || $IsPhysio ==0))
		{
			if($IsPhysio==1)
			{
				$is_physio_recommended=1;
				$physiosessions = $_POST['ddlPhysioSessions']; 
				$physiotakingsessions = $_POST['txtPhysioSessionsTaking']; 
				
			}
			else if($IsPhysio==0)
			{
				$is_chiro_recommended=1;
				$chiriosessions = $_POST['ddlChirioSessions']; 
				$chiriotakingsessions = $_POST['txtChirioSessionsTaking']; 
			}
		}
	}
	
	
	$injury_array = array();
	$injury_array = json_decode($_POST['injuries']);
	
	foreach($injury_array as $my_array)
	{
		$name_attr = $my_array->name_attr;
		
		if($name_attr=='dizziness')
		{
			$dizziness = 1;
		}
		else if($name_attr=='nausea')
		{
			$nausea = 1;
		}
		else if($name_attr=='post_traumatic_stress_disorder')
		{
			$post_traumatic_stress_disorder = 1;
		}
		else if($name_attr=='insomnia')
		{
			$insomnia = 1;
		}
		else if($name_attr=='vertigo')
		{
			$vertigo = 1;
		}
		else if($name_attr=='driving_difficulty_post_accident')
		{
			$driving_difficulty_post_accident = 1;
		}
		else if($name_attr=='fear_of_being_hit')
		{
			$fear_of_being_hit = 1;
		}
	}
	
	$view_treatment_history = getQuery('select id from lead_treatment_history where lead_accident_id='.$hfLeadAccidentId);
	if(mysqli_num_rows($view_treatment_history)>0)
	{
		$rc6 = mysqli_fetch_object($view_treatment_history);
		
		$loginmaster = $mysqli->prepare("update lead_treatment_history set lead_id=?, ambulance_at_scene=?, ambulance_treated_at_scene=?, ambulance_treatment=?, ambulance_took_to_hospital=?, ambulance_hospital_detail=?, painreliefs_recommended=?, has_physio_chirio=?, is_physio_recommended=?, is_chiro_recommended=?, can_afford_physio=?, can_afford_chiropractic=?, visits_for_physio=?, visits_for_chiro=?, have_x_ray=?, x_ray_result=?, why_not_x_ray=?, pre_existing_injuries=?, dizziness=?, nausea=?, post_traumatic_stress_disorder=?, insomnia=?, vertigo=?, driving_difficulty_post_accident=?, fear_of_being_hit=?, went_hospital_with_friend=?, hospital_detail_took_by_friend=?, visited_gp=?, gp_name=?, surgery_name=?, visits_to_gp=?, last_visit_to_gp=?, any_other_psychological_injury=?,hospital_treatment=?, physiosessions=?, physiotakingsessions=?, chiriosessions=?, chiriotakingsessions=? where id=?");
		$loginmaster->bind_param("iiisisiiiiiiiiisssiiiiiiiisississsssssi",$lead_id, $rbtnAmbulanceComeScene, $ambulanceTreatScene, $txtWhatWasTreatment, $rbtnAmbulanceTakenHospital, $txtAmbulanceHospitalDetails, $rbtnAnyPainRelief, $rbtnRecommendedPhysioChrio, $is_physio_recommended,$is_chiro_recommended, $affordPhysio, $affordChiro, $txtPhysioVisits, $txtChirioVisits, $rbtnXrayScan, $txtScanResult, $txtNoScan, $txtPreExistingInjuries, $dizziness, $nausea, $post_traumatic_stress_disorder, $insomnia, $vertigo, $driving_difficulty_post_accident, $fear_of_being_hit, $rbtnGoToHospital, $txtFriendHospitalDetails, $rbtnVisitGP, $txtGPName, $txtGPSurgeryName, $txtGPVisits, $txtGPLastVisit, $txtAnyOther,$txtWhatWasHospitalTreatment,$physiosessions, $physiotakingsessions, $chiriosessions, $chiriotakingsessions,$rc6->id);
	}
	else
	{
		$loginmaster = $mysqli->prepare("INSERT INTO lead_treatment_history(lead_id, ambulance_at_scene, ambulance_treated_at_scene, ambulance_treatment, ambulance_took_to_hospital, ambulance_hospital_detail, painreliefs_recommended, has_physio_chirio, is_physio_recommended, is_chiro_recommended, can_afford_physio, can_afford_chiropractic, visits_for_physio, visits_for_chiro, have_x_ray, x_ray_result, why_not_x_ray, pre_existing_injuries, dizziness, nausea, post_traumatic_stress_disorder, insomnia, vertigo, driving_difficulty_post_accident, fear_of_being_hit, went_hospital_with_friend, hospital_detail_took_by_friend, visited_gp, gp_name, surgery_name, visits_to_gp, last_visit_to_gp, any_other_psychological_injury, hospital_treatment,lead_accident_id,physiosessions, physiotakingsessions, chiriosessions, chiriotakingsessions) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		$loginmaster->bind_param("iiisisiiiiiiiiisssiiiiiiiisississsissss",$lead_id, $rbtnAmbulanceComeScene, $ambulanceTreatScene, $txtWhatWasTreatment, $rbtnAmbulanceTakenHospital, $txtAmbulanceHospitalDetails, $rbtnAnyPainRelief, $rbtnRecommendedPhysioChrio, $is_physio_recommended,$is_chiro_recommended, $affordPhysio, $affordChiro, $txtPhysioVisits, $txtChirioVisits, $rbtnXrayScan, $txtScanResult, $txtNoScan, $txtPreExistingInjuries, $dizziness, $nausea, $post_traumatic_stress_disorder, $insomnia, $vertigo, $driving_difficulty_post_accident, $fear_of_being_hit, $rbtnGoToHospital, $txtFriendHospitalDetails, $rbtnVisitGP, $txtGPName, $txtGPSurgeryName, $txtGPVisits, $txtGPLastVisit, $txtAnyOther,$txtWhatWasHospitalTreatment, $hfLeadAccidentId,$physiosessions, $physiotakingsessions, $chiriosessions, $chiriotakingsessions);
	}
	$loginmaster->execute();
	echo $mysqli->error;
	$loginmaster->close();
}

?>