<?php 
include('../includes/basic_auth.php'); 
$is_dis = 0;
if(isset($_GET['s']) && trim($_GET['s']==1))
{
	$is_dis = 1;
}
			
if(isset($_GET['u']) && trim($_GET['u']!=null))
{
	$uid = trim($_GET['u']);
	
	$view_result = getUserData($uid, $CHR);
	if(mysqli_num_rows($view_result)==0)
	{
		header("location:index.php");
	}
	else
	{
		$rc = mysqli_fetch_object($view_result);
	}
}
else
{
	header("location:index.php");
}

$getStates = getStates();
$getCallcenters = getCallcenters('');

if(isset($_POST["btnCallCenter"]))
{
	if(trim($_POST["ddlCallCenterName"])!=null && trim($_POST["ddlState"])!=null && trim($_POST["ddlSubUrb"])!=null && trim($_POST["txtCallCenterAddress"])!=null && trim($_POST["txtCallCenterPhone"])!=null && trim($_POST["txtCallCenterPostal"])!=null)
	{
		$count_users = countUsersWithMobileforEdit($_POST['txtCallCenterPhone'],$uid);

		if($count_users==0)
		{
			$loginmaster = $mysqli->prepare("update login_master set phone=?, status=?, updation_date=? where uid=?");
			$loginmaster->bind_param("sssi",$_POST['txtCallCenterPhone'],$_POST['rbtnStatus'],$thisdate, $uid);
			$loginmaster->execute();
			$loginmaster->close();
			 
			$loginmaster = $mysqli->prepare("update callcenter_hr set callcenter_id=?, phone=?, alt_number=?, state_id=?, suburb_id=?, postal_code=?, address=? where uid=?");
			$loginmaster->bind_param("issiissi",$_POST['ddlCallCenterName'],$_POST['txtCallCenterPhone'],$_POST['txtCallCenterAltPhone'],$_POST['ddlState'],$_POST['ddlSubUrb'],$_POST['txtCallCenterPostal'],$_POST['txtCallCenterAddress'],$uid);
			$loginmaster->execute();
			$loginmaster->close();
			
			$_SESSION['success']='HR updated successfully.';
			
			$_POST=array();
			header( "refresh:3;url=edit-call-center-hr-each.php?u=".$uid);
		}
		else
		{
			$_SESSION['error']="A user exists with this mobile number.";
		}
	}
	else
	{
		$_SESSION['error']="Enter all the required fields.";
	}
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>

	<body class="fixed-nav sticky-footer" id="page-top">
	
		<?php include('../includes/navigation.php'); ?>
	  
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Edit HR</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
			
				<!-- row -->
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
							<div class="card-body">
								<form name='frmEditHR' id='frmEditHR' action='' method='post'>
									<?php include("../includes/messages.php") ?>
									<div class="row">
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="ddlCallCenterName">Select Call center</label>
											<select name="ddlCallCenterName"  id="ddlCallCenterName" class="d-flex form-control">
												<option value=''>Select Call center</option>
												<?php
												while($crow = mysqli_fetch_object($getCallcenters))
												{
													?>	
														<option <?php echo $rc->callcenter_id==$crow->id?'selected':''; ?> value='<?php echo $crow->id ?>'><?php echo $crow->centre_name ?></option>
													<?php
												}
												?>
											</select>
											<input value="<?php echo $rc->uid ?>" name="hfUID" id="hfUID" type="hidden"/>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtCallCenterPhone">Phone Number</label>
											<input value="<?php echo $rc->phone ?>" class="form-control" name="txtCallCenterPhone" id="txtCallCenterPhone" placeholder="Mobile Number" type="text"/>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtCallCenterAltPhone">Alternate Number</label>
											<input value="<?php echo $rc->alt_number ?>" class="form-control" name="txtCallCenterAltPhone" id="txtCallCenterAltPhone" placeholder="Alternate Mobile" type="text" />
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="">Address</label>
											<select name="ddlState" id="ddlState" class="d-flex form-control" onchange="getAllSuburbs(this.value)">
												<option value=''>Select State</option>
												<?php
													while($staterow = mysqli_fetch_object($getStates))
													{
														?>	
															<option <?php echo $rc->state_id==$staterow->id?'selected':''; ?> value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
														<?php
													}
												?>
												
											</select>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="ddlSubUrb" class="invisible d-none d-md-block" >Select Sub urb</label>
											<select name="ddlSubUrb" id="ddlSubUrb" class="d-flex form-control">
												<?php
												$getSubUrbs = getSubUrbs(" and state_id = ".$rc->state_id);
												while($srow = mysqli_fetch_object($getSubUrbs))
												{
													?>	
														<option value='<?php echo $srow->id ?>' <?php echo $rc->suburb_id==$srow->id?'selected':''; ?>><?php echo $srow->suburb_name ?></option>
													<?php
												}
												?>
											</select>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="txtCallCenterPostal" class="invisible d-none d-md-block" >Postal Code</label>
											<input value="<?php echo $rc->postal_code ?>"  class="form-control" name="txtCallCenterPostal" id="txtCallCenterPostal" placeholder="Postal Code" type="text" />
										  </div>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
										  <div class="form-group fullwidthTextarea">
											<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Address First Line" name='txtCallCenterAddress' id='txtCallCenterAddress'><?php echo $rc->address ?></textarea>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
											<label class="d-block">Status</label>
											<div class="custom-radio d-inline-block fl-left mr-3">
												<input <?php echo $rc->status=='AC'?'checked':''; ?> type="radio" class="custom-control-input" id="rbtnActive" name="rbtnStatus" value="AC"/>
												<label class="custom-control-label" for="rbtnActive">Active</label>
											</div>
											<div class="custom-radio d-inline-block fl-left mr-3">
												<input <?php echo $rc->status=='IN'?'checked':''; ?> type="radio" class="custom-control-input" id="rbtnInactive" name="rbtnStatus" value="IN"/>
												<label class="custom-control-label" for="rbtnInactive">Inactive</label>
											</div>
										</div>
										
										<?php
										if($is_dis!=1)
										{
											?>
											<div class="col-md-12 col-sm-12 col-xs-12">
												<input name="btnCallCenter" id="btnCallCenter" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save" />
											</div>
											<?php											
										}
										?>
										
										
									  </div>
								</form>
							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			
		<?php include('../includes/copyright.php'); ?>
		
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
	  
		<?php
			if($is_dis==1)
			{
				?>
					<script>is_disabled(<?php echo $is_dis ?>);</script>
				<?php
			}
		?>
	</body>
</html>
