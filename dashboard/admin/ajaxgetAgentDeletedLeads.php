<?php
include('../includes/basic_auth.php'); 

$p=1;
$pagecount=1;
$last_page=1;
$first_page=0;
$offset=0;
$condition=" and (client_status!='IP' or client_status is NULL) ";

if($_SESSION['userType']==$CA || $_SESSION['userType']==$CTL)
{
	$datamaster = $mysqli->prepare("SELECT callcenter_id FROM tl_and_agents where uid = ?");
	$datamaster->bind_param('i',$_SESSION['userId']);
	$datamaster->execute();
	$datamaster->bind_result($u_cid);
	$datamaster->fetch();
	$datamaster->close();
}
else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
{
	$u_cid = $_SESSION['userId'];
}

if($_SESSION['userType']!=$MSW)
{
	$condition.=" and admin_assigned_directly=0 ";
}
	
$usertype = $_SESSION["userType"];

if(isset($_POST['page_skip']))
{
	$offset=($_POST['page_skip']-1)*$limit;
	$n = $offset+1;
}
else
{
	$_POST['page_skip']=0;
}

$linked_ids_con = $condition;

if(trim($_POST['txtSearch'])!=null)
{
	$condition.=" and (phone like '%{$_POST['txtSearch']}%' or email like '%{$_POST['txtSearch']}%' or first_name like '%{$_POST['txtSearch']}%' or middle_name like '%{$_POST['txtSearch']}%' or last_name like '%{$_POST['txtSearch']}%') ";
}

if(trim($_POST['txtFromDate'])!=null)
{
	$create_date=date('Y-m-d',strtotime($_POST['txtFromDate']));
	$condition.=" and date(updation_date) >= '".get_timezone_offset_calender($create_date,'UTC',$_SESSION['userTimeZone'])."'";
}

if(trim($_POST['txtToDate'])!=null)
{
	$to_date=date('Y-m-d',strtotime($_POST['txtToDate']));
	$condition.=" and date(updation_date) <= '".get_timezone_offset_calender($to_date,'UTC',$_SESSION['userTimeZone'])."'";
}

if(trim($_POST['txtAccidentDateSearchFrom'])!=null && trim($_POST['txtAccidentDateSearchTo'])!=null)
{
	$ac_date_from=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchFrom']));
	$ac_date_to=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchTo']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) >= '".$ac_date_from."' and date(accident_date_time) <= '".$ac_date_to."' and lead_accidents.status=1)) ";
}
else if(trim($_POST['txtAccidentDateSearchFrom'])!=null)
{
	$ac_date_from=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchFrom']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) >= '".$ac_date_from."' and lead_accidents.status=1)) ";
}
else if(trim($_POST['txtAccidentDateSearchTo'])!=null)
{
	$ac_date_to=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchTo']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) <= '".$ac_date_to."' and lead_accidents.status=1)) ";
}
		
if(trim($_POST['ddlState'])!=null)
{
	$condition.=" and leads.state_id = ".$_POST['ddlState'];
}

if(isset($_POST['ddlLCallcenters']) && trim($_POST['ddlLCallcenters'])!=null)
{
	$condition.=" and leads.callcenter_id = ".$_POST['ddlLCallcenters'];
}

if(isset($_POST['ddlClients']) && is_array($_POST['ddlClients']) && count(array_filter($_POST['ddlClients']))>0)
{
	$condition.=" and assigned_client in (".trim(implode(',',$_POST['ddlClients']),',').")";
}

$outcome_array = array();
$c1='';
$c2='';
if(isset($_POST['ddlLeadStatus']) && is_array($_POST['ddlLeadStatus']) && count($_POST['ddlLeadStatus'])>0)
{
	foreach($_POST['ddlLeadStatus'] as $ddlLeadStatus)
	{
		if(trim($ddlLeadStatus)!=null)
		{
			if($ddlLeadStatus=='DA')
			{
				$c2.=" or leads.outcome_type is null";
			}
			else
			{
				array_push($outcome_array,"'".$ddlLeadStatus."'");
			}
		}
	}
	
	if(count($outcome_array)>0)
	{
		$c1.=" leads.outcome_type in (".implode(',',$outcome_array).") ";
	}
		
	if(trim($c1)!=null || trim($c2)!=null)
	{
		$condition.=" and (".$c1.$c2.")";
	}
}

if(isset($_POST['ddlSubUrb']) && trim($_POST['ddlSubUrb'])!=null)
{
	$condition.=" and leads.suburb_id = ".$_POST['ddlSubUrb'];
}

if(isset($_POST['ddlAgentCheck']) && trim($_POST['ddlAgentCheck'])!=null)
{
	$condition.=" AND agent_assigned =  ".$_POST['ddlAgentCheck'] ." and callcenter_id = ".$u_cid;
}
if(isset($_POST['ddlTeamLeadCheck']) && trim($_POST['ddlTeamLeadCheck'])!=null)
{
	$condition.=" AND team_lead_assigned =  ".$_POST['ddlTeamLeadCheck'] ." and callcenter_id = ".$u_cid;
}
		
if(trim($_POST['ddlSortBy'])!=null)
{
	$orderby = $_POST['ddlSortBy'];
}

$outcome_array = array();
$c1='';
$c2='';
if(isset($_POST['ddlLeadStatus']) && is_array($_POST['ddlLeadStatus']) && count(array_filter($_POST['ddlLeadStatus']))>0)
{
	foreach($_POST['ddlLeadStatus'] as $ddlLeadStatus)
	{
		if(trim($ddlLeadStatus)!=null)
		{
			if($ddlLeadStatus=='DA')
			{
				$c2.=" or leads.outcome_type is null";
			}
			else
			{
				array_push($outcome_array,"'".$ddlLeadStatus."'");
			}
		}
	}
	
	if(count($outcome_array)>0)
	{
		$c1.=" leads.outcome_type in (".implode(',',$outcome_array).") ";
	}
		
	if(trim($c1)!=null || trim($c2)!=null)
	{
		$condition.=" and (".$c1.$c2.")";
	}
}

if(trim($_POST['ddlSortBy'])!=null)
{
	$orderby = $_POST['ddlSortBy'];
}

$condition.=" AND assigned_usertype = 'CA' and agent_assigned =  ".$_SESSION['userId']."  AND callcenter_id = ".$u_cid;
$linked_ids_con.=" AND assigned_usertype = 'CA' and agent_assigned =  ".$_SESSION['userId']."  AND callcenter_id = ".$u_cid;
/* $condition.=" and (( assigned_usertype is null and added_by = ".$_SESSION['userId']." ) or (assigned_usertype = 'CA' and if(((select comment_to from login_master inner join lead_comments on lead_comments.comment_to = login_master.uid where lead_comments.comment_utype='CA' and lead_comments.lead_id=leads.id order by lead_comments.creation_date desc limit 1)=".$_SESSION['userId']."),1,0)))";
	
$linked_ids_con.=" and (( assigned_usertype is null and added_by = ".$_SESSION['userId']." ) or (assigned_usertype = 'CA' and if(((select comment_to from login_master inner join lead_comments on lead_comments.comment_to = login_master.uid where lead_comments.comment_utype='CA' and lead_comments.lead_id=leads.id order by lead_comments.creation_date desc limit 1)=".$_SESSION['userId']."),1,0)))"; */

if(isset($_POST['nextpage']) && trim($_POST['nextpage'])!=null)
{
	$pagecount = $_POST['nextpage'];
	$last_page = $_POST['nextpage'];
}

if(isset($_POST['firstpage']) && trim($_POST['firstpage'])!=null)
{		
	$pagecount = $_POST['firstpage']-($set_count-1);
	$last_page = $_POST['firstpage']-($set_count-1);
}

if(isset($_POST['firstpage1']) && trim($_POST['firstpage1'])!=null)
{
	$firstpage1 = trim($_POST['firstpage1']);
	
	$pagecount = $firstpage1;
	$last_page = $firstpage1;
}

$condition.=" and (shifted_by_usertype!='S_CA' or shifted_by_usertype is NULL)";  //SHIFTED AGENT
$condition.=" and del_ag_id = ".$_SESSION['userId'];

$queryCount= "select /*+ MAX_EXECUTION_TIME(5000) */ count(*) from leads left outer join states on states.id = leads.state_id where leads.is_deleted=0 ".$condition;
$queryResults = "select leads.*, leads.suburb_name as textbox_suburb,state_name,initials,
(case when (master_lead_id is not null and master_lead_id > 0) 
 THEN
      (select group_concat(m.id SEPARATOR ',') from leads m where (m.master_lead_id = leads.master_lead_id or m.id = leads.master_lead_id) and m.id!= leads.id ".$linked_ids_con." and m.is_deleted=0)
 ELSE
      (select group_concat(m.id SEPARATOR ',') from leads m where m.master_lead_id=leads.id and m.id!=leads.id ".$linked_ids_con." and  m.is_deleted=0)
 END)
 as linked_ids
 from leads left outer join states on states.id = leads.state_id where leads.is_deleted=0 ".$condition." order by ".$orderby." desc limit ".$limit." offset ".$offset;

$totalCount = getFetchQuery($queryCount);
$totalResults = getQuery($queryResults);

if(mysqli_num_rows($totalResults)>0)
{
	?>
		<div class="hh5">
		<table class="table newTable table-bordered">
			<thead>
				<tr>
					<th class='text-center width20'>Sr.No.</th>
					<th class="text-center width20">Id</th>
					<th class="text-center">Linked Ids</th>
					<th>Name</th>
					<th>Accident&nbsp;Type and Date</th>
					<th class="widthCustom">Phone</th>
					<th class="widthCustom small">State</th>
					<?php
					if($_SESSION['userType']=='CA' || $_SESSION['userType']=='CTL' )
					{
						?>
							<th class="widthCustom">Email</th>
							<th>Address</th>
						<?php
					}
					else
					{
						?>
							<th>SignUp</th>
						<?php
					}
					if($_SESSION['userType']!=$C)
					{
						?>
							<th>Sol Notes</th>
						<?php
					}
					if($_SESSION['userType']==$C || $_SESSION['userType']==$CCA || $_SESSION['userType']==$MSW || $_SESSION['userType']==$CTL)
					{
						?>
							<th>Admin Notes</th>
						<?php
					}
					if($_SESSION['userType']==$MSW)
					{
						?>
							<th class="widthCustom small">Callcenter</th>
							<th class="widthCustom small">Sol</th>
							<th class="widthCustom small">Verifier</th>
						<?php
					}
					if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
					{
						?>
							<th class="widthCustom text-center">Sol</th>
							<th class="widthCustom text-center">Team Lead</th>
							<th class="widthCustom text-center">Agent</th>
						<?php
					}
					
					if($_SESSION['userType']==$CTL)
					{
						?>
							<th class="widthCustom text-center">Sol</th>
							<th class="widthCustom text-center">Agent</th>
						<?php
					}
					
					if($_SESSION['userType']!=$C)
					{
						?>
							<th class="widthCustom">Assigned&nbsp;On</th>
						<?php
					}
					if($_SESSION['userType']==$MSW || $_SESSION['userType']==$CCA  || $_SESSION['userType']==$C  || $_SESSION['userType']==$CTL)
					{
						?>
							<th class="widthCustom">Modified&nbsp;On</th>
							<th>Outcomes</th>
						<?php
					}
					?>
					<th>Lead Updates</th>
					<th class='text-center'>View</th>
				</tr>
			</thead>
			<tbody>
			<?php
			while($rc = mysqli_fetch_object($totalResults))
			{
				?>
					<tr>
						<td class='text-center width20'><?php echo $n ?></td>
						<td class='text-center width20'><?php echo $rc->id ?></td>
						<td class='text-center'>
							<?php
									// echo getLinkedIds($rc->id,$rc->master_lead_id,$linked_ids_con);
								
									$str='';
									$res = $rc->linked_ids;
									$resr = explode(',',$res);
									foreach($resr as $kkey)
									{
										$str.=', <a class="text-danger" target="_blank" href="edit-lead-each.php?u='.$kkey.'">'.$kkey.'</a>';
									}
									
									echo trim($str,','); 
							?>
						</td>
						<td><?php echo $rc->first_name.' '.$rc->middle_name.' '.$rc->last_name; ?></td>
						<td>
							<?php
								if($rc->last_accident_type!=null)
								{
									if($rc->last_accident_na!=1)
									{
										echo $rc->last_accident_type." <br/><span style='color:red'>(".date('d-M-Y',strtotime($rc->latest_accident_dt)).")</span>";
									}
									else
									{
										echo $rc->last_accident_type." <br/><span style='color:red'>(".date('M-Y',strtotime($rc->latest_accident_dt)).")</span>";
									}
								}
								
							?>
						</td>
						<td class="widthCustom"><?php echo $rc->phone ?></td>
						<td class="widthCustom small"><?php echo $rc->initials ?></td>
						
						<?php
						if($_SESSION['userType']=='CA' || $_SESSION['userType']=='CTL' )
						{
							?>
								<td class="widthCustom"><?php echo $rc->email ?></td>
								<td><?php echo $rc->address ?></td>
							<?php
						}
						else
						{
							$client_id = null;
							if($_SESSION['userType']=='C')
							{
								$client_id = $_SESSION['userId'];
							}
							?>
							
							
							<!--
							<td><?php #echo getSignUpDetails($rc->id, $client_id) ?></td>
							-->
							
							<td style="max-width:500px">
								<?php
									$getSignUpDetails = str_replace('"','',$rc->latest_signup_detail);
									$x='';
									if(trim($getSignUpDetails)!=null)
									{
										if($rc->latest_signuptype=='E' || $rc->latest_signuptype=='P')
										{
											$x= $rc->latest_signuptype=='E'?'Email':'Post';
											$x.=", <span style='color:red'>Call: ".date("d M,Y",strtotime($rc->latest_signup_datetime))." (".$rc->latest_signup_timezone.")</span>, Comments: ".$getSignUpDetails; 
											
											$x = '<p data-toggle="tooltip" data-original-title="'.trim(strip_tags($x)).'">'.$x.'</p>';
										}
										else if($rc->latest_signuptype=='O' || $rc->latest_signuptype=='H')
										{
											$x = $rc->latest_signuptype=='O'?'Office Signup':'Home Signup';
											$x.=", <span style='color:red'>Call: ".date("d M,Y H:i",strtotime($rc->latest_signup_datetime))." (".$rc->latest_signup_timezone.")</span>, Comments: ".$getSignUpDetails; 
											
											$x = '<p data-toggle="tooltip" data-original-title="'.trim(strip_tags($x)).'">'.$x.'</p>';
										}
										
										echo '<button id="'.$rc->id.'signup" class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" a_val="'.strip_tags(preg_replace('/\s+/', ' ',$x)).'" onclick="sign_val_copy('.$rc->id.')"></button>'.$x;
									}
								?>
							</td>
							<?php
						}

					
						/* if($_SESSION['userType']!=$C)
						{
							// $getCallBackDetails = getCallBackDetails($rc->id) ;
							$getCallBackDetails = str_replace('"','',$rc->latest_calback);
							?>
							<td style="max-width:500px">
								<?php
									if(trim($getCallBackDetails)!=null)
									{
										?>
											<button class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" onclick="copyClipbord('<?php echo preg_replace('/\s+/', ' ',$getCallBackDetails) ?>')"></button>
										<?php
									}
								?>
								<p data-toggle="tooltip" data-original-title="<?php echo $getCallBackDetails ?>"><?php echo $getCallBackDetails ?></p>
							</td>
							<?php
						} */
						
						if($_SESSION['userType']!=$C)
						{
							$latest_sol_notes = str_replace('"','',$rc->latest_sol_notes);
							?>
							<td style="max-width:500px">
								<?php
									if(trim($latest_sol_notes)!=null)
									{
										?>
											<button class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" onclick="copyClipbord('<?php echo preg_replace('/\s+/', ' ',$latest_sol_notes) ?>')"></button>
										<?php
									}
								?>
								<p data-toggle="tooltip" data-original-title="<?php echo $latest_sol_notes ?>"><?php echo $latest_sol_notes ?></p>
							</td>
							<?php
						}
						
						if($_SESSION['userType']==$C || $_SESSION['userType']==$CCA || $_SESSION['userType']==$MSW || $_SESSION['userType']==$CTL)
						{
							// $getNotesDetails = getNotesDetails($rc->id) ;
							$getNotesDetails = str_replace('"','',$rc->latest_notes) ;
							?>
								<td style="max-width:500px">
									<?php
										if(trim($getNotesDetails)!=null)
										{
											?>
												<button class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" onclick="copyClipbord('<?php echo preg_replace('/\s+/', ' ', $getNotesDetails) ?>')"></button>
											<?php
										}
									?>
									<p data-toggle="tooltip" data-original-title="<?php echo $getNotesDetails ?>"><?php echo $getNotesDetails ?></p>
								</td>
							<?php
						}
						if($_SESSION['userType']==$MSW)
						{
							// echo getCallcenterAndClient($rc->id); 
							?>
								<td class='widthCustom small'><?php echo isset($callcenter_array[$rc->callcenter_id])?$callcenter_array[$rc->callcenter_id]:'' ?></td>
								<td class='widthCustom small'><?php echo isset($client_array[$rc->assigned_client])?$client_array[$rc->assigned_client]:''; ?></td>
								<td class='widthCustom small'><?php echo isset($verifier_array[$rc->assigned_verifier])?$verifier_array[$rc->assigned_verifier]:''; ?></td>
							<?php 
						}
						/* if(($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0) || ($_SESSION['userType']==$CTL))
						{
							echo getClientForCallcenter($rc->id); 
						} */
						if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
						{
							// echo getLeadTLAndAgentName($rc->id,$_SESSION['userId']);
							?>
								<td class='widthCustom small'><?php echo isset($client_array[$rc->assigned_client])?$client_array[$rc->assigned_client]:''; ?></td>
								<td class='widthCustom small'><?php echo isset($team_lead_array[$rc->team_lead_assigned])?$team_lead_array[$rc->team_lead_assigned]:'' ?></td>
								<td class='widthCustom small'><?php echo isset($agents_array[$rc->agent_assigned])?$agents_array[$rc->agent_assigned]:'' ?></td>
							<?php 																
						}
						if($_SESSION['userType']==$CTL)
						{
							// echo getLeadagentNameForTL($rc->id,$_SESSION['userId']); 
							?>
								<td class='widthCustom small'><?php echo isset($client_array[$rc->assigned_client])?$client_array[$rc->assigned_client]:''; ?></td>
								<td class='widthCustom small'><?php echo isset($agents_array[$rc->agent_assigned])?$agents_array[$rc->agent_assigned]:'' ?></td>
							<?php 
						}
						
						if($_SESSION['userType']!=$C)
						{
							switch($_SESSION['userType'])
							{
								case 'C': $d = $rc->assign_client;break;
								case 'CCA': $d = $rc->assign_n_callcenter;
											if($_SESSION['is_affliated']==1)
											{
												$d = $rc->assign_affliated;
											}
											break;
								case 'CTL': $d = $rc->assign_teamlead;break;
								case 'CA': $d = $rc->assign_agent;break;
								case 'MSW': $d = $rc->assign_admin;break;
								case 'VA': $d = $rc->assign_verifier;break;
							}
							if($d!=null)	
							{
								?>
									<td class="widthCustom"><?php echo get_timezone_offset(date('Y-m-d H:i:s',strtotime($d)),'UTC',$_SESSION['userTimeZone']) ?></td>
								<?php
							}
							else
							{
								?>
									<td></td>
								<?php
							}
						}
						if($_SESSION['userType']==$MSW || $_SESSION['userType']==$CCA  || $_SESSION['userType']==$C  || $_SESSION['userType']==$CTL)
						{
						?>
							<td class="widthCustom"><?php echo get_timezone_offset(date('Y-m-d H:i:s',strtotime($rc->updation_date)),'UTC',$_SESSION['userTimeZone']) ?></td>
							<td style="max-width:500px">
							<?php 
								echo $rc->outcome 
							?>
							</td>
							<?php
						}
						?>
						<td style="max-width:500px">
						<?php
							// $com = getComments($rc->id);
							$com = str_replace('"','',$rc->last_comment);
						?>
						
						<?php
							if(trim($com)!=null)
							{
								?>
									<button class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" onclick="copyClipbord('<?php echo preg_replace('/\s+/', ' ', $com) ?>')"></button>
								<?php
							}
						?>
						
						<p data-toggle="tooltip" data-original-title="<?php echo $com ?>"><?php echo $com  ?></p>
						<?php
						if($_SESSION['userType']==$CCA || $_SESSION['userType']==$MSW)
						{
							?>
								<span style='color:red'><?php echo $rc->msw_to_cc_reject_reason ?></span>
							<?php
						}
						?>
						</td>
						<td class='text-center'>
							<div class="icnos">
								<a target='_blank' class="text-success icn" href="view-each-lead.php?u=<?php echo $rc->id ?>" title="" data-toggle="tooltip" data-original-title="View"><i class="ti-eye"></i></a>
								<a class="text-info icn" onclick="return confirm('Are You Sure?')" href="reassign_lead_agent.php?u=<?php echo $rc->id ?>&a=0" title="" data-toggle="tooltip" data-original-title="Restore"><i class="fa fa-registered"></i></a>
							</div>
						</td>
						
					</tr>
				<?php
				$n++;
			}
			?>
			</tbody>
		</table>
		</div>
		<?php include('../includes/ajax_pagination.php'); ?>
		
	</div>
	<?php
}
else
{
	?>
		<?php include('../includes/norows.php'); ?>
	<?php
}
?>