<?php 
include('../includes/basic_auth.php');

$condition='';

if((isset($_GET['u']) && trim($_GET['u'])!=null) || (isset($_POST['is_post']) && trim($_POST['is_post'])!=null))
{
	if(isset($_POST['is_post']) && trim($_POST['is_post'])!=null)
	{
		$lead_id = trim($_POST['txtLeadId']);
	}
	else
	{
		$lead_id = trim($_GET['u']);
	}
	
	if(isset($_GET['a']) && trim($_GET['a'])==0)
	{
		$_SESSION['success']='Lead has been assigned to admin successfully. Refreshing page in 2 seconds.';	
	}
	else
	{
		$datamaster = $mysqli->prepare("select assigned_client, callcenter_id, assigned_usertype, assigned_verifier, agent_assigned, team_lead_assigned from leads where id=?");
		$datamaster->bind_param("i",$lead_id);
		$datamaster->execute();
		$datamaster->bind_result($l_assigned_client, $l_assigned_callcenter_id, $assigned_usertype, $l_assigned_verifier, $l_assigned_agent , $l_assigned_team_lead);
		$datamaster->fetch();
		$datamaster->close();
		
		if($l_assigned_client!=null)
		{
			$loginmaster = $mysqli->prepare("update leads set assign_admin = ?,assigned_client=null,assigned_usertype='MSW', is_sent_to_client=0, updation_date=?, client_status=null, show_to_admin=1,reached_admin = 1 where id=?");
			$loginmaster->bind_param("ssi", $thisdate,$thisdate, $lead_id);
			$loginmaster->execute();
			$loginmaster->close();
		}
		else
		{
			$loginmaster = $mysqli->prepare("update leads set assign_admin = ?,assigned_usertype='MSW', updation_date=?, show_to_admin=1, reached_admin = 1 where id=?");
			$loginmaster->bind_param("ssi", $thisdate,$thisdate, $lead_id);
			$loginmaster->execute();
			$loginmaster->close();
		}
		
		$comment_status='DN';		
		$lead_transfer_type = 'LT';
		
		/* 
		Pulled From Affliated Callcenter - PFAC
		Pulled From Agent - PFA
		Pulled From Normal Callcenter - PFNC
		Pulled From solicitor - PFS
		Pulled From Team Lead - PFTL
		Pulled From Verifier - PFV 
		*/

		if(trim($assigned_usertype)==$C)
		{
			$txtSaveComment = 'Pulled From Sol';
			$outcome = 'PFS';
			$l_assigned_user = $l_assigned_client;
		}
		else if(trim($assigned_usertype)==$VA)
		{
			$txtSaveComment = 'Pulled From Verifier';
			$outcome = 'PFV';
			$l_assigned_user = $l_assigned_verifier;
			
			$datamaster = $mysqli->prepare("update leads set is_previous_verifier = 1 where id = ?");
			$datamaster->bind_param('i', $lead_id);
			$datamaster->execute();
			$datamaster->close();
		}
		else if(trim($assigned_usertype)==$CCA)
		{
			$datamaster = $mysqli->prepare("select is_affliated from callcenter where uid=?");
			$datamaster->bind_param('i', $l_assigned_callcenter_id);
			$datamaster->execute();
			$datamaster->bind_result($is_affliated);
			$datamaster->fetch();
			$datamaster->close();
		
			$outcome = $is_affliated==0?'PFNC':'PFAC';
			$l_assigned_user = $l_assigned_callcenter_id;
			$txtSaveComment = 'Pulled From CallCenter';
		}
		else if(trim($assigned_usertype)==$CA)
		{
			$outcome = 'PFA';
			$l_assigned_user = $l_assigned_agent;
			$txtSaveComment = 'Pulled From Agent';
		}
		else if(trim($assigned_usertype)==$CTL)
		{
			$outcome = 'PFTL';
			$l_assigned_user = $l_assigned_team_lead;
			$txtSaveComment = 'Pulled From Team Lead';
		}
		
		$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type,directly_assigned_admin) VALUES (?,(select uid from login_master where type='MSW'),'MSW',?,?,?,?,?,1)");
		$loginmaster->bind_param("iissss",$lead_id,$l_assigned_user,$txtSaveComment,$comment_status,$thisdate, $lead_transfer_type);
		$loginmaster->execute();
		$loginmaster->close();
		
		$last_comment = $txtSaveComment;
		if($l_assigned_client!=null)
		{
			$loginmaster = $mysqli->prepare("update leads set outcome=(select outcome_title from outcomes where outcome_initials = ?), outcome_type=?, latest_sol_notes = NULL, latest_signup_detail = NULL, latest_signuptype = NULL, latest_signup_datetime = NULL, latest_signup_timezone = NULL, last_directly_assign_admin_dt = ?, last_comment = ?, latest_spoken_to_client = 0, msw_lead_type = 'f',latest_comment_usertype_from = ?,latest_comment_usertype_to = ? WHERE id = ?");
		
		}
		else
		{
			$loginmaster = $mysqli->prepare("update leads set outcome=(select outcome_title from outcomes where outcome_initials = ?), outcome_type=?, last_directly_assign_admin_dt = ?, last_comment = ?, latest_spoken_to_client = 0, msw_lead_type = 'f',latest_comment_usertype_from = ?,latest_comment_usertype_to = ? WHERE id = ?");
		}
		$loginmaster->bind_param("ssssssi",$outcome, $outcome, $thisdate,$txtSaveComment, $MSW, $MSW, $lead_id);
		$loginmaster->execute();
		$loginmaster->close();
		
		if(!isset($_POST['is_post']) || trim($_POST['is_post'])==null)
		{
			$_SESSION['success']='Lead has been assigned to admin successfully.';
		}
	}
	
	restoreDeletedLead($lead_id, $thisdate);	
	if(!isset($_POST['is_post']) || trim($_POST['is_post'])==null)
	{
		header("location:deleted-leads.php");
	}
}
else
{
	header("location:index.php");
}

?>