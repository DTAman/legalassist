<div id="callPopUpClientPopUpId" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Have you called client?</h4>
			</div>
			<form action='' method='post' name='frmcallPopUpClientPopUp' id='frmcallPopUpClientPopUp' >
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="bg-primary-light form-group jumbotron p-4">
								<label class="d-block">Have you called client?</label>
								<div class="custom-radio mr-3">
									<input checked type="radio" class="custom-control-input" id="rbtnNoAnswer" name="rbtnClientPopup" value="NA" />
									<label class="custom-control-label" for="rbtnNoAnswer">No Answer</label>
								</div>
								<div class="custom-radio mr-3">
									<input type="radio" class="custom-control-input" id="rbtnCallBackRequested" name="rbtnClientPopup" value="CB" />
									<label class="custom-control-label" for="rbtnCallBackRequested">Call Back Requested</label>
								</div>
								<div class="custom-radio mr-3">
									<input type="radio" class="custom-control-input" id="rbtnSpokenToClient" name="rbtnClientPopup" value="SC" />
									<label class="custom-control-label" for="rbtnSpokenToClient">Spoken To Client</label>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<input class="form-control" value="" name="hfLeadPopUpLeadClientId" id="hfLeadPopUpLeadClientId" type="hidden"/>	
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<label class="d-block">Enter the details</label>
								<textarea required class="form-control mt-3" rows="4" style='width:100%' name='txtCallPopUpClientPopUp' id='txtCallPopUpClientPopUp'></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnCallPopUpClientPopUp" name="btnCallPopUpClientPopUp" type="submit" class="btn btn-primary" value='Save' />
				</div>
			</form>
		</div>
	</div>
</div>