<div id="furtherInvestigationModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Enter Sol Notes</h4>
			</div>
			<form action='' method='post' name='frmfurtherInvestigation' id='frmfurtherInvestigation'>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 inv">
							
						</div>
						<div class="col-md-12 col-sm-12">
							<input class="form-control" value="" name="hfLeadInvestigationId" id="hfLeadInvestigationId" type="hidden" />	
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<textarea required="required" class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter details here..." name='txtFurtherSaveComment' id='txtFurtherSaveComment'></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnFurtherInvestigation" name="btnFurtherInvestigation" type="submit" class="btn btn-primary" value='Save' />
				</div>
			</form>
		</div>
	</div>
</div>