<?php 
include('../includes/basic_auth.php');

$aa_var = 0;
set_time_limit(0); 

/********************************************************************************************/
if($_SESSION['userType']==$CA || $_SESSION['userType']==$CTL)
{
	$datamaster = $mysqli->prepare("SELECT callcenter_id FROM tl_and_agents where uid = ?");
	$datamaster->bind_param('i',$_SESSION['userId']);
	$datamaster->execute();
	$datamaster->bind_result($u_cid);
	$datamaster->fetch();
	$datamaster->close();
}
else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
{
	$u_cid = $_SESSION['userId'];
}
/********************************************************************************************/

if(isset($_SESSION['txtCopyName']))
{
	unset($_SESSION['txtCopyName']);
}

if(isset($_SESSION['txtCopyNumber']))
{
	unset($_SESSION['txtCopyNumber']);
}

if(!isset($_SESSION['userType']) || !isset($_SESSION['userId']))
{
	session_destroy();
	header("location:".$mysiteurl.'logout.php');
}
else
{
	$getStates = getStates();
	$getClients = getClients();
	$getCallcenters = getCallcenters('');
	
	if(isset($_POST['btnSaveAndCloseComment']))
	{
		$comment_status = 'PN';
		
		if(isset($_POST['hfLeadIdComment']) && trim($_POST['hfLeadIdComment'])!=null && trim($_POST['ddlAssignTo'])!=null)
		{
			$callcenter_id = $_POST['ddlAssignTo'];
			if(isset($_POST["ddlGetTeamLeads"]) && $_POST["ddlGetTeamLeads"]!=null && $_POST['hfLeadCommentUType']=='CTL')
			{
				$_POST['ddlAssignTo'] = $_POST["ddlGetTeamLeads"];
			}

			$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
			$loginmaster->bind_param("i",$_POST['hfLeadIdComment']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$lead_transfer_type='LT';
			$client_status=null;
			
			$_SESSION['success']='Lead has been assigned successfully.';
			
			if($_SESSION['userType']=='C')
			{
				$lead_transfer_type='RJ';
				$client_status='RJ';
				$_SESSION['success']='Lead has been rejected successfully.';
				
				$loginmaster = $mysqli->prepare("update leads set assigned_client=null where id=?");
				$loginmaster->bind_param("i",$_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
				
			}
			$rbtnValues='';
			if($_POST['hfLeadCommentUType']=='CCA' && isset($_POST['rbtnValues'][0]))
			{
				$rbtnValues = $_POST['rbtnValues'][0];
				$rbtnValues = $rbtnValues=='RRC'?'Assign to CC &amp; Request Changes':'Reject and Assign';
			}
			
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type,reason_lead_from_msw_to_cc) VALUES (?,?,?,?,?,?,?,?,?)");
			$loginmaster->bind_param("iisisssss",$_POST['hfLeadIdComment'],$_POST['ddlAssignTo'], $_POST['hfLeadCommentUType'],$_SESSION['userId'],$_POST['txtSaveComment'],$comment_status,$thisdate, $lead_transfer_type, $rbtnValues);
			$loginmaster->execute();
			$loginmaster->close();
			
			updateLastComment($_POST['hfLeadIdComment'],$_POST['txtSaveComment'],$_SESSION['userType'],$_POST['hfLeadCommentUType']);
			
			$is_sent_to_client = $_POST['hfLeadCommentUType']=='C'?1:0;
			
			if($is_sent_to_client==1)
			{
				$assigned_client = $_POST['ddlAssignTo'];
				
				$loginmaster = $mysqli->prepare("update leads set assigned_client=? where id=?");
				$loginmaster->bind_param("ii",$assigned_client, $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
				
				$html = getAssignedLeadEmail($_POST['hfLeadIdComment'],$_POST['txtSaveComment']);
				$subject = 'LegalAssist  - New Lead Assigned';
				sendClientEmails($_POST['hfLeadIdComment'], $subject, $html);
			}
			
			$aff_val = 0;
			
			if($_POST['hfLeadCommentUType']=='CCA' || $_POST['hfLeadCommentUType']=='CTL' || $_POST['hfLeadCommentUType']=='CA')
			{
				if($_POST['hfLeadCommentUType']=='CCA')
				{
					$loginmaster = $mysqli->prepare("update leads set del_cc_id = NULL, del_by_cc = 0, del_by_cc_time = NULL  where id=?");
				}
				else if($_POST['hfLeadCommentUType']=='CTL')
				{
					$loginmaster = $mysqli->prepare("update leads set del_tl_id = NULL, del_by_tl = 0, del_by_tl_time = NULL  where id=?");
				}
				else if($_POST['hfLeadCommentUType']=='CA')
				{
					$loginmaster = $mysqli->prepare("update leads set del_ag_id = NULL, del_by_ag = 0, del_by_ag_time = NULL  where id=?");
				}
				$loginmaster->bind_param("i",$_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			
			if($_POST['hfLeadCommentUType']=='CTL')
			{			
				$loginmaster = $mysqli->prepare("update leads set team_lead_assigned=? where id=?");
				$loginmaster->bind_param("ii",$_POST['ddlAssignTo'], $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			else if($_POST['hfLeadCommentUType']=='VA')
			{			
				$loginmaster = $mysqli->prepare("update leads set assigned_verifier=? where id=?");
				$loginmaster->bind_param("ii",$_POST['ddlAssignTo'], $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			else if($_POST['hfLeadCommentUType']=='CA')
			{
				$loginmaster = $mysqli->prepare("update leads set assign_agent=? where id=?");
				$loginmaster->bind_param("ii",$_POST['ddlAssignTo'], $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			
			if(($_POST['hfLeadCommentUType']=='CTL' || $_POST['hfLeadCommentUType']=='CCA') && $_SESSION['userType']=='MSW')
			{
				$loginmaster = $mysqli->prepare("update leads set shifted_by_usertype = NULL, assigned_usertype=?, is_sent_to_client=?, updation_date=?, client_status=?, callcenter_id=?, msw_to_cc_reject_reason = ? where id=?");
				$loginmaster->bind_param("sissisi",$_POST['hfLeadCommentUType'],$is_sent_to_client, $thisdate,$client_status, $callcenter_id,$rbtnValues, $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			else
			{
				$loginmaster = $mysqli->prepare("update leads set shifted_by_usertype = NULL, assigned_usertype=?, is_sent_to_client=?, updation_date=?, client_status=?, msw_to_cc_reject_reason = ? where id=?");
				$loginmaster->bind_param("sisssi",$_POST['hfLeadCommentUType'],$is_sent_to_client, $thisdate,$client_status,$rbtnValues, $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			
			if($_POST['hfLeadCommentUType']=='MSW')
			{
				$loginmaster = $mysqli->prepare("update leads set reached_admin=1 where id=?");
				$loginmaster->bind_param("i",$_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			
			if($client_status!=null && $client_status=='RJ')
			{
				outcome_function($_POST['hfLeadIdComment'], $_SESSION['userId'],'RJ', $thisdate);
			}
			
			$c_outcome=null;
			
			if($_SESSION['userType']==$MSW && $_POST['hfLeadCommentUType']==$CCA)
			{
				removeAssignedDirectlyCheck($_POST['hfLeadIdComment']);
				setMSWLeadType($_POST['hfLeadIdComment'], 'f');
				
				$loginmaster = $mysqli->prepare("select is_affliated from callcenter where uid=?");
				$loginmaster->bind_param("i",$_POST['ddlAssignTo']);
				$loginmaster->execute();
				$loginmaster->bind_result($is_affliated_check);
				$loginmaster->fetch();
				$loginmaster->close();
				
				$c_outcome = $is_affliated_check==1?'ATAC':'ATCC';
				$aff_val = $is_affliated_check;
			}
			else if($_SESSION['userType']==$MSW && $_POST['hfLeadCommentUType']==$C)
			{
				removeAssignedDirectlyCheck($_POST['hfLeadIdComment']);
				$c_outcome = 'ATC';
				setMSWLeadType($_POST['hfLeadIdComment'], 's');
			}
			else if($_SESSION['userType']==$CCA && $_POST['hfLeadCommentUType']==$MSW && $_SESSION['is_affliated']==1)
			{
				$c_outcome = 'ACTA';
				restoreDeletedLead($_POST['hfLeadIdComment'], $thisdate);
				setMSWLeadType($_POST['hfLeadIdComment'], 'f');
				
				$html = getAssignedLeadEmailAdmin($_POST['hfLeadIdComment'],$_POST['txtSaveComment']);
				$subject = 'LegalAssist  - Contact Us Query';
				sendMSWEmails($_POST['hfLeadIdComment'], $subject, $html);
			}
			else if($_SESSION['userType']==$CCA && $_POST['hfLeadCommentUType']==$CTL)
			{
				$c_outcome = 'CTTL';
			}
			else if($_SESSION['userType']==$CCA && $_POST['hfLeadCommentUType']==$MSW)
			{
				$c_outcome = 'CTA';
				restoreDeletedLead($_POST['hfLeadIdComment'], $thisdate);
				setMSWLeadType($_POST['hfLeadIdComment'], 'f');
				
				$html = getAssignedLeadEmailAdmin($_POST['hfLeadIdComment'],$_POST['txtSaveComment']);
				$subject = 'LegalAssist  - Contact Us Query';
				sendMSWEmails($_POST['hfLeadIdComment'], $subject, $html);	
			}
			else if($_SESSION['userType']==$CTL && $_POST['hfLeadCommentUType']==$CA)
			{
				$c_outcome = 'TLTA';
			}
			else if($_SESSION['userType']==$CTL && $_POST['hfLeadCommentUType']==$CCA)
			{
				$c_outcome = 'TTC';
			}
			else if($_SESSION['userType']==$CA && $_POST['hfLeadCommentUType']==$CTL)
			{
				$c_outcome = 'ATTL';
			}
			else if($_SESSION['userType']==$C && $client_status!=null && $client_status=='RJ')
			{
				$c_outcome = 'RJ';
				setMSWLeadType($_POST['hfLeadIdComment'], 'w');
			}
			else if($_SESSION['userType']==$MSW && $_POST['hfLeadCommentUType']==$VA)
			{
				$c_outcome = 'SV';
				removeAssignedDirectlyCheck($_POST['hfLeadIdComment']);
				setMSWLeadType($_POST['hfLeadIdComment'], 'v');
			}
			else if($_SESSION['userType']==$VA && $_POST['hfLeadCommentUType']==$MSW)
			{
				$c_outcome = $_POST['hfOutcome'];
				setMSWLeadType($_POST['hfLeadIdComment'], 'w');
			}
			if($c_outcome!=null)
			{
				outcome_function($_POST['hfLeadIdComment'], $_SESSION['userId'],$c_outcome, $thisdate);
			}
			
			if($_SESSION['userType']==$C || $_SESSION['userType']==$CCA)
			{
				//restoreDeletedLead($_POST['hfLeadIdComment'], $thisdate);
			}
			
			updateAssignDate($_POST['hfLeadCommentUType'], $_POST['hfLeadIdComment'],$aff_val);
			header("location:view-leads.php?pp=".$_GET['pp']);
			
		}
	}
	?>

<!DOCTYPE html>
	<html lang="en">
		<head>
			<?php include('../includes/header.php'); ?>
		</head>
		<body class="fixed-nav sticky-footer" id="page-top">
			<?php include('../includes/navigation.php'); ?>
		  
			<div class="content-wrapper">

				<div class="container-fluid">
				
					<!-- Title & Breadcrumbs-->
					<div class="row page-titles <?php echo isset($_POST["btnAdd"])?'':'collapsed' ?>"" data-toggle="collapse" data-target=".viewLeadAcc" style="margin-bottom:0;">
						<div class="col-md-12 align-self-center">
							<h4 class="theme-cl">View Shifted Leads <i class="ti-arrow-circle-down pull-right"></i></h4>
						</div>
					</div>
					<!-- Title & Breadcrumbs-->
					
					<!-- row -->
				
					<!-- row -->
					
					<div class="collapse viewLeadAcc <?php echo isset($_POST["btnAdd"])?'show':'' ?>" style="margin-bottom:0;">
						<div class="card-body">
							<form action='' name='frmSearch' id='frmSearch' method='post'>
								<?php include("../includes/messages.php") ?>
								<div class="row">
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtSearch"])) echo $_POST["txtSearch"] ?>" class="form-control" placeholder='Name / Mobile / Email' id="txtSearch" name="txtSearch" placeholder="" type="text" />
										</div>
									</div>
									
									<?php
									if($_SESSION['userType']==$MSW)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
											  	<a href="#!" id="openSelect1">Select Sol</a>
												<select name="ddlClients[]" id="ddlClients" class="d-flex form-control" multiple>
													<option value=''>Select Sol</option>
													<?php
														while($clientrow = mysqli_fetch_object($getClients))
														{
															?>	
																<option <?php if(isset($_POST['ddlClients']) && count($_POST['ddlClients'])>0){echo in_array($clientrow->uid,$_POST['ddlClients'])?'selected':'';} ?> value='<?php echo $clientrow->uid ?>'><?php echo $clientrow->company_name ?></option>
															<?php
														}
													?>
												</select>
											  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
												<select name="ddlLCallcenters" id="ddlLCallcenters" class="d-flex form-control">
													<option value=''>Select Callcenter</option>
													<?php
														while($ccrow = mysqli_fetch_object($getCallcenters))
														{
															?>	
																<option <?php if(isset($_POST['ddlLCallcenters']) && trim($_POST['ddlLCallcenters'])!=null){echo $_POST['ddlLCallcenters']==$ccrow->uid?'selected':'';} ?> value='<?php echo $ccrow->uid ?>'><?php echo $ccrow->centre_name ?></option>
															<?php
														}
													?>
												</select>
											  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
											  	<a href="#!" id="openSelect2">Select Outcome</a>
												<?php include("outcomes_select.php"); ?>
											  </div>
											</div>
										<?php
									}
									else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==1)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
													<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
													<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
											  	<a href="#!" id="openSelect1">Select Sol</a>
												<select name="ddlClients[]" id="ddlClients" class="d-flex form-control">
													<option value=''>Select Sol</option>
													<?php
														while($clientrow = mysqli_fetch_object($getClients))
														{
															?>	
																<option <?php if(isset($_POST['ddlClients']) && count($_POST['ddlClients'])>0){echo in_array($clientrow->uid,$_POST['ddlClients'])?'selected':'';} ?> value='<?php echo $clientrow->uid ?>'><?php echo $clientrow->company_name ?></option>
															<?php
														}
													?>
												</select>
											  </div>
											</div>
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else if($_SESSION['userType']==$C)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
													<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else if($_SESSION['userType']==$CTL)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
													<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else
									{
										?>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
											<input class="form-control" value="" name="ddlLeadStatus[]" id="ddlLeadStatus" type="hidden" />
										<?php
									}
									?>	
									<div class="col-md-3 col-sm-12 col-xs-12">
									  <div class="form-group">
										<!--<select name="ddlState" id="ddlState" class="d-flex form-control" onchange="getAllSuburbs(this.value)">-->
										<select name="ddlState" id="ddlState" class="d-flex form-control">
											<option value=''>Select State</option>
											<?php
												while($staterow = mysqli_fetch_object($getStates))
												{
													?>	
														<option <?php if(isset($_POST['ddlState']) && trim($_POST['ddlState'])!=null){echo $_POST['ddlState']==$staterow->id?'selected':'';} ?> value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
													<?php
												}
											?>
										</select>
									  </div>
									</div>
									
									<?php
									if(($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0) || $_SESSION['userType']==$CTL)
									{
											if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
											{
												?>
													<div class="col-md-3 col-sm-12 col-xs-12">
														<div class="form-group">
															<select name="ddlTeamLeadCheck" id="ddlTeamLeadCheck" class="d-flex form-control">
																<option value=''>Select Team Lead</option>
																<?php
																$getTeamLeads = getUsersofCallcenter($u_cid,$CTL);
																while($tlrow = mysqli_fetch_object($getTeamLeads))
																{
																	?>	
																		<option <?php if(isset($_POST['ddlTeamLeadCheck'])){echo $_POST['ddlTeamLeadCheck']==$tlrow->uid?'selected':'';} ?> value='<?php echo $tlrow->uid ?>'><?php echo $tlrow->first_name ?></option>
																	<?php
																}
																?>
															</select>
														</div>
													</div>
												<?php
											}
									}
									?>
									<!--<div class="col-md-3 col-sm-12 col-xs-12">
										<div class="form-group">
											<select name="ddlSubUrb" id="ddlSubUrb" class="d-flex form-control">
												<option value=''>Select Suburb</option>
												<?php
												/* if(isset($_POST['ddlState']) && trim($_POST['ddlState'])!=null)
												{
													$getSubUrbs = getSubUrbs(" and state_id = ".$_POST['ddlState']);
													while($srow = mysqli_fetch_object($getSubUrbs))
													{
														?>	
															<option <?php echo $_POST['ddlSubUrb']==$srow->id?'selected':''; ?> value='<?php echo $srow->id ?>'><?php echo $srow->suburb_name ?></option>
														<?php
													}
												} */
												?>
											</select>
										</div>
									</div>-->
									
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtFromDate"])) echo $_POST["txtFromDate"] ?>" class="form-control" placeholder='From' id="txtFromDate" name="txtFromDate" type="text" />
										</div>
									</div>
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtToDate"])) echo $_POST["txtToDate"] ?>" class="form-control" placeholder='To' id="txtToDate" name="txtToDate" type="text" />
											<input value="ajaxgetLeads_shift.php" class="form-control" id="hfAjaxPage" name="hfAjaxPage" placeholder="" type="hidden" />
										</div>
									</div>
									<input value="updation_date" class="form-control" name="ddlSortBy" id="ddlSortBy"  type="hidden" />
									<!--<div class="col-md-3 col-sm-12 col-xs-12">
										<div class="form-group">
											<select name="ddlSortBy" id="ddlSortBy" class="d-flex form-control">
												<option <?php echo isset($_POST['ddlSortBy']) && $_POST['ddlSortBy']=='updation_date'?'selected':''; ?> value='updation_date'>Sort By</option>
												<option <?php echo isset($_POST['ddlSortBy']) && $_POST['ddlSortBy']=='first_name'?'selected':''; ?> value='first_name'>Sort By Name</option>
												<option <?php echo isset($_POST['ddlSortBy']) && $_POST['ddlSortBy']=='phone'?'selected':''; ?> value='phone'>Sort By Phone</option>
												<option <?php echo isset($_POST['ddlSortBy']) && $_POST['ddlSortBy']=='email'?'selected':''; ?> value='email'>Sort By Email</option>
												<option <?php echo isset($_POST['ddlSortBy']) && $_POST['ddlSortBy']=='state_name'?'selected':''; ?> value='state_name'>Sort By State Name</option>
											</select>
										</div>
									</div>-->
									
									<div class="col-md-12 col-sm-12 col-xs-12">
										<input type="button" onclick="getPagination(1)" class="btn btn-primary" value="Search" name="btnAdd" id="btnAdd" />
										<input type="button" class="btn btn-warning" value="Reset" name="btnReset" id="btnReset" onclick="window.location='view-leads.php'" />
										<?php
										if($_SESSION['userType']==$MSW)
										{
											?>
												<input type="button" class="btn btn-danger" value="Delete Leads" name="btnDeleteMultiple" id="btnDeleteMultiple" onclick="deleteMultipleLeads()" disabled />
												<input value="[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]" class="col_array" type="hidden" autocomplete="0.9714395105092326">
												<input type="button" class="btn btn-success" value="Export to Excel" name="btnExportToExcel" id="btnExportToExcel">
											<?php
										}
										?>
									</div>
								</div>
							</form>
						</div>
					</div>
					
					<div class="row">
					
						
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="change-password">
							<div class="card">
							
								
								<div class="card-body">

									<!-- Table Start -->

									<div class="table-responsive viewLead ajaxClass">
										
									</div>
									<!-- Table End -->

								</div>
							</div>
						</div>
					
						</div>
					</div>
					<!-- /.row -->
				</div>

			</div>
				<!-- /.content-wrapper -->
				<?php include('saveCommentmodal.php'); ?>
				<?php include('callPopUpClientPopUp.php'); ?>
				<?php include('furtherInvestigationModal.php'); ?>
				<?php include('signupModal.php'); ?>
				<?php include('approvemodal.php'); ?>
				<?php include('invoiceModal.php'); ?>
				<?php include('invoicePaidModal.php'); ?>
				<?php include('copymodal.php'); ?>
				<?php include('copymodalShow.php'); ?>
				<?php include('rejectApprovalModal.php'); ?>
				<?php include('rejectApprovalInvoiceModal.php'); ?>
				<?php include('../includes/copyright.php'); ?>
			<!-- Scroll to Top Button-->
			<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
				<i class="ti-angle-double-up"></i>
			</a>
			<?php include('../includes/web_footer.php'); ?>
			<script src="<?php echo $mysiteurl ?>js/jquery.table2excel.js"></script>
			  <script type="text/javascript">
				$(document).ready(function() 
				{
					var delay = 1000;
						setTimeout(function() {
						 getPagination(1);
						}, delay);
				});
			</script>
		</body>
	</html>
	<?php
}
