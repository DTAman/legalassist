<?php 
include('../includes/basic_auth.php'); 

if(!isset($_SESSION['userType']) || $_SESSION['userType']!=$CCA  || $_SESSION['is_affliated']!=0)
{
	header("location:index.php");
}

//Shifting Team Leads

if(isset($_POST["btnShift"]))
{
	if(trim($_POST["ddlShiftOf"])!=null && trim($_POST["ddlShiftTo"])!=null && trim($_POST["txtComments"])!=null)
	{
		if($_POST["ddlShiftOf"]!=$_POST["ddlShiftTo"])
		{
			$u_comment_type='CTL';
			$u_comments='Shifting';
			$u_status='DN';
			$u_lead_transfer_type='LT';
			$is_shifted=1;
			$outcome='Lead Shifted';
			$outcome_type='LS'; //Lead Shifted
			
			$loginmaster = $mysqli->prepare("UPDATE lead_comments set status='DN' where lead_id in(select id from leads where team_lead_assigned = ?)");
			$loginmaster->bind_param("i",$_POST['ddlShiftOf']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("INSERT INTO shift_master(shift_data_of_user, shift_data_to_user, shift_data_usertype, comments, shifted_by, creation_date, callcenter_id) VALUES (?,?,'CTL',?,?,?,?)");
			$loginmaster->bind_param("iisisi",$_POST['ddlShiftOf'],$_POST['ddlShiftTo'],$_POST['txtComments'],$_SESSION['userId'],$thisdate,$_SESSION['userId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$shift_id = mysqli_insert_id($mysqli);
			
			//Step 1: Maintain logs for the leads going to be shifted
			$loginmaster = $mysqli->prepare("INSERT INTO shift_details(lead_id,shift_id) SELECT id,? FROM leads WHERE team_lead_assigned = ?");
			$loginmaster->bind_param("ii",$shift_id, $_POST['ddlShiftOf']);
			$loginmaster->execute();
			$loginmaster->close();
			
			//Step 2: Shift leads to new team lead		
			$loginmaster = $mysqli->prepare("update leads set last_comment = NULL,  shifted_by_usertype='S_CTL',shift_id = ?,assign_teamlead = ?, updation_date = ?, is_shifted = 1, team_lead_assigned = ?,outcome=(select outcome_title from outcomes where outcome_initials = 'SBCC'), outcome_type='SBCC' WHERE team_lead_assigned = ?");
			$loginmaster->bind_param("issii",$shift_id,$thisdate,$thisdate,$_POST['ddlShiftTo'],$_POST['ddlShiftOf']);
			$loginmaster->execute();
			$loginmaster->close();
			
			//Step 3: Create Lead Logs
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type,is_shift) select id,?,?,?,?,?,?,?,? from leads where shift_id = ?");
			$loginmaster->bind_param("isissssii",$_POST['ddlShiftTo'],$u_comment_type,$_SESSION['userId'],$u_comments,$u_status,$thisdate,$u_lead_transfer_type,$is_shifted,$shift_id);
			$loginmaster->execute();
			$loginmaster->close();
			
			$_SESSION['success']='Data shifted successfully.';
			header( "location:total_shift_data_reports.php");			
		}
		else
		{
			$_SESSION['error']="Data cannot be shifted back to the same user. Select other user to shift data.";
		}
	}
	else
	{
		$_SESSION['error']="Enter all the required fields.";
	}
}

$callcenters = getTeamLeadsCC($_SESSION['userId']);
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Shift Team Lead Data</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
							<div class="card-body">
								<form name='frmShiftData' id='frmShiftData' action='' method='post'>
									<?php include("../includes/messages.php") ?>
									 <div class="row"> 
										 <div class="col-md-6 col-sm-12 col-xs-12">
											 <div class="form-group">
												<label for="ddlShiftOf">Shift Data Of</label>
												<select name="ddlShiftOf" id="ddlShiftOf" class="d-flex form-control" required>
													<option value=''>Select Team Lead</option>
													<?php
														while($c1 = mysqli_fetch_object($callcenters))
														{
															?>	
																<option value='<?php echo $c1->uid ?>'><?php echo $c1->first_name ?></option>
															<?php
														}
													?>
												</select>
											 </div>
										</div> 
										<div class="col-md-6 col-sm-12 col-xs-12">
											 <div class="form-group">
												<label for="ddlShiftTo">Shift Data To</label>
												<select name="ddlShiftTo" id="ddlShiftTo" class="d-flex form-control" required>
													<option value=''>Select Team Lead</option>
													<?php
														mysqli_data_seek($callcenters,0);
														while($c2 = mysqli_fetch_object($callcenters))
														{
															?>	
																<option value='<?php echo $c2->uid ?>'><?php echo $c2->first_name ?></option>
															<?php
														}
													?>
												</select>
											 </div>
										</div> 
									</div> 
									
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="form-group fullwidthTextarea">
												<textarea required maxlength='300' class="form-control mt-3" rows="4" style='width:100%' placeholder="Comments" name='txtComments' id='txtComments'></textarea>
											</div>
										</div>
									 </div>
									<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<input name="btnShift" id="btnShift" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Shift Now" />
											</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			<!-- /.content-wrapper -->
				<?php include('../includes/copyright.php'); ?>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
	  
	</body>
</html>
