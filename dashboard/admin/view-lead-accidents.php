<?php 
include('../includes/basic_auth.php'); 

$getStates = getStates();
$accident_count=5;

$vehicle_flag=2;
$injury_array = array();

 
if(isset($_GET['aid']) && trim($_GET['aid'])!=null && isset($_GET['lid']) && trim($_GET['lid'])!=null)
{
	$aid = trim($_GET['aid']);
	$lid = trim($_GET['lid']);
	
	if($aid==0)
	{
		$view_accidents = getQuery('select * from lead_accidents where claim_accident=1 and status=1 and lead_id='.$lid.' order by id limit 1');
		if(mysqli_num_rows($view_accidents)==0)
		{
			header("location:index.php");
		}
		else
		{
			$rc2 = mysqli_fetch_object($view_accidents);
			$uid = $rc2->lead_id;
			$aid = $rc2->id;
			header("location:edit-lead-accidents.php?aid=".$aid."&lid=".$uid);
		}
	}
	else
	{
		$view_accidents = getQuery('select * from lead_accidents where id='.$aid);
		if(mysqli_num_rows($view_accidents)==0)
		{
			header("location:index.php");
		}
		else
		{
			$rc2 = mysqli_fetch_object($view_accidents);
			$uid = $rc2->lead_id;
			$aid = $rc2->id;
		}
	}
}
else
{
	header("location:index.php");
}

$view_vehicles = getQuery('select * from lead_vehicles where lead_accident_id='.$aid.' and lead_id='.$uid);
$view_work_history = getQuery('select * from lead_work_care_history where lead_accident_id='.$aid.' and lead_id='.$uid);
$view_injuries = getQuery('select * from lead_injury where lead_accident_id='.$aid.' and lead_id='.$uid);
$view_treatment_history = getQuery('select * from lead_treatment_history where lead_accident_id='.$aid.' and lead_id='.$uid);
$view_passengers = getQuery('select * from lead_passenger_details where lead_accident_id='.$aid.' and lead_id='.$uid);
$view_witness = getQuery('select * from lead_witness where lead_accident_id='.$aid.' and lead_id='.$uid);
$view_documents = getQuery('select * from lead_documents where lead_accident_id='.$aid.' and lead_id='.$uid);
$document_flag = mysqli_num_rows($view_documents);

$vehicles_flag = mysqli_num_rows($view_vehicles);
$accidents_flag = mysqli_num_rows($view_accidents);
$work_history_flag = mysqli_num_rows($view_work_history);
$treatment_flag = mysqli_num_rows($view_treatment_history);
$injury_flag = mysqli_num_rows($view_injuries);
$passengers_flag = mysqli_num_rows($view_passengers);
$witness_flag = mysqli_num_rows($view_witness);

if($vehicles_flag>0)
{
	$rc4 = mysqli_fetch_object($view_vehicles);
}
if($injury_flag>0)
{
	$rc5 = mysqli_fetch_object($view_injuries);
	
	$view_injuries_details = getQuery('select * from lead_injury_details where injury_master_id='.$rc5->id);
	$injury_details_flag = mysqli_num_rows($view_injuries_details);
	if($injury_details_flag>0)
	{
		while($rc5_1 = mysqli_fetch_object($view_injuries_details))
		{
			$injury_array[$rc5_1->id]['i_id']=$rc5_1->injury_id;
			$injury_array[$rc5_1->id]['b_id']=$rc5_1->body_part_id;
		}
	}
}
if($treatment_flag>0)
{
	$rc6 = mysqli_fetch_object($view_treatment_history);
}
if($work_history_flag>0)
{
	$rc7 = mysqli_fetch_object($view_work_history);
}
if($witness_flag>0)
{
	$rc9 = mysqli_fetch_object($view_witness);
}
if($document_flag>0)
{
	$rc10 = mysqli_fetch_object($view_documents);
	
	$fupDoc1=$rc10->doc1;
	$fupDoc2=$rc10->doc2;
	$fupDoc3=$rc10->doc3;
	$fupDoc4=$rc10->doc4;
}
else
{
	$fupDoc1=$fupDoc2=$fupDoc3=$fupDoc4=null;
}


?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
		<style>
			.p-4 
			{
				padding: 10px 10px 10px 20px!important;
			}
		</style>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
		
		<div class="content-wrapper">
			
			<div class="container-fluid">
				<input class="form-control" value='<?php echo $aid ?>' id="hfLeadAccidentId" name="hfLeadAccidentId" type="hidden" />
				<input class="form-control" value='<?php echo $uid ?>' id="hfLeadId" name="hfLeadId" type="hidden" />
				<input class="form-control" value='0' id="hfSomethingChanged" name="hfSomethingChanged" type="hidden" />
				<form action='' method='post' name='frm3Leads' id='frm3Leads' onsubmit='return false'>
					<input class="form-control" value='1' id="ddl2SelectCount" name="ddl2SelectCount" type="hidden" />
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class='stage3'>
								<?php
			
									$options_val='';
									$sel11=$sel12=$sel13=$sel21=$sel22=$sel31=$sel32=NULL;

									$getStates = getStates();
									while($staterow = mysqli_fetch_object($getStates))
									{
										$sele_state = $rc2->state_id==$staterow->id?'selected':'';
										$options_val.='<option '.$sele_state.' value="'.$staterow->id.'">'.$staterow->state_name.'</option>';
									}
									
									$sele_sub='';
									if(trim($rc2->state_id)!=null)
									{	
										$getSubUrbs = getSubUrbs(" and state_id = ".$rc2->state_id);
										while($srow = mysqli_fetch_object($getSubUrbs))
										{
											$sele_state = $rc2->suburb_id==$srow->id?'selected':'';
											$sele_sub.='<option '.$sele_state.' value='.$srow->id.'>'.$srow->suburb_name.'</option>';
										}
									}
								
									if($rc2->weather=="Wet")
									{
										$sel11='selected';
									}
									else if($rc2->weather=="Dry")
									{
										$sel12='selected';
									}
									else if($rc2->weather=="Snow")
									{
										$sel13='selected';
									}
									
									if(trim($rc2->police_attended)!=null)
									{
										if($rc2->police_attended==1)
										{
											$sel21='selected';
										}
										else if($rc2->police_attended==0)
										{
											$sel22='selected';
										}
									}
									
									if(trim($rc2->accident_reported)!=null)
									{
										if($rc2->accident_reported==1)
										{
											$sel31='selected';
										}
										else if($rc2->accident_reported==0)
										{
											$sel32='selected';
										}
									}
									
									$is_checked_d=$rc2->is_driver==1?'checked':'';
									$is_checked_p=$rc2->is_driver==0?'checked':'';
									$checked_name=$rc2->is_driver==1?'Driver':'Passenger';
									
									$is_attended='';
									$is_not_attended='';
									$accident_reported='';
									
									if($rc2->police_attended!=null)
									{
										$is_attended=$rc2->police_attended==1?'':'d-none';
										$is_not_attended=$rc2->police_attended==0?'':'d-none';
									}
									else
									{
										$is_attended='d-none';
										$is_not_attended='d-none';
									}
									if($rc2->accident_reported!=null)
									{
										$accident_reported=$rc2->accident_reported==1?'':'d-none';
									}
									else
									{
										$accident_reported='d-none';
									}
									
									$day_cal='';
									$mon_cal='';
									$year_cal='';
									$hours_cal='';
									$min_cal='';
									
									for($day=1;$day<=31;$day++)
									{
										$selected=date('d',strtotime($rc2->accident_date_time))==$day?'selected':'';
										$day_cal.= "<option ".$selected." value='".$day."'>".$day."</option>";
									}
									
									for($mon=1;$mon<=12;$mon++)
									{
										$selected=date('m',strtotime($rc2->accident_date_time))==$mon?'selected':'';
										$mon_cal.= "<option ".$selected." value='".$mon."'>".substr(date('F', mktime(0,0,0,$mon, 1, date('Y'))),0,3)."</option>";
									}
									
									$running_year = date('Y');
									for($year=$running_year;$year>=$running_year-100;$year--)
									{
										$selected=date('Y',strtotime($rc2->accident_date_time))==$year?'selected':'';
										$year_cal.= "<option ".$selected." value='".$year."'>".$year."</option>";
									}
									
									for($hours=0;$hours<=23;$hours++)
									{
										$selected=date('H',strtotime($rc2->accident_date_time))==$hours?'selected':'';
										$hours_cal.= "<option ".$selected." value='".$hours."'>".$hours."</option>";
									}
									
									for($min=0;$min<=59;$min++)
									{
										$selected=date('i',strtotime($rc2->accident_date_time))==$min?'selected':'';
										$min_cal.= "<option ".$selected." value='".$min."'>".$min."</option>";
									}

										?>
										<div class="row page-titles" style="margin-bottom:0;">
										   <div class="col-md-12 align-self-center">
											  <h4 class="theme-cl">Accident Details <span class="ml-2"><?php echo date('d-M-Y',strtotime($rc2->accident_date_time)) ?></span></h4>
										   </div>
										</div>
										<div class="row">
										   <div class="col-md-12 col-lg-12 col-sm-12">
											  <div class="change-password">
												 <div class="card">
													<div class="card-body">
													   
													   <?php
															
															if($rc2->accident_kind==1)
															{
																?>
																	<div class="row">
																	  <div class="col-sm-12">
																		 <div class="bg-primary-light form-group jumbotron p-4">
																			<label class="d-block" for="">Is the claimant driver or passenger?</label>
																			<div class="custom-radio d-inline-block fl-left mr-3">
																			   <input <?php echo $is_checked_d ?> type="radio" class="custom-control-input" onclick="showName(this.value,<?php echo $r ?>)" id="rbtnSelectedDriverYes<?php echo $r ?>" name="rbtnSelectedDriver<?php echo $r ?>" value="1">
																			   <label class="custom-control-label" for="rbtnSelectedDriverYes<?php echo $r ?>">Driver</label>
																			</div>
																			<div class="custom-radio d-inline-block fl-left mr-3">
																			   <input <?php echo $is_checked_p ?> type="radio" class="custom-control-input" onclick="showName(this.value,<?php echo $r ?>)" name="rbtnSelectedDriver<?php echo $r ?>" id="rbtnSelectedDriverNo<?php echo $r ?>" value="0">
																			   <label class="custom-control-label" for="rbtnSelectedDriverNo<?php echo $r ?>">Passenger</label>
																			</div>
																		 </div>
																	  </div>
																   </div>
																<?php
															}
															?>
													   
													   
													   <?php $checked_name=$rc2->accident_kind==1?$checked_name:''; ?>
													   <div class="row">
														  <?php
															if($rc2->accident_kind==1)
															{
																?>
																  <div class="col-sm-12 mb-2">
																	 <hr><h5><strong id="theName<?php echo $r ?>">Accident Details <?php echo $checked_name ?></strong></h5><hr>
																  </div>
																 <?php
															}
															?>
														  <div class="col-sm-12">
															 <div class="row">
																<input value="<?php echo $rc2->id ?>" name="txtAccidentId<?php echo $r ?>" id="txtAccidentId<?php echo $r ?>" type="hidden" />
																<div class="col-md-8 col-sm-6">
																	 <label for="">Select Accident Date And Time</label>
																	 <div class="calendertime-wrap">
																		<div>
																			<select required="required" name="txt33DOBDay[<?php echo $r ?>]" id="txt33DOBDay<?php echo $r ?>" class="form-control">
																				<option value="">Day</option><?php echo $day_cal ?>
																			</select>
																		</div>
																		<div>
																			<select required="required" name="txt33DOBMonth<?php echo $r ?>" id="txt33DOBMonth<?php echo $r ?>" class="form-control">
																				<option value="">Month</option><?php echo $mon_cal ?>
																			</select>
																		</div>
																		<div>
																			<select required="required" name="txt33DOBYear<?php echo $r ?>" id="txt33DOBYear<?php echo $r ?>" class="form-control">
																				<option value="">Year</option><?php echo $year_cal ?>
																			</select>
																		</div>
																		<div>
																			<select required="required" name="txt33DOBHours<?php echo $r ?>" id="txt33DOBHours<?php echo $r ?>" class="form-control">
																				<option value="">Hours</option><?php echo $hours_cal ?>
																			</select>
																		</div>
																		<div>
																			<select required="required" name="txt33DOBMinutes<?php echo $r ?>" id="txt33DOBMinutes<?php echo $r ?>" class="form-control">
																				<option value="">Minutes</option><?php echo $min_cal ?>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-12 col-xs-12">
																   <div class="form-group">
																	  <label for="ddl3RoadState<?php echo $r ?>">Weather Condition</label>
																	  <select name="ddl3RoadState<?php echo $r ?>" id="ddl3RoadState<?php echo $r ?>" class="d-flex form-control">
																		 <option value="">Select</option>
																		 <option <?php echo $sel11 ?> value="Wet">Wet</option>
																		 <option <?php echo $sel12 ?> value="Dry">Dry</option>
																		 <option <?php echo $sel13 ?> value="Snow">Snow</option>
																	  </select>
																   </div>
																</div>
															 </div>
															 <div class="row">
																<div class="col-md-4 col-sm-12 col-xs-12">
																   <div class="form-group">
																	  <label for="ddlState<?php echo $r ?>">Accident Location</label>
																	  <select name="ddlState<?php echo $r ?>" id="ddlState<?php echo $r ?>" class="d-flex form-control" onchange="getLeadSuburbs(this.value,<?php echo $r ?>)">
																		<option value="">Select State</option>
																		<?php echo $options_val ?>
																	</select>
																   </div>
																</div>
																<div class="col-md-4 col-sm-12 col-xs-12">
																   <div class="form-group">
																	  <label for="ddlSubUrb<?php echo $r ?>" class="invisible d-none d-md-block" >Select Sub urb</label>
																		<select name="ddlSubUrb<?php echo $r ?>" id="ddlSubUrb<?php echo $r ?>" class="d-flex form-control">
																			<option value="">Select Sub urb</option><?php echo $sele_sub ?>
																		</select>
																   </div>
																</div>
																<div class="col-md-4 col-sm-12 col-xs-12">
																   <div class="form-group">
																	  <label for="txt3PostalCode<?php echo $r ?>" class="invisible d-none d-md-block" >Postal Code</label>
																	  <input maxlength="4" value="<?php echo $rc2->postal_code ?>" class="form-control" id="txt3PostalCode<?php echo $r ?>" name="txt3PostalCode<?php echo $r ?>" placeholder="Postal Code" type="text">
																   </div>
																</div>
																<div class="col-md-8 col-sm-12 col-xs-12">
																  <div class="form-group fullwidthTextarea">
																	<textarea class="form-control mt-3" rows="4" style="width:100%" placeholder="Address First Line" name="txt3AddAddress<?php echo $r ?>" id="txt3AddAddress<?php echo $r ?>"><?php echo $rc2->address ?></textarea>
																  </div>
																</div>
																<div class="col-md-4 col-sm-12 col-xs-12">
																  <div class="form-group fullwidthTextarea">
																	<textarea class="form-control mt-3" rows="4" style="width:100%" placeholder="Accident Circumstances" name="txtAccidentCircumstanes<?php echo $r ?>" id="txtAccidentCircumstanes<?php echo $r ?>"><?php echo $rc2->accident_circumstances ?></textarea>
																  </div>
																</div>
															 </div>
															 <div class="row">
																<div class="col-sm-12 col-md-3" id="policeMainOne">
																   <div class="form-group">
																	  <label for="ddl3PoliceInformed">Police</label>
																	  <select name="ddl3PoliceInformed<?php echo $r ?>" id="ddl3PoliceInformed<?php echo $r ?>" onchange="policeOne(this.value,<?php echo $r ?>)" class="d-flex form-control">
																		 <option  value="">Select</option>
																		 <option  <?php echo $sel21 ?> value="Yes">Yes</option>
																		 <option  <?php echo $sel22 ?> value="No">No</option>
																	  </select>
																   </div>
																</div>
																<div class="col-md-9 col-sm-12 <?php echo $is_attended ?>" id="policeEventStationOne<?php echo $r ?>">
																   <div class="row">
																	  <div class="col-md-4 col-sm-12">
																		 <div class="form-group">
																			<label for="txtPoliceStation<?php echo $r ?>">Police Station</label>
																			<input class="form-control" id="txtPoliceStation<?php echo $r ?>" name="txtPoliceStation<?php echo $r ?>" placeholder="Police Station" type="text" value="<?php echo $rc2->attended_police_station ?>">
																		 </div>
																	  </div>
																	  <div class="col-md-4 col-sm-12">
																		 <div class="form-group">
																			<label for="txtPoliceNumber<?php echo $r ?>">Police Event Number</label>
																			<input class="form-control" id="txtPoliceNumber<?php echo $r ?>" name="txtPoliceNumber<?php echo $r ?>" placeholder="Police Event Number" type="text" value="<?php echo $rc2->attended_police_station ?>">
																		 </div>
																	  </div>
																	  <div class="col-md-4 col-sm-12">
																		 <div class="form-group">
																			<label for="txtPoliceInfo<?php echo $r ?>">Other Info</label>
																			<input class="form-control" id="txtPoliceInfo<?php echo $r ?>" name="txtPoliceInfo<?php echo $r ?>" placeholder="Other Info" type="text" value="<?php echo $rc2->attended_police_station ?>">
																		 </div>
																	  </div>
																   </div>
																</div>
																<div class="col-sm-12 col-md-3 <?php echo $is_not_attended ?>" id="policeNotAttend<?php echo $r ?>">
																   <div class="form-group">
																	  <label for="ddlPoliceReported<?php echo $r ?>">Accident reported if Police not attended?</label>
																	  <select name="ddlPoliceReported<?php echo $r ?>" id="ddlPoliceReported<?php echo $r ?>" onchange="policeNotAttendV(this.value,<?php echo $r ?>)" class="d-flex form-control">
																		 <option value="">Select</option>
																		 <option <?php echo $sel31 ?> value="Yes">Yes</option>
																		 <option <?php echo $sel32 ?> value="No">No</option>
																	  </select>
																   </div>
																</div>
																<div class="col-md-6 col-sm-12 <?php echo $accident_reported ?>" id="policeNotAttendYes<?php echo $r ?>">
																   <div class="row">
																	  <div class="col-md-6 col-sm-12">
																		 <div class="form-group">
																			<label for="txtOfficersName<?php echo $r ?>">Officers Name</label>
																			<input class="form-control" id="txtOfficersName<?php echo $r ?>" name="txtOfficersName<?php echo $r ?>" placeholder="Officers Name" type="text" value="<?php echo $rc2->officer_name ?>">
																		 </div>
																	  </div>
																	  <div class="col-md-6 col-sm-12">
																		 <div class="form-group">
																			<label for="txtPoliceStationReported<?php echo $r ?>">Police Station Reported</label>
																			<input class="form-control" id="txtPoliceStationReported<?php echo $r ?>" name="txtPoliceStationReported<?php echo $r ?>" placeholder="Police Station Reported" type="text" value="<?php echo $rc2->informed_police_station ?>">
																		 </div>
																	  </div>
																   </div>
																</div>
															 </div>
														  </div>
													   </div>
													   <?php
													   if($rc2->accident_kind!=1)
													   {
															?>
																<div class="row">
																	<div class="col-sm-12 col-md-12">
																		<div class="form-group">
																			<label>Was the accident recorded in the accident book?</label>
																			<input type="text" value="<?php echo $rc2->accident_recorded?>" class="form-control" placeholder="Enter Details" name="txt3AccidentRecord[<?php echo $r?>]" id="txt3AccidentRecord<?php echo $r?>" />
																			 <input value="<?php echo $rc2->id?>" name="txtAccidentId<?php echo $r?>" id="txtAccidentId<?php echo $r?>" type="hidden" />
																		</div>
																  </div>
															   </div>
															<?php
														}
														?>
													</div>
												 </div>
											  </div>
										   </div>
										</div>
										
										
										
										
										
										
							</div>
							
						</form>
			</div>
			
			<div class="container-fluid <?php echo $rc2->accident_kind==1?'d-block':'d-none'?>">
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Vehicle Details</h4>
					</div>
				</div>

				<form action='' method='post' name='frm4Leads' id='frm4Leads' onsubmit='return false'>
				
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<div class="card-body">
										<div class=''>
											<div class="row">
												<div class="col-sm-12 mb-3 mt-2">
													<h5><strong>Client Vehicle</strong></h5>
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdMake">Make</label>
														<input value="<?php echo isset($rc4->make)?$rc4->make:'' ?>" class="form-control" id="txt5ThirdMake" name="txt5ThirdMake" placeholder="Make" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdModel">Model</label>
														<input value="<?php echo isset($rc4->model)?$rc4->model:'' ?>" class="form-control" id="txt5ThirdModel" name="txt5ThirdModel" placeholder="Model" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdRegistration">Registration</label>
														<input value="<?php echo isset($rc4->registration)?$rc4->registration:'' ?>" class="form-control" id="txt5ThirdRegistration" name="txt5ThirdRegistration" placeholder="Registration" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label class="d-none d-md-block invisible" for="">.</label>
														<a href="https://www.google.com/" class="text-primary" target="_blank"><strong>google.com</strong></a>
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdInsuranceCompany">Insurance company Name</label>
														<input value="<?php echo isset($rc4->insurance_company)?$rc4->insurance_company:'' ?>" class="form-control" id="txt5ThirdInsuranceCompany" name="txt5ThirdInsuranceCompany" placeholder="Insurance company Name" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdInsuranceRef">Insurance reference number</label>
														<input value="<?php echo isset($rc4->insurance_reference_no)?$rc4->insurance_reference_no:'' ?>"  class="form-control" id="txt5ThirdInsuranceRef" name="txt5ThirdInsuranceRef" placeholder="Insurance reference number" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdInsuranceClaim">Insurance Claim number</label>
														<input value="<?php echo isset($rc4->insurance_claim_number)?$rc4->insurance_claim_number:'' ?>"  class="form-control" id="txt5ThirdInsuranceClaim" name="txt5ThirdInsuranceClaim" placeholder="Insurance Claim number" type="text">
													</div>
												</div>
												<div class="col-sm-12 col-md-9">
													<div class="form-group">
														<label for="txt5ThirdInsuranceOther">Other Info</label>
														<textarea class="form-control" name="txt5ThirdInsuranceOther" id="txt5ThirdInsuranceOther" cols="30" rows="3" placeholder="Other Info"><?php echo isset($rc4->other_info)?$rc4->other_info:'' ?></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class=''>
											<div class="row">
												<div class="col-sm-12 mb-3 mt-2">
													<h5><strong id='myvehicle'>Third Party Vehicle</strong></h5>
												</div>
											</div>
											<div class="row myvehicle">
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdDriverName">Driver Name</label>
														<input value="<?php echo isset($rc4->driver_name)?$rc4->driver_name:'' ?>" class="form-control" id="txt5DThirdDriverName" name="txt5DThirdDriverName" placeholder="Driver Name" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdAddress">Driver Address</label>
														<input value="<?php echo isset($rc4->driver_address)?$rc4->driver_address:'' ?>" class="form-control" id="txt5DThirdAddress" name="txt5DThirdAddress" placeholder="Driver Address" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdDLNumber">DL number</label>
														<input value="<?php echo isset($rc4->dl_number)?$rc4->dl_number:'' ?>" class="form-control" id="txt5DThirdDLNumber" name="txt5DThirdDLNumber" placeholder="DL number" type="text">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdMake">Make</label>
														<input value="<?php echo isset($rc4->make_d)?$rc4->make_d:'' ?>" class="form-control" id="txt5DThirdMake" name="txt5DThirdMake" placeholder="Make" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdModel">Model</label>
														<input value="<?php echo isset($rc4->model_d)?$rc4->model_d:'' ?>" class="form-control" id="txt5DThirdModel" name="txt5DThirdModel" placeholder="Model" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdRegistration">Registration</label>
														<input value="<?php echo isset($rc4->registration_d)?$rc4->registration_d:'' ?>" class="form-control" id="txt5DThirdRegistration" name="txt5DThirdRegistration" placeholder="Registration" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label class="d-none d-md-block invisible" for="">.</label>
														<a href="https://www.google.com/" class="text-primary" target="_blank"><strong>google.com</strong></a>
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdInsuranceCompany">Insurance company Name</label>
														<input value="<?php echo isset($rc4->insurance_company_d)?$rc4->insurance_company_d:'' ?>" class="form-control" id="txt5DThirdInsuranceCompany" name="txt5DThirdInsuranceCompany" placeholder="Insurance company Name" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdInsuranceRef">Insurance reference number</label>
														<input value="<?php echo isset($rc4->insurance_reference_no_d)?$rc4->insurance_reference_no_d:'' ?>"  class="form-control" id="txt5DThirdInsuranceRef" name="txt5DThirdInsuranceRef" placeholder="Insurance reference number" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdInsuranceClaim">Insurance Claim number</label>
														<input value="<?php echo isset($rc4->insurance_claim_number_d)?$rc4->insurance_claim_number_d:'' ?>"  class="form-control" id="txt5DThirdInsuranceClaim" name="txt5DThirdInsuranceClaim" placeholder="Insurance Claim number" type="text">
													</div>
												</div>
												<div class="col-sm-12 col-md-9">
													<div class="form-group">
														<label for="txt5DThirdInsuranceOther">Other Info</label>
														<textarea class="form-control" name="txt5DThirdInsuranceOther" id="txt5DThirdInsuranceOther" cols="30" rows="3" placeholder="Other Info"><?php echo isset($rc4->other_info_d)?$rc4->other_info_d:'' ?></textarea>
													</div>
												</div>
											</div>
										</div>
										
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				<!-- /.row -->
			</div>
			<div class="container-fluid">
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Injuries - Matrix Format</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
				
				<!-- row -->
				<form action='' method='post' name='frm5Leads' id='frm5Leads' onsubmit='return false'>
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								
								
								<div class="card-body">
									<form>
										<!-- tablel-start -->
										<div class="tble-rspon table-responsive mb-5">
											<table class="w-100">
												<thead>
													<tr>
														<th class="p-3"></th>
														<?php
															$getInjuries = getInjuries();
															$getBodyParts = getBodyParts();
															while($irow = mysqli_fetch_object($getInjuries))
															{
																?>
																	<th><?php echo $irow->injury ?></th>
																<?php
															}
														?>
													</tr>
												</thead>
												<tbody>
													<?php
													while($brow = mysqli_fetch_object($getBodyParts))
													{
														?>
															<tr>
																<td class="p-3"><?php echo $brow->body_part ?></td>
																<?php
																	mysqli_data_seek($getInjuries,0);
																	while($irow = mysqli_fetch_object($getInjuries))
																	{
																		$check_var='';
																		foreach($injury_array as $i_array=>$i_arr)
																		{
																			$check_var = ($i_arr['b_id']==$brow->id && $i_arr['i_id']==$irow->id)?'checked':'';
																			if(trim($check_var)!=null)
																			{
																				break;
																			}
																		}
																		?>
																			<td class="custom-checkbox m-0 text-center">
																				<input <?php echo $check_var ?> type="checkbox" class="custom-control-input customCheck chckInjury" name="chckInjury" body_part='<?php echo $brow->id ?>'injuries='<?php echo $irow->id ?>' />
																				<label class="custom-control-label" for="customCheck"></label>
																			</td>
																		<?php
																	}
																?>
															</tr>
														<?php
													}
													?>
												</tbody>
											</table>
										</div>
										<!-- tablel-end -->
										<div class="row">
											<div class="col-md-7 col-sm-12">
												<div class="form-group">
													<label for="txt6AnyOtherInjury">Any Other Injuries</label>
													<textarea class="form-control" name="txt6AnyOtherInjury" id="txt6AnyOtherInjury" cols="30" rows="3" placeholder="Write in Brief"><?php echo isset($rc5->other_injuries)?$rc5->other_injuries:''; ?></textarea>
												</div>
											</div>
											<?php
											$c1='';
											$c2='';
											$c3='';
											if(isset($rc5->still_suffering))
											{
												$c1 = $rc5->still_suffering==1?'checked':'';
												$c2 = $rc5->still_suffering==0?'checked':'';
											}
											else
											{
												$c3='checked';
											}
											?>
											<div class="col-md-3 col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="">Still Suffering</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo trim($c1)!=null?$c1:''; ?> type="radio" class="custom-control-input" id="txt6StillSufferingYes" name="txt6StillSuffering" value="1">
														<label class="custom-control-label" for="txt6StillSufferingYes">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo trim($c2)!=null?$c2:''; ?> <?php echo trim($c3)!=null?$c3:''; ?> type="radio" class="custom-control-input" id="txt6StillSufferingNo" name="txt6StillSuffering" value="0">
														<label class="custom-control-label" for="txt6StillSufferingNo">No</label>
													</div>
												</div>
											</div>
										</div>
										
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				</form>
				<!-- /.row -->
			</div>
			
			<div class="container-fluid">
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Treatment History</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<div class="card-body">
									<form action='' method='post' name='frm6Leads' id='frm6Leads' onsubmit='return false'>
										<?php 
										$painreliefs_recommended=0;
										$has_physio_chirio=0;
										$is_physio_recommended=null;
										$is_chiro_recommended=null;
										$to_hospital=0;
										$hos_detail='';
										
										if(isset($rc6->painreliefs_recommended) && trim($rc6->painreliefs_recommended)!=null)
										{
											$painreliefs_recommended = $rc6->painreliefs_recommended;
										}
										if(isset($rc6->has_physio_chirio) && trim($rc6->has_physio_chirio)!=null)
										{
											$has_physio_chirio = $rc6->has_physio_chirio;
											$is_physio_recommended = $rc6->is_physio_recommended;
											$is_chiro_recommended = $rc6->is_chiro_recommended;
										}
										if(isset($rc6->ambulance_took_to_hospital) || isset($rc6->went_hospital_with_friend))
										{
											$to_hospital = trim($rc6->ambulance_took_to_hospital)==1?1:0;
										}
										?> 
										<div class="row">
											<div class="col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block">Did An Ambulance Come To The Scene Of The Accident?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->ambulance_at_scene) && $rc6->ambulance_at_scene==1?'checked':'';?> type="radio" class="custom-control-input" id="ambulanceComeSceneY" onclick="ambulanceComeSceneV(this.value)" name="ambulanceComeScene" value="Yes">
														<label class="custom-control-label" for="ambulanceComeSceneY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->ambulance_at_scene) && $rc6->ambulance_at_scene==0?'checked':'';?> type="radio" class="custom-control-input" id="ambulanceComeSceneN" onclick="ambulanceComeSceneV(this.value)" name="ambulanceComeScene" value="No">
														<label class="custom-control-label" for="ambulanceComeSceneN">No</label>
													</div>
												</div>
											</div>
										</div>
										<!--<div class="row mt-4 d-none" id="ambulanceComeSceneYes">-->
										<div class="row  <?php echo isset($rc6->ambulance_at_scene) && trim($rc6->ambulance_at_scene)==1?'':'d-none'; ?> " id="ambulanceComeSceneYes">
											<div class="col-sm-12">
												<div class="bg-success-light form-group jumbotron p-4">
													<label class="d-block" for="">Did The Ambulance Treat You At The Scene Of The Accident?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->ambulance_treated_at_scene) && $rc6->ambulance_treated_at_scene==1?'checked':''; ?> type="radio" class="custom-control-input" id="ambulanceTreatSceneY" onclick="ambulanceTreatSceneV(this.value)" name="ambulanceTreatScene" value="Yes">
														<label class="custom-control-label" for="ambulanceTreatSceneY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->ambulance_treated_at_scene) && $rc6->ambulance_treated_at_scene==0?'checked':''; ?> type="radio" class="custom-control-input" id="ambulanceTreatSceneN" onclick="ambulanceTreatSceneV(this.value)" name="ambulanceTreatScene" value="No">
														<label class="custom-control-label" for="ambulanceTreatSceneN">No</label>
													</div>
												</div>
											</div>
										</div>
										<!--<div class="row mt-4 d-none" id="ambulanceComeSceneNo">-->
										<div class="row <?php echo isset($rc6->ambulance_at_scene) && trim($rc6->ambulance_at_scene)==0?'':'d-none'; ?>" id="ambulanceComeSceneNo">
											<div class="col-sm-12">
												<div class="bg-success-light form-group jumbotron p-4">
													<label class="d-block" for="">Did You Go To A Hospital By Yourself Or Taken There By A Family Member Or Friend?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->went_hospital_with_friend) && $rc6->went_hospital_with_friend==1?'checked':''; ?> type="radio" class="custom-control-input" id="goToHospitalY" onclick="goToHospitalV(this.value)" name="rbtnGoToHospital" value="Yes">
														<label class="custom-control-label" for="goToHospitalY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->went_hospital_with_friend) && $rc6->went_hospital_with_friend==0?'checked':''; ?> type="radio" class="custom-control-input" id="goToHospitalN" onclick="goToHospitalV(this.value)" name="rbtnGoToHospital" value="No">
														<label class="custom-control-label" for="goToHospitalN">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row <?php echo isset($rc6->ambulance_treated_at_scene) && trim($rc6->ambulance_treated_at_scene)==1?'':'d-none'; ?>" id="goToHospitalYes">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="txtWhatWasTreatment">What Was The Treatment?</label>
													<textarea class="form-control" name="txtWhatWasTreatment" id="txtWhatWasTreatment" cols="30" rows="4" placeholder="Write in Brief"><?php echo isset($rc6->ambulance_treatment)?$rc6->ambulance_treatment:'';?></textarea>
												</div>
											</div>
										</div>
										<div class="row <?php echo isset($rc6->ambulance_treated_at_scene) && trim($rc6->ambulance_treated_at_scene)==0?'':'d-none'; ?>" id="ambulanceTakeYouHospital">
											<div class="col-sm-12">
												<div class="bg-success-light form-group jumbotron p-4">
													<label class="d-block" for="exampleInputEmail1">Did The Ambulance Take You To A Hospital?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->ambulance_took_to_hospital) && trim($rc6->ambulance_took_to_hospital)==1?'checked':''; ?> type="radio" class="custom-control-input" id="ambulanceTakenHospitalY" onclick="ambulanceTakenHospitalV(this.value)" name="rbtnAmbulanceTakenHospital" value="Yes">
														<label class="custom-control-label" for="ambulanceTakenHospitalY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->ambulance_took_to_hospital) && trim($rc6->ambulance_took_to_hospital)==0?'checked':''; ?> type="radio" class="custom-control-input" id="ambulanceTakenHospitalN" onclick="ambulanceTakenHospitalV(this.value)" name="rbtnAmbulanceTakenHospital" value="No">
														<label class="custom-control-label" for="ambulanceTakenHospitalN">No</label>
													</div>
												</div>
											</div>
										</div>
										<?php
										if(isset($rc6->ambulance_hospital_detail) || isset($rc6->hospital_detail_took_by_friend))
										{
											$hos_detail = trim($rc6->ambulance_hospital_detail)!=null?$rc6->ambulance_hospital_detail:$rc6->hospital_detail_took_by_friend;
										}
										?>
										<div class="row <?php echo trim($hos_detail)!=null?'':'d-none'; ?>" id="hospitalDetail">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="txtHospitalDetails">Hospital Detail</label>
													<input value='<?php echo $hos_detail ?>' class="form-control" id="txtHospitalDetails" placeholder="Hospital Detail" type="text" />
												</div>
												<div class="form-group">
													<label for="txtWhatWasHospitalTreatment">What Was The Treatment?</label>
													<textarea class="form-control" name="txtWhatWasHospitalTreatment" id="txtWhatWasHospitalTreatment" cols="30" rows="4" placeholder="Write in Brief"><?php echo isset($rc6->hospital_treatment)?$rc6->hospital_treatment:'';?></textarea>
												</div>
											</div>
										</div>
										<!--<div class="row mt-4 d-none" id="gpAfterAccident">-->
										<div class="row <?php echo (isset($rc6->ambulance_took_to_hospital) && $rc6->ambulance_took_to_hospital==1) || (isset($rc6->went_hospital_with_friend) && $rc6->went_hospital_with_friend==0)?'':'d-none' ?>" id="gpAfterAccident">
											<div class="col-sm-12">
												<div class="bg-success-light form-group jumbotron p-4">
													<label class="d-block">Did you go to the GP after the accident?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->visited_gp) && $rc6->visited_gp==1?'checked':''; ?>  type="radio" class="custom-control-input" id="rbtnWentToGPY" onclick="showTwo(this.value)" name="rbtnVisitGP" value="Yes">
														<label class="custom-control-label" for="rbtnWentToGPY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->visited_gp) && $rc6->visited_gp==0?'checked':''; ?> type="radio" class="custom-control-input" id="rbtnWentToGPN" onclick="showTwo(this.value)" name="rbtnVisitGP" value="No">
														<label class="custom-control-label" for="rbtnWentToGPN">No</label>
													</div>
												</div>
											</div>
										
											<div class="col-sm-12 <?php echo isset($rc6->visited_gp) && $rc6->visited_gp==1?'':'d-none' ?>" id="gpNameSurgeryVisit">
												<div class="row">
													<div class="col-md-3 col-sm-12">
														<div class="form-group">
															<label for="txtGPName">GP Name</label>
															<input value='<?php echo isset($rc6->gp_name)?$rc6->gp_name:'';?>' class="form-control" name="txtGPName" id="txtGPName" placeholder="GP Name" type="text">
														</div>
													</div>
													<div class="col-md-3 col-sm-12">
														<div class="form-group">
															<label for="txtGPSurgeryName">Surgery Name</label>
															<input value='<?php echo isset($rc6->surgery_name)?$rc6->surgery_name:'';?>' class="form-control" name="txtGPSurgeryName" id="txtGPSurgeryName" placeholder="Surgery Name" type="text">
														</div>
													</div>
													<div class="col-md-3 col-sm-12">
														<div class="form-group">
															<label for="txtGPVisits">No Of Visits To The GP Since Accident</label>
															<input value='<?php echo isset($rc6->visits_to_gp)?$rc6->visits_to_gp:'';?>' class="form-control" name="txtGPVisits" id="txtGPVisits" placeholder="Visit No." type="number">
														</div>
													</div>
													<div class="col-md-3 col-sm-12">
														<div class="form-group">
															<label for="txtGPVisits">Last Visit</label>
															<input value='<?php echo isset($rc6->last_visit_to_gp) && trim($rc6->last_visit_to_gp)!=null?date('Y-m-d',strtotime($rc6->last_visit_to_gp)):'';?>' class="form-control datepicker_class" name="txtGPLastVisit" id="txtGPLastVisit" placeholder="DD/MM/YY" type="text">
														</div>
													</div>
												</div>
											</div>
										</div>
										
										<div class="row">
										<!--<div class="row d-none" id="takingAnyPainRelief">-->
											<div class="col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="">Are you taking any pain relief to manage your symptoms?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo $painreliefs_recommended==1?'checked':''; ?> type="radio" class="custom-control-input" id="takingAnyPainReliefY" name="rbtnAnyPainRelief" value="Yes">
														<label class="custom-control-label" for="takingAnyPainReliefY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo $painreliefs_recommended==0?'checked':''; ?> type="radio" class="custom-control-input" id="takingAnyPainReliefN" name="rbtnAnyPainRelief" value="No">
														<label class="custom-control-label" for="takingAnyPainReliefN">No</label>
													</div>
												</div>
											</div>
										</div>
										
										<div class="row">
										<!--<div class="row mt-4 d-none" id="physiotherapyChrioOne">-->
											<div class="col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="">Were you recommended for Physiotherapy Or Chiropractic Sessions?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo $has_physio_chirio==1?'checked':''; ?> type="radio" class="custom-control-input" id="rbtnRecommendedPhysioChrioY" onclick="selectPhysioChiro(this.value)" name="rbtnRecommendedPhysioChrio" value="Yes">
														<label class="custom-control-label" for="rbtnRecommendedPhysioChrioY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo $has_physio_chirio==0?'checked':''; ?> type="radio" class="custom-control-input" id="rbtnRecommendedPhysioChrioN" onclick="selectPhysioChiro(this.value)" name="rbtnRecommendedPhysioChrio" value="No">
														<label class="custom-control-label" for="rbtnRecommendedPhysioChrioN">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row <?php echo $has_physio_chirio==1?'':'d-none'; ?>" id="bothPhysioChiroNew">
											<div class="col-sm-12 pt-3">
												<div class="form-group">
													<div class="custom-radio mr-3">
														<input <?php echo $is_physio_recommended==1?'checked':''; ?> type="radio" class="custom-control-input" id="IsPhysioY" onclick="selectOnePhysioChiro(this.value)" name="IsPhysio" value="Yes">
														<label class="custom-control-label" for="IsPhysioY">Physiotherapy</label>
													</div>
													<div class="custom-radio mr-3">
														<input <?php echo $is_chiro_recommended==1?'checked':''; ?> type="radio" class="custom-control-input" id="IsPhysioN" onclick="selectOnePhysioChiro(this.value)" name="IsPhysio" value="No">
														<label class="custom-control-label" for="IsPhysioN">Chiropractic</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row <?php echo $is_physio_recommended==1?'':'d-none'; ?>" id="affordPhysioNew">
											<div class="col-md-3 col-sm-12">
												<div class="form-group">
													<label class="d-block" for="">Can you afford the Physiotherapy?</label>
													<div class="custom-radio mr-3">
														<input <?php echo isset($rc6->can_afford_physio) && $rc6->can_afford_physio==1?'checked':''; ?> type="radio" class="custom-control-input" id="customRadio6.4" onclick="physioYes(this.value)" name="affordPhysio" value="Yes">
														<label class="custom-control-label" for="customRadio6.4">Yes</label>
													</div>
													<div class="custom-radio mr-3">
														<input <?php echo isset($rc6->can_afford_physio) && $rc6->can_afford_physio==0?'checked':''; ?>  type="radio" class="custom-control-input" id="customRadio6.5" onclick="physioYes(this.value)" name="affordPhysio" value="No">
														<label class="custom-control-label" for="customRadio6.5">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row <?php echo $is_chiro_recommended==1?'':'d-none'; ?>" id="affordChiroNew">
											<div class="col-md-3 col-sm-12">
												<div class="form-group">
													<label class="d-block" for="">Can you afford the Chiropractic?</label>
													<div class="custom-radio mr-3">
														<input <?php echo isset($rc6->can_afford_chiropractic) && $rc6->can_afford_chiropractic==1?'checked':''; ?> type="radio" class="custom-control-input" id="customRadio6.6" onclick="chiroYes(this.value)" name="affordChiro" value="Yes">
														<label class="custom-control-label" for="customRadio6.6">Yes</label>
													</div>
													<div class="custom-radio mr-3">
														<input <?php echo isset($rc6->can_afford_chiropractic) && $rc6->can_afford_chiropractic==0?'checked':''; ?> type="radio" class="custom-control-input" id="customRadio6.7" onclick="chiroYes(this.value)" name="affordChiro" value="No">
														<label class="custom-control-label" for="customRadio6.7">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row <?php echo isset($rc6->can_afford_physio) && $rc6->can_afford_physio==1?'':'d-none';?>" id="eachVisitPhysio">
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="txtPhysioVisits">No. of Visits For Physiotherapy</label>
													<input value="<?php echo isset($rc6->visits_for_physio)?$rc6->visits_for_physio:'' ?>" class="form-control" name="txtPhysioVisits" id="txtPhysioVisits" placeholder="Physiotherapy Visit No." type="number">
												</div>
											</div>
										</div>
										<div class="row <?php echo isset($rc6->can_afford_chiropractic) && $rc6->can_afford_chiropractic==1?'':'d-none';?>"  id="eachVisitChiro">
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="txtChirioVisits">No. of Visits For Chiropractic</label>
													<input value="<?php echo isset($rc6->visits_for_chiro)?$rc6->visits_for_chiro:'' ?>"  class="form-control" name="txtChirioVisits" id="txtChirioVisits" placeholder="Chiropractic Visit No." type="number">
												</div>
											</div>
										</div>
										<!--<div class="row mt-4 d-none" id="xrayMriOne">-->
										<div class="row">
											<div class="col-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="">Did You Go For Any Xray, MRI Or CT-Scan?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->have_x_ray) && $rc6->have_x_ray==1?'checked':'' ?> type="radio" class="custom-control-input" id="customRadio7" onclick="showFive(this.value)" name="rbtnXrayScan" value="Yes">
														<label class="custom-control-label" for="customRadio7">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo isset($rc6->have_x_ray) && $rc6->have_x_ray==0?'checked':'' ?> type="radio" class="custom-control-input" id="customRadio7.1" onclick="showFive(this.value)" name="rbtnXrayScan" value="No">
														<label class="custom-control-label" for="customRadio7.1">No</label>
													</div>
												</div>
											</div>
											<div class="col-12 <?php echo isset($rc6->have_x_ray) && $rc6->have_x_ray==1?'':'d-none' ?>" id="resultScanOne">
												<div class="form-group">
													<label for="txtScanResult">What Was The Result Of The Scan?</label>
													<textarea class="form-control" name="txtScanResult" id="txtScanResult" cols="30" rows="4" placeholder="Write in Brief"><?php echo isset($rc6->x_ray_result)?$rc6->x_ray_result:'' ?></textarea>
												</div>
											</div>
											<div class="col-12 <?php echo isset($rc6->have_x_ray) && $rc6->have_x_ray==0?'':'d-none' ?>" id="resultScanNo">
												<div class="form-group">
													<label for="txtNoScan">Why did you not go for Xray, MRI and CT Scan?</label>
													<textarea class="form-control" name="txtNoScan" id="txtNoScan" cols="30" rows="4" placeholder="Write in Brief"><?php echo isset($rc6->why_not_x_ray)?$rc6->why_not_x_ray:'' ?></textarea>
												</div>
											</div>
										</div>
										<!--<div class="row d-none mt-4" id="preExcistingInjury">-->
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="txtPreExistingInjuries">Pre-existing injuries</label>
													<textarea class="form-control" name="txtPreExistingInjuries" id="txtPreExistingInjuries" cols="30" rows="6" placeholder="List all previous injuries (even if they do not relate to the same body part)"><?php echo isset($rc6->pre_existing_injuries)?$rc6->pre_existing_injuries:'' ?></textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
											<!--<div class="col-sm-12 d-none mt-4" id="sufferPsychological">-->
												<div class="row">
													<div class="col-sm-12">
														<h6 class="p-3 m-0 font-midium bg-secondary">Is the client suffering for psychological injuries? Please tick which ever applicable - multiple ticks allowed:</h6>
													</div>
													<div class="col-sm-12 col-md-3">
														<div class="form-group">
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input <?php echo isset($rc6->dizziness) && $rc6->dizziness==1?'checked':'' ?> type="checkbox" class="custom-control-input form-control injuryList" id="Dizziness" name="injuryList[]" name_attr="dizziness">
																<label class="custom-control-label" for="Dizziness">Dizziness</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input <?php echo isset($rc6->nausea) && $rc6->nausea==1?'checked':'' ?> type="checkbox" class="custom-control-input form-control injuryList" id="Nausea" name="injuryList[]" name_attr="nausea">
																<label class="custom-control-label" for="Nausea">Nausea</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input <?php echo isset($rc6->post_traumatic_stress_disorder) && $rc6->post_traumatic_stress_disorder==1?'checked':'' ?> type="checkbox" class="custom-control-input form-control injuryList" id="Post" name="injuryList[]"  name_attr="post_traumatic_stress_disorder">
																<label class="custom-control-label" for="Post">Post Traumatic Stress Disorder</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input <?php echo isset($rc6->insomnia) && $rc6->insomnia==1?'checked':'' ?> type="checkbox" class="custom-control-input form-control injuryList" id="Insomnia" name="injuryList[]"  name_attr="insomnia">
																<label class="custom-control-label" for="Insomnia">Insomnia</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input <?php echo isset($rc6->vertigo) && $rc6->vertigo==1?'checked':'' ?> type="checkbox" class="custom-control-input form-control injuryList" id="Vertigo" name="injuryList[]"  name_attr="vertigo">
																<label class="custom-control-label" for="Vertigo">Vertigo</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input <?php echo isset($rc6->driving_difficulty_post_accident) && $rc6->driving_difficulty_post_accident==1?'checked':'' ?> type="checkbox" class="custom-control-input form-control injuryList" id="Driving" name="injuryList[]"  name_attr="driving_difficulty_post_accident">
																<label class="custom-control-label" for="Driving">Driving difficulty post accident</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input <?php echo isset($rc6->fear_of_being_hit) && $rc6->fear_of_being_hit==1?'checked':'' ?> type="checkbox" class="custom-control-input form-control injuryList" id="Fear" name="injuryList[]"  name_attr="fear_of_being_hit">
																<label class="custom-control-label" for="Fear">Fear of being hit every time client drives</label>
															</div>
														</div>
													</div>
													<div class="col-sm-12 col-md-9">
														<div class="form-group p-lg-5 p-0">
															<label for="txtAnyOther">Any Other</label>
															<textarea class="form-control" name="txtAnyOther" id="txtAnyOther" cols="30" rows="8"><?php echo isset($rc6->any_other_psychological_injury)?$rc6->any_other_psychological_injury:'' ?></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
										
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- /.row -->
			</div>
			<div class="container-fluid">
				
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Work & Care Details</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
				
				<!-- row -->
				<div class="row">
					
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<div class="card-body">
									<form action='' method='post' name='frm7Leads' id='frm7Leads' onsubmit='return false'>
										<div class="row">
											<div class="col-sm-12 mb-2 d-block" id="passengerName">
												<hr><h5><strong>Work</strong></h5><hr>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label for="ddl7Employed">Are you employed?</label>
													<select name="ddl7Employed" id="ddl7Employed" class="d-flex form-control" onchange="meansOfIncomeV(this.value)">
														<option value='2'>Select</option>
														<option <?php echo isset($rc7->is_employed) && $rc7->is_employed==1?'selected':'' ?> value="1">Yes</option>
														<option <?php echo isset($rc7->is_employed) && $rc7->is_employed==0?'selected':'' ?> value="0">No</option>
													</select>
												</div>
											</div>
											<div class="col-md-4 col-sm-12 <?php echo isset($rc7->is_employed) && $rc7->is_employed==0?'':'d-none' ?>" id="meansOfIncome">
												<div class="form-group">
													<label for="ddl7IncomeState">State Means of Income?</label>
													<select name="ddl7IncomeState" id="ddl7IncomeState" class="form-control" onchange="meansOfIncomeOtherV(this.value)">
														<option value=''>Select</option>
														<option <?php echo isset($rc7->income_from) && $rc7->income_from=='Disability Pension'?'selected':'' ?> value="Disability Pension">Disability Pension</option>
														<option <?php echo isset($rc7->income_from) && $rc7->income_from=='Centerlink'?'selected':'' ?> value="Centerlink">Centerlink</option>
														<option <?php echo isset($rc7->income_from) && $rc7->income_from=='State Pension'?'selected':'' ?> value="State Pension">State Pension</option>
														<option <?php echo isset($rc7->income_from) && $rc7->income_from=='Other'?'selected':'' ?> value="Other">Other</option>
													</select>
												</div>
											</div>
											<div class="col-md-4 col-sm-12 <?php echo isset($rc7->income_from) && trim(strtolower($rc7->income_from))=='other'?'':'d-none' ?>" id="stateMeansIncomeOther">
												<div class="form-group">
													<label for="txt7OtherIncomes">Other Means of Income</label>
													<input value="<?php echo isset($rc7->other_income_means)?$rc7->other_income_means:'' ?>" class="form-control" id="txt7OtherIncomes" name='txt7OtherIncomes' placeholder="Other" type="text">
												</div>
											</div>
											<div class="col-md-4 col-sm-12 <?php echo isset($rc7->is_employed) && $rc7->is_employed==1?'':'d-none' ?>" id="yourOccupation">
												<div class="form-group">
													<label for="txt7Occupation">What is your occupation</label>
													<input value="<?php echo isset($rc7->occupation)?$rc7->occupation:'' ?>" class="form-control" id="txt7Occupation" name="txt7Occupation" placeholder="Occupation" type="text">
												</div>
											</div>
											<div class="col-sm-12 <?php echo isset($rc7->is_employed) && $rc7->is_employed==1?'':'d-none' ?>" id="weeklyGrossIncome">
												<div class="row">
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label for="txt7WeeklySalary">Weekly salary (Gross/net) AUD</label>
															<input value="<?php echo isset($rc7->weekly_salary)?$rc7->weekly_salary:'' ?>" class="form-control" id="txt7WeeklySalary" name="txt7WeeklySalary" placeholder="(Gross/net) Salary" type="text">
														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label for="txt7WorkingHours">How many hours per week do you work?</label>
															<input value="<?php echo isset($rc7->working_hours)?$rc7->working_hours:'' ?>" class="form-control" id="txt7WorkingHours" name="txt7WorkingHours" placeholder="Hours" type="text">
														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label for="txt7WorkingIncomeLost">How much money have you lost as a result of this accident?</label>
															<input  value="<?php echo isset($rc7->income_loss_by_accident)?$rc7->income_loss_by_accident:'' ?>" class="form-control" id="txt7WorkingIncomeLost" name="txt7WorkingIncomeLost" placeholder="Lost Income" type="text">
														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label for="ddl7HaveInjuries">Are your injuries affecting your work?</label>
															<select name="ddl7HaveInjuries" id="ddl7HaveInjuries" class="d-flex form-control">
																<option value=''>Select</option>
																<option <?php echo isset($rc7->injury_affecting_work) && $rc7->injury_affecting_work==1?'selected':'' ?> value="1">Yes</option>
																<option <?php echo isset($rc7->injury_affecting_work) && $rc7->injury_affecting_work==0?'selected':'' ?> value="0">No</option>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 mb-2 d-block" id="passengerName">
												<hr><h5><strong>Care</strong></h5><hr>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6 col-sm-12">
												<div class="form-group">
													<label for="txt7ReceivingAssistance">Are you receiving any assistance from anyone as a result of your injuries?</label>
													<label class="d-flex">
													<input value="<?php echo isset($rc7->receiving_assistance)?$rc7->receiving_assistance:'' ?>" class="form-control" id="txt7ReceivingAssistance" name="txt7ReceivingAssistance" placeholder="Receiving Any Assistance" type="text">
												</div>
											</div>
											<div class="col-md-6 col-sm-12">
												<div class="form-group">
													<label for="txt7RequireReceivingAssistance">Did you require this assistance before this injury?</label>
													<label class="d-flex">
													<input value="<?php echo isset($rc7->receiving_assistance)?$rc7->receiving_assistance:'' ?>" class="form-control" id="txt7RequireReceivingAssistance" name="txt7RequireReceivingAssistance" placeholder="Require Assistance" type="text">
												</div>
											</div>
										</div>
										
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- /.row -->
			</div>
			
			<div class="container-fluid <?php echo $rc2->accident_kind==1?'d-block':'d-none'?>">
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Passenger Details</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<div class="card-body">
									<form action='' method='post' name='frm8Leads' id='frm8Leads' onsubmit='return false'>
										<div class="row">
											<div class="col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="exampleInputEmail1">Were There Any Other Persons In The Vehicle?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo $passengers_flag!=0?'checked':'' ?> type="radio" class="custom-control-input" id="customRadio8" onclick="showSix(this.value)" name="any-passengers" value="Yes">
														<label class="custom-control-label" for="customRadio8">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input <?php echo $passengers_flag==0?'checked':'' ?> type="radio" class="custom-control-input" id="customRadio8.1" onclick="showSix(this.value)" name="any-passengers" value="No">
														<label class="custom-control-label" for="customRadio8.1">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row mb-3 <?php echo $passengers_flag!=0?'':'d-none' ?>" id='txtPassengerDiv'>
											<div class="col-sm-12 col-md-3">
												<div class="form-group">
													<label for="exampleInputEmail1" id="the_passenger_label">How many total people were there in the vehicle?</label>
													<input maxlength='1' value="<?php echo $passengers_flag ?>" class="form-control" count_attr="<?php echo $passengers_flag ?>" id="txt8PassengersCount" name="txt8PassengersCount" placeholder="How Many?" onchange="addPassengerDetails(this.value)"  type="text">
													<input value="0" class="form-control" id="hfSomethingDoneInPassenger" name="hfSomethingDoneInPassenger" type="hidden" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12" id="fullInjuryAndTreatment">
												<div class="row" id="passengerDetailOne">
														<?php
														$p=1;
														if($passengers_flag>0)
														{
															while($rc8 = mysqli_fetch_object($view_passengers))
															{
																?>
																	<div class="box-passenger-wrap">
																		<div class="col-md-12 col-sm-12"> 
																			<div class="form-group">
																				<label for="txt8Passenger[<?php echo $p ?>]">Passenger <span><?php echo $p ?></span> Detail</label>
																				<textarea class="form-control text_class_change" the_id = <?php echo $rc8->id ?> name="txt8Passenger[<?php echo $p ?>]" id="txt8Passenger<?php echo $p ?>" cols="30" rows="6" placeholder="Write in Brief"><?php echo $rc8->passenger_details ?></textarea> 
																			</div>
																		</div>
																	</div>
																<?php
																$p++;
															}
														}
														?>
												</div>
											</div>
										</div>
										
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- /.row -->
			</div>
			<div class="container-fluid">
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Witness Details</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
				
				<!-- row -->
				<div class="row">
					
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								
								
								<div class="card-body">
									<form action='' method='post' name='frm9Leads' id='frm9Leads' onsubmit='return false'>
										<div class="row">
											<div class="col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="">Do you have any witness?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" <?php echo $witness_flag!=0?'checked':'' ?> class="custom-control-input" id="witnessOne1" onclick="witnessOne(this.value)" name="witnessO" value="1">
														<label class="custom-control-label" for="witnessOne1">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" <?php echo $witness_flag==0?'checked':'' ?> class="custom-control-input" id="witnessOne2" onclick="witnessOne(this.value)" name="witnessO" value="0">
														<label class="custom-control-label" for="witnessOne2">No</label>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-sm-12 <?php echo $witness_flag==0?'d-none':'' ?>" id="witnessFields">
												<div class="row">
													<div class="col-md-6 col-sm-12">
														<div class="form-group">
															<label for="txt9WitnessName">Name</label>
															<input value='<?php echo isset($rc9->name)?$rc9->name:'' ?>' class="form-control" id="txt9WitnessName" name="txt9WitnessName" placeholder="Name" type="text">
														</div>
													</div>
													<div class="col-md-6 col-sm-12 col-xs-12">
														<div class="form-group">
															<label for="txt9WitnessPhone">Phone Number</label>
															<input value='<?php echo isset($rc9->phone)?$rc9->phone:'' ?>' class="form-control" id="txt9WitnessPhone" name="txt9WitnessPhone" placeholder="Phone Number" type="number">
														</div>
													</div>
													<div class="col-md-4 col-sm-12 col-xs-12">
														<div class="form-group">
															<label for="">Address</label>
															<select name="ddl9State" id="ddl9State" class="d-flex form-control" onchange="getAllSuburbsWitness(this.value)">
																<option value=''>Select State</option>
																<?php
																	mysqli_data_seek($getStates,0);
																	while($staterow = mysqli_fetch_object($getStates))
																	{
																		?>	
																			<option <?php echo isset($rc9->state_id) && $rc9->state_id==$staterow->id ?'selected':'' ?> value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
																		<?php
																	}
																?>
															</select>
														</div>
													</div>
													<div class="col-md-4 col-sm-12 col-xs-12">
														<div class="form-group">
															<label for="txt9WitnessPostalCode" class="invisible d-none d-md-block" >Postal Code</label>
															<input value='<?php echo isset($rc9->postal_code)?$rc9->postal_code:'' ?>' class="form-control" id="txt9WitnessPostalCode" name="txt9WitnessPostalCode" placeholder="Postal Code" type="text">
														</div>
													</div>
													<div class="col-md-4 col-sm-12 col-xs-12">
														<div class="form-group">
															<label for="ddl9SubUrb" class="invisible d-none d-md-block" >Select Sub urb</label>
															<select name="ddl9SubUrb" id="ddl9SubUrb" class="d-flex form-control">
																<option value='' <?php echo isset($rc9->suburb_id) && $rc9->suburb_id ==null?'selected':''; ?>>Select SubUrb</option>
																<?php
																	if(isset($rc9->state_id))
																	{
																		$getSubUrbs = getSubUrbs(" and state_id = ".$rc9->state_id);
																		while($srow = mysqli_fetch_object($getSubUrbs))
																		{
																			?>	
																				<option <?php echo isset($rc9->suburb_id) && $rc9->suburb_id==$srow->id ?'selected':'' ?> value='<?php echo $srow->id ?>' <?php echo $rc9->suburb_id==$srow->id?'selected':''; ?>><?php echo $srow->suburb_name ?></option>
																			<?php
																		}
																	}
																	?>
															</select>
														</div>
													</div>
													<div class="col-md-12 col-sm-12 col-xs-12">
													  <div class="form-group fullwidthTextarea">
														<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Address First Line" name='txt9Address' id='txt9Address'><?php echo isset($rc9->address)?$rc9->address:'' ?></textarea>
													  </div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<input id="" onclick="window.location.href='view-each-lead.php?u=<?php echo $lid?>'" type="button" class="btn btn-primary ml-0 mr-3 mt-2" value='Back' />
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- /.row -->
			</div>
			
			
			<div class="container-fluid">
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Upload Documents</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<div class="card-body">
									<form action='' method='post' name='frm10Leads' id='frm10Leads' enctype="multipart/form-data">
										
										<div class="col-12 d-flex flex-wrap bg-secondary">
										<?php
										if($document_flag!=0)
										{
											if(trim($rc10->doc1)!=null)
											{
												?>
													<div class="d-flex flex-column form-group widget bg-transparent pr-4">
														<div class="">
															<label class="d-flex">
																<a href="<?php echo $mysiteurl.has_file_on_server($rc10->doc1) ?>" class="bg-primary-light p-2 w-100" download ><i class="ti-pin-alt"></i><img src="<?php echo $mysiteurl.$rc10->doc1 ?>" class="d-none" alt="">
																	<span class="pl-1">Download</span>
																</a>
															</label>
														</div>
													</div>
												<?php
											}
											if(trim($rc10->doc2)!=null)
											{
												?>
													<div class="d-flex flex-column form-group widget bg-transparent pr-4">
														<div class="">
															<label class="d-flex">
																<a href="<?php echo $mysiteurl.has_file_on_server($rc10->doc2) ?>" class="bg-primary-light p-2 w-100" download ><i class="ti-pin-alt"></i><img src="<?php echo $mysiteurl.$rc10->doc2 ?>" class="d-none" alt="">
																	<span class="pl-1">Download</span>
																</a>
															</label>
														</div>
													</div>
												<?php
											}
											if(trim($rc10->doc3)!=null)
											{
												?>
													<div class="d-flex flex-column form-group widget bg-transparent pr-4">
														<div class="">
															<label class="d-flex">
																<a href="<?php echo $mysiteurl.has_file_on_server($rc10->doc3) ?>" class="bg-primary-light p-2 w-100" download ><i class="ti-pin-alt"></i><img src="<?php echo $mysiteurl.$rc10->doc3 ?>" class="d-none" alt="">
																	<span class="pl-1">Download</span>
																</a>
															</label>
														</div>
													</div>
												<?php
											}
											if(trim($rc10->doc4)!=null)
											{
												?>
													<div class="d-flex flex-column form-group widget bg-transparent pr-4">
														<div class="">
															<label class="d-flex">
																<a href="<?php echo $mysiteurl.has_file_on_server($rc10->doc4) ?>" class="bg-primary-light p-2 w-100" download ><i class="ti-pin-alt"></i><img src="<?php echo $mysiteurl.$rc10->doc4 ?>" class="d-none" alt="">
																	<span class="pl-1">Download</span>
																</a>
															</label>
														</div>
													</div>
												<?php
											}
										}
										?>
										</div>
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- /.row -->
			</div>
			
			
			
			
		</div>
		<!-- /.content-wrapper -->
		
		<?php include('../includes/copyright.php'); ?>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
		<script type="text/javascript" src="<?php echo $mysiteurl ?>js/main.js"></script>
		<script type='text/javascript'>
		$(document).ready(function() 
		{
			$("input[type='text'], select, textarea").prop("disabled", true);
		
		});
		</script>
	</body>
</html>