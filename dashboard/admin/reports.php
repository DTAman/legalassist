<?php 
include('../includes/basic_auth.php');

$fur_inv='';
$condition=" ";
$usertype = $_SESSION["userType"];

$getStates = getStates();
$getClients = getClients();
$getCallcenters = getCallcenters('');
$getVerifiers = getVerifiers('');

$getOutcomes = getOutcomes($_SESSION['userId']);

if($usertype!=$CCA && $usertype!=$C && $usertype!=$MSW && $usertype!=$SA)
{
	header("location:index.php");
	exit;
}

if($_SESSION['userType']!=$CCA)
{
	$getAgents = getAgents();
	$getTeamLeads = getTeamLeads();
}
else
{
	$getAgents = getAgentsCC($_SESSION['userId']);
	$getTeamLeads = getTeamLeadsCC($_SESSION['userId']);
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
		<style>
			.table-responsive.viewLead table {
				width: 100%;
			}
			.a_red
			{
				color:red;
			}
			.table-responsive.viewLead table tbody td button.ti-clipboard {
				font-size: 17px;
				position: inherit;
				top: 0;
				transform: translateY(22%);
				right: 0;
				color: #F44336;
				cursor: pointer;
				background: transparent;
				box-shadow: none;
				border: 0;
				float: right;
			}
		</style>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
	  
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles <?php echo isset($_POST["btnAdd"])?'':'collapsed' ?>" data-toggle="collapse" data-target=".viewLeadAcc" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">View Reports <i class="ti-arrow-circle-down pull-right"></i></h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
			
				<!-- row -->
				
				<div class="collapse viewLeadAcc <?php echo isset($_POST["btnAdd"])?'show':'' ?>"  style="margin-bottom:0;">
					<div class="card-body">
						<form target="_blank" action='reportsdownload.php' name='frmSearch' id='frmSearch' method='post'>
							<?php include("../includes/messages.php") ?>
							<div class="row">
								<div class="col-md-3 col-sm-8 col-xs-12">
									<div class="form-group">
										<input value="<?php if(isset($_GET['pp'])) echo $_GET['pp'] ?>" class="form-control" id="txtPP" name="txtPP" type="hidden" />
										<input value="<?php if(isset($_POST["txtSearch"])) echo $_POST["txtSearch"] ?>" class="form-control" placeholder='Name / Mobile / Email' id="txtSearch" name="txtSearch" placeholder="" type="text" />
									</div>
								</div>
								<?php
								if($_SESSION['userType']==$MSW || $_SESSION['userType']==$CCA)
								{
									?>
										<input value="[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14]" class="col_array" type="hidden"  />
									<?php
								}
								else if($_SESSION['userType']==$C)
								{
									?>
										<input value="[0,1,2,3,4,5,6,7,8,9,10]" class="col_array" type="hidden"  />
									<?php
								}
								else
								{
									?>
										<input value="[0]" class="col_array" type="hidden"  />
									<?php
								}
								?>
								
								
										<?php
										if($_SESSION['userType']!=$C && $_SESSION["is_affliated"]==0)
										{
											?>
												<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
													<select name="ddlAgents" id="ddlAgents" class="d-flex form-control">
														<option value=''>Select Agent</option>
														<?php
															while($agentrow = mysqli_fetch_object($getAgents))
															{
																?>	
																	<option <?php if(isset($_POST['ddlAgents']) && trim($_POST['ddlAgents'])!=null){echo $_POST['ddlAgents']==$agentrow->uid?'selected':'';} ?> value='<?php echo $agentrow->uid ?>'><?php echo $agentrow->first_name." ".$agentrow->last_name ?></option>
																<?php
															}
														?>
													</select>
												  </div>
												</div>
												<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
													<select name="ddlTeamLeads" id="ddlTeamLeads" class="d-flex form-control">
														<option value=''>Select Team Lead</option>
														<?php
															while($teamrow = mysqli_fetch_object($getTeamLeads))
															{
																?>	
																	<option <?php if(isset($_POST['ddlTeamLeads']) && trim($_POST['ddlTeamLeads'])!=null){echo $_POST['ddlTeamLeads']==$teamrow->uid?'selected':'';} ?> value='<?php echo $teamrow->uid ?>'><?php echo $teamrow->first_name." ".$teamrow->last_name ?></option>
																<?php
															}
														?>
													</select>
												  </div>
												</div>
												
											<?php
										}
										
										if($_SESSION['userType']!=$C)
										{
											if($_SESSION['userType']!=$CCA)
											{
												?>
													<div class="col-md-3 col-sm-12 col-xs-12">
													  <div class="form-group">
														<select name="ddlLCallcenters" id="ddlLCallcenters" class="d-flex form-control">
															<option value=''>Select Callcenter</option>
															<?php
																while($ccrow = mysqli_fetch_object($getCallcenters))
																{
																	?>	
																		<option <?php if(isset($_POST['ddlLCallcenters']) && trim($_POST['ddlLCallcenters'])!=null){echo $_POST['ddlLCallcenters']==$ccrow->uid?'selected':'';} ?> value='<?php echo $ccrow->uid ?>'><?php echo $ccrow->	centre_name ?></option>
																	<?php
																}
															?>
														</select>
													  </div>
													</div>
											<?php
											}
											else
											{
												?>
													<input value="<?php echo $_SESSION['userId'] ?>" class="form-control" placeholder="From" id="ddlLCallcenters" name="ddlLCallcenters" type="hidden" />
												<?php
											}
										}
										
										if($_SESSION['userType']==$MSW || $_SESSION['userType']==$SA)
										{
											if($_SESSION['userType']!=$C)
											{
												?>
												<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  <a href="#!" id="openSelect1">Select Sol</a>
													<select name="ddlClients[]" id="ddlClients" class="d-flex form-control" multiple>
														<option value=''>Select Sol</option>
														<?php
															while($clientrow = mysqli_fetch_object($getClients))
															{
																?>	
																	<option <?php if(isset($_POST['ddlClients']) && count($_POST['ddlClients'])>0){echo in_array($clientrow->uid,$_POST['ddlClients'])?'selected':'';} ?> value='<?php echo $clientrow->uid ?>'><?php echo $clientrow->company_name ?></option>
																<?php
															}
														?>
													</select>
												  </div>
												</div>
											<?php
											}
											else
											{
												?>
													<input value="<?php echo $_SESSION['userId'] ?>" class="form-control" placeholder="From" id="ddlClients" name="ddlClients[]" type="hidden" />
												<?php
											}
										}
									
										if($_SESSION['userType']==$MSW || $_SESSION['userType']==$SA)
										{
											?>
									
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
												<a href="#!" id="openSelect2">Select Outcome</a>
												<?php include("outcomes_select.php"); ?>
											  </div>
											</div>
											<?php
										}
									?>
									
								<div class="col-md-3 col-sm-12 col-xs-12">
								  <div class="form-group">
									<select name="ddlState" id="ddlState" class="d-flex form-control" onchange="getLeadSuburbsAll(this.value)">
										<option value=''>Select State</option>
										<?php
											while($staterow = mysqli_fetch_object($getStates))
											{
												?>	
													<option <?php if(isset($_POST['ddlState']) && trim($_POST['ddlState'])!=null){echo $_POST['ddlState']==$staterow->id?'selected':'';} ?> value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
												<?php
											}
										?>
									</select>
								  </div>
								</div>
								<div class="col-md-3 col-sm-12 col-xs-12">
									<div class="form-group">
										<select name="ddlSubUrb" id="ddlSubUrb" class="d-flex form-control">
											<option value=''>Select Suburb</option>
											<?php
											if(isset($_POST['ddlState']) && trim($_POST['ddlState'])!=null)
											{
												$getSubUrbs = getSubUrbs(" and state_id = ".$_POST['ddlState']);
												while($srow = mysqli_fetch_object($getSubUrbs))
												{
													?>	
														<option <?php echo $_POST['ddlSubUrb']==$srow->id?'selected':''; ?> value='<?php echo $srow->id ?>'><?php echo $srow->suburb_name ?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>
								
								<div class="col-md-3 col-sm-8 col-xs-12">
									<div class="form-group">
										<input value="<?php if(isset($_POST["txtFromDate"])) echo $_POST["txtFromDate"] ?>" class="form-control" placeholder='From' id="txtFromDate" name="txtFromDate" placeholder="" type="text" />
									</div>
								</div>
								<div class="col-md-3 col-sm-8 col-xs-12">
									<div class="form-group">
										<input value="<?php if(isset($_POST["txtToDate"])) echo $_POST["txtToDate"] ?>" class="form-control" placeholder='To' id="txtToDate" name="txtToDate" placeholder="" type="text" />
										<input value="ajaxgetReportsLeads.php" class="form-control" id="hfAjaxPage" name="hfAjaxPage" placeholder="" type="hidden" />
									</div>
								</div>
								<div class="col-md-3 col-sm-12 col-xs-12">
									<div class="form-group">
										<select name="ddlSortBy" id="ddlSortBy" class="d-flex form-control">
											<option <?php echo isset($_POST['ddlSortBy']) && $_POST['ddlSortBy']=='updation_date'?'selected':''; ?> value='updation_date'>Sort By</option>
											<option <?php echo isset($_POST['ddlSortBy']) && $_POST['ddlSortBy']=='first_name'?'selected':''; ?> value='first_name'>Sort By Name</option>
											<option <?php echo isset($_POST['ddlSortBy']) && $_POST['ddlSortBy']=='phone'?'selected':''; ?> value='phone'>Sort By Phone</option>
											<option <?php echo isset($_POST['ddlSortBy']) && $_POST['ddlSortBy']=='email'?'selected':''; ?> value='email'>Sort By Email</option>
											<option <?php echo isset($_POST['ddlSortBy']) && $_POST['ddlSortBy']=='state_name'?'selected':''; ?> value='state_name'>Sort By State Name</option>
										</select>
									</div>
								</div>
								<div class="col-md-3 col-sm-8 col-xs-12">
									<div class="form-group">
										<input value="<?php if(isset($_POST["txtAccidentDateSearchFrom"])) echo $_POST["txtAccidentDateSearchFrom"] ?>" class="form-control" placeholder='Accident Date From' id="txtAccidentDateSearchFrom" name="txtAccidentDateSearchFrom" type="text" />
									</div>
								</div>
								
								<div class="col-md-3 col-sm-8 col-xs-12">
									<div class="form-group">
										<input value="<?php if(isset($_POST["txtAccidentDateSearchTo"])) echo $_POST["txtAccidentDateSearchTo"] ?>" class="form-control" placeholder='Accident Date To' id="txtAccidentDateSearchTo" name="txtAccidentDateSearchTo" type="text" />
									</div>
								</div>
								<?php
								if($_SESSION['userType']==$MSW)
								{
									?>
										<div class="col-md-3 col-sm-12 col-xs-12">
											<div class="form-group">
												<select name="ddlWasRejected" id="ddlWasRejected" class="d-flex form-control">
													<option value='1'>Lead Rejected Sometime (Yes)</option>
													<option selected value='0'>Lead Rejected Sometime (No)</option>
												</select>
											</div>
										</div>
										<div class="col-md-3 col-sm-12 col-xs-12">
											<div class="form-group">
												<select name="ddlWasAssignedToVerifier" id="ddlWasAssignedToVerifier" class="d-flex form-control">
													<option value=''>Was Assigned To Verifier</option>
													<?php
														while($vcrow = mysqli_fetch_object($getVerifiers))
														{
															?>	
																<option value='<?php echo $vcrow->uid ?>'><?php echo $vcrow->verifier_name ?></option>
															<?php
														}
													?>
												</select>
											</div>
										</div>
									<?php
								}
								?>
								
								<div class="col-md-3 col-sm-12 col-xs-12">
									<input type="button" onclick="getPagination(1)"  class="btn btn-primary" value="Search" name="btnAdd" id="btnAdd" />
									<input type="button" class="btn btn-warning" value="Reset" name="btnReset" id="btnReset" onclick="window.location='reports.php'" />
									<?php
									
									if($_SESSION['userType']==$MSW || $_SESSION['userType']==$CCA || $_SESSION['userType']==$C)
									{
										?>
											<input type="hidden" name="hfIsViewLeads" id="hfIsViewLeads" value='0' />
											<input type="submit" class="btn btn-success" value="Export to Excel" name="" id="" /> 
										<?php
									}
									
									?>
								</div>
							</div>
						</form>
					</div>
				</div>
				
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
						
							
							<div class="card-body">

								<!-- Table Start -->

								<div class="table-responsive viewLead ajaxClass">	
								</div>

								<!-- Table End -->

							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			<!-- /.content-wrapper -->
			<?php include('../includes/copyright.php'); ?>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
		<script src="<?php echo $mysiteurl ?>js/jquery.table2excel.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() 
			{
				var delay = 1000;
					setTimeout(function() {
					 getPagination(1);
					}, delay);
			});
		</script>
	  
	</body>
</html>
