<?php

include('../includes/basic_auth.php'); 
$status='AC';
$hfArray=array();

if(isset($_POST['hfLeadId']) && isset($_POST['accident_array']) && trim($_POST['hfLeadId'])!=0)
{
	$lead_id = $_POST['hfLeadId'];
	$accident_array = array();
	$accident_array = json_decode($_POST['accident_array']);
	
	$param = date('Y-m-d H:i:s',strtotime("1970-01-01"));
	foreach($accident_array as $my_array)
	{
		if(isset($my_array->hfId) && isset($my_array->txt2DOB) && isset($my_array->txt2AccidentType) && isset($my_array->txt2AccidentClaim))
		{
			$dt = date('Y-m-d H:i:s',strtotime($my_array->txt2DOB));
			$accident_kind = intval($my_array->txt2AccidentType);
			$claim_accident = intval($my_array->txt2AccidentClaim);
			$is_na = $my_array->is_na;
			$hidden_id = $my_array->hfId;
			
			if($hidden_id==0)
			{
				$loginmaster = $mysqli->prepare("insert into  lead_accidents(lead_id, accident_date_time, accident_kind, claim_accident,is_na) values(?,?,?,?,?)");
				$loginmaster->bind_param("isiii",$lead_id,$dt,$accident_kind, $claim_accident,$is_na);
				$loginmaster->execute();
				$loginmaster->close();
				
				$hidden_id = mysqli_insert_id($mysqli);
			}
			else
			{
				$loginmaster = $mysqli->prepare("update lead_accidents set lead_id=?, accident_date_time=?, accident_kind=?, claim_accident=?, is_na=? where id=?");
				$loginmaster->bind_param("isiiii",$lead_id,$dt,$accident_kind, $claim_accident,$is_na,$hidden_id);
				$loginmaster->execute();
				$loginmaster->close();
			}
			
			$hfArray[] = $hidden_id;
			
			if($dt>$param)
			{
				$loginmaster = $mysqli->prepare("update leads set last_accident_type = (select type from accident_types where id = ?), last_accident_na = ?, latest_accident_dt = ? where id=?");
				$loginmaster->bind_param("iisi",$accident_kind,$is_na, $dt,$lead_id);
				$loginmaster->execute();
				$loginmaster->close();
				
				$param = $dt;
			}
		}
	}
	
	if(count($hfArray)>0)
	{
		$loginmaster = $mysqli->prepare("update lead_accidents set status=0 where id not in(".implode(',',$hfArray).") and lead_id = ?");
		$loginmaster->bind_param('i',$lead_id);
		$loginmaster->execute();
		$loginmaster->close();
	}
}
?>