<?php

include('../includes/basic_auth.php'); 
$status='AC';

if(isset($_POST['hfLeadId']) && isset($_POST['txt1FName']) && isset($_POST['txt1MName']) && isset($_POST['txt1LName']) && isset($_POST['txt1DOB']) && isset($_POST['txt1Email'])&& isset($_POST['txt1Mobile']) && isset($_POST['ddl1State']) && isset($_POST['txt1AddPostal']) && isset($_POST['ddl1SubUrb']) && isset($_POST['txt1SubUrbName']) && isset($_POST['txt1AddAddress']) && isset($_POST['txt1Relation']) && isset($_POST['txt1AltNumber']))
{
	$lead_id = $_POST['hfLeadId'];
	$_POST['txt1DOB'] = trim($_POST['txt1DOB'])!=null?$_POST['txt1DOB']:null;
	$_POST['ddl1State'] = $_POST['ddl1State']!=0?$_POST['ddl1State']:null;
	$_POST['ddl1SubUrb'] = $_POST['ddl1SubUrb']!=0?$_POST['ddl1SubUrb']:null;
	$_POST['txt1SubUrbName'] = $_POST['ddl1SubUrb']==0?$_POST['txt1SubUrbName']:null;

	if($lead_id==0)
	{		
		if($_SESSION["userType"]=='CCA')
		{
			$callcenter_id = $_SESSION["userId"];
		}
		else
		{
			$loginmaster = $mysqli->prepare("select callcenter_id from  tl_and_agents where uid=?");
			$loginmaster->bind_param("i",$_SESSION['userId']);
			$loginmaster->execute();
			$loginmaster->bind_result($callcenter_id);
			$loginmaster->fetch();
			$loginmaster->close();	
		}
		
		if(isset($_SESSION["userType"]) && trim($_SESSION["userType"])==$CCA && $_SESSION["is_affliated"]==1)
		{
			$the_date2 = $thisdate;
			$agent_assigned = null;
			$assigned_usertype=$CCA;
		}
		else if(isset($_SESSION["userType"]) && trim($_SESSION["userType"])==$CA)
		{
			$the_date1 = $thisdate;
			$agent_assigned = $_SESSION['userId'];
			$assigned_usertype=$CA;
		}
		
		$lead_transfer_type = 'LT';
		$comments = 'New Lead';
		$comment_status = 'PN';
		
		$loginmaster = $mysqli->prepare("INSERT INTO leads(first_name, middle_name, last_name, phone, email, relative_name, relative_number, dob, state_id, suburb_id, suburb_name, postal_code, address, added_by, status, creation_date, updation_date, callcenter_id,assign_agent, assign_affliated, agent_assigned,assigned_usertype, last_comment, latest_comment_usertype_from, latest_comment_usertype_to) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		$loginmaster->bind_param("ssssssssiisssisssississss",$_POST['txt1FName'],$_POST['txt1MName'],$_POST['txt1LName'],$_POST['txt1Mobile'],$_POST['txt1Email'],$_POST['txt1Relation'],$_POST['txt1AltNumber'],$_POST['txt1DOB'],$_POST['ddl1State'],$_POST['ddl1SubUrb'],$_POST['txt1SubUrbName'],$_POST['txt1AddPostal'],$_POST['txt1AddAddress'],$_SESSION['userId'],$status,$thisdate,$thisdate,$callcenter_id,$the_date1, $the_date2,$agent_assigned,$assigned_usertype,$comments, $_SESSION['userType'], $_SESSION['userType']);
		$loginmaster->execute();
		$loginmaster->close();
		
		$lead_id = mysqli_insert_id($mysqli);
			
		if(isset($_SESSION["userType"]) && trim($_SESSION["userType"])==$CCA && $_SESSION["is_affliated"]==1)
		{
			outcome_function($lead_id, $_SESSION['userId'],'ACL', $thisdate);
		}
		else if(isset($_SESSION["userType"]) && trim($_SESSION["userType"])==$CA)
		{
			outcome_function($lead_id, $_SESSION['userId'],'AL', $thisdate);
		}
		
		$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type) VALUES (?,?,?,?,?,?,?,?)");
		$loginmaster->bind_param("iisissss",$lead_id,$_SESSION['userId'],$_SESSION['userType'],$_SESSION['userId'],$comments,$comment_status,$thisdate, $lead_transfer_type);
		$loginmaster->execute();
		$loginmaster->close();
	}
	else
	{
		$loginmaster = $mysqli->prepare("update leads set first_name=?, middle_name=?, last_name=?, phone=?, email=?, relative_name=?, relative_number=?, dob=?, state_id=?, suburb_id=?, suburb_name=?, postal_code=?, address=?, status=?, updation_date=? where id=?");
		$loginmaster->bind_param("ssssssssiisssssi",$_POST['txt1FName'],$_POST['txt1MName'],$_POST['txt1LName'],$_POST['txt1Mobile'],$_POST['txt1Email'],$_POST['txt1Relation'],$_POST['txt1AltNumber'],$_POST['txt1DOB'],$_POST['ddl1State'],$_POST['ddl1SubUrb'],$_POST['txt1SubUrbName'],$_POST['txt1AddPostal'],$_POST['txt1AddAddress'],$status,$thisdate,$_POST['hfLeadId']);
		$loginmaster->execute();
		$loginmaster->close();
	}
}
?>
<input class="form-control" value='<?php echo $lead_id ?>' id="hfLeadId" name="hfLeadId" type="hidden">