<?php
include('../includes/basic_auth.php');

if(isset($_POST['hfLeadId']))
{
	$lead_id = $_POST['hfLeadId'];
	$checklist1 = $_POST['rbtn1'];
	$checklist2 = $_POST['rbtn2'];
	$checklist3 = $_POST['rbtn3'];
	
	$loginmaster = $mysqli->prepare("update leads set checklist1 = ?, checklist2 = ?, checklist3 = ? where id = ?");
	$loginmaster->bind_param("iiii",$checklist1,$checklist2,$checklist3, $lead_id);
	$loginmaster->execute();
	$loginmaster->close();
}
?>