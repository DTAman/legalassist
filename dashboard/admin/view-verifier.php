<?php 
include('../includes/basic_auth.php');

$condition='';

if(isset($_POST["btnAdd"]))
{
	if(trim($_POST['txtSearch'])!=null)
	{
		$condition.=" and (verifier.phone like '%{$_POST['txtSearch']}%' or verifier.verifier_name like '%{$_POST['txtSearch']}%') ";
	}
	
	if(trim($_POST['txtFromDate'])!=null)
	{
		$create_date=date('Y-m-d',strtotime($_POST['txtFromDate']));
		$condition.=" and login_master.creation_date >= '{$create_date}'";
	}
	
	if(trim($_POST['txtToDate'])!=null)
	{
		$to_date=date('Y-m-d',strtotime($_POST['txtToDate']));
		$condition.=" and login_master.creation_date <= '{$to_date}'";
	}
}

$totalCount = getUserCount($VA, $condition);
$totalResults = getUserDataByPagination($VA,$condition,$limit,$offset);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
	  
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">View Verifier</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
			
				<!-- row -->
				
				<div class="card"  style="margin-bottom:0;">
					<div class="card-body">
						<form action='' name='frmSearch' id='frmSearch' method='post'>
							<div class="row">
								<div class="col-md-3 col-sm-8 col-xs-12">
									<div class="form-group">
										<input value="<?php if(isset($_POST["txtSearch"])) echo $_POST["txtSearch"] ?>" class="form-control" placeholder='Mobile / Verifier Name' id="txtSearch" name="txtSearch" placeholder="" type="text" />
									</div>
								</div>
								<div class="col-md-3 col-sm-8 col-xs-12">
									<div class="form-group">
										<input value="<?php if(isset($_POST["txtFromDate"])) echo $_POST["txtFromDate"] ?>" class="form-control" placeholder='From' id="txtFromDate" name="txtFromDate" placeholder="" type="text" />
									</div>
								</div>
								<div class="col-md-3 col-sm-8 col-xs-12">
									<div class="form-group">
										<input value="<?php if(isset($_POST["txtToDate"])) echo $_POST["txtToDate"] ?>" class="form-control" placeholder='To' id="txtToDate" name="txtToDate" placeholder="" type="text" />
										<input value="ajaxgetVAs.php" class="form-control" id="hfAjaxPage" name="hfAjaxPage" placeholder="" type="hidden" />
									</div>
								</div>
								<div class="col-md-3 col-sm-4 col-xs-4 text-right">
									<input type="submit" class="btn btn-primary" value="Search" name="btnAdd" id="btnAdd" />
									<input type="button" class="btn btn-warning" value="Reset" name="btnReset" id="btnReset" onclick="window.location='view-verifier.php'" />
								</div>
							</div>
						</form>
					</div>
				</div>
				
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
						
							
							<div class="card-body">

								<!-- Table Start -->

								<div class="table-responsive ajaxClass viewLeadTable">
									<?php
									if(mysqli_num_rows($totalResults)!=0)
									{
										?>
										<div class='hh5'>	<table class="w-100 table newTable table-striped table-hover">
											<thead>
												<tr>
													<th class='text-center'>Sr.No.</th>
													<th>Name</th>
													<th>Phone Number</th>
													<th>Alt Number</th>
													<th style="min-width: 300px">Address</th>
													<th>Status</th>
													<th class='text-center'>Quick Actions</th>
												</tr>
											</thead>
											<tbody>
											<?php
											$n = 1;
											while($rc = mysqli_fetch_object($totalResults))
											{
												?>
													<tr id="delete<?php echo $rc->uid ?>">
														<td class='text-center'><?php echo $n ?></td>
														<td><?php echo $rc->verifier_name ?></td>
														<td><?php echo $rc->phone ?></td>
														<td><?php echo $rc->alt_number ?></td>
														<td><?php echo $rc->address.', '.$rc->cname ?></td>
														<td><?php echo $rc->status=='AC'?'Active':'Inactive' ?></td>
														<td class='text-center'>
															<div class='icnos'>
																<a href="edit-verifier-each.php?u=<?php echo $rc->uid ?>" class="icn settings" title="" data-toggle="tooltip" data-original-title="Edit" aria-describedby="tooltip600250"><i class="ti-pencil"></i></a>
																<a class="icn text-success" href="edit-verifier-each.php?u=<?php echo $rc->uid ?>&s=1" title="" data-toggle="tooltip" data-original-title="View" aria-describedby="tooltip600250"><i class="ti-eye"></i></a>
																<a href="javascript:void(0)" onclick='deleteUser(<?php echo $rc->uid ?>)'  class="icn delete" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
															</div>
														</td>
													</tr>
												<?php
												$n++;
											}
											?>
											</tbody>
										</table></div>
										<?php
										include('../includes/ajax_before_pagination.php'); 
									}
									else
									{
										include('../includes/norows.php'); 
									}
									?>
								</div>

								<!-- Table End -->

							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			<!-- /.content-wrapper -->
			<?php include('../includes/copyright.php'); ?>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
	  
	</body>
</html>
