<div id="saveCommentModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Enter Comment Details</h4>
			</div>
			<form action='' method='post' name='frmSaveCloseComment' id='frmSaveCloseComment'>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 selectOptions"></div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea selectOptionsTextarea">
								<textarea required="required" class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter details if any" name='txtSaveComment' id='txtSaveComment'></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnSaveAndCloseComment" name="btnSaveAndCloseComment" type="submit" class="btn btn-primary" value='Assign Lead' />
				</div>
			</form>
		</div>
	</div>
</div>