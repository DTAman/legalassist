<div id="saveCloseModalNotes" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Enter Notes</h4>
			</div>
			<form action='' method='post' name='frmSaveCloseNotes' id='frmSaveCloseNotes' onsubmit='return false'>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<textarea required class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter details if any" name='txtSaveCloseDetailsNotes' id='txtSaveCloseDetailsNotes'></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnSaveAndCloseNotes" name="btnSaveAndCloseNotes" type="submit" class="btn btn-primary" value='Save' />
				</div>
			</form>
		</div>
	</div>
</div>