<?php 
include('../includes/basic_auth.php'); 
$getStates = getStates();
$accident_count=5;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
		<style>
			.p-4 
			{
				padding: 10px 10px 10px 20px!important;
			}
		</style>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
		
		<div class="content-wrapper">
			<div class="container-fluid" id="personal-detail">
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Personal Details</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
				
				<!-- row -->
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<input class="form-control" value='0' id="hfSomethingChanged" name="hfSomethingChanged" type="hidden" />
								<input class="form-control" value='0' id="openModal_val" name="openModal_val" type="hidden" />
								<div class='hfLeadId'>
									<input class="form-control" value='0' id="hfLeadId" name="hfLeadId" type="hidden" />
								</div>
								
								<div class="card-body">
									<form action='' method='post' name='frm1Leads' id='frm1Leads' onsubmit='return false'>
										<div class="row">
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="txt1FName">First Name</label>
													<input class="form-control" id="txt1FName" name="txt1FName" placeholder="First Name" type="text">
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="txt1MName">Middle Name</label>
													<input class="form-control" id="txt1MName" name="txt1MName" placeholder="Middle Name" type="text">
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="txt1LName">Last Name</label>
													<input class="form-control" id="txt1LName" name="txt1LName" placeholder="Last Name" type="text">
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="txt1DOB">D.O.B</label>
														<div class="calender-wrap">
															<div>
																<select name="txt1Day" id="txt1Day" class="form-control">
																	<option value=''>Day</option>
																	<?php
																	for($day=1;$day<=31;$day++)
																	{
																		?>
																			<option value='<?php echo $day ?>'><?php echo $day ?></option>
																		<?php
																	}
																	?>
																</select>
															</div>
															
															<div>
																<select name="txt1Month" id="txt1Month" class="form-control">
																	<option value=''>Month</option>
																	<?php
																	for($mon=1;$mon<=12;$mon++)
																	{
																		?>
																			<option value='<?php echo $mon ?>'><?php echo  substr(date('F', mktime(0,0,0,$mon, 1, date('Y'))),0,3) ?></option>
																		<?php
																	}
																	?>
																</select>
															</div>
															
															<div>
																<select name="txt1Year" id="txt1Year" class="form-control">
																	<option value=''>Year</option>
																	<?php
																	$running_year = date('Y');
																	for($y=$running_year;$y>=$running_year-100;$y--)
																	{
																		?>
																			<option value='<?php echo $y ?>'><?php echo $y ?></option>
																		<?php
																	}
																	?>
																</select>
															</div>
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="txt1Email">E-Mail</label>
													<input class="form-control" id="txt1Email" name="txt1Email" placeholder="E-Mail" type="text">
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="txt1Mobile">Mobile Number</label>
													<input value='0' class="form-control" id="txt1Mobile" name="txt1Mobile" placeholder="Mobile Number" type="text" maxlength="10" minlength="10">
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label for="">Address</label>
													<select name="ddl1State" id="ddl1State" class="d-flex form-control" onchange="getAllPersonalDetailsLeadSuburbs(this.value)">
														<option value=''>Select State</option>
														<?php
															while($staterow = mysqli_fetch_object($getStates))
															{
																?>	
																	<option value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
																<?php
															}
														?>
													</select>
												</div>
											</div>
											
											<div class="col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label for="txt1AddPostal" class="invisible d-none d-md-block">Postal Code</label>
													<input class="form-control" name="txt1AddPostal" id="txt1AddPostal" placeholder="Postal Code" type="text" />
												</div>
											</div>
											<div class="col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label for="ddl1SubUrb" class="invisible d-none d-md-block" >Select Sub urb</label>
													<select name="ddl1SubUrb" id="ddl1SubUrb" class="d-flex form-control" onchange="noneSelcet(this.value)">
														<option value=''>Select Sub urb</option>
													</select>
												</div>
											</div>
											<div class="col-md-4 col-sm-12 col-xs-12 d-none" id="subUrbTextBox">
												<div class="form-group">
													<label for="txt1SubUrbName" class="invisible d-none d-md-block" >Suburb Name</label>
													<input class="form-control" name="txt1SubUrbName" id="txt1SubUrbName" placeholder="SubUrb Name" type="text" />
												</div>
											</div>
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="form-group fullwidthTextarea">
													<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Address First Line" name='txt1AddAddress' id='txt1AddAddress'></textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<h6>Write Alternative Number</h6><hr>
											</div>
											<div class="col-md-4 col-sm-12 float-left">
												<div class="form-group">
													<label for="txt1Relation">Relation</label>
													<input class="form-control" id="txt1Relation" name="txt1Relation" placeholder="Relation" type="text">
												</div>
											</div>
											<div class="col-md-8 col-sm-12 float-left">
												<div class="form-group">
													<label for="txt1AltNumber">Alternate Phone Number</label>
													<input class="form-control" id="txt1AltNumber" name="txt1AltNumber" placeholder="Phone Number" type="text">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<input id="check1" name="check1" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save & Continue" />
												<input div_attr='1' type="submit" class="btn btn-danger ml-0 mr-3 mt-2 openModal" value="Save & Close" />
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
						
					</div>
				<!-- /.row -->
			</div>
			<div class="container-fluid" id="past-accident-detail">
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Accident Details</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<div class="card-body">
									<form action='' method='post' name='frm2Leads' id='frm2Leads' onsubmit='return false'>
										<div class="row">
											<div class="col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													
													<input class="form-control" value="0" id="txtAccidentTotalCount" name="txtAccidentTotalCount" type="hidden" />
													<input class="form-control" value="0" id="last_count" name="last_count" type="hidden" />
													
													<label for="ddl2SelectCount">How many accidents have you had in the 3 years</label>
													<select name="ddl2SelectCount" id="ddl2SelectCount" class="d-flex form-control" onchange="dateOfAccidentV(this.value)">
														<option disabled selected value="">Select</option>
														<?php 
															for($accident_loop=1;$accident_loop<=$accident_count;$accident_loop++)
															{
																?>
																	<option value="<?php echo $accident_loop ?>"><?php echo $accident_loop ?></option>
																<?php
															}
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12" id="dateOfAccident">
												
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<button id="check21" type="button" class="btn btn-primary ml-0 mr-3 mt-2">Back</button>
												<!--<input id="check2" name="check2" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save & Continue" />-->
												<input id="check2" name="check2" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save & Continue" />
												<input div_attr='2' type="submit" class="btn btn-danger ml-0 mr-3 mt-2 openModal" value="Save & Close" />
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- /.row -->
			</div>
			<div class="container-fluid" id="accident-details">
				<form action='' method='post' name='frm3Leads' id='frm3Leads' onsubmit='return false'>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class='stage3'>
						</div>
						<!--<button id="check31" type="button" class="btn btn-primary ml-0 mr-3 mt-2">Back</button>
						<input id="check3" name="check3" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save & Continue" />-->
					</div>
				</form>
			</div>
			<div class="container-fluid" id="vehicle-detail">
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Vehicle Details</h4>
					</div>
				</div>

				<form action='' method='post' name='frm4Leads' id='frm4Leads' onsubmit='return false'>
				<div class="row">
					<div class="col-sm-12">
						<div class="bg-primary-light form-group jumbotron p-4">
							<label class="d-block">Were you having your vehicle or third party vehicle?</label>
							<div class="custom-radio d-inline-block fl-left mr-3">
								<input type="radio" checked="checked" class="custom-control-input" id="txt4MyVehicle" onclick="showVehicle(this.value)" name="txt4Vehicle" value="1">
								<label class="custom-control-label" for="txt4MyVehicle">My Vehicle</label>
							</div>
							<div class="custom-radio d-inline-block fl-left mr-3">
								<input type="radio" class="custom-control-input" id="txt4ThirdParty" onclick="showVehicle(this.value)" name="txt4Vehicle" value="0">
								<label class="custom-control-label" for="txt4ThirdParty">Third Party Vehicle</label>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<div class="card-body">
										<div class=''>
											<div class="row">
												<div class="col-sm-12 mb-3 mt-2">
													<h5><strong>Client Vehicle</strong></h5>
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdMake">Make</label>
														<input class="form-control" id="txt5ThirdMake" name="txt5ThirdMake" placeholder="Make" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdModel">Model</label>
														<input class="form-control" id="txt5ThirdModel" name="txt5ThirdModel" placeholder="Model" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdRegistration">Registration</label>
														<input class="form-control" id="txt5ThirdRegistration" name="txt5ThirdRegistration" placeholder="Registration" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label class="d-none d-md-block invisible" for="">.</label>
														<a href="https://www.google.com/" class="text-primary" target="_blank"><strong>google.com</strong></a>
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdInsuranceCompany">Insurance company Name</label>
														<input class="form-control" id="txt5ThirdInsuranceCompany" name="txt5ThirdInsuranceCompany" placeholder="Insurance company Name" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdInsuranceRef">Insurance reference number</label>
														<input class="form-control" id="txt5ThirdInsuranceRef" name="txt5ThirdInsuranceRef" placeholder="Insurance reference number" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5ThirdInsuranceClaim">Insurance Claim number</label>
														<input class="form-control" id="txt5ThirdInsuranceClaim" name="txt5ThirdInsuranceClaim" placeholder="Insurance Claim number" type="text">
													</div>
												</div>
												<div class="col-sm-12 col-md-9">
													<div class="form-group">
														<label for="txt5ThirdInsuranceOther">Other Info</label>
														<textarea class="form-control" name="txt5ThirdInsuranceOther" id="txt5ThirdInsuranceOther" cols="30" rows="3" placeholder="Other Info"></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class=''>
											<div class="row">
												<div class="col-sm-12 mb-3 mt-2">
													<h5><strong id='myvehicle'>Third Party Vehicle</strong></h5>
												</div>
											</div>
											<div class="row myvehicle">
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdDriverName">Driver Name</label>
														<input class="form-control" id="txt5DThirdDriverName" name="txt5DThirdDriverName" placeholder="Driver Name" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdAddress">Driver Address</label>
														<input class="form-control" id="txt5DThirdAddress" name="txt5DThirdAddress" placeholder="Driver Address" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdDLNumber">DL number</label>
														<input class="form-control" id="txt5DThirdDLNumber" name="txt5DThirdDLNumber" placeholder="DL number" type="text">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdMake">Make</label>
														<input class="form-control" id="txt5DThirdMake" name="txt5DThirdMake" placeholder="Make" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdModel">Model</label>
														<input class="form-control" id="txt5DThirdModel" name="txt5DThirdModel" placeholder="Model" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdRegistration">Registration</label>
														<input class="form-control" id="txt5DThirdRegistration" name="txt5DThirdRegistration" placeholder="Registration" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label class="d-none d-md-block invisible" for="">.</label>
														<a href="https://www.google.com/" class="text-primary" target="_blank"><strong>google.com</strong></a>
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdInsuranceCompany">Insurance company Name</label>
														<input class="form-control" id="txt5DThirdInsuranceCompany" name="txt5DThirdInsuranceCompany" placeholder="Insurance company Name" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdInsuranceRef">Insurance reference number</label>
														<input class="form-control" id="txt5DThirdInsuranceRef" name="txt5DThirdInsuranceRef" placeholder="Insurance reference number" type="text">
													</div>
												</div>
												<div class="col-md-3 col-sm-12">
													<div class="form-group">
														<label for="txt5DThirdInsuranceClaim">Insurance Claim number</label>
														<input class="form-control" id="txt5DThirdInsuranceClaim" name="txt5DThirdInsuranceClaim" placeholder="Insurance Claim number" type="text">
													</div>
												</div>
												<div class="col-sm-12 col-md-9">
													<div class="form-group">
														<label for="txt5DThirdInsuranceOther">Other Info</label>
														<textarea class="form-control" name="txt5DThirdInsuranceOther" id="txt5DThirdInsuranceOther" cols="30" rows="3" placeholder="Other Info"></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<button id="check41" type="button" class="btn btn-primary ml-0 mr-3 mt-2">Back</button>
												<input id="check4" name="check4" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save & Continue" />
											</div>
										</div>
									
								</div>
							</div>
						</div>
						
					</div>
				</div>
				</form>
				<!-- /.row -->
			</div>
			<div class="container-fluid" id="injuries-matrix">
				
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Injuries - Matrix Format</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
				
				<!-- row -->
				<form action='' method='post' name='frm5Leads' id='frm5Leads' onsubmit='return false'>
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								
								
								<div class="card-body">
									<form>
										<!-- tablel-start -->
										<div class="tble-rspon table-responsive mb-5">
											<table class="w-100">
												<thead>
													<tr>
														<th class="p-3"></th>
														<?php
															$getInjuries = getInjuries();
															$getBodyParts = getBodyParts();
															while($irow = mysqli_fetch_object($getInjuries))
															{
																?>
																	<th><?php echo $irow->injury ?></th>
																<?php
															}
														?>
													</tr>
												</thead>
												<tbody>
													<?php
													while($brow = mysqli_fetch_object($getBodyParts))
													{
														?>
															<tr>
																<td class="p-3"><?php echo $brow->body_part ?></td>
																<?php
																mysqli_data_seek($getInjuries,0);
																while($irow = mysqli_fetch_object($getInjuries))
																{
																	?>
																		<td class="custom-checkbox m-0 text-center">
																			<input type="checkbox" class="custom-control-input customCheck chckInjury" name="chckInjury" body_part='<?php echo $brow->id ?>'injuries='<?php echo $irow->id ?>'>
																			<label class="custom-control-label" for="customCheck"></label>
																		</td>
																	<?php
																}
																?>
															</tr>
														<?php
													}
													?>
												</tbody>
											</table>
										</div>
										<!-- tablel-end -->
										<div class="row">
											<div class="col-md-7 col-sm-12">
												<div class="form-group">
													<label for="txt6AnyOtherInjury">Any Other Injuries</label>
													<textarea class="form-control" name="txt6AnyOtherInjury" id="txt6AnyOtherInjury" cols="30" rows="3" placeholder="Write in Brief"></textarea>
												</div>
											</div>
											<div class="col-md-3 col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="">Still Suffering</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="txt6StillSufferingYes" name="txt6StillSuffering" value="1">
														<label class="custom-control-label" for="txt6StillSufferingYes">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input checked='checked' type="radio" class="custom-control-input" id="txt6StillSufferingNo" name="txt6StillSuffering" value="0">
														<label class="custom-control-label" for="txt6StillSufferingNo">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<button id="check51" type="button" class="btn btn-primary ml-0 mr-3 mt-2">Back</button>
												<input id="check5" name="check5" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save & Continue" />
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				</form>
				<!-- /.row -->
			</div>
			
			<div class="container-fluid" id="treatment-history">			
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Treatment History</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<div class="card-body">
									<form action='' method='post' name='frm6Leads' id='frm6Leads' onsubmit='return false'>
										<div class="row">
											<div class="col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block">Did An Ambulance Come To The Scene Of The Accident?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="ambulanceComeSceneY" onclick="ambulanceComeSceneV(this.value)" name="ambulanceComeScene" value="Yes">
														<label class="custom-control-label" for="ambulanceComeSceneY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="ambulanceComeSceneN" onclick="ambulanceComeSceneV(this.value)" name="ambulanceComeScene" value="No">
														<label class="custom-control-label" for="ambulanceComeSceneN">No</label>
													</div>
												</div>
											</div>
										</div>
										<!--<div class="row mt-4 d-none" id="ambulanceComeSceneYes">-->
										<div class="row d-none" id="ambulanceComeSceneYes">
											<div class="col-sm-12">
												<div class="bg-success-light form-group jumbotron p-4">
													<label class="d-block" for="">Did The Ambulance Treat You At The Scene Of The Accident?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="ambulanceTreatSceneY" onclick="ambulanceTreatSceneV(this.value)" name="ambulanceTreatScene" value="Yes">
														<label class="custom-control-label" for="ambulanceTreatSceneY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="ambulanceTreatSceneN" onclick="ambulanceTreatSceneV(this.value)" name="ambulanceTreatScene" value="No">
														<label class="custom-control-label" for="ambulanceTreatSceneN">No</label>
													</div>
												</div>
											</div>
										</div>
										<!--<div class="row mt-4 d-none" id="ambulanceComeSceneNo">-->
										<div class="row d-none" id="ambulanceComeSceneNo">
											<div class="col-sm-12">
												<div class="bg-success-light form-group jumbotron p-4">
													<label class="d-block" for="">Did You Go To A Hospital By Yourself Or Taken There By A Family Member Or Friend?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="goToHospitalY" onclick="goToHospitalV(this.value)" name="rbtnGoToHospital" value="Yes">
														<label class="custom-control-label" for="goToHospitalY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="goToHospitalN" onclick="goToHospitalV(this.value)" name="rbtnGoToHospital" value="No">
														<label class="custom-control-label" for="goToHospitalN">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row d-none" id="goToHospitalYes">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="txtWhatWasTreatment">What Was The Treatment?</label>
													<textarea class="form-control" name="txtWhatWasTreatment" id="txtWhatWasTreatment" cols="30" rows="4" placeholder="Write in Brief"></textarea>
												</div>
											</div>
										</div>
										<div class="row d-none" id="ambulanceTakeYouHospital">
											<div class="col-sm-12">
												<div class="bg-success-light form-group jumbotron p-4">
													<label class="d-block" for="exampleInputEmail1">Did The Ambulance Take You To A Hospital?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="ambulanceTakenHospitalY" onclick="ambulanceTakenHospitalV(this.value)" name="rbtnAmbulanceTakenHospital" value="Yes">
														<label class="custom-control-label" for="ambulanceTakenHospitalY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="ambulanceTakenHospitalN" onclick="ambulanceTakenHospitalV(this.value)" name="rbtnAmbulanceTakenHospital" value="No">
														<label class="custom-control-label" for="ambulanceTakenHospitalN">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row d-none" id="hospitalDetail">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="txtHospitalDetails">Hospital Detail</label>
													<input class="form-control" id="txtHospitalDetails" placeholder="Hospital Detail" type="text">
												</div>
												<div class="form-group">
													<label for="txtWhatWasHospitalTreatment">What Was The Treatment?</label>
													<textarea class="form-control" name="txtWhatWasHospitalTreatment" id="txtWhatWasHospitalTreatment" cols="30" rows="4" placeholder="Write in Brief"></textarea>
												</div>
											</div>
										</div>
										<!--<div class="row mt-4 d-none" id="gpAfterAccident">-->
										<div class="row d-none" id="gpAfterAccident">
											<div class="col-sm-12">
												<div class="bg-success-light form-group jumbotron p-4">
													<label class="d-block">Did you go to the GP after the accident?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="rbtnWentToGPY" onclick="showTwo(this.value)" name="rbtnVisitGP" value="Yes">
														<label class="custom-control-label" for="rbtnWentToGPY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="rbtnWentToGPN" onclick="showTwo(this.value)" name="rbtnVisitGP" value="No">
														<label class="custom-control-label" for="rbtnWentToGPN">No</label>
													</div>
												</div>
											</div>
											
											<div class="col-sm-12 d-none" id="gpNameSurgeryVisit">
												<div class="row">
													<div class="col-md-3 col-sm-12">
														<div class="form-group">
															<label for="txtGPName">GP Name</label>
															<input class="form-control" name="txtGPName" id="txtGPName" placeholder="GP Name" type="text">
														</div>
													</div>
													<div class="col-md-3 col-sm-12">
														<div class="form-group">
															<label for="txtGPSurgeryName">Surgery Name</label>
															<input class="form-control" name="txtGPSurgeryName" id="txtGPSurgeryName" placeholder="Surgery Name" type="text">
														</div>
													</div>
													<div class="col-md-3 col-sm-12">
														<div class="form-group">
															<label for="txtGPVisits">No Of Visits To The GP Since Accident</label>
															<input class="form-control" name="txtGPVisits" id="txtGPVisits" placeholder="Visit No." type="number">
														</div>
													</div>
													<div class="col-md-3 col-sm-12">
														<div class="form-group">
															<label for="txtGPVisits">Last Visit</label>
															<input class="form-control datepicker_class" name="txtGPLastVisit" id="txtGPLastVisit" placeholder="DD/MM/YY" type="text">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
										<!--<div class="row d-none" id="takingAnyPainRelief">-->
											<div class="col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="">Are you taking any pain relief to manage your symptoms?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="takingAnyPainReliefY" name="rbtnAnyPainRelief" value="Yes">
														<label class="custom-control-label" for="takingAnyPainReliefY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input checked="checked"  type="radio" class="custom-control-input" id="takingAnyPainReliefN" name="rbtnAnyPainRelief" value="No">
														<label class="custom-control-label" for="takingAnyPainReliefN">No</label>
													</div>
												</div>
											</div>
										</div>
										
										<div class="row">
										<!--<div class="row mt-4 d-none" id="physiotherapyChrioOne">-->
											<div class="col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="">Were you recommended for Physiotherapy Or Chiropractic Sessions?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="rbtnRecommendedPhysioChrioY" onclick="selectPhysioChiro(this.value)" name="rbtnRecommendedPhysioChrio" value="Yes">
														<label class="custom-control-label" for="rbtnRecommendedPhysioChrioY">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input checked="checked" type="radio" class="custom-control-input" id="rbtnRecommendedPhysioChrioN" onclick="selectPhysioChiro(this.value)" name="rbtnRecommendedPhysioChrio" value="No">
														<label class="custom-control-label" for="rbtnRecommendedPhysioChrioN">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row d-none" id="bothPhysioChiroNew">
											<div class="col-sm-12 pt-3">
												<div class="form-group">
													<div class="custom-radio mr-3">
														<input type="radio" class="custom-control-input" id="IsPhysioY" onclick="selectOnePhysioChiro(this.value)" name="IsPhysio" value="Yes">
														<label class="custom-control-label" for="IsPhysioY">Physiotherapy</label>
													</div>
													<div class="custom-radio mr-3">
														<input type="radio" class="custom-control-input" id="IsPhysioN" onclick="selectOnePhysioChiro(this.value)" name="IsPhysio" value="No">
														<label class="custom-control-label" for="IsPhysioN">Chiropractic</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row d-none" id="affordPhysioNew">
											<div class="col-md-3 col-sm-12">
												<div class="form-group">
													<label class="d-block" for="">Can you afford the Physiotherapy?</label>
													<div class="custom-radio mr-3">
														<input type="radio" class="custom-control-input" id="customRadio6.4" onclick="physioYes(this.value)" name="affordPhysio" value="Yes">
														<label class="custom-control-label" for="customRadio6.4">Yes</label>
													</div>
													<div class="custom-radio mr-3">
														<input type="radio" class="custom-control-input" id="customRadio6.5" onclick="physioYes(this.value)" name="affordPhysio" value="No">
														<label class="custom-control-label" for="customRadio6.5">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row d-none" id="affordChiroNew">
											<div class="col-md-3 col-sm-12">
												<div class="form-group">
													<label class="d-block" for="">Can you afford the Chiropractic?</label>
													<div class="custom-radio mr-3">
														<input type="radio" class="custom-control-input" id="customRadio6.6" onclick="chiroYes(this.value)" name="affordChiro" value="Yes">
														<label class="custom-control-label" for="customRadio6.6">Yes</label>
													</div>
													<div class="custom-radio mr-3">
														<input type="radio" class="custom-control-input" id="customRadio6.7" onclick="chiroYes(this.value)" name="affordChiro" value="No">
														<label class="custom-control-label" for="customRadio6.7">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row d-none" id="eachVisitPhysio">
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="txtPhysioVisits">No. of Visits For Physiotherapy</label>
													<input class="form-control" name="txtPhysioVisits" id="txtPhysioVisits" placeholder="Physiotherapy Visit No." type="number">
												</div>
											</div>
										</div>
										<div class="row d-none" id="eachVisitChiro">
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="txtChirioVisits">No. of Visits For Chiropractic</label>
													<input class="form-control" name="txtChirioVisits" id="txtChirioVisits" placeholder="Chiropractic Visit No." type="number">
												</div>
											</div>
										</div>
										<!--<div class="row mt-4 d-none" id="xrayMriOne">-->
										<div class="row">
											<div class="col-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="">Did You Go For Any Xray, MRI Or CT-Scan?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="customRadio7" onclick="showFive(this.value)" name="rbtnXrayScan" value="Yes">
														<label class="custom-control-label" for="customRadio7">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="customRadio7.1" onclick="showFive(this.value)" name="rbtnXrayScan" value="No">
														<label class="custom-control-label" for="customRadio7.1">No</label>
													</div>
												</div>
											</div>
											<div class="col-12 d-none" id="resultScanOne">
												<div class="form-group">
													<label for="txtScanResult">What Was The Result Of The Scan?</label>
													<textarea class="form-control" name="txtScanResult" id="txtScanResult" cols="30" rows="4" placeholder="Write in Brief"></textarea>
												</div>
											</div>
											<div class="col-12 d-none" id="resultScanNo">
												<div class="form-group">
													<label for="txtNoScan">Why did you not go for Xray, MRI and CT Scan?</label>
													<textarea class="form-control" name="txtNoScan" id="txtNoScan" cols="30" rows="4" placeholder="Write in Brief"></textarea>
												</div>
											</div>
										</div>
										<!--<div class="row d-none mt-4" id="preExcistingInjury">-->
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="txtPreExistingInjuries">Pre-existing injuries</label>
													<textarea class="form-control" name="txtPreExistingInjuries" id="txtPreExistingInjuries" cols="30" rows="6" placeholder="List all previous injuries (even if they do not relate to the same body part)"></textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
											<!--<div class="col-sm-12 d-none mt-4" id="sufferPsychological">-->
												<div class="row">
													<div class="col-sm-12">
														<h6 class="p-3 m-0 font-midium bg-secondary">Is the client suffering for psychological injuries? Please tick which ever applicable - multiple ticks allowed:</h6>
													</div>
													<div class="col-sm-12 col-md-3">
														<div class="form-group">
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input type="checkbox" class="custom-control-input form-control injuryList" id="Dizziness" name="injuryList[]" name_attr="dizziness">
																<label class="custom-control-label" for="Dizziness">Dizziness</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input type="checkbox" class="custom-control-input form-control injuryList" id="Nausea" name="injuryList[]" name_attr="nausea">
																<label class="custom-control-label" for="Nausea">Nausea</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input type="checkbox" class="custom-control-input form-control injuryList" id="Post" name="injuryList[]"  name_attr="post_traumatic_stress_disorder">
																<label class="custom-control-label" for="Post">Post Traumatic Stress Disorder</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input type="checkbox" class="custom-control-input form-control injuryList" id="Insomnia" name="injuryList[]"  name_attr="insomnia">
																<label class="custom-control-label" for="Insomnia">Insomnia</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input type="checkbox" class="custom-control-input form-control injuryList" id="Vertigo" name="injuryList[]"  name_attr="vertigo">
																<label class="custom-control-label" for="Vertigo">Vertigo</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input type="checkbox" class="custom-control-input form-control injuryList" id="Driving" name="injuryList[]"  name_attr="driving_difficulty_post_accident">
																<label class="custom-control-label" for="Driving">Driving difficulty post accident</label>
															</div>
															<div class="custom-checkbox custom-checkbox-own mt-3">
																<input type="checkbox" class="custom-control-input form-control injuryList" id="Fear" name="injuryList[]"  name_attr="fear_of_being_hit">
																<label class="custom-control-label" for="Fear">Fear of being hit every time client drives</label>
															</div>
														</div>
													</div>
													<div class="col-sm-12 col-md-9">
														<div class="form-group p-lg-5 p-0">
															<label for="txtAnyOther">Any Other</label>
															<textarea class="form-control" name="txtAnyOther" id="txtAnyOther" cols="30" rows="8"></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<button id="check61" type="button" class="btn btn-primary ml-0 mr-3 mt-2">Back</button>
												<input id="check6" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save & Continue" />
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- /.row -->
			</div>
			<div class="container-fluid" id="workCare-details">
				
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Work & Care Details</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
				
				<!-- row -->
				<div class="row">
					
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<div class="card-body">
									<form action='' method='post' name='frm7Leads' id='frm7Leads' onsubmit='return false'>
										<div class="row">
											<div class="col-sm-12 mb-2 d-block" id="passengerName">
												<hr><h5><strong>Work</strong></h5><hr>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label for="ddl7Employed">Are you employed?</label>
													<select name="ddl7Employed" id="ddl7Employed" class="d-flex form-control" onchange="meansOfIncomeV(this.value)">
														<option value='2'>Select</option>
														<option value="1">Yes</option>
														<option value="0">No</option>
													</select>
												</div>
											</div>
											<div class="col-md-4 col-sm-12 d-none" id="meansOfIncome">
												<div class="form-group">
													<label for="ddl7IncomeState">State Means of Income?</label>
													<select name="ddl7IncomeState" id="ddl7IncomeState" class="form-control" onchange="meansOfIncomeOtherV(this.value)">
														<option value=''>Select</option>
														<option value="Disability Pension">Disability Pension</option>
														<option value="Centerlink">Centerlink</option>
														<option value="State Pension">State Pension</option>
														<option value="Other">Other</option>
													</select>
												</div>
											</div>
											<div class="col-md-4 col-sm-12 d-none" id="stateMeansIncomeOther">
												<div class="form-group">
													<label for="txt7OtherIncomes">Other Means of Income</label>
													<input class="form-control" id="txt7OtherIncomes" name='txt7OtherIncomes' placeholder="Other" type="text">
												</div>
											</div>
											<div class="col-md-4 col-sm-12 d-none" id="yourOccupation">
												<div class="form-group">
													<label for="txt7Occupation">What is your occupation</label>
													<input class="form-control" id="txt7Occupation" name="txt7Occupation" placeholder="Occupation" type="text">
												</div>
											</div>
											<div class="col-sm-12 d-none" id="weeklyGrossIncome">
												<div class="row">
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label for="txt7WeeklySalary">Weekly salary (Gross/net) AUD</label>
															<input class="form-control" id="txt7WeeklySalary" name="txt7WeeklySalary" placeholder="(Gross/net) Salary" type="text">
														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label for="txt7WorkingHours">How many hours per week do you work?</label>
															<input class="form-control" id="txt7WorkingHours" name="txt7WorkingHours" placeholder="Hours" type="text">
														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label for="txt7WorkingIncomeLost">Have you lost income from this injury?</label>
															<input class="form-control" id="txt7WorkingIncomeLost" name="txt7WorkingIncomeLost" placeholder="Lost Income" type="text">
														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label for="ddl7HaveInjuries">Are your injuries affecting your work?</label>
															<select name="ddl7HaveInjuries" id="ddl7HaveInjuries" class="d-flex form-control">
																<option value=''>Select</option>
																<option value="1">Yes</option>
																<option value="0">No</option>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 mb-2 d-block" id="passengerName">
												<hr><h5><strong>Care</strong></h5><hr>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6 col-sm-12">
												<div class="form-group">
													<label for="txt7ReceivingAssistance">Are you receiving any assistance from anyone as a result of your injuries?</label>
													<label class="d-flex"><input class="form-control" id="txt7ReceivingAssistance" name="txt7ReceivingAssistance" placeholder="Receiving Any Assistance" type="text">
												</div>
											</div>
											<div class="col-md-6 col-sm-12">
												<div class="form-group">
													<label for="txt7RequireReceivingAssistance">Did you require this assistance before this injury?</label>
													<label class="d-flex"><input class="form-control"  id="txt7RequireReceivingAssistance" name="txt7RequireReceivingAssistance" placeholder="Require Assistance" type="text">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<button id="check71" type="button" class="btn btn-primary ml-0 mr-3 mt-2">Back</button>
												<input id="check7" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value='Save & Continue' />
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- /.row -->
			</div>
			
			<div class="container-fluid" id="passenger-details">
			
			<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Passenger Details</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<div class="card-body">
									<form action='' method='post' name='frm8Leads' id='frm8Leads' onsubmit='return false'>
										<div class="row">
											<div class="col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="exampleInputEmail1">Were There Any Passengers In The Vehicle</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="customRadio8" onclick="showSix(this.value)" name="any-passengers" value="Yes">
														<label class="custom-control-label" for="customRadio8">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" checked="checked" class="custom-control-input" id="customRadio8.1" onclick="showSix(this.value)" name="any-passengers" value="No">
														<label class="custom-control-label" for="customRadio8.1">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 d-none" id="fullInjuryAndTreatment">
												<div class="row mb-3">
													<div class="col-sm-12 col-md-3">
														<div class="form-group">
															<label for="exampleInputEmail1" id="the_passenger_label">How Many?</label>
															<input value="0"  class="form-control" count_attr="0" id="txt8PassengersCount" name="txt8PassengersCount" placeholder="How Many?" onkeyup="addPassengerDetails(this.value)" type="text" />
															<input value="0" class="form-control" id="hfSomethingDoneInPassenger" name="hfSomethingDoneInPassenger" type="hidden" />
														</div>
													</div>
												</div>
												<div class="row" id="passengerDetailOne">
													
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<button id="check81" type="button" class="btn btn-primary ml-0 mr-3 mt-2">Back</button>
												<input id="check8" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save & Continue" />
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- /.row -->
			</div>
			<div class="container-fluid" id="witness-details">
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Witness Details</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
				
				<!-- row -->
				<div class="row">
					
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								
								
								<div class="card-body">
									<form action='' method='post' name='frm9Leads' id='frm9Leads' onsubmit='return false'>
										<div class="row">
											<div class="col-sm-12">
												<div class="bg-primary-light form-group jumbotron p-4">
													<label class="d-block" for="">Do you have any witness?</label>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" class="custom-control-input" id="witnessOne1" onclick="witnessOne(this.value)" name="witnessO" value="1">
														<label class="custom-control-label" for="witnessOne1">Yes</label>
													</div>
													<div class="custom-radio d-inline-block fl-left mr-3">
														<input type="radio" checked='checked' class="custom-control-input" id="witnessOne2" onclick="witnessOne(this.value)" name="witnessO" value="0">
														<label class="custom-control-label" for="witnessOne2">No</label>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-sm-12 d-none" id="witnessFields">
												<div class="row">
													<div class="col-md-6 col-sm-12">
														<div class="form-group">
															<label for="txt9WitnessName">Name</label>
															<input class="form-control" id="txt9WitnessName" name="txt9WitnessName" placeholder="Name" type="text">
														</div>
													</div>
													<div class="col-md-6 col-sm-12 col-xs-12">
														<div class="form-group">
															<label for="txt9WitnessPhone">Phone Number</label>
															<input class="form-control" id="txt9WitnessPhone" name="txt9WitnessPhone" placeholder="Phone Number" type="number">
														</div>
													</div>
													<div class="col-md-4 col-sm-12 col-xs-12">
														<div class="form-group">
															<label for="">Address</label>
															<select name="ddl9State" id="ddl9State" class="d-flex form-control" onchange="getAllSuburbsWitness(this.value)">
																<option value=''>Select State</option>
																<?php
																	mysqli_data_seek($getStates,0);
																	while($staterow = mysqli_fetch_object($getStates))
																	{
																		?>	
																			<option value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
																		<?php
																	}
																?>
															</select>
														</div>
													</div>
													<div class="col-md-4 col-sm-12 col-xs-12">
														<div class="form-group">
															<label for="txt9WitnessPostalCode" class="invisible d-none d-md-block" >Post Code</label>
															<input class="form-control" id="txt9WitnessPostalCode" name="txt9WitnessPostalCode" placeholder="Post Code" type="text">
														</div>
													</div>
													<div class="col-md-4 col-sm-12 col-xs-12">
														<div class="form-group">
															<label for="ddl9SubUrb" class="invisible d-none d-md-block" >Select Sub urb</label>
															<select name="ddl9SubUrb" id="ddl9SubUrb" class="d-flex form-control">
																<option value=''>Select Sub urb</option>
															</select>
														</div>
													</div>
													<div class="col-md-12 col-sm-12 col-xs-12">
													  <div class="form-group fullwidthTextarea">
														<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Address First Line" name='txt9Address' id='txt9Address'></textarea>
													  </div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<button id="check91" type="button" class="btn btn-primary ml-0 mr-3 mt-2">Back</button>
												<input id="check9" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value='Save' />
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.content-wrapper -->
		<?php include('saveclosemodal.php'); ?>
		<?php include('../includes/copyright.php'); ?>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
		<script type="text/javascript" src="<?php echo $mysiteurl ?>js/main.js"></script>
	</body>
</html>