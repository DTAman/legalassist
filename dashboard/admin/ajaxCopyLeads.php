<?php
	include('../includes/basic_auth.php');
	if(isset($_POST['copy_id']) && trim($_POST['copy_id'])!=null)
	{
		$queryResults = "select id, first_name, phone from leads where is_deleted=0 and master_lead_id = ".$_POST['copy_id'];
		$dataresult = getQuery($queryResults);
		if(mysqli_num_rows($dataresult)>0)
		{
			?>
				<div class='row'>
					<div class='col-md-12'>
						<table class="table table-bordered">
							<thead>
								<tr class="bg-default-light">
									<th class="text-center">#</th>
									<th>Name</th>
									<th>Mobile</th>
									<th class="text-center">View</th>
								</tr>
							</thead>
							<tbody>
							<?php
								$i=0;
								while($row = mysqli_fetch_object($dataresult))
								{
									?>
										<tr>
											<td class="text-center"><?php echo ++$i ?></td>
											<td><?php echo $row->first_name ?></td>
											<td><?php echo $row->phone ?></td>
											<td class="text-center">
												<a class="text-danger" target='_blank' href="edit-lead-each.php?u=<?php echo $row->id ?>">
													<i class='fa fa-eye'></i>
												</a>
											</td>
										</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			<?php
		}
	}
?>
