<?php

$out_array = array(
	'NA'=>'No Answer',
	'CB'=>'CallBack Requested',
	'SC'=>'Spoken to Sol',
	'FI'=>'Further Investigation',
	'SU'=>'Sign Up Arranged',
	'RJ'=>'Lead Rejected',
	'AP'=>'Lead Approved',
	'IP'=>'Invoice Paid',
	'SI'=>'Invoice Sent',
	'COP'=>'Lead Copied',
	'AL'=>'Fresh - Agent',
	'ATTL'=>'Assigned To Team Lead',
	'TTC'=>'Assigned To Callcenter',
	'ACTA'=>'Pending - Sent From Affliated Callcenter To Admin',
	'CTA'=>'Pending - Sent From Normal Callcenter To Admin',
	'ATC'=>'Sent To Sol',
	'ATCC'=>'Sent Back To Normal Callcenter',
	'ATAC'=>'Sent Back To Affliated Callcenter',
	'ACL'=>'Fresh - Affliated Callcenter',
	'CTTL'=>'Sent Back To Team Lead',
	'TLTA'=>'Sent Back To Agent',
	'DA'=>'Directly Assigned To Admin'
);


foreach($out_array as $o=>$v)
{
	?>
		<div class='col-md-2'>
			<input type="checkbox" class="custom-control-input" value='<?php echo $o ?>' name='ddlLeadStatus' id='ddlLeadStatus<?php echo $o ?>'/>
			<label class="custom-control-label" for='ddlLeadStatus<?php echo $o ?>'><?php echo $v ?></label>
		</div>
	<?php
}


?>