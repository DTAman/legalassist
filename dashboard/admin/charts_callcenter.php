<?php 
include('../includes/basic_auth.php');

$date = date("Y-m-d");
$arr_1 = '';
$arr_2 = '';
$arr_3 = '';
$arr_4 = '';

$q3 = getCountLeads(' and leads.callcenter_id = '.$_SESSION['userId'],'CCA');

$datamaster = $mysqli->prepare("select count(*), 
							   (select count(*) from leads x where x.callcenter_id = ".$_SESSION['userId']." and (x.client_status='AP' or x.client_status='IP' or x.client_status='SI')) as approved,
							   (select count(*) from leads z where z.callcenter_id = ".$_SESSION['userId']." and z.client_status='SU') as signup,
							   (select count(*) from leads y where y.callcenter_id = ".$_SESSION['userId']." and assigned_usertype='CCA') as pending
							   from leads where callcenter_id = ".$_SESSION['userId']);
$datamaster->execute();
$datamaster->bind_result($d1, $d2, $d3, $d4);
$datamaster->fetch();
$datamaster->close();

$arr_3 ="[
		  ['Task', ''],
		  ['Leads Pending In Callcenter',".$d4."],
		  ['Approved Leads',".$d2."],
		  ['Currently Signed Up Leads',".$d3."]
		]";

if(isset($_SESSION["is_affliated"]) && $_SESSION["is_affliated"]==0)
{	
	$getAgents = getUsersofCallcenterLimit($_SESSION['userId'],'CA',5);
	while($b=mysqli_fetch_object($getAgents))
	{
		$query = "
			select count(*),
		   (select count(*) from leads q where q.added_by = ".$b->uid.") as generated_by,
		   (select count(*) from leads p where p.agent_assigned = ".$b->uid." and (p.assigned_usertype='CA')) as working,
		   (select count(*) from leads x where x.agent_assigned = ".$b->uid." and (x.client_status='AP' or x.client_status='IP' or x.client_status='SI')) as approved,
		   (select count(*) from leads l where l.agent_assigned = ".$b->uid." and assigned_client is not null) as leads_with_sol
		   from leads where agent_assigned = ".$b->uid;
		$datamaster = $mysqli->prepare($query);
		$datamaster->execute();
		$datamaster->bind_result($d1, $d2, $d3, $d4, $d5);
		$datamaster->fetch();
		$datamaster->close();
		
		$arr_1.= "['".($b->first_name.' '.$b->last_name)."',".$d1.",".$d2.",".$d3.",".$d4.",".$d5."],";
	}

	$getTL = getUsersofCallcenter($_SESSION['userId'],'CTL');
	while($b=mysqli_fetch_object($getTL))
	{
		$query = "
			select count(*),
		   (select count(*) from leads p where p.team_lead_assigned = ".$b->uid." and (p.assigned_usertype='CTL')) as working,
		   (select count(*) from leads x where x.team_lead_assigned = ".$b->uid." and (x.client_status='AP' or x.client_status='IP' or x.client_status='SI')) as approved,
		   (select count(*) from leads l where l.team_lead_assigned = ".$b->uid." and assigned_client is not null) as leads_with_sol
		   from leads where team_lead_assigned = ".$b->uid;
		$datamaster = $mysqli->prepare($query);
		$datamaster->execute();
		$datamaster->bind_result($d1, $d2, $d3, $d4);
		$datamaster->fetch();
		$datamaster->close();
		
		$arr_2.= "['".($b->first_name.' '.$b->last_name)."',".$d1.",".$d2.",".$d3.",".$d4."],";
	}

	$q1 = getCountLeads(' and leads.callcenter_id = '.$_SESSION['userId'],'CA');
	$q2 = getCountLeads(' and leads.callcenter_id = '.$_SESSION['userId'],'CTL');

	$arr_4 ="[
		  ['Task', ''],
		  ['In Callcenter',".$q3."],
		  ['In Agents',".$q1."],
		  ['In Team Leads',".$d2."]
		]";
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
		<div class="content-wrapper">
			<div class="container-fluid">
				<div class="row mt-3">
					<div class="col-md-3">
						<div class="alert alert-dark">
							<?php echo getCountLeads(' and leads.callcenter_id = '.$_SESSION['userId'],'') ?>
							<br/>
							Total Leads In Callcenter
						</div>
					</div>
					<?php
					if(isset($_SESSION["is_affliated"]) && $_SESSION["is_affliated"]==0)
					{
						?>
							<div class="col-md-3">
								<div class="alert alert-danger">
									<?php
									echo $q1?>
									<br/>
									Pending Leads in Agent
								</div>
							</div>
							<div class="col-md-3">
								<div class="alert alert-info">
									<?php echo $q2 ?>
									<br/>
									Pending Leads in Team Lead
								</div>
							</div>
						<?php
					}
					?>
					
					<div class="col-md-3">
						<div class="alert alert-success">
							<?php echo $q3 ?>
							<br/>
							Pending Leads in Callcenter
						</div>
					</div>
				</div>
				<div class="row mt-3">
					<div class="col-md-6">
						<div id="chart_3" style="height: 500px;"></div>
					</div>
					<div class="col-md-6">
						<div id="chart_4" style="height: 500px;"></div>
					</div>
				</div>
				<div class="row mt-3">
					<div class="col-md-12 agent_row">
						<div id="chart_1" style="height: 500px;"></div>
						<div>
							<ul class="pagination">
							<?php
							$total = getUserCountProgram("CA", " and tl_and_agents.type='CA' and is_deleted=0 and tl_and_agents.callcenter_id = ".$_SESSION['userId']);
							$total=ceil($total/$set_count);
							
							for($pagecount=1;$pagecount<=$total;$pagecount++)
							{
								?>
									<li id="li_page<?php echo $pagecount ?>" class="page-item <?php echo $pagecount==1?'active':'' ?>">
										<a class="page-link waves-effect" href="javascript:void(0)" role="button" onclick='getCharts(<?php echo $pagecount ?>)' href="javascript:void(0)"><?php echo $pagecount; ?>
										</a>
									</li>
								<?php
							}
							?>

							</ul>
						</div>
					</div>
				</div>
				<div class="row mt-3">
					<div class="col-md-12">
						<div id="chart_2" style="height: 500px;"></div>
					</div>
				</div>
			</div>
		</div>  
		<?php include('../includes/copyright.php'); ?>
		
		<!-- Scroll to Top Button-->  
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
		  <i class="ti-angle-double-up"></i>
		</a>

		<?php include('../includes/web_footer.php'); ?>
			
	  </div>
	  <!-- Wrapper -->
	  
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
	google.charts.load('current', {'packages':['bar']});
	google.charts.load("current", {packages:["corechart"]});
	<?php
	if(isset($_SESSION["is_affliated"]) && $_SESSION["is_affliated"]==0)
	{
		?>
			google.charts.setOnLoadCallback(drawChart_1);
			google.charts.setOnLoadCallback(drawChart_2);
			google.charts.setOnLoadCallback(drawChart_3);
			google.charts.setOnLoadCallback(drawChart_4);
		<?php
	}
	else
	{
		?>
			google.charts.setOnLoadCallback(drawChart_3);
		<?php
	}
	?>

	function drawChart_1() {
		var data = google.visualization.arrayToDataTable([
			['Leads', 'Total Assigned', 'Generated', 'Working', 'Approved By Sol','Leads With Sol'],
			<?php echo trim($arr_1,',') ?>
		  ]);

		var options = {
		  chart: {
			title: 'Overall Agent Performance',
			subtitle: 'Leads Total',
		  },
		  bars: 'vertical'
		};

		var chart = new google.charts.Bar(document.getElementById('chart_1'));
		chart.draw(data, google.charts.Bar.convertOptions(options));
	}
	
	function drawChart_2() {
		var data = google.visualization.arrayToDataTable([
			['Leads', 'Total Assigned', 'Working', 'Approved By Sol','Leads With Sol'],
			<?php echo trim($arr_2,',') ?>
		  ]);

		var options = {
		  chart: {
			title: 'Overall Team Lead Performance',
			subtitle: 'Leads Total',
		  },
		  bars: 'vertical'
		};

		var chart = new google.charts.Bar(document.getElementById('chart_2'));
		chart.draw(data, google.charts.Bar.convertOptions(options));
	}
	
	function drawChart_3() {
        var data = google.visualization.arrayToDataTable(<?php echo trim($arr_3) ?>);

       var options = {
          title: 'Callcenter Report',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_3'));
        chart.draw(data, options);
      }
	  
	  function drawChart_4() {
        var data = google.visualization.arrayToDataTable(<?php echo trim($arr_4) ?>);
		var options = {
          title: 'Pending Leads Report',
          pieHole: 0.4,
        };
		
        var chart = new google.visualization.PieChart(document.getElementById('chart_4'));
        chart.draw(data, options);
      }
	</script>
	</body>
</html>
