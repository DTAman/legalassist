<?php
include('../includes/basic_auth.php'); 

$p=1;
$pagecount=1;
$last_page=1;
$first_page=0;
$offset=0;
$condition='';
if($_SESSION['userType']!=$MSW)
{
	$condition=" and ((client_status!='IP' && client_status!='AP' &&  client_status!='SI') or client_status is NULL) ";
}

/********************************************************************************************/
if($_SESSION['userType']==$CA || $_SESSION['userType']==$CTL)
{
	$datamaster = $mysqli->prepare("SELECT callcenter_id FROM tl_and_agents where uid = ?");
	$datamaster->bind_param('i',$_SESSION['userId']);
	$datamaster->execute();
	$datamaster->bind_result($u_cid);
	$datamaster->fetch();
	$datamaster->close();
}
else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
{
	$u_cid = $_SESSION['userId'];
}
/********************************************************************************************/

$team_lead_array = array();
$agents_array = array();
$client_array = array();
$callcenter_array = array();
$verifier_array = array();

if(isset($u_cid))
{
	$getAgents = getUsersofCallcenter($u_cid,$CA);
	$getTeamLeads = getUsersofCallcenter($u_cid,$CTL);
}
$getClients = getClients();
$getCallcenters = getCallcenters('');
$getVerifiers = getVerifiers('');

if(isset($u_cid))
{
	while($tlrow = mysqli_fetch_object($getTeamLeads))
	{
		$team_lead_array[$tlrow->uid] = $tlrow->first_name.' '.$tlrow->last_name;
	}
	while($arow = mysqli_fetch_object($getAgents))
	{
		$agents_array[$arow->uid] = $arow->first_name.' '.$arow->last_name;
	}
}
while($vcrow = mysqli_fetch_object($getVerifiers))
{
	$verifier_array[$vcrow->uid] = $vcrow->verifier_name;
}
while($clientrow = mysqli_fetch_object($getClients))
{
	$client_array[$clientrow->uid] = $clientrow->company_name;
}
while($ccrow = mysqli_fetch_object($getCallcenters))
{
	$callcenter_array[$ccrow->uid] = $ccrow->centre_name;
}


if($_SESSION['userType']!=$MSW)
{
	$condition.=" and admin_assigned_directly=0 ";
}
	
$usertype = $_SESSION["userType"];

if(isset($_POST['page_skip']))
{
	$offset=($_POST['page_skip']-1)*$limit;
	$n = $offset+1;
}
else
{
	$_POST['page_skip']=0;
}

$linked_ids_con = $condition;

if(isset($_POST['ddlVerifier']) && trim($_POST['ddlVerifier'])!=null)
{
	$condition.=" AND assigned_verifier =  ".$_POST['ddlVerifier'];
}

if(isset($_POST['chckCheckList']) && count($_POST['chckCheckList'])>0)
{
	foreach($_POST['chckCheckList'] as $xkey)
	{
		$condition.=" and checklist".$xkey." = 1 ";
	}
}

if(trim($_POST['txtSearch'])!=null)
{
	$condition.=" and (phone like '%{$_POST['txtSearch']}%' or email like '%{$_POST['txtSearch']}%' or first_name like '%{$_POST['txtSearch']}%' or middle_name like '%{$_POST['txtSearch']}%' or last_name like '%{$_POST['txtSearch']}%') ";
}

if(trim($_POST['txtFromDate'])!=null)
{
	$create_date=date('Y-m-d',strtotime($_POST['txtFromDate']));
	$condition.=" and date(updation_date) >= '".get_timezone_offset_calender($create_date,'UTC',$_SESSION['userTimeZone'])."'";
}

if(trim($_POST['txtToDate'])!=null)
{
	$to_date=date('Y-m-d',strtotime($_POST['txtToDate']));
	$condition.=" and date(updation_date) <= '".get_timezone_offset_calender($to_date,'UTC',$_SESSION['userTimeZone'])."'";
}

if(trim($_POST['txtAccidentDateSearchFrom'])!=null && trim($_POST['txtAccidentDateSearchTo'])!=null)
{
	$ac_date_from=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchFrom']));
	$ac_date_to=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchTo']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) >= '".$ac_date_from."' and date(accident_date_time) <= '".$ac_date_to."' and lead_accidents.status=1)) ";
}
else if(trim($_POST['txtAccidentDateSearchFrom'])!=null)
{
	$ac_date_from=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchFrom']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) >= '".$ac_date_from."' and lead_accidents.status=1)) ";
}
else if(trim($_POST['txtAccidentDateSearchTo'])!=null)
{
	$ac_date_to=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchTo']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) <= '".$ac_date_to."' and lead_accidents.status=1)) ";
}
		
if(trim($_POST['ddlState'])!=null)
{
	$condition.=" and leads.state_id = ".$_POST['ddlState'];
}

if(isset($_POST['ddlLCallcenters']) && trim($_POST['ddlLCallcenters'])!=null)
{
	$condition.=" and leads.callcenter_id = ".$_POST['ddlLCallcenters'];
}

if(isset($_POST['ddlClients']) && is_array($_POST['ddlClients']) && count(array_filter($_POST['ddlClients']))>0)
{
	$condition.=" and assigned_client in (".trim(implode(',',$_POST['ddlClients']),',').")";
}

$outcome_array = array();
$c1='';
$c2='';
if(isset($_POST['ddlLeadStatus']) && is_array($_POST['ddlLeadStatus']) && count($_POST['ddlLeadStatus'])>0)
{
	foreach($_POST['ddlLeadStatus'] as $ddlLeadStatus)
	{
		if(trim($ddlLeadStatus)!=null)
		{
			if($ddlLeadStatus=='DA')
			{
				$c2.=" or leads.outcome_type is null";
			}
			else
			{
				array_push($outcome_array,"'".$ddlLeadStatus."'");
			}
		}
	}
	
	if(count($outcome_array)>0)
	{
		$c1.=" leads.outcome_type in (".implode(',',$outcome_array).") ";
	}
		
	if(trim($c1)!=null || trim($c2)!=null)
	{
		$condition.=" and (".$c1.$c2.")";
	}
}

if(isset($_POST['ddlSubUrb']) && trim($_POST['ddlSubUrb'])!=null)
{
	$condition.=" and leads.suburb_id = ".$_POST['ddlSubUrb'];
}

/**********************************************************************************************/
if(isset($_POST['ddlAgentCheck']) && trim($_POST['ddlAgentCheck'])!=null)
{
	$condition.=" AND agent_assigned =  ".$_POST['ddlAgentCheck'] ." and callcenter_id = ".$u_cid;
}

if(isset($_POST['ddlTeamLeadCheck']) && trim($_POST['ddlTeamLeadCheck'])!=null)
{
	$condition.=" AND team_lead_assigned =  ".$_POST['ddlTeamLeadCheck'] ." and callcenter_id = ".$u_cid;
}
/**********************************************************************************************/
		
if(trim($_POST['ddlSortBy'])!=null)
{
	$orderby = $_POST['ddlSortBy'];
}

$outcome_array = array();
$c1='';
$c2='';
if(isset($_POST['ddlLeadStatus']) && is_array($_POST['ddlLeadStatus']) && count(array_filter($_POST['ddlLeadStatus']))>0)
{
	foreach($_POST['ddlLeadStatus'] as $ddlLeadStatus)
	{
		if(trim($ddlLeadStatus)!=null)
		{
			array_push($outcome_array,"'".$ddlLeadStatus."'");
		}
	}
	
	if(count($outcome_array)>0)
	{
		$c1.=" leads.outcome_type in (".implode(',',$outcome_array).") ";
	}
		
	if(trim($c1)!=null || trim($c2)!=null)
	{
		$condition.=" and (".$c1.$c2.")";
	}
}

if(trim($_POST['ddlSortBy'])!=null)
{
	$orderby = $_POST['ddlSortBy'];
}

if($usertype=='CA')
{
	$condition.=" AND assigned_usertype = 'CA' and agent_assigned =  ".$_SESSION['userId']."  AND callcenter_id = ".$u_cid;
	$linked_ids_con.=" AND assigned_usertype = 'CA' and agent_assigned =  ".$_SESSION['userId']."  AND callcenter_id = ".$u_cid;
}
else if($usertype=='VA')
{
	$condition.=" AND assigned_verifier =  ".$_SESSION['userId'];
	$condition.=" AND assigned_usertype = 'VA'";
	$linked_ids_con.=" AND assigned_verifier =  ".$_SESSION['userId'];
	$linked_ids_con.=" AND assigned_usertype = 'VA'";
}
else if($usertype=='CTL')
{	
	$condition.=" AND team_lead_assigned =  ".$_SESSION['userId']."  AND callcenter_id = ".$u_cid;
	$linked_ids_con.=" AND team_lead_assigned =  ".$_SESSION['userId']."  AND callcenter_id = ".$u_cid;
}
else if($usertype=='CCA')
{	
	if($_SESSION["is_affliated"]==1)
	{
		$condition.=" and leads.callcenter_id = ".$_SESSION['userId'];
		$linked_ids_con.=" and leads.callcenter_id = ".$_SESSION['userId'];
	}
	else
	{
		$condition.=" and leads.id in (select lead_id from lead_comments where lead_comments.comment_utype='CCA') and leads.callcenter_id = ".$_SESSION['userId'];
		$linked_ids_con.=" and leads.id in (select lead_id from lead_comments where lead_comments.comment_utype='CCA') and leads.callcenter_id = ".$_SESSION['userId'];
	}
	
}
else if($usertype=='C')
{
	$condition.=" and (client_status!='RJ' or client_status is NULL) and assigned_client=".$_SESSION['userId'];
	$linked_ids_con.=" and (client_status!='RJ' or client_status is NULL) and assigned_client=".$_SESSION['userId'];
}
else if($usertype=='MSW')
{
	$x_var = null;

	if(isset($_POST['ddlLeadStatus']) && count($_POST['ddlLeadStatus'])>0)
	{
		foreach($_POST['ddlLeadStatus'] as $ddlLeadStatus)
		{
			if(trim($ddlLeadStatus)!=null && $ddlLeadStatus=='DA')
			{
				$x_var.=" and leads.outcome_type is null and show_to_admin=1 ";
				break;
			}
		}
	}
	else
	{
		$x_var.=" or leads.outcome_type is null and show_to_admin=1 ";
	}
	
	// $condition.=" and ((leads.id in (select lead_id from lead_comments where lead_comments.comment_utype='MSW') and show_to_admin=1) ".$x_var.") ";
	// $linked_ids_con.=" and ((leads.id in (select lead_id from lead_comments where lead_comments.comment_utype='MSW') and show_to_admin=1) ".$x_var.") ";
	
	$condition.=" and ((reached_admin = 1 and show_to_admin=1) ".$x_var.") ";
	$linked_ids_con.=" and ((reached_admin = 1 and show_to_admin=1) ".$x_var.") ";
	
	
	$condition.=" and (msw_lead_type = '".$_POST['txtPP']."')";
}

if(isset($_POST['nextpage']) && trim($_POST['nextpage'])!=null)
{
	$pagecount = $_POST['nextpage'];
	$last_page = $_POST['nextpage'];
}

if(isset($_POST['firstpage']) && trim($_POST['firstpage'])!=null)
{		
	$pagecount = $_POST['firstpage']-($set_count-1);
	$last_page = $_POST['firstpage']-($set_count-1);
}

if(isset($_POST['firstpage1']) && trim($_POST['firstpage1'])!=null)
{
	$firstpage1 = trim($_POST['firstpage1']);
	
	$pagecount = $firstpage1;
	$last_page = $firstpage1;
}

if($usertype=='CCA' || $usertype=='CA' || $usertype=='CTL')
{
	if($usertype=='CCA')
	{
		$condition.=" and (shifted_by_usertype!='S_CCA' or shifted_by_usertype is NULL)";  //SHIFTED CALLCENTER
	}
	else if($usertype=='CTL')
	{
		$condition.=" and ((shifted_by_usertype!='S_CTL' and shifted_by_usertype!='S_CCA') or shifted_by_usertype is NULL)"; //SHIFTED TEAM LEAD
	}
	else if($usertype=='CA')
	{
		$condition.=" and (shifted_by_usertype!='S_CA' or shifted_by_usertype is NULL)";  //SHIFTED AGENT
	}
}

/********* AMANJOT ****************/
if(($usertype=='CCA' && $_SESSION['is_affliated']==0) || $usertype=='CTL' || $usertype=='CA')
{
	if($usertype=='CCA')
	{
		$condition.=" and (del_cc_id != ".$_SESSION['userId']." or del_cc_id is null)";
	}
	else if($usertype=='CTL')
	{
		$condition.=" and (del_tl_id != ".$_SESSION['userId']." or del_tl_id is null)";
	}
	else if($usertype=='CA')
	{
		$condition.=" and (del_ag_id != ".$_SESSION['userId']." or del_ag_id is null)";
	}
}
/********* AMANJOT ****************/

$queryCount= "select count(*) from leads left outer join states on states.id = leads.state_id where leads.is_deleted=0 ".$condition;
$queryResults = "select leads.*, leads.suburb_name as textbox_suburb,state_name,initials, 
(select count(*) from leads x where x.phone = leads.phone and leads.is_deleted=0) as count_exist_leads ,
(case when (master_lead_id is not null and master_lead_id > 0) 
 THEN
      (select group_concat(m.id SEPARATOR ',') from leads m where (m.master_lead_id = leads.master_lead_id or m.id = leads.master_lead_id) and m.id!= leads.id ".$linked_ids_con." and m.is_deleted=0)
 ELSE
      (select group_concat(m.id SEPARATOR ',') from leads m where m.master_lead_id=leads.id and m.id!=leads.id ".$linked_ids_con." and  m.is_deleted=0)
 END)
 as linked_ids
 from leads left outer join states on states.id = leads.state_id where leads.is_deleted=0 ".$condition." order by ".$orderby." desc limit ".$limit." offset ".$offset;

$totalCount = getFetchQuery($queryCount);
$totalResults = getQuery($queryResults);

if(mysqli_num_rows($totalResults)>0)
{
	?>
		<div class="hh5">
		<table class="table newTable table-bordered">
			<thead>
				<tr>
					<?php
					if($_SESSION['userType']==$MSW || $_SESSION['userType']==$CTL || ($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0))
					{
						?>
							<th class='text-center'>
								<p class="custom-checkbox m-0 text-center">
									<input type="checkbox" class="custom-control-input customCheck" onclick="selectAll()" id="chckSelectCheckBox" >
									<label class="custom-control-label" for="customCheck"></label>
								</p>
							</th>
						<?php
					}
					?>
					<?php
					if($_SESSION['userType']==$MSW)
					{
						?>
							<th class='text-center'  style="min-width:60px!important">
								<p class="custom-checkbox m-0 text-center">
									<input type="checkbox" class="custom-control-input customCheck" onclick="selectAllAssignVerifier()" id="chckSelectCheckBoxAssignVerifier" >
									<label class="custom-control-label" for="customCheck"></label>
								</p>
							</th>
						<?php
					}
					?>
					<th class='text-center width20'>Sr.No.</th>
					<th class="text-center width20">Id</th>
					<th>Name &amp; Details</th>
					<th>Details</th>
					<?php
					
					if(($_SESSION['userType']==$VA) || ($_SESSION['userType']==$MSW) || ($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0) || $_SESSION['userType']==$CTL)
					{
						?>
							<th class="widthCustom small">Users</th>
						<?php
					}

					if(($_SESSION['userType']!=$C) || ($_SESSION['userType']==$MSW || $_SESSION['userType']==$CCA  || $_SESSION['userType']==$C  || $_SESSION['userType']==$CTL))
					{
						?>
							<th class="widthCustom">Modified&nbsp;On</th>
						<?php
					}
					?>
				</tr>
			</thead>
			<tbody>
			<?php
			while($rc = mysqli_fetch_object($totalResults))
			{			
				$count_exist_leads = $rc->count_exist_leads;
				if($_SESSION['userType']!=$MSW)
				{
					$count_exist_leads=0;
				}
				?>
					<tr id="delete<?php echo $rc->id ?>" class="<?php echo $count_exist_leads>1?'bg-info-light':''; ?>">
						<?php
						if($usertype==$MSW && $rc->assigned_client==null)
						{
							?>
								<td class="text-center"><input type="checkbox" value="<?php echo $rc->id ?>" name="chckDeleteMultiple[]" id="chckDeleteMultiple<?php echo $rc->id ?>" class='delete_checkbox'/></td>
							<?php
						}
						else if($usertype==$MSW)
						{
							?>
								<td></td>
							<?php
						}
						else if($usertype==$CTL)
						{
							if($rc->assigned_usertype==$CA)
							{
								?>
									<td class="text-center"><input type="checkbox" value="<?php echo $rc->id ?>" name="chckDeleteMultiple[]" id="chckDeleteMultiple<?php echo $rc->id ?>" class='delete_checkbox'/></td>
								<?php
							}
							else
							{
								?>
									<td></td>
								<?php
							}
						}
						else if($usertype==$CCA && $_SESSION['is_affliated']==0)
						{
							if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0 && ($rc->assigned_usertype!=$C && $rc->assigned_usertype!=$MSW && $rc->assigned_usertype!=$CCA))
							{
								?>
									<td class="text-center"><input type="checkbox" value="<?php echo $rc->id ?>" name="chckDeleteMultiple[]" id="chckDeleteMultiple<?php echo $rc->id ?>" class='delete_checkbox'/></td>
								<?php
							}
							else
							{
								?>
									<td></td>
								<?php
							}
						}
						
						if($_SESSION['userType']==$MSW)
						{
							if(($rc->client_status==null || $rc->client_status=='FI' || $rc->client_status=='SU' || $rc->client_status=='RJ') && $usertype==$MSW && $rc->assigned_usertype==$MSW)
							{
								?>
									<td class="text-center" style="min-width:60px!important"><input type="checkbox" value="<?php echo $rc->id ?>" name="chckAssignVerifierMultiple[]" id="chckAssignVerifierMultiple<?php echo $rc->id ?>" class='chckAssignVerifierMultiple'/></td>
								<?php
							}
							else
							{
								?>
									<td style="min-width:60px!important"></td>
								<?php
							}
						}
						?>
						
						<td class='text-center width20'><?php echo $n ?></td>
						<td class='text-center'>
							<?php
							if($_SESSION['userType']=='MSW' || $_SESSION['userType']=='VA'  || $_SESSION['userType']=='CCA')
							{
								?>
									<div>
										<div class="checklist_boxes <?php echo $rc->checklist1!=1?"checklist_boxes_no":'' ?>"></div>
										<div class="checklist_boxes <?php echo $rc->checklist2!=1?"checklist_boxes_no":'' ?>"></div>
										<div class="checklist_boxes <?php echo $rc->checklist3!=1?"checklist_boxes_no":'' ?>"></div>
									</div>
								<?php
							}
							?>
							<?php echo $rc->id ?>
							<?php
								$str='';
								$res = $rc->linked_ids;
								$resr = explode(',',$res);
								foreach($resr as $kkey)
								{
									$str.=', <a class="text-danger" target="_blank" href="edit-lead-each.php?u='.$kkey.'">'.$kkey.'</a>';
								}
								
								echo '<br/>'.trim($str,','); 
							?>
						</td>

						<td style="max-width:300px">
							
							<?php echo $rc->first_name.' '.$rc->middle_name.' '.$rc->last_name; ?><br/>
							<?php echo $rc->phone ?> <?php echo $rc->initials!=null?"<span style='color:red'>(".$rc->initials.")</span>":'' ?><br/>
							
							<?php
							if($_SESSION['userType']=='CA' || $_SESSION['userType']=='CTL' )
							{
								echo trim($rc->email)!=null?$rc->email.'<br/>':'';
								echo trim($rc->address)!=null?$rc->address.'<br/>':'';
							}
							if($rc->last_accident_type!=null)
							{
								if($rc->last_accident_na!=1)
								{
									echo $rc->last_accident_type." <br/><span style='color:red'>(".date('d-M-Y',strtotime($rc->latest_accident_dt)).")</span>";
								}
								else
								{
									echo $rc->last_accident_type." <br/><span style='color:red'>(".date('M-Y',strtotime($rc->latest_accident_dt)).")</span>";
								}
							}
							?>
						</td>
						
						<td style="max-width:300px">
							<?php
							if($_SESSION['userType']==$MSW || $_SESSION['userType']==$CCA  || $_SESSION['userType']==$C  || $_SESSION['userType']==$CTL)
							{
								?>
									<div class="bg-danger-light">
										<?php echo $rc->outcome; ?>
									</div>
								<?php
							}
							
							if($_SESSION['userType']!='CA' && $_SESSION['userType']!='CTL' )
							{
								?>

								<div>
									<?php
										$getSignUpDetails = str_replace('"','',$rc->latest_signup_detail);
										$x='';
										if(trim($getSignUpDetails)!=null)
										{
											if($rc->latest_signuptype=='E' || $rc->latest_signuptype=='P')
											{
												$x= $rc->latest_signuptype=='E'?'Email':'Post';
												$x.=", <span style='color:red'>Call: ".date("d M,Y",strtotime($rc->latest_signup_datetime))." (".$rc->latest_signup_timezone.")</span>, Comments: ".$getSignUpDetails; 
												
												$x = '<p data-toggle="tooltip" data-original-title="'.trim(strip_tags($x)).'">'.$x.'</p>';
											}
											else if($rc->latest_signuptype=='O' || $rc->latest_signuptype=='H')
											{
												$x = $rc->latest_signuptype=='O'?'Office Signup':'Home Signup';
												$x.=", <span style='color:red'>Call: ".date("d M,Y H:i",strtotime($rc->latest_signup_datetime))." (".$rc->latest_signup_timezone.")</span>, Comments: ".$getSignUpDetails; 
												
												$x = '<p data-toggle="tooltip" data-original-title="'.trim(strip_tags($x)).'">'.$x.'</p>';
											}
											
											echo '<button id="'.$rc->id.'signup" class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" a_val="'.strip_tags(preg_replace('/\s+/', ' ',$x)).'" onclick="sign_val_copy('.$rc->id.')"></button>'.$x;
										}
									?>
								</div>
								<?php
							}

							// if($_SESSION['userType']==$MSW || $_SESSION['userType']==$VA || $_SESSION['userType']==$CCA)
							if($_SESSION['userType']==$MSW || $_SESSION['userType']==$VA || $_SESSION['userType']==$CCA || $_SESSION['userType']==$CTL || $_SESSION['userType']==$CA)
							{
								?>
								<div>
									<?php
										$getCallBackDetails = str_replace('"','',$rc->latest_calback);
										$x='';
										if(trim($rc->latest_callbacktype)!=null)
										{
											$x = $rc->latest_callbacktype=='N'?'No Answer(Callback)':'Callback Requested';
											$x.=", <span style='color:red'>Call: ".date("d M,Y H:i",strtotime($rc->latest_callback_datetime))." (".$rc->latest_callback_timezone.")</span>, Comments: ".$getCallBackDetails; 
											$x = '<p data-toggle="tooltip" data-original-title="'.trim(strip_tags($x)).'">'.$x.'</p>';
											echo '<button id="'.$rc->id.'callback" class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" a_val="'.strip_tags(preg_replace('/\s+/', ' ',$x)).'" onclick="callback_val_copy('.$rc->id.')"></button>'.$x;
										}
									?>
								</div>
								<?php
							}
							
							/* if($_SESSION['userType']!=$C)
							{
								$latest_sol_notes = str_replace('"','',$rc->latest_sol_notes);
								if(trim($latest_sol_notes)!=null)
								{
									?>
										<div>
											<button class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" onclick="copyClipbord('<?php echo preg_replace('/\s+/', ' ',$latest_sol_notes) ?>')"></button>
											<p data-toggle="tooltip" data-original-title="<?php echo $latest_sol_notes ?>">Sol Notes: <?php echo $latest_sol_notes ?></p>
										</div>
									<?php
								}
							} */
							
							/* if($_SESSION['userType']==$C || $_SESSION['userType']==$CCA || $_SESSION['userType']==$MSW || $_SESSION['userType']==$CTL)
							{
								$getNotesDetails = str_replace('"','',$rc->latest_notes) ;
								if(trim($getNotesDetails)!=null)
								{
									?>
										<div>
											<button class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" onclick="copyClipbord('<?php echo preg_replace('/\s+/', ' ', $getNotesDetails) ?>')"></button>
											<p data-toggle="tooltip" data-original-title="<?php echo $getNotesDetails ?>">Admin Notes: <?php echo $getNotesDetails ?></p>
										</div>
									<?php
								}
							} */
							$com = str_replace('"','',$rc->last_comment);
							if(trim($com)!=null)
							{
								?>
								<div>
									<button class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" onclick="copyClipbord('<?php echo preg_replace('/\s+/', ' ', $com) ?>')"></button>
									
									<p data-toggle="tooltip" data-original-title="<?php echo $com ?>">
									<span style="color:#1db9aa">	
									<?php
									if($_SESSION['userType']==$MSW && trim($rc->latest_comment_usertype_to)!=null)
									{
										?>
											<?php echo getCommentUserType($rc->latest_comment_usertype_from) ?> to <?php echo getCommentUserType($rc->latest_comment_usertype_to) ?>
										<?php
									}
									?>
									Updates:</span> <?php echo $com  ?></p>
									<?php
									/* if($_SESSION['userType']==$CCA || $_SESSION['userType']==$MSW)
									{
										?>
											<span style='color:red'><?php echo $rc->msw_to_cc_reject_reason ?></span>
										<?php
									} */
									?>
								<div>
								<?php
							}
							if(trim($rc->invoice_number)!=null && $_SESSION['userType']==$MSW)
							{
								echo '<div class="bg-info-light pl-3">'.$rc->invoice_number.'<br>'.$rc->invoice_remarks.'</div>';
							}
							?>
						</td>
						
						<?php
						if(($_SESSION['userType']==$VA) || ($_SESSION['userType']==$MSW) || ($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0) || $_SESSION['userType']==$CTL)
						{
							?>
								<td class='widthCustom'>
									<?php
										if($_SESSION['userType']==$MSW)
										{
											echo isset($callcenter_array[$rc->callcenter_id])?'<span class="a_red">CC</span>: '.$callcenter_array[$rc->callcenter_id].'<br/>':'';
											echo isset($client_array[$rc->assigned_client])?'<span class="a_red">Sol</span>: '.$client_array[$rc->assigned_client].'<br/>':'';
											echo isset($verifier_array[$rc->assigned_verifier])?'<span class="a_red">'.($rc->is_previous_verifier==1?'Previous ':'').'Verifier</span>: '.$verifier_array[$rc->assigned_verifier].'<br/>':'';
										}
										if($_SESSION['userType']==$VA)
										{
											echo isset($verifier_array[$rc->assigned_verifier])?'<span class="a_red">'.($rc->is_previous_verifier==1?'Previous ':'').'Verifier</span>: '.$verifier_array[$rc->assigned_verifier].'<br/>':'';
										}
										if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
										{
											echo isset($client_array[$rc->assigned_client])?'<span class="a_red">Sol</span>: '.$client_array[$rc->assigned_client].'<br/>':'';
											echo isset($team_lead_array[$rc->team_lead_assigned])?'<span class="a_red">TL</span>: '.$team_lead_array[$rc->team_lead_assigned].'<br/>':'';
											echo isset($agents_array[$rc->agent_assigned])?'<span class="a_red">Agent</span>: '.$agents_array[$rc->agent_assigned].'<br/>':'';
										}
										if($_SESSION['userType']==$CTL)
										{
											echo isset($client_array[$rc->assigned_client])?'<span class="a_red">Sol</span>: '.$client_array[$rc->assigned_client].'<br/>':'';
											echo isset($agents_array[$rc->agent_assigned])?'<span class="a_red">Agent</span>: '.$agents_array[$rc->agent_assigned].'<br/>':'';
										}
									?>
								</td>
							<?php
						}
						?>
						
						
						<td>
						<?php
						if($_SESSION['userType']!=$C)
						{
							switch($_SESSION['userType'])
							{
								case 'C': $d = $rc->assign_client;break;
								case 'CCA': $d = $rc->assign_n_callcenter;
											if($_SESSION['is_affliated']==1)
											{
												$d = $rc->assign_affliated;
											}
											break;
								case 'CTL': $d = $rc->assign_teamlead;break;
								case 'CA': $d = $rc->assign_agent;break;
								case 'MSW': $d = $rc->assign_admin;break;
								case 'VA': $d = $rc->assign_verifier;break;
								default: $d='';
							}
							if($d!=null)	
							{
								echo "<span style='display:block' class='a_red'>Assigned: </span>".get_timezone_offset(date('Y-m-d H:i:s',strtotime($d)),'UTC',$_SESSION['userTimeZone']).'<br/>';
							}
						}
						if($_SESSION['userType']==$CA || $_SESSION['userType']==$VA || $_SESSION['userType']==$MSW || $_SESSION['userType']==$CCA  || $_SESSION['userType']==$C  || $_SESSION['userType']==$CTL)
						{
							echo "<span style='display:block' class='a_red'>Modified: </span>".get_timezone_offset(date('Y-m-d H:i:s',strtotime($rc->updation_date)),'UTC',$_SESSION['userTimeZone']);
						}
						?>
						</td>
						<?php
						/* if($_SESSION['userType']==$MSW || $_SESSION['userType']==$CCA  || $_SESSION['userType']==$C  || $_SESSION['userType']==$CTL)
						{
							?>
								<td style="max-width:500px">
									<?php echo $rc->outcome ?>
								</td>
							<?php
						} */
						?>
						</tr>
						<tr class="bg-warning-light"  id="deletex<?php echo $rc->id ?>">
						<td class='text-center' colspan='<?php echo $_SESSION['userType']=='CA'?2:($_SESSION['userType']=='MSW'?4:3)?>'>
							<?php
								if(($rc->client_status==null || $rc->client_status=='FI' || $rc->client_status=='SU' || $rc->client_status=='RJ') && $usertype==$MSW && $rc->assigned_usertype==$MSW)
								{
									if($rc->checklist1==1 && $rc->checklist2==1 && $rc->checklist3==1)
									{
										?>
											<input attr_lead='<?php echo $rc->id ?>' attr_type='1' type="button" class="btn-success ml-0 mr-3 mt-2 callPopUp" value="Assign To Sol" />
										<?php
									}
									?>
										<input attr_lead='<?php echo $rc->id ?>' attr_type='2' type="button" class="btn-danger ml-0 mr-3 mt-2 callPopUp" value="Assign To Callcenter" />
										<input attr_lead='<?php echo $rc->id ?>' attr_type='30' type="button" class="btn-dark ml-0 mr-3 mt-2 callPopUp" value="Assign To Verifier" />
									<?php
									
									if($rc->msw_lead_type=='f')
									{
										?>
											<input onclick = "changeLeadWorkingStatus('w',<?php echo $rc->id ?>)" type="button" class="btn-warning callPopUpWP ml-0 mr-3 mt-2" value="Move To Working Portal" />
										<?php
									}
									
								}
								else if($rc->client_status==null && $usertype==$CCA && isset($_SESSION["is_affliated"]) && $_SESSION["is_affliated"]==1 && ($rc->assigned_usertype==null || $rc->assigned_usertype==$CCA))
								{
									?>
										<input attr_lead='<?php echo $rc->id ?>' attr_type='3' type="button" class="btn-success ml-0 mr-3 mt-2 callPopUp" value="Assign To Admin" />
									<?php
								}
								else if($rc->client_status==null && $usertype==$CCA && $rc->assigned_usertype==$CCA)
								{
									?>
										<input attr_lead='<?php echo $rc->id ?>' attr_type='3' type="button" class="btn-success ml-0 mr-3 mt-2 callPopUp" value="Assign To Admin" />
										<input attr_lead='<?php echo $rc->id ?>' attr_type='4' type="button" class="btn-warning ml-0 mr-3 mt-2 callPopUp" value="Assign To TL" />
										<?php
								}
								else if($rc->client_status==null && $usertype==$CTL && $rc->assigned_usertype==$CTL)
								{
									?>																		
										<input attr_lead='<?php echo $rc->id ?>' attr_type='5' type="button" class="btn-success ml-0 mr-3 mt-2 callPopUp" value="Assign To Callcenter" />
										
										<input attr_lead='<?php echo $rc->id ?>' attr_type='7' type="button" class="btn-danger ml-0 mr-3 mt-2 callPopUp" value="Assign To Agent" />
									<?php
								}
								else if($rc->client_status==null && $usertype==$CA)
								{
									?>	
										<input attr_lead='<?php echo $rc->id ?>' attr_type='6' type="button" class="btn-success ml-0 mr-3 mt-2 callPopUp" value="Assign To Team Lead" />
									<?php
								}
								else if(($rc->client_status==null || $rc->client_status=='LT' || $rc->client_status=='FI' || $rc->client_status=='SU') && $usertype==$C && $rc->assigned_usertype==$C)
								{
									
									?>
										<input attr_lead='<?php echo $rc->id ?>' type="button" class="btn-primary ml-0 mr-3 mt-2 callPopUpFurtherInvestigation" value="Sol Notes" />
										
										<?php
										// $sbtn = getSignupButtonName($rc->id)>0?'Rearrange SignUp':'SignUp';
										$sbtn = $rc->latest_signup_datetime!=null?'Rearrange SignUp':'SignUp';
										// $getHaveCalledButtonName = getHaveCalledButtonName($rc->id); 
										$getHaveCalledButtonName = $rc->latest_spoken_to_client;
										
										if($getHaveCalledButtonName>0)
										{
											?>
												<input attr_lead='<?php echo $rc->id ?>' type="button" class="btn-warning ml-0 mr-3 mt-2 callPopUpSignUp" value="<?php echo $sbtn ?>" />
												<?php
												if($rc->client_status=='SU' && $usertype==$C)
												{
													?>
														<input attr_lead='<?php echo $rc->id ?>' attr_type='9' type="button" class="btn-success ml-0 mr-3 mt-2 callPopUpApprove" value="Approve" />
													<?php
												}
												// else if(trim(getSignUpDetails($rc->id , $_SESSION['userId']))!=null)
												else if(trim($rc->latest_signup_detail)!=null)
												{
													?>
														<input attr_lead='<?php echo $rc->id ?>' attr_type='9' type="button" class="btn-success ml-0 mr-3 mt-2 callPopUpApprove" value="Approve" />
													<?php
												}
										}
										else
										{
											?>
												<input attr_lead='<?php echo $rc->id ?>' attr_type='22' type="button" class="btn-info ml-0 mr-3 mt-2 callPopUpClient" value="Have you called client?" />
											<?php
										}
										?>
										
										
										<input attr_lead='<?php echo $rc->id ?>' attr_type='10' type="button" class="btn-danger ml-0 mr-3 mt-2 callPopUp" value="Reject" />
									<?php	
								}
								else if(($rc->client_status=='AP' || $rc->client_status=='IP' || $rc->client_status=='SI') && $usertype==$MSW)
								{
									?>
										<input attr_lead_inv='<?php echo $rc->invoice_number ?>' attr_lead_details='<?php echo $rc->invoice_remarks ?>' attr_lead='<?php echo $rc->id ?>' attr_type='11' type="button" class="btn-success ml-0 mr-3 mt-2 callPopUpSendInvoice" value="Invoice Details" />
									<?php
								}
								else if($rc->client_status=='SI' && $usertype==$C)
								{
									?>
										<input attr_lead='<?php echo $rc->id ?>' attr_type='25' type="button" class="btn-danger ml-0 mr-3 mt-2 callPopUpInvoicePaid" value="Invoice Paid?" />
									<?php
								}
								else if($usertype==$VA && $rc->assigned_usertype==$VA)
								{
									?>
										<input attr_lead='<?php echo $rc->id ?>' attr_type='31' type="button" class="btn-success ml-0 mr-3 mt-2 callPopUp" value="Approve Lead" />
										<input attr_lead='<?php echo $rc->id ?>' attr_type='32' type="button" class="btn-danger ml-0 mr-3 mt-2 callPopUp" value="Reject Lead" />
									<?php
								}
								else if($rc->client_status=='AP' && $usertype==$C)
								{
									// echo "Lead Approved. <br/>Waiting for invoice <br/>from admin.";
									echo "Lead Approved.";
								}
								/* else if($rc->client_status=='SI' && $usertype==$MSW)
								{
									echo "Invoice Sent &amp; waiting for invoice payment.";
								}
								else if($rc->client_status=='IP')
								{
									//IP is invoice paid
									echo "Lead Finished";
								} */
								
								/* if(trim($rc->assigned_client)!=null && $_SESSION['userType']==$MSW)
								{
									?>
										<input attr_lead='<?php echo $rc->id ?>' attr_type='23' type="button" class="btn-danger ml-0 mr-3 mt-2 rejectApproval" value="Pull from Sol" />
									<?php
								} */
								
								if($_SESSION['userType']==$MSW)
								{
									$assigned_usertype = $rc->assigned_usertype;
									$txtBtn='';
									if(trim($assigned_usertype)==$C && trim($rc->assigned_client)!=null && trim($rc->client_status)!='AP' && trim($rc->client_status)!='SI' && trim($rc->client_status)!='IP')
									{
										$txtBtn = 'Pull From Sol';
									}
									else if(trim($assigned_usertype)==$VA)
									{
										$txtBtn = 'Pull From Verifier';
									}
									else if(trim($assigned_usertype)==$CCA)
									{
										$txtBtn = 'Pull From CallCenter';
									}
									else if(trim($assigned_usertype)==$CA)
									{
										$txtBtn = 'Pull From Agent';
									}
									else if(trim($assigned_usertype)==$CTL)
									{
										$txtBtn = 'Pulled From Team Lead';
									}
									
									if(trim($txtBtn)!=null)
									{
										?>
											<input <?php echo trim($rc->assigned_client)!=null && trim($rc->client_status)!='AP' && trim($rc->client_status)!='SI' && trim($rc->client_status)!='IP'?1:0 ?> onclick="pull_from_anyone(<?php echo $rc->id ?>, '<?php echo $rc->assigned_usertype ?>')" type="button" class="btn-info pull_from_anyone ml-0 mr-3 mt-2" value="<?php echo $txtBtn ?>" />
										<?php
									}
								} 
							?>
							</td>
						
						
						<td class='text-center' colspan='<?php echo $_SESSION['userType']=='CA'?3:($_SESSION['userType']=='MSW'?4:4)?>'>
								<div class="icnos">
							<?php
								// if(($usertype!=$C && ($_SESSION['userType']==$rc->assigned_usertype)) || $_SESSION['userType']=='MSW')
								if($_SESSION['userType']==$rc->assigned_usertype || $_SESSION['userType']=='MSW' || ($rc->assigned_usertype==null) || $_SESSION['userType']=='C')
								{
									?>
										<a target='_blank' href="edit-lead-each.php?u=<?php echo $rc->id ?>" class="settings icn" title="" data-toggle="tooltip" data-original-title="Edit"><i class="ti-pencil"></i></a>
										<?php
										if($usertype==$MSW && $rc->assigned_client==null)
										{
											?>
												<a href="javascript:void(0)" onclick='deleteLead(<?php echo $rc->id ?>)'  class="delete icn d-flex" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
											<?php
										}
								}
							?>
							<a target='_blank' class="text-success icn" href="view-each-lead.php?u=<?php echo $rc->id ?>" title="" data-toggle="tooltip" data-original-title="View"><i class="ti-eye"></i></a>
							<?php
								// if($usertype==$CTL || $usertype==$MSW || ($usertype==$CCA && $_SESSION['is_affliated']==0))
								if($usertype==$CTL || $usertype==$MSW || $usertype==$CCA || $usertype==$VA)
								{
									?>
									<!--
									<a target='_blank' class="text-success icn" href="view-logs.php?u=<?php echo $rc->id ?>" title="" data-toggle="tooltip" data-original-title="View Logs"><i class="ti-bar-chart-alt"></i></a>
									-->
									<a target='_blank' class="text-success icn" onclick="view_logs(<?php echo $rc->id ?>)" title="" data-toggle="tooltip" data-original-title="View Logs"><i class="ti-bar-chart-alt"></i></a>
									<?php
								}
							if($_SESSION['userType']==$VA || $_SESSION['userType']==$CTL || $_SESSION['userType']==$MSW || $_SESSION['userType']==$C || $_SESSION['userType']==$CCA)
							{
								?>
									<!--<a class="text-danger download_lead" l_id = '<?php echo $rc->id ?>' title="" data-toggle="tooltip" data-original-title="Download Lead" aria-describedby="tooltip600250">Download Lead</a>-->
									<a target='_blank' href='ajaxFullLead.php?u=<?php echo $rc->id ?>' class="text-danger icn" l_id = '<?php echo $rc->id ?>' title="" data-toggle="tooltip" data-original-title="Print Lead"><i class="ti-printer"></i></a>
								<?php
							}
							
							if($_SESSION['userType']!=$VA && $_SESSION['userType']!=$C && $_SESSION['userType']!=$CTL && $rc->master_lead_id==null)
							{
								?>
									<a onclick='copyLeadPopUp(<?php echo $rc->id ?>)' href='javascript:void(0)' class="text-primary icn" title="" data-toggle="tooltip" data-original-title="Create New Lead"><i class="fa fa-copy"></i></a>
								<?php
							}
							
							if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0 && ($rc->assigned_usertype!=$C && $rc->assigned_usertype!=$MSW && $rc->assigned_usertype!=$CCA))
							{
								?>
									<a href="javascript:void(0)" onclick='deleteLeadView(<?php echo $rc->id ?>)'  class="delete icn d-flex" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
								<?php
							}
							
							if($_SESSION['userType']==$CTL && ($rc->assigned_usertype==$CA))
							{
								?>
									<a href="javascript:void(0)" onclick='deleteLeadView(<?php echo $rc->id ?>)'  class="delete icn d-flex" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
								<?php
							}
							
							if($_SESSION['userType']==$CA && ($rc->assigned_usertype==$CA || $rc->assigned_usertype==null) && $rc->agent_assigned==$_SESSION['userId'])
							{
								?>
									<a href="javascript:void(0)" onclick='deleteLeadView(<?php echo $rc->id ?>)'  class="delete icn d-flex" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
								<?php
							}
							?>
							<a href="javascript:void(0)" onclick='setAttachments(<?php echo $rc->id ?>)'  class="text-primary icn" title="" data-toggle="tooltip" data-original-title="Attachments"><i class="ti-download"></i></a>
							<?php
							if($_SESSION['userType']==$MSW || $_SESSION['userType']==$VA)
							{
								?>
									<a href="javascript:void(0)" onclick='setCheckList(<?php echo $rc->id ?>)'  class="text-success icn" title="" data-toggle="tooltip" data-original-title="CheckList"><i class="ti-check-box"></i></a>
								<?php
							}
							// if(($_SESSION['userType']==$MSW || $_SESSION['userType']==$VA || $_SESSION['userType']==$CCA) && ($_SESSION['userType'] == $rc->assigned_usertype))
							if(($_SESSION['userType']==$CA || $_SESSION['userType']==$CTL || $_SESSION['userType']==$MSW || $_SESSION['userType']==$VA || $_SESSION['userType']==$CCA) && ($_SESSION['userType'] == $rc->assigned_usertype))
							{
								?>
									<a attr_lead='<?php echo $rc->id ?>' href="javascript:void(0)" class="text-success icn callPopUpCallback" title="" data-toggle="tooltip" data-original-title="Call Back"><i class="fa fa-assistive-listening-systems"></i></a>
								<?php
							}
							if($_SESSION['userType']==$MSW || $_SESSION['userType']==$VA || $_SESSION['userType']==$CCA)
							{
								?>
									<a attr_lead='<?php echo $rc->id ?>' attr_lead_email='<?php echo $rc->email ?>' href="javascript:void(0)" class="text-success icn callPopUpCallbackEmail" title="" data-toggle="tooltip" data-original-title="Email"><i class="fa fa-envelope-open-o"></i></a>
								<?php
							}
							if($_SESSION['userType']==$MSW || $_SESSION['userType']==$VA)
							{
								?>
									<a attr_lead='<?php echo $rc->id ?>' attr_lead_phone='<?php echo $rc->phone ?>' href="javascript:void(0)" class="text-success icn callPopUpCallbackPhone" title="" data-toggle="tooltip" data-original-title="Send SMS"><i class="fa fa-commenting-o"></i></a>
								<?php
							}
							if($_SESSION['userType']==$MSW || $_SESSION['userType']==$VA || $_SESSION['userType']==$CCA)
							{
								?>
									<a onclick='openNotes(<?php echo $rc->id ?>)' href="javascript:void(0)" class="text-info icn openNotes" title="" data-toggle="tooltip" data-original-title="Notes"><i class="fa fa-book"></i></a>
								<?php
							}
							if($_SESSION['userType']==$MSW || $_SESSION['userType']==$VA)
							{
								?>
									<a target="_blank" href="zoiper://<?php echo '61'.ltrim($rc->phone,'0') ?>" class="text-danger icn callZoiper" title="" data-toggle="tooltip" data-original-title="Zoiper"><i class="fa fa-phone"></i></a>
								<?php
							}
							?>
							</div>
						</td>
						
					</tr>
				<?php
				$n++;
			}
			?>
			</tbody>
		</table>
		</div>
		<?php include('../includes/ajax_pagination.php'); ?>
		
	</div>
	<?php
}
else
{
	?>
		<?php include('../includes/norows.php'); ?>
	<?php
}
?>