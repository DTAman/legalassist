<?php
	include('../includes/basic_auth.php');
	
	if(isset($_POST["btnResetPassword"]))
	{
		if(trim($_POST["txtOldPassword"])!=null && trim($_POST["txtNewPassword"])!=null && trim($_POST["txtConfirmNewPassword"])!=null && $_POST["txtConfirmNewPassword"]==$_POST["txtNewPassword"])
		{
			$loginmaster = $mysqli->prepare("select password from login_master where uid=?");
			$loginmaster->bind_param("i",$_SESSION['userId']);
			$loginmaster->execute();
			$loginmaster->bind_result($sign_pass);
			$loginmaster->fetch();
			$loginmaster->close();	

			if($sign_pass==create_password($_POST['txtOldPassword']))
			{
				$password=create_password($_POST["txtConfirmNewPassword"]);
				
				$loginmaster = $mysqli->prepare("update login_master set password=? where uid=?");
				$loginmaster->bind_param("si",$password,$_SESSION['userId']);
				$loginmaster->execute();
				$loginmaster->close();
				
				$_SESSION['success']='Password has been changed successfully.';
				
				$_POST=array();
			}
			else
			{
				$_SESSION['error']="Current password does not match.";
			}
		}
		else
		{
			$_SESSION['error']="Enter all the required fields.";
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>

	<body class="fixed-nav sticky-footer" id="page-top">
	
		<?php include('../includes/navigation.php'); ?>
	  
		<div class="content-wrapper">

			<div class="container-fluid" id="personal-detail">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Change Password</h4>
					</div>
				</div>
				
				<div class="row">		
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
							<div class="card-body">
								<form name="frmChangePassword" id="frmChangePassword" method="post" class="chnge-paswd" action="#">
									<div class="row">
										<div class="col-12 mb-4">
											<?php include("../includes/messages.php") ?>		
										</div>
									</div>
									<div class="row">
										<div class="col-md-4 col-12">
											<div class="form-group">
												<label for="txtOldPassword">Current Password</label>
												<input type="password" name="txtOldPassword" id="txtOldPassword" class="form-control" placeholder="Current Password"/>
											 </div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4 col-12">
											<div class="form-group">
												<label for="txtNewPassword">New Password</label>
												<input type="password" name="txtNewPassword" id="txtNewPassword" class="form-control" placeholder="New Password"/>
											 </div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4 col-12">
											<div class="form-group">
												<label for="txtConfirmNewPassword">Confirm Password</label>
												<input type="password" name="txtConfirmNewPassword" id="txtConfirmNewPassword" class="form-control" placeholder="Confirm Password" />
											</div>
										</div>
									</div>
								  	<input type="submit" class="btn btn-primary" name="btnResetPassword" value="Change Password" type="submit" />
								</form>
							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->

		</div>
			<!-- /.content-wrapper -->
			
			<?php include('../includes/copyright.php'); ?>
			
			<!-- Scroll to Top Button-->  
			<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			  <i class="ti-angle-double-up"></i>
			</a>

			<?php include('../includes/web_footer.php'); ?>
	  
	</body>
</html>
