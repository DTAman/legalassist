<div id="assignNewVerifierModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Assign To Verifier</h4>
			</div>
			<form action='' method='post' name='frmAssignNewVerifier' id='frmAssignNewVerifier'>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<select required="required" name="ddlAssignToVerifier" id="ddlAssignToVerifier" class="form-control">
								<option value=''>Select Verifier</option>
							<?php
								$getData = getVerifiers('');
								while($vvrow = mysqli_fetch_object($getData))
								{
									?>
										<option value='<?php echo $vvrow->uid ?>'><?php echo $vvrow->verifier_name ?></option>
									<?php
								}
							?>
						</select>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<textarea required="required" class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter details if any" name='txtAssignVerifierComments' id='txtAssignVerifierComments'></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnAssignNewVerifier" name="btnAssignNewVerifier" type="submit" class="btn btn-primary" value='Assign Verifier' />
				</div>
			</form>
		</div>
	</div>
</div>