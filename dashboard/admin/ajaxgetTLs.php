<?php
include('../includes/basic_auth.php'); 

$p=1;
$pagecount=1;
$last_page=1;
$first_page=0;
$offset=0;
$condition='';

if(isset($_POST['t'])&& trim($_POST['t'])==1)
{
	$name='Team Lead';
	$type=$CTL;
	$is_a = 1;
}
else
{
	$name='Agent';
	$type=$CA;
	$is_a = 2;
}

if(isset($_POST['page_skip']))
{
	$offset=($_POST['page_skip']-1)*$limit;
	$n = $offset+1;
}
else
{
	$_POST['page_skip']=0;
}

if(trim($_POST['txtSearch'])!=null)
{
	$condition.=" and (login_master.phone like '%{$_POST['txtSearch']}%' or first_name like '%{$_POST['txtSearch']}%' or middle_name like '%{$_POST['txtSearch']}%' or last_name like '%{$_POST['txtSearch']}%') ";
}

if(trim($_POST['txtFromDate'])!=null)
{
	$create_date=date('Y-m-d',strtotime($_POST['txtFromDate']));
	$condition.=" and login_master.creation_date >= '{$create_date}'";
}

if(trim($_POST['txtToDate'])!=null)
{
	$to_date=date('Y-m-d',strtotime($_POST['txtToDate']));
	$condition.=" and login_master.creation_date <= '{$to_date}'";
}

if(isset($_POST['nextpage']) && trim($_POST['nextpage'])!=null)
{
	$pagecount = $_POST['nextpage'];
	$last_page = $_POST['nextpage'];
}

if(isset($_POST['firstpage']) && trim($_POST['firstpage'])!=null)
{		
	$pagecount = $_POST['firstpage']-($set_count-1);
	$last_page = $_POST['firstpage']-($set_count-1);
}

if(isset($_POST['firstpage1']) && trim($_POST['firstpage1'])!=null)
{
	$firstpage1 = trim($_POST['firstpage1']);
	
	$pagecount = $firstpage1;
	$last_page = $firstpage1;
}

// $condition.=" and tl_and_agents.type='".$type."' ";
$condition.=" and tl_and_agents.type='".$type."' and callcenter_id=".$_SESSION['userId'];

$totalCount = getUserCount($type, $condition);
$totalResults = getUserDataByPagination($type,$condition,$limit,$offset);
	
if(mysqli_num_rows($totalResults)>0)
{
	?>
		<div class='hh5'><table class="w-100 table newTable table-striped table-hover">
			<thead>
				<tr>
					<th class='text-center'>Sr.No.</th>
					<th>Name</th>
					<th>Phone Number</th>
					<th>PAN</th>
					<th style="min-width: 300px">Address</th>
					<th style="min-width: 60px">Attachments</th>
					<th>Status</th>
					<th class='text-center'>Quick Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php
			//$n = 1;
			while($rc = mysqli_fetch_object($totalResults))
			{
				$attachments = 0;
				trim($rc->adhar)!=null?$attachments++:'';
				trim($rc->pan)!=null?$attachments++:'';
				trim($rc->driving_license)!=null?$attachments++:'';
				trim($rc->voter_id)!=null?$attachments++:'';
				trim($rc->id_proof)!=null?$attachments++:'';
				trim($rc->passport)!=null?$attachments++:'';
				trim($rc->other_proof)!=null?$attachments++:'';
				?>
					<tr id="delete<?php echo $rc->uid ?>">
						<td class='text-center'><?php echo $n ?></td>
						<td><?php echo $rc->first_name." ".$rc->middle_name." ".$rc->last_name ?></td>
						<td><?php echo $rc->phone ?></td>
						<td><?php echo $rc->pan_number ?></td>
						<td><?php echo $rc->address.', '.$rc->cname ?></td>
						<td style="min-width: 60px"><i class="p-2 text-danger ti-pin-alt" style="font-size: 18px"></i><?php echo $attachments ?></td>
						<td><?php echo $rc->status=='AC'?'Active':'Inactive' ?></td>
						<td class='text-center'>
							<div class='icnos'>
								<a href="edit-agent-each.php?t=<?php echo $is_a?>&u=<?php echo $rc->uid ?>" class="icn settings" title="" data-toggle="tooltip" data-original-title="Edit" aria-describedby="tooltip600250"><i class="ti-pencil"></i></a>
								<a class="icn text-success" href="edit-agent-each.php?t=<?php echo $is_a?>&u=<?php echo $rc->uid ?>&s=1" title="" data-toggle="tooltip" data-original-title="View" aria-describedby="tooltip600250"><i class="ti-eye"></i></a>
								<a href="javascript:void(0)" onclick='deleteUser(<?php echo $rc->uid ?>)'  class="icn delete" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
							</div>
						</td>
					</tr>
				<?php
				$n++;
			}
			?>
			</tbody>
		</table></div>
		<?php include('../includes/ajax_pagination.php'); ?>
	</div>
	<?php
}
else
{
	?>
		<?php include('../includes/norows.php'); ?>
	<?php
}
?>