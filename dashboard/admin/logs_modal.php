<?php 
include('../includes/basic_auth.php');
if(isset($_POST['txtLeadId']) && ($_POST['txtLeadId'])!=null)
{
	$uid = $_POST['txtLeadId'];
	// $view_result = getLeadInfo(' where id='.$uid,1,0);
	$queryResults = "select IF(login_master.type='CCA', (select is_affliated from callcenter where uid = comment_to), 0) as is_affliated,login_master.type as comment_to_type, lead_comments.* ,
	(select type from login_master where uid=lead_comments.comment_from) as comment_from_type
	from lead_comments inner join login_master on login_master.uid = lead_comments.comment_to where lead_id=".$uid." order by cid desc";
	$totalResults = getQuery($queryResults);
	?>
		<div class="table table-responsive">
			<?php
			if(mysqli_num_rows($totalResults)>0)
			{
				?>
				<table class="w-100 table newTable table-striped table-bordered table-hover" border='1' cellspacing='0' cellpadding='4'>
					<thead>
						<tr class='alert alert-danger'>
							<th>Date Time</th>
							<th>Comment By<br/>Comment To</th>
							<th>Comment</th>
						</tr>
					</thead>
					<tbody>
					<?php
					$n = 1;
					while($rc = mysqli_fetch_object($totalResults))
					{
							if($rc->is_shift==1)
							{
								?>
									<tr class='bg-info-light'>
								<?php
							}													
							else
							{
								?>
									<tr class='<?php echo $rc->directly_assigned_admin==1?"alert-success":""  ?>'>
								<?php
							}
							?>
								<td>
									<?php echo get_timezone_offset(date('Y-m-d H:i:s',strtotime($rc->creation_date)),'UTC',$_SESSION['userTimeZone']) ?>
								</td>
								<td>
								<?php 
									if($rc->comment_from!=null && $rc->comment_from_type!=null)
									{
										echo '<b>From</b>: '.getNames($rc->comment_from,$rc->comment_from_type);
									}
									echo '<br/>';
									if($rc->comment_to!=null && $rc->comment_to_type!=null)
									{
										echo '<b>To</b>: '. getNames($rc->comment_to,$rc->comment_to_type);
										
										?>
										<span style='color:red'>
										(<?php
										if($rc->comment_utype=='MSW')
										{
											echo 'Administrator';
										}
										else if($rc->comment_utype=='C')
										{
											echo 'Solicitor';
										}
										else if($rc->comment_utype=='CA')
										{
											echo 'Callcenter Agent';
										}
										else if($rc->comment_utype=='CCA' && $rc->is_affliated==1)
										{
											echo "Affiliated Callcenter";
										}
										else if($rc->comment_utype=='CCA')
										{
											echo "Normal Callcenter";
										}
										else if($rc->comment_utype=='CTL')
										{
											echo 'Callcenter Team Lead';
										}
										else if($rc->comment_utype=='VA')
										{
											echo 'Verifier';
										}
										?>
										)</span>
										<?php
									}
								?>
								</td>																				
								<td style="max-width:400px">
									<span class='badge badge-warning'>
										<?php 
											if($rc->is_shift==1)
											{
												echo 'Lead Shifted';
											}
											else if($rc->reject_after_sol_assign==1)
											{
												echo 'Pulled By Admin';
											}
											else if($rc->directly_assigned_admin==1)
											{
												echo 'Transferred To Admin';
											}
											else if($rc->lead_transfer_type=='LT' && trim($rc->is_approved_verifier)!=null)
											{
												echo $rc->is_approved_verifier=='AV'?'Lead Approved By Verifier':'Lead Rejected By Verifier';
											}
											else if($rc->email_sent>0)
											{
												echo 'Email Sent';
											}
											else if($rc->sms_sent>0)
											{
												echo 'SMS Sent';
											}
											else if($rc->lead_transfer_type=='LT')
											{
												echo 'Lead Transferred';
											}
											else if($rc->lead_transfer_type=='AP')
											{
												echo 'Lead Approved';
											}
											else if($rc->lead_transfer_type=='SI')
											{
												echo 'Lead Invoice Sent';
											}
											else if($rc->lead_transfer_type=='IP')
											{
												echo 'Lead Invoice Paid';
											}
											else if($rc->lead_transfer_type=='RJ')
											{
												echo 'Lead Rejected';
											}
											else if($rc->lead_transfer_type=='SC')
											{
												echo 'Spoken To Client';
											}
											else if($rc->lead_transfer_type=='NA')
											{
												echo 'No Answer';
											}
											else if($rc->lead_transfer_type=='CB')
											{
												echo 'Callback Requested';
											}
											else if($rc->lead_transfer_type=='FI')
											{
												echo 'Further Investigation';
											}
											else if($rc->lead_transfer_type=='SU')
											{
												echo 'Sign Up Done';
											}
											else if($rc->lead_transfer_type=='CBA')
											{
												echo 'Callback Requested By Admin';
											}
											else if($rc->lead_transfer_type=='CBC')
											{
												echo 'Callback Requested By Callcenter';
											}
											else if($rc->lead_transfer_type=='CBV')
											{
												echo 'Callback Requested By Verifier';
											}
											else if($rc->lead_transfer_type=='CBAD')
											{
												echo 'Callback Done By Admin';
											}
											else if($rc->lead_transfer_type=='CBCD')
											{
												echo 'Callback Done By Callcenter';
											}
											else if($rc->lead_transfer_type=='CBVD')
											{
												echo 'Callback Done By Verifier';
											}
											else if($rc->lead_transfer_type=='NTS')
											{
												echo 'Notes added';
											}
											else if($rc->lead_transfer_type=='CBAG')
											{
												echo 'Callback Requested By Agent';
											}
											else if($rc->lead_transfer_type=='CBTL')
											{
												echo 'Callback Requested By Team Lead';
											}
											else if($rc->lead_transfer_type=='CBAGD')
											{
												echo 'Callback Done By Agent';
											}
											else if($rc->lead_transfer_type=='CBTLD')
											{
												echo 'Callback Done By Team Lead';
											}
										?>
										</span>
										<br/>
										<span class="mycomments">
										<?php
										if(trim($rc->callback_type)!=null && ($rc->lead_transfer_type=='CBA' || $rc->lead_transfer_type=='CBC' || $rc->lead_transfer_type=='CBV'))
										{
											echo $rc->callback_type=='N'?'No Answer(Callback)':'Callback Requested'."<br/>";
											echo date("d M,Y H:i",strtotime($rc->callback_dt))."<br/>";
											echo getTimeZone($rc->client_state_zone_callback)."<br/>";
										}
										else if(trim($rc->signup_type)!=null)
										{
											echo $rc->signup_type=='E'?'Email':($rc->signup_type=='P'?'Post': ($rc->signup_type=='O'?'Office Signup':'Home Signup'));
											echo "<br/>";
											if($rc->signup_type=='E' || $rc->signup_type=='P')
											{
												echo date("d M,Y",strtotime($rc->signup_dt))."<br/>";
											}
											else
											{
												echo date("d M,Y H:i",strtotime($rc->signup_dt))."<br/>";
											}
											echo getTimeZone($rc->client_state_zone)."<br/>";
										}
										?>
										<?php echo $rc->comments ?>
										</span>
								</td>		
							</tr>
						<?php
						$n++;
					}
					?>
					</tbody>
				</table>
				<?php
			}
			else
			{
				include('../includes/norows.php'); 
			}
			?>
		</div>

	<?php
}