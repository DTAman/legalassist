<?php 
include('../includes/basic_auth.php');

$date = date("Y-m-d");
$arr_1 = '';

if(isset($_POST['page_skip']) && trim($_POST['page_skip'])!=null)
{
	$skip = $chart_pagination * ($_POST['page_skip']- 1);
	$getAgents = getUsersofCallcenterLimit($_SESSION['userId'],'CA',$skip);
	while($b=mysqli_fetch_object($getAgents))
	{
		$query = "
			select count(*),
		   (select count(*) from leads q where q.added_by = ".$b->uid.") as generated_by,
		   (select count(*) from leads p where p.agent_assigned = ".$b->uid." and (p.assigned_usertype='CA')) as working,
		   (select count(*) from leads x where x.agent_assigned = ".$b->uid." and (x.client_status='AP' or x.client_status='IP' or x.client_status='SI')) as approved,
		   (select count(*) from leads l where l.agent_assigned = ".$b->uid." and assigned_client is not null) as leads_with_sol
		   from leads where agent_assigned = ".$b->uid;
		$datamaster = $mysqli->prepare($query);
		$datamaster->execute();
		$datamaster->bind_result($d1, $d2, $d3, $d4, $d5);
		$datamaster->fetch();
		$datamaster->close();
		
		$arr_1.= "['".($b->first_name.' '.$b->last_name)."',".$d1.",".$d2.",".$d3.",".$d4.",".$d5."],";
	}

	?>
	<div id="chart_1" style="height: 500px;"></div>
	<div class="text-center">
		<ul class="pagination">
		<?php
		$total = getUserCountProgram("CA", " and tl_and_agents.type='CA' and is_deleted=0 and tl_and_agents.callcenter_id = ".$_SESSION['userId']);
		$total=ceil($total/$set_count);
		
		for($pagecount=1;$pagecount<=$total;$pagecount++)
		{
			?>
				<li id="li_page<?php echo $pagecount ?>" class="page-item <?php echo $_POST['page_skip']==$pagecount?'active':'' ?>">
					<a class="page-link waves-effect" href="javascript:void(0)" role="button" onclick='getCharts(<?php echo $pagecount ?>)' href="javascript:void(0)"><?php echo $pagecount; ?>
					</a>
				</li>
			<?php
		}
		?>

		</ul>
	</div>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
	google.charts.load('current', {'packages':['bar']});
	google.charts.setOnLoadCallback(drawChart_1);

	function drawChart_1() {
		var data = google.visualization.arrayToDataTable([
			['Leads', 'Total Assigned', 'Generated', 'Working', 'Approved By Sol','Leads With Sol'],
			<?php echo trim($arr_1,',') ?>
		  ]);

		var options = {
		  chart: {
			title: 'Overall Agent Performance',
			subtitle: 'Leads Total',
		  },
		  bars: 'vertical'
		};

		var chart = new google.charts.Bar(document.getElementById('chart_1'));
		chart.draw(data, google.charts.Bar.convertOptions(options));
	}
	</script>
	<?php
}
?>