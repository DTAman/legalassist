<?php
include '../includes/basic_auth.php';
if($_SESSION['userType'] == $SA || $_SESSION['userType'] == $VA || $_SESSION['userType'] == $CTL || $_SESSION['userType'] == $MSW || $_SESSION['userType'] == $C || ($_SESSION['userType']==$CCA )) 
{
   //Nothing
}
else
{
	 header("location:index.php");
}
$r = 1;
$getStates = getStates();
$accident_count = 5;
$vehicle_flag = 2;
$injury_array = array();
if (isset($_GET['u']) && trim($_GET['u'] != null)) 
{
    $uid = trim($_GET['u']);
    if (isset($_GET['d']) && trim($_GET['d'] == 1)) 
	{
        $view_result = getLeadsDataByPaginationDeleted(' and id=' . $uid, 1, 0);
    } 
	else 
	{
        $view_result = getLeadsDataByPagination(' and id=' . $uid, 1, 0);
    }
	
    if (mysqli_num_rows($view_result) == 0) 
	{
        header("location:index.php");
    } 
	else 
	{
        $rc1 = mysqli_fetch_object($view_result);
    }
} 
else 
{
    header("location:index.php");
}
$view_accidents = getQuery('select * from  lead_accidents where status=1 and lead_id=' . $uid);
$accidents_flag = mysqli_num_rows($view_accidents);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Injury Assist</title>
		<style>
			@media print {
				@page 
				{
					margin: 15px;
				}
				body 
				{
					margin: 20px;
				}
			}
			td span 
			{
				opacity: 0.6;
			}
			td p 
			{
				margin: 0;
			}
			td
			{
				padding: 5px 10px;
			}
			.borderClass
			{
				border: 1px solid #d0d0d0;
			}
		</style>
	</head>
	<body>
		<button style="    border: 1px solid white; padding: 10px 30px; border-radius: 5px; box-shadow: 0 3px 6px rgba(0, 0, 0, 0.15); float: right; width: 100%; margin-bottom: 18px; background: #26af48; border-color: #26af48; color: #fff; text-transform: uppercase; margin: 10px;" id="btnGeneratePDF">Print Lead</button> <br/><br/>
		
		<table style="width: 100%; border: 1px solid #d0d0d0; border-collapse: collapse;font-family: sans-serif;">
			<tbody style="padding: 15px">
				<tr>
					<td colspan="3" class="borderClass"><h3 style="margin: 0; padding: 10px 0;"><b>Personal Details</b></h3></td>
				</tr>
				<?php
					$a1_state = null;
					$a1_suburb = null;
					while ($staterow = mysqli_fetch_object($getStates)) 
					{
						if ($rc1->state_id == $staterow->id) 
						{
							$a1_state = $staterow->initials . ',';
							break;
						}
					}
					
					if (trim($rc1->state_id) != null) 
					{
						$getSubUrbs = getSubUrbs(" and state_id = " . $rc1->state_id);
						while ($srow = mysqli_fetch_object($getSubUrbs)) 
						{
							if ($rc1->suburb_id == $srow->id) 
							{
								$a1_suburb = $srow->suburb_name;
								break;
							}
						}
					}
				?>
				<tr>
					<td><p><span>Name: </span><?php echo $rc1->first_name . ' ' . $rc1->middle_name . ' ' . $rc1->last_name ?></p></td>
					<td><p><span>Email: </span><?php echo $rc1->email ?></p></td>
					<td><p><span>DOB: </span><?php echo $rc1->dob ?></p></td>
				</tr>
				<tr>
					<td><p><span>Mobile: </span><?php echo $rc1->phone ?></p></td>
					<td><p><span>State/Suburb: </span><?php echo $a1_state . ' ' . $a1_suburb . ' ' . $rc1->suburb_name ?></p></td>
					<td><p><span>Postal Code: </span><?php echo $rc1->postal_code ?></p></td>
				</tr>
				<?php
				if(trim($rc1->address)!=null)
				{
					?>
						<tr>
							<td colspan="3"><p ><span>Address: </span><?php echo $rc1->address ?></p></td>
						</tr>
					<?php
				}
				if(trim($rc1->relative_name)!=null || trim($rc1->relative_number)!=null)
				{
					?>
						<tr>
							<td><p><span>Relative: </span><?php echo $rc1->relative_name ?></p></td>
							<td colspan="2"><p><span>Alt. No: </span><?php echo $rc1->relative_number ?></p></td>
						</tr>
					<?php
				}
				?>
				
				<tr>
					<td colspan="3" class="borderClass"><h3 style="margin: 0; padding: 10px 0;"><b>Accident Basics</b></h3>
						<?php
							$acci_count = 0;
							if ($accidents_flag > 0) 
							{
								while ($rc2 = mysqli_fetch_object($view_accidents)) 
								{
									$acci_count++;
									?>
									<table style="width: 100%;border-bottom: 5px dashed #a09898; border-collapse: collapse;margin-bottom: 30px;">
										<tbody>
											<tr style="background: #d0d0d0;">
												<td colspan="3" class="borderClass"><h4 style="margin: 0; padding: 10px 0;"><b>Details of Accident <?php echo $acci_count ?></b></h4>
													<tr>
														<td colspan="3" class="borderClass">
															<h4 style="margin: 0; padding: 10px 0;"><b>Accident Details Driver</b></h4>
															<table style="width: 100%; border: 1px solid #d0d0d0; border-collapse: collapse;margin-bottom: 10px;">
																<tbody>
																	<tr>
																		<td><p><span>Accident Date: </span><?php echo date('d', strtotime($rc2->accident_date_time)) == 1 && $rc2->is_na == 1 ? '' : date('d', strtotime($rc2->accident_date_time)) . '-'; ?><?php echo date('M-Y h:i A', strtotime($rc2->accident_date_time)); ?></p></td>
																		<td>
																			<p>
																				<span>Accident Kind: </span>
																				<?php
																					switch ($rc2->accident_kind) 
																					{
																						case 1: echo 'Motor Vehicle Accident';
																								break;
																						case 2:	echo 'Occupiers Liability';
																								break;
																						case 3:	echo 'Public Liability';
																								break;
																						case 4: echo 'Accident at Work';
																								break;
																					}
																				?>
																			</p>
																		</td>
																		<?php
																			$vehicle_flag = 2;
																			$injury_array = array();
																			if (isset($rc2->id) && trim($rc2->id) != null && isset($rc2->lead_id) && trim($rc2->lead_id) != null) 
																			{
																				$aid = trim($rc2->id);
																				$lid = trim($rc2->lead_id);
																			}

																			$view_vehicles = getQuery('select * from lead_vehicles where lead_accident_id=' . $aid . ' and lead_id=' . $uid);
																			$view_work_history = getQuery('select * from lead_work_care_history where lead_accident_id=' . $aid . ' and lead_id=' . $uid);
																			$view_injuries = getQuery('select * from lead_injury where lead_accident_id=' . $aid . ' and lead_id=' . $uid);
																			$view_treatment_history = getQuery('select * from lead_treatment_history where lead_accident_id=' . $aid . ' and lead_id=' . $uid);
																			$view_passengers = getQuery('select * from lead_passenger_details where lead_accident_id=' . $aid . ' and lead_id=' . $uid);
																			$view_witness = getQuery('select * from lead_witness where lead_accident_id=' . $aid . ' and lead_id=' . $uid);
																			$vehicles_flag = mysqli_num_rows($view_vehicles);
																			$accidents_flag = mysqli_num_rows($view_accidents);
																			$work_history_flag = mysqli_num_rows($view_work_history);
																			$treatment_flag = mysqli_num_rows($view_treatment_history);
																			$injury_flag = mysqli_num_rows($view_injuries);
																			$passengers_flag = mysqli_num_rows($view_passengers);
																			$witness_flag = mysqli_num_rows($view_witness);
																			$rc4 = $rc5 = $rc6 = $rc7 = $rc9 = null;
																			if ($vehicles_flag > 0) {
																				$rc4 = mysqli_fetch_object($view_vehicles);
																			}
																			$injury_details_flag = 0;
																			if ($injury_flag > 0) 
																			{
																				$rc5 = mysqli_fetch_object($view_injuries);
																				$view_injuries_details = getQuery('SELECT body_part,injury FROM lead_injury_details inner join injury_type on injury_type.id = lead_injury_details.injury_id inner join body_parts on body_parts.id = lead_injury_details.body_part_id inner join lead_injury on lead_injury.id=lead_injury_details.injury_master_id where lead_accident_id=' . $rc5->lead_accident_id);
																				$injury_details_flag = mysqli_num_rows($view_injuries_details);
																			}
																			if ($treatment_flag > 0) 
																			{
																				$rc6 = mysqli_fetch_object($view_treatment_history);
																			}
																			if ($work_history_flag > 0) 
																			{
																				$rc7 = mysqli_fetch_object($view_work_history);
																			}
																			if ($witness_flag > 0) 
																			{
																				$rc9 = mysqli_fetch_object($view_witness);
																			}

																			$a2_state = $a2_suburb = null;
																			mysqli_data_seek($getStates, 0);
																			while ($staterow = mysqli_fetch_object($getStates)) 
																			{
																				if ($rc2->state_id == $staterow->id) 
																				{
																					$a2_state = $staterow->initials . ',';
																					break;
																				}
																			}
																			if (trim($rc2->state_id) != null) 
																			{
																				$getSubUrbs = getSubUrbs(" and state_id = " . $rc2->state_id);
																				while ($srow = mysqli_fetch_object($getSubUrbs))
																				{
																					if ($rc2->suburb_id == $srow->id) 
																					{
																						$a2_suburb = $srow->suburb_name;
																						break;
																					}
																				}
																			}
																		?>
																		<td>
																			<p><span>Want To Claim: </span>
																				<?php echo $rc2->claim_accident == 1 ? 'Yes' : 'No'; ?>
																			</p>
																		</td>
																	</tr>
																	<tr>
																		<?php
																		if($rc2->accident_kind == 1)
																		{
																			?>
																				<td><p><span>Claimant Is:</span> <?php echo $rc2->is_driver == 1 ? 'Driver' : 'Passenger' ?></p></td>
																				<td><p><span>Weather:</span> <?php echo $rc2->weather ?></p></td>
																				<td><p><span>Postal:</span> <?php echo $rc2->postal_code ?></p></td>
																			<?php
																		}
																		else
																		{
																			?>
																				<td><p><span>Weather:</span> <?php echo $rc2->weather ?></p></td>
																				<td colspan="2"><p><span>Postal:</span> <?php echo $rc2->postal_code ?></p></td>
																			<?php
																		}
																		?>
																		
																	</tr>
																	
																	<?php
																	if(trim($rc2->address)!=null || trim($a2_state)!=null || trim($a2_suburb)!=null || trim($rc2->suburb_name)!=null)
																	{
																		?>
																			<tr>
																				<td colspan="2"><p><span>Address:</span> <?php echo $rc2->address ?></p></td>
																				<td><p><span>State/Suburb: </span><?php echo $a2_state . ' ' . $a2_suburb . ' ' . $rc2->suburb_name ?></p></td>
																			</tr>
																		<?php
																	}
																	if(trim($rc2->accident_circumstances)!=null)
																	{
																		?>
																			<tr>
																				<td colspan="3"><p><span>Accident Circumstances:</span> <?php echo $rc2->accident_circumstances ?></p></td>
																			</tr>
																		<?php
																	}
																		if ($rc2->police_attended == 1) 
																		{
																			?>
																				<tr>
																					<td>
																						<p><span>Police: </span>Yes</p>
																					</td>
																					<td><p><span>Police Station: </span> <?php echo $rc2->attended_police_station ?></p></td>
																					<td><p><span>Police Event Number: </span> <?php echo $rc2->event_number ?></p></td>
																				</tr>
																				<tr>
																					<td colspan="3"><p><span>Other Info: </span> <?php echo $rc2->other_info ?></p></td>
																				</tr>
																			<?php
																		} 
																		else if ($rc2->police_attended == 0) 
																		{
																			?>
																				<tr>
																					<td>
																						<p><span>Police: </span> No</p>
																					</td>
																					<td colspan="2">
																						<?php
																							if (trim($rc2->accident_reported) != null) 
																							{
																								?>
																									<p>
																										<span>Accident reported if Police not attended? </span>
																										<?php echo $rc2->accident_reported == 1 ? 'Yes' : 'No'; ?>
																									</p>
																								<?php
																							}
																						?>
																					</td>
																				</tr>
																			<?php
																			if ($rc2->accident_reported == 1) 
																			{
																				?>
																					<tr>
																						<td><p><span>Officer Name: </span> <?php echo $rc2->officer_name ?></p></td>
																						<td colspan="2"><p><span>Police Station Reported: </span> <?php echo $rc2->informed_police_station ?></p></td>
																					</tr>
																				<?php
																			}
																		}
																		else 
																		{
																			echo '';
																		}
																		
																		if ($rc2->accident_kind != 1) 
																		{
																			?>
																				<tr>
																					<td colspan="3"><p><span>Was the accident recorded in the accident book?</span> <?php echo $rc2->accident_recorded ?></p></td>
																				</tr>
																			<?php
																		}
																	?>
																</tbody>
															</table>
														</td>
													</tr>
													
													
													<?php
													if ($rc2->accident_kind == 1 && $vehicles_flag > 0) 
													{
														
														if(trim($rc4->make)!=null || trim($rc4->model)!=null || trim($rc4->registration)!=null || trim($rc4->insurance_company)!=null || trim($rc4->insurance_reference_no)!=null || trim($rc4->insurance_claim_number)!=null || trim($rc4->other_info)!=null || trim($rc4->driver_name)!=null || trim($rc4->driver_address)!=null || trim($rc4->dl_number)!=null || trim($rc4->make_d)!=null || trim($rc4->model_d)!=null || trim($rc4->registration_d)!=null || trim($rc4->insurance_company_d)!=null || trim($rc4->insurance_reference_no_d)!=null || trim($rc4->insurance_claim_number_d)!=null || trim($rc4->other_info_d)!=null)
														{
															?>
																<tr>
																	<td colspan="3" class="borderClass"><h4 style="margin: 0; padding: 10px 0;"><b>Vehicle Details</b></h4>
																		<table style="width: 100%; border: 1px solid #d0d0d0; border-collapse: collapse;margin-bottom: 10px;">
																			<tbody>
																				<?php
																				if(trim($rc4->make)!=null || trim($rc4->model)!=null || trim($rc4->registration)!=null || trim($rc4->insurance_company)!=null || trim($rc4->insurance_reference_no)!=null || trim($rc4->insurance_claim_number)!=null || trim($rc4->other_info)!=null)
																				{
																					?>
																						<tr>
																							<td colspan="3" class="borderClass"><h4 style="margin: 0; padding: 10px 0;"><b>Client Vehicle</b></h4>
																								<table style="width: 100%;margin-bottom: 10px;">
																									<tbody>
																										<?php
																										if(trim($rc4->make)!=null || trim($rc4->model)!=null || trim($rc4->registration)!=null)
																										{
																											?>
																												<tr>
																													<td><p><span>Make:</span> <?php echo isset($rc4->make) ? $rc4->make : '' ?></p></td>
																													<td><p><span>Model:</span> <?php echo isset($rc4->model) ? $rc4->model : '' ?></p></td>
																													<td><p><span>Registration:</span> <?php echo isset($rc4->registration) ? $rc4->registration : '' ?></p></td>
																												</tr>
																											<?php
																										}
																										if(trim($rc4->insurance_company)!=null || trim($rc4->insurance_reference_no)!=null || trim($rc4->insurance_claim_number)!=null)
																										{
																											?>
																												<tr>
																													<td><p><span>Insurance company Name:</span> <?php echo isset($rc4->insurance_company) ? $rc4->insurance_company : '' ?></p>
																													</td>
																													<td><p><span>Insurance reference number:</span> <?php echo isset($rc4->insurance_reference_no) ? $rc4->insurance_reference_no : '' ?></p></td>
																													<td><p><span>Insurance Claim number:</span> <?php echo isset($rc4->insurance_claim_number) ? $rc4->insurance_claim_number : '' ?></p></td>
																												</tr>
																											<?php
																										}
																										if(trim($rc4->other_info)!=null)
																										{
																											?>
																												<tr>
																													<td colspan="3"><p><span>Other Info:</span> <?php echo isset($rc4->other_info) ? $rc4->other_info : '' ?></p></td>
																												</tr>
																											<?php
																										}
																										?>
																									</tbody>
																								</table>
																							</td>
																							</tr>
																					<?php
																				}
																				
																				if(trim($rc4->driver_name)!=null || trim($rc4->driver_address)!=null || trim($rc4->dl_number)!=null || trim($rc4->make_d)!=null || trim($rc4->model_d)!=null || trim($rc4->registration_d)!=null || trim($rc4->insurance_company_d)!=null || trim($rc4->insurance_reference_no_d)!=null || trim($rc4->insurance_claim_number_d)!=null || trim($rc4->other_info_d)!=null)
																				{
																					?>
																						<tr>
																							<td colspan="3" class="borderClass"><h4 style="margin: 0; padding: 10px 0;"><b>Third Party Vehicle</b></h4>
																							<table style="width: 100%;margin-bottom: 10px;">
																								<tbody>
																								<?php
																									if(trim($rc4->driver_name)!=null || trim($rc4->driver_address)!=null || trim($rc4->dl_number)!=null)
																									{
																										?>
																										<tr>
																											<td><p><span>Driver Name:</span> <?php echo isset($rc4->driver_name) ? $rc4->driver_name : '' ?></p></td>
																											<td><p><span>Driver Address:</span> <?php echo isset($rc4->driver_address) ? $rc4->driver_address : '' ?></p></td>
																											<td><p><span>DL number:</span> <?php echo isset($rc4->dl_number) ? $rc4->dl_number : '' ?></p></td>
																										</tr>
																										<?php
																									}
																									if(trim($rc4->make_d)!=null || trim($rc4->model_d)!=null || trim($rc4->registration_d)!=null)
																									{
																										?>
																											<tr>
																												<td><p><span>Make:</span> <?php echo isset($rc4->make_d) ? $rc4->make_d : '' ?></p></td>
																												<td><p><span>Model:</span> <?php echo isset($rc4->model_d) ? $rc4->model_d : '' ?></p></td>
																												<td><p><span>Registration:</span> <?php echo isset($rc4->registration_d) ? $rc4->registration_d : '' ?></p></td>
																											</tr>
																										<?php
																									}
																									if(trim($rc4->insurance_company_d)!=null || trim($rc4->insurance_reference_no_d)!=null || trim($rc4->insurance_claim_number_d)!=null)
																									{
																										?>
																											<tr>
																												<td><p><span>Insurance company Name:</span> <?php echo isset($rc4->insurance_company_d) ? $rc4->insurance_company_d : '' ?></p></td>
																												<td><p><span>Insurance reference number:</span> <?php echo isset($rc4->insurance_reference_no_d) ? $rc4->insurance_reference_no_d : '' ?></p></td>
																												<td><p><span>Insurance Claim number:</span> <?php echo isset($rc4->insurance_claim_number_d) ? $rc4->insurance_claim_number_d : '' ?></p></td>
																											</tr>
																										<?php
																									}
																									if(trim($rc4->other_info_d)!=null)
																									{
																										?>
																											<tr>
																												<td colspan="3"><p><span>Other Info:</span> <?php echo isset($rc4->other_info_d) ? $rc4->other_info_d : '' ?></p></td>
																											</tr>
																										<?php
																									}
																								 ?>
																								</tbody>
																							</table>
																						</td>
																					</tr>
																					<?php
																				}
																				?>	
																			</tbody>
																		</table>
																	</td>
																</tr>
															<?php
														}
													}

													if ($injury_flag > 0) 
													{
														?>
															<tr>
																<td colspan="3" class="borderClass"><h4 style="margin: 0; padding: 10px 0;"><b>Injuries - Matrix Format</b></h4>
																	<table style="width: 100%; border: 1px solid #d0d0d0; border-collapse: collapse;margin-bottom: 10px;">
																		<tbody>
																			<?php
																				if ($injury_details_flag > 0) 
																				{
																					$inj = '';
																					while ($rc5_1 = mysqli_fetch_object($view_injuries_details)) 
																					{
																						$inj.= $rc5_1->body_part . ' (' . $rc5_1->injury . ')' . ' , ';
																					}
																					?>
																						<tr>
																							<td colspan="3"><p><span>Injuries:</span> <?php echo trim($inj, ', '); ?></p></td>
																						</tr>
																					<?php
																				}
																				
																				if (isset($rc5->other_injuries) && trim($rc5->other_injuries)!=null) 
																				{
																					?>
																						<tr>
																							<td colspan="3">
																								<p>
																									<span>Any Other Injuries:</span> <?php echo $rc5->other_injuries ?>
																								</p>
																							</td>
																						</tr>
																					<?php
																				}
																				
																				if (isset($rc5->still_suffering)) 
																				{
																					?>
																					<tr>
																						<td colspan="3"><p><span>Still Suffering:</span> <?php echo $rc5->still_suffering == 1 ? 'Yes' : 'No'; ?></p></td>
																					</tr>
																					<?php
																				}
																			?>
																		</tbody>
																	</table>
																</td>
															</tr>
														<?php
													}
													
													if ($treatment_flag > 0) 
													{
														$painreliefs_recommended = 0;
														$has_physio_chirio = 0;
														$is_physio_recommended = null;
														$is_chiro_recommended = null;
														$to_hospital = 0;
														$hos_detail = '';
														if (isset($rc6->painreliefs_recommended) && trim($rc6->painreliefs_recommended) != null) 
														{
															$painreliefs_recommended = $rc6->painreliefs_recommended;
														}
														if (isset($rc6->has_physio_chirio) && trim($rc6->has_physio_chirio) != null) 
														{
															$has_physio_chirio = $rc6->has_physio_chirio;
															$is_physio_recommended = $rc6->is_physio_recommended;
															$is_chiro_recommended = $rc6->is_chiro_recommended;
														}
														if (isset($rc6->ambulance_took_to_hospital) || isset($rc6->went_hospital_with_friend)) 
														{
															$to_hospital = trim($rc6->ambulance_took_to_hospital) == 1 ? 1 : 0;
														}
														if (isset($rc6->ambulance_hospital_detail) || isset($rc6->hospital_detail_took_by_friend)) 
														{
															$hos_detail = trim($rc6->ambulance_hospital_detail) != null ? $rc6->ambulance_hospital_detail : $rc6->hospital_detail_took_by_friend;
														}
														?>
															<tr>
																<td colspan="3" class="borderClass">
																	<h4 style="margin: 0; padding: 10px 0;"><b>Treatment History</b></h4>
																	<table style="width: 100%; border: 1px solid #d0d0d0; border-collapse: collapse;margin-bottom: 10px;">
																		<tbody>
																			<?php
																				if (trim($rc6->ambulance_at_scene) != null && ($rc6->ambulance_at_scene == 1 || $rc6->ambulance_at_scene == 0)) 
																				{
																					?>
																					<tr>
																						<td colspan="3">
																							<p><span>Did An Ambulance Come To The Scene Of The Accident? </span> <?php echo $rc6->ambulance_at_scene == 1 ? 'Yes' : 'No' ?></p>
																						</td>
																					</tr>
																					<?php
																						if ($rc6->ambulance_at_scene == 1) 
																						{
																							?>
																							<tr>
																								<td colspan="3">
																									<p><span>Did The Ambulance Treat You At The Scene Of The Accident? </span>  <?php echo isset($rc6->ambulance_treated_at_scene) && $rc6->ambulance_treated_at_scene == 1 ? 'Yes' : 'No'; ?></p>
																								</td>
																							</tr>
																							<?php
																								if (isset($rc6->ambulance_treated_at_scene) && $rc6->ambulance_treated_at_scene == 1) 
																								{
																									?>
																										<tr>
																											<td colspan="3">
																												<p><span>What Was The Treatment? </span>  <?php echo $rc6->ambulance_treatment ?></p>
																											</td>
																										</tr>
																									<?php
																									if (isset($rc6->ambulance_took_to_hospital) && trim($rc6->ambulance_took_to_hospital) != null) 
																									{
																										?>
																											<tr>
																												<td colspan="3">
																													<p><span>Did The Ambulance Take You To A Hospital? </span>  <?php echo isset($rc6->ambulance_took_to_hospital) && trim($rc6->ambulance_took_to_hospital) == 1 ? 'Yes' : 'No'; ?> </p>
																												</td>
																											</tr>
																										<?php
																									}
																							}
																							if (isset($rc6->ambulance_took_to_hospital) && trim($rc6->ambulance_took_to_hospital) == 1) 
																							{
																								?>
																										<tr>
																										   <td>
																											  <p><span>Hospital Detail: </span>  <?php echo $rc6->ambulance_hospital_detail ?></p>
																										   </td>
																										   <td colspan="2">
																											  <p><span>What Was The Treatment?</span>  <?php echo isset($rc6->hospital_treatment) ? $rc6->hospital_treatment : ''; ?></p>
																										   </td>
																										</tr>
																										<?php
																							} 
																							else if (isset($rc6->ambulance_took_to_hospital) && trim($rc6->ambulance_took_to_hospital) == 0) 
																							{
																								?>
																									<tr>
																									   <td colspan="3">
																										  <p><span>Did you go to the GP after the accident?</span>  <?php echo isset($rc6->visited_gp) && $rc6->visited_gp == 1 ? 'Yes' : 'No'; ?></p>
																									   </td>
																									</tr>
																									<?php
																										if (isset($rc6->visited_gp) && $rc6->visited_gp == 1) 
																										{
																											?>
																												<tr>
																													<td>
																														<p><span>GP Name: </span> <?php echo $rc6->gp_name ?></p>
																													</td>
																													<td>
																														<p><span>Surgery Name: </span> <?php echo $rc6->surgery_name ?></p>
																													</td>
																													<td>
																														<p><span>Last Visit: </span> <?php echo isset($rc6->last_visit_to_gp) && trim($rc6->last_visit_to_gp) != null ? $rc6->last_visit_to_gp : ''; ?></p>
																													</td>
																												</tr>
																												<tr>
																													<td colspan="3">
																														<p><span>No Of Visits To The GP Since Accident: </span> <?php echo $rc6->visits_to_gp ?></p>
																													</td>
																												</tr>
																											<?php
																										}
																							}
																					} 
																					else if ($rc6->ambulance_at_scene == 0) 
																					{
																						?>
																							<tr>
																							   <td colspan="3">
																								  <p><span>Did You Go To A Hospital By Yourself Or Taken There By A Family Member Or Friend? </span>  <?php echo isset($rc6->went_hospital_with_friend) && $rc6->went_hospital_with_friend == 1 ? 'Yes' : 'No'; ?></p>
																							   </td>
																							</tr>
																							<?php
																								if (isset($rc6->went_hospital_with_friend) && trim($rc6->went_hospital_with_friend) == 1) 
																								{
																									?>
																										<tr>
																											<td>
																												<p><span>Hospital Detail:</span>  <?php echo $rc6->hospital_detail_took_by_friend ?></p>
																											</td>
																											<td colspan="2">
																												<p><span>What Was The Treatment?</span>  <?php echo isset($rc6->hospital_treatment) ? $rc6->hospital_treatment : ''; ?></p>
																											</td>
																										</tr>
																									<?php
																								}
																								else if (isset($rc6->went_hospital_with_friend) && $rc6->went_hospital_with_friend == 0) 
																								{
																									?>
																										<tr>
																											<td colspan="3">
																												<p><span>Did you go to the GP after the accident?</span>  <?php echo isset($rc6->visited_gp) && $rc6->visited_gp == 1 ? 'Yes' : 'No'; ?></p>
																											</td>
																										</tr>
																									<?php
																										if (isset($rc6->visited_gp) && $rc6->visited_gp == 1) 
																										{
																											?>
																												<tr>
																													<td>
																														<p><span>GP Name: </span> <?php echo $rc6->gp_name ?></p>
																													</td>
																													<td>
																														<p><span>Surgery Name: </span> <?php echo $rc6->surgery_name ?></p>
																													</td>
																													<td>
																														<p><span>Last Visit: </span> <?php echo isset($rc6->last_visit_to_gp) && trim($rc6->last_visit_to_gp) != null ? $rc6->last_visit_to_gp : ''; ?></p>
																													</td>
																												</tr>
																												<tr>
																													<td colspan="3">
																														<p><span>No Of Visits To The GP Since Accident: </span> <?php echo $rc6->visits_to_gp ?></p>
																													</td>
																												</tr>
																											<?php
																										}
																							 }
																					}
																				} 
																				else 
																				{
																					?>
																						<tr>
																							<td colspan="3">
																								<p><span>Did An Ambulance Come To The Scene Of The Accident? </span> N/A</p>
																							</td>
																						</tr>
																					<?php
																				}
																			?>
																		<tr>
																			<td colspan="3">
																				<p><span>Are you taking any pain relief to manage your symptoms?</span> <?php echo $painreliefs_recommended == 1 ? 'Yes' : 'No'; ?></p>
																			</td>
																		</tr>
																		<?php
																			if (trim($has_physio_chirio) != null) 
																			{
																				?>
																					<tr>
																						<td colspan="3">
																							<p><span>Were you recommended for Physiotherapy Or Chiropractic Sessions? </span> <?php echo $has_physio_chirio == 1 ? 'Yes' : 'No' ?></p>
																						</td>
																					</tr>
																				<?php
																				if ($has_physio_chirio == 1) 
																				{
																					if ($is_physio_recommended == 1) 
																					{
																						?>
																							<tr>
																								<td>
																									<p><span>Physiotherapy? </span> Yes</p>
																								</td>
																								<td>
																									<p><span>Sessions so far? </span> <?php echo $rc6->physiosessions ?></p>
																								</td>
																							</tr>
																						<?php
																							if (trim($rc6->physiotakingsessions) != null) 
																							{
																								?>
																									<tr>
																										<td colspan="3">
																											<p><span>Are you currently taking sessions or will need sessions in the future?</span> <?php echo $rc6->physiotakingsessions ?></p>
																										</td>
																									</tr>
																								<?php
																							}
																					} 
																					else if ($is_chiro_recommended == 1) 
																					{
																						?>
																							<tr>
																								<td>
																									<p><span>Chiropractic? </span> Yes</p>
																								</td>
																								<td>
																									<p><span>Sessions so far? </span> <?php echo $rc6->chiriosessions ?></p>
																								</td>
																							</tr>
																						<?php
																						if (trim($rc6->chiriotakingsessions) != null) 
																						{
																							?>
																								<tr>
																								   <td colspan="3">
																									  <p><span>Are you currently taking sessions or will need sessions in the future?</span> <?php echo $rc6->chiriotakingsessions ?></p>
																								   </td>
																								</tr>
																							<?php
																						}
																					}
																				}
																			}
																			if(trim($rc6->have_x_ray) != null)
																			{
																				?>				
																					<tr>
																						<td colspan="3">
																							<p><span>Did You Go For Any Xray, MRI Or CT-Scan?</span> <?php echo $rc6->have_x_ray == 1 ? 'Yes' : 'No'; ?></p>
																						</td>
																					</tr>
																					<?php
																			}
																		
																			if (trim($rc6->have_x_ray) != null) 
																			{
																				if ($rc6->have_x_ray == 1) 
																				{
																					?>
																						<tr>
																						   <td colspan="3">
																							  <p><span>What Was The Result Of The Scan?</span> <?php echo $rc6->x_ray_result ?></p>
																						   </td>
																						</tr>
																					<?php
																				} 
																				else 
																				{
																					?>
																						<tr>
																						   <td colspan="3">
																							  <p><span>Why did you not go for Xray, MRI and CT Scan?</span> <?php echo $rc6->why_not_x_ray ?></p>
																						   </td>
																						</tr>
																					<?php
																				}
																			}
																			if (trim($rc6->pre_existing_injuries) != null) 
																			{
																				?>
																					<tr>
																					   <td colspan="3">
																						  <p><span>Pre-existing injuries: </span> <?php echo isset($rc6->pre_existing_injuries) ? $rc6->pre_existing_injuries : '' ?></p>
																					   </td>
																					</tr>
																				<?php
																			}
																			
																			$diseases = array();
																			if ($rc6->dizziness == 1) 
																			{
																				array_push($diseases, 'Dizziness');
																			}
																			if ($rc6->nausea == 1) 
																			{
																				array_push($diseases, 'Nausea');
																			}
																			if ($rc6->post_traumatic_stress_disorder == 1) 
																			{
																				array_push($diseases, 'Post Traumatic Stress Disorder');
																			}
																			if ($rc6->insomnia == 1) 
																			{
																				array_push($diseases, 'Insomnia');
																			}
																			if ($rc6->vertigo == 1) 
																			{
																				array_push($diseases, 'Vertigo');
																			}
																			if ($rc6->driving_difficulty_post_accident == 1) 
																			{
																				array_push($diseases, 'Driving difficulty post accident');
																			}
																			if ($rc6->fear_of_being_hit == 1) 
																			{
																				array_push($diseases, 'Fear of being hit every time client drives');
																			}
																			if (count($diseases) > 0) 
																			{
																				?>
																					<tr>
																					   <td colspan="3">
																						  <p><span>Are you suffering from any psychological injuries? </span> <?php echo implode(',', $diseases); ?></p>
																					   </td>
																					</tr>
																				<?php
																			}
																			if (trim($rc6->any_other_psychological_injury) != null) 
																			{
																				?>
																					<tr>
																					   <td colspan="3">
																						  <p><span>Any Other? </span> <?php echo $rc6->any_other_psychological_injury ?></p>
																					   </td>
																					</tr>
																					<?php
																			}
																		?>
																		</tbody>
																	</table>
																</td>
															</tr>
														<?php
													}
													
													if ($work_history_flag > 0)
													{
														?>
															<tr>
																<td colspan="3" class="borderClass">
																	<h4 style="margin: 0; padding: 10px 0;"><b>Work & Care Details</b></h4>
																	<table style="width: 100%; border: 1px solid #d0d0d0; border-collapse: collapse;margin-bottom: 10px;">
																		<tbody>
																			<?php
																			if (isset($rc7->is_employed) && trim($rc7->is_employed) != null) 
																			{
																				if (trim($rc7->is_employed) == 1) 
																				{
																					?>
																						<tr>
																							<td><p><span>Are you employed?:</span> <?php echo isset($rc7->is_employed) && $rc7->is_employed == 1 ? 'Yes' : 'No' ?></p></td>
																							<td><p><span>What is your occupation:</span> <?php echo isset($rc7->occupation) ? $rc7->occupation : '' ?></p></td>
																							<td><p><span>Weekly salary (Gross/net) AUD:</span> <?php echo isset($rc7->weekly_salary) ? $rc7->weekly_salary : '' ?></p></td>
																						</tr>
																						<tr>
																							<td><p><span>How many hours per week do you work?</span> <?php echo isset($rc7->working_hours) ? $rc7->working_hours : '' ?></p></td>
																							<td colspan="2"><p><span>How much money have you lost as a result of this accident?</span> <?php echo isset($rc7->income_loss_by_accident) ? $rc7->income_loss_by_accident : '' ?></p></td>
																						</tr>
																						<tr>
																							<td colspan="3">
																								<p>
																								<span>Are your injuries affecting your work?</span>
																								<?php
																									if (trim($rc7->injury_affecting_work) != null) 
																									{
																										if ($rc7->injury_affecting_work == 1) 
																										{
																											echo 'Yes';
																										} 
																										else if ($rc7->injury_affecting_work == 0) 
																										{
																											echo 'No';
																										}
																									} 
																									else 
																									{
																										echo '';
																									}
																								?>
																								</p>
																							</td>
																						</tr>
																					<?php
																				}
																				else 
																				{
																					?>
																					<tr>
																						<td><p><span>Are you employed?:</span> <?php echo isset($rc7->is_employed) && $rc7->is_employed == 1 ? 'Yes' : 'No' ?></p></td>
																						<td colspan="2"><p><span>State Means of Income:</span> <?php echo $rc7->income_from . ' ' . $rc7->other_income_means ?></p></td>
																					</tr>
																					<?php
																				}
																			} 
																			else 
																			{
																				?>
																				<tr>
																					<td colspan="3"><p><span>Are you employed?:</span> Not Known</p></td>
																				</tr>
																				<?php
																			}
																			
																			if(trim($rc7->receiving_assistance)!=null || trim($rc7->want_assistance)!=null)
																			{
																				?>
																					<tr>
																						<td colspan="3"><b>Care</b></td>
																					</tr>
																				<?php
																				if(trim($rc7->receiving_assistance)!=null)
																				{
																					?>
																						<tr>
																							<td colspan="3"><p><span>Are you receiving any assistance from anyone as a result of your injuries?</span> <?php echo isset($rc7->receiving_assistance) ? $rc7->receiving_assistance : '' ?></p></td>
																						</tr>
																					<?php
																				}
																				if(trim($rc7->want_assistance)!=null)
																				{
																					?>
																						<tr>
																							<td colspan="3"><p><span>Did you require this assistance before this injury?</span> <?php echo isset($rc7->want_assistance) ? $rc7->want_assistance : '' ?></p></td>
																						</tr>
																					<?php
																				}
																			}
																			?>
																		</tbody>
																	</table>
																</td>
															</tr>
														<?php
													}
													
													if ($passengers_flag > 0 && $rc2->accident_kind == 1) 
													{
														?>
															<tr>
																<td colspan="3" class="borderClass">
																	<h4 style="margin: 0; padding: 10px 0;"><b>Passenger Details</b></h4>
																	<table style="width: 100%; border: 1px solid #d0d0d0; border-collapse: collapse;margin-bottom: 10px;">
																		<tbody>
																			<tr>
																				<td colspan="3"><p><span>Were There Any Other Persons In The Vehicle?</span> <?php echo trim($passengers_flag) != 0 ? 'Yes' : 'No' ?></p></td>
																			</tr>
																			<?php
																				$cnt = 1;
																				while ($rc8 = mysqli_fetch_object($view_passengers)) 
																				{
																					?>
																						<tr>
																							<td colspan="3"><p><span><?php echo $cnt++; ?>: </span> (<?php echo $rc8->is_driver == 1 ? 'Driver' : 'Passenger' ?>) <?php echo $rc8->passenger_details ?></p></td>
																						</tr>
																					<?php
																				}
																			?>
																		</tbody>
																	</table>
																</td>
															</tr>
														<?php
													}
													
													if ($witness_flag > 0) 
													{
														?>
															<tr>
																<td colspan="3" class="borderClass">
																	<h4 style="margin: 0; padding: 10px 0;"><b>Witness Details</b></h4>
																	<table style="width: 100%; border: 1px solid #d0d0d0; border-collapse: collapse;margin-bottom: 10px;">
																		<tbody>
																			<tr>
																				<td colspan="3"><p><span>Do you have any witness?</span> <?php echo trim($witness_flag) > 0 ? 'Yes' : 'No' ?></p></td>
																			</tr>
																			<?php
																				$a3_state = $a3_suburb = null;
																				mysqli_data_seek($getStates, 0);
																				while ($staterow = mysqli_fetch_object($getStates)) 
																				{
																					$a3_state = $rc9->state_id == $staterow->id ? $staterow->initials . ', ' : '';
																					if ($rc9->state_id == $staterow->id) 
																					{
																						$a3_state = $staterow->initials . ',';
																						break;
																					}
																				}
																				if (trim($rc9->state_id) != null) 
																				{
																					$getSubUrbs = getSubUrbs(" and state_id = " . $rc9->state_id);
																					while ($srow = mysqli_fetch_object($getSubUrbs)) 
																					{
																						if ($rc9->suburb_id == $srow->id) 
																						{
																							$a3_suburb = $srow->suburb_name;
																							break;
																						}
																					}
																				}
																			?>
																			<tr>
																				<td><p><span>Name: </span> <?php echo $rc9->name ?></p></td>
																				<td><p><span>Phone: </span> <?php echo $rc9->phone ?></p></td>
																				<td><p><span>Postal Code: </span> <?php echo $rc9->postal_code ?></p></td>
																			</tr>
																			<tr>
																				<td colspan="1"><p><span>State/Suburb: </span> <?php echo $a3_state . ' ' . $a3_suburb ?></p></td>
																				<td colspan="2"><p><span>Address: </span> <?php echo $rc9->address ?></p></td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														<?php
													}
												?>
										</tbody>
									</table>
								<?php
							}
						}
						?>
					</td>
				</tr>
			</tbody>
		</table>

<?php include '../includes/web_footer.php'; ?>
<script type="text/javascript" src="<?php echo $mysiteurl ?>js/main.js"></script>
<script type='text/javascript'>
$(document).ready(function() 
{
	$("input, select, textarea").prop("disabled", true);
	
	$('#btnGeneratePDF').click(function ()
	{
		$('#btnGeneratePDF').css('display','none');
		setTimeout(window.print, 1000);
	});
	
	<?php
	if(isset($_GET['d']) && trim($_GET['d']==1))
	{
		?>
			window.onafterprint = function()
			{
				window.location.href='ajaxFullLead.php?u=<?php echo $uid ?>&d=1';
			}
		<?php
	}
	else
	{
		?>
			window.onafterprint = function()
			{
				window.location.href='ajaxFullLead.php?u=<?php echo $uid ?>';
			}
		<?php
	}
	?>

});
</script>
		
</body>
</html>