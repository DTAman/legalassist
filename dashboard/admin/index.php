<?php 
include('../includes/basic_auth.php');

if($_SESSION['userType']==$SA)
{
	header("location:reports.php");
	exit();
}


$dataresult = getUserAssignedComments($_SESSION['userId']);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
		

	</head>

	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
		<div class="content-wrapper">
			<div class="container-fluid">
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles">
					<div class="col-md-12 align-self-center">
						<?php
						if($_SESSION['userType']==$VA || $_SESSION['userType']==$CCA)
						{
							?>
								<a href='callbacks.php' target='_blank' class="text-danger pull-right"><b>Callbacks</b></a> 
								<a href='charts_callcenter.php' target='_blank' class="text-danger pull-right ml-5 mr-5"><b>Charts</b></a>
							<?php
						}
						else if($_SESSION['userType']==$MSW)
						{
							?>
								<a href='callbacks.php' target='_blank' class="text-danger pull-right"><b>Callbacks</b></a> 
								<a href='charts.php' target='_blank' class="text-danger pull-right ml-5 mr-5"><b>Charts</b></a>
								<a href='dump.php' target='_blank' class="text-danger pull-right"><b>Export DB Now</b></a>
							<?php
						}
						else if($_SESSION['userType']==$CTL || $_SESSION['userType']==$CA)
						{
							?>
								<a href='callbacks.php' target='_blank' class="text-danger pull-right"><b>Callbacks</b></a> 
							<?php
						}
						else
						{
							?>
								<h4 class="theme-cl">Dashboard</h4>
							<?php
						}
						?>
					</div>
				</div>
				
				
				<?php
				$ss_show = 0;
				if($_SESSION['userType']=='MSW')
				{
					$thisdated = date('Y-m-d');
					
					$loginmaster = $mysqli->prepare("select count(*) from shift_master where date(creation_date) = ?");
					$loginmaster->bind_param("s",$thisdated);
					$loginmaster->execute();
					$loginmaster->bind_result($ss_show);
					$loginmaster->fetch();
					$loginmaster->close();
				}
				else if($_SESSION['userType']=='CA')
				{
					// $loginmaster = $mysqli->prepare("select count(*) from leads where ( assigned_usertype = 'CA' and if( (select comment_to from lead_comments where lead_comments.comment_utype='CA' and lead_comments.lead_id=leads.id order by lead_comments.creation_date desc limit 1)=?,1,0 ) ) and is_shifted = 1");
					$loginmaster = $mysqli->prepare("select count(*) from leads where agent_assigned = ? and is_shifted = 1");
					$loginmaster->bind_param("i",$_SESSION['userId']);
					$loginmaster->execute();
					$loginmaster->bind_result($ss_show);
					$loginmaster->fetch();
					$loginmaster->close();
				}
				else if($_SESSION['userType']=='CTL')
				{
					$loginmaster = $mysqli->prepare("select count(*) from leads where team_lead_assigned = ? and is_shifted = 1");
					$loginmaster->bind_param("i",$_SESSION['userId']);
					$loginmaster->execute();
					$loginmaster->bind_result($ss_show);
					$loginmaster->fetch();
					$loginmaster->close();
				}
				else if($_SESSION['userType']=='CCA')
				{
					$loginmaster = $mysqli->prepare("select count(*) from leads where callcenter_id = ? and is_shifted = 1");
					$loginmaster->bind_param("i",$_SESSION['userId']);
					$loginmaster->execute();
					$loginmaster->bind_result($ss_show);
					$loginmaster->fetch();
					$loginmaster->close();
				}
				
				if($ss_show>0 && $_SESSION['userType']=='MSW')
				{
					?>
						<div class="alert alert-success">
							Leads has been shifted today. Please check <b><a href='total_shift_data_reports.php'>Shift Reports</a></b> for more details.
						</div>
					<?php
				}
				if($ss_show>0 && $_SESSION['userType']=='CCA' && $_SESSION['is_affliated']==0)
				{
					?>
						<div class="alert alert-success">
							Leads has been shifted to your team leads.
						</div>
					<?php
				}
				else if($ss_show>0 && $_SESSION['userType']!='MSW' && $_SESSION['is_affliated']==1)
				{
					?>
						<div class="alert alert-success">
							Please check leads in the <b> <a href='shift_data_reports.php'>Shift Reports</a></b> Section.
						</div>
					<?php
				}
				?>
				
				<div class="row home_div">
					<?php
					if(mysqli_num_rows($dataresult)>0)
					{
						while($r = mysqli_fetch_object($dataresult))
						{ 
							$c = $r->lead_transfer_type;
							$t='';
							
							switch($c)
							{
								case 'LT': $color = 'info';$t = 'Lead Transferred';break;
								case 'FI': $color = 'light';$t = 'Further Investigation';break;
								case 'RJ': $color = 'danger';$t = 'Lead Rejected By Sol';break;
								case 'SU': $color = 'primary';$t = 'Sign Up Arranged By Sol';break;
								case 'AP': $color = 'dark'; $t = 'Lead Approved';break;
								case 'SI': $color = 'warning';$t = 'Invoice Sent By Sol';break;
								case 'IP': $color = 'success';$t = 'Invoice Paid By Sol';break;
								case 'CBA': $color = 'success';$t = 'Callback';break;
								case 'CBV': $color = 'success';$t = 'Callback';break;
								case 'CBTL': $color = 'success';$t = 'Callback';break;
								case 'CBAG': $color = 'success';$t = 'Callback';break;
								case 'CBC': $color = 'success';$t = 'Callback';break;
							}
							
							?>
								<div class="col-md-6">
									<div class="alertBoxesOuter alert alert-<?php echo $color?>">
										<h3><span><?php echo trim($r->first_name.' '.$r->middle_name.' '.$r->last_name) ?></span> - <?php echo $t ?></h3>
										<a target='_blank' href="view-each-lead.php?u=<?php echo $r->lead_id ?>">
											<p><?php echo $r->comments ?></p>
										</a>
									</div>
								</div>
							<?php
						}
					}
					else
					{
						?>
							<div class="col-md-12">
								<div class="alert alert-danger" role="alert">
									You have no new notifications to checkout. Come back later.
								</div>
							</div>
						<?php
					}
					?>
				</div>
				
				</div>
				<!-- /.row -->
				
			
				

			</div>  
			<!-- /.content-wrapper -->
			
			<!-- Footer -->
			<?php include('../includes/copyright.php'); ?>
			
			<!-- Scroll to Top Button-->  
			<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			  <i class="ti-angle-double-up"></i>
			</a>

			<?php include('../includes/web_footer.php'); ?>
			
	  </div>
	  <!-- Wrapper -->
	  
	  <script type='text/javascript'>
		$(document).ready(function() 
		{
			setInterval(function(){callAjax('ajax_get_comments.php',{},'.home_div');}, 5000);	
		});
	  </script>
	  
	</body>
</html>
