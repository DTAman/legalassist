<?php 
include('../includes/basic_auth.php');

$con='';
if($_SESSION['userType']==$CCA)
{
	$con.=' where shift_data_to_user = '.$_SESSION['userId'].' or shifted_by = '.$_SESSION['userId'];
}

$queryResults = "select (select count(*) from shift_details where shift_details.shift_id = shift_master.shift_id) as leads_shifted, centre_name, shift_master.*,
				IF(shift_data_usertype = 'CCA', (select centre_name from callcenter where uid = shift_master.shift_data_of_user), (select first_name from tl_and_agents where uid = shift_master.shift_data_of_user)) as data_shifted_of,IF(shift_data_usertype = 'CCA', (select centre_name from callcenter where uid = shift_master.shift_data_to_user), (select first_name from tl_and_agents where uid = shift_master.shift_data_to_user)) as data_shifted_to from shift_master inner join callcenter on callcenter.uid = shift_master.callcenter_id ".$con." order by creation_date desc";
$totalResults = getQuery($queryResults);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
	  
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">View Shifting</h4>
					</div>
				</div>
				
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
						
							
							<div class="card-body">

								<!-- Table Start -->

								<div class="table-responsive ajaxClass viewLeadTable">
									<?php
									if(mysqli_num_rows($totalResults)!=0)
									{
										?>
										<!--<div class='hh5'>-->	
										<div class=''>	
											<table class="w-100 table newTable table-striped table-hover">
												<thead>
													<tr>
														<th class='text-center'>Sr.No.</th>
														<th>Data Shifted Of</th>
														<th>Data Shifted To</th>
														<th>User Type</th>
														<th>Comments</th>
														<th>Call Center</th>
														<th>Lead Count</th>
														<th>Creation Date</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$n = 1;
													while($rc = mysqli_fetch_object($totalResults))
													{
														?>
															<tr>
																<td class='text-center'><?php echo $n ?></td>
																<td><?php echo $rc->data_shifted_of ?></td>
																<td><?php echo $rc->data_shifted_to ?></td>
																<td><?php echo $rc->shift_data_usertype=='CCA'?'Callcenter':($rc->shift_data_usertype=='CTL'?'Team Lead':'Agent') ?></td>
																<td><?php echo $rc->comments ?></td>
																<td><?php echo $rc->centre_name ?></td>
																<td><?php echo $rc->leads_shifted ?></td>
																<td>
																	<?php echo get_timezone_offset(date('Y-m-d H:i:s',strtotime($rc->creation_date)),'UTC',$_SESSION['userTimeZone']) ?>
																</td>
															</tr>
														<?php
														$n++;
													}
													?>
												</tbody>
											</table>
										</div>
										<?php
									}
									else
									{
										include('../includes/norows.php'); 
									}
									?>
								</div>

								<!-- Table End -->

							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			<!-- /.content-wrapper -->
			<?php include('../includes/copyright.php'); ?>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
	  
	</body>
</html>
