<div id="copymodalShow" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Passenger For Lead <span class='txtCopyLeadId'></span></h4>
			</div>
			<div class="modal-body">
				<form action='' method='post' name='frmCopyLead' id='frmCopyLead'>
					<div class="form-group">
						<div class='row'>
							<div class='col-md-12'>
								<input value="" class="form-control" placeholder='First Name' id="txtCopyName" name="txtCopyName" type="text" />
								<input value="" class="form-control" id="txtCopyLeadId" name="txtCopyLeadId" type="hidden" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class='row'>
							<div class='col-md-12'>
								<input value="" class="form-control" placeholder='Last Name' id="txtCopyLastName" name="txtCopyLastName" type="text" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class='row'>
							<div class='col-md-12'>
								<input value="" class="form-control" placeholder='Email' id="txtCopyLastEmail" name="txtCopyLastEmail" type="text" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class='row'>
							<div class='col-md-12'>
								<input value="" class="form-control" maxlength="10" minlength="10" placeholder='Mobile' id="txtCopyNumber" name="txtCopyNumber" type="text" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class='row'>
							<div class="col-md-12 col-sm-12">
								<div class="form-group">
									<label for="txt1DOB">D.O.B</label>
										<div class="calender-wrap">
											<div>
												<select name="txtCopyDay" id="txtCopyDay" class="form-control">
													<option value=''>Day</option>
													<?php
													for($day=1;$day<=31;$day++)
													{
														?>
															<option value='<?php echo $day ?>'><?php echo $day ?></option>
														<?php
													}
													?>
												</select>
											</div>
											
											<div>
												<select name="txtCopyMonth" id="txtCopyMonth" class="form-control">
													<option value=''>Month</option>
													<?php
													for($mon=1;$mon<=12;$mon++)
													{
														?>
															<option value='<?php echo $mon ?>'><?php echo  substr(date('F', mktime(0,0,0,$mon, 1, date('Y'))),0,3) ?></option>
														<?php
													}
													?>
												</select>
											</div>
											
											<div>
												<select name="txtCopyYear" id="txtCopyYear" class="form-control">
													<option value=''>Year</option>
													<?php
													$running_year = date('Y');
													for($y=$running_year;$y>=$running_year-100;$y--)
													{
														?>
															<option value='<?php echo $y ?>'><?php echo $y ?></option>
														<?php
													}
													?>
												</select>
											</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class='row'>
						<div class='col-md-12'>
							<input type="submit" class="btn btn-primary" value="Copy Lead" name="btnCopy" id="btnCopy" />
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>