<?php 
include('../includes/basic_auth.php'); 
$is_dis = 0;
if(isset($_GET['s']) && trim($_GET['s']==1))
{
	$is_dis = 1;
}

if(isset($_GET['u']) && trim($_GET['u']!=null))
{
	$uid = trim($_GET['u']);
	
	$view_result = getUserData($uid, $VA);
	if(mysqli_num_rows($view_result)==0)
	{
		header("location:index.php");
	}
	else
	{
		$rc = mysqli_fetch_object($view_result);
	}
}
else
{
	header("location:index.php");
}

$getStates = getStates();
$getCountries = getCountries();

if(isset($_POST["btnGeneratePassword"]))
{
	$pswd = generate_password($uid);
	$_SESSION['success']='Password has been changed successfully.<br/>New Password is: '.$pswd;
	
	$_POST=array();
}

if(isset($_POST["btnCallCenter"]))
{
	if(trim($_POST["txtCallCenterName"])!=null && trim($_POST["txtCallCenterAddress"])!=null && trim($_POST["txtCallCenterPhone"])!=null && trim($_POST["ddlCountry"])!=null)
	{
		$count_users = countUsersWithMobileforEdit($_POST['txtCallCenterPhone'],$uid);

		if($count_users==0)
		{
			$loginmaster = $mysqli->prepare("update login_master set phone=?, status=?, updation_date=? where uid=?");
			$loginmaster->bind_param("sssi",$_POST['txtCallCenterPhone'],$_POST['rbtnStatus'],$thisdate, $uid);
			$loginmaster->execute();
			$loginmaster->close();
			
			$_POST['ddlState']=null;
			$_POST['ddlSubUrb']=null;
			$_POST['txtCallCenterPostal']=null;
			
			$loginmaster = $mysqli->prepare("update verifier set verifier_name=?, phone=?, alt_number=?, state_id=?, suburb_id=?, postal_code=?, address=?, country=? where uid=?");
			$loginmaster->bind_param("sssiissii",$_POST['txtCallCenterName'],$_POST['txtCallCenterPhone'],$_POST['txtCallCenterAltPhone'],$_POST['ddlState'],$_POST['ddlSubUrb'],$_POST['txtCallCenterPostal'],$_POST['txtCallCenterAddress'],$_POST['ddlCountry'],$uid);
			$loginmaster->execute();
			$loginmaster->close();
			
			$_SESSION['success']='Verifier user updated successfully.';
			
			header("location:edit-verifier-each.php?u=".$uid);
			exit();
		}
		else
		{
			$_SESSION['error']="A user exists with this mobile number.";
		}
	}
	else
	{
		$_SESSION['error']="Enter all the required fields.";
	}
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>

	<body class="fixed-nav sticky-footer" id="page-top">
	
		<?php include('../includes/navigation.php'); ?>
	  
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl"><?php echo $is_dis!=1?'Edit':'View' ?> Verifier</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
			
				<!-- row -->
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
							<div class="card-body">
								<form name='frmEditCallCenter' id='frmEditCallCenter' action='' method='post'>
									<?php include("../includes/messages.php") ?>
									<div class="row">
										<div class="col-md-3 col-sm-12">
										  <div class="form-group">
											<label for="txtCallCenterName">Verifier Name</label>
											<input class="form-control" value="<?php echo $rc->verifier_name ?>" name="txtCallCenterName" id="txtCallCenterName" placeholder="Callcenter" type="text"/>
											<input value="<?php echo $rc->uid ?>" name="hfUID" id="hfUID" type="hidden"/>
										  </div>
										</div>
										<div class="col-md-3 col-sm-12">
										  <div class="form-group">
											<label for="txtCallCenterPhone">Phone Number</label>
											<input class="form-control" value="<?php echo $rc->phone ?>" name="txtCallCenterPhone" id="txtCallCenterPhone" placeholder="Mobile Number" type="text"/>
										  </div>
										</div>
										<div class="col-md-3 col-sm-12">
										  <div class="form-group">
											<label for="txtCallCenterAltPhone">Alternate Number</label>
											<input class="form-control" value="<?php echo $rc->alt_number ?>"  name="txtCallCenterAltPhone" id="txtCallCenterAltPhone" placeholder="Alternate Mobile" type="text" />
										  </div>
										</div>
										
										<div class="col-md-3 col-sm-12">
										  <div class="form-group">
											<label for="">Country</label>
											<select name="ddlCountry" id="ddlCountry" class="d-flex form-control">
												<option value=''>Select Country</option>
												<?php
													while($cnrow = mysqli_fetch_object($getCountries))
													{
														?>	
															<option <?php echo $rc->country==$cnrow->id?'selected':''; ?> value='<?php echo $cnrow->id ?>'><?php echo $cnrow->name ?></option>
														<?php
													}
												?>
												
											</select>
										  </div>
										</div>
										
										<!--<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="">Address</label>
											<select name="ddlState" id="ddlState" class="d-flex form-control" onchange="getAllSuburbs(this.value)">
												<option value=''>Select State</option>
												<?php
													while($staterow = mysqli_fetch_object($getStates))
													{
														?>	
															<option <?php echo $rc->state_id==$staterow->id?'selected':''; ?> value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
														<?php
													}
												?>
											</select>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="ddlSubUrb" class="invisible d-none d-md-block" >Select Sub urb</label>
											<select name="ddlSubUrb" id="ddlSubUrb" class="d-flex form-control">
												<?php
												/* $getSubUrbs = getSubUrbs(" and state_id = ".$rc->state_id);
												while($srow = mysqli_fetch_object($getSubUrbs))
												{
													?>	
														<option <?php echo $rc->suburb_id==$srow->id?'selected':''; ?> value='<?php echo $srow->id ?>'><?php echo $srow->suburb_name ?></option>
													<?php
												} */
												?>
											</select>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="txtCallCenterPostal" class="invisible d-none d-md-block" >Postal Code</label>
											<input class="form-control" value="<?php echo $rc->postal_code ?>"  name="txtCallCenterPostal" id="txtCallCenterPostal" placeholder="Postal Code" type="text" />
										  </div>
										</div>-->
										<div class="col-md-12 col-sm-12 col-xs-12">
										  <div class="form-group fullwidthTextarea">
											<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Address First Line" name='txtCallCenterAddress' id='txtCallCenterAddress'><?php echo $rc->address ?></textarea>
										  </div>
										</div>
										
										<div class="col-md-4 col-sm-12 col-xs-12">
											<label class="d-block">Status</label>
											<div class="custom-radio d-inline-block fl-left mr-3">
												<input <?php echo $rc->status=='AC'?'checked':''; ?> type="radio" class="custom-control-input" id="rbtnActive" name="rbtnStatus" value="AC"/>
												<label class="custom-control-label" for="rbtnActive">Active</label>
											</div>
											<div class="custom-radio d-inline-block fl-left mr-3">
												<input <?php echo $rc->status=='IN'?'checked':''; ?> type="radio" class="custom-control-input" id="rbtnInactive" name="rbtnStatus" value="IN"/>
												<label class="custom-control-label" for="rbtnInactive">Inactive</label>
											</div>
										</div>
										<?php
										if($is_dis!=1)
										{
											?>
												<div class="col-md-12 col-sm-12 col-xs-12">
													<input name="btnCallCenter" id="btnCallCenter" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save" />
												</div>
											<?php
										}
										else
										{
											?>
											<div class="col-md-12 col-sm-12 col-xs-12 text-right">
												<input name="btnGeneratePassword" id="btnGeneratePassword" type="submit" class="btn btn-danger ml-0 mr-3 mt-2" value="Generate Password" onclick="return confirm('Are you sure?')" />
											</div>
											<?php
										}
										?>
									  </div>
								</form>
							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			
		<?php include('../includes/copyright.php'); ?>
		
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
		<?php
			if($is_dis==1)
			{
				?>
					<script>is_disabled(<?php echo $is_dis ?>);</script>
				<?php
			}
		?>
	</body>
</html>
