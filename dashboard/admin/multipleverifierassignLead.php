<?php
include('../includes/basic_auth.php');
$comment_utype = 'VA';
$comment_status = 'PN';
$lead_transfer_type = 'LT';
$rbtnValues = '';
$c_outcome = 'SV';

if($_SESSION['userType']==$MSW)
{
	if(isset($_POST['chckDeleteMultiple']) && count($_POST['chckDeleteMultiple'])>0)
	{
		foreach($_POST['chckDeleteMultiple'] as $chckDeleteMultiple_id)
		{
			$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id = ?");
			$loginmaster->bind_param("i",$chckDeleteMultiple_id);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type,reason_lead_from_msw_to_cc) VALUES (?,?,?,?,?,?,?,?,?)");
			$loginmaster->bind_param("iisisssss",$chckDeleteMultiple_id,$_POST['ddlAssignToVerifier'],$comment_utype ,$_SESSION['userId'],$_POST['txtAssignVerifierComments'],$comment_status,$thisdate, $lead_transfer_type, $rbtnValues);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("update leads set assigned_usertype = 'VA', admin_assigned_directly=0, assign_verifier = ?, msw_lead_type = 'v', assigned_verifier=?, is_previous_verifier = 0, last_comment = ?,outcome=(select outcome_title from outcomes where outcome_initials = ?), outcome_type=?, latest_comment_usertype_from = ?, latest_comment_usertype_to = ? where id=?");
			$loginmaster->bind_param("sisssssi",$thisdate, $_POST['ddlAssignToVerifier'],$_POST['txtAssignVerifierComments'],$c_outcome, $c_outcome, $MSW, $VA, $chckDeleteMultiple_id);
			$loginmaster->execute();
			$loginmaster->close();
		}
	}
}
?>
