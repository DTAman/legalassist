<?php 
include('../includes/basic_auth.php');

$condition='';

if(isset($_GET['u']) && trim($_GET['u'])!=null)
{
	$uid = trim($_GET['u']);
	
	if(isset($_GET['d']) && trim($_GET['d']==1))
	{
		$view_result = getLeadsDataByPaginationDeleted(' and id='.$uid,1,0);
	}
	else
	{
		$view_result = getLeadsDataByPagination(' and id='.$uid,1,0);
	}
	
	if(mysqli_num_rows($view_result)==0)
	{
		header("location:index.php");
	}
}
else
{
	header("location:index.php");
}

$con='';

$queryResults = "select lead_comments.*, (select type from login_master where uid=lead_comments.comment_to) as uutype_to, (select type from login_master where uid=lead_comments.comment_from) as uutype_from from lead_comments where lead_id=".$uid.' '.$con." order by cid desc";
$totalResults = getQuery($queryResults);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
	  
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">View Lead Logs</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
			
				<!-- row -->
				
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
						
							
							<div class="card-body">

								<!-- Table Start -->

								<div class="table-responsive ajaxClass">
									<?php
									if(mysqli_num_rows($totalResults)!=0)
									{
										?>
										<table class="w-100 table newTable table-striped table-bordered table-hover">
											<thead>
												<tr class='alert alert-danger'>
													<th>Date Time</th>
													<th>Comment By</th>
													<th>Comment To</th>
													<th>Assigned User</th>
													<th>Comment</th>
													<th>Status</th>
												</tr>
											</thead>
											<tbody>
											<?php
											$n = 1;
											while($rc = mysqli_fetch_object($totalResults))
											{
												?>
													<?php
													if($rc->is_shift==1)
													{
														?>
															<tr class='bg-info-light'>
														<?php
													}													
													else
													{
														?>
															<tr class='<?php echo $rc->directly_assigned_admin==1?"bg-primary":($rc->reject_after_sol_assign==1?"bg-success":"")  ?>'>
														<?php
													}
													?>
														<td>
															<?php echo get_timezone_offset(date('Y-m-d H:i:s',strtotime($rc->creation_date)),'UTC',$_SESSION['userTimeZone']) ?>
														</td>
														<td>
														<?php 
															if($rc->comment_from!=null && $rc->uutype_from!=null)
															{
																echo getNames($rc->comment_from,$rc->uutype_from);
															}
														?>
														</td>
														<td>
														<?php 
															if($rc->comment_to!=null && $rc->uutype_to!=null)
															{
																echo getNames($rc->comment_to,$rc->uutype_to);
															}
														?>
														</td>																		
														<td>
														<?php 
															if($rc->comment_utype=='MSW')
															{
																echo 'Administrator';
															}
															else if($rc->comment_utype=='C')
															{
																echo 'Solicitor';
															}
															else if($rc->comment_utype=='CA')
															{
																echo 'Callcenter Agent';
															}
															else if($rc->comment_utype=='CCA')
															{
																echo getFetchQuery('select is_affliated from callcenter where uid='.$rc->comment_to)==1?'Affiliated Callcenter':'Callcenter';
															}
															else if($rc->comment_utype=='CTL')
															{
																echo 'Callcenter Team Lead';
															}
															else if($rc->comment_utype=='VA')
															{
																echo 'Verifier';
															}
															else 
															{
																echo '';
															}
														?>
														</td>			
														<td><?php echo $rc->comments ?></td>			
														<td>
														<?php 
															if($rc->is_shift==1)
															{
																echo 'Lead Shifted';
															}
															else if($rc->reject_after_sol_assign==1)
															{
																echo 'Pulled By Admin';
															}
															else if($rc->directly_assigned_admin==1)
															{
																echo 'Transferred To Admin';
															}
															else if($rc->lead_transfer_type=='LT' && trim($rc->is_approved_verifier)!=null)
															{
																echo $rc->is_approved_verifier=='AV'?'Lead Approved By Verifier':'Lead Rejected By Verifier';
															}
															else if($rc->email_sent>0)
															{
																echo 'Email Sent';
															}
															else if($rc->sms_sent>0)
															{
																echo 'SMS Sent';
															}
															else if($rc->lead_transfer_type=='LT')
															{
																echo 'Lead Transferred';
															}
															else if($rc->lead_transfer_type=='AP')
															{
																echo 'Lead Approved';
															}
															else if($rc->lead_transfer_type=='SI')
															{
																echo 'Lead Invoice Sent';
															}
															else if($rc->lead_transfer_type=='IP')
															{
																echo 'Lead Invoice Paid';
															}
															else if($rc->lead_transfer_type=='RJ')
															{
																echo 'Lead Rejected';
															}
															else if($rc->lead_transfer_type=='SC')
															{
																echo 'Spoken To Client';
															}
															else if($rc->lead_transfer_type=='NA')
															{
																echo 'No Answer';
															}
															else if($rc->lead_transfer_type=='CB')
															{
																echo 'Callback Requested';
															}
															else if($rc->lead_transfer_type=='FI')
															{
																echo 'Further Investigation';
															}
															else if($rc->lead_transfer_type=='SU')
															{
																echo 'Sign Up Done';
															}
															else if($rc->lead_transfer_type=='CBA')
															{
																echo 'Callback Requested By Admin';
															}
															else if($rc->lead_transfer_type=='CBC')
															{
																echo 'Callback Requested By Callcenter';
															}
															else if($rc->lead_transfer_type=='CBV')
															{
																echo 'Callback Requested By Verifier';
															}
															else if($rc->lead_transfer_type=='CBAD')
															{
																echo 'Callback Done By Admin';
															}
															else if($rc->lead_transfer_type=='CBCD')
															{
																echo 'Callback Done By Callcenter';
															}
															else if($rc->lead_transfer_type=='CBVD')
															{
																echo 'Callback Done By Verifier';
															}
															else if($rc->lead_transfer_type=='NTS')
															{
																echo 'Notes added';
															}
															else if($rc->lead_transfer_type=='CBAG')
															{
																echo 'Callback Requested By Agent';
															}
															else if($rc->lead_transfer_type=='CBTL')
															{
																echo 'Callback Requested By Team Lead';
															}
															else if($rc->lead_transfer_type=='CBAGD')
															{
																echo 'Callback Done By Agent';
															}
															else if($rc->lead_transfer_type=='CBTLD')
															{
																echo 'Callback Done By Team Lead';
															}
														?>
														</td>			
													</tr>
												<?php
												$n++;
											}
											?>
											</tbody>
										</table>
										<?php
									}
									else
									{
										include('../includes/norows.php'); 
									}
									?>
								</div>

								<!-- Table End -->

							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			<!-- /.content-wrapper -->
			<?php include('../includes/copyright.php'); ?>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
	  
	</body>
</html>
