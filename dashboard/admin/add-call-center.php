<?php 
include('../includes/basic_auth.php'); 

$getStates = getStates();
$getCountries = getCountries();
$getCallcenters = getCallcenters('');

if(isset($_POST["btnCallCenter"]))
{
	if(trim($_POST["txtCallCenterName"])!=null && trim($_POST["ddlCountry"])!=null && trim($_POST["txtCallCenterAddress"])!=null && trim($_POST["txtCallCenterPhone"])!=null)
	{
		$count_users = countUsersWithMobile($_POST['txtCallCenterPhone']);

		if($count_users==0)
		{
			$ori_pass = create_unique_pass(); //send pass by message or email
			$password=create_password($ori_pass);
			
			$type=$CCA; //callcenter admin
			$status='AC';
			$loginmaster = $mysqli->prepare("insert into login_master(phone, password, type, status, added_by, creation_date, updation_date) values(?,?,?,?,?,?,?)");
			$loginmaster->bind_param("ssssiss",$_POST['txtCallCenterPhone'],$password,$type,$status,$_SESSION['userId'], $thisdate, $thisdate);
			$loginmaster->execute();
			$loginmaster->close();
			
			$uid = mysqli_insert_id($mysqli);
			
			$_POST['ddlState']=null;
			$_POST['ddlSubUrb']=null;
			$_POST['txtCallCenterPostal']=null;
			
			$loginmaster = $mysqli->prepare("INSERT INTO callcenter(uid, centre_name, phone, alt_number, state_id, suburb_id, postal_code, address, is_affliated, country) VALUES (?,?,?,?,?,?,?,?,?,?)");
			$loginmaster->bind_param("isssiissii",$uid,$_POST['txtCallCenterName'],$_POST['txtCallCenterPhone'],$_POST['txtCallCenterAltPhone'],$_POST['ddlState'],$_POST['ddlSubUrb'],$_POST['txtCallCenterPostal'],$_POST['txtCallCenterAddress'],$_POST['rbtnIsAffliated'],$_POST['ddlCountry']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$_SESSION['success']='Callcenter user created successfully.';
			
			$_POST=array();
			header( "refresh:3;url=add-call-center.php" );
		}
		else
		{
			$_SESSION['error']="A user exists with this mobile number.";
		}
	}
	else
	{
		$_SESSION['error']="Enter all the required fields.";
	}
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>

	<body class="fixed-nav sticky-footer" id="page-top">
	
		<?php include('../includes/navigation.php'); ?>
	  
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Add Call Center</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
			
				<!-- row -->
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
							<div class="card-body">
								<form name='frmAddCallCenter' id='frmAddCallCenter' action='' method='post'>
									<?php include("../includes/messages.php") ?>
									<div class="row">
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtCallCenterName">Call Center Name</label>
											<input class="form-control" name="txtCallCenterName" id="txtCallCenterName" placeholder="Callcenter" type="text"/>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtCallCenterPhone">Phone Number</label>
											<input class="form-control" name="txtCallCenterPhone" id="txtCallCenterPhone" placeholder="Mobile Number" type="text"/>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtCallCenterAltPhone">Alternate Number</label>
											<input class="form-control" name="txtCallCenterAltPhone" id="txtCallCenterAltPhone" placeholder="Alternate Mobile" type="text" />
										  </div>
										</div>
										<div class="col-sm-4">
											<label class="d-block">Is this an affiliated call center?</label>
											<div class="custom-radio d-inline-block fl-left mr-3">
												<input type="radio" class="custom-control-input" id="rbtnYes" name="rbtnIsAffliated" value="1"/>
												<label class="custom-control-label" for="rbtnYes">Yes</label>
											</div>
											<div class="custom-radio d-inline-block fl-left mr-3">
												<input type="radio" class="custom-control-input" id="rbtnNo" name="rbtnIsAffliated" value="0" />
												<label class="custom-control-label" for="rbtnNo">No</label>
											</div>
											<span class='radioclass'></span>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="">Country</label>
											<select name="ddlCountry" id="ddlCountry" class="d-flex form-control">
												<option value=''>Select Country</option>
												<?php
													while($cnrow = mysqli_fetch_object($getCountries))
													{
														?>	
															<option value='<?php echo $cnrow->id ?>'><?php echo $cnrow->name ?></option>
														<?php
													}
												?>
												
											</select>
										  </div>
										</div>
										
										<!--<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="">Address</label>
											<select name="ddlState" id="ddlState" class="d-flex form-control" onchange="getAllSuburbs(this.value)">
												<option value=''>Select State</option>
												<?php
													while($staterow = mysqli_fetch_object($getStates))
													{
														?>	
															<option value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
														<?php
													}
												?>
												
											</select>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="ddlSubUrb" class="invisible d-none d-md-block" >Select Sub urb</label>
											<select name="ddlSubUrb" id="ddlSubUrb" class="d-flex form-control">
												<option value=''>Select Sub urb</option>
											</select>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="txtCallCenterPostal" class="invisible d-none d-md-block" >Postal Code</label>
											<input class="form-control" name="txtCallCenterPostal" id="txtCallCenterPostal" placeholder="Postal Code" type="text" />
										  </div>
										</div> -->
										<div class="col-md-12 col-sm-12 col-xs-12">
										  <div class="form-group fullwidthTextarea">
											<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Address First Line" name='txtCallCenterAddress' id='txtCallCenterAddress'></textarea>
										  </div>
										</div>
										
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input name="btnCallCenter" id="btnCallCenter" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save" />
										</div>	
									  </div>
								</form>
							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			
		<?php include('../includes/copyright.php'); ?>
		
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
	  
	</body>
</html>
