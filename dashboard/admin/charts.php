<?php 
include('../includes/basic_auth.php');

$date = date("Y-m-d");
$arr_1 = '';
$arr_2 = '';
$arr_3 = '';

if(isset($_POST['btnSearch']))
{
	if(trim($_POST['ddlSelectType'])==1)
	{
		$txtFromDate = "1970-01-01";
		$txtToDate = date("Y-m-d");
	}
	else if(trim($_POST['ddlSelectType'])==2)
	{
		$txtFromDate = $txtToDate = date("Y-m-d");
	}
	else if(trim($_POST['ddlSelectType'])==3)
	{
		$txtFromDate = date( 'Y-m-d', strtotime("this week"));
		$txtToDate = date( 'Y-m-d', strtotime("sunday 0 week"));
	}
	else if(trim($_POST['ddlSelectType'])==4)
	{
		$txtFromDate = date('Y-m-01');
		$txtToDate = date('Y-m-t');
	}
	else if(trim($_POST['ddlSelectType'])==5)
	{
		$txtFromDate = date('Y-m-d', strtotime($_POST['txtFromDate']));
		$txtToDate = date('Y-m-d', strtotime($_POST['txtToDate']));
	}
}
else
{
	$txtFromDate = $txtToDate = date("Y-m-d");
	$_POST['ddlSelectType']=2;
}

$getClients = getClients();
while($a=mysqli_fetch_object($getClients))
{
	$query = 
	"select count(distinct leads.id), 
	(select count(distinct x.id) from leads x inner join lead_comments lc2 on lc2.lead_id = x.id where x.assigned_client = ".$a->uid." and (x.client_status='AP' or x.client_status='IP' or x.client_status='SI') and x.assign_client<=lc2.creation_date and date(lc2.creation_date)>='".$txtFromDate."' and date(lc2.creation_date)<='".$txtToDate."') as approved,
	(select count(distinct y.id) from leads y inner join lead_comments lc3 on lc3.lead_id = y.id where lc3.comment_from = ".$a->uid." and lc3.lead_transfer_type='SU' and date(lc3.creation_date)>='".$txtFromDate."' and date(lc3.creation_date)<='".$txtToDate."') as signup,
	(select count(distinct z.id) from leads z inner join lead_comments lc4 on lc4.lead_id = z.id where (lc4.comment_from = ".$a->uid." or lc4.comment_to = ".$a->uid.") and lc4.lead_transfer_type='RJ' and date(lc4.creation_date)>='".$txtFromDate."' and date(lc4.creation_date)<='".$txtToDate."') as rejected
	from leads inner join lead_comments lc1 on lc1.lead_id = leads.id where lc1.comment_to = ".$a->uid." and date(lc1.creation_date)>='".$txtFromDate."' and date(lc1.creation_date)<='".$txtToDate."' and lc1.lead_transfer_type='LT'";
	$datamaster = $mysqli->prepare($query);
	$datamaster->execute();
	$datamaster->bind_result($d1, $d2, $d3, $d4);
	$datamaster->fetch();
	$datamaster->close();
	
	$arr_1.= "['".$a->company_name."',".$d1.",".$d2.",".$d3.",".$d4."],";
}

$getCallcenters = getCallcenters('');
while($b=mysqli_fetch_object($getCallcenters))
{
	$query="
	select count(*),
	(select count(*) from leads x inner join lead_comments lc1 on lc1.lead_id = x.id where x.callcenter_id = ".$b->uid." and (lc1.lead_transfer_type='AP' or lc1.lead_transfer_type='IP' or lc1.lead_transfer_type='SI') and x.assign_client<=lc1.creation_date and date(lc1.creation_date)>='".$txtFromDate."' and date(lc1.creation_date)<='".$txtToDate."') as approved,
	(select count(*) from leads z inner join lead_comments lc2 on lc2.lead_id = z.id where z.callcenter_id = ".$b->uid." and lc2.lead_transfer_type='SU' and z.assign_client<=lc2.creation_date and date(lc2.creation_date)>='".$txtFromDate."' and date(lc2.creation_date)<='".$txtToDate."') as signup,
	(select count(*) from leads y inner join lead_comments lc3 on lc3.lead_id = y.id  where y.callcenter_id = ".$b->uid." and y.reached_admin=1 and date(lc3.creation_date)>='".$txtFromDate."' and date(lc3.creation_date)<='".$txtToDate."' and lc3.comment_utype='MSW' and lc3.comment_from = ".$b->uid.") as assigned_to_admin
	from leads where callcenter_id = ".$b->uid." and 
	(date(assign_n_callcenter)>='".$txtFromDate."' and date(assign_n_callcenter)<='".$txtToDate."' or date(assign_affliated)>='".$txtFromDate."' and date(assign_affliated)<='".$txtToDate."')";	
	$datamaster = $mysqli->prepare($query);
	$datamaster->execute();
	$datamaster->bind_result($d1, $d2, $d3, $d4);
	$datamaster->fetch();
	$datamaster->close();
	
	$arr_2.= "['".$b->centre_name."',".$d1.",".$d2.",".$d3.",".$d4."],";
}


$getVerifier = getVerifiers('');
while($c=mysqli_fetch_object($getVerifier))
{
	$query = "
	select count(distinct leads.id), 
	(select count(*) from leads x where x.assigned_verifier = ".$c->uid." and x.assigned_usertype='VA' and date(x.assign_verifier)>='".$txtFromDate."' and date(x.assign_verifier)<='".$txtToDate."') as pending,
	(select count(distinct m1.id) from lead_comments z1 inner join leads m1 on m1.id = z1.lead_id where z1.comment_from = ".$c->uid." and z1.is_approved_verifier = 'AV' and date(z1.creation_date)>='".$txtFromDate."' and date(z1.creation_date)<='".$txtToDate."') as approved,
	(select count(distinct m2.id) from lead_comments z2 inner join leads m2 on m2.id = z2.lead_id where z2.comment_from = ".$c->uid." and z2.is_approved_verifier = 'RV' and date(z2.creation_date)>='".$txtFromDate."' and date(z2.creation_date)<='".$txtToDate."') as rejected
	from leads inner join lead_comments lc1 on lc1.lead_id = leads.id where lc1.comment_to = ".$c->uid." and date(lc1.creation_date)>='".$txtFromDate."' and date(lc1.creation_date)<='".$txtToDate."' and lc1.lead_transfer_type='LT'";
	$datamaster = $mysqli->prepare($query);
	$datamaster->execute();
	$datamaster->bind_result($d1, $d2, $d3, $d4);
	$datamaster->fetch();
	$datamaster->close();
	
	$arr_3.= "['".$c->verifier_name."',".$d1.",".$d2.",".$d3.",".$d4."],";
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
		<style type="text/css">
			.content-wrapper {
				background: #fff;
			}
		</style>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
		<div class="content-wrapper">
			<div class="container-fluid">
				<div class="row mt-3">
					<div class="col-md-12">
						<div class="alert alert-warning">
							Total Leads in database: <?php echo getCountLeads('','') ?>
						</div>
					</div>
					<div class="col-md-2">
						<div class="alert alert-danger">
							<?php echo getCountLeads('','CA') ?>
							<br/>
							Agent
						</div>
					</div>
					<div class="col-md-2">
						<div class="alert alert-info">
							<?php echo getCountLeads('','CTL') ?>
							<br/>
							Team Lead
						</div>
					</div>
					<div class="col-md-2">
						<div class="alert alert-success">
							<?php echo getCountLeads('','CCA') ?>
							<br/>
							Callcenter
						</div>
					</div>
					<div class="col-md-2">
						<div class="alert alert-primary">
							<?php echo getCountLeads('','VA') ?>
							<br/>
							Verifier
						</div>
					</div>
					<div class="col-md-2">
						<div class="alert alert-dark">
							<?php echo getCountLeads('','MSW') ?>
							<br/>
							MSW
						</div>
					</div>
					<div class="col-md-2">
						<div class="alert alert-danger">
							<?php echo getCountLeads('','C') ?>
							<br/>
							Sol
						</div>
					</div>
				</div>
				
				<div class="card">
					<div class="card-body">
						<form action='' name='frmChartSearch' id='frmChartSearch' method='post'>
							<div class="row">
								<div class="col-md-3 col-sm-8 col-xs-12">
									<div class="form-group">
										<select class="form-control" required placeholder='' id="ddlSelectType" name="ddlSelectType" onchange="selectChartCustom()">
											<option value="1" <?php echo isset($_POST["ddlSelectType"]) && $_POST["ddlSelectType"]==1?'selected':'' ?>>All</option>
											<option value="2" <?php echo isset($_POST["ddlSelectType"]) && $_POST["ddlSelectType"]==2?'selected':'' ?>>Today</option>
											<option value="3" <?php echo isset($_POST["ddlSelectType"]) && $_POST["ddlSelectType"]==3?'selected':'' ?>>This Week</option>
											<option value="4" <?php echo isset($_POST["ddlSelectType"]) && $_POST["ddlSelectType"]==4?'selected':'' ?>>This Month</option>
											<option value="5" <?php echo isset($_POST["ddlSelectType"]) && $_POST["ddlSelectType"]==5?'selected':'' ?>>Custom</option>
										</select>
									</div>
								</div>
								<div class="col-md-3 col-sm-8 col-xs-12 txtFromDate" style="display:<?php echo isset($_POST["ddlSelectType"]) && $_POST["ddlSelectType"]==5?'block':'none'?>">
									<div class="form-group">
										<input value="<?php if(isset($_POST["txtFromDate"])) echo $_POST["txtFromDate"] ?>" class="form-control" placeholder='From' id="txtFromDate" name="txtFromDate" placeholder="" type="text" />
									</div>
								</div>
								<div class="col-md-3 col-sm-8 col-xs-12 txtToDate" style="display:<?php echo isset($_POST["ddlSelectType"]) && $_POST["ddlSelectType"]==5?'block':'none'?>">
									<div class="form-group">
										<input value="<?php if(isset($_POST["txtToDate"])) echo $_POST["txtToDate"] ?>" class="form-control" placeholder='To' id="txtToDate" name="txtToDate" placeholder="" type="text" />
									</div>
								</div>
								<div class="col-md-3 col-sm-4 col-xs-4">
									<input type="submit" class="btn btn-primary" value="Search" name="btnSearch" id="btnSearch" />
									<input type="button" class="btn btn-warning" value="Reset" name="btnReset" id="btnReset" onclick="window.location='charts.php'" />
								</div>
							</div>
						</form>
					</div>
				</div>
				
				<div class="row mt-3">
					<div class="col-md-12">
						<div id="chart_1" style="height: 500px;"></div>
					</div>
				</div>
				<div class="row mt-3">
					<div class="col-md-12">
						<div id="chart_2" style="height: 500px;"></div>
					</div>
				</div>
				<div class="row mt-3">
					<div class="col-md-12">
						<div id="chart_3" style="height: 500px;"></div>
					</div>
				</div>
			</div>
		</div>  
		<?php include('../includes/copyright.php'); ?>
		
		<!-- Scroll to Top Button-->  
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
		  <i class="ti-angle-double-up"></i>
		</a>

		<?php include('../includes/web_footer.php'); ?>
			
	  </div>
	  <!-- Wrapper -->
	  
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
	google.charts.load('current', {'packages':['bar']});
	google.charts.setOnLoadCallback(drawChart_1);
	google.charts.setOnLoadCallback(drawChart_2);
	google.charts.setOnLoadCallback(drawChart_3);
	google.charts.setOnLoadCallback(drawChart_4);

	function drawChart_1() 
	{
		var data = google.visualization.arrayToDataTable([
			['Leads', 'Assigned To Sol', 'Approved', 'SignUp', 'Rejected'],
			<?php echo trim($arr_1,',') ?>
		  ]);

		var options = {
		  chart: {
			title: 'Overall Sol Performance',
			subtitle: 'Leads Total',
		  },
		  bars: 'vertical'
		};

		var chart = new google.charts.Bar(document.getElementById('chart_1'));
		chart.draw(data, google.charts.Bar.convertOptions(options));
	}
	
	function drawChart_2() 
	{
		var data = google.visualization.arrayToDataTable([
			['Leads', 'Assigned To CC', 'Approved', 'SignUp', 'Assigned To Admin'],
			<?php echo trim($arr_2,',') ?>
		  ]);

		var options = {
		  chart: {
			title: 'Overall Callcenter Performance With Currently Assigned Leads',
			subtitle: 'Leads Total',
		  },
		  bars: 'vertical'
		};

		var chart = new google.charts.Bar(document.getElementById('chart_2'));
		chart.draw(data, google.charts.Bar.convertOptions(options));
	}
	
	function drawChart_3() 
	{
		var data = google.visualization.arrayToDataTable([
			['Leads', 'Assigned To Verifier', 'Pending From Verifier', 'Approved By Verifier', 'Rejected By Verifier'],
			<?php echo trim($arr_3,',') ?>
		  ]);
		var options = {
		  chart: {
			title: 'Overall Verifier Performance',
			subtitle: 'Leads Total',
		  },
		  bars: 'vertical'
		};

		var chart = new google.charts.Bar(document.getElementById('chart_3'));
		chart.draw(data, google.charts.Bar.convertOptions(options));
	}
	
	</script>
	</body>
</html>
