<?php include('../includes/basic_auth.php'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
		
		<div class="content-wrapper">
			<div class="container-fluid">
				
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Leave Form</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
				<div class="row">
					
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								
								
								<div class="card-body">
									<form>
										<div class="row">
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="exampleInputEmail1">Employee Name</label>
													<input class="form-control" id="exampleInputEmail1" placeholder="Employee Name" type="text">
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="exampleInputEmail1">Department</label>
													<input class="form-control" id="exampleInputEmail1" placeholder="Department" type="text">
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="exampleInputEmail1">Designation</label>
													<input class="form-control" id="exampleInputEmail1" placeholder="Designation" type="text">
												</div>
											</div>
											<div class="col-sm-12">
												<div class="form-group">
													<label for="exampleInputEmail1">Reason For Requested Leave</label>
													<textarea class="form-control" rows="4" placeholder="Please give us a valid reason"></textarea>
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="exampleInputEmail1">Leave From (Date)</label>
													<label class="d-flex"><input class="form-control" id="exampleInputEmail1" placeholder="DD/MM/YY" type="text">
													<span class="clndr"><i class=""></i></span></label>
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
													<label for="exampleInputEmail1">Leave To (Date)</label>
													<label class="d-flex"><input class="form-control" id="exampleInputEmail1" placeholder="DD/MM/YY" type="text">
													<span class="clndr"><i class=""></i></span></label>
												</div>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="form-group">
												<div class="form-group">
													<label for="exampleInputEmail1">Lorem Ipsum</label>
													<select name="state" class="d-flex form-control">
														<option value="" name="">Casual Leave</option>
														<option value="" name="">Short Leave</option>
														<option value="" name="">Half Day</option>
													</select>
												</div>
												</div>
											</div>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
											<button id="check2" type="button" class="btn btn-primary ml-0 mr-3 mt-2">Apply</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				<!-- /row -->
				
				
				
			</div>
			<!-- /.content-wrapper -->
			
			<?php include('../includes/copyright.php'); ?>
			<!-- Scroll to Top Button-->
			<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
				<i class="ti-angle-double-up"></i>
			</a>
			<?php include('../includes/web_footer.php'); ?>
			
		</div>
		<!-- Wrapper -->
		
	</body>
</html>