<?php
include('../includes/basic_auth.php');
if(isset($_POST['accident_array']))
{
	$lead_id = $_POST['hfLeadId'];
	$accident_array = array();
	$accident_array = json_decode($_POST['accident_array']);
	
	foreach($accident_array as $my_array)
	{
		if(isset($my_array->txtAccidentId) && $my_array->txtAccidentId>0)
		{
			$txtAccidentId = $my_array->txtAccidentId;
			$getAccidentDetails = getAccidentDetails(' and id = '.$txtAccidentId, $lead_id);
			
			if(mysqli_num_rows($getAccidentDetails)>0)
			{
				$arow = mysqli_fetch_object($getAccidentDetails);
				
				$dt_datetime = date('Y-m-d H:i:s',strtotime($my_array->txt3AccidentDate.' '.$my_array->txt3AccidentTime));
				$ddl3RoadState = $my_array->ddl3RoadState;
				$ddlState = trim($my_array->ddlState)==null?null:$my_array->ddlState;
				$ddlSubUrb = trim($my_array->ddlSubUrb)==null?null:$my_array->ddlSubUrb;
				$txt3PostalCode = $my_array->txt3PostalCode;
				$txt3AddAddress = $my_array->txt3AddAddress;
				$txtAccidentCircumstanes = $my_array->txtAccidentCircumstanes;
				$ddl3PoliceInformed = $my_array->ddl3PoliceInformed;
				$ddlPoliceReported = $my_array->ddlPoliceReported;
				$rbtnSelectedDriver = $my_array->rbtnSelectedDriver;
				$txt3AccidentRecord = $my_array->txt3AccidentRecord;
				$txt3SubUrbNameAccident = $my_array->txt3SubUrbNameAccident;
				$is_na = $my_array->is_na;
				
				$ddlSubUrb = trim($my_array->ddlSubUrb)==0?null:$my_array->ddlSubUrb;
				
				if(trim($my_array->ddlSubUrb)!=0)
				{
					$txt3SubUrbNameAccident = null;
				}
				
				if($ddl3PoliceInformed!=null)
				{
					$ddl3PoliceInformed = strtolower($ddl3PoliceInformed)=='yes'?1:0;
				}
				else
				{
					$ddl3PoliceInformed=null;
				}
				if($ddlPoliceReported!=null)
				{
					$ddlPoliceReported = strtolower($ddlPoliceReported)=='yes'?1:0;
				}
				else
				{
					$ddlPoliceReported=null;
				}
				
				$txtPoliceStation=null;
				$txtPoliceNumber=null;
				$txtPoliceInfo=null;
				$txtOfficersName=null;
				$txtPoliceStationReported=null;
				
				if($ddl3PoliceInformed==1)
				{
					$txtPoliceStation = $my_array->txtPoliceStation;
					$txtPoliceNumber = $my_array->txtPoliceNumber;
					$txtPoliceInfo = $my_array->txtPoliceInfo;
				}
				else if($ddlPoliceReported==1)
				{
					$txtOfficersName = $my_array->txtOfficersName;
					$txtPoliceStationReported = $my_array->txtPoliceStationReported;
				}
				
				$loginmaster = $mysqli->prepare("update lead_accidents set accident_date_time=?, is_driver=?, weather=?, state_id=?, suburb_id=?, postal_code=?, address=?, accident_circumstances=?, police_attended=?, attended_police_station=?, event_number=?, other_info=?, accident_reported=?, officer_name=?, informed_police_station=?,accident_recorded=?,is_na=?,suburb_name = ?  where id=?");
				echo $mysqli->error;
				$loginmaster->bind_param("sisiisssisssisssisi",$dt_datetime, $rbtnSelectedDriver, $ddl3RoadState, $ddlState, $ddlSubUrb, $txt3PostalCode, $txt3AddAddress, $txtAccidentCircumstanes, $ddl3PoliceInformed, $txtPoliceStation, $txtPoliceNumber,$txtPoliceInfo, $ddlPoliceReported, $txtOfficersName, $txtPoliceStationReported,$txt3AccidentRecord,$is_na,$txt3SubUrbNameAccident,$txtAccidentId);
				echo $mysqli->error;
				$loginmaster->execute();
				echo $mysqli->error;
				$loginmaster->close();
			}
		}
	}
}
?>