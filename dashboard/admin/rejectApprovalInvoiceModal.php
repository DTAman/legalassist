<div id="rejectApprovalInvoiceModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Reject And Assign To Callcenter</h4>
			</div>
			<form action='' method='post' name='frmRejectApprovalInvoiceModal' id='frmRejectApprovalInvoiceModal'>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<select required="required" name="ddlAssignToCallcenterInvoice" id="ddlAssignToCallcenterInvoice" class="form-control"></select>
								<textarea required="required" class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter details if any" name='txtSaveCommentCallcenterInvoice' id='txtSaveCommentCallcenterInvoice'></textarea>
								<input class="form-control" value="0" name="hfLeadIdCommentCallcenterInvoice" id="hfLeadIdCommentCallcenterInvoice" type="hidden" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnCommentCallcenterInvoice" name="btnCommentCallcenterInvoice" type="submit" class="btn btn-primary" value='Reject &amp; Assign To Callcenter' />
				</div>
			</form>
		</div>
	</div>
</div>