<div id="attachmentsModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Lead Attachments</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<form action='' method='post' name='frm10Leads' id='frm10Leads' enctype="multipart/form-data">
							<div class="attachments">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>