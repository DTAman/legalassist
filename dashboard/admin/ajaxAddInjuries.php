<?php
include('../includes/basic_auth.php');

if(isset($_POST['hfLeadId']) && isset($_POST['txtStillSuffering']) && isset($_POST['txt6AnyOtherInjury']) && trim($_POST['hfLeadId'])!=0 && isset($_POST['hfLeadAccidentId']) && trim($_POST['hfLeadAccidentId'])!=0)
{
	$lead_id = $_POST['hfLeadId'];
	$hfSomethingChanged = $_POST['hfSomethingChanged'];
	$hfLeadAccidentId = $_POST['hfLeadAccidentId'];
	
	if($hfSomethingChanged==1)
	{
		$querymaster = $mysqli->prepare("delete from  lead_injury_details where injury_master_id=(select id from lead_injury where lead_accident_id=?)");
		$querymaster->bind_param("i",$hfLeadAccidentId);
		$querymaster->execute();
		$querymaster->close();
	}
	
	$view_injuries = getQuery('select id from lead_injury where lead_accident_id='.$hfLeadAccidentId);
	if(mysqli_num_rows($view_injuries)>0)
	{
		$rc5 = mysqli_fetch_object($view_injuries);
	
		$injury_master_id = $rc5->id;
		
		$querymaster = $mysqli->prepare("update lead_injury set lead_id=?, still_suffering=?,other_injuries=? where id=?");
		$querymaster->bind_param("iisi",$lead_id, $_POST['txtStillSuffering'],$_POST['txt6AnyOtherInjury'], $rc5->id);
		$querymaster->execute();
		$querymaster->close();
	}
	else
	{
		$querymaster = $mysqli->prepare("insert into lead_injury(lead_id, still_suffering,other_injuries, lead_accident_id)values(?,?,?,?)");
		$querymaster->bind_param("iisi",$lead_id, $_POST['txtStillSuffering'],$_POST['txt6AnyOtherInjury'],$hfLeadAccidentId);
		$querymaster->execute();
		$querymaster->close();
		
		$injury_master_id = mysqli_insert_id($mysqli);
	}

	if(isset($_POST['checked_array']) && $hfSomethingChanged==1)
	{
		$checked_array = array();
		$checked_array = json_decode($_POST['checked_array']);
		
		if(count($checked_array)>0)
		{
			foreach($checked_array as $my_array)
			{
				$querymaster = $mysqli->prepare("insert into lead_injury_details(injury_master_id, body_part_id, injury_id) values(?,?,?)");
				$querymaster->bind_param("iii",$injury_master_id, $my_array->body_part,$my_array->injuries);
				$querymaster->execute();
				$querymaster->close();
			}
		}
	}
}
?>