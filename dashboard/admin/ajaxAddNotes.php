<?php
include('../includes/basic_auth.php'); 

$status='AC';
if(isset($_POST['hfLeadId']) && isset($_POST['txtSaveCloseDetailsNotes']) && trim($_POST['hfLeadId'])!=0)
{
	$creation_date = date('Y-m-d H:i:s');  // saving the current date
	
	$loginmaster = $mysqli->prepare("insert into lead_notes(lead_id, creation_date, notes, status, added_by) values(?,?,?,?,?)");
	$loginmaster->bind_param("isssi",$_POST['hfLeadId'],$creation_date,$_POST['txtSaveCloseDetailsNotes'],$status,$_SESSION['userId']);
	$loginmaster->execute();
	$loginmaster->close();
	
	$loginmaster = $mysqli->prepare("update leads set latest_notes = ?, updation_date=?, latest_comment_usertype_from = ?, latest_comment_usertype_to = ?, last_comment = ? where id=?");
	$loginmaster->bind_param("sssssi",$_POST['txtSaveCloseDetailsNotes'],$thisdate,$_SESSION['userType'],$_SESSION['userType'],$_POST['txtSaveCloseDetailsNotes'],$_POST['hfLeadId']);
	$loginmaster->execute();
	$loginmaster->close();
	
	$comment_status='PN';
	$lead_transfer_type = 'NTS';
	
	$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
	$loginmaster->bind_param("i",$_POST['hfLeadId']);
	$loginmaster->execute();
	$loginmaster->close();
	
	$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type) VALUES (?,?,?,?,?,?,?,?)");
	$loginmaster->bind_param("iisissss",$_POST['hfLeadId'],$_SESSION['userId'],$_SESSION['userType'], $_SESSION['userId'],$_POST['txtSaveCloseDetailsNotes'],$comment_status,$thisdate,$lead_transfer_type);
	$loginmaster->execute();
	$loginmaster->close();
		
	$subject = 'LegalAssist  - New Notes';
	$html = '<!DOCTYPE html>
				<html lang="en">
				<head>
					<meta charset="UTF-8">
					<title>LegalAssist</title>
				</head>

				<body>
					<!-- Table Start -->
					<table style="text-align: center; max-width: 700px;width:100%; margin: 0 auto; border-collapse: collapse; border: 1px solid rgba(0,0,0,.15); font-family: sans-serif;">
						<thead style=" background: rgb(245, 245, 245);color: white; border-bottom: 1px solid #d1d1d1;">
							<tr>
								<th style="padding: 15px 15px;"><img src="'.$mysiteurl.'images/logo.png" alt="logo" style="max-width: 150px"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="padding: 25px 10px;font-weight: 700;font-size: 22px;text-transform: uppercase;"><span style="color: #40a1d8;">New Notes Added</span></td>
							</tr>
							<tr>
								<td style="text-align: left;padding: 0 20px;font-size: 15px;line-height: 24px;text-align:center">
									New Note has been added for the following lead.<br/>
									Name of the lead is <b>%title%</b> <br/>
									Notes added: '.$_POST['txtSaveCloseDetailsNotes'].'
									'.email_notification_accidents($_POST['hfLeadId']).'
								</td>
							</tr>
							<tr>
								<td>
									<P style="margin: 30px 0;">For further details <a target="_blank" href="'.$mysiteurl.'" style="text-decoration: none;background: #40a1d8;padding: 10px;color: white;">Click Here</a></P>
								</td>
							</tr>
						</tbody>
						<tfoot style="background: #f5f5f5;border-top: 1px solid rgba(0,0,0,.15);">
							<tr>
								<td style="padding: 13px;font-size: 13px;">
									Copyright © All Rights Reserved
								</td>
							</tr>
						</tfoot>
					</table>
					<!-- Table End -->
				</body>

				</html>';

	sendClientEmails($_POST['hfLeadId'], $subject, $html);		
}


?>