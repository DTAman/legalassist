<?php
include('../includes/basic_auth.php'); 

if(isset($_GET['u']) && trim($_GET['u'])>0 && isset($_SESSION['txtCopyNumber']) && isset($_SESSION['txtCopyName']) && trim($_SESSION['txtCopyNumber'])!=null && trim($_SESSION['txtCopyName'])!=null)
{
	$lead_id = $_GET['u'];
	$userType = $_SESSION["userType"];
	$user_id = $_SESSION["userId"];
	$txtCopyName = $_SESSION['txtCopyName'];
	$txtCopyNumber = $_SESSION['txtCopyNumber'];
	$added_by = $user_id;
	$txtCopyMiddleName = null;
	
	$txtCopyLastEmail = $_SESSION['txtCopyLastEmail'];
	$txtCopyLastName = $_SESSION['txtCopyLastName'];
	
	$dob=NULL;
	if(trim($_SESSION['txtCopyDay'])!=null)
	{
		$txtCopyDay = $_SESSION['txtCopyDay'];
		$txtCopyMonth = $_SESSION['txtCopyMonth'];
		$txtCopyYear = $_SESSION['txtCopyYear'];
	
		$dob = date('Y-m-d', strtotime($txtCopyDay."-".$txtCopyMonth."-".$txtCopyYear));
	}
	
	unset($_SESSION['txtCopyName']);
	unset($_SESSION['txtCopyNumber']);
	unset($_SESSION['txtCopyLastEmail']);
	unset($_SESSION['txtCopyLastName']);
	unset($_SESSION['txtCopyDay']);
	unset($_SESSION['txtCopyMonth']);
	unset($_SESSION['txtCopyYear']);
	
	$is_msw_reached = 0;
	switch($userType)
	{
		case 'C': 	$column_name = 'assign_client';
					$assigned_column = 'assigned_client';
					break;
		case 'CCA': $column_name = 'assign_n_callcenter';
					if($_SESSION['is_affliated']==1)
					{
						$column_name = 'assign_affliated';
					}
					$assigned_column = 'callcenter_id';
					break;
		case 'CTL': $column_name = 'assign_teamlead';
					$assigned_column = 'team_lead_assigned';
					break;
		case 'CA': 	$column_name = 'assign_agent';
					$assigned_column = 'agent_assigned';
					break;
		case 'MSW': $column_name = 'assign_admin';
					$assigned_column = '';
					$is_msw_reached = 1;
					break;
		case 'VA':  $column_name = 'assign_verifier';
					$assigned_column = 'assigned_verifier';
					break;
		default:    $column_name='';
				    $assigned_column='';
	}
	
	//Personal Details
	$querymaster = $mysqli->prepare("INSERT INTO leads(first_name, middle_name, last_name, phone, email, relative_name, relative_number, dob, state_id, suburb_id, suburb_name, postal_code, address, added_by, status, creation_date, updation_date, callcenter_id, master_lead_id, last_accident_type, last_accident_na, latest_accident_dt, reached_admin) 
	SELECT ?, ?, ?, ?, ?, relative_name, relative_number, ?, state_id, suburb_id, suburb_name, postal_code, address, ?, status, ?, ?, callcenter_id, ?,  last_accident_type, last_accident_na, latest_accident_dt, ? FROM leads WHERE id = ?");
	$querymaster->bind_param("ssssssissiii",$txtCopyName,$txtCopyMiddleName,$txtCopyLastName, $txtCopyNumber,$txtCopyLastEmail,$dob,$added_by,$thisdate,$thisdate,$lead_id,$is_msw_reached,$lead_id);
	$querymaster->execute();
	$querymaster->close();

	$copy_lead_id = mysqli_insert_id($mysqli);
	
	//If Admin is adding lead, callcenter_id will be null until unless he assign the lead to callcenter
	if($userType!=$CA && $userType!=$CTL && $userType!=$CCA)
	{
		$querymaster = $mysqli->prepare("update leads set callcenter_id = NULL where id = ?");
		$querymaster->bind_param("i",$copy_lead_id);
		$querymaster->execute();
		$querymaster->close();
	}
	
	//Accident Details
	$querymaster = $mysqli->prepare("select id from lead_accidents where lead_id= ? and status=1");
	$querymaster->bind_param("i",$lead_id);
	$querymaster->execute();
	$accident_result = $querymaster->get_result();
	$querymaster->close(); 
	
	while($a_row = mysqli_fetch_object($accident_result))
	{
		$querymaster = $mysqli->prepare("INSERT INTO lead_accidents(lead_id, accident_date_time, accident_kind, claim_accident, is_driver, weather, state_id, suburb_id, suburb_name, postal_code, address, accident_circumstances, police_attended, attended_police_station, event_number, other_info, accident_reported, officer_name, informed_police_station, accident_recorded, is_na, status ) SELECT ?,accident_date_time, accident_kind, claim_accident, is_driver, weather, state_id, suburb_id, suburb_name, postal_code, address, accident_circumstances, police_attended, attended_police_station, event_number, other_info, accident_reported, officer_name, informed_police_station, accident_recorded, is_na, status FROM lead_accidents WHERE id = ?");
		$querymaster->bind_param("ii",$copy_lead_id,$a_row->id);
		$querymaster->execute();
		$querymaster->close(); 
		
		$copy_accident_id = mysqli_insert_id($mysqli);
		
		//Vehicle Details
		$querymaster = $mysqli->prepare("INSERT INTO lead_vehicles(lead_id, lead_accident_id, make, model, registration, insurance_company, insurance_reference_no, insurance_claim_number, other_info, driver_name, driver_address, dl_number, make_d, model_d, registration_d, insurance_company_d, insurance_reference_no_d, insurance_claim_number_d, other_info_d) SELECT ?, ?, make, model, registration, insurance_company, insurance_reference_no, insurance_claim_number, other_info, driver_name, driver_address, dl_number, make_d, model_d, registration_d, insurance_company_d, insurance_reference_no_d, insurance_claim_number_d, other_info_d from lead_vehicles WHERE lead_accident_id = ?");
		$querymaster->bind_param("iii",$copy_lead_id,$copy_accident_id,$a_row->id);
		$querymaster->execute();
		$querymaster->close();
		
		//Injury Details
		$querymaster = $mysqli->prepare("INSERT INTO lead_injury(lead_id, lead_accident_id, still_suffering, other_injuries) SELECT ?, ?, still_suffering, other_injuries FROM lead_injury WHERE lead_accident_id = ?");
		$querymaster->bind_param("iii",$copy_lead_id,$copy_accident_id,$a_row->id);
		$querymaster->execute();
		$querymaster->close();
		
		$copy_injury_id = mysqli_insert_id($mysqli);
		
		//Injury Matrix
		$querymaster = $mysqli->prepare("select id from lead_injury where lead_accident_id = ?");
		$querymaster->bind_param("i",$a_row->id);
		$querymaster->execute();
		$injury_result = $querymaster->get_result();
		$querymaster->close(); 
		
		while($i_row = mysqli_fetch_object($injury_result))
		{
			$querymaster = $mysqli->prepare("INSERT INTO lead_injury_details(injury_master_id, injury_id, body_part_id) SELECT ?, injury_id, body_part_id FROM lead_injury_details WHERE injury_master_id = ?");
			$querymaster->bind_param("ii",$copy_injury_id, $i_row->id);
			$querymaster->execute();
			$querymaster->close();
		}
		
		//Treatment Details
		$querymaster = $mysqli->prepare("INSERT INTO lead_treatment_history(lead_id, lead_accident_id, ambulance_at_scene, ambulance_treated_at_scene, ambulance_treatment, ambulance_took_to_hospital, ambulance_hospital_detail, painreliefs_recommended, has_physio_chirio, is_physio_recommended, is_chiro_recommended, can_afford_physio, can_afford_chiropractic, visits_for_physio, visits_for_chiro, have_x_ray, x_ray_result, why_not_x_ray, pre_existing_injuries, dizziness, nausea, post_traumatic_stress_disorder, insomnia, vertigo, driving_difficulty_post_accident, fear_of_being_hit, went_hospital_with_friend, hospital_detail_took_by_friend, visited_gp, gp_name, surgery_name, visits_to_gp, last_visit_to_gp, any_other_psychological_injury, hospital_treatment, physiosessions, physiotakingsessions, chiriosessions, chiriotakingsessions) SELECT ?, ?, ambulance_at_scene, ambulance_treated_at_scene, ambulance_treatment, ambulance_took_to_hospital, ambulance_hospital_detail, painreliefs_recommended, has_physio_chirio, is_physio_recommended, is_chiro_recommended, can_afford_physio, can_afford_chiropractic, visits_for_physio, visits_for_chiro, have_x_ray, x_ray_result, why_not_x_ray, pre_existing_injuries, dizziness, nausea, post_traumatic_stress_disorder, insomnia, vertigo, driving_difficulty_post_accident, fear_of_being_hit, went_hospital_with_friend, hospital_detail_took_by_friend, visited_gp, gp_name, surgery_name, visits_to_gp, last_visit_to_gp, any_other_psychological_injury, hospital_treatment, physiosessions, physiotakingsessions, chiriosessions, chiriotakingsessions FROM lead_treatment_history where lead_accident_id = ?");
		$querymaster->bind_param("iii",$copy_lead_id,$copy_accident_id,$a_row->id);
		$querymaster->execute();
		$querymaster->close();
		
		//Work History
		$querymaster = $mysqli->prepare("INSERT INTO lead_work_care_history(lead_id, lead_accident_id, is_employed, income_from, other_income_means, occupation, weekly_salary, working_hours, income_loss_by_accident, injury_affecting_work, receiving_assistance, want_assistance) SELECT ?, ?, is_employed, income_from, other_income_means, occupation, weekly_salary, working_hours, income_loss_by_accident, injury_affecting_work, receiving_assistance, want_assistance FROM lead_work_care_history where lead_accident_id = ?");
		$querymaster->bind_param("iii",$copy_lead_id,$copy_accident_id,$a_row->id);
		$querymaster->execute();
		$querymaster->close();
		
		//Witness Details
		$querymaster = $mysqli->prepare("INSERT INTO lead_witness(lead_id, lead_accident_id, name, phone, state_id, suburb_id, postal_code, address) SELECT ?, ?, name, phone, state_id, suburb_id, postal_code, address FROM lead_witness where lead_accident_id = ?");
		$querymaster->bind_param("iii",$copy_lead_id,$copy_accident_id,$a_row->id);
		$querymaster->execute();
		$querymaster->close();
		
		//Documents Details
		$querymaster = $mysqli->prepare("INSERT INTO lead_documents(lead_id, lead_accident_id, doc1, doc2, doc3, doc4, doc5, doc6, doc7, desc1, desc2, desc3, desc4, desc5, desc6, desc7) SELECT ?, ?, doc1, doc2, doc3, doc4, doc5, doc6, doc7, desc1, desc2, desc3, desc4, desc5, desc6, desc7 FROM lead_documents where lead_accident_id = ?");
		$querymaster->bind_param("iii",$copy_lead_id,$copy_accident_id,$a_row->id);
		$querymaster->execute();
		$querymaster->close();
	}
	
	$hfLeadCommentUType = $userType;
	$comments = 'Lead Copied';
	$comment_status='PN';
	// $comment_from = null;
	$comment_from = $_SESSION['userId'];
	
	$querymaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type, copy_lead_comment) VALUES (?,?,?,?,?,?,?,'LT',1)");
	$querymaster->bind_param("iisisss",$copy_lead_id,$user_id,$hfLeadCommentUType,$comment_from,$comments,$comment_status,$thisdate);
	$querymaster->execute();
	$querymaster->close();
	
	$loginmaster = $mysqli->prepare("update leads set assigned_usertype = ?,outcome=(select outcome_title from outcomes where outcome_initials = 'COP'), outcome_type='COP', last_comment = 'Lead Copied', latest_comment_usertype_from = ?, latest_comment_usertype_to = ? where id= ?");
	$loginmaster->bind_param("sssi",$hfLeadCommentUType,$_SESSION['userType'],$_SESSION['userType'],$copy_lead_id);
	$loginmaster->execute();
	$loginmaster->close();
	
	if($column_name!=null)
	{
		$loginmaster = $mysqli->prepare("update leads set ".$column_name." = ? where id = ?");
		$loginmaster->bind_param("si", $thisdate,$copy_lead_id);
		$loginmaster->execute();
		$loginmaster->close();
	}
	if(trim($assigned_column)!=null)
	{
		$loginmaster = $mysqli->prepare("update leads set ".$assigned_column." = ? where id = ?");
		$loginmaster->bind_param("si", $_SESSION['userId'],$copy_lead_id);
		$loginmaster->execute();
		$loginmaster->close();
	}
	header("location:view-leads.php?pp=f"); 
}
else
{
	header("location:view-leads.php?pp=f"); 
}
?>