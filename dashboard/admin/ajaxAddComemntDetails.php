<?php
include('../includes/basic_auth.php');

$m='';
$is_affliated=0;
$hfForCallcenter=0;
$hfOutcome = '';
if(isset($_POST['hfLeadId']) && isset($_POST['attr_type']) && trim($_POST['attr_type'])!=0 && trim($_POST['hfLeadId'])!=0)
{
	$attr_type = $_POST['attr_type'];
	$comm_to = getLastLeadCommentUser($_POST['hfLeadId']);
	
	$last_rejected_by=0;
	
	if($attr_type==1) //1 MSW - Client - Upwards
	{
		$last_rejected_by_result = lastLeadRejectedByDetails($_POST['hfLeadId']);
		if(mysqli_num_rows($last_rejected_by_result)!=0)
		{
			$lrow = mysqli_fetch_object($last_rejected_by_result);
			
			$last_rejected_by = $lrow->comment_from;
			$last_rejected_by_reason = $lrow->comments;
		}
		
		$query = "SELECT login_master.*, company_name as name FROM login_master inner join clients on clients.uid = login_master.uid where is_deleted=0 and type='C' order by company_name";
		$uutype='C';
	}
	else if($attr_type==3 || $attr_type==10  || $attr_type==31  || $attr_type==32) //3 Callcenter - Admin - Upwards  // 10 - lead rejected by client // 31 - Approve by verifier // 32 - Reject by verifier
	{
		$query = "SELECT * FROM login_master where is_deleted=0 and type='MSW'";
		$uutype='MSW';
		
		$m = '';
		if($attr_type==31)
		{
			$hfOutcome = 'AV';
			$m = "<b>Notice:</b> You are approving a lead";
			echo "<span class='alert alert-success' style='display: block;'>".$m."</span>";
		}
		else if($attr_type==32)
		{
			$hfOutcome = 'RV';
			$m = "<b>Notice:</b> You are rejecting a lead";
			echo "<span class='alert alert-danger' style='display: block;'>".$m."</span>";
		}
	}
	else if($attr_type==5) //5 Team Lead - Callcenter - Upwards
	{
		$query = "SELECT *,(select centre_name from callcenter where is_deleted=0 and uid=login_master.uid) as name FROM login_master where is_deleted=0 and  type='CCA' and uid in (select callcenter_id from tl_and_agents inner join login_master l on l.uid = tl_and_agents.uid where l.uid=".$_SESSION['userId']." )";
		$uutype='CCA';
	}
	else if($attr_type==6) //6 Agent To Team Lead - Upwards
	{
		$query = "SELECT login_master.*,tl_and_agents.first_name as name FROM login_master inner join tl_and_agents on tl_and_agents.uid = login_master.uid where is_deleted=0 and login_master.type='CTL' and login_master.uid in (select uid from tl_and_agents where callcenter_id in (select callcenter_id from tl_and_agents where uid = ".$_SESSION['userId'].")) order by tl_and_agents.first_name";
		$uutype='CTL';
	}
	else if($attr_type==2) //2 MSW - Callcenter - Downwards
	{
		$query = "select centre_name as name,login_master.uid from callcenter inner join login_master on login_master.uid = callcenter.uid where login_master.type='CCA' and is_deleted=0";
		
		$c_res = getCallcenterIdLead($_POST['hfLeadId']);
		if(mysqli_num_rows($c_res)>0)
		{
			$crow = mysqli_fetch_object($c_res);
			$is_affliated = $crow->is_affliated;
			$c_id = $crow->callcenter_id;
		}
		else
		{
			$is_affliated = $c_id = null;
		}
		
		$uutype='CCA';
		
		//$comm_to = getLastLeadCommentUserByType($_POST['hfLeadId'],'CCA');
		
		$getC = $mysqli->prepare('select callcenter_id from leads where id=?');
		$getC->bind_param('i',$_POST['hfLeadId']);
		$getC->execute();
		$getC->bind_result($comm_to);
		$getC->fetch();
		$getC->close();
		
		$hfForCallcenter = 1;
	}
	else if($attr_type==4) //4 Callcenter - Team Lead - Downwards
	{
		$query = "select first_name as name, l.uid from tl_and_agents inner join login_master l on l.uid = tl_and_agents.uid where is_deleted=0 and l.type='CTL' and callcenter_id=".$_SESSION['userId']." order by tl_and_agents.first_name";
		$uutype='CTL';
		
		$comm_to = getLastLeadCommentUserByType($_POST['hfLeadId'],'CTL');
	}
	else if($attr_type==7) //7 Team Lead - Agent - Downwards
	{
		$query = "select first_name as name, l.uid from tl_and_agents inner join login_master l on l.uid = tl_and_agents.uid where is_deleted=0 and l.type='CA' and callcenter_id in (select callcenter_id from tl_and_agents where uid = ".$_SESSION['userId'].") order by tl_and_agents.first_name";
		$uutype='CA';
		
		// $comm_to = getLastLeadCommentUserByType($_POST['hfLeadId'],'CA');
		$comm_to = getLeadLastAgent($_POST['hfLeadId']);
	}
	else if($attr_type==8) //8 MSW - Team Lead - Downwards
	{
		$comm_to = getLastLeadCommentUserByType($_POST['hfLeadId'],'CTL');
		
		$query = "select first_name as name, l.uid from tl_and_agents inner join login_master l on l.uid = tl_and_agents.uid where l.type='CTL' and callcenter_id in (select callcenter_id from tl_and_agents where uid = ".$comm_to.") ";
		$uutype='CTL';
		
	}
	else if($attr_type==30) //MSW to verifier
	{
		$query = "select verifier_name as name,login_master.uid from verifier inner join login_master on login_master.uid = verifier.uid where login_master.type='VA' and is_deleted=0 and status='AC'";
		$uutype='VA';
	}
	if($last_rejected_by!=0)
	{
		$m = '<div class="alert alert-danger d_aman">This lead has been rejected earlier by sol for reason: '.$last_rejected_by_reason.'</div>';
		echo $m;
	}
	?>
		<select required="required" name="ddlAssignTo" id="ddlAssignTo" class="form-control">
			<?php
				$getData = getQuery($query);
				while($srow = mysqli_fetch_object($getData))
				{
					?>
						<option <?php echo $comm_to==$srow->uid?'selected':'' ?> value='<?php echo $srow->uid ?>'><?php echo isset($srow->name)?ucwords(strtolower($srow->name)):'Administrator' ?></option>
					<?php
				}
			?>
		</select>
		
		<input class="form-control" value="<?php echo $_POST['hfLeadId'] ?>" name="hfLeadIdComment" id="hfLeadIdComment" type="hidden" />
		<input class="form-control" value="<?php echo $uutype ?>" name="hfLeadCommentUType" id="hfLeadCommentUType" type="hidden" />
		<input class="form-control" value="<?php echo $hfForCallcenter ?>" name="hfForCallcenter" id="hfForCallcenter" type="hidden" />
		<input class="form-control" value="<?php echo $hfOutcome ?>" name="hfOutcome" id="hfOutcome" type="hidden" />
	<?php
	if($attr_type==2)
	{
		?>
			<div class="row mt-3">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="bg-primary-light form-group p-1">
						<div class="custom-radio mr-3">
							<input type="radio" class="custom-control-input" id="rbtnvalx1" name="rbtnValues[]" value="RAA" />
							<label class="custom-control-label" for="rbtnvalx1">Reject and Assign</label>
						</div>
						<div class="custom-radio mr-3">
							<input checked type="radio" class="custom-control-input" id="rbtnvalx2" name="rbtnValues[]" value="RRC" />
							<label class="custom-control-label" for="rbtnvalx2">Assign to CC &amp; Request Changes</label>
						</div>
					</div>
				</div>
			</div>
		<?php
	}
}
?>