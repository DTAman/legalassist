<?php
	/* $getZones = getStates(); */
?>
<div id="saveCloseModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Enter Callback</h4>
			</div>
			<form action='' method='post' name='frmSaveClose' id='frmSaveClose' onsubmit='return false'>
				<div class="modal-body">
					<div class="row">
						
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<textarea required class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter details if any" name='txtSaveCloseDetails' id='txtSaveCloseDetails'></textarea>
							</div>
						</div>
						<!--<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<select required name="ddlSignUpStates" id="ddlSignUpStates" class="d-flex form-control" required>
									<option value=''>Select State (Zone) For Callback</option>
									<?php
										/* while($zrow = mysqli_fetch_object($getZones))
										{
											?>	
												<option value='<?php echo $zrow->id ?>'><?php echo $zrow->state_name." (".$zrow->timezones.")" ?></option>
											<?php
										} */
									?>
								</select>
							</div>
						</div>-->
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnSaveAndClose" name="btnSaveAndClose" type="submit" class="btn btn-primary" value='Save' />
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</form>
		</div>
	</div>
</div>