<?php 
include('../includes/basic_auth.php');

$aa_var = 0;
set_time_limit(0); 

if($_SESSION['userType']==$CA || $_SESSION['userType']==$CTL)
{
	$datamaster = $mysqli->prepare("SELECT callcenter_id FROM tl_and_agents where uid = ?");
	$datamaster->bind_param('i',$_SESSION['userId']);
	$datamaster->execute();
	$datamaster->bind_result($u_cid);
	$datamaster->fetch();
	$datamaster->close();
}
else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
{
	$u_cid = $_SESSION['userId'];
}

if(!isset($_SESSION['userType']) || !isset($_SESSION['userId']))
{
	session_destroy();
	header("location:".$mysiteurl.'logout.php');
}
else
{
	$usertype = $_SESSION["userType"];
	
	$getStates = getStates();
	$getClients = getClients();
	$getCallcenters = getCallcenters('');
	?>

<!DOCTYPE html>
	<html lang="en">
		<head>
			<?php include('../includes/header.php'); ?>
		</head>
		<body class="fixed-nav sticky-footer" id="page-top">
			<?php include('../includes/navigation.php'); ?>
		  
			<div class="content-wrapper">

				<div class="container-fluid">
				
					<!-- Title & Breadcrumbs-->
					<div class="row page-titles <?php echo isset($_POST["btnAdd"])?'':'collapsed' ?>"" data-toggle="collapse" data-target=".viewLeadAcc" style="margin-bottom:0;">
						<div class="col-md-12 align-self-center">
							<h4 class="theme-cl">Deleted Leads <i class="ti-arrow-circle-down pull-right"></i></h4>
						</div>
					</div>
					<!-- Title & Breadcrumbs-->
					
					<!-- row -->
				
					<!-- row -->
					
					<div class="collapse viewLeadAcc <?php echo isset($_POST["btnAdd"])?'show':'' ?>" style="margin-bottom:0;">
						<div class="card-body">
							<form action='' name='frmSearch' id='frmSearch' method='post'>
								<?php include("../includes/messages.php") ?>
								<div class="row">
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_GET['pp'])) echo $_GET['pp'] ?>" class="form-control" id="txtPP" name="txtPP" type="hidden" />
											<input value="<?php if(isset($_POST["txtSearch"])) echo $_POST["txtSearch"] ?>" class="form-control" placeholder='Name / Mobile / Email' id="txtSearch" name="txtSearch" placeholder="" type="text" />
										</div>
									</div>
									
									<?php
									if($_SESSION['userType']==$MSW)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
											  	<a href="#!" id="openSelect1">Select Sol</a>
												<select name="ddlClients[]" id="ddlClients" class="d-flex form-control" multiple>
													<option value=''>Select Sol</option>
													<?php
														while($clientrow = mysqli_fetch_object($getClients))
														{
															?>	
																<option <?php if(isset($_POST['ddlClients']) && count($_POST['ddlClients'])>0){echo in_array($clientrow->uid,$_POST['ddlClients'])?'selected':'';} ?> value='<?php echo $clientrow->uid ?>'><?php echo $clientrow->company_name ?></option>
															<?php
														}
													?>
												</select>
											  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
												<select name="ddlLCallcenters" id="ddlLCallcenters" class="d-flex form-control">
													<option value=''>Select Callcenter</option>
													<?php
														while($ccrow = mysqli_fetch_object($getCallcenters))
														{
															?>	
																<option <?php if(isset($_POST['ddlLCallcenters']) && trim($_POST['ddlLCallcenters'])!=null){echo $_POST['ddlLCallcenters']==$ccrow->uid?'selected':'';} ?> value='<?php echo $ccrow->uid ?>'><?php echo $ccrow->centre_name ?></option>
															<?php
														}
													?>
												</select>
											  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
											  	<a href="#!" id="openSelect2">Select Outcome</a>
												<?php include("outcomes_select.php"); ?>
											  </div>
											</div>
										<?php
									}
									else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==1)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
													<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
													<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
											  	<a href="#!" id="openSelect1">Select Sol</a>
												<select name="ddlClients[]" id="ddlClients" class="d-flex form-control">
													<option value=''>Select Sol</option>
													<?php
														while($clientrow = mysqli_fetch_object($getClients))
														{
															?>	
																<option <?php if(isset($_POST['ddlClients']) && count($_POST['ddlClients'])>0){echo in_array($clientrow->uid,$_POST['ddlClients'])?'selected':'';} ?> value='<?php echo $clientrow->uid ?>'><?php echo $clientrow->company_name ?></option>
															<?php
														}
													?>
												</select>
											  </div>
											</div>
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else if($_SESSION['userType']==$C)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
													<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else if($_SESSION['userType']==$CTL)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
													<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else
									{
										?>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
											<input class="form-control" value="" name="ddlLeadStatus[]" id="ddlLeadStatus" type="hidden" />
										<?php
									}
									?>	
									<div class="col-md-3 col-sm-12 col-xs-12">
									  <div class="form-group">
										<!--<select name="ddlState" id="ddlState" class="d-flex form-control" onchange="getAllSuburbs(this.value)">-->
										<select name="ddlState" id="ddlState" class="d-flex form-control">
											<option value=''>Select State</option>
											<?php
												while($staterow = mysqli_fetch_object($getStates))
												{
													?>	
														<option <?php if(isset($_POST['ddlState']) && trim($_POST['ddlState'])!=null){echo $_POST['ddlState']==$staterow->id?'selected':'';} ?> value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
													<?php
												}
											?>
										</select>
									  </div>
									</div>
									
									<?php
									if(($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0) || $_SESSION['userType']==$CTL)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												<div class="form-group">
													<select name="ddlAgentCheck" id="ddlAgentCheck" class="d-flex form-control">
														<option value=''>Select Agent</option>
														<?php
														$getAgents = getUsersofCallcenter($u_cid,$CA);
														while($arow = mysqli_fetch_object($getAgents))
														{
															?>	
																<option <?php if(isset($_POST['ddlAgentCheck'])){echo $_POST['ddlAgentCheck']==$arow->uid?'selected':'';} ?> value='<?php echo $arow->uid ?>'><?php echo $arow->first_name ?></option>
															<?php
														}
														?>
													</select>
												</div>
											</div>
											<?php
											if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
											{
												?>
													<div class="col-md-3 col-sm-12 col-xs-12">
														<div class="form-group">
															<select name="ddlTeamLeadCheck" id="ddlTeamLeadCheck" class="d-flex form-control">
																<option value=''>Select Team Lead</option>
																<?php
																$getTeamLeads = getUsersofCallcenter($u_cid,$CTL);
																while($tlrow = mysqli_fetch_object($getTeamLeads))
																{
																	?>	
																		<option <?php if(isset($_POST['ddlTeamLeadCheck'])){echo $_POST['ddlTeamLeadCheck']==$tlrow->uid?'selected':'';} ?> value='<?php echo $tlrow->uid ?>'><?php echo $tlrow->first_name ?></option>
																	<?php
																}
																?>
															</select>
														</div>
													</div>
												<?php
											}
									}
									?>
									
									
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtAccidentDateSearchFrom"])) echo $_POST["txtAccidentDateSearchFrom"] ?>" class="form-control" placeholder='Accident Date From' id="txtAccidentDateSearchFrom" name="txtAccidentDateSearchFrom" type="text" />
										</div>
									</div>
									
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtAccidentDateSearchTo"])) echo $_POST["txtAccidentDateSearchTo"] ?>" class="form-control" placeholder='Accident Date To' id="txtAccidentDateSearchTo" name="txtAccidentDateSearchTo" type="text" />
										</div>
									</div>
									
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtFromDate"])) echo $_POST["txtFromDate"] ?>" class="form-control" placeholder='From' id="txtFromDate" name="txtFromDate" type="text" />
										</div>
									</div>
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtToDate"])) echo $_POST["txtToDate"] ?>" class="form-control" placeholder='To' id="txtToDate" name="txtToDate" type="text" />
											<input value="ajaxgetAgentDeletedLeads.php" class="form-control" id="hfAjaxPage" name="hfAjaxPage" placeholder="" type="hidden" />
										</div>
									</div>
									<input value="updation_date" class="form-control" name="ddlSortBy" id="ddlSortBy"  type="hidden" />
									
									<div class="col-md-12 col-sm-12 col-xs-12">
										<input onclick="getPagination(1)" type="button" class="btn btn-primary" value="Search" name="btnAdd" id="btnAdd" />
										<input type="button" class="btn btn-warning" value="Reset" name="btnReset" id="btnReset" onclick="window.location=view-agent-deleted-leads.php'" />
									</div>
								</div>
							</form>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="change-password">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive viewLead ajaxClass">
									</div>
								</div>
							</div>
						</div>
					
						</div>
					</div>
					<!-- /.row -->
				</div>

			</div>
				<!-- /.content-wrapper -->
				<?php include('saveCommentmodal.php'); ?>
				<?php include('callPopUpClientPopUp.php'); ?>
				<?php include('furtherInvestigationModal.php'); ?>
				<?php include('signupModal.php'); ?>
				<?php include('approvemodal.php'); ?>
				<?php include('invoiceModal.php'); ?>
				<?php include('invoicePaidModal.php'); ?>
				<?php include('copymodal.php'); ?>
				<?php include('copymodalShow.php'); ?>
				<?php include('rejectApprovalModal.php'); ?>
				<?php include('rejectApprovalInvoiceModal.php'); ?>
				<?php include('../includes/copyright.php'); ?>
			<!-- Scroll to Top Button-->
			<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
				<i class="ti-angle-double-up"></i>
			</a>
			<?php include('../includes/web_footer.php'); ?>
			<script src="<?php echo $mysiteurl ?>js/jquery.table2excel.js"></script>
			<script type="text/javascript">
				$(document).ready(function() 
				{
					var delay = 1000;
						setTimeout(function() {
						 getPagination(1);
						}, delay);
				});
			</script>
		</body>
	</html>
	<?php
}
