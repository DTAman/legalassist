<?php 
include('../includes/basic_auth.php'); 

if(isset($_GET['t'])&& trim($_GET['t'])==1)
{
	$name='Team Lead';
	$type=$CTL;
}
else
{
	$name='Agent';
	$type=$CA;
}

$getStates = getStates();
$getCountries = getCountries();
$fupAdhar = $fupPAN = $fupDrivingLicense = $fupVoterId = $fupIdProof = $fupPassport = $fupOthers = null;

if(isset($_POST["btnCallCenter"]))
{
	// if(trim($_POST["txtFirstName"])!=null && trim($_POST["txtMiddleName"])!=null && trim($_POST["txtLastName"])!=null && trim($_POST["txtAddAddress"])!=null && trim($_POST["txtAddPhone"])!=null && trim($_POST["ddlCountry"])!=null && trim($_POST["txtAddAdhar"])!=null && trim($_POST["txtAddPAN"])!=null)
	if(trim($_POST["txtFirstName"])!=null && trim($_POST["txtLastName"])!=null && trim($_POST["txtAddAddress"])!=null && trim($_POST["txtAddPhone"])!=null && trim($_POST["ddlCountry"])!=null)
	{
		$count_users = countUsersWithMobile($_POST['txtAddPhone']);

		if($count_users==0)
		{
			$ori_pass = create_unique_pass(); //send pass by message or email
			$password=create_password($ori_pass);
			
			$status='AC';
			
			$loginmaster = $mysqli->prepare("insert into login_master(phone, password, type, status, added_by, creation_date, updation_date) values(?,?,?,?,?,?,?)");
			$loginmaster->bind_param("ssssiss",$_POST['txtAddPhone'],$password,$type,$status,$_SESSION['userId'], $thisdate, $thisdate);
			$loginmaster->execute();
			$loginmaster->close();
			
			$uid = mysqli_insert_id($mysqli);
			
			if(isset($_FILES['fupAdhar']['name']) && trim($_FILES['fupAdhar']['name'])!=null)
			{
				$fupAdhar = file_upload($_FILES['fupAdhar']['name'], $_FILES['fupAdhar']['tmp_name'],'proofs');
			}
			if(isset($_FILES['fupPAN']['name']) && trim($_FILES['fupPAN']['name'])!=null)
			{
				$fupPAN = file_upload($_FILES['fupPAN']['name'], $_FILES['fupPAN']['tmp_name'],'proofs');
			}
			if(isset($_FILES['fupDrivingLicense']['name']) && trim($_FILES['fupDrivingLicense']['name'])!=null)
			{
				$fupDrivingLicense = file_upload($_FILES['fupDrivingLicense']['name'], $_FILES['fupDrivingLicense']['tmp_name'],'proofs');
			}
			if(isset($_FILES['fupVoterId']['name']) && trim($_FILES['fupVoterId']['name'])!=null)
			{
				$fupVoterId = file_upload($_FILES['fupVoterId']['name'], $_FILES['fupVoterId']['tmp_name'],'proofs');
			}
			if(isset($_FILES['fupIdProof']['name']) && trim($_FILES['fupIdProof']['name'])!=null)
			{
				$fupIdProof = file_upload($_FILES['fupIdProof']['name'], $_FILES['fupIdProof']['tmp_name'],'proofs');
			}
			if(isset($_FILES['fupPassport']['name']) && trim($_FILES['fupPassport']['name'])!=null)
			{
				$fupPassport = file_upload($_FILES['fupPassport']['name'], $_FILES['fupPassport']['tmp_name'],'proofs');
			}
			if(isset($_FILES['fupOthers']['name']) && trim($_FILES['fupOthers']['name'])!=null)
			{
				$fupOthers = file_upload($_FILES['fupOthers']['name'], $_FILES['fupOthers']['tmp_name'],'proofs');
			}
			
			$_POST['ddlState']=null;
			$_POST['ddlSubUrb']=null;
			$_POST['txtAddPostal']=null;
			
			$loginmaster = $mysqli->prepare("INSERT INTO tl_and_agents(uid, type, first_name, middle_name, last_name, phone, state_id, suburb_id, postal_code, address, adhar_number, pan_number, bank_name, account_number, account_ifsc, adhar, pan, driving_license, voter_id, id_proof, passport, other_proof, callcenter_id, country) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			$loginmaster->bind_param("isssssiisssssssssssssssi",$uid,$type,$_POST['txtFirstName'],$_POST['txtMiddleName'],$_POST['txtLastName'],$_POST['txtAddPhone'],$_POST['ddlState'],$_POST['ddlSubUrb'],$_POST['txtAddPostal'],$_POST['txtAddAddress'],$_POST['txtAddAdhar'],$_POST['txtAddPAN'],$_POST['txtAddBankName'],$_POST['txtAddBankAccount'],$_POST['txtAddBankIFSC'],$fupAdhar, $fupPAN, $fupDrivingLicense,$fupVoterId, $fupIdProof, $fupPassport, $fupOthers, $_SESSION['userId'], $_POST['ddlCountry']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$_SESSION['success']='Data saved successfully.';
			
			$_POST=array();
			header( "refresh:3;url=add-agent.php?t=".$_GET['t']);
		}
		else
		{
			$_SESSION['error']="A user exists with this mobile number.";
		}
	}
	else
	{
		$_SESSION['error']="Enter all the required fields.";
	}
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>

	<body class="fixed-nav sticky-footer" id="page-top">
	
		<?php include('../includes/navigation.php'); ?>
	  
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Add <?php echo $name ?></h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
			
				<!-- row -->
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
						
							
							<div class="card-body">
								<form name='frmAgent' id='frmAgent' action='' method='post' enctype="multipart/form-data">
									<?php include("../includes/messages.php") ?>
									<div class="row">
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtFirstName">First Name</label>
											<input class="form-control" name="txtFirstName" id="txtFirstName" placeholder="First Name" type="text">
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtMiddleName">Middle Name</label>
											<input class="form-control" name="txtMiddleName" id="txtMiddleName" placeholder="Middle Name" type="text">
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtLastName">Last Name</label>
											<input class="form-control" name="txtLastName" id="txtLastName" placeholder="Last Name" type="text">
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtAddPhone">Phone Number</label>
											<input class="form-control" name="txtAddPhone" id="txtAddPhone" placeholder="Mobile Number" type="text"/>
										  </div>
										</div>
										
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="">Country</label>
											<select name="ddlCountry" id="ddlCountry" class="d-flex form-control">
												<option value=''>Select Country</option>
												<?php
													while($cnrow = mysqli_fetch_object($getCountries))
													{
														?>	
															<option value='<?php echo $cnrow->id ?>'><?php echo $cnrow->name ?></option>
														<?php
													}
												?>
												
											</select>
										  </div>
										</div>
										
										<div class="col-md-12 col-sm-12 col-xs-12">
										  <div class="form-group fullwidthTextarea">
											<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Address First Line" name='txtAddAddress' id='txtAddAddress'></textarea>
										  </div>
										</div>
										<div class="col-md-6 col-sm-12">
										  <div class="form-group">
											<label for="txtAddAdhar">Aadhar Card Number</label>
											<input class="form-control" name="txtAddAdhar" id="txtAddAdhar" placeholder="Adhar Number" type="text" />
										  </div>
										</div>
										<div class="col-md-6 col-sm-12">
										  <div class="form-group">
											<label for="txtAddPAN">Pan Card Number</label>
											<input class="form-control" name="txtAddPAN" id="txtAddPAN" placeholder="PAN Card" type="text" />
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtAddBankName">Bank Name</label>
											<input class="form-control" name="txtAddBankName" id="txtAddBankName" placeholder="Bank Name" type="text" />
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtAddBankAccount">Bank Acc. Number</label>
											<input class="form-control" name="txtAddBankAccount" id="txtAddBankAccount" placeholder="Bank Acc. Number" type="text" />
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtAddBankIFSC">IFSC Code</label>
											<input class="form-control" name="txtAddBankIFSC" id="txtAddBankIFSC" placeholder="IFSC Code" type="text" />
										  </div>
										</div>

										<div class="col-12 d-flex flex-wrap bg-secondary">
											<div class="d-flex flex-column form-group widget bg-transparent pr-4">
												<label>Upload Aadhar</label>
												<input type="file" name="fupAdhar" id="fupAdhar"/>
											</div>
											<div class="d-flex flex-column form-group widget bg-transparent pr-4">
												<label>Upload Pan Card</label>
												<input type="file" name="fupPAN" id="fupPAN"/>
											</div>
											<div class="d-flex flex-column form-group widget bg-transparent pr-4">
												<label>Upload Driving License</label>
												<input type="file" name="fupDrivingLicense" id="fupDrivingLicense"/>
											</div>
											<div class="d-flex flex-column form-group widget bg-transparent pr-4">
												<label>Upload Voter ID</label>
												<input type="file" name="fupVoterId" id="fupVoterId"/>
											</div>
											<div class="d-flex flex-column form-group widget bg-transparent pr-4">
												<label>Upload ID proof</label>
												<input type="file" name="fupIdProof" id="fupIdProof"/>
											</div>
											<div class="d-flex flex-column form-group widget bg-transparent pr-4">
												<label>Upload Passport</label>
												<input type="file" name="fupPassport" id="fupPassport"/>
											</div>
											<div class="d-flex flex-column form-group widget bg-transparent pr-4">
												<label>Upload Others</label>
												<input type="file" name="fupOthers" id="fupOthers"/>
											</div>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input name="btnCallCenter" id="btnCallCenter" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save" />
										</div>	
									  </div>
								</form>
							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			<!-- /.content-wrapper -->
			
			<?php include('../includes/copyright.php'); ?>
			
			<!-- Scroll to Top Button-->  
			<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			  <i class="ti-angle-double-up"></i>
			</a>

			<?php include('../includes/web_footer.php'); ?>
	</body>
</html>
