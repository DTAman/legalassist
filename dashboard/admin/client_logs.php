<?php 
include('../includes/basic_auth.php');

$queryResults = "select reject_after_sol_assign, concat(first_name,' ',middle_name,' ',last_name) as title , lead_comments.comments,lead_comments.creation_date from leads inner join lead_comments on leads.id = lead_comments.lead_id where comment_from=".$_SESSION['userId']." and lead_transfer_type='RJ' order by lead_comments.creation_date desc";

$totalResults = getQuery($queryResults);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
	  
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">View Rejected Leads</h4>
						<input value="[0,1,2,3,4,5,6,7,8,9,10]" class="col_array" placeholder="Title" type="hidden"  />
						<input type="button" class="btn btn-success" value="Export to Excel" name="btnExportToExcel" id="btnExportToExcel" /> 
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
			
				<!-- row -->
				
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
						
							
							<div class="card-body">

								<!-- Table Start -->

								<div class="table-responsive">
									<?php
									if(mysqli_num_rows($totalResults)!=0)
									{
										?>
										<table class="w-100 table table-bordered viewLeadRejectedLogs newTable">
											<thead>
												<tr>
													<th class='text-center'>Sr.No.</th>
													<th>Name</th>
													<th>Rejection Reason</th>
													<th class='text-center'>Created On</th>
												</tr>
											</thead>
											<tbody>
											<?php
											$n = 1;
											while($rc = mysqli_fetch_object($totalResults))
											{
												?>
													<tr>
														<td class='text-center'><?php echo $n.'.' ?></td>
														<td><?php echo $rc->title ?></td>
														<td style='max-width:700px'><?php echo $rc->comments ?>
														<?php  
														if($rc->reject_after_sol_assign==1)
														{
															?>
																<span style='color:red'>(Pulled By Admin)</span>
															<?php
														}
														?>
														</td>
														<td class='text-center'><?php echo get_timezone_offset(date('Y-m-d H:i:s',strtotime($rc->creation_date)),'UTC',$_SESSION['userTimeZone']) ?></td>
													</tr>
												<?php
												$n++;
											}
											?>
											</tbody>
										</table>
										<?php
									}
									else
									{
										include('../includes/norows.php'); 
									}
									?>
								</div>

								<!-- Table End -->

							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			<!-- /.content-wrapper -->
			<?php include('saveCommentmodal.php'); ?>
			<?php include('furtherInvestigationModal.php'); ?>
			<?php include('signupModal.php'); ?>
			<?php include('approvemodal.php'); ?>
			<?php include('invoiceModal.php'); ?>
			<?php include('invoicePaidModal.php'); ?>
			<?php include('../includes/copyright.php'); ?>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
		<script src="<?php echo $mysiteurl ?>js/jquery.table2excel.js"></script>
	</body>
</html>
