<?php
	$getZones = getStates();
?>
<div id="callbackModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Enter CallBack Details</h4>
			</div>
			<form action='' method='post' name='frmLeadCallBack' id='frmLeadCallBack'>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<input class="form-control" value="" name="hfLeadCallBackId" id="hfLeadCallBackId" type="hidden" />	
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<label class="d-block" for="">Select CallBack Reason</label>
								<div class="custom-radio d-inline-block fl-left mr-3">
									<input checked type="radio" class="custom-control-input label_no_weight_class" id="rbtnLeadCallbackTypeNo" name="rbtnLeadCallbackType" value="N">
									<label class="custom-control-label label_no_weight" for="rbtnLeadCallbackTypeNo">No Answer</label>
								</div>
								<div class="custom-radio d-inline-block fl-left mr-3">
									<input type="radio" class="custom-control-input label_no_weight_class" id="rbtnLeadCallbackTypeYes" name="rbtnLeadCallbackType" value="Y">
									<label class="custom-control-label label_no_weight" for="rbtnLeadCallbackTypeYes">Call back requested by the client</label>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<label for="txtLCallBackDOB">Date</label>
									<div class="calender-wraps">
										<?php
											$day_cal='';
											$mon_cal='';
											$year_cal='';
											$hours_cal='';
											$min_cal='';
											$selected='';
											
											for($day=1;$day<=31;$day++)
											{
												$day_cal.= "<option ".$selected." value='".$day."'>".$day."</option>";
											}
											
											for($mon=1;$mon<=12;$mon++)
											{
												$mon_cal.= "<option ".$selected." value='".$mon."'>".substr(date('F', mktime(0,0,0,$mon, 1, date('Y'))),0,3)."</option>";
											}
											
											$running_year = date('Y')+5;
											for($year=$running_year;$year>=$running_year-100;$year--)
											{
												$year_cal.= "<option ".$selected." value='".$year."'>".$year."</option>";
											}
											
											for($hours=0;$hours<=23;$hours++)
											{
												$hours_cal.= "<option ".$selected." value='".$hours."'>".$hours."</option>";
											}
											
											for($min=0;$min<=59;$min++)
											{
												$min_cal.= "<option ".$selected." value='".$min."'>".$min."</option>";
											}
										?>
										<div>
											<select required="required" name="txtCCallBackDay" id="txtCCallBackDay" class="form-control">
												<option value="">Day</option><?php echo $day_cal ?>
											</select>
										</div>
										<div>
											<select required="required" name="txtCCallBackMonth" id="txtCCallBackMonth" class="form-control">
												<option value="">Mon</option><?php echo $mon_cal ?>
											</select>
										</div>
										<div>
											<select required="required" name="txtCCallBackYear" id="txtCCallBackYear" class="form-control">
												<option value="">Year</option><?php echo $year_cal ?>
											</select>
										</div>
										<div>
											<select required="required"  name="txtCCallBackHours" id="txtCCallBackHours" class="form-control">
												<option value="">Hour</option><?php echo $hours_cal ?>
											</select>
										</div>
										<div>
											<select required="required"  name="txtCCallBackMinutes" id="txtCCallBackMinutes" class="form-control">
												<option value="">Min</option><?php echo $min_cal ?>
											</select>
										</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<select name="ddlCallBackState" id="ddlCallBackState" class="d-flex form-control" required>
									<option value=''>Select State (Zone)</option>
									<?php
										while($zrow = mysqli_fetch_object($getZones))
										{
											?>	
												<option value='<?php echo $zrow->id ?>'><?php echo $zrow->state_name." (".$zrow->timezones.")" ?></option>
											<?php
										}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter details here..." name='txtLeadCallBackComment' id='txtLeadCallBackComment'></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnLeadCallBack" name="btnLeadCallBack" type="submit" class="btn btn-primary" value='Save' />
				</div>
			</form>
		</div>
	</div>
</div>