<?php
	include('../includes/basic_auth.php');
	
	if(isset($_POST['delId']) && trim($_POST['delId'])!=null)
	{
		$vmaster = $mysqli->prepare("update lead_documents set doc".$_POST['num']." = NULL, desc".$_POST['num']." = NULL where id = ?");
		$vmaster->bind_param("i",$_POST['delId']);
		$vmaster->execute();
		$vmaster->close();
	}
	
	if(isset($_POST['txtLeadId']) && trim($_POST['txtLeadId'])!=null)
	{
		// $view_documents = getQuery('select * from lead_documents where lead_accident_id='.$aid.' and lead_id='.$uid);
		$view_documents = getQuery('select * from lead_documents where lead_id='.$_POST['txtLeadId']);
		$document_flag = mysqli_num_rows($view_documents);
		if($document_flag>0)
		{
			$rc10 = mysqli_fetch_object($view_documents);
			
			$fupDoc1=$rc10->doc1;
			$fupDoc2=$rc10->doc2;
			$fupDoc3=$rc10->doc3;
			$fupDoc4=$rc10->doc4;
			$fupDoc5=$rc10->doc5;
			$fupDoc6=$rc10->doc6;
			$fupDoc7=$rc10->doc7;
			
			$fupDoc1Desc=$rc10->desc1;
			$fupDoc2Desc=$rc10->desc2;
			$fupDoc3Desc=$rc10->desc3;
			$fupDoc4Desc=$rc10->desc4;
			$fupDoc5Desc=$rc10->desc5;
			$fupDoc6Desc=$rc10->desc6;
			$fupDoc7Desc=$rc10->desc7;
		}
		else
		{
			$fupDoc1=$fupDoc2=$fupDoc3=$fupDoc4=$fupDoc5=$fupDoc6=$fupDoc7=null;
			$fupDoc1Desc=$fupDoc2Desc=$fupDoc3Desc=$fupDoc4Desc=$fupDoc5Desc=$fupDoc6Desc=$fupDoc7Desc=null;
		}
		?>
	
		<input value="<?php echo $_POST['txtLeadId'] ?>" type="hidden" name="hfDocumentLeadId" id="hfDocumentLeadId"/>
		<div class="col-12 d-flex flex-wrap bg-secondary">
			<div class="col-md-6 form-group">
				<label>Document 1</label>
				<?php
				if(isset($rc10->doc1) && trim($rc10->doc1)!=null)
				{
					?>
						<a href="<?php echo $mysiteurl.has_file_on_server($rc10->doc1) ?>" class="text-success mr-3 ml-3" download><i class="fa fa-download"></i></a>
						<a href="javascript:void(0)" onclick="deleteUploadFile(1,<?php echo $rc10->id ?>,<?php echo $rc10->lead_id ?>)" class="text-danger" download><i class="fa fa-trash"></i></a>
					<?php
				}
				?>
				<input type="file" name="fupDoc1" id="fupDoc1" class="mb-2"/>
				<input value="<?php echo $fupDoc1Desc ?>" placeholder='Description' class="form-control" type="text" name="fupDoc1Desc" id="fupDoc1Desc"/>
			</div>
			<div class="col-md-6 form-group">
				<label>Document 2</label>
				<?php
				if(isset($rc10->doc2) && trim($rc10->doc2)!=null)
				{
					?>
						<a href="<?php echo $mysiteurl.has_file_on_server($rc10->doc2) ?>" class="text-success mr-3 ml-3" download><i class="fa fa-download"></i></a>
						<a href="javascript:void(0)" onclick="deleteUploadFile(2,<?php echo $rc10->id ?>,<?php echo $rc10->lead_id ?>)" class="text-danger" download><i class="fa fa-trash"></i></a>
					<?php
				}
				?>
				<input type="file" name="fupDoc2" id="fupDoc2" class="mb-2"/>
				<input value="<?php echo $fupDoc2Desc ?>" placeholder='Description' class="form-control" type="text" name="fupDoc2Desc" id="fupDoc2Desc"/>
			</div>
			<div class="col-md-6 form-group">
				<label>Document 3</label>
				<?php
				if(isset($rc10->doc3) && trim($rc10->doc3)!=null)
				{
					?>
						<a href="<?php echo $mysiteurl.has_file_on_server($rc10->doc3) ?>" class="text-success mr-3 ml-3" download><i class="fa fa-download"></i></a>
						<a href="javascript:void(0)" onclick="deleteUploadFile(3,<?php echo $rc10->id ?>,<?php echo $rc10->lead_id ?>)" class="text-danger" download><i class="fa fa-trash"></i></a>
					<?php
				}
				?>
				<input type="file" name="fupDoc3" id="fupDoc3" class="mb-2"/>
				<input value="<?php echo $fupDoc3Desc ?>" placeholder='Description' class="form-control" type="text" name="fupDoc3Desc" id="fupDoc3Desc"/>
			</div>
			<div class="col-md-6 form-group">
				<label>Document 4</label>
				<?php
				if(isset($rc10->doc4) && trim($rc10->doc4)!=null)
				{
					?>
						<a href="<?php echo $mysiteurl.has_file_on_server($rc10->doc4) ?>" class="text-success mr-3 ml-3" download><i class="fa fa-download"></i></a>
						<a href="javascript:void(0)" onclick="deleteUploadFile(4,<?php echo $rc10->id ?>,<?php echo $rc10->lead_id ?>)" class="text-danger" download><i class="fa fa-trash"></i></a>
					<?php
				}
				?>
				<input type="file" name="fupDoc4" id="fupDoc4" class="mb-2"/>
				<input value="<?php echo $fupDoc4Desc ?>" placeholder='Description' class="form-control" type="text" name="fupDoc4Desc" id="fupDoc4Desc"/>
			</div>
			<div class="col-md-6 form-group">
				<label>Document 5</label>
				<?php
				if(isset($rc10->doc5) && trim($rc10->doc5)!=null)
				{
					?>
						<a href="<?php echo $mysiteurl.has_file_on_server($rc10->doc5) ?>" class="text-success mr-3 ml-3" download><i class="fa fa-download"></i></a>
						<a href="javascript:void(0)" onclick="deleteUploadFile(5,<?php echo $rc10->id ?>,<?php echo $rc10->lead_id ?>)" class="text-danger" download><i class="fa fa-trash"></i></a>
					<?php
				}
				?>
				<input type="file" name="fupDoc5" id="fupDoc5" class="mb-2"/>
				<input value="<?php echo $fupDoc5Desc ?>" placeholder='Description' class="form-control" type="text" name="fupDoc5Desc" id="fupDoc5Desc"/>
			</div>
			<div class="col-md-6 form-group">
				<label>Document 6</label>
				<?php
				if(isset($rc10->doc6) && trim($rc10->doc6)!=null)
				{
					?>
						<a href="<?php echo $mysiteurl.has_file_on_server($rc10->doc6) ?>" class="text-success mr-3 ml-3" download><i class="fa fa-download"></i></a>
						<a href="javascript:void(0)" onclick="deleteUploadFile(6,<?php echo $rc10->id ?>,<?php echo $rc10->lead_id ?>)" class="text-danger" download><i class="fa fa-trash"></i></a>
					<?php
				}
				?>
				<input type="file" name="fupDoc6" id="fupDoc6" class="mb-2"/>
				<input value="<?php echo $fupDoc6Desc ?>" placeholder='Description' class="form-control" type="text" name="fupDoc6Desc" id="fupDoc6Desc"/>
			</div>
			<div class="col-md-6 form-group">
				<label>Document 7</label>
				<?php
				if(isset($rc10->doc7) && trim($rc10->doc7)!=null)
				{
					?>
						<a href="<?php echo $mysiteurl.has_file_on_server($rc10->doc7) ?>" class="text-success mr-3 ml-3" download><i class="fa fa-download"></i></a>
						<a href="javascript:void(0)" onclick="deleteUploadFile(7,<?php echo $rc10->id ?>,<?php echo $rc10->lead_id ?>)" class="text-danger" download><i class="fa fa-trash"></i></a>
					<?php
				}
				?>
				<input type="file" name="fupDoc7" id="fupDoc7" class="mb-2"/>
				<input value="<?php echo $fupDoc7Desc ?>" placeholder='Description' class="form-control" type="text" name="fupDoc7Desc" id="fupDoc7Desc"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<input id="btnSaveDocument" name="btnSaveDocument" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value='Save' />
			</div>
		</div>
		
		<?php
	}
?>