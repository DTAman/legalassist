<?php 
include('../includes/basic_auth.php'); 
$is_dis = 0;
if(isset($_GET['s']) && trim($_GET['s']==1))
{
	$is_dis = 1;
}

if(isset($_GET['u']) && trim($_GET['u']!=null))
{
	$uid = trim($_GET['u']);
	
	$view_result = getUserData($uid, $C);
	if(mysqli_num_rows($view_result)==0)
	{
		header("location:index.php");
	}
	else
	{
		$rc = mysqli_fetch_object($view_result);
	}
}
else
{
	header("location:index.php");
}

if(isset($_POST["btnGeneratePassword"]))
{
	$pswd = generate_password($uid);
	$_SESSION['success']='Password has been changed successfully.<br/>New Password is: '.$pswd;
	
	$_POST=array();
}

$getStates = getStates();

if(isset($_POST["btnClient"]))
{
	if(trim($_POST["txtClientCompany"])!=null && trim($_POST["ddlState"])!=null && trim($_POST["ddlSubUrb"])!=null && trim($_POST["txtClientAddress"])!=null && trim($_POST["txtClientEmail"])!=null && trim($_POST["txtClientPostal"])!=null)
	{
		$count_users = countUsersWithMobileforEdit($_POST['txtAddPhone'],$uid);
		
		if($count_users==0)
		{
			$loginmaster = $mysqli->prepare("update login_master set phone=?, status=?, updation_date=? where uid=?");
			$loginmaster->bind_param("sssi",$_POST['txtAddPhone'],$_POST['rbtnStatus'],$thisdate, $uid);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("update clients set company_name=?, auth_person=?, person_designation=?, email=?, state_id=?, suburb_id=?, postal_code=?, address=?,status=?, updation_date=? where uid=?");
			$loginmaster->bind_param("ssssiissssi",$_POST['txtClientCompany'],$_POST['txtClientName'],$_POST['txtClientDesignation'],$_POST['txtClientEmail'],$_POST['ddlState'],$_POST['ddlSubUrb'],$_POST['txtClientPostal'],$_POST['txtClientAddress'],$_POST['rbtnStatus'],$thisdate, $uid);
			$loginmaster->execute();
			// echo $mysqli->error;
			$loginmaster->close();

			/*$_POST['txtCCEmails'] = array(0=>'abc@gmail.com',1=>'abc1@gmail.com',2=>'abc2@gmail.com');*/

			$loginmaster = $mysqli->prepare("delete from clients_bcc where master_client_id=?");
			$loginmaster->bind_param("i",$uid);
			$loginmaster->execute();
			echo $mysqli->error;
			$loginmaster->close();

			if(isset($_POST['txtCCEmails']) && count($_POST['txtCCEmails'])>0)
			{
				foreach ($_POST['txtCCEmails'] as $key => $cc_email) 
				{
					if(trim($cc_email)!=null)
					{
						$loginmaster = $mysqli->prepare("insert into clients_bcc(master_client_id, cc_email, added_by, creation_date) values(?,?,?,?)");
						$loginmaster->bind_param("isis",$uid,$cc_email,$_SESSION['userId'], $thisdate);
						$loginmaster->execute();
						$loginmaster->close();
					}
				}
			}
			
			$_SESSION['success']='Client updated successfully.';
			header( "refresh:3;url=edit-client-name-each.php?u=".$uid);
		}
		else
		{
			$_SESSION['error']="A user exists with this mobile number.";
		}
	}
	else
	{
		$_SESSION['error']="Enter all the required fields.";
	}
}


$clients_bcc  = getQuery('select cc_email from clients_bcc where master_client_id = '.$uid);

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl"><?php echo $is_dis!=1?'Edit':'View' ?> Sol</h4>
					</div>
				</div>
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
						
							
							<div class="card-body">
								<form name='frmEditClients' id='frmEditClients' action='' method='post'>
									<?php include("../includes/messages.php") ?>
									<div class="row">
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtClientCompany">Name Of Company</label>
											<input value="<?php echo $rc->company_name ?>" class="form-control" name="txtClientCompany" id="txtClientCompany" placeholder="Company Name" type="text" />
											<input value="<?php echo $rc->uid ?>" name="hfUID" id="hfUID" type="hidden" />
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtClientName">Authorized Person Name</label>
											<input value="<?php echo $rc->auth_person ?>" class="form-control" name="txtClientName" id="txtClientName" placeholder="Authorised Person Name" type="text">
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtClientDesignation">Authorized Person Designation</label>
											<input value="<?php echo $rc->person_designation ?>" class="form-control" name="txtClientDesignation" id="txtClientDesignation" placeholder="Designation" type="text">
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtAddPhone">Phone Number</label>
											<input value="<?php echo $rc->phone ?>"   class="form-control" name="txtAddPhone" id="txtAddPhone" placeholder="Mobile Number" type="text"/>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtClientEmail">E-Mail ID</label>
											<input value="<?php echo $rc->email ?>" class="form-control" name="txtClientEmail" id="txtClientEmail" placeholder="E-mail" type="text">
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="ddlState">Address</label>
											<select name="ddlState" id="ddlState" class="d-flex form-control" onchange="getAllSuburbs(this.value)">
												<option value=''>Select State</option>
												<?php
													while($staterow = mysqli_fetch_object($getStates))
													{
														?>	
															<option <?php echo $rc->state_id==$staterow->id?'selected':''; ?> value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
														<?php
													}
												?>
												
											</select>
										  </div>
										</div>
										
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="ddlSubUrb" class="invisible d-none d-md-block" >Select Sub urb</label>
											<select name="ddlSubUrb" id="ddlSubUrb" class="d-flex form-control">
												<?php
												$getSubUrbs = getSubUrbs(" and state_id = ".$rc->state_id);
												while($srow = mysqli_fetch_object($getSubUrbs))
												{
													?>	
														<option value='<?php echo $srow->id ?>' <?php echo $rc->suburb_id==$srow->id?'selected':''; ?>><?php echo $srow->suburb_name ?></option>
													<?php
												}
												?>
											</select>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="txtClientPostal" class="invisible d-none d-md-block" >Postal Code</label>
											<input value="<?php echo $rc->postal_code ?>" class="form-control" name="txtClientPostal" id="txtClientPostal"  placeholder="Post Code" type="text">
										  </div>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
										 <div class="form-group fullwidthTextarea">
											<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Address First Line" name='txtClientAddress' id='txtClientAddress'><?php echo $rc->address ?></textarea>
										  </div>
										</div>
										
										<div class="col-md-12 col-sm-12 col-xs-12">
											<label class="d-block">Status</label>
											<div class="custom-radio d-inline-block fl-left mr-3">
												<input <?php echo $rc->status=='AC'?'checked':''; ?> type="radio" class="custom-control-input" id="rbtnActive" name="rbtnStatus" value="AC"/>
												<label class="custom-control-label" for="rbtnActive">Active</label>
											</div>
											<div class="custom-radio d-inline-block fl-left mr-3">
												<input <?php echo $rc->status=='IN'?'checked':''; ?> type="radio" class="custom-control-input" id="rbtnInactive" name="rbtnStatus" value="IN"/>
												<label class="custom-control-label" for="rbtnInactive">Inactive</label>
											</div>
										</div>
										</div>
										<!-------------------------------------------------------------------------------->
										<br/>
										<div class="row">
											<div class="col-md-8 col-10">
												<label for="txtClientCompany">Sol Emails</label>
											</div>
											<?php
											if($is_dis!=1)
											{
												?>
													<div class="col-md-4 col-2">
														<i class="addClientsBox bg-success fa fa-plus" onclick="appendTextBoxCCEmail()"></i>
													</div>
												<?php
											}
											?>
										</div>
										
									
										<div id="appendTextBoxCCOuter">
											<?php
												if(mysqli_num_rows($clients_bcc)!=0)
												{
													$ci = 1;
													while($cc_email_row = mysqli_fetch_object($clients_bcc))
													{
														?>
															<div class="row appendTextBoxCCEmail">
																<div class="col-md-8 col-10">	
																	<div class="form-group">
																		<input value="<?php echo $cc_email_row->cc_email; ?>" class="form-control" name="txtCCEmails[]" id="txtCCEmails1" type="email" />
																	</div>
																</div>
																<?php
																if($is_dis!=1)
																{
																	?>
																		<div class="col-md-4 col-2">
																			<i class="addClientsBox bg-danger fa fa-minus removeTextBoxCCEmail"></i>
																		</div>
																	<?php
																}
																?>
																
															</div>
														<?php
														$ci++;
													}
												}
												else if($is_dis!=1)
												{
													?>
														<div class="row appendTextBoxCCEmail">
															<div class="col-md-8 col-10">	
																<div class="form-group">
																	<input value="" class="form-control" name="txtCCEmails[]" id="txtCCEmails1" type="email" />
																</div>
															</div>
															<div class="col-md-4 col-2">
																<i class="addClientsBox bg-danger fa fa-minus removeTextBoxCCEmail"></i>
															</div>
														</div>
													<?php
												}
											?>
										</div>

										<!-------------------------------------------------------------------------------->
										
										<div class="row">
											<?php
												if($is_dis!=1)
												{
													?>
														<div class="col-md-12 col-sm-12 col-xs-12">
															<input name="btnClient" id="btnClient" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save" />
														</div>	
													<?php
												}
												else
												{
													?>
													<div class="col-md-12 col-sm-12 col-xs-12 text-right">
														<input name="btnGeneratePassword" id="btnGeneratePassword" type="submit" class="btn btn-danger ml-0 mr-3 mt-2" value="Generate Password" onclick="return confirm('Are you sure?')" />
													</div>
													<?php
												}
												?>
										</div>

										
									  
								</form>
							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			<!-- /.content-wrapper -->
				<?php include('../includes/copyright.php'); ?>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
		<?php
			if($is_dis==1)
			{
				?>
					<script>is_disabled(<?php echo $is_dis ?>);</script>
				<?php
			}
		?>
	</body>
</html>
