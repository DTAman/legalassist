<?php
	include('../includes/basic_auth.php');
	include('send_sms.php');
	
	if(isset($_POST['hfLeadIdPhone']) && trim($_POST['hfLeadIdPhone'])!=null && isset($_POST['hfLeadPhone']) && trim($_POST['hfLeadPhone'])!=null && isset($_POST['ddlPhoneTemplates']) && trim($_POST['ddlPhoneTemplates'])!=null)
	{
		$has_attachments_array = array();
		$email_templates = phoneTemplates($_SESSION['userType']," and sid = ".$_POST['ddlPhoneTemplates']);
		$em = mysqli_fetch_object($email_templates);
		
		$api = new wholesalesms($api_key_sms,$api_secret_sms);
		// echo $api->getBalance();
		$response = $api->sendMessage($_POST['hfLeadPhone'],$em->sms_body);
		
		$x = json_decode($response);
		
		if(isset($x->error->code) && $x->error->code=='SUCCESS')
		{
			$comment_status = 'PN';
			$lead_transfer_type = 'LT';
			
			$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
			$loginmaster->bind_param("i",$_POST['hfLeadIdPhone']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$comments="<span class='emailClassL'>Template: </span> ".$em->subject;
			$comments.="<br/><span class='emailClassL'>Number: </span> ".$_POST['hfLeadPhone'];
			
			$latest_comment="SMS Sent (".$_POST['hfLeadPhone']."), Template: ".$em->subject;
			updateLastComment($_POST['hfLeadIdPhone'],$latest_comment,$_SESSION['userType'],$_SESSION['userType']);

			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type, sms_sent) VALUES (?,?,?,?,?,?,?,?,?)");
			$loginmaster->bind_param("iisissssi",$_POST['hfLeadIdPhone'],$_SESSION['userId'],$_SESSION['userType'],$_SESSION['userId'],$comments,$comment_status,$thisdate,$lead_transfer_type,$_POST['ddlPhoneTemplates']);
			$loginmaster->execute();
			$loginmaster->close();
			
			echo 'Message Delivered';
		}
		else if(isset($x->error->description))
		{
			echo $x->error->description;
		}
		else
		{
			echo 'Message not sent.';
		}
	}
?>