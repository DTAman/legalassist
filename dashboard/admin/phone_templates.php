<div id="phoneTemplate" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Send Message</h4>
			</div>
			<form action='' method='post' name='frmPhoneTemplate' id='frmPhoneTemplate'>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<input class="form-control" value="" name="hfLeadIdPhone" id="hfLeadIdPhone" type="hidden" />
								<input class="form-control" value="" name="hfLeadPhone" id="hfLeadPhone" type="text" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<select required="required" name="ddlPhoneTemplates" id="ddlPhoneTemplates" class="form-control">
									<option value="">Select Template</option>
									<?php
									$phoneTemplates = phoneTemplates($_SESSION['userType'],'');
									while($em = mysqli_fetch_object($phoneTemplates))
									{
										?>
											<option value="<?php echo $em->sid?>"><?php echo $em->subject?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnSendPhone" name="btnSendPhone" type="submit" class="btn btn-primary" value='Send SMS' />
				</div>
			</form>
		</div>
	</div>
</div>