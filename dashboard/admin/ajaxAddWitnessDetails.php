<?php
include('../includes/basic_auth.php');

if(isset($_POST["witnessO"]) && isset($_POST["hfLeadId"]) && isset($_POST["txt9Address"]) && isset($_POST["txt9WitnessName"]) && isset($_POST["txt9WitnessPhone"]) && isset($_POST["txt9WitnessPostalCode"]) && isset($_POST["ddl9State"]) && isset($_POST["ddl9SubUrb"]) && trim($_POST["hfLeadId"])>0 && isset($_POST['hfLeadAccidentId']) && trim($_POST['hfLeadAccidentId'])!=0)
{
	/************************* If any one field in not empty and radio button should not be false  *******************/
	if($_POST["witnessO"]==1 && (trim($_POST["hfLeadId"])!=null || trim($_POST["txt9Address"])!=null || trim($_POST["txt9WitnessName"])!=null || trim($_POST["txt9WitnessPhone"])!=null || trim($_POST["txt9WitnessPostalCode"])!=null || trim($_POST["ddl9State"])!=null || trim($_POST["ddl9SubUrb"])))
	{
		$lead_id = $_POST["hfLeadId"];
		$hfLeadAccidentId = $_POST["hfLeadAccidentId"];
		$ddl9State = trim($_POST["ddl9State"])==null?null:$_POST["ddl9State"];
		$ddl9SubUrb = trim($_POST["ddl9SubUrb"])==null?null:$_POST["ddl9SubUrb"];
		
		$view_witness = getQuery('select id from lead_witness where lead_accident_id='.$hfLeadAccidentId);
		if(mysqli_num_rows($view_witness)>0)
		{
			$rc9 = mysqli_fetch_object($view_witness);
			
			$loginmaster = $mysqli->prepare("update lead_witness set lead_id=?, name=?, phone=?, state_id=?, suburb_id=?, postal_code=?, address=? where id=?");
			$loginmaster->bind_param("issiissi",$lead_id,$_POST['txt9WitnessName'],$_POST['txt9WitnessPhone'],$ddl9State,$ddl9SubUrb,$_POST['txt9WitnessPostalCode'],$_POST['txt9Address'],$rc9->id);
		}
		else
		{
			$loginmaster = $mysqli->prepare("INSERT INTO lead_witness(lead_id, name, phone, state_id, suburb_id, postal_code, address, lead_accident_id) VALUES (?,?,?,?,?,?,?,?)");
			$loginmaster->bind_param("issiissi",$lead_id,$_POST['txt9WitnessName'],$_POST['txt9WitnessPhone'],$ddl9State,$ddl9SubUrb,$_POST['txt9WitnessPostalCode'],$_POST['txt9Address'], $hfLeadAccidentId);
		}
		$loginmaster->execute();
		$loginmaster->close();
	}
	else
	{
		$loginmaster = $mysqli->prepare("delete from lead_witness where lead_accident_id=?");
		$loginmaster->bind_param("i",$_POST['hfLeadAccidentId']);
		$loginmaster->execute();
		$loginmaster->close();
	}
}
else
{
	$loginmaster = $mysqli->prepare("delete from lead_witness where lead_id=?");
	$loginmaster->bind_param("i",$_POST['hfLeadAccidentId']);
	$loginmaster->execute();
	$loginmaster->close();
}
?>