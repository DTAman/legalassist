<?php 
include('../includes/basic_auth.php');
include('xlsxwriter.class.php');

$condd = '';
$condition = '';
$n=1;
$orderby = "updation_date";

if($_SESSION['userType']==$CA || $_SESSION['userType']==$CTL)
{
	$datamaster = $mysqli->prepare("SELECT callcenter_id FROM tl_and_agents where uid = ?");
	$datamaster->bind_param('i',$_SESSION['userId']);
	$datamaster->execute();
	$datamaster->bind_result($u_cid);
	$datamaster->fetch();
	$datamaster->close();
}
else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
{
	$u_cid = $_SESSION['userId'];
}

$team_lead_array = array();
$agents_array = array();
$client_array = array();
$callcenter_array = array();
$verifier_array = array();

if(isset($u_cid))
{
	$getAgents = getUsersofCallcenter($u_cid,$CA);
	$getTeamLeads = getUsersofCallcenter($u_cid,$CTL);
}
$getClients = getClients();
$getCallcenters = getCallcenters('');
$getVerifiers = getVerifiers('');

if(isset($u_cid))
{
	while($tlrow = mysqli_fetch_object($getTeamLeads))
	{
		$team_lead_array[$tlrow->uid] = $tlrow->first_name.' '.$tlrow->last_name;
	}
	while($arow = mysqli_fetch_object($getAgents))
	{
		$agents_array[$arow->uid] = $arow->first_name.' '.$arow->last_name;
	}
}
while($vcrow = mysqli_fetch_object($getVerifiers))
{
	$verifier_array[$vcrow->uid] = $vcrow->verifier_name;
}
while($clientrow = mysqli_fetch_object($getClients))
{
	$client_array[$clientrow->uid] = $clientrow->company_name;
}
while($ccrow = mysqli_fetch_object($getCallcenters))
{
	$callcenter_array[$ccrow->uid] = $ccrow->centre_name;
}

if($_SESSION['userType']!=$MSW)
{
	$condition.=" and admin_assigned_directly=0 ";
}
	
$usertype = $_SESSION["userType"];
$linked_ids_con = $condition;

if(isset($_POST['ddlVerifier']) && trim($_POST['ddlVerifier'])!=null)
{
	$condition.=" AND assigned_verifier =  ".$_POST['ddlVerifier'];
}

if(isset($_POST['txtSearch']) && trim($_POST['txtSearch'])!=null)
{
	$condition.=" and (phone like '%{$_POST['txtSearch']}%' or email like '%{$_POST['txtSearch']}%' or first_name like '%{$_POST['txtSearch']}%' or middle_name like '%{$_POST['txtSearch']}%' or last_name like '%{$_POST['txtSearch']}%') ";
}

if(isset($_POST['txtFromDate']) && trim($_POST['txtFromDate'])!=null)
{
	$create_date=date('Y-m-d',strtotime($_POST['txtFromDate']));
	$condition.=" and date(updation_date) >= '".get_timezone_offset_calender($create_date,'UTC',$_SESSION['userTimeZone'])."'";
}

if(isset($_POST['txtToDate']) && trim($_POST['txtToDate'])!=null)
{
	$to_date=date('Y-m-d',strtotime($_POST['txtToDate']));
	$condition.=" and date(updation_date) <= '".get_timezone_offset_calender($to_date,'UTC',$_SESSION['userTimeZone'])."'";
}

if(isset($_POST['txtAccidentDateSearchFrom']) && isset($_POST['txtAccidentDateSearchTo']) && trim($_POST['txtAccidentDateSearchFrom'])!=null && trim($_POST['txtAccidentDateSearchTo'])!=null)
{
	$ac_date_from=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchFrom']));
	$ac_date_to=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchTo']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) >= '".$ac_date_from."' and date(accident_date_time) <= '".$ac_date_to."' and lead_accidents.status=1)) ";
}
else if(isset($_POST['txtAccidentDateSearchFrom']) && trim($_POST['txtAccidentDateSearchFrom'])!=null)
{
	$ac_date_from=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchFrom']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) >= '".$ac_date_from."' and lead_accidents.status=1)) ";
}
else if(isset($_POST['txtAccidentDateSearchTo']) && trim($_POST['txtAccidentDateSearchTo'])!=null)
{
	$ac_date_to=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchTo']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) <= '".$ac_date_to."' and lead_accidents.status=1)) ";
}
		
if(isset($_POST['ddlState']) && trim($_POST['ddlState'])!=null)
{
	$condition.=" and leads.state_id = ".$_POST['ddlState'];
}

if(isset($_POST['ddlLCallcenters']) && trim($_POST['ddlLCallcenters'])!=null)
{
	$condition.=" and leads.callcenter_id = ".$_POST['ddlLCallcenters'];
}

if(isset($_POST['ddlClients']) && is_array($_POST['ddlClients']) && count(array_filter($_POST['ddlClients']))>0)
{
	$condition.=" and assigned_client in (".trim(implode(',',$_POST['ddlClients']),',').")";
}

if(isset($_POST['ddlTeamLeads']) && trim($_POST['ddlTeamLeads'])!=null)
{
	$condition.=" AND team_lead_assigned =  ".$_POST['ddlTeamLeads'];
}

if(isset($_POST['ddlAgents']) && trim($_POST['ddlAgents'])!=null)
{
	$condition.=" AND agent_assigned =  ".$_POST['ddlAgents'];
}

if(isset($_POST['ddlSubUrb']) && trim($_POST['ddlSubUrb'])!=null)
{
	$condition.=" and leads.suburb_id = ".$_POST['ddlSubUrb'];
}

$outcome_array = array();
$c1='';
$c2='';
if(isset($_POST['ddlLeadStatus']) && is_array($_POST['ddlLeadStatus']) && count($_POST['ddlLeadStatus'])>0)
{
	foreach($_POST['ddlLeadStatus'] as $ddlLeadStatus)
	{
		if(trim($ddlLeadStatus)!=null)
		{
			array_push($outcome_array,"'".$ddlLeadStatus."'");
		}
	}
	
	if(count($outcome_array)>0)
	{
		$c1.=" leads.outcome_type in (".implode(',',$outcome_array).") ";
	}
		
	if(trim($c1)!=null || trim($c2)!=null)
	{
		$condition.=" and (".$c1.$c2.")";
	}
}

if($usertype=='CA')
{
	$condition.=" AND assigned_usertype = 'CA' and agent_assigned =  ".$_SESSION['userId']."  AND callcenter_id = ".$u_cid;
	$linked_ids_con.=" AND assigned_usertype = 'CA' and agent_assigned =  ".$_SESSION['userId']."  AND callcenter_id = ".$u_cid;
}
else if($usertype=='VA')
{
	$condition.=" AND assigned_verifier =  ".$_SESSION['userId'];
	$condition.=" AND assigned_usertype = 'VA'";
	$linked_ids_con.=" AND assigned_verifier =  ".$_SESSION['userId'];
	$linked_ids_con.=" AND assigned_usertype = 'VA'";
}
else if($usertype=='CTL')
{	
	$condition.=" AND team_lead_assigned =  ".$_SESSION['userId']."  AND callcenter_id = ".$u_cid;
	$linked_ids_con.=" AND team_lead_assigned =  ".$_SESSION['userId']."  AND callcenter_id = ".$u_cid;
}
else if($usertype=='CCA')
{
	if($_SESSION["is_affliated"]==1)
	{
		$condition.=" and leads.callcenter_id = ".$_SESSION['userId'];
		$linked_ids_con.=" and leads.callcenter_id = ".$_SESSION['userId'];
	}
	else
	{
		$condition.=" and leads.id in (select lead_id from lead_comments where lead_comments.comment_utype='CCA') and leads.callcenter_id = ".$_SESSION['userId'];
		$linked_ids_con.=" and leads.id in (select lead_id from lead_comments where lead_comments.comment_utype='CCA') and leads.callcenter_id = ".$_SESSION['userId'];
	}	
}
else if($usertype=='C')
{
	$condition.=" and (assigned_client = ".$_SESSION['userId'].")";
	$linked_ids_con.=" and (assigned_client = ".$_SESSION['userId'].")";
}
else if($usertype=='MSW')
{
	$x_var = null;

	if(isset($_POST['ddlLeadStatus']) && count($_POST['ddlLeadStatus'])>0)
	{
		foreach($_POST['ddlLeadStatus'] as $ddlLeadStatus)
		{
			if(trim($ddlLeadStatus)!=null && $ddlLeadStatus=='DA')
			{
				$x_var.=" and leads.outcome_type is null and show_to_admin=1 ";
				break;
			}
		}
	}
	else
	{
		$x_var.=" or leads.outcome_type is null and show_to_admin=1 ";
	}
	
	if(isset($_POST['hfIsViewLeads']) && $_POST['hfIsViewLeads']==1)
	{
		$condition.=" and ((reached_admin = 1 and show_to_admin=1) ".$x_var.") ";
		$linked_ids_con.=" and ((reached_admin = 1 and show_to_admin=1) ".$x_var.") ";
		
		if(isset($_POST['hfIsViewLeadsSelected']) && $_POST['hfIsViewLeadsSelected']==1)
		{
			$condition.=" and (msw_lead_type = '".$_POST['txtPP']."')";
		}
	}
}

if(isset($_POST['hfIsViewLeads']) && $_POST['hfIsViewLeads'] == 1 &&  $_SESSION['userType']!=$MSW)
{
	if($usertype=='CCA' || $usertype=='CA' || $usertype=='CTL')
	{
		if($usertype=='CCA')
		{
			$condition.=" and (shifted_by_usertype!='S_CCA' or shifted_by_usertype is NULL)";  //SHIFTED CALLCENTER
		}
		else if($usertype=='CTL')
		{
			$condition.=" and ((shifted_by_usertype!='S_CTL' and shifted_by_usertype!='S_CCA') or shifted_by_usertype is NULL)"; //SHIFTED TEAM LEAD
		}
		else if($usertype=='CA')
		{
			$condition.=" and (shifted_by_usertype!='S_CA' or shifted_by_usertype is NULL)";  //SHIFTED AGENT
		}
	}

	if(($usertype=='CCA' && $_SESSION['is_affliated']==0) || $usertype=='CTL' || $usertype=='CA')
	{
		if($usertype=='CCA')
		{
			$condition.=" and (del_cc_id != ".$_SESSION['userId']." or del_cc_id is null)";
		}
		else if($usertype=='CTL')
		{
			$condition.=" and (del_tl_id != ".$_SESSION['userId']." or del_tl_id is null)";
		}
		else if($usertype=='CA')
		{
			$condition.=" and (del_ag_id != ".$_SESSION['userId']." or del_ag_id is null)";
		}
	}
}

if(isset($_POST['hfIsViewLeads']) && $_POST['hfIsViewLeads'] == 1 &&  $_SESSION['userType']!=$MSW)
{
	$condition.=" and ((client_status!='IP' && client_status!='AP' &&  client_status!='SI') or client_status is NULL) ";
}
else if(isset($_POST['hfIsViewLeads']) && $_POST['hfIsViewLeads'] == 0 &&  $_SESSION['userType']!=$MSW)
{
	$condition.=" and (client_status='IP' || client_status='SI' || client_status='AP') ";
}


if(isset($_POST['ddlWasRejected']) && trim($_POST['ddlWasRejected'])==1)
{
	$condition.=" and (leads.id in (select lead_id from lead_comments where lead_transfer_type = 'RJ')) ";
}
if(isset($_POST['ddlWasAssignedToVerifier']) && trim($_POST['ddlWasAssignedToVerifier'])!=null)
{
	$condition.=" and (leads.id in (select lead_id from lead_comments where comment_to = ".$_POST['ddlWasAssignedToVerifier'].")) ";
}

$queryResults = "select leads.*, leads.suburb_name as textbox_suburb,state_name,initials,
(case when (master_lead_id is not null and master_lead_id > 0) 
 THEN
      (select group_concat(m.id SEPARATOR ',') from leads m where (m.master_lead_id = leads.master_lead_id or m.id = leads.master_lead_id) and m.id!= leads.id ".$linked_ids_con." and m.is_deleted=0)
 ELSE
      (select group_concat(m.id SEPARATOR ',') from leads m where m.master_lead_id=leads.id and m.id!=leads.id ".$linked_ids_con." and  m.is_deleted=0)
 END)
 as linked_ids
 from leads left outer join states on states.id = leads.state_id where leads.is_deleted=0 ".$condition." order by ".$orderby." desc";

$totalResults = getQuery($queryResults);

$xx=1;
$writer = new XLSXWriter();
$writer->setAuthor('DevelopTech');

$arr = array();
$arr['Sr.No.'] = 'string';
$arr['Linked Ids'] = 'string';
$arr['Name'] = 'string';
$arr['Accident Type'] = 'string';
$arr['Date'] = 'string';
$arr['Phone'] = 'string';
$arr['State'] = 'string';

if($_SESSION['userType']=='CA' || $_SESSION['userType']=='CTL' )
{
	$arr['Email'] = 'string';
	$arr['Suburb'] = 'string';
	$arr['Address'] = 'string';
}
else
{
	$arr['SignUp'] = 'string';
}
if($_SESSION['userType']!=$C)
{
	$arr['Callback'] = 'string';
}
$arr['Updates'] = 'string';
if($_SESSION['userType']==$MSW)
{
	$arr['Callcenter'] = 'string';
	$arr['Sol'] = 'string';
	$arr['Verifier'] = 'string';
}

if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
{
	$arr['Team Lead'] = 'string';
	$arr['Agent'] = 'string';
}

if($_SESSION['userType']==$CTL)
{
	$arr['Agent'] = 'string';
}

if($_SESSION['userType']!=$C)
{
	$arr['Created On'] = 'string';
}
if($_SESSION['userType']==$MSW || $_SESSION['userType']==$CCA  || $_SESSION['userType']==$C)
{
	$arr['Modified On'] = 'string';
	$arr['Outcomes'] = 'string';
}
// $arr['Sol Notes'] = 'string';

$header = $arr;
$writer->writeSheetHeader('Sheet1', $header);

if(mysqli_num_rows($totalResults)!=0)
{
	$n = 1;
	while($rc = mysqli_fetch_object($totalResults))
	{
		$line_array = array();
		
		array_push($line_array,$n);
		
		$str='';
		$res = $rc->linked_ids;
		array_push($line_array,trim($res));
		
		array_push($line_array,$rc->first_name.' '.$rc->middle_name.' '.$rc->last_name);
		
		if($rc->last_accident_type!=null)
		{
			if($rc->last_accident_na!=1)
			{
				array_push($line_array,$rc->last_accident_type);
				array_push($line_array,date('d-M-Y',strtotime($rc->latest_accident_dt)));
			}
			else
			{
				array_push($line_array,$rc->last_accident_type);
				array_push($line_array,date('d-M-Y',strtotime($rc->latest_accident_dt)));
			}
		}
		else
		{
			array_push($line_array,'');
			array_push($line_array,'');
		}

		array_push($line_array,$rc->phone);
		array_push($line_array,$rc->initials);

		if($_SESSION['userType']=='CA' || $_SESSION['userType']=='CTL' )
		{
			array_push($line_array,$rc->email);
			array_push($line_array,$rc->drop_suburb.$rc->textbox_suburb);
			array_push($line_array,$rc->address);
		}
		else
		{
			$x='';
			if(trim($rc->latest_signuptype)!=null)
			{
				if($rc->latest_signuptype=='E' || $rc->latest_signuptype=='P')
				{
					$x= $rc->latest_signuptype=='E'?'Email':'Post';
					$x.=", Call: ".date("d M,Y",strtotime($rc->latest_signup_datetime))." \n(".$rc->latest_signup_timezone.") \n Comments: ".$rc->latest_signup_detail; 
				}
				else if($rc->latest_signuptype=='O' || $rc->latest_signuptype=='H')
				{
					$x = $rc->latest_signuptype=='O'?'Office Signup':'Home Signup';
					$x.=", Call: ".date("d M,Y H:i",strtotime($rc->latest_signup_datetime))." \n(".$rc->latest_signup_timezone.") \n Comments: ".$rc->latest_signup_detail; 
				}
			}
			array_push($line_array,$x);
		}
	
		if($_SESSION['userType']!=$C)
		{
			$x='';
			if(trim($rc->latest_callbacktype)!=null)
			{
				$x = $rc->latest_callbacktype=='N'?'No Answer':'Callback';
				$x.=", Call: ".date("d M,Y H:i",strtotime($rc->latest_callback_datetime))." \n(".$rc->latest_callback_timezone.") \n Comments: ".$rc->latest_calback;
			}
			array_push($line_array,$x);
		}
		
		/* if($_SESSION['userType']==$C || $_SESSION['userType']==$CCA || $_SESSION['userType']==$MSW)
		{
			array_push($line_array,$rc->latest_sol_notes);
		} */
		array_push($line_array,$rc->last_comment);
		if($_SESSION['userType']=='MSW')
		{
			array_push($line_array,isset($callcenter_array[$rc->callcenter_id])?$callcenter_array[$rc->callcenter_id]:'');
			array_push($line_array,isset($client_array[$rc->assigned_client])?$client_array[$rc->assigned_client]:'');
			array_push($line_array,isset($verifier_array[$rc->assigned_verifier])?$verifier_array[$rc->assigned_verifier]:'');
		}
		
		if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
		{
			array_push($line_array,isset($team_lead_array[$rc->team_lead_assigned])?$team_lead_array[$rc->team_lead_assigned]:'');
			array_push($line_array,isset($agents_array[$rc->agent_assigned])?$agents_array[$rc->agent_assigned]:'');
		}
		
		if($_SESSION['userType']==$CTL)
		{
			array_push($line_array,isset($agents_array[$rc->agent_assigned])?$agents_array[$rc->agent_assigned]:'');
		}
		
		if($_SESSION['userType']!=$C)
		{
			switch($_SESSION['userType'])
			{
				case 'C': $d = $rc->assign_client;break;
				case 'CCA': $d = $rc->assign_n_callcenter;
							if($_SESSION['is_affliated']==1)
							{
								$d = $rc->assign_affliated;
							}
							break;
				case 'CTL': $d = $rc->assign_teamlead;break;
				case 'CA': $d = $rc->assign_agent;break;
				case 'MSW': $d = $rc->assign_admin;break;
				case 'VA': $d = $rc->assign_verifier;break;
				default: $d='';
			}
			if($d!=null)	
			{
				array_push($line_array,date('d-M-Y',strtotime($rc->creation_date)));
			}
			else
			{
				array_push($line_array,"");
			}
			
		}
		if($_SESSION['userType']==$MSW || $_SESSION['userType']==$CCA  || $_SESSION['userType']==$C  || $_SESSION['userType']==$CTL)
		{
			array_push($line_array,get_timezone_offset(date('Y-m-d H:i:s',strtotime($rc->updation_date)),'UTC',$_SESSION['userTimeZone']));
			array_push($line_array,$rc->outcome);
		}

		// array_push($line_array,$rc->last_comment);
		
		$n++;
		
		$writer->writeSheetRow('Sheet1',$line_array);
	}
}

$basename = time().'.xlsx';
$file = 'excels/'.$basename;
$writer->writeToFile($file);

if(!file_exists($file))
{
	die("File not found");
}
else
{
	header("Content-Disposition: attachment; filename=".$basename);
	header("Content-Length: " . filesize($file));
	header("Content-Type: application/octet-stream;");
	readfile($file);
}
?>