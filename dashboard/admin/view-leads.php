<?php 
include('../includes/basic_auth.php');

$aa_var = 0;
set_time_limit(0); 

$getOutcomes = getOutcomes($_SESSION['userId']);

if(!isset($_GET['pp']) && $_SESSION['userType']==$MSW)
{
	header("location:view-leads.php?pp=f");
}
if($_SESSION['userType']==$MSW)
{
	$type = $_GET['pp'];
	writeNotificationZero($type);
}
/********************************************************************************************/
if($_SESSION['userType']==$CA || $_SESSION['userType']==$CTL)
{
	$datamaster = $mysqli->prepare("SELECT callcenter_id FROM tl_and_agents where uid = ?");
	$datamaster->bind_param('i',$_SESSION['userId']);
	$datamaster->execute();
	$datamaster->bind_result($u_cid);
	$datamaster->fetch();
	$datamaster->close();
}
else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
{
	$u_cid = $_SESSION['userId'];
}
/********************************************************************************************/

if(isset($_SESSION['txtCopyName']))
{
	unset($_SESSION['txtCopyName']);
}

if(isset($_SESSION['txtCopyNumber']))
{
	unset($_SESSION['txtCopyNumber']);
}

if(!isset($_SESSION['userType']) || !isset($_SESSION['userId']))
{
	session_destroy();
	header("location:".$mysiteurl.'logout.php');
}
else
{
	$usertype = $_SESSION["userType"];

	$getStates = getStates();
	$getClients = getClients();
	$getCallcenters = getCallcenters('');
	$getVerifiers = getVerifiers('');
	
	if(isset($_POST['btnSaveAndCloseComment']))
	{
		$comment_status = 'PN';
		
		if(isset($_POST['hfLeadIdComment']) && trim($_POST['hfLeadIdComment'])!=null && trim($_POST['ddlAssignTo'])!=null)
		{
			$callcenter_id = $_POST['ddlAssignTo'];
			if(isset($_POST["ddlGetTeamLeads"]) && $_POST["ddlGetTeamLeads"]!=null && $_POST['hfLeadCommentUType']=='CTL')
			{
				$_POST['ddlAssignTo'] = $_POST["ddlGetTeamLeads"];
			}

			$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
			$loginmaster->bind_param("i",$_POST['hfLeadIdComment']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$lead_transfer_type='LT';
			$client_status=null;
			
			$_SESSION['success']='Lead has been assigned successfully.';
			
			if($_SESSION['userType']=='C')
			{
				$lead_transfer_type='RJ';
				$client_status='RJ';
				$_SESSION['success']='Lead has been rejected successfully.';
				
				$loginmaster = $mysqli->prepare("update leads set assigned_client=null where id=?");
				$loginmaster->bind_param("i",$_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
				
			}
			
			$rbtnValues='';
			if($_POST['hfLeadCommentUType']=='CCA' && isset($_POST['rbtnValues'][0]))
			{
				$rbtnValues = $_POST['rbtnValues'][0];
				$rbtnValues = $rbtnValues=='RRC'?'Assign to CC &amp; Request Changes':'Reject and Assign';
			}
			
			$is_approved_verifier = null;
			if($_SESSION['userType']==$VA && $_POST['hfLeadCommentUType']==$MSW)
			{			
				$is_approved_verifier = $_POST['hfOutcome'];
			}
			
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type,reason_lead_from_msw_to_cc,is_approved_verifier) VALUES (?,?,?,?,?,?,?,?,?,?)");
			$loginmaster->bind_param("iisissssss",$_POST['hfLeadIdComment'],$_POST['ddlAssignTo'], $_POST['hfLeadCommentUType'],$_SESSION['userId'],$_POST['txtSaveComment'],$comment_status,$thisdate, $lead_transfer_type, $rbtnValues,$is_approved_verifier);
			$loginmaster->execute();
			$loginmaster->close();
			

			
			$is_sent_to_client = $_POST['hfLeadCommentUType']=='C'?1:0;
			
			if($is_sent_to_client==1)
			{
				$assigned_client = $_POST['ddlAssignTo'];
				
				$loginmaster = $mysqli->prepare("update leads set assigned_client=? where id=?");
				$loginmaster->bind_param("ii",$assigned_client, $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
				
				$html = getAssignedLeadEmail($_POST['hfLeadIdComment'],$_POST['txtSaveComment']);
				$subject = 'LegalAssist  - New Lead Assigned';
				sendClientEmails($_POST['hfLeadIdComment'], $subject, $html);	
			}
			
			$aff_val = 0;
			
			if($_POST['hfLeadCommentUType']=='CCA' || $_POST['hfLeadCommentUType']=='CTL' || $_POST['hfLeadCommentUType']=='CA')
			{
				if($_POST['hfLeadCommentUType']=='CCA')
				{
					$loginmaster = $mysqli->prepare("update leads set del_cc_id = NULL, del_by_cc = 0, del_by_cc_time = NULL  where id=?");
				}
				else if($_POST['hfLeadCommentUType']=='CTL')
				{
					$loginmaster = $mysqli->prepare("update leads set del_tl_id = NULL, del_by_tl = 0, del_by_tl_time = NULL  where id=?");
				}
				else if($_POST['hfLeadCommentUType']=='CA')
				{
					$loginmaster = $mysqli->prepare("update leads set del_ag_id = NULL, del_by_ag = 0, del_by_ag_time = NULL  where id=?");
				}
				$loginmaster->bind_param("i",$_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			
			if($_POST['hfLeadCommentUType']=='CTL')
			{			
				$loginmaster = $mysqli->prepare("update leads set team_lead_assigned=? where id=?");
				$loginmaster->bind_param("ii",$_POST['ddlAssignTo'], $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			else if($_POST['hfLeadCommentUType']=='VA')
			{			
				$loginmaster = $mysqli->prepare("update leads set assigned_verifier=?, is_previous_verifier = 0 where id=?");
				$loginmaster->bind_param("ii",$_POST['ddlAssignTo'], $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			else if($_POST['hfLeadCommentUType']=='CA')
			{
				// $loginmaster = $mysqli->prepare("update leads set assign_agent=? where id=?");
				$loginmaster = $mysqli->prepare("update leads set agent_assigned=? where id=?");
				$loginmaster->bind_param("ii",$_POST['ddlAssignTo'], $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			
			if(($_POST['hfLeadCommentUType']=='CTL' || $_POST['hfLeadCommentUType']=='CCA') && $_SESSION['userType']=='MSW')
			{
				$loginmaster = $mysqli->prepare("update leads set shifted_by_usertype = NULL, assigned_usertype=?, is_sent_to_client=?, updation_date=?, client_status=?, callcenter_id=?, msw_to_cc_reject_reason = ? where id=?");
				$loginmaster->bind_param("sissisi",$_POST['hfLeadCommentUType'],$is_sent_to_client, $thisdate,$client_status, $callcenter_id,$rbtnValues, $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			else
			{
				$loginmaster = $mysqli->prepare("update leads set shifted_by_usertype = NULL, assigned_usertype=?, is_sent_to_client=?, updation_date=?, client_status=?, msw_to_cc_reject_reason = ? where id=?");
				$loginmaster->bind_param("sisssi",$_POST['hfLeadCommentUType'],$is_sent_to_client, $thisdate,$client_status,$rbtnValues, $_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			
			if($_POST['hfLeadCommentUType']=='MSW')
			{
				$loginmaster = $mysqli->prepare("update leads set reached_admin=1 where id=?");
				$loginmaster->bind_param("i",$_POST['hfLeadIdComment']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			
			if($client_status!=null && $client_status=='RJ')
			{
				outcome_function($_POST['hfLeadIdComment'], $_SESSION['userId'],'RJ', $thisdate);
			}
			
			$c_outcome=null;
			
			if($_SESSION['userType']==$MSW && $_POST['hfLeadCommentUType']==$CCA)
			{
				removeAssignedDirectlyCheck($_POST['hfLeadIdComment']);
				setMSWLeadType($_POST['hfLeadIdComment'], 'c');
				
				$loginmaster = $mysqli->prepare("select is_affliated from callcenter where uid=?");
				$loginmaster->bind_param("i",$_POST['ddlAssignTo']);
				$loginmaster->execute();
				$loginmaster->bind_result($is_affliated_check);
				$loginmaster->fetch();
				$loginmaster->close();
				
				$aff_val = $is_affliated_check;
				
				$rbtnValues='';
				if($_POST['hfLeadCommentUType']=='CCA' && isset($_POST['rbtnValues'][0]))
				{
					$rbtnValues = $_POST['rbtnValues'][0];
					if($rbtnValues=='RRC')
					{
						$c_outcome = $is_affliated_check==1?'ATACC':'ATCCC';
					}
					else
					{
						$c_outcome = $is_affliated_check==1?'ATAC':'ATCC';
					}
				}
				else
				{
					$c_outcome = $is_affliated_check==1?'ATAC':'ATCC';
				}
			}
			else if($_SESSION['userType']==$MSW && $_POST['hfLeadCommentUType']==$C)
			{
				removeAssignedDirectlyCheck($_POST['hfLeadIdComment']);
				$c_outcome = 'ATC';
				setMSWLeadType($_POST['hfLeadIdComment'], 's');
			}
			else if($_SESSION['userType']==$CCA && $_POST['hfLeadCommentUType']==$MSW && $_SESSION['is_affliated']==1)
			{
				$c_outcome = 'ACTA';
				restoreDeletedLead($_POST['hfLeadIdComment'], $thisdate);
				setMSWLeadType($_POST['hfLeadIdComment'], 'f');
				
				$html = getAssignedLeadEmailAdmin($_POST['hfLeadIdComment'],$_POST['txtSaveComment']);
				$subject = 'LegalAssist  - Contact Us Query';
				sendMSWEmails($_POST['hfLeadIdComment'], $subject, $html);
			}
			else if($_SESSION['userType']==$CCA && $_POST['hfLeadCommentUType']==$CTL)
			{
				$c_outcome = 'CTTL';
			}
			else if($_SESSION['userType']==$CCA && $_POST['hfLeadCommentUType']==$MSW)
			{
				$c_outcome = 'CTA';
				restoreDeletedLead($_POST['hfLeadIdComment'], $thisdate);
				setMSWLeadType($_POST['hfLeadIdComment'], 'f');
				
				$html = getAssignedLeadEmailAdmin($_POST['hfLeadIdComment'],$_POST['txtSaveComment']);
				$subject = 'LegalAssist  - Contact Us Query';
				sendMSWEmails($_POST['hfLeadIdComment'], $subject, $html);	
				
			}
			else if($_SESSION['userType']==$CTL && $_POST['hfLeadCommentUType']==$CA)
			{
				$c_outcome = 'TLTA';
			}
			else if($_SESSION['userType']==$CTL && $_POST['hfLeadCommentUType']==$CCA)
			{
				$c_outcome = 'TTC';
			}
			else if($_SESSION['userType']==$CA && $_POST['hfLeadCommentUType']==$CTL)
			{
				$c_outcome = 'ATTL';
			}
			else if($_SESSION['userType']==$C && $client_status!=null && $client_status=='RJ')
			{
				$c_outcome = 'RJ';
				setMSWLeadType($_POST['hfLeadIdComment'], 'w');
			}
			else if($_SESSION['userType']==$MSW && $_POST['hfLeadCommentUType']==$VA)
			{
				$c_outcome = 'SV';
				removeAssignedDirectlyCheck($_POST['hfLeadIdComment']);
				setMSWLeadType($_POST['hfLeadIdComment'], 'v');
			}
			else if($_SESSION['userType']==$VA && $_POST['hfLeadCommentUType']==$MSW)
			{
				$c_outcome = $_POST['hfOutcome'];
				setMSWLeadType($_POST['hfLeadIdComment'], 'w');
			}
			if($c_outcome!=null)
			{
				outcome_function($_POST['hfLeadIdComment'], $_SESSION['userId'],$c_outcome, $thisdate);
			}
			
			if($_SESSION['userType']==$C || $_SESSION['userType']==$CCA)
			{
				//restoreDeletedLead($_POST['hfLeadIdComment'], $thisdate);
			}
			
			updateAssignDate($_POST['hfLeadCommentUType'], $_POST['hfLeadIdComment'],$aff_val);
			updateLastComment($_POST['hfLeadIdComment'],$_POST['txtSaveComment'],$_SESSION['userType'],$_POST['hfLeadCommentUType']);
			header("location:view-leads.php?pp=".$_GET['pp']);
			
		}
	}

	if(isset($_POST['btnFurtherInvestigation']))
	{
		$comment_status = 'PN';
		
		if(trim($_POST['hfLeadInvestigationId'])!=null && trim($_POST['txtFurtherSaveComment'])!=null)
		{
			$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
			$loginmaster->bind_param("i",$_POST['hfLeadInvestigationId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type) VALUES (?,?,'C',?,?,?,?,'FI')");
			$loginmaster->bind_param("iiisss",$_POST['hfLeadInvestigationId'],$_SESSION['userId'],$_SESSION['userId'],$_POST['txtFurtherSaveComment'],$comment_status,$thisdate);
			$loginmaster->execute();
			$loginmaster->close();
			
			updateLastComment($_POST['hfLeadInvestigationId'],$_POST['txtFurtherSaveComment'],$_SESSION['userType'],$_SESSION['userType']);
			
			$loginmaster = $mysqli->prepare("update leads set assigned_usertype='C', updation_date=?, client_status='FI', latest_sol_notes = ? where id=?");
			$loginmaster->bind_param("ssi", $thisdate,$_POST['txtFurtherSaveComment'],$_POST['hfLeadInvestigationId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$_SESSION['success']='Details have been saved successfully.';
			
			header("location:view-leads.php?pp=".$_GET['pp']);
			
		}
	}

	if(isset($_POST['btnLeadSignup']))
	{
		$comment_status = 'PN';
		
		if(trim($_POST['hfLeadSignupId'])!=null && trim($_POST['txtLeadSignUpComment'])!=null && trim($_POST['ddlSignUpState'])!=null)
		{
			
			if(!isset($_POST['txtLSignUpHours']) || trim($_POST['txtLSignUpHours'])==null)
			{
				$_POST['txtLSignUpHours']="00";
			}
			if(!isset($_POST['txtLSignUpMinutes']) || trim($_POST['txtLSignUpMinutes'])==null)
			{
				$_POST['txtLSignUpMinutes']="00";
			}
			
			$dt = $_POST['txtLSignUpYear']."-".$_POST['txtLSignUpMonth']."-".$_POST['txtLSignUpDay']." ".$_POST['txtLSignUpHours'].":".$_POST['txtLSignUpMinutes'].":00";
			$sel_d = date('Y-m-d H:i:s',strtotime($dt));
			
			//Change Datetime to UTC
			//$sel_d = changeTimeZone($sel_d,getAustraliaStateQuery($_POST['ddlSignUpState']),"UTC");
			
			$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
			$loginmaster->bind_param("i",$_POST['hfLeadSignupId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type, signup_dt,signup_type,client_state_zone) VALUES (?,?,'C',?,?,?,?,'SU',?,?,?)");
			$loginmaster->bind_param("iiisssssi",$_POST['hfLeadSignupId'],$_SESSION['userId'],$_SESSION['userId'],$_POST['txtLeadSignUpComment'],$comment_status,$thisdate,$sel_d,$_POST['rbtnLeadSignUpType'],$_POST['ddlSignUpState']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("update leads set assigned_usertype='C', updation_date=?, client_status='SU', latest_signup_detail = ?, latest_signuptype = ? , latest_signup_datetime = ? , latest_signup_timezone = (select initials from states y where y.id = ?) where id=?");
			$loginmaster->bind_param("sssssi", $thisdate,$_POST['txtLeadSignUpComment'],$_POST['rbtnLeadSignUpType'], $sel_d,$_POST['ddlSignUpState'], $_POST['hfLeadSignupId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			outcome_function($_POST['hfLeadSignupId'], $_SESSION['userId'],'SU', $thisdate);
			
			$_SESSION['success']='Details have been saved successfully.';
			header("location:view-leads.php?pp=".$_GET['pp']);
			
		}
	}
	
	if(isset($_POST['btnLeadCallBack']))
	{
		$comment_status = 'PN';
		
		if(trim($_POST['hfLeadCallBackId'])!=null && trim($_POST['ddlCallBackState'])!=null)
		{
			$loginmaster = $mysqli->prepare("update lead_callbacks set status='EX' where lead_id = ?");
			$loginmaster->bind_param("i",$_POST['hfLeadCallBackId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$dt = $_POST['txtCCallBackYear']."-".$_POST['txtCCallBackMonth']."-".$_POST['txtCCallBackDay']." ".$_POST['txtCCallBackHours'].":".$_POST['txtCCallBackMinutes'].":00";
			$sel_d = date('Y-m-d H:i:s',strtotime($dt));
			
			$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
			$loginmaster->bind_param("i",$_POST['hfLeadCallBackId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			if($_SESSION['userType']=='MSW')
			{
				$lead_transfer_type = 'CBA';
			}
			else if($_SESSION['userType']=='VA')
			{
				$lead_transfer_type = 'CBV';
			}
			else if($_SESSION['userType']=='CCA')
			{
				$lead_transfer_type = 'CBC';
			}
			else if($_SESSION['userType']=='CA')
			{
				$lead_transfer_type = 'CBAG';
			}
			else if($_SESSION['userType']=='CTL')
			{
				$lead_transfer_type = 'CBTL';
			}
			
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type, callback_dt,callback_type,client_state_zone_callback) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
			$loginmaster->bind_param("iisissssssi",$_POST['hfLeadCallBackId'],$_SESSION['userId'],$_SESSION['userType'], $_SESSION['userId'],$_POST['txtLeadCallBackComment'],$comment_status,$thisdate,$lead_transfer_type, $sel_d,$_POST['rbtnLeadCallbackType'],$_POST['ddlCallBackState']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("update leads set updation_date=?, latest_calback = ?, latest_callbacktype = ? , latest_callback_datetime = ? , latest_callback_timezone = (select initials from states y where y.id = ?), outcome_type=?, outcome = (select outcome_title from outcomes where outcome_initials = ?) where id=?");
			$loginmaster->bind_param("sssssssi", $thisdate,$_POST['txtLeadCallBackComment'],$_POST['rbtnLeadCallbackType'], $sel_d,$_POST['ddlCallBackState'],$lead_transfer_type,$lead_transfer_type, $_POST['hfLeadCallBackId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("INSERT INTO lead_callbacks(lead_id, callback_datetime, callback_details, call_back_type, status, timezone_target, added_by) VALUES (?,?,?,?,'AC',?,?)");
			$loginmaster->bind_param("issssi",$_POST['hfLeadCallBackId'],$sel_d,$_POST['txtLeadCallBackComment'], $_POST['rbtnLeadCallbackType'],$_POST['ddlCallBackState'],$_SESSION['userId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$_SESSION['success']='Details have been saved successfully.';
			header("location:view-leads.php?pp=".$_GET['pp']);
			
		}
	}

	if(isset($_POST['btnApproveComment']))
	{
		$comment_status = 'PN';
		
		if(trim($_POST['hfLeadApproveId'])!=null && trim($_POST['txtApproveComment'])!=null)
		{
			$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
			$loginmaster->bind_param("i",$_POST['hfLeadApproveId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type) VALUES (?,(select uid from login_master where type='MSW'),'MSW',?,?,?,?,'AP')");
			$loginmaster->bind_param("iisss",$_POST['hfLeadApproveId'],$_SESSION['userId'],$_POST['txtApproveComment'],$comment_status,$thisdate);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("update leads set reached_admin = 1, assigned_usertype='MSW', updation_date=?, client_status='AP', is_verified=1 where id=?");
			$loginmaster->bind_param("si", $thisdate, $_POST['hfLeadApproveId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			updateLastComment($_POST['hfLeadApproveId'],$_POST['txtApproveComment'],$_SESSION['userType'],$MSW);
			
			outcome_function($_POST['hfLeadApproveId'], $_SESSION['userId'],'AP', $thisdate);
			setMSWLeadType($_POST['hfLeadApproveId'], 'a');
			
			$_SESSION['success']='Lead has been approved successfully.';			
			header("location:view-leads.php?pp=".$_GET['pp']);
			
		}
	}

	if(isset($_POST['btnInvoiceComment']))
	{
		$comment_status = 'PN';
		
		if(trim($_POST['hfLeadInvoiceId'])!=null && trim($_POST['txtInvoiceComment'])!=null)
		{
			$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
			$loginmaster->bind_param("i",$_POST['hfLeadInvoiceId']);
			$loginmaster->execute();
			$loginmaster->close();
						
			$loginmaster = $mysqli->prepare("update leads set invoice_number = ?, invoice_remarks = ?, assigned_usertype='C', updation_date=?, is_verified=1 where id=?");
			$loginmaster->bind_param("sssi",$_POST['txtInvoiceNumber'],$_POST['txtInvoiceComment'],$thisdate, $_POST['hfLeadInvoiceId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			setMSWLeadType($_POST['hfLeadInvoiceId'], 'i');
			$_SESSION['success']='Lead has been finished successfully.';
			// header("location:view-leads.php?pp=".$_GET['pp']);
		}
	}

	if(isset($_POST['btnInvoicePaidComment']))
	{
		$comment_status = 'DN';
		
		if(trim($_POST['hfLeadInvoicePaidId'])!=null && trim($_POST['txtInvoicePaidComment'])!=null)
		{
			$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
			$loginmaster->bind_param("i",$_POST['hfLeadInvoicePaidId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type) VALUES (?,(select uid from login_master where type='MSW'),'MSW',?,?,?,?,'IP')");
			$loginmaster->bind_param("iisss",$_POST['hfLeadInvoicePaidId'],$_SESSION['userId'],$_POST['txtInvoicePaidComment'],$comment_status,$thisdate);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("update leads set reached_admin = 1, assigned_usertype='MSW', updation_date=?, client_status='IP', is_verified=1 where id=?");
			$loginmaster->bind_param("si", $thisdate, $_POST['hfLeadInvoicePaidId']);
			$loginmaster->execute();
			$loginmaster->close();
			
			updateLastComment($_POST['hfLeadInvoicePaidId'],$_POST['txtInvoicePaidComment'],$_SESSION['userType'],$_SESSION['userType']);
			
			outcome_function($_POST['hfLeadInvoicePaidId'], $_SESSION['userId'],'IP', $thisdate);
			//restoreDeletedLead($_POST['hfLeadInvoicePaidId'], $thisdate);
			
			$_SESSION['success']='Invoice Paid Successfully.';
			
			// header("refresh:2;url=view-leads.php");
			header("location:view-leads.php?pp=".$_GET['pp']);
			
		}
	}
	
	if(isset($_POST['btnCallPopUpClientPopUp']))
	{
		if(trim($_POST['rbtnClientPopup'])!=null && trim($_POST['txtCallPopUpClientPopUp'])!=null && trim($_POST['txtCallPopUpClientPopUp'])!=null)
		{
			$comment_status = 'DN';
			
			// $loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type) VALUES (?,(select uid from login_master where type='MSW'),'MSW',?,?,?,?,?)");
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type) VALUES (?,?,'C',?,?,?,?,?)");
			$loginmaster->bind_param("iiissss",$_POST['hfLeadPopUpLeadClientId'],$_SESSION['userId'],$_SESSION['userId'],$_POST['txtCallPopUpClientPopUp'],$comment_status,$thisdate,$_POST['rbtnClientPopup']);
			$loginmaster->execute();
			$loginmaster->close();
			
			if($_POST['rbtnClientPopup']=='SC')
			{
				$loginmaster = $mysqli->prepare("update leads set latest_spoken_to_client = 1 where id = ?");
				$loginmaster->bind_param("i",$_POST['hfLeadPopUpLeadClientId']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			
			updateLastComment($_POST['hfLeadPopUpLeadClientId'],$_POST['txtCallPopUpClientPopUp'],$_SESSION['userType'],$_SESSION['userType']);
			
			outcome_function($_POST['hfLeadPopUpLeadClientId'], $_SESSION['userId'], $_POST['rbtnClientPopup'], $thisdate);
			//restoreDeletedLead($_POST['hfLeadPopUpLeadClientId'], $thisdate);
			
			$_SESSION['success']='Client Status Updated.';
			
			// header("refresh:2;url=view-leads.php");
			header("location:view-leads.php?pp=".$_GET['pp']);
			
		}
	}

	if(isset($_POST['btnCopy']))
	{
		if(trim($_POST['txtCopyName'])!=null && trim($_POST['txtCopyNumber'])!=null && trim($_POST['txtCopyNumber'])!=null)
		{
			$_SESSION['txtCopyName'] = $_POST['txtCopyName'];
			$_SESSION['txtCopyNumber'] = $_POST['txtCopyNumber'];
			$_SESSION['txtCopyLastEmail'] = $_POST['txtCopyLastEmail'];
			$_SESSION['txtCopyLastName'] = $_POST['txtCopyLastName'];
			$_SESSION['txtCopyDay'] = $_POST['txtCopyDay'];
			$_SESSION['txtCopyMonth'] = $_POST['txtCopyMonth'];
			$_SESSION['txtCopyYear'] = $_POST['txtCopyYear'];
			header("location:recreate_lead.php?u=".$_POST['txtCopyLeadId']);
		}
	}
	
	if(isset($_POST['btnCommentCallcenter']))
	{
		if(trim($_POST['txtSaveCommentCallcenter'])!=null && trim($_POST['hfLeadIdCommentCallcenter'])!=null)
		{
			$lead_transfer_type='RJ';
			$client_status=null;
			$comment_status_1 = 'DN';
			$comment_status_2 = 'PN';
			
			//Set all previous status as Done DN
			$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
			$loginmaster->bind_param("i",$_POST['hfLeadIdCommentCallcenter']);
			$loginmaster->execute();
			$loginmaster->close();
			
			//Output comments that lead is rejected by admin after approval
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type,reject_after_sol_assign) VALUES (?,?,'MSW',(select assigned_client from leads where id = ?),?,?,?,?,1)");
			$loginmaster->bind_param("iiissss",$_POST['hfLeadIdCommentCallcenter'],$_SESSION['userId'],$_POST['hfLeadIdCommentCallcenter'],$_POST['txtSaveCommentCallcenter'],$comment_status_1,$thisdate, $lead_transfer_type);
			$loginmaster->execute();
			$loginmaster->close();
			
			//Set client as null
			$loginmaster = $mysqli->prepare("update leads set assigned_client = NULL,updation_date = ?,client_status='RJ',assigned_usertype = 'MSW' where id=?");
			$loginmaster->bind_param("si",$thisdate,$_POST['hfLeadIdCommentCallcenter']);
			$loginmaster->execute();
			$loginmaster->close();
			
			//Add the outcome in DB
			outcome_function($_POST['hfLeadIdCommentCallcenter'], $_SESSION['userId'], 'RJ', $thisdate);
			updateAssignDate($MSW, $_POST['hfLeadIdCommentCallcenter'],0);
			updateLastComment($_POST['hfLeadIdCommentCallcenter'],$_POST['txtSaveCommentCallcenter'],$_SESSION['userType'],$MSW);
			
			header("location:view-leads.php?pp=".$_GET['pp']);
		}
	}
	
	
if(isset($_POST["btnSaveDocument"]))
{
	$loginmaster = $mysqli->prepare("select id from lead_accidents where lead_id = ? order by accident_date_time desc limit 1");
	$loginmaster->bind_param("i",$_POST['hfDocumentLeadId']);
	$loginmaster->execute();
	$loginmaster->bind_result($aid);
	$loginmaster->fetch();
	$loginmaster->close();
	
	$uid = $_POST['hfDocumentLeadId'];
	$view_documents = getQuery('select * from lead_documents where lead_accident_id = '.$aid.' and lead_id='.$_POST['hfDocumentLeadId']);
	$document_flag = mysqli_num_rows($view_documents);
	
	$fupDoc1 = $fupDoc2 = $fupDoc3 = $fupDoc4 = $fupDoc5 = $fupDoc6 = $fupDoc7 = null;
	
	if($document_flag==1)
	{
		$drow = mysqli_fetch_object($view_documents);
		$fupDoc1 = $drow->doc1;
		$fupDoc2 = $drow->doc2;
		$fupDoc3 = $drow->doc3;
		$fupDoc4 = $drow->doc4;
		$fupDoc5 = $drow->doc5;
		$fupDoc6 = $drow->doc6;
		$fupDoc7 = $drow->doc7;
	}
	
	if(isset($_FILES['fupDoc1']['name']) && trim($_FILES['fupDoc1']['name'])!=null)
	{
		$fupDoc1 = file_upload($_FILES['fupDoc1']['name'], $_FILES['fupDoc1']['tmp_name'],'lead_docs');
	}
	
	if(isset($_FILES['fupDoc2']['name']) && trim($_FILES['fupDoc2']['name'])!=null)
	{
		$fupDoc2 = file_upload($_FILES['fupDoc2']['name'], $_FILES['fupDoc2']['tmp_name'],'lead_docs');
	}
	
	if(isset($_FILES['fupDoc3']['name']) && trim($_FILES['fupDoc3']['name'])!=null)
	{
		$fupDoc3 = file_upload($_FILES['fupDoc3']['name'], $_FILES['fupDoc3']['tmp_name'],'lead_docs');
	}
	
	if(isset($_FILES['fupDoc4']['name']) && trim($_FILES['fupDoc4']['name'])!=null)
	{
		$fupDoc4 = file_upload($_FILES['fupDoc4']['name'], $_FILES['fupDoc4']['tmp_name'],'lead_docs');
	}
	
	if(isset($_FILES['fupDoc5']['name']) && trim($_FILES['fupDoc5']['name'])!=null)
	{
		$fupDoc5 = file_upload($_FILES['fupDoc5']['name'], $_FILES['fupDoc5']['tmp_name'],'lead_docs');
	}
	
	if(isset($_FILES['fupDoc6']['name']) && trim($_FILES['fupDoc6']['name'])!=null)
	{
		$fupDoc6 = file_upload($_FILES['fupDoc6']['name'], $_FILES['fupDoc6']['tmp_name'],'lead_docs');
	}
	
	if(isset($_FILES['fupDoc7']['name']) && trim($_FILES['fupDoc7']['name'])!=null)
	{
		$fupDoc7 = file_upload($_FILES['fupDoc7']['name'], $_FILES['fupDoc7']['tmp_name'],'lead_docs');
	}
	
	if($document_flag==0)
	{
		$loginmaster = $mysqli->prepare("insert into lead_documents (doc1, doc2, doc3, doc4, lead_id, lead_accident_id,doc5, doc6, doc7,desc1,desc2,desc3,desc4,desc5,desc6,desc7) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		$loginmaster->bind_param("ssssiissssssssss", $fupDoc1, $fupDoc2, $fupDoc3,$fupDoc4, $uid, $aid,$fupDoc5, $fupDoc6, $fupDoc7,$_POST['fupDoc1Desc'],$_POST['fupDoc2Desc'],$_POST['fupDoc3Desc'],$_POST['fupDoc4Desc'],$_POST['fupDoc5Desc'],$_POST['fupDoc6Desc'],$_POST['fupDoc7Desc']);
	}
	else
	{
		$loginmaster = $mysqli->prepare("update lead_documents set doc1=?, doc2=?, doc3=?, doc4=?, doc5=?, doc6=?, doc7=?,desc1=?,desc2=?,desc3=?,desc4=?,desc5=?,desc6=?,desc7=?  where lead_accident_id=?");
		$loginmaster->bind_param("ssssssssssssssi", $fupDoc1, $fupDoc2, $fupDoc3,$fupDoc4, $fupDoc5, $fupDoc6, $fupDoc7,$_POST['fupDoc1Desc'],$_POST['fupDoc2Desc'],$_POST['fupDoc3Desc'],$_POST['fupDoc4Desc'],$_POST['fupDoc5Desc'],$_POST['fupDoc6Desc'],$_POST['fupDoc7Desc'], $aid);
	}
	$loginmaster->execute();
	$loginmaster->close();
	
	header("location:view-leads.php?pp=".$_GET['pp']);
}

?>

<!DOCTYPE html>
	<html lang="en">
		<head>
			<?php include('../includes/header.php'); ?>
			<style>
				.table-responsive.viewLead table tbody td input[type=button] 
				{
					display: inline-block!important;
				}
				.table-responsive.viewLead table tbody td p 
				{
					max-width: 360px;
				}
				.table-responsive.viewLead table tbody td button.ti-clipboard 
				{
					font-size: 17px;
					position: inherit;
					top: 0;
					transform: translateY(22%);
					right: 0;
					color: #F44336;
					cursor: pointer;
					background: transparent;
					box-shadow: none;
					border: 0;
					float: right;
				}
				.a_red
				{
					color:red;
				}
				
				.table-responsive.viewLead table {
					width: 100%;
				}
				
				.table-responsive.viewLead table td .icnos {
					display: flex;
					height: 100%;
					border: 0;
					justify-content: left;
				}
			</style>
		</head>
		<body class="fixed-nav sticky-footer" id="page-top">
			<?php include('../includes/navigation.php'); ?>
		  
			<div class="content-wrapper">

				<div class="container-fluid">
				
					<!-- Title & Breadcrumbs-->
					<div class="row page-titles <?php echo isset($_POST["btnAdd"])?'':'collapsed' ?>"" data-toggle="collapse" data-target=".viewLeadAcc" style="margin-bottom:0;">
						<div class="col-md-12 align-self-center">
							<h4 class="theme-cl"><?php echo $_SESSION['userType']==$MSW && isset($_GET['pp'])?msw_leads($_GET['pp']):'View Leads' ?> <i class="ti-arrow-circle-down pull-right"></i></h4>
						</div>
					</div>
					<!-- Title & Breadcrumbs-->
					
					<!-- row -->
				
					<!-- row -->
					
					<div class="collapse viewLeadAcc <?php echo isset($_POST["btnAdd"])?'show':'' ?>" style="margin-bottom:0;">
						<div class="card-body">
							<form target="_blank" action='reportsdownload.php' name='frmSearch' id='frmSearch' method='post'>
								<?php include("../includes/messages.php") ?>
								<div class="row">
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_GET['pp'])) echo $_GET['pp'] ?>" class="form-control" id="txtPP" name="txtPP" type="hidden" />
											<input value="<?php if(isset($_POST["txtSearch"])) echo $_POST["txtSearch"] ?>" class="form-control" placeholder='Name / Mobile / Email' id="txtSearch" name="txtSearch" placeholder="" type="text" />
										</div>
									</div>
									
									<?php
									if($_SESSION['userType']!=$MSW)
									{
										?>
											<input class="form-control" value="" name="ddlVerifier" id="ddlVerifier" type="hidden" />
										<?php
									}										
									if($_SESSION['userType']==$MSW)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
											  	<a href="#!" id="openSelect1">Select Sol</a>
												<select name="ddlClients[]" id="ddlClients" class="d-flex form-control" multiple>
													<option value=''>Select Sol</option>
													<?php
														$client_array = array();
														while($clientrow = mysqli_fetch_object($getClients))
														{
															?>	
																<option <?php if(isset($_POST['ddlClients']) && count($_POST['ddlClients'])>0){echo in_array($clientrow->uid,$_POST['ddlClients'])?'selected':'';} ?> value='<?php echo $clientrow->uid ?>'><?php echo $clientrow->company_name ?></option>
															<?php
															$client_array[$clientrow->uid] = $clientrow->company_name;
														}
													?>
												</select>
											  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
												<select name="ddlLCallcenters" id="ddlLCallcenters" class="d-flex form-control">
													<option value=''>Select Callcenter</option>
													<?php
														$callcenter_array = array();
														while($ccrow = mysqli_fetch_object($getCallcenters))
														{
															?>	
																<option <?php if(isset($_POST['ddlLCallcenters']) && trim($_POST['ddlLCallcenters'])!=null){echo $_POST['ddlLCallcenters']==$ccrow->uid?'selected':'';} ?> value='<?php echo $ccrow->uid ?>'><?php echo $ccrow->centre_name ?></option>
															<?php
															$callcenter_array[$ccrow->uid] = $ccrow->centre_name;
														}
													?>
												</select>
											  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
												<select name="ddlVerifier" id="ddlVerifier" class="d-flex form-control">
													<option value=''>Select Verifier</option>
													<?php
														$verifier_array = array();
														while($vcrow = mysqli_fetch_object($getVerifiers))
														{
															?>	
																<option <?php if(isset($_POST['ddlVerifier']) && trim($_POST['ddlVerifier'])!=null){echo $_POST['ddlVerifier']==$vcrow->uid?'selected':'';} ?> value='<?php echo $vcrow->uid ?>'><?php echo $vcrow->verifier_name ?></option>
															<?php
															$verifier_array[$vcrow->uid] = $vcrow->verifier_name;
														}
													?>
												</select>
											  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
											  	<a href="#!" id="openSelect2">Select Outcome</a>
												<?php include("outcomes_select.php"); ?>
											  </div>
											</div>
										<?php
									}
									else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==1)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
													<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
														<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
											  	<a href="#!" id="openSelect1">Select Sol</a>
												<select name="ddlClients[]" id="ddlClients" class="d-flex form-control">
													<option value=''>Select Sol</option>
													<?php
														$client_array = array();
														while($clientrow = mysqli_fetch_object($getClients))
														{
															?>	
																<option <?php if(isset($_POST['ddlClients']) && count($_POST['ddlClients'])>0){echo in_array($clientrow->uid,$_POST['ddlClients'])?'selected':'';} ?> value='<?php echo $clientrow->uid ?>'><?php echo $clientrow->company_name ?></option>
															<?php
															$client_array[$clientrow->uid] = $clientrow->company_name;
														}
													?>
												</select>
											  </div>
											</div>
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else if($_SESSION['userType']==$C)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
													<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else if($_SESSION['userType']==$CTL)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												  <div class="form-group">
												  	<a href="#!" id="openSelect2">Select Outcome</a>
														<?php include("outcomes_select.php"); ?>
												  </div>
											</div>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
										<?php
									}
									else
									{
										?>
											<input class="form-control" value="" name="ddlClients[]" id="ddlClients" type="hidden" />
											<input class="form-control" value="" name="ddlLCallcenters" id="ddlLCallcenters" type="hidden" />
											<input class="form-control" value="" name="ddlLeadStatus[]" id="ddlLeadStatus" type="hidden" />
										<?php
									}
									?>	
									<div class="col-md-3 col-sm-12 col-xs-12">
									  <div class="form-group">
										<select name="ddlState" id="ddlState" class="d-flex form-control">
											<option value=''>Select State</option>
											<?php
												while($staterow = mysqli_fetch_object($getStates))
												{
													?>	
														<option <?php if(isset($_POST['ddlState']) && trim($_POST['ddlState'])!=null){echo $_POST['ddlState']==$staterow->id?'selected':'';} ?> value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
													<?php
												}
											?>
										</select>
									  </div>
									</div>
									
									<?php
									if(($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0) || $_SESSION['userType']==$CTL)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												<div class="form-group">
													<select name="ddlAgentCheck" id="ddlAgentCheck" class="d-flex form-control">
														<option value=''>Select Agent</option>
														<?php
														$getAgents = getUsersofCallcenter($u_cid,$CA);
														$agents_array = array();
														while($arow = mysqli_fetch_object($getAgents))
														{
															?>	
																<option <?php if(isset($_POST['ddlAgentCheck'])){echo $_POST['ddlAgentCheck']==$arow->uid?'selected':'';} ?> value='<?php echo $arow->uid ?>'><?php echo $arow->first_name ?></option>
															<?php
															$agents_array[$arow->uid] = $arow->first_name.' '.$arow->last_name;
														}
														?>
													</select>
												</div>
											</div>
											<?php
											if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
											{
												?>
													<div class="col-md-3 col-sm-12 col-xs-12">
														<div class="form-group">
															<select name="ddlTeamLeadCheck" id="ddlTeamLeadCheck" class="d-flex form-control">
																<option value=''>Select Team Lead</option>
																<?php
																$getTeamLeads = getUsersofCallcenter($u_cid,$CTL);
																$team_lead_array = array();
																while($tlrow = mysqli_fetch_object($getTeamLeads))
																{
																	?>	
																		<option <?php if(isset($_POST['ddlTeamLeadCheck'])){echo $_POST['ddlTeamLeadCheck']==$tlrow->uid?'selected':'';} ?> value='<?php echo $tlrow->uid ?>'><?php echo $tlrow->first_name ?></option>
																	<?php
																	$team_lead_array[$tlrow->uid] = $tlrow->first_name.' '.$tlrow->last_name;
																}
																?>
															</select>
														</div>
													</div>
												<?php
											}
									}
									?>
									
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtAccidentDateSearchFrom"])) echo $_POST["txtAccidentDateSearchFrom"] ?>" class="form-control" placeholder='Accident Date From' id="txtAccidentDateSearchFrom" name="txtAccidentDateSearchFrom" type="text" />
										</div>
									</div>
									
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtAccidentDateSearchTo"])) echo $_POST["txtAccidentDateSearchTo"] ?>" class="form-control" placeholder='Accident Date To' id="txtAccidentDateSearchTo" name="txtAccidentDateSearchTo" type="text" />
										</div>
									</div>
									
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtFromDate"])) echo $_POST["txtFromDate"] ?>" class="form-control" placeholder='From' id="txtFromDate" name="txtFromDate" type="text" />
										</div>
									</div>
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtToDate"])) echo $_POST["txtToDate"] ?>" class="form-control" placeholder='To' id="txtToDate" name="txtToDate" type="text" />
											<input value="ajaxgetLeads.php" class="form-control" id="hfAjaxPage" name="hfAjaxPage" placeholder="" type="hidden" />
										</div>
									</div>
									<?php
									if($_SESSION['userType']==$MSW || $_SESSION['userType']==$VA)
									{
										?>
											<div class="col-md-3 col-sm-8 col-xs-12">
												<div class="form-group">
													<?php
													for($ms = 1;$ms<=3;$ms++)
													{
														?>
															<label class="mr-2" for="chckCheckList<?php echo $ms ?>"><input class="checkList_checkbox" type="checkbox" value="<?php echo $ms ?>" name="chckCheckList[]" id="chckCheckList<?php echo $ms ?>" />&nbsp; 
																<?php
																if($ms==1)
																{
																	echo "Claim Information Gathered, Verified and Updated";
																}
																else if($ms==2)
																{
																	echo "CTP Medical Certificate Received";
																}
																else if($ms==3)
																{
																	echo "Police Event Number Received";
																}
																?>
															</label>
														<?php
													}
													?>
												</div>
											</div>
										<?php
									}
									?>
									<input value="updation_date" class="form-control" name="ddlSortBy" id="ddlSortBy"  type="hidden" />
									
									<div class="col-md-12 col-sm-12 col-xs-12">
										<input type="button" onclick="getPagination(1)" class="btn btn-primary" value="Search" name="btnAdd" id="btnAdd" />
										<input type="button" class="btn btn-warning" value="Reset" name="btnReset" id="btnReset" onclick="window.location='view-leads.php?pp=<?php echo $_GET['pp'] ?>'" />
										<?php
										if($_SESSION['userType']==$MSW || $_SESSION['userType']==$CTL || ($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0))
										{
											?>
												<input type="button" class="btn btn-danger" value="Delete Leads" name="btnDeleteMultiple" id="btnDeleteMultiple" onclick="deleteMultipleLeads()" disabled />
											<?php
										}
										if($_SESSION['userType']==$MSW)
										{
											?>
												<input data-toggle="modal" data-target="#assignNewVerifierModal" type="button" class="btn btn-danger" value="Assign To Verifier" name="btnAssignVerifier" id="btnAssignVerifier" disabled />
											<?php
										}
										if($_SESSION['userType']==$C  || $_SESSION['userType']==$MSW || ($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==1))
										{
											?>
												<input type="hidden" name="hfIsViewLeads" id="hfIsViewLeads" value='1' />
												<input type="submit" class="btn btn-success" value="Export All to Excel" onclick="resetIsViewLeadsSelected()" name="" id=""  />
											<?php
										}
										if($_SESSION['userType']==$MSW)
										{
											?>
												<input type="hidden" name="hfIsViewLeadsSelected" id="hfIsViewLeadsSelected" value='0' />
												<input type="submit" class="btn btn-success" onclick="setIsViewLeadsSelected()" value="Export to Excel" name="" id=""  />
											<?php
										}
										?>
									</div>
								</div>
							</form>
						</div>
					</div>
					
					<div class="row">
					
						
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="change-password">
							<div class="card">
							
								
								<div class="card-body">

									<!-- Table Start -->

									<div class="table-responsive viewLead ajaxClass">
										<?php
										if(isset($totalResults) && mysqli_num_rows($totalResults)!=0)
										{
											?>
											<div class="hh5">
											
											</div>
											<?php
											include('../includes/ajax_before_pagination.php');
										}
										/* else
										{
											include('../includes/norows.php'); 
										} */
										?>
									</div>
									<!-- Table End -->

								</div>
							</div>
						</div>
					
						</div>
					</div>
					<!-- /.row -->
				</div>

			</div>
				<!-- /.content-wrapper -->
				<?php include('saveCommentmodal.php'); ?>
				<?php include('callPopUpClientPopUp.php'); ?>
				<?php include('furtherInvestigationModal.php'); ?>
				<?php include('signupModal.php'); ?>
				<?php include('callbackModal.php'); ?>
				<?php include('approvemodal.php'); ?>
				<?php include('invoiceModal.php'); ?>
				<?php include('invoicePaidModal.php'); ?>
				<?php include('copymodal.php'); ?>
				<?php include('copymodalShow.php'); ?>
				<?php include('rejectApprovalModal.php'); ?>
				<?php include('rejectApprovalInvoiceModal.php'); ?>
				<?php include('attachmentsModal.php'); ?>
				<?php include('checkListModal.php'); ?>
				<?php include('logsmodal.php'); ?>
				<?php include('email_templates.php'); ?>
				<?php include('phone_templates.php'); ?>
				<?php include('assignMultipleVerifiers.php'); ?>
				<?php include('saveclosemodaNotesLeads.php');  ?>
				<?php include('../includes/copyright.php'); ?>
			<!-- Scroll to Top Button-->
			<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
				<i class="ti-angle-double-up"></i>
			</a>
			<?php include('../includes/web_footer.php'); ?>
			<script src="<?php echo $mysiteurl ?>js/jquery.table2excel.js"></script>
			
			<script type="text/javascript">
				$(document).ready(function() 
				{
					var delay = 1000;
						setTimeout(function() {
						 getPagination(1);
						}, delay);
				});
			</script>
		  
		</body>
	</html>
	<?php
}
