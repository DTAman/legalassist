<?php
include('../includes/basic_auth.php');

if(isset($_POST["hfLeadId"]) && isset($_POST["ddl7Employed"]) && isset($_POST["txt7Occupation"]) && isset($_POST["txt7WeeklySalary"]) && isset($_POST["txt7WorkingHours"]) && isset($_POST["txt7WorkingIncomeLost"]) && isset($_POST["ddl7HaveInjuries"]) && isset($_POST["txt7ReceivingAssistance"]) && isset($_POST["txt7RequireReceivingAssistance"]) && isset($_POST["ddl7IncomeState"]) && isset($_POST["txt7OtherIncomes"]) && trim($_POST["hfLeadId"])>0 && isset($_POST['hfLeadAccidentId']) && trim($_POST['hfLeadAccidentId'])!=0)
{
	$lead_id = $_POST['hfLeadId'];
	$hfLeadAccidentId = $_POST['hfLeadAccidentId'];

	if($_POST['ddl7Employed']!=2 || trim($_POST['txt7ReceivingAssistance'])!=null || trim($_POST['txt7RequireReceivingAssistance'])!=null)
	{
		$_POST['txt7OtherIncomes'] = strtolower($_POST['ddl7IncomeState'])!='other'?null:$_POST['txt7OtherIncomes'];
		
		$_POST['ddl7HaveInjuries'] = trim($_POST['ddl7HaveInjuries'])!= NULL ?$_POST['ddl7HaveInjuries']:NULL;
		
		if($_POST['ddl7Employed']==2)
		{
			$_POST['ddl7Employed'] = NULL;
			$_POST['txt7Occupation'] = NULL;
			$_POST['txt7WeeklySalary'] = NULL;
			$_POST['txt7WorkingHours'] = NULL;
			$_POST['txt7WorkingIncomeLost'] = NULL;
			$_POST['ddl7HaveInjuries'] = NULL;
			$_POST['ddl7IncomeState']= NULL;
			$_POST['txt7OtherIncomes']= NULL;
		}
		else if($_POST['ddl7Employed']==1)
		{
			$_POST['ddl7IncomeState']= NULL;
			$_POST['txt7OtherIncomes']= NULL;
		}
		else
		{
			$_POST['txt7Occupation'] = NULL;
			$_POST['txt7WeeklySalary'] = NULL;
			$_POST['txt7WorkingHours'] = NULL;
			$_POST['txt7WorkingIncomeLost'] = NULL;
			$_POST['ddl7HaveInjuries'] = NULL;
		}
		
		$view_work_history = getQuery('select id from lead_work_care_history where lead_accident_id='.$hfLeadAccidentId);
		if(mysqli_num_rows($view_work_history)>0)
		{
			$rc7 = mysqli_fetch_object($view_work_history);
			
			$loginmaster = $mysqli->prepare("update lead_work_care_history set lead_id=?, is_employed=?, income_from=?, other_income_means=?, occupation=?, weekly_salary=?, working_hours=?, income_loss_by_accident=?,injury_affecting_work=?, receiving_assistance=?, want_assistance=? where id=?");
			$loginmaster->bind_param("iissssisissi",$lead_id,$_POST['ddl7Employed'],$_POST['ddl7IncomeState'],$_POST['txt7OtherIncomes'],$_POST['txt7Occupation'],$_POST['txt7WeeklySalary'],$_POST['txt7WorkingHours'],$_POST['txt7WorkingIncomeLost'],$_POST['ddl7HaveInjuries'],$_POST['txt7ReceivingAssistance'],$_POST['txt7RequireReceivingAssistance'], $rc7->id);
		}
		else
		{
			$loginmaster = $mysqli->prepare("INSERT INTO lead_work_care_history(lead_id, is_employed, income_from, other_income_means, occupation, weekly_salary, working_hours, income_loss_by_accident, injury_affecting_work, receiving_assistance, want_assistance, lead_accident_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
			$loginmaster->bind_param("iissssisissi",$lead_id,$_POST['ddl7Employed'],$_POST['ddl7IncomeState'],$_POST['txt7OtherIncomes'],$_POST['txt7Occupation'],$_POST['txt7WeeklySalary'],$_POST['txt7WorkingHours'],$_POST['txt7WorkingIncomeLost'],$_POST['ddl7HaveInjuries'],$_POST['txt7ReceivingAssistance'],$_POST['txt7RequireReceivingAssistance'], $hfLeadAccidentId);
		}
		$loginmaster->execute();
		$loginmaster->close();
	}
}
?>