<?php 
include('../includes/basic_auth.php'); 

$getStates = getStates();
$getCountries = getCountries();
$getCallcenters = getCallcenters('');

if(isset($_POST["btnCallCenter"]))
{
	if(trim($_POST["txtCallCenterName"])!=null && trim($_POST["ddlCountry"])!=null && trim($_POST["txtCallCenterAddress"])!=null && trim($_POST["txtCallCenterPhone"])!=null)
	{
		$count_users = countUsersWithMobile($_POST['txtCallCenterPhone']);

		if($count_users==0)
		{
			$ori_pass = create_unique_pass(); //send pass by message or email
			$password=create_password($ori_pass);
			
			$type=$VA; //callcenter admin
			$status='AC';
			$loginmaster = $mysqli->prepare("insert into login_master(phone, password, type, status, added_by, creation_date, updation_date) values(?,?,?,?,?,?,?)");
			$loginmaster->bind_param("ssssiss",$_POST['txtCallCenterPhone'],$password,$type,$status,$_SESSION['userId'], $thisdate, $thisdate);
			$loginmaster->execute();
			$loginmaster->close();
			
			$uid = mysqli_insert_id($mysqli);
			
			$_POST['ddlState']=null;
			$_POST['ddlSubUrb']=null;
			$_POST['txtCallCenterPostal']=null;
			
			$loginmaster = $mysqli->prepare("INSERT INTO verifier(uid, verifier_name, phone, alt_number, state_id, suburb_id, postal_code, address, country) VALUES (?,?,?,?,?,?,?,?,?)");
			$loginmaster->bind_param("isssiissi",$uid,$_POST['txtCallCenterName'],$_POST['txtCallCenterPhone'],$_POST['txtCallCenterAltPhone'],$_POST['ddlState'],$_POST['ddlSubUrb'],$_POST['txtCallCenterPostal'],$_POST['txtCallCenterAddress'],$_POST['ddlCountry']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$_SESSION['success']='Verifier created successfully.';
			header("location:add-verifier.php" );
			exit();
		}
		else
		{
			$_SESSION['error']="A user exists with this mobile number.";
		}
	}
	else
	{
		$_SESSION['error']="Enter all the required fields.";
	}
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>

	<body class="fixed-nav sticky-footer" id="page-top">
	
		<?php include('../includes/navigation.php'); ?>
	  
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Add Verifier</h4>
					</div>
				</div>
				<!-- Title & Breadcrumbs-->
				
				<!-- row -->
			
				<!-- row -->
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
							<div class="card-body">
								<form name='frmAddCallCenter' id='frmAddCallCenter' action='' method='post'>
									<?php include("../includes/messages.php") ?>
									<div class="row">
										<div class="col-md-3 col-sm-12">
										  <div class="form-group">
											<label for="txtCallCenterName">Verifier Name</label>
											<input class="form-control" name="txtCallCenterName" id="txtCallCenterName" placeholder="Verifier" type="text"/>
										  </div>
										</div>
										<div class="col-md-3 col-sm-12">
										  <div class="form-group">
											<label for="txtCallCenterPhone">Phone Number</label>
											<input class="form-control" name="txtCallCenterPhone" id="txtCallCenterPhone" placeholder="Mobile Number" type="text"/>
										  </div>
										</div>
										<div class="col-md-3 col-sm-12">
										  <div class="form-group">
											<label for="txtCallCenterAltPhone">Alternate Number</label>
											<input class="form-control" name="txtCallCenterAltPhone" id="txtCallCenterAltPhone" placeholder="Alternate Mobile" type="text" />
										  </div>
										</div>
										<div class="col-md-3 col-sm-12">
										  <div class="form-group">
											<label for="">Country</label>
											<select name="ddlCountry" id="ddlCountry" class="d-flex form-control">
												<option value=''>Select Country</option>
												<?php
													while($cnrow = mysqli_fetch_object($getCountries))
													{
														?>	
															<option value='<?php echo $cnrow->id ?>'><?php echo $cnrow->name ?></option>
														<?php
													}
												?>
												
											</select>
										  </div>
										</div>
										
										<div class="col-md-12 col-sm-12 col-xs-12">
										  <div class="form-group fullwidthTextarea">
											<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Address First Line" name='txtCallCenterAddress' id='txtCallCenterAddress'></textarea>
										  </div>
										</div>
										
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input name="btnCallCenter" id="btnCallCenter" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save" />
										</div>	
									  </div>
								</form>
							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			
		<?php include('../includes/copyright.php'); ?>
		
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
	  
	</body>
</html>
