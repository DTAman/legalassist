<?php 
include('../includes/basic_auth.php');

if(isset($_POST['btnLeadCallBack']))
{
	$comment_status = 'PN';
	
	if(trim($_POST['hfLeadCallBackId'])!=null && trim($_POST['ddlCallBackState'])!=null)
	{
		$loginmaster = $mysqli->prepare("update lead_callbacks set status='EX' where lead_id = ?");
		$loginmaster->bind_param("i",$_POST['hfLeadCallBackId']);
		$loginmaster->execute();
		$loginmaster->close();
		
		$dt = $_POST['txtCCallBackYear']."-".$_POST['txtCCallBackMonth']."-".$_POST['txtCCallBackDay']." ".$_POST['txtCCallBackHours'].":".$_POST['txtCCallBackMinutes'].":00";
		$sel_d = date('Y-m-d H:i:s',strtotime($dt));
		
		$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
		$loginmaster->bind_param("i",$_POST['hfLeadCallBackId']);
		$loginmaster->execute();
		$loginmaster->close();
		
		if($_SESSION['userType']=='MSW')
		{
			$lead_transfer_type = 'CBA';
		}
		else if($_SESSION['userType']=='VA')
		{
			$lead_transfer_type = 'CBV';
		}
		else if($_SESSION['userType']=='CCA')
		{
			$lead_transfer_type = 'CBC';
		}
		else if($_SESSION['userType']=='CTL')
		{
			$lead_transfer_type = 'CBTL';
		}
		else if($_SESSION['userType']=='CA')
		{
			$lead_transfer_type = 'CBAG';
		}
		
		$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type, callback_dt,callback_type,client_state_zone_callback) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
		$loginmaster->bind_param("iisissssssi",$_POST['hfLeadCallBackId'],$_SESSION['userId'],$_SESSION['userType'], $_SESSION['userId'],$_POST['txtLeadCallBackComment'],$comment_status,$thisdate,$lead_transfer_type, $sel_d,$_POST['rbtnLeadCallbackType'],$_POST['ddlCallBackState']);
		$loginmaster->execute();
		$loginmaster->close();
		
		$loginmaster = $mysqli->prepare("update leads set updation_date=?, latest_calback = ?, latest_callbacktype = ? , latest_callback_datetime = ? , latest_callback_timezone = (select initials from states y where y.id = ?), outcome_type=?, outcome = (select outcome_title from outcomes where outcome_initials = ?) where id=?");
		$loginmaster->bind_param("sssssssi", $thisdate,$_POST['txtLeadCallBackComment'],$_POST['rbtnLeadCallbackType'], $sel_d,$_POST['ddlCallBackState'],$lead_transfer_type, $lead_transfer_type, $_POST['hfLeadCallBackId']);
		$loginmaster->execute();
		$loginmaster->close();
		
		$loginmaster = $mysqli->prepare("INSERT INTO lead_callbacks(lead_id, callback_datetime, callback_details, call_back_type, status, timezone_target, added_by) VALUES (?,?,?,?,'AC',?,?)");
		$loginmaster->bind_param("issssi",$_POST['hfLeadCallBackId'],$sel_d,$_POST['txtLeadCallBackComment'], $_POST['rbtnLeadCallbackType'],$_POST['ddlCallBackState'],$_SESSION['userId']);
		$loginmaster->execute();
		$loginmaster->close();
		
		$_SESSION['success']='Details have been saved successfully.';
		header("location:callbacks.php");
		
	}
}

if(isset($_GET['hfId']) && trim($_GET['hfId'])!=null)
{
	$loginmaster = $mysqli->prepare("update lead_callbacks set status='DN' where id = ?");
	$loginmaster->bind_param("i",$_GET['hfId']);
	$loginmaster->execute();
	$loginmaster->close();
	
	$comment_status='PN';
	if($_SESSION['userType']=='MSW')
	{
		$lead_transfer_type = 'CBAD';
	}
	else if($_SESSION['userType']=='VA')
	{
		$lead_transfer_type = 'CBVD';
	}
	else if($_SESSION['userType']=='CCA')
	{
		$lead_transfer_type = 'CBCD';
	}
	else if($_SESSION['userType']=='CTL')
	{
		$lead_transfer_type = 'CBTLD';
	}
	else if($_SESSION['userType']=='CA')
	{
		$lead_transfer_type = 'CBAGD';
	}
	
	$txtSaveCloseDetailsNotes = "Callback Done";
	
	$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id in (select lead_id from lead_callbacks where id = ?)");
	$loginmaster->bind_param("i",$_GET['hfId']);
	$loginmaster->execute();
	$loginmaster->close();
	
	$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type) VALUES ((select lead_id from lead_callbacks where id = ?),?,?,?,?,?,?,?)");
	$loginmaster->bind_param("iisissss",$_GET['hfId'],$_SESSION['userId'],$_SESSION['userType'], $_SESSION['userId'],$txtSaveCloseDetailsNotes,$comment_status,$thisdate,$lead_transfer_type);
	$loginmaster->execute();
	$loginmaster->close();
	
	// $loginmaster = $mysqli->prepare("update leads set updation_date=?, outcome_type=?, outcome = (select outcome_title from outcomes where outcome_initials = ?) where id in (select lead_id from lead_callbacks where id = ?)");
	$loginmaster = $mysqli->prepare("update leads set latest_callbacktype = null, latest_callback_datetime = null, latest_callback_timezone = null, latest_calback = null, updation_date=?, outcome_type=?, outcome = (select outcome_title from outcomes where outcome_initials = ?) where id in (select lead_id from lead_callbacks where id = ?)");
	$loginmaster->bind_param("sssi", $thisdate, $lead_transfer_type, $lead_transfer_type, $_GET['hfId']);
	$loginmaster->execute();
	$loginmaster->close();
	
	$_SESSION['success']='Details have been saved successfully.';
	header("location:callbacks.php");
}

$dataresult = getCallbacksDashboard(" and lead_callbacks.added_by = ".$_SESSION['userId']);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
		

	</head>

	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
		<div class="content-wrapper">
			<div class="container-fluid">
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Callbacks</h4>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
							<div class="card">
								<div class="card-body">
									<?php
									if(mysqli_num_rows($dataresult)>0)
									{
										$n=1;
										?>
										<div class="hh5">
											<table class="table newTable table-bordered">
												<thead>
													<tr class="alert-danger">
														<th class="text-center">Sr. No</th>
														<th>Type</th>
														<th class="text-center">Lead Id</th>
														<th>Lead Details</th>
														<th class="text-center">Callback</th>
														<th>Comments</th>
														<th class="text-center">Action</th>
													</tr>
												</thead>
											<tbody>
												<?php
													while($r = mysqli_fetch_object($dataresult))
													{
														?>
															<tr class="myrow<?php echo $r->lead_id ?>">
																<td class="text-center"><?php echo $n ?></td>
																<td><?php echo $r->call_back_type=='N'?'No Answer':'Callback Requested By Client' ?></td>
																<td class="text-center"><?php echo trim($r->lead_id) ?></td>
																<td><?php echo trim($r->first_name.' '.$r->middle_name.' '.$r->last_name) ?></td>
																<td class="text-center"><?php echo date("Y-m-d H:i A",strtotime($r->callback_datetime)).' ('.$r->initials.')' ?></td>
																<td style="max-width:400px"><?php echo $r->callback_details ?></td>
																<td class="text-center">
																	<button attr_lead='<?php echo $r->lead_id ?>' class="btn btn-danger btn-small callPopUpCallback">New CallBack</button>
																	<button attr_lead='<?php echo $r->id ?>' class="btn btn-success btn-small callPopUpCallbackDone">Done</button>
																</td>
															</tr>
														<?php
														$n++;
													}
													?>
											</tbody>
											</table>
										</div>
										<?php
									}
									else
									{
										?>
											<div class="col-md-12">
												<div class="alert alert-danger" role="alert">
													You have no new callbacks. Come back later.
												</div>
											</div>
										<?php
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				</div>
				<!-- /.row -->
				
			
				

			</div>  
			<!-- /.content-wrapper -->
			
			<!-- Footer -->
			<?php include('callbackModal.php'); ?>
			<?php include('../includes/copyright.php'); ?>
			
			<!-- Scroll to Top Button-->  
			<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			  <i class="ti-angle-double-up"></i>
			</a>

			<?php include('../includes/web_footer.php'); ?>
			
	  </div>
	  <!-- Wrapper -->
	  
	</body>
</html>
