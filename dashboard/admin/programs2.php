<?php
include('../includes/basic_auth.php'); 

$queryResults = "select * from lead_accidents order by lead_id, accident_date_time";
$totalResults = getQuery($queryResults);

while($rc = mysqli_fetch_object($totalResults))
{
	$loginmaster = $mysqli->prepare("update leads set last_accident_type = (select type from accident_types where id = ?), last_accident_na = ?, latest_accident_dt = ? where id=?");
	$loginmaster->bind_param("iisi",$rc->accident_kind,$rc->is_na, $rc->accident_date_time,$rc->lead_id);
	$loginmaster->execute();
	$loginmaster->close();
}

?>
