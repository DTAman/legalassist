<?php 
include('../includes/basic_auth.php');

$condition='';

if(!isset($_SESSION['userType']) || !isset($_SESSION['userId']))
{
	session_destroy();
	header("location:".$mysiteurl.'logout.php');
}
else if(trim(($_SESSION['userId']))==null)
{
	session_destroy();
	header("location:".$mysiteurl.'logout.php');
}
else
{
	if(isset($_GET['t'])&& trim($_GET['t'])==1)
	{
		$name='Team Lead';
		$type=$CTL;
		$is_a = 1;
	}
	else
	{
		$name='Agent';
		$type=$CA;
		$is_a = 2;
	}

	if(isset($_POST["btnAdd"]))
	{
		if(trim($_POST['txtSearch'])!=null)
		{
			$condition.=" and (login_master.phone like '%{$_POST['txtSearch']}%' or first_name like '%{$_POST['txtSearch']}%' or middle_name like '%{$_POST['txtSearch']}%' or last_name like '%{$_POST['txtSearch']}%') ";
		}
		
		if(trim($_POST['txtFromDate'])!=null)
		{
			$create_date=date('Y-m-d',strtotime($_POST['txtFromDate']));
			$condition.=" and login_master.creation_date >= '{$create_date}'";
		}
		
		if(trim($_POST['txtToDate'])!=null)
		{
			$to_date=date('Y-m-d',strtotime($_POST['txtToDate']));
			$condition.=" and login_master.creation_date <= '{$to_date}'";
		}
	}

	// $condition.=" and tl_and_agents.type='".$type."' ";
	$condition.=" and tl_and_agents.type='".$type."' and callcenter_id=".$_SESSION['userId'];

	$totalCount = getUserCount($type, $condition);
	$totalResults = getUserDataByPagination($type,$condition,$limit,$offset);

	?>
	<!DOCTYPE html>
	<html lang="en">
		<head>
			<?php include('../includes/header.php'); ?>
		</head>
		<body class="fixed-nav sticky-footer" id="page-top">
			<?php include('../includes/navigation.php'); ?>
		  
			<div class="content-wrapper">

				<div class="container-fluid">
				
					<!-- Title & Breadcrumbs-->
					<div class="row page-titles" style="margin-bottom:0;">
						<div class="col-md-12 align-self-center">
							<h4 class="theme-cl">View <?php echo $name ?></h4>
						</div>
					</div>
					<!-- Title & Breadcrumbs-->
					
					<!-- row -->
				
					<!-- row -->
					
					<div class="card"  style="margin-bottom:0;">
						<div class="card-body">
							<form action='' name='frmSearch' id='frmSearch' method='post'>
								<div class="row">
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtSearch"])) echo $_POST["txtSearch"] ?>" class="form-control" placeholder='Mobile / Name' id="txtSearch" name="txtSearch" placeholder="" type="text" />
										</div>
									</div>
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtFromDate"])) echo $_POST["txtFromDate"] ?>" class="form-control" placeholder='From' id="txtFromDate" name="txtFromDate" placeholder="" type="text" />
										</div>
									</div>
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtToDate"])) echo $_POST["txtToDate"] ?>" class="form-control" placeholder='To' id="txtToDate" name="txtToDate" placeholder="" type="text" />
											<input value="ajaxgetTLs.php" class="form-control" id="hfAjaxPage" name="hfAjaxPage" placeholder="" type="hidden" />
											<input value="<?php echo $is_a ?>" class="form-control" id="t_val" name="t_val" placeholder="" type="hidden" />
										</div>
									</div>
									<div class="col-md-3 col-sm-4 col-xs-4 text-right">
										<input type="submit" class="btn btn-primary" value="Search" name="btnAdd" id="btnAdd" />
										<input type="button" class="btn btn-warning" value="Reset" name="btnReset" id="btnReset" onclick="window.location='view-agent.php?t=<?php echo $is_a?>'" />
									</div>
								</div>
							</form>
						</div>
					</div>
					
					<div class="row">
					
						
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="change-password">
							<div class="card">
							
								
								<div class="card-body">

									<!-- Table Start -->

									<div class="table-responsive ajaxClass viewLeadTable">
										<?php
										if(mysqli_num_rows($totalResults)!=0)
										{
											?>
											<div class='hh5'>	<table class="w-100 table newTable table-striped table-hover">
												<thead>
													<tr>
														<th class='text-center'>Sr.No.</th>
														<th>Name</th>
														<th>Phone Number</th>
														<th>PAN</th>
														<th style="min-width: 300px">Address</th>
														<th style="min-width: 60px">Attachments</th>
														<th>Status</th>
														<th class='text-center'>Quick Actions</th>
													</tr>
												</thead>
												<tbody>
												<?php
												$n = 1;
												while($rc = mysqli_fetch_object($totalResults))
												{
													$attachments = 0;
													trim($rc->adhar)!=null?$attachments++:'';
													trim($rc->pan)!=null?$attachments++:'';
													trim($rc->driving_license)!=null?$attachments++:'';
													trim($rc->voter_id)!=null?$attachments++:'';
													trim($rc->id_proof)!=null?$attachments++:'';
													trim($rc->passport)!=null?$attachments++:'';
													trim($rc->other_proof)!=null?$attachments++:'';
													?>
														<tr id="delete<?php echo $rc->uid ?>">
															<td class='text-center'><?php echo $n ?></td>
															<td><?php echo $rc->first_name." ".$rc->middle_name." ".$rc->last_name ?></td>
															<td><?php echo $rc->phone ?></td>
															<td><?php echo $rc->pan_number ?></td>
															<td><?php echo $rc->address.', '.$rc->cname ?></td>
															<td style="min-width: 60px"><i class="p-2 text-danger ti-pin-alt" style="font-size: 18px"></i><?php echo $attachments ?></td>
															<td><?php echo $rc->status=='AC'?'Active':'Inactive' ?></td>
															<td class='text-center'>
																<div class='icnos'>
																	<a href="edit-agent-each.php?t=<?php echo $is_a?>&u=<?php echo $rc->uid ?>" class="icn settings" title="" data-toggle="tooltip" data-original-title="Edit" aria-describedby="tooltip600250"><i class="ti-pencil"></i></a>
																	<a class="icn text-success" href="edit-agent-each.php?t=<?php echo $is_a?>&u=<?php echo $rc->uid ?>&s=1" title="" data-toggle="tooltip" data-original-title="View" aria-describedby="tooltip600250"><i class="ti-eye"></i></a>
																	<a href="javascript:void(0)" onclick='deleteUser(<?php echo $rc->uid ?>)'  class="icn delete" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
																</div>
															</td>
														</tr>
													<?php
													$n++;
												}
												?>
												</tbody>
											</table></div>
										<?php
											include('../includes/ajax_before_pagination.php'); 
										}
										else
										{
											include('../includes/norows.php'); 
										}
										?>
									</div>

									<!-- Table End -->

								</div>
							</div>
						</div>
					
						</div>
					</div>
					<!-- /.row -->
				</div>

			</div>
				<!-- /.content-wrapper -->
				<?php include('../includes/copyright.php'); ?>
			<!-- Scroll to Top Button-->
			<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
				<i class="ti-angle-double-up"></i>
			</a>
			<?php include('../includes/web_footer.php'); ?>
		  
		</body>
	</html>
	<?php
}
?>