<?php 
include('../includes/basic_auth.php'); 

$type=$C;
$getStates = getStates();

if(isset($_POST["btnClient"]))
{
	if(trim($_POST["txtClientCompany"])!=null && trim($_POST["ddlState"])!=null && trim($_POST["ddlSubUrb"])!=null && trim($_POST["txtClientAddress"])!=null && trim($_POST["txtClientEmail"])!=null && trim($_POST["txtClientPostal"])!=null)
	{
		$count_users = countUsersWithMobile($_POST['txtAddPhone']);

		if($count_users==0)
		{
			$ori_pass = create_unique_pass(); //send pass by message or email
			$password=create_password($ori_pass);
			
			$status='AC';
			
			$loginmaster = $mysqli->prepare("insert into login_master(phone, password, type, status, added_by, creation_date, updation_date) values(?,?,?,?,?,?,?)");
			$loginmaster->bind_param("ssssiss",$_POST['txtAddPhone'],$password,$type,$status,$_SESSION['userId'], $thisdate, $thisdate);
			$loginmaster->execute();
			$loginmaster->close();
			
			$uid = mysqli_insert_id($mysqli);
			
			$loginmaster = $mysqli->prepare("INSERT INTO clients(uid, company_name, auth_person, person_designation, email, state_id, suburb_id, postal_code, address, added_by, status, creation_date, updation_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
			$loginmaster->bind_param("issssiississs",$uid, $_POST['txtClientCompany'],$_POST['txtClientName'],$_POST['txtClientDesignation'],$_POST['txtClientEmail'],$_POST['ddlState'],$_POST['ddlSubUrb'],$_POST['txtClientPostal'],$_POST['txtClientAddress'],$_SESSION['userId'],$status,$thisdate,$thisdate);
			$loginmaster->execute();
			$loginmaster->close();

			if(isset($_POST['txtCCEmails']) && count($_POST['txtCCEmails'])>0)
			{
				foreach ($_POST['txtCCEmails'] as $key => $cc_email) 
				{
					if(trim($cc_email)!=null)
					{
						$loginmaster = $mysqli->prepare("insert into clients_bcc(master_client_id, cc_email, added_by, creation_date) values(?,?,?,?)");
						$loginmaster->bind_param("isis",$uid,$cc_email,$_SESSION['userId'], $thisdate);
						$loginmaster->execute();
						$loginmaster->close();
					}
				}
			}

			$_SESSION['success']='Client created successfully.';
			
			$_POST=array();
			header( "refresh:3;url=add-client-name.php" );
		}
		else
		{
			$_SESSION['error']="A user exists with this mobile number.";
		}
	}
	else
	{
		$_SESSION['error']="Enter all the required fields.";
	}
}

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Add Sol Name</h4>
					</div>
				</div>
				<div class="row">
				
					
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
						
							
							<div class="card-body">
								<form name='frmAddClients' id='frmAddClients' action='' method='post'>
									<?php include("../includes/messages.php") ?>
									<div class="row">
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtClientCompany">Name Of Company</label>
											<input class="form-control" name="txtClientCompany" id="txtClientCompany" placeholder="Company Name" type="text" />
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtClientName">Authorized Person Name</label>
											<input class="form-control" name="txtClientName" id="txtClientName" placeholder="Authorised Person Name" type="text">
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtClientDesignation">Authorized Person Designation</label>
											<input class="form-control" name="txtClientDesignation" id="txtClientDesignation" placeholder="Designation" type="text">
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtAddPhone">Phone Number</label>
											<input class="form-control" name="txtAddPhone" id="txtAddPhone" placeholder="Mobile Number" type="text"/>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12">
										  <div class="form-group">
											<label for="txtClientEmail">E-Mail ID</label>
											<input class="form-control" name="txtClientEmail" id="txtClientEmail" placeholder="E-mail" type="text">
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="ddlState">Address</label>
											<select name="ddlState" id="ddlState" class="d-flex form-control" onchange="getAllSuburbs(this.value)">
												<option value=''>Select State</option>
												<?php
													while($staterow = mysqli_fetch_object($getStates))
													{
														?>	
															<option value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
														<?php
													}
												?>
												
											</select>
										  </div>
										</div>
										
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="ddlSubUrb" class="invisible d-none d-md-block" >Select Sub urb</label>
											<select name="ddlSubUrb" id="ddlSubUrb" class="d-flex form-control">
												<option value=''>Select Sub urb</option>
											</select>
										  </div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
										  <div class="form-group">
											<label for="txtClientPostal" class="invisible d-none d-md-block" >Postal Code</label>
											<input class="form-control" name="txtClientPostal" id="txtClientPostal"  placeholder="Post Code" type="text">
										  </div>
										</div>
										<div class="col-md-8 col-sm-12 col-xs-12">
										 <div class="form-group fullwidthTextarea">
											<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Address First Line" name='txtClientAddress' id='txtClientAddress'></textarea>
										  </div>
										</div>
									  </div>
										

										<!-------------------------------------------------------------------------------->
										<div class="row">
											<div class="col-md-8 col-10">
												<label for="txtClientCompany">Sol Emails</label>
											</div>
											<div class="col-md-4 col-2">
												<i class="addClientsBox bg-success fa fa-plus" onclick="appendTextBoxCCEmail()"></i>
											</div>
										</div>


										<div id="appendTextBoxCCOuter">
											<div class="row appendTextBoxCCEmail">
												<div class="col-md-8 col-10">	
													<div class="form-group">
														<input value="" class="form-control" name="txtCCEmails[]" id="txtCCEmails1" type="email" />
													</div>
												</div>
												<div class="col-md-4 col-2">
													<i class="addClientsBox bg-danger fa fa-minus removeTextBoxCCEmail"></i>
												</div>
											</div>
										</div>


										<!-------------------------------------------------------------------------------->

										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<input name="btnClient" id="btnClient" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Save" />
											</div>
										</div>
								</form>
							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			<!-- /.content-wrapper -->
				<?php include('../includes/copyright.php'); ?>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
	  
	</body>
</html>
