<?php
	include('../includes/basic_no_auth.php');
	
	if(isset($_SESSION['userId']))
	{
		$dataresult = getUserAssignedComments($_SESSION['userId']);

		if(mysqli_num_rows($dataresult)>0)
		{
			while($r = mysqli_fetch_object($dataresult))
			{ 
				$c = $r->lead_transfer_type;
				$t='';
				
				switch($c)
				{
					case 'LT': $color = 'info';$t = 'Lead Transferred';break;
					case 'FI': $color = 'light';$t = 'Further Investigation';break;
					case 'RJ': $color = 'danger';$t = 'Lead Rejected By Sol';break;
					case 'SU': $color = 'primary';$t = 'Sign Up Arranged By Sol';break;
					case 'AP': $color = 'dark'; $t = 'Lead Approved';break;
					case 'SI': $color = 'warning';$t = 'Invoice Sent By Sol';break;
					case 'IP': $color = 'success';$t = 'Invoice Paid By Sol';break;
					case 'CBA': $color = 'success';$t = 'Callback';break;
					case 'CBV': $color = 'success';$t = 'Callback';break;
					case 'CBTL': $color = 'success';$t = 'Callback';break;
					case 'CBAG': $color = 'success';$t = 'Callback';break;
					case 'CBC': $color = 'success';$t = 'Callback';break;
				}
				
				?>
					<div class="col-md-6">
						<div class="alertBoxesOuter alert alert-<?php echo $color?>">
							<h3><span><?php echo trim($r->first_name.' '.$r->middle_name.' '.$r->last_name) ?></span> - <?php echo $t ?></h3>
							<a target='_blank' href="view-each-lead.php?u=<?php echo $r->lead_id ?>">
								<p><?php echo $r->comments ?></p>
							</a>
						</div>
					</div>
				<?php
			}
		}
		else
		{
			?>
				<div class="col-md-12">
					<div class="alert alert-danger" role="alert">
						You have no new notifications to checkout. Come back later.
					</div>
				</div>
			<?php
		}
	}
	
?>