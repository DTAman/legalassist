<div id="copymodal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">View Passengers</h4>
			</div>
			<div class="modal-body">
				<div id='copyleaddata'></div>
			</div>
		</div>
	</div>
</div>