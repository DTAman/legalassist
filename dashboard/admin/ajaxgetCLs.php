<?php
include('../includes/basic_auth.php'); 

$p=1;
$pagecount=1;
$last_page=1;
$first_page=0;
$offset=0;
$condition='';

if(isset($_POST['page_skip']))
{
	$offset=($_POST['page_skip']-1)*$limit;
	$n = $offset+1;
}
else
{
	$_POST['page_skip']=0;
}

if(trim($_POST['txtSearch'])!=null)
{
	$condition.=" and (auth_person like '%{$_POST['txtSearch']}%' or company_name like '%{$_POST['txtSearch']}%') ";
}

if(trim($_POST['txtFromDate'])!=null)
{
	$create_date=date('Y-m-d',strtotime($_POST['txtFromDate']));
	$condition.=" and login_master.creation_date >= '{$create_date}'";
}

if(trim($_POST['txtToDate'])!=null)
{
	$to_date=date('Y-m-d',strtotime($_POST['txtToDate']));
	$condition.=" and login_master.creation_date <= '{$to_date}'";
}

if(isset($_POST['nextpage']) && trim($_POST['nextpage'])!=null)
{
	$pagecount = $_POST['nextpage'];
	$last_page = $_POST['nextpage'];
}

if(isset($_POST['firstpage']) && trim($_POST['firstpage'])!=null)
{		
	$pagecount = $_POST['firstpage']-($set_count-1);
	$last_page = $_POST['firstpage']-($set_count-1);
}

if(isset($_POST['firstpage1']) && trim($_POST['firstpage1'])!=null)
{
	$firstpage1 = trim($_POST['firstpage1']);
	
	$pagecount = $firstpage1;
	$last_page = $firstpage1;
}

$totalCount = getUserCount($C, $condition);
$totalResults = getUserDataByPagination($C,$condition,$limit,$offset);
	
if(mysqli_num_rows($totalResults)>0)
{
	?>
		<div class='hh5'> <table class="w-100 table newTable table-striped table-hover">
			<thead>
				<tr>
					<th class='text-center'>Sr.No.</th>
					<th>Name of Company</th>
					<th>Phone</th>
					<th>Person Name</th>
					<th>Person Designation</th>
					<th>E-Mail Id</th>
					<th style="min-width: 300px">Address</th>
					<th>Status</th>
					<th>Online</th>
					<th class='text-center'>Quick Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php
			//$n = 1;
			while($rc = mysqli_fetch_object($totalResults))
			{
				?>
					<tr id="delete<?php echo $rc->uid ?>">
						<td class='text-center'><?php echo $n ?></td>
						<td><?php echo $rc->company_name ?></td>
						<td><?php echo $rc->phone ?></td>
						<td><?php echo $rc->auth_person ?></td>
						<td><?php echo $rc->person_designation ?></td>
						<td><?php echo $rc->email ?></td>
						<td><?php echo $rc->address.', '.$rc->suburb_name.', '.$rc->state_name ?></td>
						<td><?php echo $rc->status=='AC'?'Active':'Inactive' ?></td>
						<td class='text-center'><?php echo $rc->is_online==1?'<i class="fa fa-check text-success" style="font-size:14px"></i>':'<i class="fa fa-times text-danger" style="font-size:14px"></i>' ?></td>
						<td class='text-center'>
							<div class="icnos">
								<a href="edit-client-name-each.php?u=<?php echo $rc->uid ?>" class="settings icn" title="" data-toggle="tooltip" data-original-title="Edit" aria-describedby="tooltip600250"><i class="ti-pencil"></i></a>
								<a class="icn text-success" href="edit-client-name-each.php?u=<?php echo $rc->uid ?>&s=1" title="" data-toggle="tooltip" data-original-title="View" aria-describedby="tooltip600250"><i class="ti-eye"></i></a>
								<a href="javascript:void(0)" onclick='deleteUser(<?php echo $rc->uid ?>)'  class="icn delete" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
							</div>
						</td>
					</tr>
				<?php
				$n++;
			}
			?>
			</tbody>
		</table></div>
		<?php include('../includes/ajax_pagination.php'); ?>
	</div>
	<?php
}
else
{
	?>
		<?php include('../includes/norows.php'); ?>
	<?php
}
?>