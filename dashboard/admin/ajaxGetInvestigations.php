<?php
include('../includes/basic_auth.php'); 

if(isset($_POST['hfLeadId']) && trim($_POST['hfLeadId'])!=null)
{
	$lead_id = $_POST['hfLeadId'];
	
	$datamaster = $mysqli->prepare("select creation_date from lead_comments where lead_id=? and directly_assigned_admin=1 order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$datamaster->bind_result($directly_creation_date);
	$datamaster->fetch();
	$datamaster->close();
	
	$condition = '';
	if($directly_creation_date!=null)
	{
		$condition.=" and lead_comments.creation_date > '".$directly_creation_date."'";
	}
	
	$datamaster = $mysqli->prepare("select creation_date from lead_comments where comment_utype ='C' and lead_transfer_type='RJ' ".$condition." and lead_id=".$lead_id." and comment_from=".$_SESSION['userId']." order by lead_comments.creation_date desc limit 1");
	$datamaster->execute();
	$datamaster->bind_result($rej_date);
	$datamaster->fetch();
	$datamaster->close();
	
	if($rej_date!=null)
	{
		$condition.= " and lead_comments.creation_date>='".$rej_date."'";
	}
	
	$loginmaster = $mysqli->prepare("select comments from lead_comments where lead_id=? and comment_to=? and lead_transfer_type='FI' ".$condition." order by creation_date");
	$loginmaster->bind_param("ii",$lead_id,$_SESSION['userId']);
	$loginmaster->execute();
	$d = $loginmaster->get_result();
	$loginmaster->close();
	
	if(mysqli_num_rows($d)>0)
	{
		?>
			<div class="alert alert-warning fade in alert-dismissible show">
				<?php
				while($dr = mysqli_fetch_object($d))
				{
					echo '---> '.$dr->comments."<br/>";
				}
				?>
			</div>
		<?php
	}
}
?>