<?php
include('../includes/basic_auth.php'); 

$status='AC';

/* if(isset($_POST['hfLeadId']) && isset($_POST['txtCallBackDate']) && isset($_POST['txtSaveCloseDetails']) && trim($_POST['hfLeadId'])!=0)
{
	$user_lead_data = getLeadsDataByPagination(" and id=".$_POST['hfLeadId'],1, 0);
	$rc = mysqli_fetch_object($user_lead_data);
	
	$txtCallBackDate = date('Y-m-d H:i:s',strtotime($_POST['txtCallBackDate']));
	// $txtCallBackDate = changeTimeZone(date('Y-m-d H:i:s',strtotime($_POST['txtCallBackDate'])),getAustraliaStateQuery($rc->state_id),"UTC");
	
	$hfLeadAccidentId = $_POST['hfLeadAccidentId']==0?NULL:$_POST['hfLeadAccidentId'];

	$loginmaster = $mysqli->prepare("insert into lead_callbacks(lead_id, lead_accident_id, callback_datetime, callback_details, status, timezone_target) values(?,?,?,?,?,?)");
	$loginmaster->bind_param("iissss",$_POST['hfLeadId'],$hfLeadAccidentId,$txtCallBackDate,$_POST['txtSaveCloseDetails'],$status,$_POST['ddlSignUpStates']);
	$loginmaster->execute();
	$loginmaster->close();
} */


if(isset($_POST['hfLeadId']) && isset($_POST['txtSaveCloseDetails']) && trim($_POST['hfLeadId'])!=0)
{
	$txtCallBackDate = date('Y-m-d H:i:s');  // saving the current date
	$_POST['ddlSignUpStates'] = null;
	
	$hfLeadAccidentId = $_POST['hfLeadAccidentId']==0?NULL:$_POST['hfLeadAccidentId'];

	$loginmaster = $mysqli->prepare("insert into lead_callbacks(lead_id, lead_accident_id, callback_datetime, callback_details, status, timezone_target,added_by) values(?,?,?,?,?,?,?)");
	$loginmaster->bind_param("iissssi",$_POST['hfLeadId'],$hfLeadAccidentId,$txtCallBackDate,$_POST['txtSaveCloseDetails'],$status,$_POST['ddlSignUpStates'],$_SESSION['userId']);
	$loginmaster->execute();
	$loginmaster->close();
	
	$loginmaster = $mysqli->prepare("update leads set updation_date=? where id=?");
	$loginmaster->bind_param("si",$thisdate,$_POST['hfLeadId']);
	$loginmaster->execute();
	$loginmaster->close();
}

?>