<div id="invoiceModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Enter Details</h4>
			</div>
			<form action='' method='post' name='frmInvoiceComment' id='frmInvoiceComment'>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<input required class="form-control" value="" maxlength="30" name="txtInvoiceNumber" id="txtInvoiceNumber" type="text"  placeholder="Invoice Number" />	
							<input class="form-control" value="" name="hfLeadInvoiceId" id="hfLeadInvoiceId" type="hidden" />	
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<textarea required="required" class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter reason" name='txtInvoiceComment' id='txtInvoiceComment'></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnInvoiceComment" name="btnInvoiceComment" type="submit" class="btn btn-primary" value='Invoice Sent?' />
				</div>
			</form>
		</div>
	</div>
</div>