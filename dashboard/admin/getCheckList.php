<?php
	include('../includes/basic_auth.php');
	
	if(isset($_POST['txtLeadId']) && trim($_POST['txtLeadId'])!=null)
	{
		$view_documents = getQuery('select checklist1, checklist2, checklist3 from leads where id='.$_POST['txtLeadId']);
		$document_flag = mysqli_num_rows($view_documents);
		$chck1 = $chck2 = $chck3 = 0;
		
		$desc1 = 'Claim Information Gathered, Verified and Updated';
		$desc2 = 'CTP Medical Certificate Received';
		$desc3 = 'Police Event Number Received';
		
		if($document_flag>0)
		{
			$rc10 = mysqli_fetch_object($view_documents);
			
			$chck1 = $rc10->checklist1;
			$chck2 = $rc10->checklist2;
			$chck3 = $rc10->checklist3;
		}
		?>
				<div class="modal-header">
					<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Lead Checklist</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="bg-primary-light form-group jumbotron p-4">
								
								<?php
								for($i=1;$i<=3;$i++)
								{
									if($i==1)
									{
										$chck = $chck1;
										$desc = $desc1;
									}
									else if($i==2)
									{
										$chck = $chck2;
										$desc = $desc2;
									}
									else if($i==3)
									{
										$chck = $chck3;
										$desc = $desc3;
									}
									?>
										<div class="custom-radio mr-3">
											<input <?php echo $chck==1?'checked':''; ?> type="checkbox" class="custom-control-input" id="rbtnval<?php echo $i ?>" name="rbtnValues[]" value="1" />
											<label class="custom-control-label" for="rbtnval<?php echo $i ?>"><?php echo $desc ?></label>
										</div>
									<?php
								}
								?>
								
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<input class="form-control" value="<?php echo $_POST['txtLeadId'] ?>" name="hfCheckListId" id="hfCheckListId" type="hidden"/>	
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnSaveCheckList" name="btnSaveCheckList" type="submit" class="btn btn-primary" value='Update Checklist' />
				</div>
		<?php
	}
?>