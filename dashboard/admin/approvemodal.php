<div id="approveModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Enter Details</h4>
			</div>
			<form action='' method='post' name='frmApproveComment' id='frmSaveCloseComment'>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<input class="form-control" value="" name="hfLeadApproveId" id="hfLeadApproveId" type="hidden" />	
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<textarea required="required" class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter approval reason" name='txtApproveComment' id='txtApproveComment'></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnApproveComment" name="btnApproveComment" type="submit" class="btn btn-primary" value='Approve Lead' />
				</div>
			</form>
		</div>
	</div>
</div>