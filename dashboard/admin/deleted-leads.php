<?php 
include('../includes/basic_auth.php');

set_time_limit(0); 

$getOutcomes = getOutcomes($_SESSION['userId']);
$getStates = getStates();
$getClients = getClients();
$getCallcenters = getCallcenters('');
$getVerifiers = getVerifiers('');

if(!isset($_SESSION['userType']) || !isset($_SESSION['userId']))
{
	session_destroy();
	header("location:".$mysiteurl.'logout.php');
}
else
{
	if(isset($_SESSION['success']) && trim($_SESSION['success'])!=null)
	{
		header("location:deleted-leads.php");
	}

	?>
	<!DOCTYPE html>
	<html lang="en">
		<head>
			<?php include('../includes/header.php'); ?>
		</head>
		<body class="fixed-nav sticky-footer" id="page-top">
			<?php include('../includes/navigation.php'); ?>
			<div class="content-wrapper">
				<div class="container-fluid">
					<div class="row page-titles <?php echo isset($_POST["btnAdd"])?'':'collapsed' ?>"" data-toggle="collapse" data-target=".viewLeadAcc" style="margin-bottom:0;">
						<div class="col-md-12 align-self-center">
							<h4 class="theme-cl">Deleted Leads <i class="ti-arrow-circle-down pull-right"></i></h4>
						</div>
					</div>
					
					
					
					<div class="collapse viewLeadAcc <?php echo isset($_POST["btnAdd"])?'show':'' ?>" style="margin-bottom:0;">
						<div class="card-body">
							<form target="_blank" action='reportsdownload.php' name='frmSearch' id='frmSearch' method='post'>
								<?php include("../includes/messages.php") ?>
								<div class="row">
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_GET['pp'])) echo $_GET['pp'] ?>" class="form-control" id="txtPP" name="txtPP" type="hidden" />
											<input value="<?php if(isset($_POST["txtSearch"])) echo $_POST["txtSearch"] ?>" class="form-control" placeholder='Name / Mobile / Email' id="txtSearch" name="txtSearch" placeholder="" type="text" />
										</div>
									</div>									
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
											  	<a href="#!" id="openSelect1">Select Sol</a>
												<select name="ddlClients[]" id="ddlClients" class="d-flex form-control" multiple>
													<option value=''>Select Sol</option>
													<?php
														$client_array = array();
														while($clientrow = mysqli_fetch_object($getClients))
														{
															?>	
																<option <?php if(isset($_POST['ddlClients']) && count($_POST['ddlClients'])>0){echo in_array($clientrow->uid,$_POST['ddlClients'])?'selected':'';} ?> value='<?php echo $clientrow->uid ?>'><?php echo $clientrow->company_name ?></option>
															<?php
															$client_array[$clientrow->uid] = $clientrow->company_name;
														}
													?>
												</select>
											  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
												<select name="ddlLCallcenters" id="ddlLCallcenters" class="d-flex form-control">
													<option value=''>Select Callcenter</option>
													<?php
														$callcenter_array = array();
														while($ccrow = mysqli_fetch_object($getCallcenters))
														{
															?>	
																<option <?php if(isset($_POST['ddlLCallcenters']) && trim($_POST['ddlLCallcenters'])!=null){echo $_POST['ddlLCallcenters']==$ccrow->uid?'selected':'';} ?> value='<?php echo $ccrow->uid ?>'><?php echo $ccrow->centre_name ?></option>
															<?php
															$callcenter_array[$ccrow->uid] = $ccrow->centre_name;
														}
													?>
												</select>
											  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
												<select name="ddlVerifier" id="ddlVerifier" class="d-flex form-control">
													<option value=''>Select Verifier</option>
													<?php
														$verifier_array = array();
														while($vcrow = mysqli_fetch_object($getVerifiers))
														{
															?>	
																<option <?php if(isset($_POST['ddlVerifier']) && trim($_POST['ddlVerifier'])!=null){echo $_POST['ddlVerifier']==$vcrow->uid?'selected':'';} ?> value='<?php echo $vcrow->uid ?>'><?php echo $vcrow->verifier_name ?></option>
															<?php
															$verifier_array[$vcrow->uid] = $vcrow->verifier_name;
														}
													?>
												</select>
											  </div>
											</div>
											<div class="col-md-3 col-sm-12 col-xs-12">
											  <div class="form-group">
											  	<a href="#!" id="openSelect2">Select Outcome</a>
												<?php include("outcomes_select.php"); ?>
											  </div>
											</div>
									<div class="col-md-3 col-sm-12 col-xs-12">
									  <div class="form-group">
										<select name="ddlState" id="ddlState" class="d-flex form-control">
											<option value=''>Select State</option>
											<?php
												while($staterow = mysqli_fetch_object($getStates))
												{
													?>	
														<option <?php if(isset($_POST['ddlState']) && trim($_POST['ddlState'])!=null){echo $_POST['ddlState']==$staterow->id?'selected':'';} ?> value='<?php echo $staterow->id ?>'><?php echo $staterow->state_name ?></option>
													<?php
												}
											?>
										</select>
									  </div>
									</div>
									
									<?php
									if(($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0) || $_SESSION['userType']==$CTL)
									{
										?>
											<div class="col-md-3 col-sm-12 col-xs-12">
												<div class="form-group">
													<select name="ddlAgentCheck" id="ddlAgentCheck" class="d-flex form-control">
														<option value=''>Select Agent</option>
														<?php
														$getAgents = getUsersofCallcenter($u_cid,$CA);
														$agents_array = array();
														while($arow = mysqli_fetch_object($getAgents))
														{
															?>	
																<option <?php if(isset($_POST['ddlAgentCheck'])){echo $_POST['ddlAgentCheck']==$arow->uid?'selected':'';} ?> value='<?php echo $arow->uid ?>'><?php echo $arow->first_name ?></option>
															<?php
															$agents_array[$arow->uid] = $arow->first_name.' '.$arow->last_name;
														}
														?>
													</select>
												</div>
											</div>
											<?php
											if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
											{
												?>
													<div class="col-md-3 col-sm-12 col-xs-12">
														<div class="form-group">
															<select name="ddlTeamLeadCheck" id="ddlTeamLeadCheck" class="d-flex form-control">
																<option value=''>Select Team Lead</option>
																<?php
																$getTeamLeads = getUsersofCallcenter($u_cid,$CTL);
																$team_lead_array = array();
																while($tlrow = mysqli_fetch_object($getTeamLeads))
																{
																	?>	
																		<option <?php if(isset($_POST['ddlTeamLeadCheck'])){echo $_POST['ddlTeamLeadCheck']==$tlrow->uid?'selected':'';} ?> value='<?php echo $tlrow->uid ?>'><?php echo $tlrow->first_name ?></option>
																	<?php
																	$team_lead_array[$tlrow->uid] = $tlrow->first_name.' '.$tlrow->last_name;
																}
																?>
															</select>
														</div>
													</div>
												<?php
											}
									}
									?>
									
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtAccidentDateSearchFrom"])) echo $_POST["txtAccidentDateSearchFrom"] ?>" class="form-control" placeholder='Accident Date From' id="txtAccidentDateSearchFrom" name="txtAccidentDateSearchFrom" type="text" />
										</div>
									</div>
									
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtAccidentDateSearchTo"])) echo $_POST["txtAccidentDateSearchTo"] ?>" class="form-control" placeholder='Accident Date To' id="txtAccidentDateSearchTo" name="txtAccidentDateSearchTo" type="text" />
										</div>
									</div>
									
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtFromDate"])) echo $_POST["txtFromDate"] ?>" class="form-control" placeholder='From' id="txtFromDate" name="txtFromDate" type="text" />
										</div>
									</div>
									<div class="col-md-3 col-sm-8 col-xs-12">
										<div class="form-group">
											<input value="<?php if(isset($_POST["txtToDate"])) echo $_POST["txtToDate"] ?>" class="form-control" placeholder='To' id="txtToDate" name="txtToDate" type="text" />
											<input value="ajaxgetDeletedLeads.php" class="form-control" id="hfAjaxPage" name="hfAjaxPage" placeholder="" type="hidden" />
										</div>
									</div>
									<?php
									if($_SESSION['userType']==$MSW || $_SESSION['userType']==$VA)
									{
										?>
											<div class="col-md-3 col-sm-8 col-xs-12">
												<div class="form-group">
													<?php
													for($ms = 1;$ms<=3;$ms++)
													{
														?>
															<label class="mr-2" for="chckCheckList<?php echo $ms ?>"><input class="checkList_checkbox" type="checkbox" value="<?php echo $ms ?>" name="chckCheckList[]" id="chckCheckList<?php echo $ms ?>" />&nbsp; 
																<?php
																if($ms==1)
																{
																	echo "Claim Information Gathered, Verified and Updated";
																}
																else if($ms==2)
																{
																	echo "CTP Medical Certificate Received";
																}
																else if($ms==3)
																{
																	echo "Police Event Number Received";
																}
																?>
															</label>
														<?php
													}
													?>
												</div>
											</div>
										<?php
									}
									?>
									<input value="updation_date" class="form-control" name="ddlSortBy" id="ddlSortBy"  type="hidden" />
									
									<div class="col-md-12 col-sm-12 col-xs-12">
										<input type="button" onclick="getPagination(1)" class="btn btn-primary" value="Search" name="btnAdd" id="btnAdd" />
										<input type="button" class="btn btn-warning" value="Reset" name="btnReset" id="btnReset" onclick="window.location='deleted-leads.php'" />
										<input value="[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]" class="col_array" type="hidden" autocomplete="0.9714395105092326">
											<input type="button" class="btn btn-success" value="Export to Excel" name="btnExportToExcel" id="btnExportToExcel">
									</div>
								</div>
							</form>
						</div>
					</div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<div class="row">
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="change-password">
							<div class="card">
								<div class="card-body">							
									<div class="table-responsive viewLead viewLeadDeleted ajaxClass"></div>
								</div>
							</div>
						</div>
					
						</div>
					</div>
					<!-- /.row -->
				</div>

			</div>
				<?php include('../includes/copyright.php'); ?>
			<!-- Scroll to Top Button-->
			<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
				<i class="ti-angle-double-up"></i>
			</a>
			<?php include('../includes/web_footer.php'); ?>
			<script src="<?php echo $mysiteurl ?>js/jquery.table2excel.js"></script>
			<script type="text/javascript">
				$(document).ready(function() 
				{
					var delay = 1000;
						setTimeout(function() {
						 getPagination(1);
						}, delay);
				});
			</script>
		</body>
	</html>
	<?php
}
