<?php
include('../includes/basic_auth.php');

$con='';

if(isset($_POST['hfLeadId']) && trim($_POST['hfLeadId'])!=0)
{
	if(isset($_SESSION['userType']) && trim($_SESSION['userType'])==$MSW)
	{
		// $datamaster = $mysqli->prepare("select comments from lead_comments where lead_id=? and (lead_transfer_type='LT' or lead_transfer_type='RJ')" .$con ."order by lead_comments.creation_date desc limit 1");
		$datamaster = $mysqli->prepare("select last_comment from leads where id = ?");
		$datamaster->bind_param('i', $_POST['hfLeadId']);
		$datamaster->execute();
		$datamaster->bind_result($c);
		$datamaster->fetch();
		$datamaster->close();
	}

}

?>

<textarea required="required" class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter details if any" name='txtSaveComment' id='txtSaveComment'><?php echo strip_tags($c) ?></textarea>