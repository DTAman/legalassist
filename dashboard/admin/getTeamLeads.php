<?php
	include('../includes/basic_auth.php');
	
	if(isset($_POST['ddlCallCenterId']) && trim($_POST['ddlCallCenterId'])!=null && isset($_POST['hfLeadId']) && trim($_POST['hfLeadId'])!=null)
	{
		$comm_to = getLastLeadCommentUserByType($_POST['hfLeadId'],'CTL');
		?>
		<label class="teamleads_label">Select Team Lead For Callcenter:</label>
		<select name="ddlGetTeamLeads" id="ddlGetTeamLeads" class="form-control">
		<?php	
			$query = "SELECT first_name,middle_name,last_name, tl_and_agents.uid FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid where login_master.is_deleted=0 and login_master.type='CTL' and callcenter_id = ".$_POST['ddlCallCenterId']." order by login_master.uid desc";
			$getData = getQuery($query);
			while($srow = mysqli_fetch_object($getData))
			{
				?>
					<option <?php echo $comm_to==$srow->uid?'selected':'' ?> value='<?php echo $srow->uid ?>'><?php echo $srow->first_name." ".$srow->middle_name." ".$srow->last_name ?></option>
				<?php
			}
			?>
		</select>
		<?php
	}
?>