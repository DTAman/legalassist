<?php
include('../includes/basic_auth.php');

if($_SESSION['userType']==$MSW || $_SESSION['userType']==$CTL || ($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0))
{
	if(isset($_POST['chckDeleteMultiple']) && count($_POST['chckDeleteMultiple'])>0)
	{
		$chckDeleteMultiple_array = array();
		foreach($_POST['chckDeleteMultiple'] as $chckDeleteMultiple_id)
		{
			array_push($chckDeleteMultiple_array,$chckDeleteMultiple_id);
		}
		
		if(count($chckDeleteMultiple_array)>0)
		{
			if($_SESSION['userType']==$MSW)
			{
				multideleteLead($chckDeleteMultiple_array);
			}
			else
			{
				$thisdatee = $thisdate;
				if($_SESSION['userType']=='CCA')
				{
					$datamaster = $mysqli->prepare("update leads set del_by_cc = 1, del_by_cc_time = ?, del_cc_id = ? where id in (".implode(',',$chckDeleteMultiple_array).")");
				}
				else
				{
					$datamaster = $mysqli->prepare("update leads set del_by_tl = 1, del_by_tl_time = ?, del_tl_id = ? where id in (".implode(',',$chckDeleteMultiple_array).")");
				}
				$datamaster->bind_param("si",$thisdatee,$_SESSION['userId']);
				$datamaster->execute();
				$datamaster->close();
			}
		}
	}
}
?>
