<?php
	include('../includes/basic_auth.php');
	
	require 'PHPMailer-master/src/Exception.php';
	require 'PHPMailer-master/src/PHPMailer.php';
	require 'PHPMailer-master/src/SMTP.php';

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	
	function mail_phpmailer($mailEmail, $mailFrom, $mailSubject, $mailBody, $template_type, $has_attachments)
	{
		$mail = new PHPMailer(true);
		
		try 
		{
			// $mail->SMTPDebug = 0;    
			// $mail->isSMTP();                         
			// $mail->Host = 'smtp-relay.gmail.com'; 
			// $mail->Host = 'smtp.gmail.com'; 
			// $mail->SMTPAuth   = true;         
			// $mail->Username = "info@thelegalassistgroup.com";
			// $mail->Password = "Legal@123";
			// $mail->SMTPSecure = 'tls'; 
			// $mail->Port       = 587;      
			
			
			$mail->SMTPDebug = 1;                                       // Enable verbose debug output
			$mail->isSMTP();                                            // Set mailer to use SMTP
			$mail->Host = 'smtp-relay.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username = "info@injuryassisthelpline.com";
			$mail->Password = "BlackStar@6328";                       // SMTP password
			$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
			$mail->Port       = 587;      
			$mail->setFrom($mailFrom, 'Injury Assist');
			$mail->addAddress($mailEmail, 'Injury Assist'); 
			if(count($has_attachments)>0)
			{
				foreach($has_attachments as $attachment_path)
				{
					echo $attachment_path;
					$mail->addAttachment($attachment_path);
				}				
			}
			// $mail->addCC('amanjot.kaur780@gmail.com');
			$mail->isHTML(true);
			$mail->Subject = $mailSubject;
			$mail->Body    = $mailBody;
			$mail->send();
		} 
		catch (Exception $e) 
		{
			echo "Message could not be sent now. Mailer Error: {$mail->ErrorInfo}";
		}
	}
	
	$state_id = null;
	$xx = null;
	$yy = null;
	if(isset($_POST['hfLeadIdEmail']) && trim($_POST['hfLeadIdEmail'])!=null && isset($_POST['hfLeadEmail']) && trim($_POST['hfLeadEmail'])!=null && isset($_POST['ddlEmailTemplates']) && trim($_POST['ddlEmailTemplates'])!=null)
	{
		$has_attachments_array = array();
		$email_templates = emailTemplates($_SESSION['userType'],' and email_templates.eid = '.$_POST['ddlEmailTemplates']);
		$em = mysqli_fetch_object($email_templates);
	
		if($em->has_attachments==1)
		{
			if(isset($_POST['ddlEmailState']) && trim($_POST['ddlEmailState'])!=null)
			{
				$state_id = $_POST['ddlEmailState'];
			}

			if($state_id!=null)
			{
				$sname = getTimeZone($state_id);
				$xx  = "<br/><span class='emailClassL'>State: </span> ".$sname;
				$yy = " (".$sname.")";
				
				$emailTemplatesAttachments = emailTemplatesAttachments(" and state_id = ".$state_id." and template_flag_check = ".$em->template_flag);
				while($ema = mysqli_fetch_object($emailTemplatesAttachments))
				{
					array_push($has_attachments_array,$ema->path);
				}
			}
		}
		
		$comment_status = 'PN';
		$lead_transfer_type = 'LT';
		
		$loginmaster = $mysqli->prepare("update lead_comments set status='DN' where lead_id=?");
		$loginmaster->bind_param("i",$_POST['hfLeadIdEmail']);
		$loginmaster->execute();
		$loginmaster->close();
		
		$comments="<span class='emailClassL'>Template: </span> ".$em->subject;
		$comments.="<br/><span class='emailClassL'>Email: </span> ".$_POST['hfLeadEmail'];
		if($state_id!=null)
		{
			$comments.=$xx;
		}

		$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type, email_sent) VALUES (?,?,?,?,?,?,?,?,?)");
		$loginmaster->bind_param("iisissssi",$_POST['hfLeadIdEmail'],$_SESSION['userId'],$_SESSION['userType'],$_SESSION['userId'],$comments,$comment_status,$thisdate,$lead_transfer_type,$_POST['ddlEmailTemplates']);
		$loginmaster->execute();
		$loginmaster->close();
		
		$latest_comment = 'Email Sent, ';
		$latest_comment.="Email : ".$_POST['hfLeadEmail'];
		if($state_id!=null)
		{
			
			$latest_comment.=$yy;
		}
		$latest_comment.=", Template: ".$em->subject;
		updateLastComment($_POST['hfLeadIdEmail'],$latest_comment,$_SESSION['userType'],$_SESSION['userType']);
		
		mail_phpmailer($_POST['hfLeadEmail'],$em->email_from,$em->subject,$em->email_body,$_POST['ddlEmailTemplates'], $has_attachments_array);
	}
?>