<div id="rejectApprovalModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Pull Lead from Sol</h4>
			</div>
			<form action='' method='post' name='frmRejectApprovalModal' id='frmRejectApprovalModal'>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<!--<select required="required" name="ddlAssignToCallcenter" id="ddlAssignToCallcenter" class="form-control"></select>-->
								<textarea required="required" class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter details if any" name='txtSaveCommentCallcenter' id='txtSaveCommentCallcenter'></textarea>
								<input class="form-control" value="0" name="hfLeadIdCommentCallcenter" id="hfLeadIdCommentCallcenter" type="hidden" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnCommentCallcenter" name="btnCommentCallcenter" type="submit" class="btn btn-primary" value='Pull By Admin' />
				</div>
			</form>
		</div>
	</div>
</div>