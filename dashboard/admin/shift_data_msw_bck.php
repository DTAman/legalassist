<?php 
include('../includes/basic_auth.php'); 

if(!isset($_SESSION['userType']) || $_SESSION['userType']!=$MSW)
{
	header("location:index.php");
}

//Shifting Callcenter

if(isset($_POST["btnShift"]))
{
	if(trim($_POST["ddlShiftOf"])!=null && trim($_POST["ddlShiftTo"])!=null && trim($_POST["txtComments"])!=null)
	{
		if($_POST["ddlShiftOf"]!=$_POST["ddlShiftTo"])
		{
			$u_comments='Shifting';
			$u_status='DN';
			$u_lead_transfer_type='LT';
			$is_shifted=1;
			
			$loginmaster = $mysqli->prepare("UPDATE lead_comments set status='DN' where lead_id in(select id from leads where callcenter_id = ?)");
			$loginmaster->bind_param("i",$_POST['ddlShiftOf']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("INSERT INTO shift_master(shift_data_of_user, shift_data_to_user, shift_data_usertype, comments, shifted_by, creation_date, callcenter_id) VALUES (?,?,'CCA',?,?,?,?)");
			$loginmaster->bind_param("iisisi",$_POST['ddlShiftOf'],$_POST['ddlShiftTo'],$_POST['txtComments'],$_SESSION['userId'],$thisdate,$_POST['ddlShiftOf']);
			$loginmaster->execute();
			$loginmaster->close();
			
			$shift_id = mysqli_insert_id($mysqli);
			
			$loginmaster = $mysqli->prepare("select is_affliated from callcenter where uid = ?");
			$loginmaster->bind_param("i",$_POST['ddlShiftOf']);
			$loginmaster->execute();
			$loginmaster->bind_result($is_affliated);
			$loginmaster->fetch();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("select is_affliated from callcenter where uid = ?");
			$loginmaster->bind_param("i",$_POST['ddlShiftTo']);
			$loginmaster->execute();
			$loginmaster->bind_result($other_is_affliated);
			$loginmaster->fetch();
			$loginmaster->close();
			
			$loginmaster = $mysqli->prepare("select uid from login_master where added_by = ? and type='CTL' and is_deleted = 0");
			$loginmaster->bind_param("i",$_POST['ddlShiftTo']);
			$loginmaster->execute();
			$loginmaster->bind_result($new_tl);
			$loginmaster->fetch();
			$loginmaster->close();
			
			if($is_affliated==0)
			{			
				//Get any random TL
				$loginmaster = $mysqli->prepare("select uid from login_master where added_by = ? and type='CTL' and is_deleted = 0");
				$loginmaster->bind_param("i",$_POST['ddlShiftOf']);
				$loginmaster->execute();
				$loginmaster->bind_result($random_tl);
				$loginmaster->fetch();
				$loginmaster->close();
				
				//Step 1: Shift all Leads to TL which are assigned to agent currently
				$loginmaster = $mysqli->prepare("update leads set team_lead_assigned = ?, assigned_usertype='CTL' where callcenter_id = ? and (assigned_usertype is null or assigned_usertype = 'CA')");
				$loginmaster->bind_param("ii",$random_tl, $_POST['ddlShiftOf']);
				$loginmaster->execute();
				$loginmaster->close();
				
				//Step 2: Shift all Leads to callcenter which are assigned to Team Lead
				$loginmaster = $mysqli->prepare("update leads set assigned_usertype='CCA' where assigned_usertype='CTL' and callcenter_id = ?");
				$loginmaster->bind_param("i",$_POST['ddlShiftOf']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			
			//Step 3: Maintain logs for the leads going to be shifted
			$loginmaster = $mysqli->prepare("INSERT INTO shift_details(lead_id,shift_id) SELECT id,? FROM leads WHERE callcenter_id = ?");
			$loginmaster->bind_param("ii",$shift_id, $_POST['ddlShiftOf']);
			$loginmaster->execute();
			$loginmaster->close();
			
			//Step 4: Shift leads to new callcenter
			$column_name=$other_is_affliated==0?'assign_n_callcenter':'assign_affliated';
			
			$loginmaster = $mysqli->prepare("update leads set shifted_by_usertype='S_CCA',shift_id=?,".$column_name." = ? ,callcenter_id = ?, team_lead_assigned = NULL, updation_date = ?, is_shifted = 1 WHERE callcenter_id = ?");
			$loginmaster->bind_param("isisi",$shift_id,$thisdate, $_POST['ddlShiftTo'],$thisdate,$_POST['ddlShiftOf']);
			$loginmaster->execute();
			$loginmaster->close();
			
			//Step 5: Check if new callcenter is affiliated.. i.e no TL and agents
			$new_logged_user = $other_is_affliated==0?$new_tl:$_POST['ddlShiftTo'];
			if($other_is_affliated==0)
			{
				$u_comment_type='CTL';
				
				$loginmaster = $mysqli->prepare("update leads set assign_teamlead = ?, team_lead_assigned = ?, updation_date = ? where shift_id = ?");
				$loginmaster->bind_param("sisi",$thisdate,$new_tl, $thisdate, $shift_id);
				$loginmaster->execute();
				$loginmaster->close();
				
				$loginmaster = $mysqli->prepare("update leads set assigned_usertype='CTL' WHERE callcenter_id = ? and is_shifted = 1 and assigned_usertype='CCA'");
				$loginmaster->bind_param("i",$_POST['ddlShiftTo']);
				$loginmaster->execute();
				$loginmaster->close();
			}
			else
			{
				$u_comment_type='CCA';		
			}
			
			$loginmaster = $mysqli->prepare("update leads set outcome='Lead Shifted',outcome_type='LS' WHERE shift_id = ? and assigned_client is NULL");
			$loginmaster->bind_param("i",$shift_id);
			$loginmaster->execute();
			$loginmaster->close();	
			
			$loginmaster = $mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type,is_shift) select id,?,?,NULL,?,?,?,?,? from leads where shift_id = ?");
			$loginmaster->bind_param("isssssii",$new_logged_user,$u_comment_type,$u_comments,$u_status,$thisdate,$u_lead_transfer_type,$is_shifted,$shift_id);
			$loginmaster->execute();
			$loginmaster->close();
			
			$_SESSION['success']='Data shifted successfully.';
			header( "location:total_shift_data_reports.php");			
		}
		else
		{
			$_SESSION['error']="Data cannot be shifted back to the same user. Select other user to shift data.";
		}
	}
	else
	{
		$_SESSION['error']="Enter all the required fields.";
	}
}

$callcenters = getCallcenters('');
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('../includes/header.php'); ?>
	</head>
	<body class="fixed-nav sticky-footer" id="page-top">
		<?php include('../includes/navigation.php'); ?>
		<div class="content-wrapper">

			<div class="container-fluid">
			
				<!-- Title & Breadcrumbs-->
				<div class="row page-titles" style="margin-bottom:0;">
					<div class="col-md-12 align-self-center">
						<h4 class="theme-cl">Shift Data</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="change-password">
						<div class="card">
							<div class="card-body">
								<form name='frmShiftData' id='frmShiftData' action='' method='post'>
									<?php include("../includes/messages.php") ?>
									 <div class="row"> 
										 <div class="col-md-6 col-sm-12 col-xs-12">
											 <div class="form-group">
												<label for="ddlShiftOf">Shift Data Of</label>
												<select name="ddlShiftOf" id="ddlShiftOf" class="d-flex form-control" required>
													<option value=''>Select Callcenter</option>
													<?php
														while($c1 = mysqli_fetch_object($callcenters))
														{
															?>	
																<option value='<?php echo $c1->uid ?>'><?php echo $c1->centre_name ?></option>
															<?php
														}
													?>
												</select>
											 </div>
										</div> 
										<div class="col-md-6 col-sm-12 col-xs-12">
											 <div class="form-group">
												<label for="ddlShiftTo">Shift Data To</label>
												<select name="ddlShiftTo" id="ddlShiftTo" class="d-flex form-control" required>
													<option value=''>Select Callcenter</option>
													<?php
														mysqli_data_seek($callcenters,0);
														while($c2 = mysqli_fetch_object($callcenters))
														{
															?>	
																<option value='<?php echo $c2->uid ?>'><?php echo $c2->centre_name ?></option>
															<?php
														}
													?>
												</select>
											 </div>
										</div> 
									</div> 
									
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="form-group fullwidthTextarea">
												<textarea required maxlength='300' class="form-control mt-3" rows="4" style='width:100%' placeholder="Comments" name='txtComments' id='txtComments'></textarea>
											</div>
										</div>
									 </div>
									<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<input name="btnShift" id="btnShift" type="submit" class="btn btn-primary ml-0 mr-3 mt-2" value="Shift Now" />
											</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				
					</div>
				</div>
				<!-- /.row -->
			</div>

		</div>
			<!-- /.content-wrapper -->
				<?php include('../includes/copyright.php'); ?>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded cl-white gredient-bg" href="#page-top">
			<i class="ti-angle-double-up"></i>
		</a>
		<?php include('../includes/web_footer.php'); ?>
	  
	</body>
</html>
