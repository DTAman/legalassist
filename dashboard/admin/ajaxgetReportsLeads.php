<?php
include('../includes/basic_auth.php'); 

$p=1;
$pagecount=1;
$last_page=1;
$first_page=0;
$offset=0;

$orderby='updation_date';

$condition=" ";

if($_SESSION['userType']!=$MSW)
{
	$condition.=" and admin_assigned_directly=0 ";
}

/********************************************************************************************/
if($_SESSION['userType']==$CA || $_SESSION['userType']==$CTL)
{
	$datamaster = $mysqli->prepare("SELECT callcenter_id FROM tl_and_agents where uid = ?");
	$datamaster->bind_param('i',$_SESSION['userId']);
	$datamaster->execute();
	$datamaster->bind_result($u_cid);
	$datamaster->fetch();
	$datamaster->close();
}
else if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
{
	$u_cid = $_SESSION['userId'];
}
/********************************************************************************************/

$team_lead_array = array();
$agents_array = array();
$client_array = array();
$callcenter_array = array();
$verifier_array = array();

if(isset($u_cid))
{
	$getAgents = getUsersofCallcenter($u_cid,$CA);
	$getTeamLeads = getUsersofCallcenter($u_cid,$CTL);
}
$getClients = getClients();
$getCallcenters = getCallcenters('');
$getVerifiers = getVerifiers('');

if(isset($u_cid))
{
	while($tlrow = mysqli_fetch_object($getTeamLeads))
	{
		$team_lead_array[$tlrow->uid] = $tlrow->first_name.' '.$tlrow->last_name;
	}
	while($arow = mysqli_fetch_object($getAgents))
	{
		$agents_array[$arow->uid] = $arow->first_name.' '.$arow->last_name;
	}
}
while($vcrow = mysqli_fetch_object($getVerifiers))
{
	$verifier_array[$vcrow->uid] = $vcrow->verifier_name;
}
while($clientrow = mysqli_fetch_object($getClients))
{
	$client_array[$clientrow->uid] = $clientrow->company_name;
}
while($ccrow = mysqli_fetch_object($getCallcenters))
{
	$callcenter_array[$ccrow->uid] = $ccrow->centre_name;
}

$usertype = $_SESSION["userType"];

if(isset($_POST['page_skip']))
{
	$offset=($_POST['page_skip']-1)*$limit;
	$n = $offset+1;
}
else
{
	$_POST['page_skip']=0;
}

if($_SESSION['userType']==$C)
{
	$condition.=" and (client_status='IP' || client_status='SI' || client_status='AP') ";
	$condition.=" and assigned_client = ".$_SESSION['userId'];
}
else if($_SESSION['userType']==$CCA)
{
	$_POST['ddlLCallcenters'] = $_SESSION['userId'];
	$condition.=" and (client_status='IP' || client_status='SI' || client_status='AP') ";
	$condition.=" and leads.callcenter_id = ".$_POST['ddlLCallcenters'];
}

if(trim($_POST['txtSearch'])!=null)
{
	$condition.=" and (phone like '%{$_POST['txtSearch']}%' or email like '%{$_POST['txtSearch']}%' or first_name like '%{$_POST['txtSearch']}%' or middle_name like '%{$_POST['txtSearch']}%' or last_name like '%{$_POST['txtSearch']}%') ";
}

if(trim($_POST['txtFromDate'])!=null)
{
	$create_date=date('Y-m-d',strtotime($_POST['txtFromDate']));
	$condition.=" and date(updation_date) >= '".get_timezone_offset_calender($create_date,'UTC',$_SESSION['userTimeZone'])."'";
}

if(trim($_POST['txtToDate'])!=null)
{
	$to_date=date('Y-m-d',strtotime($_POST['txtToDate']));
	$condition.=" and date(updation_date) <= '".get_timezone_offset_calender($to_date,'UTC',$_SESSION['userTimeZone'])."'";
}

if(trim($_POST['txtAccidentDateSearchFrom'])!=null && trim($_POST['txtAccidentDateSearchTo'])!=null)
{
	$ac_date_from=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchFrom']));
	$ac_date_to=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchTo']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) >= '".$ac_date_from."' and date(accident_date_time) <= '".$ac_date_to."' and lead_accidents.status=1)) ";
}
else if(trim($_POST['txtAccidentDateSearchFrom'])!=null)
{
	$ac_date_from=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchFrom']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) >= '".$ac_date_from."' and lead_accidents.status=1)) ";
}
else if(trim($_POST['txtAccidentDateSearchTo'])!=null)
{
	$ac_date_to=date('Y-m-d',strtotime($_POST['txtAccidentDateSearchTo']));
	$condition.=" and (leads.id in (select lead_id from lead_accidents where date(accident_date_time) <= '".$ac_date_to."' and lead_accidents.status=1)) ";
}

if(trim($_POST['ddlState'])!=null)
{
	$condition.=" and leads.state_id = ".$_POST['ddlState'];
}

if(isset($_POST['ddlLCallcenters']) && trim($_POST['ddlLCallcenters'])!=null)
{
	$condition.=" and leads.callcenter_id = ".$_POST['ddlLCallcenters'];
}

if(isset($_POST['ddlClients']) && count(array_filter($_POST['ddlClients']))>0)
{
	$condition.=" and assigned_client in (".trim(implode(',',$_POST['ddlClients']),',').")";
}

if(isset($_POST['ddlTeamLeads']) && trim($_POST['ddlTeamLeads'])!=null)
{
	// $condition.=" AND team_lead_assigned =  ".$_POST['ddlTeamLeads'] ." and callcenter_id = ".$u_cid;
	$condition.=" AND team_lead_assigned =  ".$_POST['ddlTeamLeads'];
}

if(isset($_POST['ddlAgents']) && trim($_POST['ddlAgents'])!=null)
{
	// $condition.=" AND agent_assigned =  ".$_POST['ddlAgents'] ." and callcenter_id = ".$u_cid;
	$condition.=" AND agent_assigned =  ".$_POST['ddlAgents'];
}
if(isset($_POST['ddlWasRejected']) && trim($_POST['ddlWasRejected'])==1)
{
	$condition.=" and (leads.id in (select lead_id from lead_comments where lead_transfer_type = 'RJ')) ";
}
if(isset($_POST['ddlWasAssignedToVerifier']) && trim($_POST['ddlWasAssignedToVerifier'])!=null)
{
	$condition.=" and (leads.id in (select lead_id from lead_comments where comment_to = ".$_POST['ddlWasAssignedToVerifier'].")) ";
}

$outcome_array = array();
$c1='';
$c2='';
if(isset($_POST['ddlLeadStatus']) && count($_POST['ddlLeadStatus'])>0)
{
	foreach($_POST['ddlLeadStatus'] as $ddlLeadStatus)
	{
		if(trim($ddlLeadStatus)!=null)
		{
			array_push($outcome_array,"'".$ddlLeadStatus."'");
		}
	}
	
	if(count($outcome_array)>0)
	{
		$c1.=" leads.outcome_type in (".implode(',',$outcome_array).") ";
	}
		
	if(trim($c1)!=null || trim($c2)!=null)
	{
		$condition.=" and (".$c1.$c2.")";
	}
}

if(isset($_POST['ddlSubUrb']) && trim($_POST['ddlSubUrb'])!=null)
{
	$condition.=" and leads.suburb_id = ".$_POST['ddlSubUrb'];
}

if(trim($_POST['ddlSortBy'])!=null)
{
	$orderby = $_POST['ddlSortBy'];
}

if(isset($_POST['nextpage']) && trim($_POST['nextpage'])!=null)
{
	$pagecount = $_POST['nextpage'];
	$last_page = $_POST['nextpage'];
}

if(isset($_POST['firstpage']) && trim($_POST['firstpage'])!=null)
{		
	$pagecount = $_POST['firstpage']-($set_count-1);
	$last_page = $_POST['firstpage']-($set_count-1);
}

if(isset($_POST['firstpage1']) && trim($_POST['firstpage1'])!=null)
{
	$firstpage1 = trim($_POST['firstpage1']);
	
	$pagecount = $firstpage1;
	$last_page = $firstpage1;
}

$queryCount= "select count(*) from leads left outer join states on states.id = leads.state_id left outer join suburbs on suburbs.id = leads.suburb_id where leads.is_deleted=0 ".$condition;
$queryResults = "select leads.*, suburbs.suburb_name as drop_suburb,leads.suburb_name as textbox_suburb,state_name, initials from leads left outer join states on states.id = leads.state_id left outer join suburbs on suburbs.id = leads.suburb_id where leads.is_deleted=0 ".$condition." order by ".$orderby." desc limit ".$limit." offset ".$offset;
$totalCount = getFetchQuery($queryCount);
$totalResults = getQuery($queryResults);

if(mysqli_num_rows($totalResults)>0)
{
	?>
	<div class="hh5">
		<table class="table newTable table-bordered">
			<thead>
				<tr>
					<th class='text-center'>Sr.No.</th>
					<th style="min-width:100px!important;width:100px!important">Lead Id</th>
					<th style="min-width:300px!important;width:300px!important;">Name &amp; Details</th>
					<?php
					if($_SESSION['userType']=='CA' || $_SESSION['userType']=='CTL' )
					{
						?>
							<th class="widthCustom">Email</th>
							<th class="widthCustom">Suburb</th>
							<th>Address</th>
						<?php
					}
					?>
					<th style="max-width:300px;width:300px">Lead Details</th>
					<?php
					if(($_SESSION['userType']==$VA) || ($_SESSION['userType']==$MSW) || ($_SESSION['userType']==$SA) || ($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0) || $_SESSION['userType']==$CTL)
					{
						?>
							<th style="max-width:200px;width:200px">Users</th>
						<?php
					}
					
					?>
					<th style="max-width:200px;width:200px">Modified&nbsp;On</th>
					<th class='text-center'>View</th>
				</tr>
			</thead>
			<tbody>
			<?php
			while($rc = mysqli_fetch_object($totalResults))
			{
				?>
					<tr id="delete<?php echo $rc->id ?>">
						<td class='text-center'><?php echo $n ?></td>
						<td style="min-width:100px!important;width:100px!important" class='text-center'>
							<?php echo $rc->id ?>
							<br/><span class="badge badge-danger"><?php echo $_SESSION['userType']==$MSW && $rc->show_to_admin==0?'Deleted':''?></span>
						</td>
						<td style="min-width:200px!important;">
							<?php echo $rc->first_name.' '.$rc->middle_name.' '.$rc->last_name ?>
							<span style="color:red"><?php echo $rc->initials!=null?'('.$rc->initials.')':'' ?></span>
							<br/>
							<?php echo $rc->phone ?>
							<?php
								if($rc->last_accident_type!=null)
								{
									if($rc->last_accident_na!=1)
									{
										echo "<br/>".$rc->last_accident_type." <br/><span style='color:red'>(".date('d-M-Y',strtotime($rc->latest_accident_dt)).")</span>";
									}
									else
									{
										echo "<br/>".$rc->last_accident_type." <br/><span style='color:red'>(".date('M-Y',strtotime($rc->latest_accident_dt)).")</span>";
									}
								}
								
							?>
						</td>
						<?php
						if($_SESSION['userType']=='CA' || $_SESSION['userType']=='CTL' )
						{
							?>
								<td class="widthCustom"><?php echo $rc->email ?></td>
								<td class="widthCustom"><?php echo $rc->drop_suburb.$rc->textbox_suburb ?></td>
								<td><?php echo $rc->address ?></td>
							<?php
						}	
						?>
						<td style="max-width:200px">
							
							<?php
							if($_SESSION['userType']!='CA' && $_SESSION['userType']!='CTL' )
							{
								$getSignUpDetails = str_replace('"','',$rc->latest_signup_detail);
								$x='';
								if(trim($getSignUpDetails)!=null)
								{
									if($rc->latest_signuptype=='E' || $rc->latest_signuptype=='P')
									{
										$x= $rc->latest_signuptype=='E'?'Email':'Post';
										$x.=", <span style='color:red'>Call: ".date("d M,Y",strtotime($rc->latest_signup_datetime))." (".$rc->latest_signup_timezone.")</span>, Comments: ".$getSignUpDetails; 
										
										$x = '<p data-toggle="tooltip" data-original-title="'.trim(strip_tags($x)).'">'.$x.'</p>';
									}
									else if($rc->latest_signuptype=='O' || $rc->latest_signuptype=='H')
									{
										$x = $rc->latest_signuptype=='O'?'Office Signup':'Home Signup';
										$x.=", <span style='color:red'>Call: ".date("d M,Y H:i",strtotime($rc->latest_signup_datetime))." (".$rc->latest_signup_timezone.")</span>, Comments: ".$getSignUpDetails; 
										
										$x = '<p data-toggle="tooltip" data-original-title="'.trim(strip_tags($x)).'">'.$x.'</p>';
									}
									
									echo '<button id="'.$rc->id.'signup" class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" a_val="'.strip_tags(preg_replace('/\s+/', ' ',$x)).'" onclick="sign_val_copy('.$rc->id.')"></button>'.$x;
								}
							}
							?>
							<div class="alert-success"><?php echo $rc->outcome ?></div>
							<?php
							$com = str_replace('"','',$rc->last_comment);
							if(trim($com)!=null)
							{
								?>
									<button class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" onclick="copyClipbord('<?php echo preg_replace('/\s+/', ' ', $com) ?>')"></button>
									<p data-toggle="tooltip" data-original-title="<?php echo $com ?>"><span class="a_red">Updates: </span><?php echo $com  ?></p>
								<?php
							}
							?>
						</td>

						<?php
						if(($_SESSION['userType']==$VA) || ($_SESSION['userType']==$MSW) || ($_SESSION['userType']==$SA) || ($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0) || $_SESSION['userType']==$CTL)
						{
							?>
								<td style="max-width:200px;width:200px">
									<?php
										if($_SESSION['userType']==$MSW || $_SESSION['userType']==$SA)
										{
											echo isset($callcenter_array[$rc->callcenter_id])?'<span class="a_red">CC</span>: '.$callcenter_array[$rc->callcenter_id].'<br/>':'';
											echo isset($client_array[$rc->assigned_client])?'<span class="a_red">Sol</span>: '.$client_array[$rc->assigned_client].'<br/>':'';
											echo isset($verifier_array[$rc->assigned_verifier])?'<span class="a_red">'.($rc->is_previous_verifier==1?'Previous ':'').'Verifier</span>: '.$verifier_array[$rc->assigned_verifier].'<br/>':'';
										}
										if($_SESSION['userType']==$VA)
										{
											echo isset($verifier_array[$rc->assigned_verifier])?'<span class="a_red">'.($rc->is_previous_verifier==1?'Previous ':'').'Verifier</span>: '.$verifier_array[$rc->assigned_verifier].'<br/>':'';
										}
										if($_SESSION['userType']==$CCA && $_SESSION['is_affliated']==0)
										{
											echo isset($client_array[$rc->assigned_client])?'<span class="a_red">Sol</span>: '.$client_array[$rc->assigned_client].'<br/>':'';
											echo isset($team_lead_array[$rc->team_lead_assigned])?'<span class="a_red">TL</span>: '.$team_lead_array[$rc->team_lead_assigned].'<br/>':'';
											echo isset($agents_array[$rc->agent_assigned])?'<span class="a_red">Agent</span>: '.$agents_array[$rc->agent_assigned].'<br/>':'';
										}
										if($_SESSION['userType']==$CTL)
										{
											echo isset($client_array[$rc->assigned_client])?'<span class="a_red">Sol</span>: '.$client_array[$rc->assigned_client].'<br/>':'';
											echo isset($agents_array[$rc->agent_assigned])?'<span class="a_red">Agent</span>: '.$agents_array[$rc->agent_assigned].'<br/>':'';
										}
									?>
								</td>
							<?php
						}
						?>
						<td  style="max-width:200px;width:200px">
						<?php
						if($_SESSION['userType']!=$C)
						{
							switch($_SESSION['userType'])
							{
								case 'C': $d = $rc->assign_client;break;
								case 'CCA': $d = $rc->assign_n_callcenter;
											if($_SESSION['is_affliated']==1)
											{
												$d = $rc->assign_affliated;
											}
											break;
								case 'CTL': $d = $rc->assign_teamlead;break;
								case 'CA': $d = $rc->assign_agent;break;
								case 'MSW': $d = $rc->assign_admin;break;
								case 'SA': $d = $rc->assign_admin;break;
								case 'VA': $d = $rc->assign_verifier;break;
								default: $d = '';break;
							}
							if($d!=null)	
							{
								echo "<span class='a_red'>Created: </span><br/>".get_timezone_offset(date('Y-m-d H:i:s',strtotime($d)),'UTC',$_SESSION['userTimeZone']);
							}
						}
						if($_SESSION['userType']==$MSW || $_SESSION['userType']==$SA || $_SESSION['userType']==$CCA  || $_SESSION['userType']==$C)
						{
							echo "<br/><span class='a_red'>Modified: </span><br/>".get_timezone_offset(date('Y-m-d H:i:s',strtotime($rc->updation_date)),'UTC',$_SESSION['userTimeZone']);
						}
						?>
						</td>
						
						
							
							<td class='text-center'>
								<div class="icnos">
							<?php
								if($_SESSION['userType']==$rc->assigned_usertype || $_SESSION['userType']=='MSW' || ($rc->assigned_usertype==null) || $_SESSION['userType']=='C')
								{
									
										if($usertype==$MSW && $rc->client_status!='IP')
										{
											?>
												<a href="javascript:void(0)" onclick='deleteLead(<?php echo $rc->id ?>)'  class="delete icn d-flex" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
											<?php
										}
								}
							if($usertype!=$SA)
							{
								?>
								<a target='_blank' class="text-success icn" href="view-each-lead.php?u=<?php echo $rc->id ?>" title="" data-toggle="tooltip" data-original-title="View"><i class="ti-eye"></i></a>
								<?php
							}
							if($usertype==$MSW)
							{
								?>
								<a target='_blank' class="text-success icn" href="view-logs.php?u=<?php echo $rc->id ?>" title="" data-toggle="tooltip" data-original-title="View Logs"><i class="ti-bar-chart-alt"></i></a>
								<?php
							}
							if($_SESSION['userType']==$MSW || $_SESSION['userType']==$C || $_SESSION['userType']==$SA)
							{
								?>
									<a target='_blank' href='ajaxFullLead.php?u=<?php echo $rc->id ?>' class="text-danger icn" l_id = '<?php echo $rc->id ?>' title="" data-toggle="tooltip" data-original-title="Print Lead"><i class="ti-printer"></i></a>
								<?php
							}
							if($_SESSION['userType']==$MSW && $rc->assigned_usertype==$CCA && $rc->msw_lead_type=='c')
							{
								?>
									<a onclick="changeLeadWorkingStatus('f',<?php echo $rc->id ?>)" href="javascript:void(0)" class="text-success icn" l_id = '<?php echo $rc->id ?>' title="" data-toggle="tooltip" data-original-title="Move To Fresh Leads"><i class="fa fa-mail-forward"></i></a>
								<?php
							}
							?>
							</div>
						</td>
						
					</tr>
				<?php
				$n++;
			}
			?>
			</tbody>
		</table>
		</div>
		<?php include('../includes/ajax_pagination.php'); ?>
	</div>
	<?php
}
else
{
	?>
		<?php include('../includes/norows.php'); ?>
	<?php
}
?>