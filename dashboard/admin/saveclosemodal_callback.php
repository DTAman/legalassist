<?php
	$getZones = getStates();
?>
<div id="saveCloseModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Enter Callback Details</h4>
			</div>
			<form action='' method='post' name='frmSaveClose' id='frmSaveClose' onsubmit='return false'>
				<div class="modal-body">
					<div class="row">
						<?php
						
						$dr_cal='';
						$mnr_cal='';
						$yr_cal='';
						$hr_cal='';
						$mr_cal='';
						
						$ry = date('Y');
						for($dr=1;$dr<=31;$dr++)
						{
							$dr_cal.= "<option value='".$dr."'>".$dr."</option>";
						}
						for($mnr=1;$mnr<=12;$mnr++)
						{
							$mnr_cal.= "<option value='".$mnr."'>".substr(date('F', mktime(0,0,0,$mnr, 1, date('Y'))),0,3)."</option>";
						}
						for($yr=$ry;$yr<=$ry+100;$yr++)
						{
							$yr_cal.= "<option value='".$yr."'>".$yr."</option>";
						}
						for($hr=0;$hr<=23;$hr++)
						{
							$hr_cal.= "<option value='".$hr."'>".$hr."</option>";
						}
						for($mr=0;$mr<=59;$mr++)
						{
							$mr_cal.= "<option value='".$mr."'>".$mr."</option>";
						}
						?>
						<div class="col-md-12 col-sm-12">
							 <div class="calendertimeModal-wrap">
								<div>
									<select required="required" name="txtSaveCloseDay" id="txtSaveCloseDay" class="form-control">
										<option value="">Day</option><?php echo $dr_cal ?>
									</select>
								</div>
								<div>
									<select required="required" name="txtSaveCloseMonth" id="txtSaveCloseMonth" class="form-control">
										<option value="">Month</option><?php echo $mnr_cal ?>
									</select>
								</div>
								<div>
									<select required="required" name="txtSaveCloseYear" id="txtSaveCloseYear" class="form-control">
										<option value="">Year</option><?php echo $yr_cal ?>
									</select>
								</div>
								<div>
									<select required="required" name="txtSaveCloseHours" id="txtSaveCloseHours" class="form-control">
										<option value="">Hours</option><?php echo $hr_cal ?>
									</select>
								</div>
								<div>
									<select required="required" name="txtSaveCloseMinutes" id="txtSaveCloseMinutes" class="form-control">
										<option value="">Minutes</option><?php echo $mr_cal ?>
									</select>
								</div>
							</div>
						</div>
					
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<textarea class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter details if any" name='txtSaveCloseDetails' id='txtSaveCloseDetails'></textarea>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<select required name="ddlSignUpStates" id="ddlSignUpStates" class="d-flex form-control" required>
									<option value=''>Select State (Zone) For Callback</option>
									<?php
										while($zrow = mysqli_fetch_object($getZones))
										{
											?>	
												<option value='<?php echo $zrow->id ?>'><?php echo $zrow->state_name." (".$zrow->timezones.")" ?></option>
											<?php
										}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnSaveAndClose" name="btnSaveAndClose" type="submit" class="btn btn-primary" value='Save' />
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</form>
		</div>
	</div>
</div>