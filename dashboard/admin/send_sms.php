<?php

class wholesalesms {
    var $api_key;
    var $secret;
    var $end_point;
    public function wholesalesms($api_key,$secret,$end_point = 'https://app.wholesalesms.com.au/api/v2/'){
    $this->api_key = $api_key;
    $this->secret = $secret;
    $this->end_point = $end_point;
}
public function getBalance(){
    return $this->send_request('get-balance.json',[]);
}
public function sendMessage($to,$message){
    $post_data = array(
    "to" => $to,
    "message" => $message
    );
    return $this->send_request('send-sms.json',$post_data);
}
private function send_request($request,$post_data){
    $headers = array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Basic '. base64_encode("{$this->api_key}:{$this->secret}")
    );
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_URL,$this->end_point.$request);
    curl_setopt($ch, CURLOPT_POST, 1);
    
    curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($post_data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    curl_close ($ch);
    return $server_output;
}
}

//$api = new wholesalesms("API_KEY","API_SECRET");
//echo $api->getBalance();
//$response = $api->sendMessage("+64210000000","testing 123");
?>