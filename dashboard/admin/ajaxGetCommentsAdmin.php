<?php
include('../includes/basic_auth.php');

$last_rejected_by_reason='';

if(isset($_POST['hfLeadId']) && trim($_POST['hfLeadId'])!=0)
{
	if(isset($_SESSION['userType']) && trim($_SESSION['userType'])==$MSW)
	{
		//$last_rejected_by_result = lastLeadRejectedByDetails($_POST['hfLeadId']);
		
		// $datamaster = $mysqli->prepare("select comments from lead_comments where lead_id=? order by creation_date desc limit 1");
		$datamaster = $mysqli->prepare("select last_comment from leads where id = ?");
		$datamaster->bind_param('i', $_POST['hfLeadId']);
		$datamaster->execute();
		$last_rejected_by_result = $datamaster->get_result();
		$datamaster->close();
		
		if(mysqli_num_rows($last_rejected_by_result)!=0)
		{
			$lrow = mysqli_fetch_object($last_rejected_by_result);
			$last_rejected_by_reason = $lrow->last_comment;
		}
	}

}

?>

<textarea required="required" class="form-control mt-3" rows="4" style='width:100%' placeholder="Enter details if any" name='txtSaveComment' id='txtSaveComment'><?php echo strip_tags($last_rejected_by_reason) ?></textarea>