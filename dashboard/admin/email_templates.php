<div id="emailTemplate" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close removeOpenModal" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Send Email</h4>
			</div>
			<form action='' method='post' name='frmEmailTemplate' id='frmEmailTemplate'>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<input class="form-control" value="" name="hfLeadIdEmail" id="hfLeadIdEmail" type="hidden" />
								<input class="form-control" value="" name="hfLeadEmail" id="hfLeadEmail" type="email" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<select required="required" name="ddlEmailTemplates" id="ddlEmailTemplates" class="form-control" onchange="showEmailStates()">
									<option value="">Select Template</option>
									<?php
									$email_templates = emailTemplates($_SESSION['userType'],'');
									while($em = mysqli_fetch_object($email_templates))
									{
										?>
											<option mytag = <?php echo $em->has_attachments ?> value="<?php echo $em->eid?>"><?php echo $em->subject?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="row myemailclass" style="display:none">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group fullwidthTextarea">
								<select name="ddlEmailState" id="ddlEmailState" class="form-control">
									<option value="">Select State</option>
									<?php
									$getStatesEmail = getStates();
									while($semail = mysqli_fetch_object($getStatesEmail))
									{
										?>	
											<option value='<?php echo $semail->id ?>'><?php echo $semail->state_name ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="btnSendEmail" name="btnSendEmail" type="submit" class="btn btn-primary" value='Send Email' />
				</div>
			</form>
		</div>
	</div>
</div>