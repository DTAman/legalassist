<?php 
/********* AMANJOT ****************/
include('../includes/basic_auth.php');

$condition='';

if(isset($_GET['u']) && trim($_GET['u'])!=null)
{
	$lead_id = trim($_GET['u']);
	
	if(isset($_GET['a']) && trim($_GET['a'])==0)
	{
		$loginmaster = $mysqli->prepare("update leads set del_ag_id = NULL, del_by_ag = 0, del_by_ag_time = NULL  where id=?");
		$loginmaster->bind_param("i",$lead_id);
		$loginmaster->execute();
		$loginmaster->close();
				
		$_SESSION['success']='Lead has been successfully assigned. Refreshing page in 2 seconds.';	
	}
	
	header("location:view-agent-deleted-leads.php");
}
else
{
	header("location:index.php");
}
/********* AMANJOT ****************/
?>