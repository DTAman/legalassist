<?php
include('includes/basic_no_auth.php');

/* if (isset($_POST["btnLogin"]))
{
	if(trim($_POST["txtUserName"]) != null && trim($_POST["txtPassword"]) != null)
	{		
		$loginmaster = $mysqli->prepare("select user_id, signup_mobile, signup_name, signup_type, signup_password from signup_master where signup_mobile=? and signup_status='AC'");
		$loginmaster->bind_param("s", $_POST["txtUserName"]);
		$loginmaster->execute();
		$loginmaster->bind_result($signup_id, $signup_mobile, $signup_name, $signup_type, $signup_password);
		if ($loginmaster->fetch() > 0 && md5($_POST["txtPassword"]) == $signup_password)
		{
			$_SESSION["userId"] = $signup_id;
			$_SESSION["userType"] = $signup_type;
			$_SESSION["userName"] = $signup_name;
			$_SESSION["userMobile"] = $signup_mobile;
			if (strToLower($signup_type) == 0)
			{
				header("location:dashboard/users/home.php");
			}
			else if(strToLower($signup_type) == 1)
			{
				header("location:dashboard/admin/home.php");
				//header("location:dashboard/users/home.php");
			}
			else
			{
				$_SESSION['error'] = "Invalid Credentials";
			}
			$loginmaster->close();
		}
		else
		{
			$_SESSION['error'] = "Invalid Credentials";
		}
	}
	else
	{
		$_SESSION['error'] = "All fields are required.";
	}
} */
?>
<html lang="en">
	<head>
		<?php include('includes/header.php'); ?>
	</head>
	<body>
	
		<div class="container-fluid">
			<div class="row">
				<div class="hidden-xs hidden-sm col-lg-6 col-md-6 gredient-bg">
					<div class="clearfix">
						<div class="animated fadeindown logo-title-container text-center">
							<h3 class="cl-white text-upper">Welcome To</h3>
							<img class="img-responsive" src="img/logo-white.png" alt="Logo Icon">
							 
						</div> <!-- .logo-title-container -->
					</div>
				</div>

				<div class="col-12 col-sm-12 col-md-6 col-lg-6 login-sidebar animated fadeindown">

					<div class="login-container animated fadeindown">

						<h2 class="text-center text-upper">Forgot Password</h2>
						<form class="form-horizontal" action="#" name="frmForgotEmail" id="frmForgotEmail" method="post">
							<div class="form-group">
								<?php include("includes/messages.php") ?>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="txtForgotEmail" name="txtForgotEmail" placeholder="Email" />
								<i class="fa fa-user"></i>
							</div>
						
							<div class="form-group">
								<div class="flexbox align-items-center">
									<input type="submit" class="btn gredient-bg" value="Forgot Password" name='btnForgotPassword' id='btnForgotPassword' />
									<p>If You Want To Login <a href="login.php" data-toggle="tooltip" class="theme-cl" data-original-title="Login">Click Here...</a>
								</div>
							</div>
						
						</form>
					</div> 
					<!-- .login-container -->
					
				</div> <!-- .login-sidebar -->
			</div> <!-- .row -->
		</div>
		<?php include('includes/web_footer.php'); ?>
	</body>
</html>
