<?php 
	// server should keep session data for AT LEAST 1 day
	ini_set('session.gc_maxlifetime', 86400);

	// each client should remember their session id for EXACTLY 1 day
	session_set_cookie_params(86400);

	session_start(); // ready to go!
	
	//Local
	/* $mysiteurl = 'https://injuryassisthelpline.com/dashboard/';
	$db_name = 'legaldb_live_2021';
	$db_user = 'root';
	$db_pass = '';
	$db_host = '127.0.0.1'; */
	
	$mysiteurl = 'http://127.0.0.1/legal/dashboard/';	
	$db_name = 'legal_live';
	$db_user = 'root';
	$db_pass = '';
	$db_host = '127.0.0.1';
	
	$admins_email = 'info@injuryassisthelpline.com';
	
	
	$mysqli = new mysqli($db_host,$db_user,$db_pass,$db_name);
	
	if ($mysqli->connect_error) 
	{
		die("Connection failed: " . $mysqli->connect_error);
	}
	
	date_default_timezone_set('UTC');
	$thisdate=date("Y-m-d H:i:s");
	$directory='../uploads/';
	
?>