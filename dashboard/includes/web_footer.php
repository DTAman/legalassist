<div id="cover" style="display:none">
	<div style="position: fixed; top: 50%;left: 50%; font-weight: bold; color: #fff; margin-top: -70px; margin-left: -175px;">
		<div class="messsagedata">
			<div id="idMsg">
				<img src="../img/loader.gif" />
			</div>
		</div>
	</div>
</div>
<script src="<?php echo $mysiteurl ?>js/jquery.min.js"></script>
<script src="<?php echo $mysiteurl ?>js/moment-with-locales.min.js"></script>
<script src="<?php echo $mysiteurl ?>js/bootstrap-material-datetimepicker.js"></script>
<script src="<?php echo $mysiteurl ?>js/jquery.validate.js"></script>
<script src="<?php echo $mysiteurl ?>js/bootstrap.bundle.min.js"></script>
<script src="<?php echo $mysiteurl ?>js/jquery.easing.min.js"></script>
<script src="<?php echo $mysiteurl ?>js/slick.js"></script>
<script src="<?php echo $mysiteurl ?>js/jquery.slimscroll.min.js"></script>
<script src="<?php echo $mysiteurl ?>js/raphael.min.js"></script>
<script src="<?php echo $mysiteurl ?>js/morris.min.js"></script>
<script src="<?php echo $mysiteurl ?>js/dashboard.js"></script>
<script src="<?php echo $mysiteurl ?>js/custom.js"></script>
<script src="<?php echo $mysiteurl ?>includes/scripts.js"></script>
<script src="<?php echo $mysiteurl ?>includes/functions.js"></script>

<script>
function openRightMenu() 
{
	document.getElementById("rightMenu").style.display = "block";
}
function closeRightMenu() 
{
	document.getElementById("rightMenu").style.display = "none";
}

$(document).ready(function() {
	$('.dropdown-toggle').dropdown();
	setInterval(function(){setNavigationNotifications();}, 5000);	
});
</script>