<?php
$MSW = 'MSW'; //Super Admin
$CCA = 'CCA'; //Callcenter Admin
$CHR = 'CHR'; //Callcenter HR
$CTL = 'CTL'; //Callcenter Team Lead
$CA = 'CA'; //Callcenter agent
$C = 'C'; //Client
$VA = 'VA'; //Verifier
$SA = 'SA'; //Sub Admin


$limit = 50;
$offset = 0 ;
$set_count = 5;
$chart_pagination = 5;

$api_key_sms = 'ZrV5mySnuRnosvjMXbJc';
$api_secret_sms = 'XfRcnVhi1KzgTo39gWfcHMtDijy4tY';


function getCountLeads($con, $type)
{
	$getFetchQuery1 = $getFetchQuery2 = $getFetchQuery3 = 0;
	
	if(trim($type)==null)
	{
		$query = "select count(*) from leads where is_deleted=0 and added_by is not null ".$con;
		$getFetchQuery1 = getFetchQuery($query);
	}
	else
	{
		$query = "select count(*) from leads where is_deleted=0 ".$con." and assigned_usertype = '".$type."'";
		$getFetchQuery1 = getFetchQuery($query);
		
		if($type=='CA' || $type=='CCA')
		{
			$outcome_type = $type=='CA'?'AL':'ACL';
			$query = "select count(*) from leads where is_deleted=0 ".$con." and  assigned_usertype is null and outcome_type='".$outcome_type."'";
			$getFetchQuery2 = getFetchQuery($query);
			
			$query = "select count(*) from leads where is_deleted=0 ".$con." and  assigned_usertype is null and outcome_type='COP' and added_by in (select uid from login_master where type='".$type."')";
			$getFetchQuery3 = getFetchQuery($query);
		}
	}
	
	return $getFetchQuery1 + $getFetchQuery2 + $getFetchQuery3;
}

function msw_leads($type)
{
	if($type=='f')
	{
		return 'Fresh Leads';
	}
	else if($type=='v')
	{
		return 'Verifier Leads';
	}
	else if($type=='s')
	{
		return 'Solicitor Leads';
	}
	else if($type=='w')
	{
		return 'Working Portal';
	}
	else if($type=='a')
	{
		return 'Approved Leads';
	}
	else if($type=='i')
	{
		return 'Invoice Sent';
	}
	else if($type=='c')
	{
		return 'Sent To Callcenter';
	}
	else
	{
		return 'Fresh Leads';
	}
}

function setMSWLeadType($id, $type)
{
	global $mysqli;
	
	$loginmaster = $mysqli->prepare("update leads set msw_lead_type=? where id=?");
	$loginmaster->bind_param("si",$type,$id);
	$loginmaster->execute();
	$loginmaster->close();
	
	if($type=='f' || $type=='a' || $type=='w')
	{
		writeNotification($id,$type);
	}

}

function converttodatetime($date_sent)
{
	return date("Y-m-d H:i:s", strtotime($date_sent));
}

function converttodate($date_sent)
{
	return date("Y-m-d", strtotime($date_sent));
}

function converttotime($date_sent)
{
	return date("H:i:s", strtotime($date_sent));
}

function create_password($password)
{
	return md5($password);
}

function create_unique_pass()
{
	// return mt_rand(100000,999999);
	return "123456";
}

function file_upload($image,$tmp_file, $folder_name)
{
	$ext = strtolower(pathinfo($image,PATHINFO_EXTENSION));
	$actual_name = pathinfo($image,PATHINFO_FILENAME);
	$directory= "../uploads/";
	$new_path=$directory.$folder_name.'/'.$image;
	
	$i=1;
	while(file_exists($new_path))
	{           
		$image = $actual_name.$i.'.'.$ext;
		$new_path=$directory.$folder_name.'/'.$image;
		$i++;
	}
	
	move_uploaded_file($tmp_file,$new_path);
	
	if (file_exists($new_path))  
	{ 
		return str_replace($directory,'',$new_path); 
	} 
	else 
	{
		return '';
	} 
}

function has_file_on_server($image)
{
	if(trim($image)!=null)
	{
		$directory= "uploads/";
		return $directory.$image;
	}
}

function generate_password($uid)
{
	global $mysqli;
	
	$ori_pass = create_unique_pass(); //send pass by message or email
	$password = create_password($ori_pass);
	
	$loginmaster = $mysqli->prepare("update login_master set password=? where uid=?");
	$loginmaster->bind_param("si",$password,$uid);
	$loginmaster->execute();
	$loginmaster->close();
	
	return $ori_pass;
}

function getStates()
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("SELECT * FROM states where status=1 order by state_name");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function emailTemplates($utype, $con)
{
	global $mysqli;
	
	if($utype=='MSW')
	{
		$con.=' and show_admin=1';
	}
	else if($utype=='CCA')
	{
		$con.=' and show_cc=1';
	}
	else
	{
		$con.=' and show_verifier=1';
	}
	$datamaster = $mysqli->prepare("SELECT * FROM  email_templates where status='AC'".$con);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function phoneTemplates($utype, $con)
{
	global $mysqli;
	
	if($utype=='MSW')
	{
		$con.=' and show_admin=1';
	}
	else if($utype=='CCA')
	{
		$con.=' and show_cc=1';
	}
	else
	{
		$con.=' and show_verifier=1';
	}
	$datamaster = $mysqli->prepare("SELECT * FROM  sms_templates where status='AC'".$con);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function emailTemplatesAttachments($con)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("SELECT * FROM  email_attachments where status='AC'".$con);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getOutcomes($usertype)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("SELECT * FROM outcomes where status='AC' order by outcome_order");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getCountries()
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("SELECT * FROM country where status=1 order by name");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getSubUrbs($condition)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("SELECT * FROM suburbs where status=1 ".$condition." order by suburb_name");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getCallcenters($condition)
{
	global $mysqli;
	
	$condition=" where is_deleted=0 ".$condition;
	
	$datamaster = $mysqli->prepare("SELECT id,centre_name, callcenter.uid FROM callcenter inner join login_master on login_master.uid = callcenter.uid ".$condition." order by centre_name");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getVerifiers($condition)
{
	global $mysqli;
	
	$condition=" where is_deleted=0 ".$condition;
	
	$datamaster = $mysqli->prepare("SELECT id,verifier_name, verifier.uid FROM verifier inner join login_master on login_master.uid = verifier.uid ".$condition." order by verifier_name");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getInjuries()
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select * from injury_type");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getBodyParts()
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select * from body_parts");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function countUsersWithMobile($number)
{
	global $mysqli;
	
	$countmaster = $mysqli->prepare("select count(*) from login_master where phone=? and is_deleted=0");
	$countmaster->bind_param("s",$number);
	$countmaster->execute();
	$countmaster->bind_result($count_users);
	$countmaster->fetch();
	$countmaster->close();
	
	return $count_users;
	
}

function countUsersWithMobileforEdit($number,$uid)
{
	global $mysqli;
	
	$countmaster = $mysqli->prepare("select count(*) from login_master where phone=? and uid!=? and is_deleted=0");
	$countmaster->bind_param("si",$number,$uid);
	$countmaster->execute();
	$countmaster->bind_result($count_users);
	$countmaster->fetch();
	$countmaster->close();
	
	return $count_users;
	
}

function getUserData($uid, $usertype)
{
	global $mysqli;
	global $MSW;
	global $CCA;
	global $CHR;
	global $CTL;
	global $CA;
	global $C;
	global $VA;
	
	$condition=' where is_deleted=0 and login_master.uid='.$uid;
	
	if($usertype==$CTL || $usertype==$CA)
	{
		$datamaster = $mysqli->prepare("SELECT * FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid ".$condition." order by login_master.uid desc");
	}
	else if($usertype==$CCA)
	{
		$datamaster = $mysqli->prepare("SELECT * FROM callcenter inner join login_master on login_master.uid = callcenter.uid ".$condition." order by login_master.uid desc");
	}
	else if($usertype==$CHR)
	{
		$datamaster = $mysqli->prepare("SELECT * FROM callcenter_hr inner join login_master on login_master.uid = callcenter_hr.uid ".$condition." order by login_master.uid desc");
	}
	else if($usertype==$MSW)
	{
		$datamaster = $mysqli->prepare("SELECT * FROM login_master ".$condition." order by uid desc");
	}
	else if($usertype==$C)
	{
		$datamaster = $mysqli->prepare("SELECT clients.*, login_master.uid, login_master.phone FROM clients inner join login_master on login_master.uid = clients.uid ".$condition." order by id desc");
	}
	else if($usertype==$VA)
	{
		$datamaster = $mysqli->prepare("SELECT * FROM verifier inner join login_master on login_master.uid = verifier.uid ".$condition." order by login_master.uid desc");
	}
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getUserCount($usertype, $condition)
{
	global $mysqli;
	global $MSW;
	global $CCA;
	global $CHR;
	global $CTL;
	global $CA;
	global $C;
	global $VA;
	
	$condition=' where is_deleted=0 '.$condition;
	
	if($usertype==$CTL || $usertype==$CA)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid ".$condition);
	}
	else if($usertype==$CCA)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM callcenter inner join login_master on login_master.uid = callcenter.uid ".$condition);
	}
	else if($usertype==$CHR)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM callcenter_hr inner join login_master on login_master.uid = callcenter_hr.uid inner join callcenter on callcenter.id = callcenter_hr.callcenter_id inner join suburbs on suburbs.id=callcenter_hr.suburb_id inner join states on states.id=callcenter_hr.state_id ".$condition);
	}
	else if($usertype==$MSW)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM login_master ".$condition);
	}
	else if($usertype==$C)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM login_master inner join clients on clients.uid = login_master.uid ".$condition);
	}
	else if($usertype==$VA)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM verifier inner join login_master on login_master.uid = verifier.uid inner join country on country.id=verifier.country ".$condition);
	}	
	$datamaster->execute();
	$datamaster->bind_result($user_count);
	$datamaster->fetch();
	$datamaster->close();
	
	return $user_count;
}

function getUserDataByPagination($usertype, $condition,$limit, $offset)
{
	global $mysqli;
	global $MSW;
	global $CCA;
	global $CHR;
	global $CTL;
	global $CA;
	global $C;
	global $VA;
	
	$condition=' where is_deleted=0 '.$condition;
	
	if($usertype==$CTL || $usertype==$CA)
	{
		$query = "SELECT tl_and_agents.*,login_master.status, country.name as cname FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid inner join country on country.id=tl_and_agents.country ".$condition." order by login_master.uid desc limit ? offset ?";
	}
	else if($usertype==$CCA)
	{
		$query = "SELECT callcenter.*,country.name as cname, login_master.status FROM callcenter inner join login_master on login_master.uid = callcenter.uid inner join country on country.id=callcenter.country ".$condition." order by login_master.uid desc limit ? offset ?";
	}
	else if($usertype==$CHR)
	{
		$query = "SELECT callcenter_hr.*, centre_name, state_name, suburb_name, login_master.status FROM callcenter_hr inner join login_master on login_master.uid = callcenter_hr.uid inner join callcenter on callcenter.id = callcenter_hr.callcenter_id inner join suburbs on suburbs.id=callcenter_hr.suburb_id inner join states on states.id=callcenter_hr.state_id ".$condition." order by login_master.uid desc limit ? offset ?";
	}
	else if($usertype==$MSW)
	{
		$query = "SELECT * FROM login_master ".$condition." order by uid desc limit ? offset ?";
	}
	else if ($usertype==$C)
	{
		$query = "SELECT clients.*,state_name, suburb_name, clients.status,login_master.is_online, login_master.phone FROM clients inner join suburbs on suburbs.id=clients.suburb_id inner join states on states.id=clients.state_id inner join login_master on clients.uid = login_master.uid ".$condition." order by id desc limit ? offset ?";
	}
	else if($usertype==$VA)
	{
		$query = "SELECT verifier.*,country.name as cname, login_master.status FROM verifier inner join login_master on login_master.uid = verifier.uid inner join country on country.id=verifier.country ".$condition." order by login_master.uid desc limit ? offset ?";
	}
	
	$datamaster = $mysqli->prepare($query);
	$datamaster->bind_param('ii',$limit, $offset);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function deleteUser($uid)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("update login_master set is_deleted=1 where uid=?");
	$datamaster->bind_param("i",$uid);
	$datamaster->execute();
	$datamaster->close();
}

function deleteClient($uid)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("update clients set is_deleted=1 where id=?");
	$datamaster->bind_param("i",$uid);
	$datamaster->execute();
	$datamaster->close();
}

function deleteLead($uid,$from_reports)
{
	global $mysqli;
	
	$thisdate = date('Y-m-d H:i:s');
	
	if($from_reports==1)
	{
		$datamaster = $mysqli->prepare("update leads set is_deleted=1, from_reports=?, updation_date=?, show_to_admin=0 where id=?");
	}
	else
	{
		$datamaster = $mysqli->prepare("update leads set is_deleted=0,from_reports=?, updation_date=?, show_to_admin=0  where id=?");
	}
	$datamaster->bind_param("isi",$from_reports,$thisdate,$uid);
	$datamaster->execute();
	$datamaster->close();
}

function getAccidentDetails($condition,$lead_id)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select * from lead_accidents where lead_id=?".$condition);
	$datamaster->bind_param("i",$lead_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getLeadsCount($condition)
{
	global $mysqli;

	$condition=' where is_deleted=0 '.$condition;
	
	$datamaster = $mysqli->prepare("SELECT count(*) FROM leads ".$condition);
	$datamaster->execute();
	$datamaster->bind_result($user_count);
	$datamaster->fetch();
	$datamaster->close();
	
	return $user_count;
}

function getLeadsDataByPagination($condition,$limit, $offset)
{
	global $mysqli;
	
	$condition=' where is_deleted=0 '.$condition;
	
	$query = "SELECT leads.* from leads ".$condition." order by id desc limit ? offset ?";
	$datamaster = $mysqli->prepare($query);
	$datamaster->bind_param('ii',$limit, $offset);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getQuery($query)
{
	global $mysqli;

	$datamaster = $mysqli->prepare($query);
	echo $mysqli->error;
    $datamaster->execute();
	echo $mysqli->error;
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getFetchQuery($query)
{
	global $mysqli;

	$datamaster = $mysqli->prepare($query);
	echo $mysqli->error;
	$datamaster->execute();
	echo $mysqli->error;
	$datamaster->bind_result($data);
	$datamaster->fetch();
	$datamaster->close();
	
	return $data;
}

function changeTimeZone($dateString, $timeZoneSource = null, $timeZoneTarget = null)
{
	if (empty($timeZoneSource)) 
	{
		$timeZoneSource = date_default_timezone_get();
	}
	
	if (empty($timeZoneTarget)) 
	{
		$timeZoneTarget = date_default_timezone_get();
	}

	$dt = new DateTime($dateString, new DateTimeZone($timeZoneSource));
	$dt->setTimezone(new DateTimeZone($timeZoneTarget));

	return $dt->format("Y-m-d H:i:s");
}

function getAustraliaStateQuery($state_id)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("select timezones from states where id=?");
	$datamaster->bind_param('i', $state_id);
	$datamaster->execute();
	$datamaster->bind_result($state_time_zone);
	$datamaster->fetch();
	$datamaster->close();
	
	return $state_time_zone;
}

function getUserAssignedComments($uid)
{
	global $mysqli;
	
	$d = date('Y-m-d');

	$datamaster = $mysqli->prepare("SELECT lead_comments.*, first_name, last_name, middle_name FROM lead_comments inner join leads on leads.id = lead_comments.lead_id where ((lead_comments.status='PN' and comment_to=?) or (lead_comments.lead_transfer_type='IP' and comment_to=? and date(lead_comments.creation_date)=?)) and is_deleted=0 order by creation_date desc");
	$datamaster->bind_param('iis',$uid,$uid,$d);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getLastLeadCommentUser($lead_id)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select comment_to from lead_comments where lead_id=? order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		return $r->comment_to;
	}
	else
	{
		return 0;
	}
}

function getLeadLastAgent($lead_id)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select comment_from from lead_comments inner join tl_and_agents on tl_and_agents.uid = lead_comments.comment_from where lead_id=? and comment_utype='CTL' order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		return $r->comment_from;
	}
	else
	{
		return 0;
	}
}

function getLeadLastTeamLead($lead_id)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select comment_from from lead_comments inner join tl_and_agents on tl_and_agents.uid = lead_comments.comment_from where lead_id=? and (comment_utype='CCA' or comment_utype='CA') order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		return $r->comment_from;
	}
	else
	{
		return 0;
	}
}

function getLastLeadCommentUserByType($lead_id, $utype)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("select comment_to from lead_comments where comment_utype=? and lead_id=? order by creation_date desc limit 1");
	$datamaster->bind_param('si', $utype, $lead_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		return $r->comment_to;
	}
	else
	{
		return 0;
	}
}

function checkClientRejectedLead($lead_id, $uid)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select comments from lead_comments where lead_id=? and lead_transfer_type='RJ' and comment_from=? order by creation_date desc limit 1");
	$datamaster->bind_param('ii', $lead_id, $uid);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		return $r->comments;
	}
	else
	{
		return '-1';
	}
}

function lastLeadRejectedByDetails($lead_id)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select comments, comment_from from lead_comments where lead_id=? and lead_transfer_type='RJ' order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getCallcenterIdLead($lead_id)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select callcenter_id,is_affliated from callcenter inner join leads on leads.callcenter_id = callcenter.uid where leads.id=?");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getCallcenterOrAgent($lead_id)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select comment_from from lead_comments where lead_id=? and comment_utype='MSW' order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		return $r->comment_from;
	}
	else
	{
		return 0;
	}
}

function getClients()
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select company_name,uid from clients order by company_name");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getNames($uid, $usertype)
{
	global $mysqli;
	global $MSW;
	global $CCA;
	global $CHR;
	global $CTL;
	global $CA;
	global $C;
	global $VA;
	
	$condition=' where login_master.uid='.$uid;
	
	if($usertype==$MSW)
	{
		$name = 'Administrator';
	}
	else
	{
		if($usertype==$CTL || $usertype==$CA)
		{
			$datamaster = $mysqli->prepare("SELECT first_name FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid ".$condition." order by login_master.uid desc");
		}
		else if($usertype==$CCA)
		{
			$datamaster = $mysqli->prepare("SELECT centre_name FROM callcenter inner join login_master on login_master.uid = callcenter.uid ".$condition." order by login_master.uid desc");
		}
		else if($usertype==$CHR)
		{
			$datamaster = $mysqli->prepare("SELECT center_name FROM callcenter_hr inner join login_master on login_master.uid = callcenter_hr.uid ".$condition." order by login_master.uid desc");
		}
		else if($usertype==$C)
		{
			$datamaster = $mysqli->prepare("SELECT company_name FROM clients inner join login_master on login_master.uid = clients.uid ".$condition." order by id desc");
		}
		else if($usertype==$VA)
		{
			$datamaster = $mysqli->prepare("SELECT verifier_name FROM verifier inner join login_master on login_master.uid = verifier.uid ".$condition." order by login_master.uid desc");
		}
		$datamaster->execute();
		$datamaster->bind_result($name);
		$datamaster->fetch();
		$datamaster->close();
	}
	return $name;
}

function getComments($lead_id)
{
	global $mysqli;

	$user_id = $_SESSION["userId"];
	$user_type = $_SESSION["userType"];
	
	$datamaster = $mysqli->prepare("select creation_date from lead_comments where lead_id=? and directly_assigned_admin=1 order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$datamaster->bind_result($creation_date);
	$datamaster->fetch();
	$datamaster->close();
	
	$condition = '';
	if($creation_date!=null)
	{
		$condition.=" and lead_comments.creation_date > '".$creation_date."'";
	}
	
	$datamaster = $mysqli->prepare("select lead_comments.* from lead_comments inner join leads on leads.id = lead_comments.lead_id where is_shift = 0 and lead_id=? and signup_type is null ".$condition." order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		return trim(str_replace('"','',$r->comments));
	}
	else
	{
		return NULL;
	}
}

function getCommentsSpecific($lead_id)
{
	global $mysqli;

	$user_id = $_SESSION["userId"];

	$datamaster = $mysqli->prepare("select creation_date from lead_comments where lead_id=? and directly_assigned_admin=1 order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$datamaster->bind_result($creation_date);
	$datamaster->fetch();
	$datamaster->close();
	
	$condition = '';
	if($creation_date!=null)
	{
		$condition.=" and lead_comments.creation_date > '".$creation_date."'";
	}
	
	$datamaster = $mysqli->prepare("select lead_comments.comments,lead_comments.directly_assigned_admin from lead_comments inner join leads on leads.id = lead_comments.lead_id where lead_id=? and (comment_to=? or comment_from=?) ".$condition." order by lead_comments.creation_date desc limit 1");
	$datamaster->bind_param('iii', $lead_id, $user_id,$user_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		return trim(str_replace('"','',$r->comments));
	}
	else
	{
		return NULL;
	}
}

function getSignupButtonName($lead_id)
{
	global $mysqli;

	$user_id = $_SESSION["userId"];
	$user_type = $_SESSION["userType"];

	$datamaster = $mysqli->prepare("select creation_date from lead_comments where lead_id=? and directly_assigned_admin=1 order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$datamaster->bind_result($directly_creation_date);
	$datamaster->fetch();
	$datamaster->close();
	
	$condition = '';
	if($directly_creation_date!=null)
	{
		$condition.=" and lead_comments.creation_date > '".$directly_creation_date."'";
	}
	
	$datamaster = $mysqli->prepare("select creation_date from lead_comments where comment_utype ='MSW' and lead_transfer_type='RJ' ".$condition." and lead_id=".$lead_id." and comment_from=".$user_id." order by lead_comments.creation_date desc limit 1");
	$datamaster->execute();
	$datamaster->bind_result($rej_date);
	$datamaster->fetch();
	$datamaster->close();
	
	$con='';
	if($rej_date!=null)
	{
		$con.= " and lead_comments.creation_date>='".$rej_date."'";
	}
	
	$datamaster = $mysqli->prepare("select count(*) from lead_comments inner join leads on leads.id = lead_comments.lead_id where lead_id=? ".$condition." and comment_from=? and lead_transfer_type='SU'" .$con);
	$datamaster->bind_param('ii', $lead_id, $user_id);
	$datamaster->execute();
	$datamaster->bind_result($c);
	$datamaster->fetch();
	$datamaster->close();
	
	return $c;
}

function getSignUpDetails($lead_id, $client_id)
{
	global $mysqli;

	$x='';
	$cr_date=null;
		
	$datamaster = $mysqli->prepare("select creation_date from lead_comments where lead_id=? and directly_assigned_admin=1 order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$datamaster->bind_result($directly_creation_date);
	$datamaster->fetch();
	$datamaster->close();
	
	$condition = '';
	if($directly_creation_date!=null)
	{
		$condition.=" and lead_comments.creation_date > '".$directly_creation_date."'";
	}

	if($client_id==null)
	{
		$datamaster = $mysqli->prepare("select creation_date from lead_comments where comment_utype ='MSW' and lead_transfer_type='RJ' ".$condition." and lead_id=".$lead_id." order by lead_comments.creation_date desc limit 1");
		$datamaster->execute();
		$datamaster->bind_result($cr_date);
		$datamaster->fetch();
		$datamaster->close();
			
		$datamaster = $mysqli->prepare("select lead_comments.creation_date as c_cr_date, comment_from,assigned_client,signup_type, signup_dt,lead_comments.comments, initials from lead_comments inner join leads on leads.id = lead_comments.lead_id inner join states on states.id = lead_comments.client_state_zone where comment_utype ='C' and signup_type is not null ".$condition." and lead_id=".$lead_id." order by lead_comments.creation_date desc limit 1");
	}
	else
	{
		$datamaster = $mysqli->prepare("select creation_date from lead_comments where comment_utype ='MSW' and lead_transfer_type='RJ' ".$condition." and lead_id=".$lead_id." and comment_from=".$client_id." order by lead_comments.creation_date desc limit 1");
		$datamaster->execute();
		$datamaster->bind_result($cr_date);
		$datamaster->fetch();
		$datamaster->close();
		
		$datamaster = $mysqli->prepare("select lead_comments.creation_date as c_cr_date, comment_from,assigned_client,signup_type, signup_dt,lead_comments.comments, initials from lead_comments inner join leads on leads.id = lead_comments.lead_id inner join states on states.id = lead_comments.client_state_zone where comment_utype ='C' and signup_type is not null ".$condition." and lead_id=".$lead_id." and comment_from=".$client_id." order by lead_comments.creation_date desc limit 1");
	}
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		
		if($r->assigned_client!=null && $r->assigned_client==$r->comment_from)
		{
			// E - Email , O - Office Signup, H - Home Signup, P - Post
			
			$r->comments = str_replace('"','',$r->comments);
			
			if($r->signup_type=='E' || $r->signup_type=='P')
			{
				$x= $r->signup_type=='E'?'Email':'Post';
				$x.=", <span style='color:red'>Call: ".date("d M,Y",strtotime($r->signup_dt))." (".$r->initials.")</span>, Comments: ".$r->comments; 
				
				$x = '<p data-toggle="tooltip" data-original-title="'.trim(strip_tags($x)).'">'.$x.'</p>';
			}
			else if($r->signup_type=='O' || $r->signup_type=='H')
			{
				$x = $r->signup_type=='O'?'Office Signup':'Home Signup';
				$x.=", <span style='color:red'>Call: ".date("d M,Y H:i",strtotime($r->signup_dt))." (".$r->initials.")</span>, Comments: ".$r->comments; 
				
				$x = '<p data-toggle="tooltip" data-original-title="'.trim(strip_tags($x)).'">'.$x.'</p>';
			}

			if($cr_date !=null && $r->c_cr_date>$cr_date)
			{
				return '<button id="'.$lead_id.'signup" class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" a_val="'.strip_tags(preg_replace('/\s+/', ' ',$x)).'" onclick="sign_val_copy('.$lead_id.')"></button>'.$x;
			}
			else if($cr_date ==null)
			{
				return '<button id="'.$lead_id.'signup" class="ti-clipboard" data-toggle="tooltip" data-original-title="Copy Text" a_val="'.strip_tags(preg_replace('/\s+/', ' ',$x)).'" onclick="sign_val_copy('.$lead_id.')"></button>'.$x;
			}
			else
			{
				return NULL;
			}
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		return NULL;
	}
}

/* function getCallBackDetails($lead_id)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("select callback_datetime,timezone_target, callback_details,initials from lead_callbacks inner join states on states.id = lead_callbacks.timezone_target where lead_id=".$lead_id." order by lead_callbacks.id desc limit 1");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		
		//UTC to user time zone
		// return date('Y-m-d h:i A',strtotime(changeTimeZone($r->callback_datetime, date_default_timezone_get(), $r->timezone_target)));
		return $r->callback_details.'<br/>Call: '.date('d M,Y h:i A',strtotime($r->callback_datetime)).' ('.$r->initials.')';
	}
	else
	{
		return NULL;
	}
} */

function getCallBackDetails($lead_id)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("select creation_date from lead_comments where lead_id=? and directly_assigned_admin=1 order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$datamaster->bind_result($directly_creation_date);
	$datamaster->fetch();
	$datamaster->close();
	
	$condition = '';
	if($directly_creation_date!=null)
	{
		$condition.=" and callback_datetime > '".$directly_creation_date."'";
	}
	
	$datamaster = $mysqli->prepare("select callback_details from lead_callbacks where lead_id=".$lead_id." order by lead_callbacks.id desc limit 1");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);		
		return trim(str_replace('"','',$r->callback_details));
	}
	else
	{
		return NULL;
	}
}

function getAccidentType($lead_id)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("select accident_types.type,accident_date_time,is_na from accident_types inner join lead_accidents on lead_accidents.accident_kind = accident_types.id where lead_id=".$lead_id." order by accident_date_time desc limit 1");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		
		if($r->is_na!=1)
		{
			return $r->type." <br/><span style='color:red'>(".date('d-M-Y',strtotime($r->accident_date_time)).")</span>";
		}
		else
		{
			return $r->type." <br/><span style='color:red'>(".date('M-Y',strtotime($r->accident_date_time)).")</span>";
		}
	}
	else
	{
		return NULL;
	}
}

function getAccidentTypeReports($lead_id)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("select accident_types.type,accident_date_time,is_na from accident_types inner join lead_accidents on lead_accidents.accident_kind = accident_types.id where lead_id=".$lead_id." order by accident_date_time desc limit 1");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);
		
		if($r->is_na!=1)
		{
			return "<td>".$r->type."</td><td>".date('d-M-Y',strtotime($r->accident_date_time))."</td>";
		}
		else
		{
			return "<td>".$r->type."</td><td>".date('M-Y',strtotime($r->accident_date_time))."</td>";
		}
	}
	else
	{
		return "<td></td><td></td>";
	}
}

function getCallcenterAndClient($lead_id)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("select centre_name from callcenter inner join leads on leads.callcenter_id = callcenter.uid where leads.id=".$lead_id);
	$datamaster->execute();
	$datamaster->bind_result($center_name);
	$datamaster->fetch();
	$datamaster->close();
	
	$datamaster = $mysqli->prepare("select company_name from clients inner join leads on clients.uid = leads.assigned_client where leads.id=".$lead_id);
	$datamaster->execute();
	$datamaster->bind_result($client_name);
	$datamaster->fetch();
	$datamaster->close();
	
	return "<td class='widthCustom small'>".$center_name."</td><td class='widthCustom small'>".$client_name."</td>";
}

function getClientForCallcenter($lead_id)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("select company_name from clients inner join leads on clients.uid = leads.assigned_client  where leads.id=".$lead_id);
	$datamaster->execute();
	$datamaster->bind_result($client_name);
	$datamaster->fetch();
	$datamaster->close();
	
	return "<td class='widthCustom small'>".$client_name."</td>";
}

function getAgents()
{
	global $mysqli;
	
	$condition=' where is_deleted=0 and login_master.type="CA"';
	
	$datamaster = $mysqli->prepare("SELECT * FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid ".$condition);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getUsersofCallcenter($callcenter_id,$userType)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("SELECT tl_and_agents.uid,first_name, last_name FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid where is_deleted=0 and login_master.type=? and callcenter_id = ? order by first_name");
	$datamaster->bind_param('si',$userType,$callcenter_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getUsersofCallcenterLimit($callcenter_id,$userType, $offset)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("SELECT tl_and_agents.uid,first_name, last_name FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid where is_deleted=0 and login_master.type=? and callcenter_id = ? order by first_name limit 5 offset ".$offset);
	$datamaster->bind_param('si',$userType,$callcenter_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getTeamLeads()
{
	global $mysqli;
	
	$condition=' where is_deleted=0 and login_master.type="CTL"';
	
	$datamaster = $mysqli->prepare("SELECT * FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid ".$condition);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getAgentsCC($c)
{
	global $mysqli;
	
	$condition=' where is_deleted=0 and login_master.type="CA" and callcenter_id='.$c;
	
	$datamaster = $mysqli->prepare("SELECT * FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid ".$condition);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getTeamLeadsCC($c)
{
	global $mysqli;
	
	$condition=' where is_deleted=0 and login_master.type="CTL" and callcenter_id='.$c;
	
	$datamaster = $mysqli->prepare("SELECT * FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid ".$condition);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getNotesDetails($lead_id)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("select creation_date from lead_comments where lead_id=? and directly_assigned_admin=1 order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$datamaster->bind_result($directly_creation_date);
	$datamaster->fetch();
	$datamaster->close();
	
	$condition = '';
	if($directly_creation_date!=null)
	{
		$condition.=" and creation_date > '".$directly_creation_date."'";
	}

	$datamaster = $mysqli->prepare("select notes from lead_notes where lead_id=".$lead_id." ".$condition." order by lead_notes.id desc limit 1");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);		
		return trim(str_replace('"','',$r->notes));
	}
	else
	{
		return NULL;
	}
}

function getLeadTLAndAgentName($lead_id,$cid)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("select first_name,last_name from lead_comments inner join tl_and_agents on tl_and_agents.uid = lead_comments.comment_to where lead_id=? and comment_utype='CA' and callcenter_id=? order by cid desc limit 1");
	$datamaster->bind_param('ii',$lead_id,$cid);
	$datamaster->execute();
	$datamaster->bind_result($agent_first_name, $agent_last_name);
	$datamaster->fetch();
	$datamaster->close();
	
	if($agent_first_name==null)
	{
		$datamaster = $mysqli->prepare("select first_name,last_name from lead_comments inner join tl_and_agents on tl_and_agents.uid = lead_comments.comment_from where lead_id=? and comment_utype='CTL' and callcenter_id=? order by cid desc limit 1");
		$datamaster->bind_param('ii',$lead_id,$cid);
		$datamaster->execute();
		$datamaster->bind_result($agent_first_name, $agent_last_name);
		$datamaster->fetch();
		$datamaster->close();
	}
	
	$datamaster = $mysqli->prepare("select tl_and_agents.first_name,tl_and_agents.last_name from leads inner join tl_and_agents on tl_and_agents.uid = leads.team_lead_assigned where leads.id=?");
	$datamaster->bind_param('i',$lead_id);
	$datamaster->execute();
	$datamaster->bind_result($teamlead_first_name, $teamlead_last_name);
	$datamaster->fetch();
	$datamaster->close();
	
	return "<td class='widthCustom small'>".$teamlead_first_name." ".$teamlead_last_name."</td><td class='widthCustom small'>".$agent_first_name." ".$agent_last_name."</td>";
}

function getLeadagentNameForTL($lead_id,$cid)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select first_name,last_name from lead_comments inner join tl_and_agents on tl_and_agents.uid = lead_comments.comment_to where lead_id=? and comment_utype='CA' and callcenter_id=(select callcenter_id from tl_and_agents where uid=?) order by cid desc limit 1");
	$datamaster->bind_param('ii',$lead_id,$cid);
	$datamaster->execute();
	$datamaster->bind_result($agent_first_name, $agent_last_name);
	$datamaster->fetch();
	$datamaster->close();
	
	if(trim($agent_first_name)==null)
	{
		$datamaster = $mysqli->prepare("select first_name,last_name from lead_comments inner join tl_and_agents on tl_and_agents.uid = lead_comments.comment_from where lead_id=? and comment_utype='CTL' and callcenter_id=(select callcenter_id from tl_and_agents where uid=?) order by cid desc limit 1");
		$datamaster->bind_param('ii',$lead_id,$cid);
		$datamaster->execute();
		$datamaster->bind_result($agent_first_name, $agent_last_name);
		$datamaster->fetch();
		$datamaster->close();
	}

	return "<td class='widthCustom small'>".$agent_first_name." ".$agent_last_name."</td>";
}

/*echo '<pre>';
print_r(getClientsEmail(7));
echo '</pre>';
*/

function getClientsEmail($client_id)
{
	global $mysqli;

	$email_array= array();

	$datamaster = $mysqli->prepare("select email from clients where uid=?");
	$datamaster->bind_param('i',$client_id);
	$datamaster->execute();
	$datamaster->bind_result($email);
	$datamaster->fetch();
	$datamaster->close();

	if($email!=null)
	{
		$email_array[] = $email;
	}

	$datamaster = $mysqli->prepare('select cc_email from clients_bcc where master_client_id = ?');
	$datamaster->bind_param('i',$client_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		while($r = mysqli_fetch_object($dataresult))		
		$email_array[] =  $r->cc_email;
	}

	return $email_array;
}

function getSolicitorNotesDetails($lead_id, $client_id)
{
	global $mysqli;

	$x='';
	$cr_date=null;
		
	if($client_id==null)
	{
		$datamaster = $mysqli->prepare("select creation_date from lead_comments where comment_utype ='MSW' and lead_transfer_type='RJ' and lead_id=".$lead_id." order by lead_comments.creation_date desc limit 1");
		$datamaster->execute();
		$datamaster->bind_result($cr_date);
		$datamaster->fetch();
		$datamaster->close();
		
		$datamaster = $mysqli->prepare("select lead_comments.creation_date as c_cr_date, comment_from,assigned_client,lead_comments.comments from lead_comments inner join leads on leads.id = lead_comments.lead_id where comment_utype ='C' and lead_transfer_type='FI' and lead_id=".$lead_id." order by lead_comments.creation_date desc limit 1");
	}
	else
	{
		$datamaster = $mysqli->prepare("select creation_date from lead_comments where comment_utype ='MSW' and lead_transfer_type='RJ' and lead_id=".$lead_id." and comment_from=".$client_id." order by lead_comments.creation_date desc limit 1");
		$datamaster->execute();
		$datamaster->bind_result($cr_date);
		$datamaster->fetch();
		$datamaster->close();
		
		$datamaster = $mysqli->prepare("select lead_comments.creation_date as c_cr_date, comment_from,assigned_client,lead_comments.comments from lead_comments inner join leads on leads.id = lead_comments.lead_id where comment_utype ='C' and lead_transfer_type='FI' and lead_id=".$lead_id." and comment_from=".$client_id." order by lead_comments.creation_date desc limit 1");
	}
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	if(mysqli_num_rows($dataresult)>0)
	{
		$r = mysqli_fetch_object($dataresult);

		if($r->assigned_client!=null && $r->assigned_client==$r->comment_from)
		{
			$x = $r->comments;

			if($cr_date !=null && $r->c_cr_date>$cr_date)
			{
				return $x;
			}
			else if($cr_date ==null)
			{
				return $x;
			}
			else
			{
				return NULL;
			}
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		return NULL;
	}
}

function send_email($to,$from,$subject,$message)
{
	require("../sendgrid-php/sendgrid-php.php");
	$email = new \SendGrid\Mail\Mail();
	$email->setFrom($from,'Injury Assist');
	$email->setSubject($subject);
	$email->addTo($to);
	$email->addContent("text/html", $message);
	//$sendgrid = new \SendGrid('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');
	$sendgrid = new \SendGrid('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');
	try 
	{
		  // $response = $sendgrid->send($email);
	} 
	catch (Exception $e) 
	{
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	}
	return 1;
}

function getLeadsDataByPaginationDeleted($condition,$limit, $offset)
{
	global $mysqli;
	
	$condition=' where is_deleted=1 '.$condition;
	
	$query = "SELECT leads.* from leads ".$condition." order by id desc limit ? offset ?";
	$datamaster = $mysqli->prepare($query);
	$datamaster->bind_param('ii',$limit, $offset);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}
	
function sendClientEmails($lead_id, $subject, $html)
{
	$email_array = array();
	global $admins_email;
	
	$title = getFetchQuery("select concat(first_name,' ',middle_name,' ',last_name) from leads where id=".$lead_id);
	$html = str_replace('%title%',$title,$html);

	$email = getFetchQuery("select clients.email from clients inner join leads on leads.assigned_client = clients.uid where leads.id=".$lead_id);
	$subject = $title." - ".$subject;
	if($email!=null)
	{
		send_email($email,$admins_email,$subject,$html);
	}
	
	$clients_bcc  = getQuery('select cc_email from clients_bcc inner join leads on leads.assigned_client = clients_bcc.master_client_id where id='.$lead_id);
	if(mysqli_num_rows($clients_bcc)!=0)
	{
		while($cc_email_row = mysqli_fetch_object($clients_bcc))
		{
			send_email($cc_email_row->cc_email,$admins_email,$subject,$html);
		}
	}
}

function outcome_function($lead_id, $outcome_by, $outcome_type, $creation_date)
{
	global $mysqli;
	
	$outcome = null;
	$cond='';
	if($outcome_type=='CTA' || $outcome_type=='ACTA' || $outcome_type=='PFV')
	{
		$cond.=" ,is_previous_verifier = 1";
	}
	
	$loginmaster = $mysqli->prepare("update leads set outcome=(select outcome_title from outcomes where outcome_initials = ?), outcome_type=? ".$cond." where id=?");
	$loginmaster->bind_param("ssi",$outcome_type, $outcome_type, $lead_id);
	$loginmaster->execute();
	$loginmaster->close();
	
	if($outcome_type=='RJ')
	{
		$loginmaster = $mysqli->prepare("update leads set latest_sol_notes = NULL, latest_signup_detail = NULL, latest_signuptype = NULL, latest_signup_datetime = NULL, latest_signup_timezone = NULL, l_lead_reject_date = ?, last_comment = NULL, latest_spoken_to_client = 0 WHERE id = ?");
		$loginmaster->bind_param("si",$thisdate,$lead_id);
		$loginmaster->execute();
		$loginmaster->close();
	}
	
}

function restoreDeletedLead($lead_id, $thisdate)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("update leads set is_deleted=0, from_reports=0, updation_date=?, show_to_admin=1 where id=?");
	$datamaster->bind_param("si",$thisdate,$lead_id);
	$datamaster->execute();
	$datamaster->close();
}

function removeAssignedDirectlyCheck($lead_id)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("update leads set admin_assigned_directly=0 where id=?");
	$datamaster->bind_param("i",$lead_id);
	$datamaster->execute();
	$datamaster->close();
}

function getHaveCalledButtonName($lead_id)
{
	global $mysqli;

	$user_id = $_SESSION["userId"];
	$user_type = $_SESSION["userType"];

	$datamaster = $mysqli->prepare("select creation_date from lead_comments where lead_id=? and directly_assigned_admin=1 order by creation_date desc limit 1");
	$datamaster->bind_param('i', $lead_id);
	$datamaster->execute();
	$datamaster->bind_result($directly_creation_date);
	$datamaster->fetch();
	$datamaster->close();
	
	$condition = '';
	if($directly_creation_date!=null)
	{
		$condition.=" and lead_comments.creation_date > '".$directly_creation_date."'";
	}
	
	// For spoken to client case only
	
	$datamaster = $mysqli->prepare("select creation_date from lead_comments where comment_utype ='C' and lead_transfer_type='RJ' ".$condition." and lead_id=".$lead_id." and comment_from=".$user_id." order by lead_comments.creation_date desc limit 1");
	$datamaster->execute();
	$datamaster->bind_result($rej_date);
	$datamaster->fetch();
	$datamaster->close();
	
	$con='';
	if($rej_date!=null)
	{
		$con.= " and lead_comments.creation_date>='".$rej_date."'";
	}
	
	$datamaster = $mysqli->prepare("select count(*) from lead_comments inner join leads on leads.id = lead_comments.lead_id where lead_id=? ".$condition." and comment_from=? and lead_transfer_type='SC'" .$con);
	$datamaster->bind_param('ii', $lead_id, $user_id);
	$datamaster->execute();
	$datamaster->bind_result($c);
	$datamaster->fetch();
	$datamaster->close();
	
	return $c;
}

function multideleteLead($my_array)
{
	global $mysqli;
	
	//Only for deletion from View Leads, not reports
	$thisdate = date('Y-m-d H:i:s');
	
	$datamaster = $mysqli->prepare("update leads set is_deleted=0,from_reports=0, updation_date=?, show_to_admin=0  where id in(".implode(',',$my_array).")");
	$datamaster->bind_param("s",$thisdate);
	$datamaster->execute();
	$datamaster->close();
}

function updateAssignDate($type, $lead_id, $is_affliated)
{
	global $mysqli;
	
	$thisdate = date('Y-m-d H:i:s');
	
	if($type=='CA')
	{
		$datamaster = $mysqli->prepare("update leads set assign_agent = ? where id = ?");
	}
	else if($type=='CCA' && $is_affliated==1)
	{
		$datamaster = $mysqli->prepare("update leads set assign_affliated = ? where id = ?");
	}
	else if($type=='CCA')
	{
		$datamaster = $mysqli->prepare("update leads set assign_n_callcenter = ? where id = ?");
	}
	else if($type=='C')
	{
		$datamaster = $mysqli->prepare("update leads set assign_client = ? where id = ?");
	}
	else if($type=='MSW')
	{
		$datamaster = $mysqli->prepare("update leads set assign_admin = ? where id = ?");
	}
	else if($type=='CTL')
	{
		$datamaster = $mysqli->prepare("update leads set assign_teamlead = ? where id = ?");
	}
	else if($type=='VA')
	{
		$datamaster = $mysqli->prepare("update leads set assign_verifier = ? where id = ?");
	}
	$datamaster->bind_param("si",$thisdate,$lead_id);
	$datamaster->execute();
	$datamaster->close();
}

function getUserTimezone()
{
	$ipaddress = '';
	if($ipaddress=="")
	{
		$ipaddress = getenv('REMOTE_ADDR');
	}
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	{
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
		$ipaddress='';
	}
	
    $ch = curl_init();
    $headers = array(
		'Accept: application/json',
		'Content-Type: application/json',
	);
   
   if(trim($ipaddress)!=null)
   {
	    curl_setopt($ch, CURLOPT_URL, 'https://api.ipgeolocation.io/ipgeo?apiKey=67ab11f5414e4ad099374ee732b8fa6b&fields=time_zone&ip='.$ipaddress);
   }
   else
   {
	    curl_setopt($ch, CURLOPT_URL, 'https://api.ipgeolocation.io/ipgeo?apiKey=67ab11f5414e4ad099374ee732b8fa6b&fields=time_zone');
   }
   
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);

    $authToken = curl_exec($ch);
	$authToken = json_decode($authToken);
	
	if(isset($authToken->time_zone->name) && trim($authToken->time_zone->name)!=null)
	{
		$user_timezone = $authToken->time_zone->name;
	}
	else
	{
		$user_timezone = 'UTC';
	}

	return $user_timezone;
}

function get_timezone_offset($sec, $remote_tz, $origin_tz = null) {
	$sec = strtotime($sec);
    if($origin_tz === null) {
        if(!is_string($origin_tz = date_default_timezone_get())) {
            return false; // A UTC timestamp was returned -- bail out!
        }
    }
    $origin_dtz = new DateTimeZone($origin_tz);
    $remote_dtz = new DateTimeZone($remote_tz);
    $origin_dt = new DateTime("now", $origin_dtz);
    $remote_dt = new DateTime("now", $remote_dtz);
    $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
	$t = round($offset);
	$offset = sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);	
	return date('Y-m-d h:i A',strtotime(($offset[0].$offset[1]).' hour '.($offset[3].$offset[4]).' minutes',$sec));
  
}

function get_timezone_offset_calender($sec, $remote_tz, $origin_tz = null) {
	$sec = strtotime($sec);
    if($origin_tz === null) {
        if(!is_string($origin_tz = date_default_timezone_get())) {
            return false; // A UTC timestamp was returned -- bail out!
        }
    }
    $origin_dtz = new DateTimeZone($origin_tz);
    $remote_dtz = new DateTimeZone($remote_tz);
    $origin_dt = new DateTime("now", $origin_dtz);
    $remote_dt = new DateTime("now", $remote_dtz);
    $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
	$t = round($offset);
	$offset = sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);	
	return date('Y-m-d',strtotime(($offset[0].$offset[1]).' hour '.($offset[3].$offset[4]).' minutes',$sec));
  
}

function getLinkedIds($lead_id,$lead_master_id,$condition)
{
	global $mysqli;

	if($lead_master_id!=null)
	{
		$datamaster = $mysqli->prepare("select id from leads where (master_lead_id = ? or id = ?) and id!=? ".$condition." and is_deleted=0 order by updation_date desc");
		$datamaster->bind_param('iii',$lead_master_id,$lead_master_id, $lead_id);
	}
	else
	{
		$datamaster = $mysqli->prepare("select id from leads where master_lead_id=? and id!=? ".$condition." and is_deleted=0 order by updation_date desc");
		$datamaster->bind_param('ii',$lead_id, $lead_id);
	}
	$datamaster->execute();
	$res = $datamaster->get_result();
	$datamaster->close();
	
	$str='';
	while($resr = mysqli_fetch_object($res))
	{
		$str.=', <a class="text-danger" target="_blank" href="edit-lead-each.php?u='.$resr->id.'">'.$resr->id.'</a>';
	}
	
	return trim($str,',');
}

function email_notification_accidents($lead_id)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("select accident_types.type,accident_date_time,is_na from accident_types inner join lead_accidents on lead_accidents.accident_kind = accident_types.id where lead_accidents.status = 1 and lead_id = ? order by accident_date_time desc");
	$datamaster->bind_param("i",$lead_id);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	$t='';
	$x=1;
	if(mysqli_num_rows($dataresult)>0)
	{
		$t.="<table border='1' cellspacing='0' cellpadding='5' style='margin: 25px auto 0;font-size: 14px;'>";
		$t.="<tr><td colspan='3' style='background: #40a1d8;color: #fff;text-align:center'>Leads Data</td></tr>";
		while($r = mysqli_fetch_object($dataresult))
		{
			$t.="<tr>";
			$t.="<td>".$x.".</td>";
			if($r->is_na!=1)
			{
				$t.="<td>".$r->type."</td><td>".date('d-M-Y',strtotime($r->accident_date_time))."</td>";
			}
			else
			{
				$t.="<td>".$r->type."</td><td>".date('M-Y',strtotime($r->accident_date_time))."</td>";
			}
			$t.="</tr>";
			$x++;
		}
		$t.="</table>";
	}
	
	return $t;
}


function updateLastComment($lead_id, $comment, $from , $to)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("update leads set last_comment = ?, latest_comment_usertype_from = ?, latest_comment_usertype_to = ? where id = ?");
	$datamaster->bind_param('sssi', $comment,$from, $to, $lead_id);
	$datamaster->execute();
	$datamaster->close();
}

function updateLastCommentAndNotes($lead_id, $comment1, $comment2)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("update leads set last_comment = ?, latest_notes = ? where id = ?");
	$datamaster->bind_param('ssi', $comment1,$comment2, $lead_id);
	$datamaster->execute();
	$datamaster->close();
}

function updateLastSolNotes($lead_id, $comment)
{
	global $mysqli;

	$datamaster = $mysqli->prepare("update leads set latest_sol_notes = ? where id = ?");
	$datamaster->bind_param('si', $comment, $lead_id);
	$datamaster->execute();
	$datamaster->close();
}

function getLeadInfo($condition)
{
	global $mysqli;
	
	$query = "SELECT leads.* from leads ".$condition;
	$datamaster = $mysqli->prepare($query);
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getUserCountProgram($usertype, $condition)
{
	global $mysqli;
	global $MSW;
	global $CCA;
	global $CHR;
	global $CTL;
	global $CA;
	global $C;
	global $VA;
	
	$condition=' where is_deleted is not null '.$condition;
	
	if($usertype==$CTL || $usertype==$CA)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM tl_and_agents inner join login_master on login_master.uid = tl_and_agents.uid ".$condition);
	}
	else if($usertype==$CCA)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM callcenter inner join login_master on login_master.uid = callcenter.uid ".$condition);
	}
	else if($usertype==$CHR)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM callcenter_hr inner join login_master on login_master.uid = callcenter_hr.uid inner join callcenter on callcenter.id = callcenter_hr.callcenter_id inner join suburbs on suburbs.id=callcenter_hr.suburb_id inner join states on states.id=callcenter_hr.state_id ".$condition);
	}
	else if($usertype==$MSW)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM login_master ".$condition);
	}
	else if($usertype==$C)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM login_master inner join clients on clients.uid = login_master.uid ".$condition);
	}
	else if($usertype==$VA)
	{
		$datamaster = $mysqli->prepare("SELECT count(*) FROM verifier inner join login_master on login_master.uid = verifier.uid inner join country on country.id=verifier.country ".$condition);
	}
	
	$datamaster->execute();
	$datamaster->bind_result($user_count);
	$datamaster->fetch();
	$datamaster->close();
	
	return $user_count;
}

function getCallbacksDashboard($condition)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("SELECT initials, lead_callbacks.*, first_name, last_name, middle_name from  lead_callbacks inner join leads on leads.id =  lead_callbacks.lead_id inner join states on states.id =  lead_callbacks.timezone_target where lead_callbacks.status='AC' ".$condition." order by callback_datetime");
	$datamaster->execute();
	$dataresult = $datamaster->get_result();
	$datamaster->close();
	
	return $dataresult;
}

function getAssignedLeadEmail($lead_id, $comments)
{
	global $mysqli;
	global $mysiteurl;
	
	$subject = 'Injury Assist  - New Lead Assigned';
	return '<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Injury Assist</title>
	</head>

	<body>
		<!-- Table Start -->
		<table style="text-align: center; max-width: 700px;width:100%; margin: 0 auto; border-collapse: collapse; border: 1px solid rgba(0,0,0,.15); font-family: sans-serif;">
			<thead style=" background: rgb(245, 245, 245);color: white; border-bottom: 1px solid #d1d1d1;">
				<tr>
					<th style="padding: 15px 15px;"><img src="'.$mysiteurl.'images/logo_new.png" alt="logo" style="max-width: 150px"></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="padding: 25px 10px;font-weight: 700;font-size: 22px;text-transform: uppercase;"><span style="color: #40a1d8;">New Lead Assigned</span></td>
				</tr>
				<tr>
					<td style="text-align: left;padding: 0 20px;font-size: 15px;line-height: 24px;text-align:center">
						New Lead has been assigned to you.<br/>
						Name of the lead is <b>%title%</b> <br/>
						Comments: '.$comments.'
						'.email_notification_accidents($lead_id).'
					</td>
				</tr>
				<tr>
					<td>
						<P style="margin: 30px 0;">For further details <a target="_blank" href="'.$mysiteurl.'" style="text-decoration: none;background: #40a1d8;padding: 10px;color: white;">Click Here</a></P>
					</td>
				</tr>
			</tbody>
			<tfoot style="background: #f5f5f5;border-top: 1px solid rgba(0,0,0,.15);">
				<tr>
					<td style="padding: 13px;font-size: 13px;">
						Copyright © All Rights Reserved
					</td>
				</tr>
			</tfoot>
		</table>
		<!-- Table End -->
	</body>
	</html>';
}

function sendMSWEmails($lead_id, $subject, $html)
{
	$email_array = array();
	global $admins_email;
	
	$title = getFetchQuery("select concat(first_name,' ',middle_name,' ',last_name) from leads where id=".$lead_id);
	$html = str_replace('%title%',$title,$html);

	send_email($admins_email,$admins_email,$subject,$html);
}

function getTimeZone($state_id)
{
	global $mysqli;
	
	$datamaster = $mysqli->prepare("SELECT initials FROM states where id = ?");
	$datamaster->bind_param("i",$state_id);
	$datamaster->execute();
	$datamaster->bind_result($initials);
	$datamaster->fetch();
	$datamaster->close();
	
	return $initials;
}

function getCommentUserType($utype)
{	
	switch($utype)
	{
		case 'C': return "Sol";
		case 'CCA': return "Callcenter";
		case 'CTL': return "Team Lead";
		case 'CA': return "Agent";
		case 'MSW': return "Admin";
		case 'VA': return "Verifier";
		default: return "";
	}
}

function getAssignedLeadEmailAdmin($lead_id, $comments)
{
	global $mysqli;
	global $mysiteurl;
	
	$datamaster = $mysqli->prepare("SELECT first_name, last_name, middle_name, email, phone, last_accident_type, (SELECT initials FROM states where states.id = leads.state_id) as initials from leads where id = ?");
	$datamaster->bind_param("i",$lead_id);
	$datamaster->execute();
	$datamaster->bind_result($first_name, $last_name, $middle_name, $email, $phone, $last_accident_type, $initials);
	$datamaster->fetch();
	$datamaster->close();
	
	return '<!DOCTYPE html>
				<html lang="en">
				<head>
					<meta charset="UTF-8">
					<title>Injury Assist</title>
				</head>

				<body>
					<!DOCTYPE html>
						<html lang="en">
						<head>
							<meta charset="UTF-8">
							<title>Injury Assist</title>
						</head>
						<body>
							<p>Hello Admin, <br/>Following person has contacted you. His details are below:</p>
							<p>
								<strong>Name: </strong>'.$first_name.' '.$last_name.'<br/>
								<strong>Phone: </strong>'.$phone.'<br/>
								<strong>Email: </strong>'.$email.'<br/>
								<strong>State: </strong>'.$initials.'<br/>
								<strong>Hear From: </strong> Website<br/>
								<strong>Claim For: </strong>'.$last_accident_type.'<br/>
								<strong>IP Tracing: </strong>'.$initials.' (Australia)'.'<br/>
							</p>
						</body>
				</html>';
}


function getOpenFile($type)
{
	if($type=='f')
	{
		$file = 'fresh.txt';
	}
	else if($type=='s')
	{
		$file = 'sol.txt';
	}
	else if($type=='v')
	{
		$file = 'verifier.txt';
	}
	else if($type=='a')
	{
		$file = 'approved.txt';
	}
	else if($type=='w')
	{
		$file = 'working.txt';
	}
	else
	{
		$file = null;
	}
	
	return $file;
}

function readNotification($type)
{
	$file = getOpenFile($type);
	$val = 0;
	if($file!=null)
	{
		$myfile = fopen($file, "r") or die("Unable to open file!");
		$val = fread($myfile,filesize($file));
		fclose($myfile);
	}
	
	return $val;
}

function writeNotification($lead_id, $type)
{
	$file = getOpenFile($type);
	if($file!=null)
	{
		$myfile = fopen($file, "r") or die("Unable to open file!");
		$val = fread($myfile,filesize($file));
		fclose($myfile);
		
		$myfile = fopen($file, "w") or die("Unable to open file!");
		fwrite($myfile,(++$val));
		fclose($myfile);
	}
}

function writeNotificationZero($type)
{
	$file = getOpenFile($type);
	if($file!=null)
	{
		$myfile = fopen($file, "w") or die("Unable to open file!");
		fwrite($myfile,0);
		fclose($myfile);
	}
}


?>