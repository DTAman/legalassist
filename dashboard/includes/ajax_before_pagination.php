<div aria-label="Page navigation" class="pull-right">
	<input type='hidden' name='hfGetSetCount' id='hfGetSetCount' value="<?php echo $totalCount ?>" />
	 <ul class="pagination">
		
		<?php
			$total=ceil($totalCount/$limit);
			$last_page=1;
			
			$p=1;
			$pagecount=1;
			while($pagecount<=$total && $p<=$set_count)
			{
				?>
					<li id="li_page<?php echo $pagecount ?>" class="page-item <?php if($pagecount==1) { echo "active"; } ?>">
						<a class="page-link waves-effect " href="javascript:void(0)" role="button" tabindex="<?php echo $pagecount ?>" onclick='getPagination(<?php echo $pagecount ?>)' href="javascript:void(0)" ><?php echo $pagecount; ?></a>
					</li>
				<?php
				$pagecount++;
				$p++;
			}

			if($total>=$pagecount)
			{
				?>
					<li class="page-item">
						<a class="page-link waves-effect nextpage" tabindex="<?php echo $pagecount ?>" href="javascript:void(0)" aria-label="Next">
							<span aria-hidden="true">»</span>
							<span class="sr-only">Next</span>
						</a>
					</li>
				<?php
			}
		?>
	</ul>
</div>