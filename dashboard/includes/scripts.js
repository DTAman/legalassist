$(document).ready(function() {
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    $('body').on("change", '.delete_checkbox', function() {
        if ($('.delete_checkbox').filter(':checked').length != 0) {
            $('#btnDeleteMultiple').removeAttr('disabled');
        } else {
            $('#btnDeleteMultiple').attr('disabled', true);
        }

    });
	
	$('body').on("change", '.chckAssignVerifierMultiple', function() {
        if ($('.chckAssignVerifierMultiple').filter(':checked').length != 0) {
            $('#btnAssignVerifier').removeAttr('disabled');
        } else {
            $('#btnAssignVerifier').attr('disabled', true);
        }

    });
	
    $('.switchOnOff').click(function() {

        var sendval = 0;

        if ($(this).prop("checked") == true) {
            var sendval = 1;
        }
        $.ajax({
            url: "status_online.php",
            type: 'post',
            data: {
                "sendval": sendval
            },
            datatype: 'html',
            success: function(rsp) {}
        });
    });
	
	$('.switchOnOffY').click(function() {
		var sendval = 0;

        if ($(this).prop("checked") == true) {
            var sendval = 1;
        }
        $.ajax({
            url: "status_online.php",
            type: 'post',
            data: {
                "sendval": sendval,
				"sub_user": $(this).attr("attr_user")
            },
            datatype: 'html',
            success: function(rsp) {}
        });
    });


    //Export to excel
    $('body').on("click", '#btnExportToExcel', function() {
        var traingIds = $(".col_array").val();
        var col_array = $.makeArray(traingIds.replace("[", "").replace("]", "").split(','));
        console.log(col_array);
        $(".newTable").table2excel({
            exclude: ".noExl",
            name: "Data",
            filename: getCurentFileName() + '-' + GetTodayDate(), //donot include extension here,
            fileext: ".xls",
            columns: col_array
        });
    });

    $('#txtPassword').on("cut copy paste", function(e) {
        e.preventDefault();
    });

    $('#ddlGetTeamLeads').removeAttr('required');

    $(".hides").css('display', 'none');
    $("#txtLSignUpHours").removeAttr('required');
    $("#txtLSignUpMinutes").removeAttr('required');

    $('body').on("change", '.label_no_weight_class', function() {
        var v = $(this).val();

        $(".data-error").css('display', 'none');
        $(".data-success").css('display', 'none');
        $("#cover").css('display', '');

        $(".hides").css('display', 'none');

        if (v == 'O' || v == 'H') {
            $(".hides").css('display', '');
            $("#txtLSignUpHours").attr('required', 'required');
            $("#txtLSignUpMinutes").attr('required', 'required');
        }
		 $("#cover").css('display', 'none');

    });
	
    $('body').on("click", '.callPopUp', function() {
        var button_type = $(this).attr('attr_type');
        var lead_id = $(this).attr('attr_lead');

        $('#txtSaveComment').val('');
        $(".data-error").css('display', 'none');
        $(".data-success").css('display', 'none');
        $("#cover").css('display', '');
		
		var json_string = {
            "hfLeadId": lead_id,
            "attr_type": button_type
        };
		
		$.ajax({
            url: "check_has_callback.php",
            type: 'POST',
            data: {
                "hfLeadId": lead_id
            },
            datatype: 'html',
            success: function(response) 
			{
				if(response==0)
				{
					if (button_type == 2) 
					{
						callAjax('ajaxGetCommentsAdmin.php', json_string, '.selectOptionsTextarea');
					} 
					else if (button_type == 4 || button_type == 7) 
					{
						callAjax('ajaxGetCommentsTL.php', json_string, '.selectOptionsTextarea');
					} 
					else if (button_type == 1) {
						callAjax('ajaxGetCommentsSol.php', json_string, '.selectOptionsTextarea');
					}
					callAjax('ajaxAddComemntDetails.php', json_string, '.selectOptions');
					$('#saveCommentModal').modal('show');
				}
				else
				{
					alert("You have an active callback for this lead. Please complete that callback to assign the lead.");
				}
			},
			error: function(jqXHR, status, err) {
				console.log("Some error occurred. Please refresh the page and try again.");
			},
			complete: function(jqXHR, status) {
				$("#cover").css('display', 'none');
			}
        });
    });
	
	$('body').on("click", '.rejectApproval', function() {
        var lead_id = $(this).attr('attr_lead');

        $('#txtSaveCommentCallcenter').val('');

        $(".data-error").css('display', 'none');
        $(".data-success").css('display', 'none');
        $("#cover").css('display', '');
        var json_string = {"hfLeadId": lead_id};
		
        $('#hfLeadIdCommentCallcenter').val(lead_id);
        $('#rejectApprovalModal').modal('show');
		$("#cover").css('display', 'none');
    });
	
	$('body').on("click", '.callPopUpCallbackEmail', function() {
        var lead_id = $(this).attr('attr_lead');
        var lead_email = $(this).attr('attr_lead_email');
        $('#hfLeadIdEmail').val(lead_id);
        $('#hfLeadEmail').val(lead_email);
        $('#emailTemplate').modal('show');
    });
	
	$('body').on("click", '.callPopUpCallbackPhone', function() {
        var lead_id = $(this).attr('attr_lead');
        var lead_phone = $(this).attr('attr_lead_phone');
        $('#hfLeadIdPhone').val(lead_id);
        $('#hfLeadPhone').val(lead_phone);
        $('#phoneTemplate').modal('show');
    });
	
    $('body').on("click", '.callPopUpApprove', function() {
        var button_type = $(this).attr('attr_type');
        var lead_id = $(this).attr('attr_lead');

        $(".data-error").css('display', 'none');
        $(".data-success").css('display', 'none');
        $("#cover").css('display', '');
        $("#hfLeadApproveId").attr('value', lead_id);
        $('#approveModal').modal('show');
        $("#cover").css('display', 'none');
    });

    $('body').on("click", '.callPopUpClient', function() {
        var button_type = $(this).attr('attr_type');
        var lead_id = $(this).attr('attr_lead');

        $(".data-error").css('display', 'none');
        $(".data-success").css('display', 'none');
        $("#cover").css('display', '');

        $("#hfLeadPopUpLeadClientId").attr('value', lead_id);
        $('#callPopUpClientPopUpId').modal('show');
		 $("#cover").css('display', 'none');
    });

    $('body').on("click", '.download_lead', function() {
        var lead_id = $(this).attr('l_id');

        $(".data-error").css('display', 'none');
        $(".data-success").css('display', 'none');
        $("#cover").css('display', '');

        $.ajax({
            url: "ajaxFullLead.php",
            type: 'GET',
            data: {
                "u": lead_id
            },
            datatype: 'html',
            success: function(rsp) {
                if (rsp != null) {
                    $.ajax({
                        url: "downloadFullLead.php",
                        type: 'POST',
                        data: {
                            "u": rsp,
                            "lead_id": lead_id
                        },
                        datatype: 'html',
                        success: function(rsp) {}
                    });
                }
            }
        });
		 $("#cover").css('display', 'none');
    });

    $('body').on("click", '.callPopUpFurtherInvestigation', function() {
        var lead_id = $(this).attr('attr_lead');

        $(".data-error").css('display', 'none');
        $(".data-success").css('display', 'none');
        $("#cover").css('display', '');

        $("#hfLeadInvestigationId").attr('value', lead_id);

        $.ajax({
            url: "ajaxGetInvestigations.php",
            type: 'post',
            data: {
                "hfLeadId": lead_id
            },
            datatype: 'html',
            success: function(rsp) {
                if (rsp != null) {
                    $('.inv').html(rsp);
                    $('.inv').css('display', 'block');
                }
            }
        });
        $('#furtherInvestigationModal').modal('show');
		$("#cover").css('display', 'none');
    });

    $('body').on("click", '.callPopUpSignUp', function() {
        var lead_id = $(this).attr('attr_lead');

        $(".data-error").css('display', 'none');
        $(".data-success").css('display', 'none');
        $("#cover").css('display', '');

        $("#hfLeadSignupId").attr('value', lead_id);
        $('#signupModal').modal('show');
		$("#cover").css('display', 'none');
    });
	
    $('body').on("click", '.callPopUpCallback', function() {
        var lead_id = $(this).attr('attr_lead');

        $(".data-error").css('display', 'none');
        $(".data-success").css('display', 'none');
        $("#cover").css('display', '');

        $("#hfLeadCallBackId").attr('value', lead_id);
        $('#callbackModal').modal('show');
		$("#cover").css('display', 'none');
    });
	
	$('body').on("click", '.callPopUpCallbackDone', function() {
        var hfId = $(this).attr('attr_lead');
        if(confirm('Are you sure?'))
		{
			window.location = 'callbacks.php?hfId='+hfId;
		}
    });
	
    $('body').on("click", '.callPopUpSendInvoice', function() {
        var button_type = $(this).attr('attr_type');
        var lead_id = $(this).attr('attr_lead');
        var inv_no = $(this).attr('attr_lead_inv');
        var inv_details = $(this).attr('attr_lead_details');

        $(".data-error").css('display', 'none');
        $(".data-success").css('display', 'none');
        $("#cover").css('display', '');
        $("#hfLeadInvoiceId").attr('value', lead_id);
        $("#txtInvoiceNumber").attr('value', inv_no);
        $("#txtInvoiceComment").val(inv_details);
        $('#invoiceModal').modal('show');
        $("#cover").css('display', 'none');
    });

    $('body').on("click", '.callPopUpInvoicePaid', function() {
        var button_type = $(this).attr('attr_type');
        var lead_id = $(this).attr('attr_lead');

        $(".data-error").css('display', 'none');
        $(".data-success").css('display', 'none');
        $("#cover").css('display', '');
        $("#hfLeadInvoicePaidId").attr('value', lead_id);
        $('#invoicePaidModal').modal('show');
        $("#cover").css('display', 'none');
    });

    $("input").attr('autocomplete', Math.random());
    $("textarea").attr('autocomplete', Math.random());

    $(document).on('focus', ':input', function() {
        $(this).attr('autocomplete', Math.random());
    });

    $('#txtFromDate,#txtToDate').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true
    });
	$('#txtAccidentDateSearchFrom,#txtAccidentDateSearchTo').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true
    });
    $('body').on("click", '.openModal', function() {
        $('#openModal_val').attr('value', 1);
    });
    $('body').on("click", '.openModalNotes', function() {
        $('#saveCloseModalNotes').modal();
    });
    $('body').on("click", '.removeOpenModal', function() {
        $('#openModal_val').attr('value', 0);
    });

    $('body').on("click", '.firstpage', function() {
        var firstpage = $(this).attr("tabindex");
        var page_skip = $(this).attr("tabindex");
        var txtSearch = $('#txtSearch').val();
        var hfAjaxPage = $('#hfAjaxPage').val();
        var txtFromDate = $('#txtFromDate').val();
        var txtToDate = $('#txtToDate').val();
		var txtAccidentDateSearchFrom  = $('#txtAccidentDateSearchFrom').val();
		var txtAccidentDateSearchTo  = $('#txtAccidentDateSearchTo').val();
		
        var t = 0;
        if ($('#t_val').length == 1) {
            t = $('#t_val').val();
        }
		
		var val = [];
		$('.checkList_checkbox:checked').each(function(i) {
			val[i] = $(this).val();
		});
		
		if ($('#ddlSortBy').length == 1) {
			$('#btnDeleteMultiple').attr('disabled', true);
			var ddlSortBy = $('#ddlSortBy').val();
			var ddlSubUrb = $('#ddlSubUrb').val();
			var ddlState = $('#ddlState').val();

			var ddlLCallcenters = check_isset('#ddlLCallcenters');
			var ddlLeadStatus = $('#ddlLeadStatus').val(); //array
			var ddlClients = $('#ddlClients').val(); //array
			var ddlAgents = check_isset('#ddlAgents');
			var ddlTeamLeads = check_isset('#ddlTeamLeads');
			var ddlAgentCheck = check_isset('#ddlAgentCheck');
			var ddlTeamLeadCheck = check_isset('#ddlTeamLeadCheck');
			var ddlVerifier = check_isset('#ddlVerifier');
			var txtPP = check_isset('#txtPP');
			
			var postData = {
				"txtSearch": txtSearch,
				"txtToDate": txtToDate,
				"txtFromDate": txtFromDate,
				"page_skip": page_skip,
				"firstpage": firstpage,
				"t": t,
				"ddlSortBy": ddlSortBy,
				"ddlState": ddlState,
				"ddlSubUrb": ddlSubUrb,
				"ddlClients": ddlClients,
				"ddlLCallcenters": ddlLCallcenters,
				"ddlLeadStatus": ddlLeadStatus,
				"ddlTeamLeads": ddlTeamLeads,
				"ddlAgents": ddlAgents,
				"ddlAgentCheck": ddlAgentCheck,
				"ddlTeamLeadCheck": ddlTeamLeadCheck,
				"txtAccidentDateSearchFrom": txtAccidentDateSearchFrom,
				"txtAccidentDateSearchTo": txtAccidentDateSearchTo,
				"ddlVerifier": ddlVerifier,
				"txtPP": txtPP,
				"chckCheckList": val
			};
		} else {
			var postData = {
				"txtSearch": txtSearch,
				"txtToDate": txtToDate,
				"txtFromDate": txtFromDate,
				"page_skip": page_skip,
				"firstpage": firstpage,
				"t": t,
				"txtAccidentDateSearchFrom": txtAccidentDateSearchFrom,
				"txtAccidentDateSearchTo": txtAccidentDateSearchTo,
				"ddlVerifier": ddlVerifier,
				"txtPP": txtPP,
				"chckCheckList": val
			};
		}
		
        callAjax(hfAjaxPage, postData, '.ajaxClass');
    });

    $('body').on("click", '.nextpage', function() {
        var nextpage = $(this).attr("tabindex");
        var page_skip = $(this).attr("tabindex");
        var txtSearch = $('#txtSearch').val();
        var hfAjaxPage = $('#hfAjaxPage').val();
        var txtFromDate = $('#txtFromDate').val();
        var txtToDate = $('#txtToDate').val();
		var txtAccidentDateSearchFrom  = $('#txtAccidentDateSearchFrom').val();
		var txtAccidentDateSearchTo  = $('#txtAccidentDateSearchTo').val();
		
        var t = 0;
        if ($('#t_val').length == 1) {
            t = $('#t_val').val();
        }
		
		var val = [];
		$('.checkList_checkbox:checked').each(function(i) {
			val[i] = $(this).val();
		});
		
		if ($('#ddlSortBy').length == 1) {
			$('#btnDeleteMultiple').attr('disabled', true);
			var ddlSortBy = $('#ddlSortBy').val();
			var ddlSubUrb = $('#ddlSubUrb').val();
			var ddlState = $('#ddlState').val();

			var ddlLCallcenters = check_isset('#ddlLCallcenters');
			var ddlLeadStatus = $('#ddlLeadStatus').val(); //array
			var ddlClients = $('#ddlClients').val(); //array
			var ddlAgents = check_isset('#ddlAgents');
			var ddlTeamLeads = check_isset('#ddlTeamLeads');
			var ddlAgentCheck = check_isset('#ddlAgentCheck');
			var ddlTeamLeadCheck = check_isset('#ddlTeamLeadCheck');
			var ddlVerifier = check_isset('#ddlVerifier');
			var txtPP = check_isset('#txtPP');
			
			var postData = {
				"txtSearch": txtSearch,
				"txtToDate": txtToDate,
				"txtFromDate": txtFromDate,
				"nextpage": nextpage,
                "page_skip": page_skip,
                "t": t,
				"ddlSortBy": ddlSortBy,
				"ddlState": ddlState,
				"ddlSubUrb": ddlSubUrb,
				"ddlClients": ddlClients,
				"ddlLCallcenters": ddlLCallcenters,
				"ddlLeadStatus": ddlLeadStatus,
				"ddlTeamLeads": ddlTeamLeads,
				"ddlAgents": ddlAgents,
				"ddlAgentCheck": ddlAgentCheck,
				"ddlTeamLeadCheck": ddlTeamLeadCheck,
				"txtAccidentDateSearchFrom": txtAccidentDateSearchFrom,
				"txtAccidentDateSearchTo": txtAccidentDateSearchTo,
				"ddlVerifier": ddlVerifier,
				"txtPP": txtPP,
				"chckCheckList": val
			};
		} else {
			var postData = {
				"txtSearch": txtSearch,
				"txtToDate": txtToDate,
				"txtFromDate": txtFromDate,
				"nextpage": nextpage,
                "page_skip": page_skip,
                "t": t,
				"txtAccidentDateSearchFrom": txtAccidentDateSearchFrom,
				"txtAccidentDateSearchTo": txtAccidentDateSearchTo,
				"ddlVerifier": ddlVerifier,
				"txtPP": txtPP,
				"chckCheckList": val
			};
		}
	
        callAjax(hfAjaxPage, postData, '.ajaxClass');
    });

    $('body').on("change", '#ddlAssignTo', function() {
        var sel = $("#ddlAssignTo option:selected").val();
        var lead_id = $('#hfLeadIdComment').attr('value');

        $('#ddlGetTeamLeads').removeAttr('required');

        if ($('#hfLeadCommentUType').attr('value') == 'CA') {
            $.ajax({
                url: "ajaxGetLastAgent.php",
                type: 'post',
                data: {
                    "hfLeadId": lead_id
                },
                datatype: 'html',
                success: function(rsp) {
                    if (sel != rsp && rsp != 0) {
                        alert('Alert: Agent Changed.');
                    }
                }
            });
        }
        if ($('#hfLeadCommentUType').attr('value') == 'CTL') {
            $.ajax({
                url: "ajaxGetLastTL.php",
                type: 'post',
                data: {
                    "hfLeadId": lead_id
                },
                datatype: 'html',
                success: function(rsp) {
                    if (sel != rsp && rsp != 0) {
                        alert('Alert: Team Lead Changed.');
                    }
                }
            });
        } else if ($('#hfLeadCommentUType').attr('value') == 'C') {
            $.ajax({
                url: "checkClientRejectedLead.php",
                type: 'post',
                data: {
                    "hfLeadId": lead_id,
                    "hfSelectedClient": sel
                },
                datatype: 'html',
                success: function(rsp) {
                    if (rsp != 0) {
                        $('.d_aman').html("This client has already rejected the lead.<br/>Reason: " + rsp);
                        $('.d_aman').css('display', 'block');
                    } else {
                        $('.d_aman').css('display', 'none');
                    }
                }
            });
        } else if ($('#hfForCallcenter').attr('value') == 1) {
            /* $('#ddlGetTeamLeads').removeAttr('required');
            $.ajax({
                    url: "checkCallcenterIsAffliated.php", 
                    type: 'post',
                    data:  {"ddlCallCenterId":sel},
                    datatype: 'html',
                    success: function(is_affliated)
                    {
                        if(is_affliated==1)
                        {
                            $(".teamleads").empty();
                            $('#ddlGetTeamLeads').attr('required','required');
                            $('#hfLeadCommentUType').attr('value','CCA');
                        }
                        else
                        {
                            $.ajax({
                                url: "getTeamLeads.php", 
                                type: 'post',
                                data:  {"hfLeadId":lead_id,"ddlCallCenterId":sel},
                                datatype: 'html',
                                success: function(rsp)
                                {
                                    $('.teamleads').html(rsp);
                                    $('.teamleads').css('display','block');
                                    $('#hfLeadCommentUType').attr('value','CTL');
                                }
                            });
                        }
                    }
                }); */
        }
    });

    $('body').on("click", '.removeTextBoxCCEmail', function() {
        $(this).parent().parent().remove();
    });



    $('body').on("click", '.row.page-titles', function() {
        if ($(this).hasClass('collapsed')) {
            var contentWraperHeight = $(document).height();
            var screenHeight = $('.table-responsive.viewLead .hh5').height();
            var collapseAccHeight = $('.viewLeadAcc').height();
            var contentWraperMainHeight = contentWraperHeight - 324;
            var finalHeight = contentWraperMainHeight - collapseAccHeight;
            // $('.table-responsive.viewLead .hh5').css({ "max-height": screenHeight - collapseAccHeight });
            $('.table-responsive.viewLead .hh5').css({
                "max-height": finalHeight
            });
        } else {
            $('.table-responsive.viewLead .hh5').css({
                "max-height": "calc(100vh - 220px)"
            });
        }
    });



    $('body').on("click", "#openSelect1", function(e) {
        $('#openSelect1 + select.form-control').addClass('onClickSelect');
        $('#openSelect1').css('opacity','0');
    });

    $('body').click(function(e) {
        var t = $(e.target);
        if (e.target.id == "openSelect1" || t.is("option")) {
        } else {
            $('#openSelect1 + select.form-control').removeClass('onClickSelect');
            $('#openSelect1').css('opacity','1');
        }
    });



    $('body').on("click", "#openSelect2", function(e) {
        $('#openSelect2 + select.form-control').addClass('onClickSelect');
        $('#openSelect2').css('opacity','0');
    });

    $('body').click(function(e) {
        var t = $(e.target);
        if (e.target.id == "openSelect2" || t.is("option")) {
        } else {
            $('#openSelect2 + select.form-control').removeClass('onClickSelect');
            $('#openSelect2').css('opacity','1');
        }
    });


});

function skip_pages(page_skip) {
    return (Math.floor(page_skip / 5)) * 5 + 1;
}

function callAjax(url_page, json_string, html_class) 
{
    $(".data-error").css('display', 'none');
    $(".data-success").css('display', 'none');
    $("#cover").css('display', '');

    $.ajax({
        url: url_page,
        type: 'post',
        data: json_string,
        datatype: 'html',
        success: function(rsp) {
            $(html_class).html(rsp);
        },
        error: function(jqXHR, status, err) {
            //alert("");
            console.log("Some error occurred. Please refresh the page and try again.");
        },
        complete: function(jqXHR, status) {
            $("#cover").css('display', 'none');
        }
    });
}

function callLeadAjax(url_page, json_string, html_class, num) {
    $(".data-error").css('display', 'none');
    $(".data-success").css('display', 'none');
    $("#cover").css('display', '');

    $.ajax({
        url: url_page,
        type: 'post',
        data: json_string,
        datatype: 'html',
        success: function(rsp) {
            $(html_class).html(rsp);

            if (saveClose() != 1) {
                callfunction(num);
            }
			else
			{
				window.location.href = 'view-leads.php';
			}

        },
        error: function(jqXHR, status, err) {
            alert("Some error occurred. Please refresh the page and try again.");
        },
        complete: function(jqXHR, status) {
            $("#cover").css('display', 'none');
        }
    });
}

function getAllSuburbs(state_id) {
    $(".data-error").css('display', 'none');
    $(".data-success").css('display', 'none');

    var postData = {
        "ddlState": state_id
    };
    callAjax('ajax_get_suburbs.php', postData, '#ddlSubUrb');
}

function getAllSuburbsWitness(state_id) {
    $(".data-error").css('display', 'none');
    $(".data-success").css('display', 'none');

    var postData = {
        "ddlState": state_id
    };
    callAjax('ajax_get_suburbs.php', postData, '#ddl9SubUrb');
}

function getLeadSuburbs(state_id, set_id) {
    $(".data-error").css('display', 'none');
    $(".data-success").css('display', 'none');

    var postData = {
        "ddlState": state_id
    };
    callAjax('ajax_get_suburbs.php', postData, '#ddlSubUrb' + set_id);
}

function check_isset(the_id) {
    if ($(the_id).length == 1) {
        return $(the_id).val();
    } else {
        return null;
    }
}


function getPagination(page_skip) 
{
    var txtSearch = $('#txtSearch').val();
    var txtFromDate = $('#txtFromDate').val();
    var txtToDate = $('#txtToDate').val();
    var firstpage = skip_pages(page_skip);
    var hfAjaxPage = $('#hfAjaxPage').val();
    var txtAccidentDateSearchFrom  = $('#txtAccidentDateSearchFrom').val();
    var txtAccidentDateSearchTo  = $('#txtAccidentDateSearchTo').val();
    var t = 0;
    if ($('#t_val').length == 1) {
        t = $('#t_val').val();
    }
	
	var val = [];
	$('.checkList_checkbox:checked').each(function(i) {
		val[i] = $(this).val();
	});
	
    if ($('#ddlSortBy').length == 1) 
	{
        $('#btnDeleteMultiple').attr('disabled', true);
        var ddlSortBy = $('#ddlSortBy').val();
        var ddlSubUrb = $('#ddlSubUrb').val();
        var ddlState = $('#ddlState').val();

        var ddlLCallcenters = check_isset('#ddlLCallcenters');
        var ddlVerifier = check_isset('#ddlVerifier');
        var txtPP = check_isset('#txtPP');
        var ddlLeadStatus = $('#ddlLeadStatus').val(); //array
        var ddlClients = $('#ddlClients').val(); //array
        var ddlAgents = check_isset('#ddlAgents');
        var ddlTeamLeads = check_isset('#ddlTeamLeads');
        var ddlAgentCheck = check_isset('#ddlAgentCheck');
        var ddlTeamLeadCheck = check_isset('#ddlTeamLeadCheck');
        var ddlWasRejected = check_isset('#ddlWasRejected');
        var ddlWasAssignedToVerifier = check_isset('#ddlWasAssignedToVerifier');

        var postData = {
            "txtSearch": txtSearch,
            "txtToDate": txtToDate,
            "txtFromDate": txtFromDate,
            "page_skip": page_skip,
            "firstpage1": firstpage,
            "t": t,
            "ddlSortBy": ddlSortBy,
            "ddlState": ddlState,
            "ddlSubUrb": ddlSubUrb,
            "ddlClients": ddlClients,
            "ddlLCallcenters": ddlLCallcenters,
            "ddlLeadStatus": ddlLeadStatus,
            "ddlTeamLeads": ddlTeamLeads,
            "ddlAgents": ddlAgents,
            "ddlAgentCheck": ddlAgentCheck,
            "ddlTeamLeadCheck": ddlTeamLeadCheck,
            "txtAccidentDateSearchFrom": txtAccidentDateSearchFrom,
            "txtAccidentDateSearchTo": txtAccidentDateSearchTo,	
            "ddlVerifier": ddlVerifier,
            "txtPP": txtPP,
			"chckCheckList": val,
			"ddlWasRejected": ddlWasRejected,
			"ddlWasAssignedToVerifier": ddlWasAssignedToVerifier
        };
    } 
	else {
        var postData = {
            "txtAccidentDateSearchFrom": txtAccidentDateSearchFrom,
            "txtAccidentDateSearchTo": txtAccidentDateSearchTo,
            "txtSearch": txtSearch,
            "txtToDate": txtToDate,
            "txtFromDate": txtFromDate,
            "page_skip": page_skip,
            "firstpage1": firstpage,
            "t": t,
            "ddlVerifier": ddlVerifier,
            "txtPP": txtPP,
			"chckCheckList": val,
			"ddlWasRejected": ddlWasRejected,
			"ddlWasAssignedToVerifier": ddlWasAssignedToVerifier
        };
    }

    callAjax(hfAjaxPage, postData, '.ajaxClass');
}

function getPagination_BCK(page_skip) 
{
    var txtSearch = $('#txtSearch').val();
    var txtFromDate = $('#txtFromDate').val();
    var txtToDate = $('#txtToDate').val();
    var firstpage = skip_pages(page_skip);
    var hfAjaxPage = $('#hfAjaxPage').val();
    var txtAccidentDateSearchFrom  = $('#txtAccidentDateSearchFrom').val();
    var txtAccidentDateSearchTo  = $('#txtAccidentDateSearchTo').val();
    var t = 0;
    if ($('#t_val').length == 1) {
        t = $('#t_val').val();
    }
	
	var val = [];
	$('.checkList_checkbox:checked').each(function(i) {
		val[i] = $(this).val();
	});
	
    if ($('#ddlSortBy').length == 1) 
	{
        $('#btnDeleteMultiple').attr('disabled', true);
        var ddlSortBy = $('#ddlSortBy').val();
        var ddlSubUrb = $('#ddlSubUrb').val();
        var ddlState = $('#ddlState').val();

        var ddlLCallcenters = check_isset('#ddlLCallcenters');
        var ddlVerifier = check_isset('#ddlVerifier');
        var txtPP = check_isset('#txtPP');
        var ddlLeadStatus = $('#ddlLeadStatus').val(); //array
        var ddlClients = $('#ddlClients').val(); //array
        var ddlAgents = check_isset('#ddlAgents');
        var ddlTeamLeads = check_isset('#ddlTeamLeads');
        var ddlAgentCheck = check_isset('#ddlAgentCheck');
        var ddlTeamLeadCheck = check_isset('#ddlTeamLeadCheck');

        var postData = {
            "txtSearch": txtSearch,
            "txtToDate": txtToDate,
            "txtFromDate": txtFromDate,
            "page_skip": page_skip,
            "firstpage1": firstpage,
            "t": t,
            "ddlSortBy": ddlSortBy,
            "ddlState": ddlState,
            "ddlSubUrb": ddlSubUrb,
            "ddlClients": ddlClients,
            "ddlLCallcenters": ddlLCallcenters,
            "ddlLeadStatus": ddlLeadStatus,
            "ddlTeamLeads": ddlTeamLeads,
            "ddlAgents": ddlAgents,
            "ddlAgentCheck": ddlAgentCheck,
            "ddlTeamLeadCheck": ddlTeamLeadCheck,
            "txtAccidentDateSearchFrom": txtAccidentDateSearchFrom,
            "txtAccidentDateSearchTo": txtAccidentDateSearchTo,	
            "ddlVerifier": ddlVerifier,
            "txtPP": txtPP,
			"chckCheckList": val
        };
    } 
	else {
        var postData = {
            "txtAccidentDateSearchFrom": txtAccidentDateSearchFrom,
            "txtAccidentDateSearchTo": txtAccidentDateSearchTo,
            "txtSearch": txtSearch,
            "txtToDate": txtToDate,
            "txtFromDate": txtFromDate,
            "page_skip": page_skip,
            "firstpage1": firstpage,
            "t": t,
            "ddlVerifier": ddlVerifier,
            "txtPP": txtPP,
			"chckCheckList": val
        };
    }

    callAjax(hfAjaxPage, postData, '.ajaxClass');
}

function getCharts(page_skip) 
{
     var postData = {
		"page_skip": page_skip
	};
    callAjax("ajax_get_cc_agent_charts.php", postData, '.agent_row');
}

function is_disabled(is_dis) {
    if (is_dis == 1) {
        $("input,textarea,select").attr('disabled', 'disabled');
        $("#btnGeneratePassword").removeAttr('disabled');
    }
}

function getUrlParameter(sParam) {
    // alert(getUrlParameter('blog'));
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName, i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

function getAllPersonalDetailsLeadSuburbs(state_id) {
    $(".data-error").css('display', 'none');
    $(".data-success").css('display', 'none');

    var postData = {
        "ddlState": state_id
    };
    callAjax('ajaxLeadPersonalDetailsSuburbs.php', postData, '#ddl1SubUrb');
    noneSelcet(1); // To hide the suburb name textbox
}

function callfunction(num) {
    switch (num) {
        case 1:
            displayOne();
            break;
        case 2:
            displayTwo();
            break;
        case 3:
            displayThree();
            break;
        case 4:
            displayFour();
            break;
        case 5:
            displayFive();
            break;
        case 6:
            displaySix();
            break;
        case 7:
            displaySeven();
            break;
        case 8:
            displayEight();
            break;
        case 9:
            displayNine();
            break;
    }
}

function deleteUser(uid) {
    if (confirm("Are you sure?")) {
        var postData = {
            "uid": uid
        };
        callAjax('deleteUser.php', postData, '.ajaxdummyClass');
        $("#delete" + uid).hide();
        getPagination($(".page-item.active a").attr('tabindex'));
    }
}

function deleteClient(id) {
    if (confirm("Are you sure?")) {
        var postData = {
            "id": id
        };
        callAjax('deleteUser.php', postData, '.ajaxdummyClass');
        $("#delete" + id).hide();
        getPagination($(".page-item.active a").attr('tabindex'));
    }
}

function deleteLead(id) {
    var path = window.location.pathname;
    var page = path.split("/").pop();

    var from_reports = 1;
    if (page == 'view-leads.php') {
        from_reports = 0;
    }
    if (confirm("Are you sure?")) {
        var postData = {
            "id": id,
            "from_reports": from_reports
        };
        callAjax('deleteLead.php', postData, '.ajaxdummyClass');
        $("#delete" + id).hide();
        $("#deletex" + id).hide();
        getPagination($(".page-item.active a").attr('tabindex'));
    }
}

function deleteMultipleLeads() {
    if (confirm("Are you sure?")) {
        var val = [];
        $('.delete_checkbox:checked').each(function(i) {
            val[i] = $(this).val();
        });
        var postData = {
            "chckDeleteMultiple": val
        };
        callAjax('multipledeleteLead.php', postData, '.ajaxdummyClass');
        getPagination($(".page-item.active a").attr('tabindex'));
    }
}

function selectAll() {
	
	var n = $('#chckSelectCheckBox').prop('checked');
    if(n == true) {
        $(".delete_checkbox").prop('checked', true);
        $('#btnDeleteMultiple').removeAttr('disabled');
    } else {
        $(".delete_checkbox").prop('checked', false);
        $('#btnDeleteMultiple').attr('disabled', true);
    }
}

function selectAllAssignVerifier() {
	
	var n = $('#chckSelectCheckBoxAssignVerifier').prop('checked');
    if(n == true) {
        $(".chckAssignVerifierMultiple").prop('checked', true);
        $('#btnAssignVerifier').removeAttr('disabled');
    } else {
        $(".chckAssignVerifierMultiple").prop('checked', false);
        $('#btnAssignVerifier').attr('disabled', true);
    }
}

function c_undefine(val) {
    if (val == undefined) {
        return null;
    }
    return val;
}

function callAccidentAjax(url_page, json_string, lead_id, x) {
    $(".data-error").css('display', 'none');
    $(".data-success").css('display', 'none');
    $("#cover").css('display', '');

    $.ajax({
        url: url_page,
        type: 'post',
        data: json_string,
        datatype: 'html',
        success: function(rsp) {
            if (rsp != 0) {
                if (saveClose() != 1) {
                    window.location.href = 'edit-lead-accidents.php?aid=' + rsp + '&lid=' + lead_id;
                }
            } else {
                if (x < 2) //Try for 2 times
                {
                    x++;
                    callAccidentAjax(url_page, json_string, lead_id, x);
                } else {
                    window.location.href = 'view-leads.php';
                }
            }
        },
        error: function(jqXHR, status, err) {
            console.log(jqXHR);
            console.log(status);
            console.log(err);
        },
        complete: function(jqXHR, status) {
            $("#cover").css('display', 'none');
        }
    });
}

function callLeadAccidentAjax(url_page, json_string, html_class, num) {
    $(".data-error").css('display', 'none');
    $(".data-success").css('display', 'none');
    $("#cover").css('display', '');

    $.ajax({
        url: url_page,
        type: 'post',
        data: json_string,
        datatype: 'html',
        success: function(rsp) {
            callAccidentAjax('ajax_get_last_accident.php', {
                "hfLeadId": $("#hfLeadId").val()
            }, $("#hfLeadId").val(), 0);
        },
        error: function(jqXHR, status, err) {
            console.log(jqXHR);
            console.log(status);
            console.log(err);
        },
        complete: function(jqXHR, status) {
            $("#cover").css('display', 'none');
        }
    });
}

function callAjaxAlert(url_page, json_string, html_class) {
    $(".data-error").css('display', 'none');
    $(".data-success").css('display', 'none');
    $("#cover").css('display', '');

    $.ajax({
        url: url_page,
        type: 'post',
        data: json_string,
        datatype: 'html',
        success: function(rsp) {
          alert(rsp);
        },
        error: function(jqXHR, status, err) {
            console.log(jqXHR);
            console.log(status);
            console.log(err);
        },
        complete: function(jqXHR, status) {
            $("#cover").css('display', 'none');
        }
    });
}

function saveClose() {
    var open_modal = $('#openModal_val').attr('value');
     if (open_modal == 1) {
        /* $('#saveCloseModal').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        }); */
		
        return 1;
    }  
    return 0;
}

/* // Month//Day/Year
alert(isDate("02/28/2019")); */

function isDate(date) {
    var regex = /\d{1,2}\/\d{1,2}\/\d{4}/;
    if (!regex.test(date)) return 0;
    var day = Number(date.split("/")[1]);
    date = new Date(date);
    if (date && date.getDate() != day) return 0;
    return 1;
}

function GetTodayDate() {
    var tdate = new Date();
    var dd = tdate.getDate(); //yields day
    var MM = tdate.getMonth(); //yields month
    var yyyy = tdate.getFullYear(); //yields year
    var currentDate = dd + "-" + (MM + 1) + "-" + yyyy;

    return currentDate;
}

function getCurentFileName() {
    //var pagePathName= window.location.pathname;
    //return pagePathName.substring(pagePathName.lastIndexOf("/") + 1);
    var loc = window.location.href;
    return loc.split('/').pop().split('.').shift()
}

function getAllLeadSuburbsLocations(state_id, view_val) {
    $(".data-error").css('display', 'none');
    $(".data-success").css('display', 'none');

    var postData = {
        "ddlState": state_id
    };

    callAjax('ajaxLeadPersonalDetailsSuburbs.php', postData, '#ddlSubUrb' + view_val);
    noneSelcetAccident(1);
}

function view_more_leads(copy_id) {
    var postData = {
        "copy_id": copy_id
    };
    callAjax('ajaxCopyLeads.php', postData, '#copyleaddata');
    $('#copymodal').modal('show');
}

function copyLeadPopUp(lead_id) {
    $('#txtCopyName').val('');
    $('#txtCopyNumber').val('');
    $('#txtCopyLeadId').val(lead_id);
    $('.txtCopyLeadId').html(lead_id);
    $('#copymodalShow').modal('show');
    $("#frmCopyLead").validate().resetForm();
}

function appendTextBoxCCEmail() {
    $('#appendTextBoxCCOuter').append('<div class="row appendTextBoxCCEmail"><div class="col-md-8 col-10 parentr"><div class="form-group"><input value="" class="form-control" name="txtCCEmails[]" id="txtCCEmails1" type="email" /></div></div><div class="col-md-4 col-2"><i class="addClientsBox bg-danger fa fa-minus removeTextBoxCCEmail"></i></div></div>')
}

function copyClipbord(str) {
    str = $.trim(str);
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

function sign_val_copy(id_val) {
    id_val = parseInt(id_val);
    copyClipbord($('#' + id_val + "signup").attr('a_val'));
}

function callback_val_copy(id_val) {
    id_val = parseInt(id_val);
    copyClipbord($('#' + id_val + "callback").attr('a_val'));
}

function deleteLeadView(id) 
{
    if (confirm("Are you sure?")) {
        var postData = {
            "id": id
        };
        callAjax('deleteLeadView.php', postData, '.ajaxdummyClass');
        $("#delete" + id).hide();
        $("#deletex" + id).hide();
    }
}

function changeLeadWorkingStatus(msw_lead_type,id) 
{
    if (confirm("Are you sure?")) 
	{
        var postData = {
            "id": id,
			"msw_lead_type":msw_lead_type
        };
        callAjax('setNewWorkingStatus.php', postData, '.ajaxdummyClass');
        $("#delete" + id).hide();
		$("#deletex" + id).hide();
    }
}

function setAttachments(id) 
{
	 var postData = {
            "txtLeadId": id
        };
	callAjax('getAttachments.php', postData, '.attachments');
	$("#attachmentsModal").modal('show');
}

function setCheckList(id) 
{
	 var postData = {
            "txtLeadId": id
        };
	callAjax('getCheckList.php', postData, '.checkList');
	$("#checkListModal").modal('show');
}

function view_logs(id) 
{
	 var postData = {
            "txtLeadId": id
        };
	callAjax('logs_modal.php', postData, '.showLeadLogs');
	$("#logsModal").modal('show');
}

function pull_from_anyone(id, assigned_user) 
{
	if (confirm("Are you sure?")) 
	{
		var postData = {
            "txtLeadId": id,
			"is_post":1,
			"txtAssignedUser":assigned_user
        };
		callAjax('reassign_lead.php', postData, '');
		$("#delete" + id).hide();
		$("#deletex" + id).hide();
		getPagination($(".page-item.active a").attr('tabindex'));
		alert("Lead has been pulled and moved to fresh leads.");
	}
}

function openNotes(id) 
{
	$('#hfNotesId').val(id);
	$('#hfNotesIdSpan').text(id);
	$('#txtSaveCloseDetailsNotesL').val('');
	$('#saveCloseModalNotesL').modal('show');
}

function showEmailStates()
{
	var x = $('option:selected', "#ddlEmailTemplates").attr('mytag');
	$(".myemailclass").css("display", "none");
	$("#ddlEmailState").val("");
	
	if(x==1)
	{
		$("#ddlEmailState").rules("add", "required");
		$(".myemailclass").css("display", "block");
	}
	else
	{
		$("#ddlEmailState").rules('remove', 'required');
	}
	$('#frmEmailTemplate').validate().resetForm();
}

function deleteUploadFile(num,delId,txtLeadId) 
{
    if (confirm("Are you sure?")) {
        var postData = {
            "num": num,
            "delId": delId,
            "txtLeadId": txtLeadId
        };
		callAjax('getAttachments.php', postData, '.attachments');
    }
}


function setNavigationNotifications() 
{
    $.ajax({
        url: "setNavigationNotifications.php",
        type: 'post',
        data: {},
        datatype: 'json',
        success: function(rsp) 
		{
			var obj = JSON.parse(rsp);
			if(obj.f > 0)
			{
				$(".f_notify").html(obj.f);
			}
			if(obj.a > 0)
			{
				$(".a_notify").html(obj.a);
			}
			if(obj.w > 0)
			{
				$(".w_notify").html(obj.w);
			}
        }
    });
}

function selectChartCustom()
{
	var a = $("#ddlSelectType").val();
	if(a==5)
	{
		$('.txtFromDate').css('display','');
		$('.txtToDate').css('display','');
		$('#txtFromDate').val('');
		$('#txtToDate').val('');
		$("#txtFromDate").rules('add','required');
		$("#txtToDate").rules('add','required');
	}
	else
	{
		$('.txtFromDate').css('display','none');
		$('.txtToDate').css('display','none');
		$("#txtFromDate").rules('remove','required');
		$("#txtToDate").rules('remove','required');
	}
	$('#frmChartSearch').validate().resetForm();
}

function getLeadSuburbsAll(state_id) {
    $(".data-error").css('display', 'none');
    $(".data-success").css('display', 'none');

    var postData = {
        "ddlState": state_id
    };
    callAjax('ajax_get_all_suburbs.php', postData, '#ddlSubUrb');
}

function setIsViewLeadsSelected()
{
	$('#hfIsViewLeadsSelected').val(1);
}

function resetIsViewLeadsSelected()
{
	$('#hfIsViewLeadsSelected').val(0);
}