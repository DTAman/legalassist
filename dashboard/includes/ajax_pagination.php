<div aria-label="Page navigation" class="pull-right">
	<ul class="pagination">
	<?php
	if(($last_page-1)!=0)
	{
		?>
			<li class="page-item">
				<a class="page-link waves-effect firstpage" href="javascript:void(0)" tabindex="<?php echo $last_page-1 ?>" aria-label="Previous">
					<span aria-hidden="true">«</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>
		<?php	
	}
	
	$total=ceil($totalCount/$limit);
	while($pagecount<=$total && $p<=$set_count)
	{
		if($first_page==0)
		{
			$class_li = "class_li";
			$first_page = $pagecount;
			$first_index = "first_index=".$first_page;
		}
		else
		{
			$first_index='';
		}
		?>
			<li id="li_page<?php echo $pagecount ?>" class="page-item <?php if($pagecount==$_POST['page_skip']) { echo "active"; } ?>">
				<a class="page-link waves-effect" href="javascript:void(0)" role="button" tabindex="<?php echo $pagecount ?>" onclick='getPagination(<?php echo $pagecount ?>)' href="javascript:void(0)" <?php echo $first_index ?> ><?php echo $pagecount; ?>
				</a>
			</li>
		<?php
		$pagecount++;
		$p++;
	}
	
	if($total>=$pagecount)
	{
		?>
			<li class="page-item">
				<a class="page-link waves-effect nextpage" tabindex="<?php echo $pagecount ?>" href="javascript:void(0)" aria-label="Next">
				<span aria-hidden="true">»</span>
				<span class="sr-only">Next</span>
				</a>
			</li>
		<?php
	}
	?>

	</ul>
</div>