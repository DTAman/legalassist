<?php 
ini_set('max_execution_time', 1000);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Injury Assist</title>

<link href="<?php echo $mysiteurl ?>css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo $mysiteurl ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $mysiteurl ?>css/themify.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $mysiteurl ?>css/morris.css" rel="stylesheet" />
<link href="<?php echo $mysiteurl ?>css/animate.css" rel="stylesheet" />
<link href="<?php echo $mysiteurl ?>css/style.css" rel="stylesheet" />
<link href="<?php echo $mysiteurl ?>css/style-responsive.css" rel="stylesheet" />
<link href="<?php echo $mysiteurl ?>css/default.css" id="jssDefault" rel="stylesheet" />
<link href="<?php echo $mysiteurl ?>css/bootstrap-material-datetimepicker.css" id="jssDefault" rel="stylesheet" />