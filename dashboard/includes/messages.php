<?php

if(isset($_SESSION['success']) && trim($_SESSION['success'])!=null)
{
	?>
		<div class="alert alert-success">
			<?php echo $_SESSION['success']; ?>
		</div>
	<?php
	unset($_SESSION['success']);
}
else if(isset($_SESSION['error']) && trim($_SESSION['error'])!=null)
{
	?>
		<div class="alert alert-danger">
			<?php echo $_SESSION['error']; ?>
		</div>
	<?php
	unset($_SESSION['error']);
}
?>

<div class="alert alert-success data-success" style='display:none'>
	Data is saved.
</div>
<div class="alert alert-danger data-error" style='display:none'>
	Error while saving data.
</div>