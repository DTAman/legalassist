<?php
if(!isset($_SESSION['userType']) || !isset($_SESSION['userId']))
{
	header("location:".$mysiteurl.'logout.php');
}
else
{
    ?>
        
<nav class="navbar navbar-expand-lg bb-1 navbar-light bg-white fixed-top" id="mainNav">
	
	<!-- Start Header -->
	<header class="header-logo bg-white bb-1 br-1">
		<a class="nav-link text-center mr-lg-3 d-none" id="sidenavToggler"><i class="ti-align-left"></i></a>
		<a class="gredient-cl navbar-brand d-lg-block d-none" href="index.php"><img src="<?php echo $mysiteurl ?>img/logo-white.png" alt=""></a>
		<a class="gredient-cl navbar-brand d-lg-none" href="index.php"><img src="<?php echo $mysiteurl ?>img/logo.png" alt=""></a>

		<ul class="ml-auto navbar-nav d-none d-lg-flex align-items-md-center">
		  
			<?php
			/* if(isset($_SESSION["userType"]) && trim($_SESSION["userType"])==$C)
			{
				$datamasterX = $mysqli->prepare("SELECT * FROM sub_clients where user_status='AC' and client_id = ?");
				$datamasterX->bind_param('i',$_SESSION['userId']);
				$datamasterX->execute();
				$dataresultX = $datamasterX->get_result();
				$datamasterX->close();

				if(mysqli_num_rows($dataresultX)==0)
				{
					?>
						<li class="nav-item switchOn"><p>Status</p>
							<label class="switch" style="position: relative;display: inline-block;width: 60px;height: 20px;float:right;margin-bottom: 0;border-radius: 20px;">
								<input <?php echo isset($_SESSION["userIsOnline"]) && trim($_SESSION["userIsOnline"])==1?'checked':'' ?> name="switchOnOff" type="checkbox" class="success switchOnOff" id="switchOnOff" autocomplete="0.8609729207597505" style="display: none;">
								<span class="slider round"></span>
							</label>
						</li>
					<?php
				}
				else
				{
					while($xxa=mysqli_fetch_object($dataresultX))
					{
						?>
						<li class="nav-item switchOn">
							<p><?php echo $xxa->name ?> </p>
							<label class="switch" style="position: relative;display: inline-block;width: 60px;height: 20px;float:right;margin-bottom: 0;border-radius: 20px;">
								<input attr_user='<?php echo $xxa->id ?>' <?php echo $xxa->online_status==1?'checked':'' ?> name="switchOnOff" type="checkbox" class="success switchOnOffY" id="switchOnOff" autocomplete="0.8609729207597505" style="display: none;">
								<span class="slider round"></span>
							</label>
						</li>
					<?php
					}
				}
			} */
			?>
		  
			<?php $page_name = basename($_SERVER['PHP_SELF']); ?>
		  
			<!-- Start Dashboard-->
			<li class="nav-item <?php echo $page_name=='index.php'?'active':''; ?>">
			  <a class="nav-link "  href="index.php" >
				<span class="nav-link-text">Dashboard</span>
			  </a>
			
			</li>
			<!-- End Dashboard -->

			<!-- Start Manage Lead -->

			<?php
			/* if($_SESSION['userType']!='CCA'  || ($_SESSION['userType']=='CCA' && $_SESSION["is_affliated"]==1))
			{ */
				?>
				<li class="nav-item dropdown <?php echo ($page_name=='add-lead.php') || ($page_name=='view-leads.php') || ($page_name=='edit-lead-each.php') || ($page_name=='edit-lead-accidents.php')?'active':''; ?>">

				  <a class="nav-link dropdown-toggle" href="#" id="manageLeads" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage Leads
					</a>
				  
				<?php
					if($_SESSION['userType']!='C')
					{
						?>
					        <div class="dropdown-menu" aria-labelledby="manageLeads">
					        	<?php
									if(($_SESSION['userType']=='CCA' && $_SESSION["is_affliated"]==1) || $_SESSION['userType']=='CA')
									{
										?>
											<a class="dropdown-item" href="add-lead.php">Add Lead</a>
										<?php
									}
									if($_SESSION['userType']!='MSW')
									{
										?>
											<a class="dropdown-item" href="view-leads.php?pp=x">View/Edit Lead</a>
										<?php
									}
									else
									{
										?>
											<a class="dropdown-item" href="view-leads.php?pp=f">Fresh Leads</a>
											<a class="dropdown-item" href="view-leads.php?pp=w">Working Portal</a>
											<a class="dropdown-item" href="view-leads.php?pp=v">Verifier Leads</a>
											<a class="dropdown-item" href="view-leads.php?pp=s">Solicitor Leads</a>
											<a class="dropdown-item" href="view-leads.php?pp=a">Approved Leads</a>
											<a class="dropdown-item" href="view-leads.php?pp=i">Invoice Sent</a>
										<?php
									}
								?>
					        </div>
						<?php
					}
					else
					{
						?>
							<div class="dropdown-menu" aria-labelledby="manageLeads">
								<a class="dropdown-item" href="view-leads.php">View Leads</a>
							</div>
						<?php
					}
					?>
				</li>
				<?php
			/* } */
			?>
			
			<!-- End Manage Lead -->

			<!-- Start Manage Team Lead -->

			<?php
			if($_SESSION['userType']=='CCA' && $_SESSION["is_affliated"]==0)
			{
				?>
					<li class="nav-item dropdown <?php echo (($page_name=='add-agent.php') || ($page_name=='view-agent.php') || ($page_name=='edit-agent-each.php')) && isset($_GET['t']) && trim($_GET['t'])==1?'active':''; ?>">
					  <a class="nav-link dropdown-toggle" href="#" id="manageTeamLeads" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage Team Leads
					</a>
					<div class="dropdown-menu" aria-labelledby="manageTeamLeads">
						<a class="dropdown-item" href="add-agent.php?t=1">Add Team Lead</a>
						<a class="dropdown-item" href="view-agent.php?t=1">View/Edit Team Lead</a>
					</div>					  
					</li>
				<?php
			}
			?>
			

			<!-- End Manage Team Lead -->

			<!-- Start Manage Call Center -->

			<?php
			if($_SESSION['userType']=='MSW')
			{
				?>
				<li class="nav-item dropdown <?php echo ($page_name=='add-call-center.php' || $page_name=='view-call-center.php' || $page_name=='edit-call-center-each.php')?'active':''; ?>">
				  <a class="nav-link dropdown-toggle" href="#" id="callCenterManager" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Call Center Manager
					</a>
				<div class="dropdown-menu" aria-labelledby="callCenterManager">
					<a class="dropdown-item" href="add-call-center.php">Add Call Center</a>
				  <a class="dropdown-item" href="view-call-center.php">View/Edit Call Center</a>
				</div>			  
			</li>
			<li class="nav-item dropdown <?php echo ($page_name=='add-verifier.php' || $page_name=='view-verifier.php' || $page_name=='edit-verifier-each.php')?'active':''; ?>">
				  <a class="nav-link dropdown-toggle" href="#" id="verifier" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Verifier
					</a>
				<div class="dropdown-menu" aria-labelledby="verifier">
					<a class="dropdown-item" href="add-verifier.php">Add Verifier</a>
				  <a class="dropdown-item" href="view-verifier.php">View/Edit Verifier</a>
				</div>			  
			</li>
			<?php
			}
			?>

			
			<?php
			if($_SESSION['userType']=='MSW')
			{
				?>
					<li class="nav-item dropdown <?php echo ($page_name=='add-client-name.php' || $page_name=='view-client-name.php' || $page_name=='edit-client-name-each.php')?'active':''; ?>" >
					  <a class="nav-link dropdown-toggle" href="#" id="manageClients" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage Sol
					</a>
					<div class="dropdown-menu" aria-labelledby="manageClients">
						  <a class="dropdown-item" href="add-client-name.php">Add Sol</a>
						  <a class="dropdown-item" href="view-client-name.php">View/Edit Sol</a>
						</div>	
					</li>
			<?php
			}
			?>
			<!-- End Manage Client Name -->

			<!-- Start Manage Agents -->

			<?php
			if($_SESSION['userType']=='CCA' && $_SESSION["is_affliated"]==0)
			{
				?>
				<li class="nav-item dropdown <?php echo (($page_name=='add-agent.php') || ($page_name=='view-agent.php') || ($page_name=='edit-agent-each.php')) && isset($_GET['t']) && trim($_GET['t'])==2?'active':''; ?>">
				  <a class="nav-link dropdown-toggle" href="#" id="manageAgents" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage Agents
					</a>
					<div class="dropdown-menu" aria-labelledby="manageAgents">
					  <a class="dropdown-item" href="add-agent.php?t=2">Add Agents</a>
					  <a class="dropdown-item" href="view-agent.php?t=2">View/Edit Agents</a>
					</div>
				  
				</li>
			<?php
			}
			?>
			
			<?php
				if($_SESSION['userType']!=$CA && $_SESSION['userType']!=$CTL)
				{
					?>
						<li class="nav-item <?php echo ($page_name=='reports.php')?'active':''; ?>">
							<a class="nav-link "  href="reports.php" >
								<span class="nav-link-text">Reports</span>
						  </a>
						</li>
					<?php
				}
			
				if($_SESSION['userType']==$C)
				{
					?>
						<li class="nav-item <?php echo ($page_name=='client_logs.php')?'active':''; ?>">
							<a class="nav-link "  href="client_logs.php" >
								<span class="nav-link-text">Rejection Logs</span>
						  </a>
						</li>
					<?php
				}
				if($_SESSION['userType']==$MSW)
				{
					?>
						<li class="nav-item <?php echo ($page_name=='deleted-leads.php')?'active':''; ?>">
							<a class="nav-link "  href="deleted-leads.php" >
								<span class="nav-link-text">Deleted Logs</span>
						  </a>
						</li>
					<?php
				}
				/********* AMANJOT ****************/
				if($_SESSION['userType']==$CA)
				{
					?>
						<li class="nav-item <?php echo ($page_name=='view-agent-deleted-leads.php')?'active':''; ?>">
							<a class="nav-link "  href="view-agent-deleted-leads.php" >
								<span class="nav-link-text">Deleted Logs</span>
						  </a>
						</li>
					<?php
				}
				/********* AMANJOT ****************/
				if($_SESSION['userType']==$MSW)
				{
					?>
						<li class="nav-item <?php echo ($page_name=='shift_data_msw.php' || $page_name=='shift_data_cca.php')?'active':''; ?>">
							<a class="nav-link" href="shift_data_<?php echo $_SESSION['userType']==$MSW?'msw':'cca' ?>.php">
								<span class="nav-link-text">Shift Data</span>
							</a>
						</li>
					<?php
				}
				else if($_SESSION['userType']==$CCA  && $_SESSION["is_affliated"]==0)
				{
					?>
						<li class="nav-item dropdown <?php echo ($page_name=='shift_data_cca_a.php') || ($page_name=='shift_data_cca_tl.php')?'active':''; ?>">
							<a class="nav-link dropdown-toggle" href="#" id="shiftLeads" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shift Leads</a>
							<div class="dropdown-menu" aria-labelledby="shiftLeads">
								<a class="dropdown-item" href="shift_data_cca_a.php">Shift Agents</a>
								<a class="dropdown-item" href="shift_data_cca_tl.php">Shift Team Leads</a>
							</div>
						</li>
					<?php
				}
				
				if($_SESSION['userType']==$CTL || $_SESSION['userType']==$CA || ($_SESSION['userType']==$CCA  && $_SESSION["is_affliated"]==1))
				{
					?>
						<li class="nav-item <?php echo ($page_name=='shift_data_reports.php')?'active':''; ?>">
							<a class="nav-link" href="shift_data_reports.php">
								<span class="nav-link-text">Shift Reports</span>
							</a>
						</li>
					<?php
				}
				
				if(($_SESSION['userType']==$CCA  && $_SESSION["is_affliated"]==0) || $_SESSION['userType']==$MSW)
				{
					?>
						<li class="nav-item <?php echo ($page_name=='total_shift_data_reports.php')?'active':''; ?>">
							<a class="nav-link" href="total_shift_data_reports.php">
								<span class="nav-link-text">Shift Reports</span>
							</a>
						</li>
					<?php
				}
			?>

			<li class="nav-item dropdown">
			  <a class="nav-link dropdown-toggle" href="#" id="logOutt" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			  <img src="<?php echo $mysiteurl ?>img/profile.svg" alt="user-img" width="36" class="img-circle"><span><?php echo $_SESSION['userMobile'] ?></span>
			  </a>
				<div class="dropdown-menu" aria-labelledby="logOutt">
				  <a class="dropdown-item" href="change-password.php">Change Password</a>
				  <a class="dropdown-item" href="../logout.php">Log Out</a>
				</div>
			  
			</li>
			<!-- End Leave Form -->
	    </ul>
	</header>
	<!-- End Header -->
	
	<?php
	if(($_SESSION['userType']==$CCA  && $_SESSION["is_affliated"]==0) || $_SESSION['userType']==$MSW)
	{
		?>
			<div class="status">
				<?php
					$datamasterX = $mysqli->prepare("SELECT * FROM sub_clients where user_status='AC'");
					$datamasterX->execute();
					$dataresultX = $datamasterX->get_result();
					$datamasterX->close();
					while($dataX = mysqli_fetch_object($dataresultX))
					{
						?>
							<p class="<?php echo $dataX->online_status==1?'':'red' ?>"><?php echo $dataX->name ?><span></span></p>
						<?php
					}
				?>
			</div>
		<?php
	}
	?>
	
	
	
	<button class="navbar-toggler navbar-toggler-right d-lg-none" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="ti-align-left"></span>
			</button>
	
	<div class="collapse navbar-collapse" id="navbarResponsive">
		 
		<!-- =============== Start Side Menu ============== -->
		<div class="navbar-side">
		  <ul class="navbar-nav navbar-sidenav d-lg-none" id="exampleAccordion">
		  
			<?php
		/*	if(isset($_SESSION["userType"]) && trim($_SESSION["userType"])==$C)
			{
				$datamasterX = $mysqli->prepare("SELECT * FROM sub_clients where user_status='AC' and client_id = ?");
				$datamasterX->bind_param('i',$_SESSION['userId']);
				$datamasterX->execute();
				$dataresultX = $datamasterX->get_result();
				$datamasterX->close();

				if(mysqli_num_rows($dataresultX)==0)
				{
					?>
						<li class="nav-item switchOn"><p>Status</p>
							<label class="switch" style="position: relative;display: inline-block;width: 60px;height: 20px;float:right;margin-bottom: 0;border-radius: 20px;">
								<input <?php echo isset($_SESSION["userIsOnline"]) && trim($_SESSION["userIsOnline"])==1?'checked':'' ?> name="switchOnOff" type="checkbox" class="success switchOnOff" id="switchOnOff" autocomplete="0.8609729207597505" style="display: none;">
								<span class="slider round"></span>
							</label>
						</li>
					<?php
				}
				else
				{
					while($xxa=mysqli_fetch_object($dataresultX))
					{
						?>
						<li class="nav-item switchOn"><p>Status</p>
							<?php echo $xxa->name ?> 
							<label class="switch" style="position: relative;display: inline-block;width: 60px;height: 20px;float:right;margin-bottom: 0;border-radius: 20px;">
								<input attr_user='<?php echo $xxa->id ?>' <?php echo $xxa->online_status==1?'checked':'' ?> name="switchOnOff" type="checkbox" class="success switchOnOffY" id="switchOnOff" autocomplete="0.8609729207597505" style="display: none;">
								<span class="slider round"></span>
							</label>
						</li>
					<?php
					}
				}
			}*/
			?>
		  
			<?php $page_name = basename($_SERVER['PHP_SELF']); ?>
		  
			<!-- Start Dashboard-->
			<li class="nav-item <?php echo $page_name=='index.php'?'active':''; ?>" data-toggle="tooltip" data-placement="right" title="Dashboard">
			  <a class="nav-link "  href="index.php" >
				<i class="ti i-cl-1 ti-dashboard"></i>
				<span class="nav-link-text">Dashboard</span>
			  </a>
			
			</li>
			<!-- End Dashboard -->

			<!-- Start Manage Lead -->

			<?php
			/* if($_SESSION['userType']!='CCA'  || ($_SESSION['userType']=='CCA' && $_SESSION["is_affliated"]==1))
			{ */
				?>
				<li class="nav-item <?php echo ($page_name=='add-lead.php') || ($page_name=='view-leads.php') || ($page_name=='edit-lead-each.php') || ($page_name=='edit-lead-accidents.php')?'active':''; ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Manage Leads">
				  <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#add-leadd" data-parent="#exampleAccordion" aria-expanded="false">
					<i class="ti i-cl-4 ti-plus"></i>
					<span class="nav-link-text">Manage Leads</span>
				  </a>
				  
				<?php
					if($_SESSION['userType']!='C')
					{
						?>
							<ul class="sidenav-second-level collapse bg-secondary" id="add-leadd" style="">
								<?php
									if(($_SESSION['userType']=='CCA' && $_SESSION["is_affliated"]==1) || $_SESSION['userType']=='CA')
									{
										?>
											<li class="nav-item">
												<a class="nav-link" href="add-lead.php">Add Lead</a>
											</li>
										<?php
									}
									?>
								<li class="nav-item">
									<a class="nav-link" href="view-leads.php">View/Edit Lead</a>
								</li>
							</ul>
						<?php
					}
					else
					{
						?>
							<ul class="sidenav-second-level collapse bg-secondary" id="add-leadd" style="">
								<li class="nav-item">
									<a class="nav-link" href="view-leads.php">View Leads</a>
								</li>
							</ul>
						<?php
					}
					?>
				</li>
				<?php
			/* } */
			?>
			
			<!-- End Manage Lead -->

			<!-- Start Manage Team Lead -->

			<?php
			if($_SESSION['userType']=='CCA' && $_SESSION["is_affliated"]==0)
			{
				?>
					<li class="nav-item <?php echo (($page_name=='add-agent.php') || ($page_name=='view-agent.php') || ($page_name=='edit-agent-each.php')) && isset($_GET['t']) && trim($_GET['t'])==1?'active':''; ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Manage Team Lead">
					  <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#add-log" data-parent="#exampleAccordion" aria-expanded="false">
						<i class="ti i-cl-4 ti-agenda"></i>
						<span class="nav-link-text">Manage Team Leads</span>
					  </a>
					  <ul class="sidenav-second-level collapse bg-secondary" id="add-log" style="">
						<li class="nav-item">
						  <a class="nav-link" href="add-agent.php?t=1">Add Team Lead</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link" href="view-agent.php?t=1">View/Edit Team Lead</a>
						</li>
					  </ul>
					  
					</li>
				<?php
			}
			?>
			

			<!-- End Manage Team Lead -->

			<!-- Start Manage Call Center -->

			<?php
			if($_SESSION['userType']=='MSW')
			{
				?>
				<li class="nav-item <?php echo ($page_name=='add-call-center.php' || $page_name=='view-call-center.php' || $page_name=='edit-call-center-each.php')?'active':''; ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Manage Call Center">
				  <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#callCenter" data-parent="#exampleAccordion" aria-expanded="false">
					<i class="ti i-cl-4 ti-tablet"></i>
					<span class="nav-link-text">CallCenter Manager</span>
				  </a>
				  <ul class="sidenav-second-level collapse bg-secondary" id="callCenter" style="">
					<li class="nav-item">
					  <a class="nav-link" href="add-call-center.php">Add Call Center</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="view-call-center.php">View/Edit Call Center</a>
					</li>
				  </ul>
			  
			</li>
			<?php
			}
			?>
			<!-- End Manage Call Center -->

			<!-- Start Manage HR -->

			<!--<li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Manage HR">
			  <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#addhr" data-parent="#exampleAccordion" aria-expanded="false">
				<i class="ti i-cl-4 ti-tablet"></i>
				<span class="nav-link-text">Manage HR</span>
			  </a>
			  <ul class="sidenav-second-level collapse bg-secondary" id="addhr" style="">
				<li class="nav-item">
				  <a class="nav-link" href="add-hr.php">Add HR</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="view-hr.php">View/Edit HR</a>
				</li>
			  </ul>
			  
			</li>-->

			<!-- End Manage HR -->

			<!-- Start Manage Client Name -->

			
			<?php
			if($_SESSION['userType']=='MSW')
			{
				?>
					<li class="nav-item <?php echo ($page_name=='add-client-name.php' || $page_name=='view-client-name.php' || $page_name=='edit-client-name-each.php')?'active':''; ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Manage Client Name">
					  <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#clientName" data-parent="#exampleAccordion" aria-expanded="false">
						<i class="ti i-cl-4 ti-user"></i>
						<span class="nav-link-text">Manage Sol</span>
					  </a>
					  <ul class="sidenav-second-level collapse bg-secondary" id="clientName" style="">
						<li class="nav-item">
						  <a class="nav-link" href="add-client-name.php">Add Sol</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link" href="view-client-name.php">View/Edit Sol</a>
						</li>
					  </ul>
					</li>
			<?php
			}
			?>
			<!-- End Manage Client Name -->

			<!-- Start Manage Agents -->

			<?php
			if($_SESSION['userType']=='CCA' && $_SESSION["is_affliated"]==0)
			{
				?>
				<li class="nav-item <?php echo (($page_name=='add-agent.php') || ($page_name=='view-agent.php') || ($page_name=='edit-agent-each.php')) && isset($_GET['t']) && trim($_GET['t'])==2?'active':''; ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Manage Agents">
				  <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#AgentsName" data-parent="#exampleAccordion" aria-expanded="false">
					<i class="ti i-cl-4 ti-user"></i>
					<span class="nav-link-text">Manage Agents</span>
				  </a>
				  <ul class="sidenav-second-level collapse bg-secondary" id="AgentsName" style="">
					<li class="nav-item">
					  <a class="nav-link" href="add-agent.php?t=2">Add Agents</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="view-agent.php?t=2">View/Edit Agents</a>
					</li>
				  </ul>
				  
				</li>
			<?php
			}
			?>
			
			<!-- End Manage Agents -->
		  
			<!-- Start Leave Form-->
			<!--<li class="nav-item <?php echo ($page_name=='leave-form.php')?'active':''; ?>" data-toggle="tooltip" data-placement="right" title="Leave Form">
			  <a class="nav-link "  href="leave-form.php" >
				<i class="ti i-cl-5  ti-clipboard"></i>
				<span class="nav-link-text">Leave Form</span>
			  </a>
			
			</li>-->
			
			<?php
				if($_SESSION['userType']!=$CA && $_SESSION['userType']!=$CTL)
				{
					?>
						<li class="nav-item <?php echo ($page_name=='reports.php')?'active':''; ?>" data-toggle="tooltip" data-placement="right" title="Reports">
							<a class="nav-link "  href="reports.php" >
								<i class="ti i-cl-5  ti-clipboard"></i>
								<span class="nav-link-text">Reports</span>
						  </a>
						</li>
					<?php
				}
				if($_SESSION['userType']==$C)
				{
					?>
						<li class="nav-item <?php echo ($page_name=='client_logs.php')?'active':''; ?>" data-toggle="tooltip" data-placement="right" title="Rejection Logs">
							<a class="nav-link "  href="client_logs.php" >
								<i class="ti i-cl-5  ti-clipboard"></i>
								<span class="nav-link-text">Rejection Logs</span>
						  </a>
						</li>
					<?php
				}
				if($_SESSION['userType']==$MSW)
				{
					?>
						<li class="nav-item <?php echo ($page_name=='deleted-leads.php')?'active':''; ?>" data-toggle="tooltip" data-placement="right" title="Deleted Logs">
							<a class="nav-link "  href="deleted-leads.php" >
								<i class="ti i-cl-5  ti-clipboard"></i>
								<span class="nav-link-text">Deleted Logs</span>
						  </a>
						</li>
					<?php
				}
			?>
			
			<li class="nav-item <?php echo ($page_name=='change-password.php')?'active':''; ?>" data-toggle="tooltip" data-placement="right" title="Change Password">
			  <a class="nav-link "  href="change-password.php" >
				<i class="ti i-cl-5  ti-key"></i>
				<span class="nav-link-text">Change Password</span>
			  </a>
			
			</li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Logout">
			  <a class="nav-link "  href="../logout.php" >
				<i class="ti i-cl-5 fa fa-power-off"></i>
				<span class="nav-link-text">Logout</span>
			  </a>
			
			</li>
			<!-- End Leave Form -->
			
			
		  </ul>
	  </div>
	 <!-- =============== End Side Menu ============== -->
	 
	  
	  <!-- =============== Header Rightside Menu ============== -->
	  <!-- <ul class="navbar-nav ml-auto d-lg-none">
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle mr-lg-0 user-img a-topbar__nav a-nav" id="userDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<img src="<?php echo $mysiteurl ?>img/user-10.jpg" alt="user-img" width="36" class="img-circle">
				<b class="f-size-17"><?php echo isset($_SESSION["userName"])?$_SESSION["userName"]:'User'; ?></b>
			</a>
			<ul class="dropdown-menu dropdown-user animated fadeInUp" aria-labelledby="userDropdown">
				<li class="align-items-center bg-busy d-flex dropdown-header">
					<div class="header-user-pic">
						<img src="<?php echo $mysiteurl ?>img/user-10.jpg" alt="user-img" width="36" class="img-circle">
					</div>
					<div class="header-user-det ml-0">
						<span class="a-dropdown__header-title"><?php echo isset($_SESSION["userName"])?$_SESSION["userName"]:'User'; ?></span>
					</div>
				</li>
				<li><a class="dropdown-item" href="change-password.php"><i class="ti-key"></i> Change Password</a></li>
				<li><a class="dropdown-item" href="../logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
			</ul>
		</li>
	  </ul> -->
	  <!-- =============== End Header Rightside Menu ============== -->
	</div>
</nav>
    <?php
}
?>

