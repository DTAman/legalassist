$(document).ready(function() 
{
	jQuery.extend(jQuery.validator.messages, {
		required: "This field is required.",
		remote: "This data is already used.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});

	jQuery.validator.addMethod("alphanumeric", function(value, element) {
			return this.optional(element) || /^[a-zA-Z0-9 ]+$/.test(value);
	});

	jQuery.validator.addMethod("only_alphanumeric", function(value, element) {
			return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
	});	
	
	jQuery.validator.addMethod("username_validate", function(value, element) {
			return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
	}); 
	
	jQuery.validator.addMethod("url_validation", function(value, element) {
			return this.optional(element) || /^[a-zA-Z0-9-]+$/.test(value);
	});
	
	jQuery.validator.addMethod("greaterStart", function (value, element, params) {
		return this.optional(element) || new Date(value) >= new Date($(params).val());
	},'Must be greater than start date.');	
	
	jQuery.validator.addMethod("lettersonly", function(value, element) {
		return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
	}, "Please enter alphabets only"); 
	
	$("#frmShiftData").validate();
	$("#frmLeadCallBack").validate();
	$("#frmChartSearch").validate();
	
	$("#frmLogin").validate({
        rules: {
			txtUserName: {
                required: true,
				digits: true,
				maxlength: 10
            },
            txtPassword: {
                required: true
            },
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		focusCleanup: true,
        messages: {
			txtUserName: {
                required: "Please enter valid mobile number",
            },
            txtPassword: {
                required: "Please provide a password"
            },
        }
    });
	
	$("#frmRejectApprovalModal").validate({
		errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;}
	});
	
	$("#frmRejectApprovalInvoiceModal").validate({
		errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;}
	});
	
	$("#frmForgotEmail").validate({
        rules: {
			txtForgotEmail: {
                required: true,
				email:true
            }
		},
		errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		focusCleanup: true,
        messages: {
			txtForgotEmail: {
                required: "Please enter email",
				email: "Enter a valid email",
				maxlength:"You can enter maximum 20 characters"
            }
        }
    });
	
	$("#frmChangePassword").validate({
        rules: {
			txtOldPassword: {
                required: true
            },
			txtNewPassword: {
                required: true,
				minlength:5
            },
			txtConfirmNewPassword: {
                required: true,
				equalTo: "#txtNewPassword",
				minlength:5
            }
		},
		errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		focusCleanup: true,
        messages: {
			txtOldPassword: {
                required: "Please provide a password"
            },
			txtNewPassword: {
                required: "Please provide a password",
				minlength:"Password should contain atleast 5 characters"
            },
			txtConfirmNewPassword: {
                required: "Please provide a password",
				equalTo: "Passwords don't match",
				minlength:"Password should contain atleast 5 characters"
            }			
        }
    });
	
	$("#frmAddHR").validate({
		rules: {
			ddlCallCenterName: {
                required: true
            },
			ddlState: {
                required: true
            },
			ddlSubUrb: {
                required: true
            },
			txtCallCenterPhone: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 15,
				remote: {
							type: 'post',
							url: 'ajax_check_mobile.php',
							data: {
								'txtNumber': function () { return $('#txtCallCenterPhone').val(); }
							},
							dataType: 'json'
						}
			},
			txtCallCenterAltPhone: {
				minlength: 8,
				maxlength: 15
			},
			txtCallCenterPostal: {
				required: true,
				minlength: 4,
				maxlength: 4
			},
			txtCallCenterAddress: {
				required: true
			},
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		focusCleanup: true,
        messages: {
			txtCallCenterPhone: {
				number: "Enter only numbers",
				remote: "This mobile number is already used"
            }	
        }
	});
	
	$("#frmAddCallCenter").validate({
		rules: {
			txtCallCenterName: {
                required: true,
				maxlength: 200
            },
			/* ddlState: {
                required: true
            },
			ddlSubUrb: {
                required: true
            }, */
			txtCallCenterPhone: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 15,
				remote: {
							type: 'post',
							url: 'ajax_check_mobile.php',
							data: {
								'txtNumber': function () { return $('#txtCallCenterPhone').val(); }
							},
							dataType: 'json'
						}
			},
			txtCallCenterAltPhone: {
				minlength: 8,
				maxlength: 15
			},
			/* txtCallCenterPostal: {
				required: true,
				minlength: 4,
				maxlength: 4
			}, */
			txtCallCenterAddress: {
				required: true
			},
			rbtnIsAffliated: {
				required: true
			},
			ddlCountry: {
				required: true
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		focusCleanup: true,
		errorPlacement: function(error, element) 
        {
            if ( element.is(":radio") ) 
            {
                error.insertAfter('.radioclass');
            }
            else 
            {
                error.insertAfter( element );
            }
        },
        messages: {
			txtCallCenterPhone: {
				number: "Enter only numbers",
				remote: "This mobile number is already used"
            }	
        }
	});
	
	$("#frmAddClients").validate({
		rules: {
			txtClientCompany: {
                required: true,
				maxlength: 100
            },
			txtClientName: {
                required: true,
				maxlength: 100
			},
			txtClientDesignation: {
                required: true,
				maxlength: 100
            },
			txtClientEmail: {
                required: true,
				maxlength: 255
            },
			ddlState: {
                required: true
            },
			ddlSubUrb: {
                required: true
            },
			txtClientPostal: {
				required: true,
				minlength: 4,
				maxlength: 4
			},
			txtClientAddress: {
				required: true
			},
			txtAddPhone: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 15,
				remote: {
							type: 'post',
							url: 'ajax_check_mobile.php',
							data: {
								'txtNumber': function () { return $('#txtAddPhone').val(); },
							},
							dataType: 'json'
						}				
			},
			txtClientEmail:
			{
				email: true, 
				required: true
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		focusCleanup: true,
		messages: {
			txtAddPhone: {
				number: "Enter only numbers",
				remote: "This mobile number is already used"
            }	
        }
	});
	
	$("#frmEditClients").validate({
		rules: {
			txtClientCompany: {
                required: true,
				maxlength: 100
            },
			txtClientName: {
                required: true,
				maxlength: 100
			},
			txtClientDesignation: {
                required: true,
				maxlength: 100
            },
			txtClientEmail: {
                required: true,
				maxlength: 255
            },
			ddlState: {
                required: true
            },
			ddlSubUrb: {
                required: true
            },
			txtClientPostal: {
				required: true,
				minlength: 4,
				maxlength: 4
			},
			txtClientAddress: {
				required: true
			},
			txtAddPhone: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 15,
				remote: {
							type: 'post',
							url: 'ajax_check_mobile.php',
							data: {
								'txtNumber': function () { return $('#txtAddPhone').val(); },
								'hfUID': function () { return $('#hfUID').val(); }
							},
							dataType: 'json'
						}				
			},
			txtClientEmail:
			{
				email: true, 
				required: true
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		focusCleanup: true,
		messages: {
			txtAddPhone: {
				number: "Enter only numbers",
				remote: "This mobile number is already used"
            }	
        }
	});
	
	$("#frmAgent").validate({
		rules: {
			txtFirstName: {
                required: true,
				maxlength: 50
            },
			txtMiddleName: {
                //required: true,
				maxlength: 50
            },
			txtLastName: {
                required: true,
				maxlength: 50
            },
			/* ddlState: {
                required: true
            },
			ddlSubUrb: {
                required: true
            }, */
			txtAddPhone: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 15,
				remote: {
							type: 'post',
							url: 'ajax_check_mobile.php',
							data: {
								'txtNumber': function () { return $('#txtAddPhone').val(); }
							},
							dataType: 'json'
						}
			},
			/* txtAddPostal: {
				required: true,
				minlength: 4,
				maxlength: 4
			}, */
			txtAddAddress: {
				required: true
			},
			txtAddAdhar:
			{
				//required: true
			},
			txtAddPAN:
			{
				//required: true
			},
			txtAddBankName:
			{
				maxlength: 100
			},
			txtAddBankAccount:
			{
				maxlength: 20
			},
			txtAddBankIFSC:
			{
				maxlength: 20
			},
			ddlCountry: {
				required: true
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		focusCleanup: true,
        messages: {
			txtAddPhone: {
				number: "Enter only numbers",
				remote: "This mobile number is already used"
            }	
        }
	});
	
	$("#frmEditCallCenter").validate({
		rules: {
			txtCallCenterName: {
                required: true,
				maxlength: 200
            },
			/* ddlState: {
                required: true
            },
			ddlSubUrb: {
                required: true
            }, */
			txtCallCenterPhone: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 15,
				remote: {
							type: 'post',
							url: 'ajax_check_mobile.php',
							data: {
								'txtNumber': function () { return $('#txtCallCenterPhone').val(); },
								'hfUID': function () { return $('#hfUID').val(); }
							},
							dataType: 'json'
						}				
			},
			txtCallCenterAltPhone: {
				minlength: 8,
				maxlength: 15
			},
			/* txtCallCenterPostal: {
				required: true,
				minlength: 4,
				maxlength: 4
			}, */
			txtCallCenterAddress: {
				required: true
			},
			rbtnIsAffliated: {
				required: true
			},
			ddlCountry: {
				required: true
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		focusCleanup: true,
		errorPlacement: function(error, element) 
        {
            if ( element.is(":radio") ) 
            {
                error.insertAfter('.radioclass');
            }
            else 
            {
                error.insertAfter( element );
            }
        },
        messages: {
			txtCallCenterPhone: {
				number: "Enter only numbers",
				remote: "This mobile number is already used"
            }	
        }
	});
	
	$("#frmEditHR").validate({
		rules: {
			ddlCallCenterName: {
                required: true
            },
			ddlState: {
                required: true
            },
			ddlSubUrb: {
                required: true
            },
			txtCallCenterPhone: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 15,
				remote: {
							type: 'post',
							url: 'ajax_check_mobile.php',
							data: {
								'txtNumber': function () { return $('#txtCallCenterPhone').val(); },
								'hfUID': function () { return $('#hfUID').val(); }
							},
							dataType: 'json'
						}
			},
			txtCallCenterAltPhone: {
				minlength: 8,
				maxlength: 15
			},
			txtCallCenterPostal: {
				required: true,
				minlength: 4,
				maxlength: 4
			},
			txtCallCenterAddress: {
				required: true
			},
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		focusCleanup: true,
        messages: {
			txtCallCenterPhone: {
				number: "Enter only numbers",
				remote: "This mobile number is already used"
            }	
        }
	});
	
	$("#frmEditAgent").validate({
		rules: {
			txtFirstName: {
                required: true,
				maxlength: 50
            },
			txtMiddleName: {
                //required: true,
				maxlength: 50
            },
			txtLastName: {
                required: true,
				maxlength: 50
            },
			/* ddlState: {
                required: true
            },
			ddlSubUrb: {
                required: true
            }, */
			txtAddPhone: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 15,
				remote: {
							type: 'post',
							url: 'ajax_check_mobile.php',
							data: {
								'txtNumber': function () { return $('#txtAddPhone').val(); },
								'hfUID': function () { return $('#hfUID').val(); }
							},
							dataType: 'json'
						}
			},
			/* txtAddPostal: {
				required: true,
				minlength: 4,
				maxlength: 4
			}, */
			txtAddAddress: {
				required: true
			},
			txtAddAdhar:
			{
				//required: true
			},
			txtAddPAN:
			{
				//required: true
			},
			txtAddBankName:
			{
				maxlength: 100
			},
			txtAddBankAccount:
			{
				maxlength: 20
			},
			txtAddBankIFSC:
			{
				maxlength: 20
			},
			ddlCountry: {
				required: true
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		focusCleanup: true,
        messages: {
			txtAddPhone: {
				number: "Enter only numbers",
				remote: "This mobile number is already used"
            }	
        }
	});
	
	$("#frmCheckList").validate({
		rules: {
			hfCheckListId: {
						required: true,
					}
			},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
        messages: {},
		submitHandler: function(form) 
		{
			var hfLeadId=$('#hfCheckListId').val();
			
			var rbtn1=0;
			var rbtn2=0;
			var rbtn3=0;
			
			if($('#rbtnval1').prop("checked"))
			{
				rbtn1 = 1;
			}
			if($('#rbtnval2').prop("checked"))
			{
				rbtn2 = 1;
			}
			if($('#rbtnval3').prop("checked"))
			{
				rbtn3 = 1;
			}
			var postData = {"hfLeadId":hfLeadId,"rbtn1":rbtn1,"rbtn2":rbtn2,"rbtn3":rbtn3};
			callAjax('saveCheckList.php',postData,'');
			$("#checkListModal").modal('hide');
			getPagination($(".page-item.active a").attr('tabindex'));
		}
	});
	
	$("#frmAssignNewVerifier").validate({
		rules: {
			},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
        messages: {},
		submitHandler: function(form) 
		{
			if (confirm("Are you sure?")) {
				var val = [];
				$('.chckAssignVerifierMultiple:checked').each(function(i) {
					val[i] = $(this).val();
				});

				var postData = {
					"chckDeleteMultiple": val,
					"ddlAssignToVerifier":$('#ddlAssignToVerifier').val(),
					"txtAssignVerifierComments":$('#txtAssignVerifierComments').val()
				};
				callAjax('multipleverifierassignLead.php', postData, '.ajaxdummyClass');
				$("#assignNewVerifierModal").modal('hide');
				alert("Leads moved to Verifier Section.");
				getPagination($(".page-item.active a").attr('tabindex'));
			}
		}
	});
	
	$("#frmEmailTemplate").validate({
		rules: {
			hfLeadEmail: {
						required: true,
						email: true
					},
			ddlEmailTemplates: {
						required: true,
					}
			},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
        messages: {},
		submitHandler: function(form) 
		{
			var hfLeadEmail=$('#hfLeadEmail').val();
			var hfLeadIdEmail=$('#hfLeadIdEmail').val();
			var ddlEmailTemplates=$('#ddlEmailTemplates').val();
			var ddlEmailState=$('#ddlEmailState').val();
			
			var postData = {"hfLeadEmail":hfLeadEmail,"hfLeadIdEmail":hfLeadIdEmail,"ddlEmailTemplates":ddlEmailTemplates,"ddlEmailState":ddlEmailState};
			callAjax('send_email_template.php',postData,'');
			$("#emailTemplate").modal('hide');
			$("#ddlEmailTemplates").val('');
			showEmailStates();
			getPagination($(".page-item.active a").attr('tabindex'));
		}
	});
	
	$("#frmPhoneTemplate").validate({
		rules: {
			hfLeadPhone: {
						required: true,
					},
			ddlPhoneTemplates: {
						required: true,
					}
			},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
        messages: {},
		submitHandler: function(form) 
		{
			var hfLeadPhone=$('#hfLeadPhone').val();
			var hfLeadIdPhone=$('#hfLeadIdPhone').val();
			var ddlPhoneTemplates=$('#ddlPhoneTemplates').val();
			
			var postData = {"hfLeadPhone":hfLeadPhone,"hfLeadIdPhone":hfLeadIdPhone,"ddlPhoneTemplates":ddlPhoneTemplates};
			callAjaxAlert('send_phone_template.php',postData,'');
			$("#phoneTemplate").modal('hide');
			$("#ddlPhoneTemplates").val('');
			getPagination($(".page-item.active a").attr('tabindex'));
		}
	});
	
	$("#frm1Leads").validate({
		rules: {
			txt1FName: {
                required: true,
				maxlength: 50
            },
			txt1MName: {
                //required: true,
				maxlength: 50
            },
			txt1LName: {
                //required: true,
				maxlength: 50
            },
			txt1Year: {
				 required: function(element) {
					var day = $("#txt1Day").val();
					var month = $("#txt1Month").val();
					return !($.trim(day).length == 0 && $.trim(month).length == 0)
				}
            },
			txt1Month: {
               required: function(element) {
					var day = $("#txt1Day").val();
					var year = $("#txt1Year").val();
					return !($.trim(day).length == 0 && $.trim(year).length == 0)
				}
            },
			txt1Day: {
               required: function(element) {
					var year = $("#txt1Year").val();
					var month = $("#txt1Month").val();
					return !($.trim(year).length == 0 && $.trim(month).length == 0)
				}
            },
			txt1Email:
			{
				email: true,
				maxlength: 255
			},
			ddl1State: {
                //required: true
            },
			ddl1SubUrb: {
                //required: true
            },
			txt1Mobile: {
				required: true,
				digits: true,
				minlength: 10,
				maxlength: 10
			},
			txt1Relation: {
				maxlength: 50
            },
			txt1SubUrbName: {
				maxlength: 100
            },
			txt1AltNumber: {
				digits: true,
				minlength: 8,
				maxlength: 15
			},
			txt1AddPostal: {
				//required: true,
				minlength: 4,
				maxlength: 4
			},
			txt1AddAddress: {
				//required: true
			},
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
        messages: {
			txt1Mobile: {
				number: "Enter only numbers"
            },
			txt1AltNumber: {
				number: "Enter only numbers"
            }			
        },
		submitHandler: function(form) 
		{
			var flag = 0;
			var hfLeadId=$('#hfLeadId').val();
			
			var day = $("#txt1Day").val();
			var month = $("#txt1Month").val();
			var year = $("#txt1Year").val();
			
			if($.trim(year).length == 0 && $.trim(month).length == 0 && $.trim(year).length == 0)
			{
				date_val = null;
			}
			else
			{	
				date_val = $('#txt1Year').val()+'-'+$('#txt1Month').val()+'-'+$('#txt1Day').val();
				var myDate = new Date(year, month-1, day);
				var today = new Date();
				if (myDate > today) 
				{
					flag=1;
					alert("Sorry! You can't enter future date for DOB");
				}
				else if(isDate(month+'/'+day+'/'+year) == 0)
				{
					flag=1;
					alert("Sorry! DOB is invalid.");
				}
			
			}
			
			if(flag==0)
			{
				var postData = {"hfLeadId":$("#hfLeadId").val(),"txt1FName":$("#txt1FName").val(),"txt1MName":$("#txt1MName").val(),"txt1LName":$("#txt1LName").val(),"txt1DOB":date_val,"txt1Email":$("#txt1Email").val(),"txt1Mobile":$("#txt1Mobile").val(),"ddl1State":$("#ddl1State").val(),"txt1AddPostal":$("#txt1AddPostal").val(),"ddl1SubUrb":$("#ddl1SubUrb").val(),"txt1SubUrbName":$("#txt1SubUrbName").val(),"txt1AddAddress":$("#txt1AddAddress").val(),"txt1Relation":$("#txt1Relation").val(),"txt1AltNumber":$("#txt1AltNumber").val()};
				callLeadAjax('ajaxAddPersonalDetails.php',postData,'.hfLeadId',1);
			}
		}
	});
	
	$("#frm2Leads").validate({
		rules: {
			ddl2SelectCount: {
                required: true
            }
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		ignore: [],
		submitHandler: function(form) 
		{
			var date_val_cnt = '';
			var date_in_val_cnt = '';
			var last_count=$('#last_count').val();
			var i;
			var my_array_final=[];
			var today = new Date();
			
			var f = 0;
			
			for(i=1;i<=last_count;i++)
			{
				var my_array={}; /*** CREATE OBJECT ***/
				
				var is_na=0;
				var days = $('#txt2DOBDay'+i).val();
				if(days==32)
				{
					days=1;
					is_na=1;
				}
				
				my_array.txt2AccidentType=$("#txt2AccidentType"+i).val();
				my_array.txt2DOB=$('#txt2DOBYear'+i).val()+'-'+$('#txt2DOBMonth'+i).val()+'-'+days+' '+$('#txt2DOBHours'+i).val()+':'+$('#txt2DOBMinutes'+i).val()+':00';
				my_array.txt2AccidentClaim=$("#txt2AccidentClaim"+i).val();
				my_array.hfId=$("#hfId"+i).val();
				my_array.is_na=is_na;
				my_array_final.push(my_array);
				
				//var f = 0;
				// var flag = 0;
				if($('#txt2DOBDay'+i).length && $('#txt2DOBMonth'+i).length && $('#txt2DOBYear'+i).length)
				{
					if(isDate($('#txt2DOBMonth'+i).val()+'/'+days+'/'+$('#txt2DOBYear'+i).val())==0)
					{
						date_val_cnt+= days+'-'+$('#txt2DOBMonth'+i).val()+'-'+$('#txt2DOBYear'+i).val()+"\n";
					}
					else
					{
						var myDate = new Date($('#txt2DOBYear'+i).val(), $('#txt2DOBMonth'+i).val()-1, days);
						if (myDate > today) 
						{
							// flag=1;
							date_in_val_cnt+= days+'-'+$('#txt2DOBMonth'+i).val()+'-'+$('#txt2DOBYear'+i).val()+"\n";
						}
					}
				}
				else
				{
					// f = 1;
					// break;
					f++;
				}
			}
			
			// if(f==1)
			if(f==last_count)
			{
				alert('Refreshing the page due to some error occured.');
				window.location = 'edit-lead-each.php?u='+$("#hfLeadId").val();
			}
			else if(date_val_cnt.length!=0)
			{
				alert(date_val_cnt+"Above accident dates is/are invalid");
			}
			else if(date_in_val_cnt.length!=0)
			{
				alert(date_in_val_cnt+"Above accident dates are not allowed. These are future dates.");
			}
			else
			{
				var postData = {"hfLeadId":$("#hfLeadId").val(),"accident_array":JSON.stringify(my_array_final)};
				callLeadAccidentAjax('ajaxAddAccidentBasics.php',postData,'#dummy',2);
			}
				
		}
	});
	
	$("#frm3Leads").validate({
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		ignore: [],
		submitHandler: function(form) 
		{
			var ddl2SelectCount=$('#ddl2SelectCount').val();
			var hfAccidentKind=$('#hfAccidentKind').val();
			var i;
			var my_array_final=[];
			for(i=1;i<=ddl2SelectCount;i++)
			{
				var my_array={};
				var is_na=0;
				var days = $('#txt33DOBDay'+i).val();
				if(days==32)
				{
					days=1;
					is_na=1;
				}
				
				my_array.txt3AccidentDate=$('#txt33DOBYear'+i).val()+'-'+$('#txt33DOBMonth'+i).val()+'-'+days;
				my_array.txt3AccidentTime=$('#txt33DOBHours'+i).val()+':'+$('#txt33DOBMinutes'+i).val()+':00';
				my_array.ddl3RoadState=$("#ddl3RoadState"+i).val();
				my_array.ddlState=$("#ddlState"+i).val();
				my_array.ddlSubUrb=$("#ddlSubUrb"+i).val();
				my_array.txt3PostalCode=$("#txt3PostalCode"+i).val();
				my_array.rbtnSelectedDriver=c_undefine($("input[name=rbtnSelectedDriver"+i+"]:checked").val());
				my_array.txt3AddAddress=$("#txt3AddAddress"+i).val();
				my_array.txtAccidentCircumstanes=$("#txtAccidentCircumstanes"+i).val();
				my_array.ddl3PoliceInformed=$("#ddl3PoliceInformed"+i).val();
				my_array.txtPoliceStation=$("#txtPoliceStation"+i).val();
				my_array.txtPoliceNumber=$("#txtPoliceNumber"+i).val();
				my_array.txtPoliceInfo=$("#txtPoliceInfo"+i).val();
				my_array.ddlPoliceReported=$("#ddlPoliceReported"+i).val();
				my_array.txtOfficersName=$("#txtOfficersName"+i).val();
				my_array.txtPoliceStationReported=$("#txtPoliceStationReported"+i).val();
				my_array.txtAccidentId=$("#txtAccidentId"+i).val();
				my_array.txt3AccidentRecord=$("#txt3AccidentRecord"+i).val();
				my_array.txt3SubUrbNameAccident=$("#txt3SubUrbNameAccident"+i).val();
				my_array.is_na=is_na;
				my_array_final.push(my_array);
			}
			var postData = {"hfLeadId":$("#hfLeadId").val(),"hfLeadAccidentId":$("#hfLeadAccidentId").val(),"accident_array":JSON.stringify(my_array_final)};
			
			if(hfAccidentKind==1)
			{
				callLeadAjax('ajaxAddAccidentDetails.php',postData,'#dummy',3);
			}
			else
			{
				callLeadAjax('ajaxAddAccidentDetails.php',postData,'#dummy',4);
			}
		}
	});
	
	$("#frm4Leads").validate({
		rules: {
			txt5ThirdMake: {
				maxlength: 100
            },
			txt5ThirdModel:
			{
				maxlength: 100
			},
			txt5ThirdRegistration: {
				maxlength: 100
            },
			txt5ThirdInsuranceCompany: {
				maxlength: 100
            },
			txt5ThirdInsuranceRef: {
				maxlength: 100
            },
			txt5ThirdInsuranceClaim: {
				maxlength: 100
            },
			txt5ThirdInsuranceOther:
			{
				maxlength: 100
			},
			txt5DThirdDriverName: {
				maxlength: 100
			},
			txt5DThirdAddress: {
				maxlength: 100
			},
			txt5DThirdDLNumber: {
				maxlength: 100
			},
			txt5DThirdMake: {
				maxlength: 100
			},
			txt5DThirdModel:
			{
				maxlength: 100
			},
			txt5DThirdRegistration: {
				maxlength: 100
			},
			txt5DThirdInsuranceCompany: {
				maxlength: 100
			},
			txt5DThirdInsuranceRef: {
				maxlength: 100
			},
			txt5DThirdInsuranceClaim: {
				maxlength: 100
			},
			txt5DThirdInsuranceOther:
			{
				maxlength: 100
			},
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		submitHandler: function(form) 
		{
			var hfLeadId=$('#hfLeadId').val();
			var postData = {"hfLeadId":hfLeadId,"txt5ThirdMake":$("#txt5ThirdMake").val(),"txt5ThirdModel":$("#txt5ThirdModel").val(),"txt5ThirdRegistration":$("#txt5ThirdRegistration").val(),"txt5ThirdInsuranceCompany":$("#txt5ThirdInsuranceCompany").val(),"txt5ThirdInsuranceRef":$("#txt5ThirdInsuranceRef").val(),"txt5ThirdInsuranceClaim":$("#txt5ThirdInsuranceClaim").val(),"txt5ThirdInsuranceOther":$("#txt5ThirdInsuranceOther").val(),"txt5DThirdDriverName":$("#txt5DThirdDriverName").val(),"txt5DThirdAddress":$("#txt5DThirdAddress").val(),"txt5DThirdDLNumber":$("#txt5DThirdDLNumber").val(),"txt5DThirdMake":$("#txt5DThirdMake").val(),"txt5DThirdModel":$("#txt5DThirdModel").val(),"txt5DThirdRegistration":$("#txt5DThirdRegistration").val(),"txt5DThirdInsuranceCompany":$("#txt5DThirdInsuranceCompany").val(),"txt5DThirdInsuranceRef":$("#txt5DThirdInsuranceRef").val(),"txt5DThirdInsuranceClaim":$("#txt5DThirdInsuranceClaim").val(),"txt5DThirdInsuranceOther":$("#txt5DThirdInsuranceOther").val(),"hfLeadAccidentId":$("#hfLeadAccidentId").val()};
			callLeadAjax('ajaxAddVehicleDetails.php',postData,'#dummy',4);	
		}
	});
	
	$("#frm5Leads").validate({
		rules: {
			txt6AnyOtherInjury: 
			{
				maxlength: 500
			},
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		submitHandler: function(form) 
		{
			var hfLeadId=$('#hfLeadId').val();
			var my_array_final=[];
			
			var x = 0;
			
			$(".chckInjury:checked").each(function() 
			{
				var my_array={};
				my_array.body_part=$(this).attr('body_part');
				my_array.injuries=$(this).attr('injuries');
				my_array_final.push(my_array);
				x=1;
			});
			
			if(x==0)
			{
				var inj = $("#txt6AnyOtherInjury").val();
				if(inj.length!=0)
				{
					x=1;
				}
			}
			
			if(x==1)
			{
				var postData = {"hfLeadId":hfLeadId,"checked_array":JSON.stringify(my_array_final),"txtStillSuffering":$("input[name=txt6StillSuffering]:checked").val(),"txt6AnyOtherInjury":$("#txt6AnyOtherInjury").val(),"hfSomethingChanged":$('#hfSomethingChanged').val(),"hfLeadAccidentId":$("#hfLeadAccidentId").val()};
				callLeadAjax('ajaxAddInjuries.php',postData,'#dummy',5);
				$('#hfSomethingChanged').val(0);	
			}
			else
			{
				alert('Select Injury First');
			}
		}
	});
	
	$("#frm6Leads").validate({
		rules: {
			txtAmbulanceHospitalDetails: 
			{
				maxlength: 500
			},
			txtWhatWasTreatment: 
			{
				maxlength: 500
			},
			txtNoScan: 
			{
				maxlength: 500
			},
			txtScanResult: 
			{
				maxlength: 500
			},
			txtPreExistingInjuries: 
			{
				maxlength: 500
			},
			txtGPName: 
			{
				maxlength: 100
			},
			txtGPSurgeryName: 
			{
				maxlength: 100
			},
			txtGPLastVisit: 
			{
				//date: true
			},
			txtAnyOther: 
			{
				maxlength: 500
			},
			txtWhatWasHospitalTreatment: 
			{
				maxlength: 1000
			},
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		submitHandler: function(form) 
		{
			var my_array_final=[];
			
			var hfLeadId=$('#hfLeadId').val();
			var rbtnAnyPainRelief=null;
			var rbtnRecommendedPhysioChrio=null;
			var IsPhysio=null;
			var affordPhysio=null;
			var txtPhysioVisits=null;
			var affordChiro=null;
			var txtChirioVisits=null;
			var rbtnXrayScan=null;
			var txtScanResult=null;
			var txtNoScan=null;
			var txtPreExistingInjuries=null;
			var txtAnyOther=null;
			var rbtnVisitGP=null;
			var txtGPLastVisit=null;
			var txtGPName=null;
			var txtGPSurgeryName=null;
			var txtGPVisits=null;
			var rbtnGoToHospital=null;
			var txtAmbulanceHospitalDetails=null;
			var txtFriendHospitalDetails=null;
			var ambulanceTreatScene=null;
			var txtWhatWasTreatment=null;
			var rbtnAmbulanceTakenHospital=null;
			var txtWhatWasHospitalTreatment=null;
			var ddlPhysioSessions=null;
			var txtPhysioSessionsTaking=null;
			var ddlChirioSessions=null;
			var txtChirioSessionsTaking=null;
			
			rbtnAnyPainRelief=$("input[name=rbtnAnyPainRelief]:checked").val();
			rbtnRecommendedPhysioChrio=$("input[name=rbtnRecommendedPhysioChrio]:checked").val();
			rbtnXrayScan=$("input[name=rbtnXrayScan]:checked").val();
			rbtnVisitGP=$("input[name=rbtnVisitGP]:checked").val();
			ambulanceComeScene=$("input[name=ambulanceComeScene]:checked").val();
			rbtnGoToHospital=$("input[name=rbtnGoToHospital]:checked").val();
			PhysioChiro=$("input[name=PhysioChiro]:checked").val();
			txtPreExistingInjuries=$("#txtPreExistingInjuries").val();
			txtAnyOther=$("#txtAnyOther").val();
			
			if(typeof ambulanceComeScene != 'undefined')
			{
				if(ambulanceComeScene.toLowerCase()=='yes')
				{
					ambulanceTreatScene=$("input[name=ambulanceTreatScene]:checked").val();
					txtWhatWasTreatment=$("#txtWhatWasTreatment").val();
					
					rbtnAmbulanceTakenHospital=$("input[name=rbtnAmbulanceTakenHospital]:checked").val();
					if(typeof rbtnAmbulanceTakenHospital != 'undefined' && rbtnAmbulanceTakenHospital.toLowerCase()=='yes')
					{
						txtAmbulanceHospitalDetails=$("#txtHospitalDetails").val();
						txtWhatWasHospitalTreatment=$("#txtWhatWasHospitalTreatment").val();
					}
				}
				else if(ambulanceComeScene.toLowerCase()=='no')
				{
					rbtnGoToHospital=$("input[name=rbtnGoToHospital]:checked").val();
					if(typeof rbtnGoToHospital != 'undefined' && rbtnGoToHospital.toLowerCase()=='yes')
					{
						txtFriendHospitalDetails=$("#txtHospitalDetails").val();
						txtWhatWasHospitalTreatment=$("#txtWhatWasHospitalTreatment").val();
					}
				}
			}
			
			if(typeof rbtnRecommendedPhysioChrio != 'undefined' && rbtnRecommendedPhysioChrio.toLowerCase()=='yes')
			{
				IsPhysio=$("input[name=IsPhysio]:checked").val();
				if(typeof IsPhysio != 'undefined')
				{
					if(IsPhysio.toLowerCase()=='yes')
					{
						ddlPhysioSessions=$("#ddlPhysioSessions").val();
						txtPhysioSessionsTaking=$("#txtPhysioSessionsTaking").val();
					}
					else if(IsPhysio.toLowerCase()=='no')
					{
						ddlChirioSessions=$("#ddlChirioSessions").val();
						txtChirioSessionsTaking=$("#txtChirioSessionsTaking").val();
					}
					
				}
			}
			
			if(typeof rbtnXrayScan != 'undefined')
			{
				if(rbtnXrayScan.toLowerCase()=='yes')
				{
					txtScanResult=$("#txtScanResult").val();
				}
				else if(rbtnXrayScan.toLowerCase()=='no')
				{
					txtNoScan=$("#txtNoScan").val();
				}	
			}

			$(".injuryList:checked").each(function() 
			{
				var my_array={};
				my_array.name_attr=$(this).attr('name_attr');
				my_array_final.push(my_array);
			});
			
			if(typeof rbtnVisitGP != 'undefined' && rbtnVisitGP.toLowerCase()=='yes')
			{
				txtGPName=$("#txtGPName").val();
				txtGPSurgeryName=$("#txtGPSurgeryName").val();
				txtGPVisits=$("#txtGPVisits").val();
				txtGPLastVisit=$("#txtGPLastVisit").val();
			}
	
			var postData = {"hfLeadId": c_undefine(hfLeadId), "rbtnAmbulanceComeScene": c_undefine(ambulanceComeScene), "rbtnAnyPainRelief": c_undefine(rbtnAnyPainRelief), "rbtnRecommendedPhysioChrio": c_undefine(rbtnRecommendedPhysioChrio), "IsPhysio": c_undefine(IsPhysio), "affordPhysio": c_undefine(affordPhysio), "txtPhysioVisits": c_undefine(txtPhysioVisits), "affordChiro": c_undefine(affordChiro), "txtChirioVisits": c_undefine(txtChirioVisits), "rbtnXrayScan": c_undefine(rbtnXrayScan), "txtScanResult": c_undefine(txtScanResult), "txtNoScan": c_undefine(txtNoScan), "txtPreExistingInjuries": c_undefine(txtPreExistingInjuries), "txtAnyOther": c_undefine(txtAnyOther), "rbtnVisitGP": c_undefine(rbtnVisitGP), "txtGPLastVisit": c_undefine(txtGPLastVisit), "txtGPName": c_undefine(txtGPName), "txtGPSurgeryName": c_undefine(txtGPSurgeryName), "txtGPVisits": c_undefine(txtGPVisits), "rbtnGoToHospital": c_undefine(rbtnGoToHospital), "txtAmbulanceHospitalDetails": c_undefine(txtAmbulanceHospitalDetails), "txtFriendHospitalDetails": c_undefine(txtFriendHospitalDetails), "ambulanceTreatScene": c_undefine(ambulanceTreatScene), "txtWhatWasTreatment": c_undefine(txtWhatWasTreatment), "rbtnAmbulanceTakenHospital": c_undefine(rbtnAmbulanceTakenHospital), "txtWhatWasHospitalTreatment": c_undefine(txtWhatWasHospitalTreatment),"PhysioChiro": c_undefine(PhysioChiro), "injuries":JSON.stringify(my_array_final),"hfLeadAccidentId":$("#hfLeadAccidentId").val(),"ddlChirioSessions":ddlChirioSessions,"ddlPhysioSessions":ddlPhysioSessions,"txtPhysioSessionsTaking":txtPhysioSessionsTaking,"txtChirioSessionsTaking":txtChirioSessionsTaking};
			callLeadAjax('ajaxAddTreatmentHistory.php',postData,'#dummy',6);	
		}
	});
	
	$("#frm7Leads").validate({
		rules: {
			txt7OtherIncomes: 
			{
				maxlength: 500
			},
			txt7WorkingHours: 
			{
				maxlength: 2
			},
			txt7Occupation: 
			{
				maxlength: 50
			},
			txt7WeeklySalary: 
			{
				number: true
			},
			txt7WorkingHours: 
			{
				digits: true
			},
			txt7WorkingIncomeLost: 
			{
				number: true
			},
			txt7ReceivingAssistance: 
			{
				maxlength: 500
			},
			txt7RequireReceivingAssistance: 
			{
				maxlength: 500
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		submitHandler: function(form) 
		{
			var hfLeadId=$('#hfLeadId').val();
			var postData = {"hfLeadId":hfLeadId,"ddl7Employed":$("#ddl7Employed").val(),"txt7Occupation":$("#txt7Occupation").val(),"txt7WeeklySalary":$("#txt7WeeklySalary").val(),"txt7WorkingHours":$("#txt7WorkingHours").val(),"txt7WorkingIncomeLost":$("#txt7WorkingIncomeLost").val(),"ddl7HaveInjuries":$("#ddl7HaveInjuries").val(),"txt7ReceivingAssistance":$("#txt7ReceivingAssistance").val(),"txt7RequireReceivingAssistance":$("#txt7RequireReceivingAssistance").val(),"ddl7IncomeState":$("#ddl7IncomeState").val(),"txt7OtherIncomes":$("#txt7OtherIncomes").val(),"hfLeadAccidentId":$("#hfLeadAccidentId").val()};
			
			var hfAccidentKind=$('#hfAccidentKind').val();
			/* if(hfAccidentKind==1)
			{
				callLeadAjax('ajaxAddWorkHistory.php',postData,'#dummy',7);	
			}
			else
			{
				callLeadAjax('ajaxAddWorkHistory.php',postData,'#dummy',8);	
			} */
			
			callLeadAjax('ajaxAddWorkHistory.php',postData,'#dummy',8);	
		}
	});
	
	$("#frm8Leads").validate({
		rules: {
			'txt8Passenger[]': 
			{
				maxlength: 500
			},
			'txt8PassengersCount': 
			{
				digits: true,
				maxlength: 2
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		submitHandler: function(form) 
		{
			var hfLeadId=$('#hfLeadId').val();
			var txt8PassengersCount=$('#txt8PassengersCount').attr('count_attr');
			var hfSomethingDoneInPassenger=$('#hfSomethingDoneInPassenger').val();
			var i;
			var my_array_final=[];
			
			for(i=1;i<=txt8PassengersCount;i++)
			{
				var my_array={};
				if($.trim($("#txt8Passenger"+i).val()).length!=0)
				{
					my_array.txtPassenger=$("#txt8Passenger"+i).val();
					my_array.txtPassengerDriver=$("input[name=txt8PassengerRadio"+i+"]:checked").val();
					my_array_final.push(my_array);
				}
			}
			var postData = {"hfLeadId":$("#hfLeadId").val(),"passenger_array":JSON.stringify(my_array_final),"hfSomethingDoneInPassenger":hfSomethingDoneInPassenger,"hfLeadAccidentId":$("#hfLeadAccidentId").val()};
			callLeadAjax('ajaxAddPassengerDetails.php',postData,'#dummy',8);
			$('#hfSomethingDoneInPassenger').val(0);
		}
	});
	
	$("#frm9Leads").validate({
		rules: {
			txt9WitnessName: {
				maxlength: 100
            },
			txt9WitnessPhone: {
				minlength: 8,
				maxlength: 15,
				number: true
			},
			txt9WitnessPostalCode: {
				minlength: 4,
				maxlength: 4
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
        messages: {
			txt9WitnessPhone: {
				number: "Enter only numbers"
            }
        },
		submitHandler: function(form) 
		{
				var hfLeadId=$('#hfLeadId').val();
	
				var postData = {"hfLeadId":$("#hfLeadId").val(),"txt9Address":$("#txt9Address").val(),"txt9WitnessName":$("#txt9WitnessName").val(),"txt9WitnessPhone":$("#txt9WitnessPhone").val(),"txt9WitnessPostalCode":$("#txt9WitnessPostalCode").val(),"ddl9State":$("#ddl9State").val(),"ddl9SubUrb":$("#ddl9SubUrb").val(),"witnessO":$("input[name=witnessO]:checked").val(),"hfLeadAccidentId":$("#hfLeadAccidentId").val()};
				callLeadAjax('ajaxAddWitnessDetails.php',postData,'#dummy',9);
				// window.location.href='view-leads.php';
				//window.location.href='edit-lead-each.php?u='+hfLeadId+'&a=1';
		}
	});
	
	/* $("#frmSaveClose").validate({
		rules: {
			txtSaveCloseDay: 
			{
				required: true 
			},
			txtSaveCloseMonth: 
			{
				required: true 
			},
			txtSaveCloseYear: 
			{
				required: true 
			},
			txtSaveCloseHours: 
			{
				required: true 
			},
			txtSaveCloseMinutes: 
			{
				required: true 
			},
			txtSaveCloseDetails: 
			{
				maxlength: 500 
			}
		},
		messages: {
			txtSaveCloseDay: 
			{
				required: 'Required' 
			},
			txtSaveCloseMonth: 
			{
				required: 'Required' 
			},
			txtSaveCloseYear: 
			{
				required: 'Required' 
			},
			txtSaveCloseHours: 
			{
				required: 'Required' 
			},
			txtSaveCloseMinutes: 
			{
				required: 'Required' 
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		submitHandler: function(form) 
		{
			var hfLeadId=$('#hfLeadId').val();
			var hfLeadAccidentId=c_undefine($('#hfLeadAccidentId').val());
			var day = $("#txtSaveCloseDay").val();
			var month = $("#txtSaveCloseMonth").val();
			var year = $("#txtSaveCloseYear").val();
			var hours = $("#txtSaveCloseHours").val();
			var minutes = $("#txtSaveCloseMinutes").val();
			var ddlSignUpStates = $("#ddlSignUpStates").val();
			var date_val = year+'-'+month+'-'+day+' '+hours+':'+minutes;
			
			var postData = {"hfLeadId":hfLeadId,"hfLeadAccidentId":hfLeadAccidentId,"txtCallBackDate":date_val,"txtSaveCloseDetails":$("#txtSaveCloseDetails").val(),"ddlSignUpStates":ddlSignUpStates};
			callLeadAjax('ajaxAddCallBackDetails.php',postData,'#dummy',0);
			window.location.href='view-leads.php';
		}
	}); */
	
	$("#frmSaveClose").validate({
		rules: {
			txtSaveCloseDetails: 
			{
				maxlength: 500 
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		submitHandler: function(form) 
		{
			var hfLeadId=$('#hfLeadId').val();
			var hfLeadAccidentId=c_undefine($('#hfLeadAccidentId').val());
			
			var postData = {"hfLeadId":hfLeadId,"hfLeadAccidentId":hfLeadAccidentId,"txtSaveCloseDetails":$("#txtSaveCloseDetails").val()};
			callLeadAjax('ajaxAddCallBackDetails.php',postData,'#dummy',0);
			window.location.href='view-leads.php';
		}
	});
	
	$("#frmSaveCloseNotes").validate({
		rules: {
			txtSaveCloseDetails: 
			{
				maxlength: 500 
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		submitHandler: function(form) 
		{
			var hfLeadId=$('#hfLeadId').val();

			var postData = {"hfLeadId":hfLeadId,"txtSaveCloseDetailsNotes":$("#txtSaveCloseDetailsNotes").val()};
			callLeadAjax('ajaxAddNotes.php',postData,'#dummy',0);
			window.location.href='view-leads.php';
		}
	});
	
	$("#frmSaveCloseNotesL").validate({
		rules: {
			txtSaveCloseDetailsNotesL: 
			{
				maxlength: 500 
			}
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
		submitHandler: function(form) 
		{
			var hfLeadId=$('#hfNotesId').val();
			var postData = {"hfLeadId":hfLeadId,"txtSaveCloseDetailsNotes":$("#txtSaveCloseDetailsNotesL").val()};
			callLeadAjax('ajaxAddNotes.php',postData,'#dummy',0);
			$('#saveCloseModalNotesL').modal('hide');
			getPagination($(".page-item.active a").attr('tabindex'));
		}
	});
	
	$("#frmCopyLead").validate({
		rules: {
			txtCopyName: {
                required: true,
				maxlength: 50
            },
			txtCopyNumber: {
				required: true,
				digits: true,
				minlength: 10,
				maxlength: 10
			},
			txtCopyLastEmail:
			{
				email: true,
				maxlength: 255
			},
			txtCopyYear: {
				 required: function(element) {
					var day = $("#txtCopyDay").val();
					var month = $("#txtCopyMonth").val();
					return !($.trim(day).length == 0 && $.trim(month).length == 0)
				}
			},
			txtCopyMonth: {
			   required: function(element) {
					var day = $("#txtCopyDay").val();
					var year = $("#txtCopyYear").val();
					return !($.trim(day).length == 0 && $.trim(year).length == 0)
				}
			},
			txtCopyDay: {
			   required: function(element) {
					var year = $("#txtCopyYear").val();
					var month = $("#txtCopyMonth").val();
					return !($.trim(year).length == 0 && $.trim(month).length == 0)
				}
			},
			txtCopyLastName: {
				maxlength: 50
			},
		},
        errorElement: 'label',
        errorClass: 'error',
        highlight: function (element) { return false;},
        unhighlight: function (element) { return false;},
        messages: {
			txt1Mobile: {
				number: "Enter only numbers"
            }		
        },
		submitHandler: function(form) 
		{
			return confirm('Are you sure? Changes cannot be reverted.');
		}
	});
	
});
