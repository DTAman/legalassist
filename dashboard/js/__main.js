$(document).ready(function() {

	$('#personal-detail').show();

	if ($('hfLead').val() == 0) {
		$('.myvehicle').hide();
	}

	$('#accident-details,#vehicle-detail,#injuries-matrix,#treatment-history,#passenger-details,#witness-details,#workCare-details,#past-accident-detail').hide();

	$('#check21').click(function() {
		$('#personal-detail').show();
		$('#accident-details,#vehicle-detail,#injuries-matrix,#treatment-history,#passenger-details,#witness-details,#workCare-details,#past-accident-detail').hide();
	});

	$('body').on("click", '#check31', function() {
		$('#past-accident-detail').show();
		$('#vehicle-detail,#personal-detail,#injuries-matrix,#treatment-history,#passenger-details,#witness-details,#workCare-details,#accident-details').hide();
	});

	$('#check41').click(function() {
		$('#accident-details').show();
		$('#vehicle-detail,#personal-detail,#injuries-matrix,#treatment-history,#passenger-details,#witness-details,#workCare-details,#past-accident-detail').hide();
	});

	$('#check51').click(function() {
		$('#vehicle-detail').show();
		$('#accident-details,#injuries-matrix,#personal-detail,#treatment-history,#passenger-details,#witness-details,#workCare-details,#past-accident-detail').hide();
	});

	$('#check61').click(function() {
		$('#injuries-matrix').show();
		$('#accident-details,#treatment-history,#vehicle-detail,#personal-detail,#passenger-details,#witness-details,#workCare-details,#past-accident-detail').hide();
	});

	$('#check71').click(function() {
		$('#treatment-history').show();
		$('#accident-details,#passenger-details,#vehicle-detail,#injuries-matrix,#personal-detail,#witness-details,#workCare-details,#past-accident-detail').hide();
	});

	$('#check81').click(function() {
		$('#workCare-details').show();
		$('#accident-details,#witness-details,#vehicle-detail,#injuries-matrix,#treatment-history,#personal-detail,#passenger-details,#past-accident-detail').hide();
	});

	$('#check91').click(function() {
		$('#passenger-details').show();
		$('#accident-details,#witness-details,#vehicle-detail,#injuries-matrix,#treatment-history,#personal-detail,#workCare-details,#past-accident-detail').hide();
	});


	$('body').on("change", '.chckInjury', function() {
		$('#hfSomethingChanged').val(1);
	});

	$('body').on("change", '.text_class_change', function() {
		$('#hfSomethingDoneInPassenger').val(1);
	});

	$('body').on("click", '.close-btn', function() {
		$(this).parent().parent().parent().remove();
		var select_val = parseInt($('#ddl2SelectCount').val()) - 1;
		$('#txtAccidentTotalCount').val($('#txtAccidentTotalCount').val() - 1);
		if (select_val > 0) {
			$('#ddl2SelectCount').val(select_val);
		} else {
			$('#ddl2SelectCount').val('');
		}

		$('.datepicker_class').bootstrapMaterialDatePicker({
			time: true,
			clearButton: false,
			format: 'YYYY-MM-DD HH:mm:ss'
		});
		$('select').rules("add", 'required');
		$('input').rules("add", 'required');

	});

});

function noneSelcet(value) {
	if (value == 0) {
		$('#selectSubUrb').addClass('d-none');
		$('#subUrbTextBox').addClass('d-block');
		$('#selectSubUrb').removeClass('d-block');
		$('#subUrbTextBox').removeClass('d-none');
	} else {
		$('#selectSubUrb').addClass('d-block');
		$('#subUrbTextBox').addClass('d-none');
		$('#selectSubUrb').removeClass('d-none');
		$('#subUrbTextBox').removeClass('d-block');
	}
}

function policeOne(value, num) {
	$('#policeEventStationOne' + num).addClass('d-none');
	$('#policeEventStationOne' + num).removeClass('d-block');
	if (value == 'Yes') {
		$('#policeEventStationOne' + num).addClass('d-block');
		$('#policeEventStationOne' + num).removeClass('d-none');
		$('#policeNotAttend' + num).addClass('d-none');
		$('#policeNotAttend' + num).removeClass('d-block');
		$('#policeNotAttendYes' + num).addClass('d-none');
		$('#policeNotAttendYes' + num).removeClass('d-block');
	} else if (value == 'No') {
		$('#policeEventStationOne' + num).addClass('d-none');
		$('#policeEventStationOne' + num).removeClass('d-block');
		$('#policeNotAttend' + num).addClass('d-block');
		$('#policeNotAttend' + num).removeClass('d-none');
	}
}

function policeNotAttendV(value, num) {
	$('#policeNotAttendYes' + num).addClass('d-none');
	$('#policeNotAttendYes' + num).removeClass('d-block');
	if (value == 'Yes') {
		$('#policeNotAttendYes' + num).addClass('d-block');
		$('#policeNotAttendYes' + num).removeClass('d-none');
	} else {
		$('#policeNotAttendYes' + num).addClass('d-none');
		$('#policeNotAttendYes' + num).removeClass('d-block');
	}
}

function showName(value, num) {
	if (value == 0) {
		$('#theName' + num).html('Accident Details Passenger');
	} else {
		$('#theName' + num).html('Accident Details Driver');
	}
}

function showOne(value) {
	if (value == 'Yes') {
		$('#hospitalDetailOne').addClass('d-block');
		$('#hospitalDetailOne').removeClass('d-none');
		$('#gpAfterAccidentMain').removeClass('d-none');
	} else {
		$('#hospitalDetailOne').addClass('d-none');
		$('#hospitalDetailOne').removeClass('d-block');
		$('#gpAfterAccidentMain').removeClass('d-none');
	}
}

function showTwo(value) {
	if (value == 'Yes') {
		$('#gpNameSurgeryVisit').addClass('d-block');
		$('#gpNameSurgeryVisit').removeClass('d-none');
		$('#anyPainReliefSymptom').addClass('d-block');
		$('#anyPainReliefSymptom').removeClass('d-none');
		$('#physiotherapyChrioOne').removeClass('d-none');
	} else {
		$('#gpNameSurgeryVisit').addClass('d-none');
		$('#gpNameSurgeryVisit').removeClass('d-block');
		$('#physiotherapyChrioOne').removeClass('d-none');
		$('#anyPainReliefSymptom').addClass('d-block');
		$('#anyPainReliefSymptom').removeClass('d-none');
	}
}

function showThree(value) {
	if (value == 'Yes') {
		$('#bothPhysioChiro').addClass('d-block');
		$('#bothPhysioChiro').removeClass('d-none');
		$('#xrayMriOne').removeClass('d-none');
	} else {
		$('#bothPhysioChiro').addClass('d-none');
		$('#eachVisitPhysio').addClass('d-none');
		$('#eachVisitChiro').addClass('d-none');
		$('#bothPhysioChiro').removeClass('d-block');
		$('#eachVisitPhysio').removeClass('d-block');
		$('#eachVisitChiro').removeClass('d-block');
		$('#xrayMriOne').removeClass('d-none');
		$('#affordPhysio').addClass('d-none');
		$('#affordPhysio').removeClass('d-block');
	}
}

function showFive(value) {
	if (value == 'Yes') {
		$('#resultScanOne').addClass('d-block');
		$('#resultScanOne').removeClass('d-none');
		$('#resultScanNo').addClass('d-none');
		$('#resultScanNo').removeClass('d-block');
		$('#preExcistingInjury').addClass('d-block');
		$('#preExcistingInjury').removeClass('d-none');
		$('#sufferPsychological').addClass('d-block');
		$('#sufferPsychological').removeClass('d-none');
	} else {
		$('#resultScanOne').addClass('d-none');
		$('#resultScanOne').removeClass('d-block');
		$('#resultScanNo').addClass('d-block');
		$('#resultScanNo').removeClass('d-none');
		$('#preExcistingInjury').addClass('d-block');
		$('#preExcistingInjury').removeClass('d-none');
		$('#sufferPsychological').addClass('d-block');
		$('#sufferPsychological').removeClass('d-none');
	}
}

function showOneO(value) {
	if (value == 'Yes') {
		$('#hospitalDetailOneO').addClass('d-block');
		$('#hospitalDetailOneO').removeClass('d-none');
		$('#gpAfterAccidentMainO').removeClass('d-none');
	} else {
		$('#hospitalDetailOneO').addClass('d-none');
		$('#hospitalDetailOneO').removeClass('d-block');
		$('#gpAfterAccidentMainO').removeClass('d-none');
	}
}

function showTwoO(value) {
	if (value == 'Yes') {
		$('#gpNameSurgeryVisitO').addClass('d-block');
		$('#gpNameSurgeryVisitO').removeClass('d-none');
		$('#physiotherapyChrioOneO').removeClass('d-none');
	} else {
		$('#gpNameSurgeryVisitO').addClass('d-none');
		$('#gpNameSurgeryVisitO').removeClass('d-block');
		$('#physiotherapyChrioOneO').removeClass('d-none');
	}
}

function showThreeO(value) {
	if (value == 'Yes') {
		$('#bothPhysioChiroO').addClass('d-block');
		$('#bothPhysioChiroO').removeClass('d-none');
		$('#xrayMriOneO').removeClass('d-none');
	} else {
		$('#bothPhysioChiroO').addClass('d-none');
		$('#eachVisitPhysioO').addClass('d-none');
		$('#eachVisitChiroO').addClass('d-none');
		$('#bothPhysioChiroO').removeClass('d-block');
		$('#eachVisitPhysioO').removeClass('d-block');
		$('#eachVisitChiroO').removeClass('d-block');
		$('#xrayMriOneO').removeClass('d-none');
	}
}

function showFourO(value) {
	if (value) {
		$('#eachVisitPhysioO').addClass('d-block');
		$('#eachVisitPhysioO').removeClass('d-none');
	} else {
		$('#eachVisitPhysioO').addClass('d-none');
		$('#eachVisitPhysioO').removeClass('d-block');
	}
}

function showFourOneO(value) {
	if (value) {
		$('#eachVisitChiroO').addClass('d-block');
		$('#eachVisitChiroO').removeClass('d-none');
	} else {
		$('#eachVisitChiroO').addClass('d-none');
		$('#eachVisitChiroO').removeClass('d-block');
	}
}

function showFiveO(value) {
	if (value == 'Yes') {
		$('#resultScanOneO').addClass('d-block');
		$('#resultScanOneO').removeClass('d-none');
	} else {
		$('#resultScanOneO').addClass('d-none');
		$('#resultScanOneO').removeClass('d-block');
	}
}

function showSix(value) {
	if (value == 'Yes') {
		$('#fullInjuryAndTreatment').addClass('d-block');
		$('#fullInjuryAndTreatment').removeClass('d-none');
		$('#txtPassengerDiv').removeClass('d-none');
		$('#txtPassengerDiv').addClass('d-block');
	} else {
		$('#fullInjuryAndTreatment').addClass('d-none');
		$('#fullInjuryAndTreatment').removeClass('d-block');
		$('#txtPassengerDiv').addClass('d-none');
		$('#txtPassengerDiv').removeClass('d-block');
	}
}

function showSeven(value) {
	if (value == 'Yes') {
		$('#driverPassengerBox').addClass('d-block');
		$('#driverPassengerBox').removeClass('d-none');
		$('#driverName').addClass('d-block');
		$('#driverName').removeClass('d-none');
		$('#passengerName').addClass('d-none');
		$('#passengerName').removeClass('d-block');
		$('#treatedAmbulance').addClass('d-none');
		$('#treatedAmbulance').removeClass('d-block');
		$('#takenAmbulance').addClass('d-none');
		$('#takenAmbulance').removeClass('d-block');
	} else {
		$('#driverPassengerBox').addClass('d-block');
		$('#driverPassengerBox').removeClass('d-none');
		$('#passengerName').addClass('d-block');
		$('#passengerName').removeClass('d-none');
		$('#treatedAmbulance').addClass('d-block');
		$('#treatedAmbulance').removeClass('d-none');
		$('#takenAmbulance').addClass('d-block');
		$('#takenAmbulance').removeClass('d-none');
		$('#driverName').addClass('d-none');
		$('#driverName').removeClass('d-block');
	}
}

function meansOfIncomeV(value) {
	if (value == 2) {
		$('#weeklyGrossIncome,#yourOccupation').addClass('d-none');
		$('#weeklyGrossIncome,#yourOccupation').removeClass('d-block');
		$('#meansOfIncome').addClass('d-none');
		$('#meansOfIncome').removeClass('d-block');
	} else if (value == 1) {
		$('#meansOfIncome').addClass('d-none');
		$('#meansOfIncome').removeClass('d-block');
		$('#weeklyGrossIncome,#yourOccupation').addClass('d-block');
		$('#weeklyGrossIncome,#yourOccupation').removeClass('d-none');
	} else if (value == 0) {
		$('#meansOfIncome').addClass('d-block');
		$('#meansOfIncome').removeClass('d-none');
		$('#weeklyGrossIncome,#yourOccupation').addClass('d-none');
		$('#weeklyGrossIncome,#yourOccupation').removeClass('d-block');
	}
}

function meansOfIncomeOtherV(value) {
	$('#stateMeansIncomeOther').addClass('d-none');
	$('#stateMeansIncomeOther').removeClass('d-block');

	if (value == 'Other') {
		$('#stateMeansIncomeOther').addClass('d-block');
		$('#stateMeansIncomeOther').removeClass('d-none');
	} else {
		$('#stateMeansIncomeOther').addClass('d-none');
		$('#stateMeansIncomeOther').removeClass('d-block');
	}
}

function ambulanceComeSceneV(value) {
	if (value == 'Yes') {
		$('#ambulanceComeSceneYes').addClass('d-block');
		$('#ambulanceComeSceneYes').removeClass('d-none');
		$('#ambulanceComeSceneNo').addClass('d-none');
		$('#ambulanceComeSceneNo').removeClass('d-block');
		$('#gpAfterAccident').addClass('d-none');
		$('#gpAfterAccident').removeClass('d-block');
		$('#goToHospitalYes').addClass('d-none');
		$('#goToHospitalYes').removeClass('d-block');
		$('#ambulanceTakeYouHospital').addClass('d-none');
		$('#ambulanceTakeYouHospital').removeClass('d-block');
		$('#hospitalDetail').addClass('d-none');
		$('#hospitalDetail').removeClass('d-block');
	} else {
		$('#ambulanceComeSceneYes').addClass('d-none');
		$('#ambulanceComeSceneYes').removeClass('d-block');
		$('#ambulanceComeSceneNo').addClass('d-block');
		$('#ambulanceComeSceneNo').removeClass('d-none');
		$('#gpAfterAccident').addClass('d-none');
		$('#gpAfterAccident').removeClass('d-block');
		$('#goToHospitalYes').addClass('d-none');
		$('#goToHospitalYes').removeClass('d-block');
		$('#ambulanceTakeYouHospital').addClass('d-none');
		$('#ambulanceTakeYouHospital').removeClass('d-block');
		$('#hospitalDetail').addClass('d-none');
		$('#hospitalDetail').removeClass('d-block');
	}
}

function ambulanceTreatSceneV(value) {
	if (value == 'Yes') {
		$('#goToHospitalYes').addClass('d-block');
		$('#goToHospitalYes').removeClass('d-none');
		$('#ambulanceTakeYouHospital').addClass('d-block');
		$('#ambulanceTakeYouHospital').removeClass('d-none');
	} else {
		$('#goToHospitalYes').addClass('d-none');
		$('#goToHospitalYes').removeClass('d-block');
		$('#ambulanceTakeYouHospital').addClass('d-block');
		$('#ambulanceTakeYouHospital').removeClass('d-none');
	}
}

function ambulanceTakenHospitalV(value) {
	if (value == 'Yes') {
		$('#hospitalDetail').addClass('d-block');
		$('#hospitalDetail').removeClass('d-none');
		$('#anyPainReliefSymptom').addClass('d-block');
		$('#anyPainReliefSymptom').removeClass('d-none');
		$('#gpAfterAccident').addClass('d-none');
		$('#gpAfterAccident').removeClass('d-block');
		$('#physiotherapyChrioOne').addClass('d-none');
		$('#physiotherapyChrioOne').removeClass('d-block');
	} else {
		$('#hospitalDetail').addClass('d-none');
		$('#hospitalDetail').removeClass('d-block');
		$('#gpAfterAccident').addClass('d-block');
		$('#gpAfterAccident').removeClass('d-none');
		$('#physiotherapyChrioOne').addClass('d-none');
		$('#physiotherapyChrioOne').removeClass('d-block');
	}
}

function goToHospitalV(value) {
	if (value == 'Yes') {
		$('#hospitalDetail').addClass('d-block');
		$('#hospitalDetail').removeClass('d-none');
		$('#anyPainReliefSymptom').addClass('d-block');
		$('#anyPainReliefSymptom').removeClass('d-none');
		$('#gpAfterAccident').addClass('d-none');
		$('#gpAfterAccident').removeClass('d-block');
		$('#goToHospitalYes').addClass('d-none');
		$('#goToHospitalYes').removeClass('d-block');
	} else {
		$('#hospitalDetail').addClass('d-none');
		$('#hospitalDetail').removeClass('d-block');
		$('#anyPainReliefSymptom').addClass('d-none');
		$('#anyPainReliefSymptom').removeClass('d-block');
		$('#goToHospitalYes').addClass('d-none');
		$('#goToHospitalYes').removeClass('d-block');
		$('#ambulanceTakeYouHospital').addClass('d-none');
		$('#ambulanceTakeYouHospital').removeClass('d-block');
		$('#gpAfterAccident').addClass('d-block');
		$('#gpAfterAccident').removeClass('d-none');
	}
}

function showVehicle(value) {
	if (value == 0) {
		$('.myvehicle').css('display', '');
		$('#myvehicle').html('Third Party Vehicle');
	} else {
		$('.myvehicle').css('display', 'none');
		$('#myvehicle').html('Client Vehicle');
	}
}

function anyPainReliefSymptomV(value) {
	if (value == 'Yes') {
		$('#physiotherapyChrioOne').addClass('d-block');
		$('#physiotherapyChrioOne').removeClass('d-none');
	} else {
		$('#physiotherapyChrioOne').addClass('d-block');
		$('#physiotherapyChrioOne').removeClass('d-none');
	}
}

function selectPhysioChiro(value) {
	if (value == 'Yes') {
		$('#bothPhysioChiroNew').addClass('d-block');
		$('#bothPhysioChiroNew').removeClass('d-none');
		$('#xrayMriOne').addClass('d-block');
		$('#xrayMriOne').removeClass('d-none');
	} else {
		$('#bothPhysioChiroNew').addClass('d-none');
		$('#bothPhysioChiroNew').removeClass('d-block');
		$('#xrayMriOne').addClass('d-block');
		$('#xrayMriOne').removeClass('d-none');
		$('#affordPhysioNew').addClass('d-none');
		$('#affordPhysioNew').removeClass('d-block');
		$('#eachVisitPhysio').addClass('d-none');
		$('#eachVisitPhysio').removeClass('d-block');
		$('#eachVisitChiro').addClass('d-none');
		$('#eachVisitChiro').removeClass('d-block');
	}
}

function selectOnePhysioChiro(value) {
	if (value == 'Yes') {
		$('#affordPhysioNew').addClass('d-block');
		$('#affordPhysioNew').removeClass('d-none');
		$('#affordChiroNew').addClass('d-none');
		$('#affordChiroNew').removeClass('d-block');
		$('#eachVisitChiro').addClass('d-none');
		$('#eachVisitChiro').removeClass('d-block');
		$('#eachVisitPhysio').addClass('d-none');
		$('#eachVisitPhysio').removeClass('d-block');
	} else {
		$('#affordPhysioNew').addClass('d-none');
		$('#affordPhysioNew').removeClass('d-block');
		$('#affordChiroNew').addClass('d-block');
		$('#affordChiroNew').removeClass('d-none');
		$('#eachVisitChiro').addClass('d-none');
		$('#eachVisitChiro').removeClass('d-block');
		$('#eachVisitPhysio').addClass('d-none');
		$('#eachVisitPhysio').removeClass('d-block');
	}
}

function physioYes(value) {
	if (value == 'Yes') {
		$('#eachVisitPhysio').addClass('d-block');
		$('#eachVisitPhysio').removeClass('d-none');
		$('#eachVisitChiro').addClass('d-none');
		$('#eachVisitChiro').removeClass('d-block');
	} else {
		$('#eachVisitPhysio').addClass('d-none');
		$('#eachVisitPhysio').removeClass('d-block');
		$('#eachVisitChiro').addClass('d-none');
		$('#eachVisitChiro').removeClass('d-block');
	}
}

function chiroYes(value) {
	if (value == 'Yes') {
		$('#eachVisitChiro').addClass('d-block');
		$('#eachVisitChiro').removeClass('d-none');
		$('#eachVisitPhysio').addClass('d-none');
		$('#eachVisitPhysio').removeClass('d-block');
	} else {
		$('#eachVisitPhysio').addClass('d-none');
		$('#eachVisitPhysio').removeClass('d-block');
		$('#eachVisitChiro').addClass('d-none');
		$('#eachVisitChiro').removeClass('d-block');
	}
}

function dateOfAccidentV(value) {
	var value_decrease = value;
	var acci_count = parseInt($('#txtAccidentTotalCount').val());
	value = value - acci_count;

	if (value > 0) {
		var i;
		for (i = 1; i <= value; i++) {
			num = $('.box-wrap').length + 1;

			while ($('#hfId' + num).length != 0) {
				num++;
			}
			
			var day_cal='';
			var mon_cal='';
			var year_cal='';
			var hours_cal='';
			var min_cal='';
			var running_year = (new Date).getFullYear();

			for(day=1;day<=31;day++)
			{
				day_cal+="<option value='"+day+"'>"+day+"</option>";
			}

			for(mon=1;mon<=12;mon++)
			{
				mon_cal+="<option value='"+mon+"'>"+mon+"</option>";
			}

			for(year=running_year;year>=running_year-100;year--)
			{
				year_cal+="<option value='"+year+"'>"+year+"</option>";
			}

			for(hours=0;hours<=23;hours++)
			{
				hours_cal+="<option value='"+hours+"'>"+hours+"</option>";
			}

			for(min=0;min<=59;min++)
			{
				min_cal+="<option value='"+min+"'>"+min+"</option>";
			}
			
			$('#dateOfAccident').append('<div class="box-wrap"> <div class="row"> <div class="col-lg-12"><img src="../img/close.png" class="float-right close-btn" style="width:15px; cursor:pointer;"/></div><div class="col-sm-12 col-md-4"> <div class="form-group"> <label for="txt2DOB'+num+ '">What was the date of this accident?</label> <input class="form-control" value="0" id="hfId'+num+ '" name="hfId['+num+ ']" type="hidden"/> <div class="form-group"> <div class="calender-wrap"> <div> <select required="required" name="txt2DOBDay['+num+']" id="txt2DOBDay'+num+'" class="form-control"> <option value="">Day</option>'+day_cal+' </select> </div><div> <select required="required" name="txt2DOBMonth['+num+']" id="txt2DOBMonth'+num+'" class="form-control"> <option value="">Month</option>'+mon_cal+' </select> </div><div> <select required="required" name="txt2DOBYear['+num+']" id="txt2DOBYear'+num+'" class="form-control"> <option value="">Year</option>'+year_cal+' </select> </div><div class="d-none"> <select required="required" name="txt2DOBHours['+num+']" id="txt2DOBHours'+num+'" class="form-control">'+hours_cal+' </select> </div><div class="d-none"> <select required="required" name="txt2DOBMinutes['+num+']" id="txt2DOBMinutes'+num+'" class="form-control">'+min_cal+' </select> </div></div></div></div></div><div class="col-sm-12 col-md-4"> <div class="form-group"> <label for="txt2AccidentType'+num+'">What kind of an accident was this?</label> <select required="required" class="d-flex form-control" name="txt2AccidentType['+num+ ']" id="txt2AccidentType'+num+ '"> <option value="">Select Kind Of Accident</option> <option value="1">Motor Vehicle Accident</option> <option value="2">Occupiers Liability</option> <option value="3">Public Liability</option> <option value="4">Accident at Work</option> </select> </div></div><div class="col-sm-12 col-md-4"> <div class="form-group"> <label for="txt2AccidentClaim'+num+ '">Do you wish to claim for this accident?</label> <select class="d-flex form-control" name="txt2AccidentClaim['+num+']" id="txt2AccidentClaim'+num+'"> <option value="1">Yes</option> <option value="0">No</option> </select> </div></div></div></div>');
			
		}
		/* $('.datepicker_class').bootstrapMaterialDatePicker({
			time: true,
			clearButton: false,
			format: 'YYYY-MM-DD HH:mm:ss'
		}); */
		$('select').rules("add", 'required');
		$('input').rules("add", 'required');
		$('#txtAccidentTotalCount').val(acci_count + value);
		$('#last_count').val(num);
	} else if (Math.abs(value - acci_count) > 0 && value_decrease>0) {
		var sel_cnt = $('.box-wrap').length;
		var select_count = parseInt($('#ddl2SelectCount').val());
		var loop = sel_cnt - parseInt(select_count);
		var i;
		for (i = 0; i < loop; i++) {
			$('#dateOfAccident .box-wrap:last').remove();
		}
		$('#txtAccidentTotalCount').val(acci_count - loop);
	} else {
		$('#dateOfAccident').empty();
		$('#last_count').val(0);
		$('#txtAccidentTotalCount').val(0);
	}
}

function witnessOne(value) {
	if (value == 1) {
		$('#witnessFields').addClass('d-block');
		$('#witnessFields').removeClass('d-none');
	} else {
		$('#witnessFields').addClass('d-none');
		$('#witnessFields').removeClass('d-block');
	}
}

function addPassengerDetails(value) 
{
	value=value-1;

	var existing_passengers = parseInt($('#txt8PassengersCount').attr('count_attr'));
	// var num = parseInt($('#txt8PassengersCount').attr('value'));
	num = existing_passengers;
	
	if(value==0)
	{
		$('#passengerDetailOne').empty();
		$('#txt8PassengersCount').attr('count_attr', 0);
		$('#txt8PassengersCount').attr('value',0);
		$('#hfSomethingDoneInPassenger').attr('value',1);
		$('#frm8Leads').submit();
	}
	else if (value > 0 && existing_passengers >= 0) 
	{
		if(existing_passengers > value)
		{
			while(existing_passengers > value)
			{
				$('#passengerDetailOne .box-passenger-wrap:last').remove();
				$('#txt8PassengersCount').attr('count_attr',existing_passengers-1);
				existing_passengers = parseInt($('#txt8PassengersCount').attr('count_attr'));
				$('#txt8PassengersCount').attr('value',existing_passengers);
			}
		}
		else
		{
			for (i = 1; i <= value-existing_passengers; i++)
			{
				num++;
				$('#passengerDetailOne').append('<div class="box-passenger-wrap"><div class="col-md-12 col-sm-12"> <div class="form-group"><label for="txt8Passenger['+num+']">Are you the driver of vehicle? &nbsp; </label><div class="custom-radio d-inline-block mr-3"><input attr_num="'+num+'" type="radio" class="custom-control-input" id="txt8YPassengerRadio'+num+'" onclick="checkdriver('+num+')" name="txt8PassengerRadio'+num+'" value="1" ><label class="custom-control-label" for="txt8YPassengerRadio'+num+'">Yes</label></div><div class="custom-radio d-inline-block mr-3"><input attr_num="'+num+'" checked type="radio" class="custom-control-input" id="txt8NPassengerRadio'+num+'" onclick="checkdriver('+num+')" name="txt8PassengerRadio'+num+'" value="0"><label class="custom-control-label" for="txt8NPassengerRadio'+num+'">No</label></div><textarea class="form-control text_class_change" name="txt8Passenger['+num+ ']" id="txt8Passenger'+num+ '" cols="30" rows="6" placeholder="Write in Brief"></textarea> </div></div></div>');
			}
			$('#txt8PassengersCount').attr('count_attr', num);
			$('#txt8PassengersCount').attr('value',num);
		}
	} 
	else
	{
		$('#passengerDetailOne').empty();
		$('#txt8PassengersCount').attr('count_attr', 0);
		$('#txt8PassengersCount').attr('value',0);
	}
	$('#hfSomethingDoneInPassenger').attr('value',1);
	
}

function displayOne() {
	$('#past-accident-detail').show();
	$('#personal-detail,#vehicle-detail,#injuries-matrix,#treatment-history,#passenger-details,#witness-details,#workCare-details,#accident-details').hide();
}

function displayTwo() {
	$('#accident-details').show();
	$('#vehicle-detail,#personal-detail,#injuries-matrix,#treatment-history,#passenger-details,#witness-details,#workCare-details,#past-accident-detail').hide();
}

function displayThree() {
	$('#vehicle-detail').show();
	$('#accident-details,#personal-detail,#injuries-matrix,#treatment-history,#passenger-details,#witness-details,#workCare-details,#past-accident-detail').hide();
}

function displayFour() {
	$('#injuries-matrix').show();
	$('#accident-details,#vehicle-detail,#personal-detail,#treatment-history,#passenger-details,#witness-details,#workCare-details,#past-accident-detail').hide();
}

function displayFive() {
	$('#treatment-history').show();
	$('#accident-details,#vehicle-detail,#injuries-matrix,#personal-detail,#passenger-details,#witness-details,#workCare-details,#past-accident-detail').hide();
}

function displaySix() {
	$('#workCare-details').show();
	$('#accident-details,#vehicle-detail,#injuries-matrix,#treatment-history,#personal-detail,#witness-details,#passenger-details,#past-accident-detail').hide();
}

function displaySeven() {
	$('#passenger-details').show();
	$('#accident-details,#vehicle-detail,#injuries-matrix,#treatment-history,#personal-detail,#witness-details,#workCare-details,#past-accident-detail').hide();
}

function displayEight() {
	$('#witness-details').show();
	$('#accident-details,#vehicle-detail,#injuries-matrix,#treatment-history,#passenger-details,#personal-detail,#workCare-details,#past-accident-detail').hide();
}

function checkdriver(i)
{
	var cnt=0;
	var clicked='';
	$(".box-passenger-wrap .custom-control-input:checked").each(function() 
	{
		var value = 0;
		if($(this).val()==1)
		{
			cnt++;
			if(cnt>1)
			{
				var key = confirm("Their can't be more than 1 driver. Are you sure you want to make him driver ?");
				if (!key) 
				{
					$("input[name="+selectedName+"][value=" + value + "]").prop('checked', true);
				} 
				else 
				{
					$(".box-passenger-wrap .custom-control-input:checked").each(function(index)
					{
						var divIndex = index+1;
						var selectedName = $(this).attr('name');
						console.log(selectedName);
						if(divIndex != i)
						{
							$("input[name="+selectedName+"][value=" + value + "]").prop('checked', true);
						}
					})
				}
			}
		}
	});
	$('#hfSomethingDoneInPassenger').attr('value',1);
}