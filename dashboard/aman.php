<?php 

$mysqli = new mysqli('127.0.0.1','root','','msw');
//$mysqli = new mysqli('166.62.30.154','dt_msw_test','e_uBLBjA-}Vb','dt_msw_test');

$datamaster = $mysqli->prepare("select leads.*, lead_comments.*,is_affliated from leads inner join callcenter on callcenter.uid = leads.callcenter_id left outer join lead_comments on lead_comments.lead_id = leads.id AND lead_comments.cid = ( SELECT MAX(cid) FROM lead_comments z WHERE z.lead_id = lead_comments.lead_id ) order by leads.updation_date desc limit 100 offset 0");
$datamaster->execute();
$queryResults = $datamaster->get_result();
$datamaster->close();

while($rc = mysqli_fetch_object($queryResults))
{
	$outcome_type = null;
	
	if($rc->comment_from == null)  //Lead Creation
	{
		$outcome_by = $rc->added_by;
		$outcome_type = getUserTypeDummy($rc->added_by)=='CA'?'AL':'ACL';// Fresh Agent --- Fresh Affliated Callcenter
	}
	else
	{
		$last_commented_user = getUserTypeDummy($rc->comment_from);
		$outcome_by = $rc->comment_from;
		
		if($rc->comment_utype=='CA')
		{
			$outcome_type = 'TLTA';// Team Lead To Agent
		}
		else if($rc->comment_utype=='CTL')
		{
			if($last_commented_user=='CCA')
			{
				$outcome_type = 'CTTL';//Callcenter to Team Lead
			}
			else if($last_commented_user=='CA')
			{
				$outcome_type = 'ATTL';//Agent to Team Lead
			}
		}
		else if($rc->comment_utype=='CCA')
		{
			if($last_commented_user=='CTL')
			{
				$outcome_type = 'TTC';//Team Lead to Callcenter
			}
			else if($last_commented_user=='MSW')
			{
				$outcome_type = $rc->is_affliated==1?'ATAC':'ATCC';  //Assigned to Affliated Callcenter: Assigned to Normal Callcenter
			}
		}
		else if($rc->comment_utype=='MSW')
		{
			if($last_commented_user=='CCA')
			{
				$outcome_type = $rc->is_affliated==1?'ACTA':'CTA';  //Affliated Callcenter to Admin: Normal Callcenter To Admin
			}
			else if($last_commented_user=='C')
			{
				$outcome_type = $rc->lead_transfer_type;  // RJ - Lead rejected, AP - Lead Approved, IP - Lead Invoice Paid
			}
		}
		else if($rc->comment_utype=='C')
		{
			if($last_commented_user=='MSW')
			{
				$outcome_type = $rc->lead_transfer_type=='LT'?'ATC':'SI'; // Sent to client --- Invoice is Sent
			}
			else if($last_commented_user=='C')
			{
				if($rc->lead_transfer_type!='FI')
				{					
					$outcome_type = $rc->lead_transfer_type;  //No Answer, CallBack Requested, Spoken to Client, Further Investigation, Sign Up Done
				}
				else
				{
					$outcome_type = getSecondLastComment($rc->id); // Further Investigation
					if($outcome_type=='LT')
					{
						$outcome_type = 'ATC';
					}
				}
			}
		}
	}
	
	if($outcome_type!=null)
	{
		outcome_function_dummy($rc->id,$outcome_by,$outcome_type);
	}
}



function outcome_function_dummy($lead_id, $outcome_by, $outcome_type)
{
	global $mysqli;
	
	$outcome = null;
	switch($outcome_type)
	{
		case 'NA': $outcome = 'No Answer'; break;
		case 'CB': $outcome = 'CallBack Requested'; break;
		case 'SC': $outcome = 'Spoken to Sol'; break;
		case 'FI': $outcome = 'Further Investigation'; break;
		case 'SU': $outcome = 'Sign Up Arranged'; break;
		case 'RJ': $outcome = 'Lead Rejected'; break;
		case 'AP': $outcome = 'Lead Approved'; break;
		case 'IP': $outcome = 'Invoice Paid'; break;
		case 'SI': $outcome = 'Invoice Sent'; break;
		case 'AL': $outcome = 'Fresh - Agent'; break; //New Lead Creation By Agent
		case 'ATTL': $outcome = 'Assigned To Team Lead'; break; //Agent To Team Lead
		case 'TTC': $outcome = 'Assigned To Callcenter'; break; //Team Lead to Callcenter
		case 'ACTA': $outcome = 'Pending - Sent From Affliated Callcenter To Admin'; break; //Affliated Callcenter To Admin
		case 'CTA': $outcome = 'Pending - Sent From Normal Callcenter To Admin'; break; //Callcenter To Admin
		case 'ATC': $outcome = 'Sent To Sol'; break; //Admin To Client
		case 'ATCC': $outcome = 'Sent Back To Normal Callcenter'; break; //Admin To Normal Callcenter
		case 'ATAC': $outcome = 'Sent Back To Affliated Callcenter'; break; //Admin To Affliated Callcenter
		case 'ACL': $outcome = 'Fresh - Affliated Callcenter'; break; //New Lead Creation By Affliated Callcenter
		case 'CTTL': $outcome = 'Sent Back To Team Lead'; break; //Callcenter To Team Lead
		case 'TLTA': $outcome = 'Sent Back To Agent'; break; //Team Lead To Agent
	}
	
	if($outcome!=null)
	{
		/* $loginmaster = $mysqli->prepare("insert into lead_outcomes(lead_id, outcome_by, outcome, outcome_type, creation_date)values(?,?,?,?,utc_timestamp())");
		$loginmaster->bind_param("iiss",$lead_id, $outcome_by, $outcome, $outcome_type);
		$loginmaster->execute();
		$loginmaster->close(); */
		
		//insert in lead table also with new field outcome
		$loginmaster = $mysqli->prepare("update leads set outcome=?, outcome_type=? where id=?");
		$loginmaster->bind_param("ssi",$outcome, $outcome_type, $lead_id);
		$loginmaster->execute();
		$loginmaster->close();
	}
}

function getUserTypeDummy($uid)
{
	global $mysqli;

	$loginmaster = $mysqli->prepare("select type from login_master where uid=?");
	$loginmaster->bind_param("i",$uid);
	$loginmaster->execute();
	$loginmaster->bind_result($usertype);
	$loginmaster->fetch();
	$loginmaster->close();
	
	return $usertype;
}

function getSecondLastComment($uid)
{
	global $mysqli;

	$loginmaster = $mysqli->prepare("select lead_transfer_type from lead_comments where lead_id=? and lead_transfer_type!='FI' order by creation_date desc limit 1");
	$loginmaster->bind_param("i",$uid);
	$loginmaster->execute();
	$loginmaster->bind_result($usertype);
	$loginmaster->fetch();
	$loginmaster->close();
	
	return $usertype;
}

?>