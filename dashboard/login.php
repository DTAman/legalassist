<?php
include('includes/basic_no_auth.php');
$_SESSION["is_affliated"] = 0;

if (isset($_POST["btnLogin"]))
{
	if(trim($_POST["txtUserName"]) != null && trim($_POST["txtPassword"]) != null)
	{		
		$loginmaster = $mysqli->prepare("select uid, phone, type, password,is_online from login_master where phone=? and status='AC' and is_deleted=0");
		$loginmaster->bind_param("s", $_POST["txtUserName"]);
		$loginmaster->execute();
		$loginmaster->bind_result($signup_id, $signup_mobile,$signup_type, $signup_password,$is_online);
		if ($loginmaster->fetch() > 0 && create_password($_POST["txtPassword"]) == $signup_password)
		{
			$loginmaster->close();
			
			$_SESSION["userId"] = $signup_id;
			$_SESSION["userType"] = $signup_type;
			$_SESSION["userName"] = $signup_mobile;
			$_SESSION["userMobile"] = $signup_mobile;
			$_SESSION["userIsOnline"] = $is_online;
			
			if(isset($_SESSION["userType"]) && trim($_SESSION["userType"])=='CCA')
			{
				$loginmaster = $mysqli->prepare("select is_affliated from callcenter where uid=?");
				$loginmaster->bind_param("i", $signup_id);
				$loginmaster->execute();
				$loginmaster->bind_result($is_affliated);
				$loginmaster->fetch();
				$loginmaster->close();
				
				$_SESSION["is_affliated"] = $is_affliated;
			}
			
			$_SESSION['userTimeZone'] = getUserTimezone();
			
			header("location:admin/");
		}
		else
		{
			$_SESSION['error'] = "Invalid Credentials";
		}
	}
	else
	{
		$_SESSION['error'] = "All fields are required.";
	}
}

if(isset($_SESSION["userId"]) && isset($_SESSION["userType"]) && isset($_SESSION["userName"]) && isset($_SESSION["userMobile"]))
{
	header("location:admin/");
}

?>
<html lang="en">
	<head>
		<?php include('includes/header.php'); ?>
	</head>
	<body>
	
		<div class="container-fluid">
			<div class="row">
				<div class="hidden-xs hidden-sm col-lg-6 col-md-6 gredient-bg">
					<div class="clearfix">
						<div class="animated fadeindown logo-title-container text-center">
							<h3 class="cl-white text-upper">Welcome To</h3>
							<img class="img-responsive" src="img/logo-white.png" alt="Logo Icon">
							 
						</div> <!-- .logo-title-container -->
					</div>
				</div>

				<div class="col-12 col-sm-12 col-md-6 col-lg-6 login-sidebar animated fadeindown">

					<div class="login-container animated fadeindown">

						<h2 class="text-center text-upper">Login</h2>
						<form class="form-horizontal" action="#" name="frmLogin" id="frmLogin" method="post">
							<div class="form-group">
								<?php include("includes/messages.php") ?>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="txtUserName" name="txtUserName" placeholder="Mobile Number" maxlength='10' />
								<i class="fa fa-user"></i>
							</div>
							
							<div class="form-group help">
								<input type="password" class="form-control" id="txtPassword" name="txtPassword" placeholder="Password" />
								<i class="fa fa-lock"></i>
							</div>
							
							<div class='col-md-12 text-center'>
								<input type="submit" class="btn gredient-bg" value="Login" name='btnLogin' id='btnLogin' />
							</div>
						
						</form>
					</div> 
					<!-- .login-container -->
					
				</div> <!-- .login-sidebar -->
			</div> <!-- .row -->
		</div>
		<?php include('includes/web_footer.php'); ?>
	</body>
</html>
