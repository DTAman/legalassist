<?php
include("includes/head.php");
?>

<body class="" contenteditable="false">
   <!-- Page-->
   <div class="page">
      <!-- Panel Thumbnail-->
      <!-- Template panel-->
      <div class="layout-panel-wrap">
         <div class="layout-panel">
         </div>
      </div>
      <?php
		include("includes/header.php");
	  ?>

      <section class="detail">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6">
                  <div class="contentee">
                     <h4 class="heading-decorated">TPD Claims - Total and Permanent Disability</h4>
                     <p>Our expert superannuation and insurance claims lawyers will ensure you get everything you’re
                        entitled to.</p>
                     <a class="button button-primary mt-4" href="contact-us.php">Contact us</a>
                  </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="sImage">
                     <img src="images/s2.png" alt="" class="img-fluid">
                  </div>
               </div>
            </div>
         </div>
         <div class="bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <p><b>TPD Claims can be categorized into following:</b></p>
                     <ol>
                        <li>
                           <p>Own Occupation TPD – inability to work in own occupation ever again comes under this
                              category of TPD</p>
                        </li>
                        <li>
                           <p>Any Occupation TPD – the inability of the claimant to perform work in their occupation and
                              also in any occupation that they are suitable for via training, experience or education
                              ever again.</p>
                        </li>
                        <li>
                           <p>Non-Occupational TPD - the claimant unable to conduct daily living activities (probably 2
                              of 5 activities) comes under Non-Occupational TPD Claims.</p>
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-12 mt-4">
                  <div class="content">
                     <h3>What you need to know about total & permanent disability (TPD) claims</h3>
                     <p>Most workers are covered for total and permanent disability (TPD) insurance through the
                        superannuation fund contributions made by their employer.
                        You may also have TPD insurance cover under a life insurance policy provided by your employer as
                        part of your EBA or Employment contract, via a union, from your financial institution or bank,
                        or as arranged by your financial advisor.
                        Exclusion or eligibility clauses, detailed in an insurance policy's small print, can affect your
                        right to make a claim, and insurance companies often use this small print to deny claims.
                        Injury Assist Helpline’s TPD lawyers have a great deal of experience working out the fine print
                        in insurance policies, and will work to get your full entitlements—if you'd like to file a TPD
                        claim, we're here to help.
                     </p>
                     <h3>Total and permanent disability claims (TPD)</h3>
                     <p>You’re eligible for a TPD compensation claim if you can show you’re unable to do your normal job
                        or any other work related to your training and experience. This doesn’t mean you’re unfit for
                        all work—it means you're unable to do the work fitting your area of skills and expertise.
                        For example, if having a bad back stops you from working in your normal construction job, you
                        can usually make a TPD claim—even if your doctor says you’re fit for another kind of employment,
                        such as office work.
                     </p>
                     <h3>Total and temporary disability claims (TTD)</h3>
                     <p>If you’re unable to do your normal job for a while, whether for two months or two years, you may
                        qualify for TTD monthly payments.
                        TTD payments are usually up to 75% of your normal income, and are payable for two years, or up
                        to the age of 65. However, a waiting period of 30 to 90 days applies before you can start
                        receiving these payments.
                        TTD payments may be offset against any workers' compensation, motor-vehicle injury compensation
                        or Centrelink payments you receive, and may stop if your employment is terminated or if you
                        receive a TPD insurance payment. Each person's situation is different.
                     </p>
                     <h3>Why Injury Assist Helpline?</h3>
                     <p>Injury Assist Helpline's TPD lawyers are the experts. We can help you to gain early access to
                        your funds when you need them the most. We understand the importance of having someone in your
                        corner when you're taking on a well-resourced insurance company.</p>
                     <p>At Injury Assist Helpline we understand the impact that an unexpected illness, injury or
                        disability can have on every aspect of your life. Our team can help you with the complicated
                        process of making a claim at the time you need it most—when everything has changed and you are
                        worried about the future.</p>
                     <p>We offer 'no win, no fee'* arrangements for these types of cases, which means that you don’t
                        have to pay for our legal services if we don't win. We have superannuation and TPD insurance
                        claims lawyers in Melbourne, Sydney, Brisbane, Perth, Adelaide, Darwin and throughout Australia.
                     </p>
                     <p>Contact us today to receive a free superannuation and insurance check and to find out how we can
                        help.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Call to Action-->
      <section class="section section-sm context-dark bg-gray-dark section-cta">
         <div class="container">
            <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
               <div class="col-xl-8 text-xl-left">
                  <h4><span class="font-weight-bold">Extrafast</span><span class="font-weight-normal">offers flexible
                        solutions with lots of advantages</span></h4>
               </div>
               <div class="col-xl-2 text-xl-right"><a class="button button-primary" target="_blank" data-toggle="modal"
                     data-target="#modalLogin">Get in touch</a></div>
            </div>
         </div>
      </section>
        <?php
	  include("includes/footer_one.php");
	  ?>
   </div>
    <?php
	  include("includes/footer_two.php");
	  ?>
</body>
<!-- Google Tag Manager -->

</html>