<?php
include("includes/head.php");
?>

<body class="" contenteditable="false">
   <!-- Page-->
   <div class="page">
      <!-- Panel Thumbnail-->
      <!-- Template panel-->
      <div class="layout-panel-wrap">
         <div class="layout-panel">
         </div>
      </div>
      <?php
		include("includes/header.php");
	  ?>

      <section class="detail pt-5">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <h4 class="heading-decorated">WERE YOU CALLED BY US?</h4>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-lg-8 order-1 order-lg-0 mt-4">
                  <div class="content">
                     <p>Injury Assist Helpline advertises on many different social media links and websites.</p>
                     <p>These can be a survey based website, competition based website or a questionnaire based website.
                     </p>
                     <p>You would have submitted the contact details on such Injury Assist Helpline associated websites.
                     </p>
                     <p>We take clear authorisation if Injury Assist Helpline can call you to assist you further in your
                        case.
                     </p>
                     <br>
                     <p><b>If you are still unsure about how we got your details, expert Injury Assist Helpline Advisors
                           are available to clear any doubts you may have!</b>
                     </p>
                     <a class="button button-primary mt-4" href="contact-us.php">Contact us</a>
                  </div>
               </div>
               <div class="col-12 col-lg-4 order-0 order-lg-1">
                  <div class="imgg2"></div>
               </div>
            </div>
         </div>
      </section>
      <!-- Call to Action-->
      <section class="section section-sm context-dark bg-gray-dark section-cta">
         <div class="container">
            <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
               <div class="col-xl-8 text-xl-left">
                  <h4><span class="font-weight-bold">Extrafast</span><span class="font-weight-normal">offers flexible
                        solutions with lots of advantages</span>
                  </h4>
               </div>
               <div class="col-xl-2 text-xl-right"><a class="button button-primary" target="_blank" data-toggle="modal"
                     data-target="#modalLogin">Get in touch</a></div>
            </div>
         </div>
      </section>
        <?php
	  include("includes/footer_one.php");
	  ?>
   </div>
    <?php
	  include("includes/footer_two.php");
	  ?>
</body>
<!-- Google Tag Manager -->

</html>