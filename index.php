<?php
include("includes/head.php");
?>

<body class="" contenteditable="false">
   <!-- Page-->
   <div class="page">
      <!-- Panel Thumbnail-->
      <!-- Template panel-->
      <div class="layout-panel-wrap">
         <div class="layout-panel">
         </div>
      </div>
      <?php
		include("includes/header.php");
	  ?>

      <!-- Swiper-->
      <section id="home">
         <div class="swiper-container swiper-slider swiper-slider_fullheight swiper-container-horizontal"
            data-simulate-touch="false" data-loop="false" data-autoplay="false">
            <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);">
               <div class="swiper-slide context-dark bg-overlay-darker swiper-slide-next"
                  data-slide-bg="images/slider-slide-1-1920x1080.jpg" style="background-size: cover; width: 1903px;">
                  <div class="swiper-slide-caption text-center">
                     <div class="container">
                        <h1><span>Get The Compensation You Deserve</span></h1>
                        <h3>Contact us today for a No Obligation Free Assessment!!!</h3>
                        <div class="group-lg group-middle"><a class="button button-primary" href="#!#features"
                              data-custom-scroll-to="features">View more</a><a class="button button-black" href="#!#"
                              data-toggle="modal" data-target="#modalLogin">Contact us</a></div>
                     </div>
                  </div>
               </div>
               <div class="swiper-slide context-dark bg-overlay-darker"
                  data-slide-bg="images/slider-slide-3-1920x1080.jpg" style="background-size: cover; width: 1903px;">
                  <div class="swiper-slide-caption">
                     <div class="container">
                        <h2>Comprehensive. Affordable. Responsible.</h2>
                        <h3>We dare you to find a cheaper one!!!</h3>
                        <a class="button button-primary" href="#!#" data-toggle="modal"
                           data-target="#modalLogin">Contact us</a>
                     </div>
                  </div>
               </div>
               <!-- <div class="swiper-slide context-dark bg-overlay-darker swiper-slide-active" data-slide-bg="images/slider-slide-1-1920x1080.jpg" style="background-size: cover; width: 1903px;"> -->
               <!-- <div class="swiper-slide-caption text-center"> -->
               <!-- <div class="container"> -->
               <!-- <h1 style="font-size: 60px"><span>NSW, WA, ACT, QLD, VIC, SA</span></h1> -->
               <!-- <div class="group-lg group-middle"><a class="button button-primary" href="#!#features" data-custom-scroll-to="features">View more</a><a class="button button-black" href="#!#" data-toggle="modal" data-target="#modalLogin">Contact us</a></div> -->
               <!-- </div> -->
               <!-- </div> -->
               <!-- </div> -->
               <div class="swiper-slide context-dark bg-overlay-darker"
                  data-slide-bg="images/slider-slide-4-1920x1080.jpg" style="background-size: cover; width: 1903px;">
                  <div class="swiper-slide-caption text-center">
                     <div class="container">
                        <div class="row justify-content-center">
                           <div class="col-lg-7">
                              <h2>BE LEGAL FOR LESS!</h2>
                              <h5 class="text-width-2 block-centered">It’s fast, free and secure</h5>
                              <div class="group-lg group-middle"><a class="button button-black" href="#!#features"
                                    data-custom-scroll-to="features">View more</a><a class="button button-primary"
                                    href="#!#" data-toggle="modal" data-target="#modalLogin">Contact us</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Swiper Pagination-->
            <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span
                  class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span
                  class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span></div>
            <!-- Swiper Navigation-->
            <div class="swiper-button-prev linear-icon-chevron-left swiper-button-disabled">
               <ion-icon name="arrow-back"></ion-icon>
            </div>
            <div class="swiper-button-next linear-icon-chevron-right">
               <ion-icon name="arrow-forward"></ion-icon>
            </div>
         </div>
      </section>
      <!-- Our Services-->
      <section class="section-md bg-default text-center">
         <div class="container">
            <h4 class="heading-decorated">Our Services</h4>
            <div class="row row-50 justify-content-md-center justify-content-lg-start">
               <div class="col-md-6 col-lg-4">
                  <!-- Blurb circle-->
                  <article class="blurb blurb-minimal h-100">
                     <div class="unit flex-row unit-spacing-md">
                        <div class="unit-left">
                           <div class="blurb-minimal__icon">
                              <span class="icon">
                                 <ion-icon name="car"></ion-icon>
                              </span>
                           </div>
                        </div>
                        <div class="unit-body">
                           <p class="blurb__title" style="position: relative;"><a class="heading-6"
                                 href="motor-vehicle-accident.php">Motor Vehicle Accidents</a></p>
                           <!-- <p>To be able to make a claim with us and our expert solicitors, your road accident must have:</p> -->
                           <p>Have you or your loved one sustained injuries while driving or through driver negligence?
                           </p>
                           <p>Were you or your loved one injured while on a public transport or as a pedestrian?</p>
                           <!-- <p>As a result of your injuries have you suffered any loss such as pain and suffering or loss of income?</p> -->
                           <!-- <ul class="mt-1"> -->
                           <!-- <li>Happened within the last three years</li> -->
                           <!-- <li>Been somebody else's fault</li> -->
                           <!-- <li>Caused injury to you</li> -->
                           <!-- </ul> -->
                        </div>
                     </div>
                  </article>
               </div>
               <div class="col-md-6 col-lg-4">
                  <!-- Blurb circle-->
                  <article class="blurb blurb-minimal">
                     <div class="unit flex-row unit-spacing-md">
                        <div class="unit-left">
                           <div class="blurb-minimal__icon">
                              <span class="icon">
                                 <ion-icon name="briefcase"></ion-icon>
                              </span>
                           </div>
                        </div>
                        <div class="unit-body">
                           <p class="blurb__title" style="position: relative;"><a class="heading-6"
                                 href="work-accidents.php">Work Accidents</a></p>
                           <p>If you've been injured at work in the last three years and it wasn't your fault, you may
                              be able to claim compensation. Injuries and work-related illnesses are often caused by
                              employers and managers failing to follow health and safety rules
                           </p>
                        </div>
                     </div>
                  </article>
               </div>
               <div class="col-md-6 col-lg-4">
                  <!-- Blurb circle-->
                  <article class="blurb blurb-minimal h-100">
                     <div class="unit flex-row unit-spacing-md">
                        <div class="unit-left">
                           <div class="blurb-minimal__icon">
                              <span class="icon">
                                 <ion-icon name="barcode"></ion-icon>
                              </span>
                           </div>
                        </div>
                        <div class="unit-body">
                           <p class="blurb__title" style="position: relative;"><a class="heading-6"
                                 href="tpd-claim.php">TPD Claims</a></p>
                           <p>In most cases with TPD claims, to qualify you must show that you are permanently unfit for
                              your usual employment, or any other employment for which you are qualified based on your
                              education, training and experience.
                           </p>
                        </div>
                     </div>
                  </article>
               </div>
            </div>
         </div>
      </section>
      <!-- About us-->
      <section class="bg-gray-lighter object-wrap" id="about">
         <div class="section-lg">
            <div class="container">
               <div class="row justify-content-end">
                  <div class="col-lg-6 pl-lg-5">
                     <h4 class="heading-decorated">We're Injury Assist Helpline </h4>
                     <p>We're Australia's leading provider of personal injury advice, services and support. With
                        25 years' experience, we've helped more people injured in accidents than anyone else.<br>
                        We believe that if you've had an injury and it feels wrong, it probably is - and we're committed
                        to helping you make it right. We do this by guiding you through the steps of making a personal
                        injury claim, and by giving free, impartial advice on compensation.
                     </p>
                     <div class="row row-30">
                        <div class="col-xl-12">
                           <!-- Blurb minimal-->
                           <article class="blurb blurb-minimal">
                              <div class="unit flex-row unit-spacing-md">
                                 <div class="unit-left">
                                    <div class="blurb-minimal__icon">
                                       <span class="icon">
                                          <ion-icon name="construct"></ion-icon>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="unit-body">
                                    <p class="blurb__title heading-6"><a href="#!#">Our team</a></p>
                                    <p>
                                       Our advisors based in our legal support centres are all
                                       legally trained, so that they're ready to help you when you call us.
                                       <!-- We also have a network of specialist personal injury solicitors across Canada. This means we can put you forward to the right solicitor if you do decide you'd like to go ahead with a claim. We monitor the standards of all our solicitors to make sure you're receiving the best possible service. -->
                                       <br>
                                       Our solicitors work on a <a href="no-win.php">no win no fee</a> basis, which
                                       means if your claim isn't successful, then you won't need to pay any money.
                                       <!-- <br>
                                             Visit our <a href="no-win.php">no win no fee page</a> to learn more -->
                                    </p>
                                 </div>
                              </div>
                           </article>
                        </div>
                        <div class="col-xl-12">
                           <!-- Blurb minimal-->
                           <article class="blurb blurb-minimal">
                              <div class="unit flex-row unit-spacing-md">
                                 <div class="unit-left">
                                    <div class="blurb-minimal__icon">
                                       <span class="icon">
                                          <ion-icon name="people"></ion-icon>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="unit-body">
                                    <p class="blurb__title heading-6"><a href="#!#">Our campaign against cold
                                          calling</a>
                                    </p>
                                    <p>
                                       We've NEVER made a single cold call. And we never will.
                                       <br>We only ever get in touch with people who have asked us to. This means we
                                       don't send spam texts or emails, or engage in any other form of nuisance
                                       marketing either.
                                       <!-- <br>
                                             In fact, we actively campaign against this practice.You can also find helpful advice and information on what you can do if you've received cold calls or spam texts on our wereyoucalledbyus page. -->
                                    </p>
                                 </div>
                              </div>
                           </article>
                        </div>
                        <!-- <div class="col-xl-12">
                              <article class="blurb blurb-minimal">
                                 <div class="unit flex-row unit-spacing-md">
                                    <div class="unit-left">
                                       <div class="blurb-minimal__icon"><span class="icon"><ion-icon name="people"></ion-icon></span></div>
                                    </div>
                                    <div class="unit-body">
                                       <p class="blurb__title heading-6"><a href="#!#">The Ethical Marketing Charter</a></p>
                                       <p>Injury Assist Helpline is part of the Ethical Marketing Charter, set up with other industry leaders in 2015 to encourage organisations in the personal injury sector to uphold the highest ethical and professional standards.
                                       <br>
                                       The Charter, supported by parliamentarians and industry regulators, calls on its members to commit to:
                                       <ul>
                                          <li>Stand firmly against nuisance calling, never cold calling or sending spam texts or spam emails</li>
                                          <li>Stand firmly against the unethical buying and selling of accident data, never misusing accident data to pressure people into making a claim</li>
                                          <li>Stand firmly against inappropriate and misleading advertising, never inducing personal injury claims by making false or misleading promises in their advertising, and are clear and upfront about any exclusions to</li>
                                          <li> no win no fee</li>
                                       </ul>
                                       </p>
                                    </div>
                                 </div>
                              </article>
                              </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="object-wrap__body object-wrap__body-sizing-1 object-wrap__body-md-left bg-image"
            style="background-image: url(images/bg-image-1.jpg);background-position: 0 90%;"></div>
      </section>
      <!-- Counters-->
      <section class="section parallax-container context-dark">
         <div class="parallax-content">
            <div class="container section-md">
               <div class="row justify-content-md-center row-50">
                  <div class="col-md-4">
                     <!-- Box counter-->
                     <article class="box-counter">
                        <div class="box-counter__icon">
                           <ion-icon name="filing"></ion-icon>
                        </div>
                        <div class="box-counter__wrap text-white">
                           <div class="counter">100+</div>
                        </div>
                        <p class="box-counter__title">New Clients</p>
                     </article>
                  </div>
                  <div class="col-md-4">
                     <!-- Box counter-->
                     <article class="box-counter">
                        <div class="box-counter__icon">
                           <ion-icon name="cube"></ion-icon>
                        </div>
                        <div class="box-counter__wrap text-white">
                           <div class="counter">98</div>
                           <span>%</span>
                        </div>
                        <p class="box-counter__title">Positive Reviews</p>
                     </article>
                  </div>
                  <div class="col-md-4">
                     <!-- Box counter-->
                     <article class="box-counter">
                        <div class="box-counter__icon">
                           <ion-icon name="contacts"></ion-icon>
                        </div>
                        <div class="box-counter__wrap text-white">
                           <div class="counter">1000s</div>
                        </div>
                        <p class="box-counter__title">Happy clients</p>
                     </article>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Why Choose Us -->
      <section class="section-md bg-default" id="features">
         <div class="container">
            <div class="row justify-content-md-center justify-content-lg-between row-50 align-items-center">
               <div class="col-md-8 col-lg-6">
                  <h4 class="heading-decorated">Why Choose Us</h4>
                  <!-- Blurb minimal-->
                  <article class="blurb blurb-minimal">
                     <div class="unit flex-row unit-spacing-md">
                        <div class="unit-left">
                           <div class="blurb-minimal__icon">
                              <span class="icon">
                                 <ion-icon name="trophy"></ion-icon>
                              </span>
                           </div>
                        </div>
                        <div class="unit-body">
                           <p class="blurb__title heading-6">Our No Win No Fee guarantee</p>
                           <p>No win no fee arrangements take the risk out of making an accident claim, because you
                              don’t have to pay anything if you lose your case.
                              <br>
                              Winning clients typically pay up to 40% of the compensation they secure towards legal
                              fees, so you will never have to pay anything upfront and there are no hidden charges so
                              you’ll never be left out of pocket.
                           </p>
                        </div>
                     </div>
                  </article>
                  <!-- Blurb minimal-->
                  <article class="blurb blurb-minimal">
                     <div class="unit flex-row unit-spacing-md">
                        <div class="unit-left">
                           <div class="blurb-minimal__icon">
                              <span class="icon">
                                 <ion-icon name="briefcase"></ion-icon>
                              </span>
                           </div>
                        </div>
                        <div class="unit-body">
                           <p class="blurb__title heading-6">Claims across the nation - OUR DIGITAL MARKETING CAMPAIGN
                           </p>
                           <p>For many people, watching our digital Ad's is the first step in the compensation claims
                              process and making something that feels wrong, right.<br />
                              As a national helpline in Australia, we help 100s of people every day. The
                              conversations which take place between our customers and Legal Support Advisors are
                              central to everything we do. We're proud of the friendly and empathetic service they
                              provide, and we felt that it was right to focus on this in our marketing campaigns.
                           </p>
                        </div>
                     </div>
                  </article>
                  <!-- Blurb minimal-->
                  <article class="blurb blurb-minimal">
                     <div class="unit flex-row unit-spacing-md">
                        <div class="unit-left">
                           <div class="blurb-minimal__icon">
                              <span class="icon">
                                 <ion-icon name="podium"></ion-icon>
                              </span>
                           </div>
                        </div>
                        <div class="unit-body">
                           <p class="blurb__title heading-6">ADVICE AND ASSISTANCE</p>
                           <p>Being a victim, you must be having numerous questions in mind, Injury Assist Helpline
                              assists you free of cost and makes you feel comfortable in a way that you can contact us
                              anytime you want.
                           </p>
                        </div>
                     </div>
                  </article>
               </div>
               <div class="col-md-7 col-lg-4">
                  <figure class="image-sizing-1"><img src="images/home-business.png" alt="" width="391" height="642">
                  </figure>
               </div>
            </div>
         </div>
      </section>
      <!-- Call to Action-->
      <section class="section section-sm context-dark bg-gray-dark section-cta">
         <div class="container">
            <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
               <div class="col-xl-8 text-xl-left">
                  <h4><span class="font-weight-bold">Extrafast</span><span class="font-weight-normal">offers flexible
                        solutions with lots of advantages</span>
                  </h4>
               </div>
               <div class="col-xl-2 text-xl-right"><a class="button button-primary" target="_blank" data-toggle="modal"
                     data-target="#modalLogin">Get in touch</a></div>
            </div>
         </div>
      </section>
      <!-- Our Clients -->
      <!-- <section class="section-md text-center bg-default" id="clients">
            <div class="container">
               <h4 class="heading-decorated">Our Clients</h4>
               <div class="row row-30">
                  <div class="col-sm-6 col-md-3">
                     <figure class="box-icon-image"><a href="#!#"><img src="images/c1.png" alt="" width="126" height="102"></a></figure>
                  </div>
                  <div class="col-sm-6 col-md-3">
                     <figure class="box-icon-image"><a href="#!#"><img src="images/c2.png" alt="" width="134" height="102"></a></figure>
                  </div>
                  <div class="col-sm-6 col-md-3">
                     <figure class="box-icon-image"><a href="#!#"><img src="images/c3.png" alt="" width="132" height="102"></a></figure>
                  </div>
                  <div class="col-sm-6 col-md-3">
                     <figure class="box-icon-image"><a href="#!#"><img src="images/c4.png" alt="" width="126" height="102"></a></figure>
                  </div>
                  <div class="col-sm-6 col-md-3">
                     <figure class="box-icon-image"><a href="#!#"><img src="images/c5.png" alt="" width="138" height="102"></a></figure>
                  </div>
                  <div class="col-sm-6 col-md-3">
                     <figure class="box-icon-image"><a href="#!#"><img src="images/c6.png" alt="" width="143" height="102"></a></figure>
                  </div>
                  <div class="col-sm-6 col-md-3">
                     <figure class="box-icon-image"><a href="#!#"><img src="images/c7.png" alt="" width="109" height="102"></a></figure>
                  </div>
                  <div class="col-sm-6 col-md-3">
                     <figure class="box-icon-image"><a href="#!#"><img src="images/c8.png" alt="" width="109" height="102"></a></figure>
                  </div>
               </div>
            </div>
            </section> -->
      <!-- Posts-->
      <!-- <section class="section-md bg-default" id="news">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12 text-center">
                     <h4 class="heading-decorated">Recent News</h4>
                  </div>
               </div>
               <div class="row row-60">
            
                  <div class="col-sm-4">
                     <div class="owl-item">
                        <article class="post-classic post-minimal">
                           <a class="post-minimal-image" href="#!#"><img src="images/news1.jpg" alt="" width="418" height="315"></a>
                           <div class="post-classic-title">
                              <h6><a href="#!#">Top 10 Web Trends in 2018</a></h6>
                           </div>
                           <div class="post-meta">
                              <div class="group"><a href="#!#">
                                 <time datetime="2018">Jan.20, 2018</time></a><a class="meta-author" href="#!#">by Brian Williamson</a>
                              </div>
                           </div>
                           <div class="post-classic-body">
                              <p>In this article, we have gathered a list of recent web design trends according to the world’s renowned web designers and their projects.</p>
                           </div>
                        </article>
                     </div>
                  </div>
            
                  <div class="col-sm-4">
                     <div class="owl-item">
                        <article class="post-classic post-minimal">
                           <a class="post-minimal-image" href="#!#"><img src="images/news2.jpg" alt="" width="418" height="315"></a>
                           <div class="post-classic-title">
                              <h6><a href="#!#">Top 10 Web Trends in 2018</a></h6>
                           </div>
                           <div class="post-meta">
                              <div class="group"><a href="#!#">
                                 <time datetime="2018">Jan.20, 2018</time></a><a class="meta-author" href="#!#">by Brian Williamson</a>
                              </div>
                           </div>
                           <div class="post-classic-body">
                              <p>In this article, we have gathered a list of recent web design trends according to the world’s renowned web designers and their projects.</p>
                           </div>
                        </article>
                     </div>
                  </div>
            
                  <div class="col-sm-4">
                     <div class="owl-item">
                        <article class="post-classic post-minimal">
                           <a class="post-minimal-image" href="#!#"><img src="images/news3.jpg" alt="" width="418" height="315"></a>
                           <div class="post-classic-title">
                              <h6><a href="#!#">Top 10 Web Trends in 2018</a></h6>
                           </div>
                           <div class="post-meta">
                              <div class="group"><a href="#!#">
                                 <time datetime="2018">Jan.20, 2018</time></a><a class="meta-author" href="#!#">by Brian Williamson</a>
                              </div>
                           </div>
                           <div class="post-classic-body">
                              <p>In this article, we have gathered a list of recent web design trends according to the world’s renowned web designers and their projects.</p>
                           </div>
                        </article>
                     </div>
                  </div>
               </div>
            </div>
            </section> -->
      <!-- Pricing table-->
      <!-- <section class="section-lg bg-gray-lighter text-center" id="pricing-charts">
            <div class="container">
               <h4 class="heading-decorated">PRICING CHART</h4>
               <div class="row row-50 justify-content-lg-center">
                  <div class="col-md-6 col-lg-4 col-xl-3">
                     <div class="pricing-table-wrap">
                        <div class="pricing-table-custom">
                           <h5>Available options</h5>
                           <ul class="list-xxs small">
                              <li>Offered service</li>
                              <li>Maximum clients</li>
                              <li>24/7 support</li>
                              <li>Additional service</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-4 col-xl-3">
                     <div class="pricing-table-wrap">
                        <div class="pricing-table pricing-table-label">
                           <div class="pricing-label">
                              <span>most popular</span>
                              <svg width="86px" height="86px" viewBox="0 0 86 86">
                                 <path d="M73.4,73.4L67.3,73l-0.8,6.2l-6-1.8l-2.1,5.9l-5.5-3l-3.2,5.3l-4.7-4L40.7,86L37,81.1l-5.1,3.5L29.2,79           l-5.7,2.4l-1.4-6l-6.1,1.1l-0.2-6.2l-6.2-0.2l1.1-6.1l-6-1.4L7,56.8l-5.6-2.7L4.9,49L0,45.3L4.5,41l-4-4.7L5.8,33l-3-5.5l5.9-2.1            l-1.8-6l6.2-0.8l-0.5-6.2l6.2,0.5l0.8-6.2l6,1.8l2.1-5.9l5.5,3l3.2-5.3l4.7,4L45.3,0L49,4.9l5.1-3.5L56.8,7l5.7-2.4l1.4,6l6.1-1.1           l0.2,6.2l6.2,0.2L75.3,22l6,1.4L79,29.2l5.6,2.7L81.1,37l4.9,3.8L81.5,45l4,4.7L80.2,53l3,5.5l-5.9,2.1l1.8,6L73,67.3L73.4,73.4z"></path>
                              </svg>
                           </div>
                           <div class="pricing-header">
                              <h5>Standard</h5>
                              <div class="price"><span>$32.89</span><span>/month</span></div>
                           </div>
                           <div class="pricing-body">
                              <ul class="list">
                                 <li><span>Web Design</span></li>
                                 <li>2 Clients</li>
                                 <li><span class="icon icon-secondary-2 linear-icon-cross2"></span></li>
                                 <li>Copywriting</li>
                              </ul>
                           </div>
                           <div class="pricing-footer"><a class="button button-primary" href="#!#">Buy now!</a></div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-4 col-xl-3">
                     <div class="pricing-table-wrap">
                        <div class="pricing-table">
                           <div class="pricing-label">
                              <span></span>
                              <svg width="86px" height="86px" viewBox="0 0 86 86">
                                 <path d="M73.4,73.4L67.3,73l-0.8,6.2l-6-1.8l-2.1,5.9l-5.5-3l-3.2,5.3l-4.7-4L40.7,86L37,81.1l-5.1,3.5L29.2,79           l-5.7,2.4l-1.4-6l-6.1,1.1l-0.2-6.2l-6.2-0.2l1.1-6.1l-6-1.4L7,56.8l-5.6-2.7L4.9,49L0,45.3L4.5,41l-4-4.7L5.8,33l-3-5.5l5.9-2.1            l-1.8-6l6.2-0.8l-0.5-6.2l6.2,0.5l0.8-6.2l6,1.8l2.1-5.9l5.5,3l3.2-5.3l4.7,4L45.3,0L49,4.9l5.1-3.5L56.8,7l5.7-2.4l1.4,6l6.1-1.1           l0.2,6.2l6.2,0.2L75.3,22l6,1.4L79,29.2l5.6,2.7L81.1,37l4.9,3.8L81.5,45l4,4.7L80.2,53l3,5.5l-5.9,2.1l1.8,6L73,67.3L73.4,73.4z"></path>
                              </svg>
                           </div>
                           <div class="pricing-header">
                              <h5>Business</h5>
                              <div class="price"><span>$76.89 </span><span>/month</span></div>
                           </div>
                           <div class="pricing-body">
                              <ul class="list">
                                 <li><span>Graphic Design</span></li>
                                 <li>5 Clients</li>
                                 <li><span class="icon icon-primary linear-icon-check"></span></li>
                                 <li>Social Marketing</li>
                              </ul>
                           </div>
                           <div class="pricing-footer"><a class="button button-primary" href="#!#">Buy now!</a></div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-4 col-xl-3">
                     <div class="pricing-table-wrap">
                        <div class="pricing-table">
                           <div class="pricing-label">
                              <span></span>
                              <svg width="86px" height="86px" viewBox="0 0 86 86">
                                 <path d="M73.4,73.4L67.3,73l-0.8,6.2l-6-1.8l-2.1,5.9l-5.5-3l-3.2,5.3l-4.7-4L40.7,86L37,81.1l-5.1,3.5L29.2,79           l-5.7,2.4l-1.4-6l-6.1,1.1l-0.2-6.2l-6.2-0.2l1.1-6.1l-6-1.4L7,56.8l-5.6-2.7L4.9,49L0,45.3L4.5,41l-4-4.7L5.8,33l-3-5.5l5.9-2.1            l-1.8-6l6.2-0.8l-0.5-6.2l6.2,0.5l0.8-6.2l6,1.8l2.1-5.9l5.5,3l3.2-5.3l4.7,4L45.3,0L49,4.9l5.1-3.5L56.8,7l5.7-2.4l1.4,6l6.1-1.1           l0.2,6.2l6.2,0.2L75.3,22l6,1.4L79,29.2l5.6,2.7L81.1,37l4.9,3.8L81.5,45l4,4.7L80.2,53l3,5.5l-5.9,2.1l1.8,6L73,67.3L73.4,73.4z"></path>
                              </svg>
                           </div>
                           <div class="pricing-header">
                              <h5>Professional</h5>
                              <div class="price"><span>$89.89</span><span>/month</span></div>
                           </div>
                           <div class="pricing-body">
                              <ul class="list">
                                 <li><span>Branding</span></li>
                                 <li>8 Clients</li>
                                 <li><span class="icon icon-primary linear-icon-check"></span></li>
                                 <li>SEO</li>
                              </ul>
                           </div>
                           <div class="pricing-footer"><a class="button button-primary" href="#!#">Buy now!</a></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            </section> -->
      <!-- Call to Action-->
      <?php
	  include("includes/footer_one.php");
	  ?>
      <!-- Page Footer-->
      
   </div>
   <?php
	  include("includes/footer_two.php");
	  ?>
</body>
<!-- Google Tag Manager -->

</html>