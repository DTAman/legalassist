<?php
include('getAddress.php');

if(isset($_POST["txtPopUpName"]) && trim($_POST["txtPopUpName"])!=null && isset($_POST["txtPopUpEmail"]) && trim($_POST["txtPopUpEmail"])!=null && isset($_POST["txtPopUpMessage"]) && trim($_POST["txtPopUpMessage"])!=null &&  isset($_POST["txtPopUpNumber"]) && trim($_POST["txtPopUpNumber"])!=null)
{
	$mailSubject="Injury Assist Helpline: Contact Us PopUp Query";
	$mailBody='<p>Hello Admin, <br/>Following person has contacted you. His details are below:</p>
				<p>
					<strong>Name: </strong>'.$_POST['txtPopUpName'].'<br/>
					<strong>Email: </strong>'.$_POST['txtPopUpEmail'].'<br/>
					<strong>Message: </strong>'.$_POST['txtPopUpMessage'].'<br/>
					<strong>Mobile: </strong>'.$_POST['txtPopUpNumber'].'<br/>
					<strong>Info: </strong>'.getUserTimezone().'<br/>
				</p>
				<p>I agree to the Terms and Conditions and the Privacy Policy. <br/>I hereby authorize Injury Assist Helpline and other third party companies to contact me by phone, email and post.</p>';
       
	send_email($admins_email,$mailSubject, $mailBody);
	  
	echo 'Your message has been received. We will contact you very soon!';
}
else
{
	echo 'Please retry again by filling all the required fields.';
}

?>