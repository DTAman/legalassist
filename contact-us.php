<?php
include("includes/head.php");
?>

<body class="" contenteditable="false">
   <!-- Page-->
   <div class="page">
      <!-- Panel Thumbnail-->
      <!-- Template panel-->
      <div class="layout-panel-wrap">
         <div class="layout-panel">
         </div>
      </div>
      <?php
		include("includes/header.php");
	  ?>
	<?php
	  include("includes/footer_three.php");
	  ?>
      <section>
         <div class="row">
            <div class="col-lg-9">


               <section class="ls ms page_map">

                  <!-- .marker -->
                  <div class="marker">
                     <div class="marker-address">Level 23/127, Creek Street, Brisbane City, QLD 4000
                     </div>

                     <img class="marker-icon" src="images/map_marker_icon.png" alt="">
                  </div>
                  <div class="marker">
                     <div class="marker-address">Melbourne</div>
                     <img class="marker-icon" src="images/map_marker_icon.png" alt="">
                  </div>
                  <div class="marker">
                     <div class="marker-address"> New South Wales</div>
                     <img class="marker-icon" src="images/map_marker_icon.png" alt="">
                  </div>
                  <div class="marker">
                     <div class="marker-address"> Queensland</div>
                     <img class="marker-icon" src="images/map_marker_icon.png" alt="">
                  </div>
                  <div class="marker">
                     <div class="marker-address">South Australia</div>
                     <img class="marker-icon" src="images/map_marker_icon.png" alt="">
                  </div>

                  <div class="marker">
                     <div class="marker-address">Perth </div>
                     <img class="marker-icon" src="images/map_marker_icon.png" alt="">
                  </div>

                  <div class="marker">
                     <div class="marker-address">Sydney, New South Wales</div>
                     <img class="marker-icon" src="images/map_marker_icon.png" alt="">
                  </div>
                  <!-- .marker -->
               </section>
            </div>

            <div class="col-lg-3 animate" data-animation="fadeInDown">
               <div style="    background-color: #dee8f6; padding: 22px; border-radius: 5px;"
                  class="media box-shadow mt-4">
                  <div class="media-body">
                     <div class="divider-20"></div>
                    <!--
					 <span class="icon">
                        <i class="fa fa-map-marker mr-2"></i>
                     </span>
                     <span class="mr-3">Level 23/127, Creek Street, Brisbane City, QLD 4000</span><br />
					-->
                     <!--
					 
					 <span class="icon">
                        <ion-icon name="call"></ion-icon>
                     </span>
                     <span class="mr-3">0 (7) 304 075 37</span><br />
					 
					  <div class="divider-20" style="border-bottom: 1px solid #fff;margin:10px 0px "></div>-->
                     <span class="icon">
                        <ion-icon name="mail"></ion-icon>
                     </span>
                     <span class="mr-3">info@injuryassisthelpline.com</span><br />
                  </div>
               </div>
            </div>
         </div>
      </section>

      <!-- Call to Action-->
      <section class="section section-sm context-dark bg-gray-dark section-cta">
         <div class="container">
            <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
               <div class="col-xl-8 text-xl-left">
                  <h4><span class="font-weight-bold">Extrafast</span><span class="font-weight-normal">offers flexible
                        solutions with lots of advantages</span>
                  </h4>
               </div>
               <div class="col-xl-2 text-xl-right"><a class="button button-primary" target="_blank" data-toggle="modal"
                     data-target="#modalLogin">Get in touch</a></div>
            </div>
         </div>
      </section>
      <?php
	  include("includes/footer_four.php");
	  ?>
   </div>
    <?php
	  include("includes/footer_two.php");
	  ?>
</body>
<!-- Google Tag Manager -->

</html>