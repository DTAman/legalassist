<?php
include("includes/head.php");
?>

<body class="" contenteditable="false">
   <!-- Page-->
   <div class="page">
      <!-- Panel Thumbnail-->
      <!-- Template panel-->
      <div class="layout-panel-wrap">
         <div class="layout-panel">
         </div>
      </div>
      <?php
		include("includes/header.php");
	  ?>

      <!-- Swiper-->
      <!-- Service Detail -->
      <section class="detail pt-5">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <h4 class="heading-decorated">about us</h4>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-lg-8 order-1 order-lg-0 mt-4">
                  <div class="content">
                     <p>We believe that its not just about the physical injury stuff, but also the emotional and
                        physical repercussions on both the person who has been injured and the family and friends who
                        care for them.
                     <div class="col-12 col-lg-8">
                        <p><b>A no-fault injury can have an impact on a person's:</b></p>
                        <ol>
                           <li>Mental health</li>
                           <li>Personal relationships</li>
                           <li>General health</li>
                           <li>Lifestyle</li>
                           <li>Working life</li>
                        </ol>
                     </div>
                     <br>
                     <h4>Exposing the life-changing impact of accidental injuries</h4>
                     <p>As part of the campaign we commissioned a new comprehensive study that reveals the true extent
                        accidental injuries can have on an individual's life.
                     </p>
                     <p>The study surveyed over 5000 people as well carrying out in depth interviews with healthcare
                        professionals to explore the wider repercussions that can last a long time following a no-fault
                        accident.
                     </p>
                     <p>From the survey we learned that 8 out of 10 people who had had an accident said that they
                        suffered from a mental health issue as a direct result of it.
                     </p>
                     <p>More than a third admitted to suffering strains on their close relationships with family and
                        friends, and almost half said the accident had negatively impacted their job.
                     </p>
                     <p>This is why we're working to highlight the true extent of the impact which injuries from
                        no-fault accidents can have.
                     </p>
                     <p>We want to raise awareness and demonstrate that claiming for these injuries is not a selfish
                        act. We hope by doing so that those people who are suffering realise that claiming is justified
                        and necessary to help get their lives back on track.
                     </p>
                  </div>
               </div>
               <div class="col-12 col-lg-4 order-0 order-lg-1">
                  <div class="imgg4"></div>
               </div>
            </div>
         </div>
      </section>
      <!-- Call to Action-->
      <section class="section section-sm context-dark bg-gray-dark section-cta">
         <div class="container">
            <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
               <div class="col-xl-8 text-xl-left">
                  <h4><span class="font-weight-bold">Extrafast</span><span class="font-weight-normal">offers flexible
                        solutions with lots of advantages</span>
                  </h4>
               </div>
               <div class="col-xl-2 text-xl-right"><a class="button button-primary" target="_blank" data-toggle="modal"
                     data-target="#modalLogin">Get in touch</a></div>
            </div>
         </div>
      </section>
       <?php
	  include("includes/footer_one.php");
	  ?>
   </div>
    <?php
	  include("includes/footer_two.php");
	  ?>
</body>
<!-- Google Tag Manager -->

</html>