<?php
include("includes/head.php");
?>

<body class="" contenteditable="false">
   <!-- Page-->
   <div class="page">
      <!-- Panel Thumbnail-->
      <!-- Template panel-->
      <div class="layout-panel-wrap">
         <div class="layout-panel">
         </div>
      </div>
      <?php
		include("includes/header.php");
	  ?>

      <section class="detail">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6">
                  <div class="contentee">
                     <h4 class="heading-decorated">Motor Vehicle Accidents</h4>
                     <p>If you've suffered an injury because of a Motor Vehicle Accident, we're here to help you get the
                        compensation you deserve
                     </p>
                     <a class="button button-primary mt-4" href="contact-us.php">Contact us</a>
                  </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="sImage">
                     <img src="images/s1.png" alt="" class="img-fluid">
                  </div>
               </div>
            </div>
         </div>
         <!-- <div class="bg-white"> -->
         <!-- <div class="container"> -->
         <!-- <div class="row"> -->
         <!-- <div class="col-12 col-lg-6"> -->
         <!-- <p><b>Claim Types – By the nature of the accident</b></p> -->
         <!-- <ol> -->
         <!-- <li> -->
         <!-- <p>Medical expense claims</p> -->
         <!-- </li> -->
         <!-- <li> -->
         <!-- <p>General damage claims</p> -->
         <!-- </li> -->
         <!-- <li> -->
         <!-- <p>Economic Loss Claims</p> -->
         <!-- </li> -->
         <!-- <li> -->
         <!-- <p>Care and Assistance Claims</p> -->
         <!-- </li> -->
         <!-- </ol> -->
         <!-- </div> -->
         <!-- <br> -->
         <!-- <div class="col-12 col-lg-6"> -->
         <!-- <p><b>Accidents can be divided into multiple categories:</b></p> -->
         <!-- <ol> -->
         <!-- <li> -->
         <!-- <p>Work Accidents</p> -->
         <!-- </li> -->
         <!-- <li> -->
         <!-- <p>Road Traffic Accidents</p> -->
         <!-- </li> -->
         <!-- <li> -->
         <!-- <p>Trip or Fall Accidents</p> -->
         <!-- </li> -->
         <!-- <li> -->
         <!-- <p>Public Place Accidents</p> -->
         <!-- </li> -->
         <!-- <li> -->
         <!-- <p>General Accidents</p> -->
         <!-- </li> -->
         <!-- </ol> -->
         <!-- </div> -->
         <!-- </div> -->
         <!-- </div> -->
         <!-- </div> -->
         <div class="container">
            <div class="row">
               <div class="col-12 mt-4">
                  <div class="content">
                     <h3>Understanding the impact of your road traffic accident and how to make a claim</h3>
                     <p>We know that the impact of a road traffic accident on your life can be considerable. Aside from
                        the physical pain of your injury, the psychological effect of being injured while walking down
                        the street, cycling to work or travelling as a passenger is often traumatic.
                        Add that to an injury that takes time to heal, may be life-changing and permanent, or may cause
                        further health complications, and the effect of your road traffic accident is devastating.
                        We can't take away what you've been through, but we can guide you through the first steps of
                        making a claim so you can get the support and finances you need for your recovery.
                        When you contact us about your injury and accident, we won't just consider the severity of your
                        injury - though we recognise this is extremely important to you - we'll also consider the full
                        impact of your injury on your life and your family.
                        That means we'll take the time to listen to what you say and make sure we fully understand how
                        your injury has affected you, your loved ones, your future, your work life and your spare time.
                        Where there's been change to your normal routine, whether temporarily or permanently, we'll look
                        to recover compensation for that change.
                     </p>
                     <h3>Can you make a MVA claim?</h3>
                     <p>To be able to make a claim with us and our expert solicitors, your road accident must have:</p>
                     <ol>
                        <li>Happened within the last three years</li>
                        <li>Been somebody else's fault</li>
                        <li>Caused injury to you</li>
                     </ol>
                     <p>While there are some circumstances where the time you have to start your claim can be longer
                        (for example, if you or a loved one has suffered a head or brain injury) we're generally only
                        able to help if your accident happened within the last three years.
                     </p>
                     <p>If you're feeling unsure, you get in touch with us for free, impartial advise. We'll be happy to
                        answer all your questions and can let you know whether we think you can make a claim.
                     </p>
                     <p>As part of our promise to you, we'll never rush or pressure you into making a claim. But if you
                        do decide you'd like to go ahead, we can pass you on to one of our specialist road accident
                        solicitors.
                     </p>
                     <h3>Common types of road accidents</h3>
                     Nearly all road traffic accidents fall into the following categories:
                     <ol>
                        <li>Pedestrian accidents</li>
                        <li>Cycling accidents</li>
                        <li>Motorcycle accidents</li>
                        <li>Passenger accidents</li>
                        <li>Car accidents</li>
                     </ol>
                     <p>However, your experience may not fall into any of these claim types. If this applies to you then
                        please don't worry, it's likely we can still help you.
                     </p>
                     <h3>What's considered in your compensation</h3>
                     <p>As each claim is completely unique to the person involved in the accident, what's considered and
                        makes up your compensation may vary.
                        To give you an idea, typically our solicitors look to recover compensation for any impact your
                        road accident and injury has had on your life. This could include:
                     </p>
                     <ol>
                        <li>Your time off work and loss of earnings</li>
                        <li>Your future loss of income and effects on your career prospects</li>
                        <li>Any changes to your ability to work</li>
                        <li>The care or support you've received, even if this has been given for free from family or
                           friends
                        </li>
                        <li>Any adaptations you might need</li>
                        <li>Any travel costs you've had to cover</li>
                     </ol>
                     <p>Because there are so many factors considered when your expert solicitor negotiates your
                        compensation, we're not able to tell you how much you could receive before you begin your claim.
                     </p>
                     <h3>Are you entitled to early compensation payments?</h3>
                     <p>You may be entitled to early compensation payments following your road traffic accident if your
                        accident and injury are particularly serious.
                        Early compensation payments, also known as interim or immediate needs payments, are given to
                        people who need financial support sooner than their compensation claim will be settled.
                     </p>
                     </p>This could be for several reasons, but is typically because the immediate costs of your injury
                     can't be covered by your current financial situation.
                     For example, this may include the cost of housing your family somewhere near your hospital or
                     rehabilitation centre, or it may be covering the cost your rent, mortgage or bills because you're
                     on reduced or statutory sick pay.</p>
                     </p>Whatever your immediate needs, if you're not able to cover the costs because of your injury,
                     your solicitor can negotiate to secure you a quicker compensation payment while they continue to
                     negotiate for your final settlement.</p>
                     <h3>We can help with your recovery and rehabilitation</h3>
                     <p>Once we've connected you to a specialist solicitor, we'll invite you to attend a free medical
                        assessment. This is so that we can assess your injury to see what extra care, rehabilitation and
                        support you might need to get you back on your feet.
                        Our solicitors work with a wide range of rehabilitation providers so that they can provide you
                        with the support you need.
                        So, whether your road traffic accident has left you needing physiotherapy to regain mobility,
                        counselling to help you feel safe and adjusted, or an occupational therapist to assess your
                        needs, we're here to help you make it right.
                     </p>
                  </div>
               </div>
               <div class="col-12">
                  <img src="" alt="">
               </div>
            </div>
         </div>
      </section>
      <!-- Call to Action-->
      <section class="section section-sm context-dark bg-gray-dark section-cta">
         <div class="container">
            <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
               <div class="col-xl-8 text-xl-left">
                  <h4><span class="font-weight-bold">Extrafast</span><span class="font-weight-normal">offers flexible
                        solutions with lots of advantages</span>
                  </h4>
               </div>
               <div class="col-xl-2 text-xl-right"><a class="button button-primary" target="_blank" data-toggle="modal"
                     data-target="#modalLogin">Get in touch</a></div>
            </div>
         </div>
      </section>
        <?php
	  include("includes/footer_one.php");
	  ?>
   </div>
    <?php
	  include("includes/footer_two.php");
	  ?>
</body>
<!-- Google Tag Manager -->

</html>