<?php

require 'PHPMailer-master/src/Exception.php';
require 'PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/src/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// $admins_email = 'info@thelegalassist.com.au';
$admins_email = 'documents@injuryassisthelpline.com';

function send_email($mailEmail, $mailSubject, $mailBody)
{
	// Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);
	
	global $admins_email;
	
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                       // Enable verbose debug output
		$mail->isSMTP();                                            // Set mailer to use SMTP
		$mail->Host = 'smtp-relay.gmail.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
		$mail->Username = $admins_email;
		$mail->Password = "Docs@Injury#0421";                       // SMTP password
		$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
		$mail->Port       = 587;                                    // TCP port to connect to
        /*$mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );*/
        
        
		//Recipients
		$mail->setFrom($admins_email, 'Injury Assist');
		$mail->addAddress($mailEmail, 'Injury Assist');     // Add a recipient
		$mail->addCC('darpanjain6@gmail.com');
		//$mail->addCC('dt.amanjotkaur@gmail.com');
		 
		// Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = $mailSubject;
		$mail->Body    = $mailBody;

		$mail->send();
	} catch (Exception $e) {
		echo "Message could not be sent now. Mailer Error: {$mail->ErrorInfo}";
	}
}

function getUserTimezone_old()
{
	$ipaddress = '';
	if($ipaddress=="")
	{
		$ipaddress = getenv('REMOTE_ADDR');
	}
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	{
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
		$ipaddress='';
	}
	
	$ch = curl_init();
	$headers = array(
		'Accept: application/json',
		'Content-Type: application/json',
	);
   
   if(trim($ipaddress)!=null)
   {
		curl_setopt($ch, CURLOPT_URL, 'https://api.ipgeolocation.io/ipgeo?apiKey=67ab11f5414e4ad099374ee732b8fa6b&ip='.$ipaddress);
   }
   else
   {
		curl_setopt($ch, CURLOPT_URL, 'https://api.ipgeolocation.io/ipgeo?apiKey=67ab11f5414e4ad099374ee732b8fa6b');
   }
   
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);

	$authToken = curl_exec($ch);
	// $authToken = json_decode($authToken);
	
	return "<pre>".$authToken."</pre>";
}


function getUserTimezone()
{
	$ipaddress = '';
	if($ipaddress=="")
	{
		$ipaddress = getenv('REMOTE_ADDR');
	}
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	{
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
		$ipaddress='';
	}
	
	$headers = array(
		'Accept: application/json',
		'Content-Type: application/json',
	);
   
   $flag=0;
   // $ipaddress='49.36.231.135';  //For Testing
   //API does not work on Local
   
   if(trim($ipaddress)!=null)
   {
	    $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.whatismyip.com/ip-address-lookup.php?key=ba8864e2dcae4e037a4dac9ff1d80135&input='.$ipaddress.'&output=json');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$authToken = curl_exec($ch);
		curl_close($ch);
		
		$jsson = json_decode($authToken);
		
		$str='';
		// $jsson=json_decode('{"ip_address_lookup": [{"status":"ok","ip":"122.160.133.141","country":"IN","region":"Delhi","city":"Delhi","postalcode":"110008","isp":"Bharti Airtel Ltd.","time":"+05:30","latitude":"28.66667","longitude":"77.216667"}]}');
		if(isset($jsson->ip_address_lookup[0]->status) && $jsson->ip_address_lookup[0]->status=='ok')
		{
			$str.='IP: '.$ipaddress.'<br/>';
			$str.='Country: '.$jsson->ip_address_lookup[0]->country.'<br/>';
			$str.='Region: '.$jsson->ip_address_lookup[0]->region.'<br/>';
			$str.='City: '.$jsson->ip_address_lookup[0]->city.'<br/>';
			$str.='Latitude: '.$jsson->ip_address_lookup[0]->latitude.'<br/>';
			$str.='Longitude: '.$jsson->ip_address_lookup[0]->longitude.'<br/>';
			
			$flag=1;
			return "<p>".$str."</p>";
		}
   }

   if($flag==0)
   {
	    $ch = curl_init();
		if(trim($ipaddress)!=null)
		{
			curl_setopt($ch, CURLOPT_URL, 'https://api.ipgeolocation.io/ipgeo?apiKey=67ab11f5414e4ad099374ee732b8fa6b&ip='.$ipaddress);
		}
		else
		{
			curl_setopt($ch, CURLOPT_URL, 'https://api.ipgeolocation.io/ipgeo?apiKey=67ab11f5414e4ad099374ee732b8fa6b');
		}
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$authToken = curl_exec($ch);
		curl_close($ch);
		return "<pre>".$authToken."</pre>";
   }
}
?>