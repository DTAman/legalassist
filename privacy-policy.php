<?php
include("includes/head.php");
?>

<body class="" contenteditable="false">
   <!-- Page-->
   <div class="page">
      <!-- Panel Thumbnail-->
      <!-- Template panel-->
      <div class="layout-panel-wrap">
         <div class="layout-panel">
         </div>
      </div>
      <?php
		include("includes/header.php");
	  ?>

        <section class="detail pt-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h4 class="heading-decorated">Privacy Policy</h4>
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-12 col-lg-8 order-1 order-lg-0 mt-4"> -->
                    <div class="col-12 col-lg-12 order-1 order-lg-0 mt-4">
                        <div class="content">
                            <p>In this Privacy Policy, we, us or our means Injury Assist (“IA”).<br />
                                This privacy policy sets out our commitment to protecting the privacy of personal
                                information provided to us, or otherwise collected by us, offline or online, including
                                through our website.
                                <br />
                                Our website address is: <a
                                    href="https://www.injuryassisthelpline.com">https://www.injuryassisthelpline.com</a>
                                <br />
                                All of our contact details can be found on the contact page of website:
                                <a
                                    href="https://www.injuryassisthelpline.com/contact-us.php">https://www.injuryassisthelpline.com/contact-us.php</a>
                            </p>

                            <br>

                            <h4> 1. ABOUT US </h4>
                            <p>
                                We are in the business of providing accident management services to motorists involved
                                in not at fault motor vehicle accidents.
                            </p>

                            <H4>2. YOUR PRIVACY IS IMPORTANT TO US</H4>
                            <P> We understand that your privacy is important to you, and we value your trust. That’s why
                                we protect your personal information and aim to be clear and open about what we do with
                                it. This policy explains how we handle your personal information.
                            </P>
                            <H4>3. WHAT INFORMATION WE COLLECT</H4>

                            <p> We collect personal information about you when you contact us, use our services, visit
                                our website or deal with us in some other way. <br /> The personal information we
                                collect from you may include:</p>
                            <ol>
                                <li><strong> Information about your identity </strong> – including your name, contact
                                    details (including telephone number, address and email address), date of birth and
                                    gender</li>
                                <li><strong> Information about your motor vehicle</strong> – such as the make, model,
                                    year of manufacture and registration number of your motor vehicle</li>
                                <li><strong> Information about the motor vehicle accident</strong> – such as the date,
                                    time and description of the accident, third party details, witness details and
                                    police details</li>
                                <li><strong> When you visit our website</strong> – your location information, IP
                                    address, mobile device and network information and any third-party sites you access
                                </li>

                            </ol>
                            <br />
                            <H4>4. HOW DO WE USE YOUR INFORMATION? </H4>

                            <p>We collect, hold, use and disclose your personal information so we can:</p>
                            <ol>
                                <li>Confirm your identity</li>
                                <li>Assess your application</li>
                                <li>Manage your claim</li>
                                <li>Contact and communicate with you</li>
                                <li>Provide services ancillary to your claim (e.g. car replacement hire, vehicle repair,
                                    insurance products and legal services) </li>
                                <li>Manage our relationship with you</li>
                                <li>Improve our service to you and your experience with us</li>
                                <li>Comply with laws, and assist government or law enforcement agencies</li>

                            </ol>

                            <p>We may also collect, hold, use and disclose your information for other reasons where the
                                law allows or requires us. </p>
                            <p>From time to time, we may also use your personal information to tell you about products
                                or services we think you might be interested in. To do this, we may contact you by: </p>
                            <ol>
                                <li>Email</li>
                                <li>Phone</li>
                                <li>SMS</li>
                                <li>Social media</li>
                                <li>Advertising through our website or third-party websites</li>
                                <li>Mail</li>

                            </ol>

                            <p>If you don’t want to receive direct marketing messages or want to change your contact
                                preference, then please contact us. </p>

                            <H4>5. WHO DO WE SHARE YOUR INFORMATION WITH? </H4>

                            <p>We may share your information with third parties for the reasons in section 4 (How do we
                                use your information?) or where the law otherwise allows. These third parties can
                                include:</p>
                            <li>Our employees, contractors and/or related entities</li>
                            <li>Accident replacement car hire companies for the purpose of hiring a replacement vehicle
                                to you</li>
                            <li>Tow truck companies for the purpose of towing your vehicle</li>
                            <li>Motor vehicle fleet companies for the purpose of hiring a replacement vehicle to you
                            </li>
                            <li>Motor vehicle repairers for the purpose of repairing your vehicle</li>
                            <li>Insurers, insurance brokers, assessors or investigators for the purpose of recovering
                                your loss and damage arising from the motor vehicle accident</li>
                            <li>Police in relation to the motor vehicle accident</li>
                            <li>Legal service providers or mercantile recovery agents for the purpose of recovering your
                                loss and damage, including any claim for personal injury</li>
                            <li>Business who do some of our work for us, including direct marketing, debt recovery and
                                IT support</li>



                            <p><strong>Sending information overseas</strong><br />Sometimes, we send your information
                                overseas, including to service providers who operate outside of Australia. If we do
                                this, we make sure there are arrangements in place to protect your personal information.
                            </p>
                            <H4>6. KEEPING YOUR INFORMATION SAFE</H4>
                            <p> We store your hard copy and electronic records in secure buildings and systems or using
                                trusted third parties.
                            </p>
                            <H4>7. ACCESSING, UPDATING AND CORRECTING YOUR INFORMATION</H4>
                            <P> You can contact us and ask to view your information. If your information isn’t correct
                                or needs updating, let us know straight away.
                            </P>
                            <h6>CAN YOU SEE WHAT INFORMATION WE HAVE? </h6>

                            <P> You can ask us for a copy of your information by calling us.
                            </P>
                            <h6>IS THERE A FEE?</h6>
                            <P> There is no fee to ask for your information.
                            </P>
                            <h6>HOW LONG WILL IT TAKE? </h6>

                            <P> We try to make your information available within 90 days after you ask us for it. Before
                                we give you the information, we’ll need to confirm your identity.
                            </P>
                            <H4>8. MAKING A PRIVACY COMPLAINT</H4>
                            <P> If you are concerned about your privacy, you can make a complaint and we’ll do our best
                                to sort it out.

                            </P>
                            <P> To make a complaint contact one of our staff or customer service representatives. We’ll
                                look into the issue and try to fix it straight away.
                            </P>
                            <H4>9. HOW DO WE MANAGE COMPLAINTS? </H4>
                            <p> We will:</p>
                            <OL>
                                <li>Keep a record of your complaint</li>
                                <li>Give you a reference number, along with a staff member’s name and contact details if
                                    you want to follow it up</li>
                                <li>Respond to the complaint within a few days if we can, or tell you if we need more
                                    time to investigate it</li>
                                <li>Keep you updated on what we’re doing to fix the problem</li>
                                <li>Give our final response within 120 days. If we can’t give you a response in this
                                    time, we’ll get in touch to tell you why and work out a new time-frame with you.
                                </li>
                            </OL>
                            <br />
                            <h6> WHAT ELSE CAN YOU DO? </h6>
                            <p> If your complaint is about how we handle your personal information, you can contact the
                                Office of the Australian Information Commissioner.
                                <br/> <strong> Office of the Australian Information Commissioner </strong>
                                <br/>
                                GPO Box 5218, Sydney NSW 2001<br />
                                Phone 1300 363 992<br />
                                Visit oaic.gov.au
                            </p>
                            <h4>10. HOW TO CONTACT US
                            </h4>
                            <P> You can contact us by phone on 0 (7) 304 075 37 or email at
                                info@injuryassisthelpline.com <br /><strong>WE MAY CHANGE THIS PRIVACY POLICY FROM TIME TO TIME
                                BUT IT WILL ALWAYS BE AVAILABLE HERE ON OUR WEBSITE.</strong> 
                            </p>




                        </div>
                    </div>
                    <!--
					<div class="col-12 col-lg-4 order-0 order-lg-1">
                        <div class="imgg4"></div>
                    </div>
					-->
                </div>
            </div>
        </section>
          <?php
	  include("includes/footer_one.php");
	  ?>
   </div>
    <?php
	  include("includes/footer_two.php");
	  ?>
</body>
<!-- Google Tag Manager -->

</html>