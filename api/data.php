<?php
require_once("RestSecure.php");
class AuthenticateAPI extends RestSecure 
{
	public $data = "";
    public $db1 = NULL;
	public $log;

	public function processApi() 
	{
        $func = strtolower(trim(str_replace("/", "", $_REQUEST['request'])));
        if((int) method_exists($this, $func) > 0)
		{
			$this->$func();
		}
        else
		{
			$this->response('', 404);    // If the method not exist with in this class, response would be "Page not found".
		}
    }
	
	public function __construct() 
	{
        parent::__construct('0');    // Init parent contructor
        $this->dbConnect();     // Initiate Database connection
    }
	
	private function save_lead() 
	{
        if ($this->get_request_method() != "POST") {
            $this->response('', 404);
        }

		$message='';
		$result=new ArrayObject();
		$user_id = $this->_userId;
		
		$thisdate = $this->getCurrentDatetTime();
		$callcenter_id = $user_id;
		$status='AC';
		$r = $last_accident_na = $reached_admin = $police_attended = 0;
		$claim_accident = 1;
		$master_lead_id = $dob = $state_name = $state_id = $suburb_id = $suburb_name = $make = $model =  $registration =  $insurance_company_name =  $insurance_reference_number =  $insurance_claim_number =  $vehicle_other_info = $driver_name = $driver_address = $dl_number = $make_d = $model_d =  $registration_d =  $insurance_company_name_d = $insurance_reference_number_d = $insurance_claim_number_d = $vehicle_other_info_d = $accident_address = $accident_circumstances = $police_event_number = $accident_state =  $accident_state_id = $accident_suburb_id = $accident_suburb_name = $witness_name = $witness_phone = $witness_state_id = $witness_suburb_id = $witness_postal_code = $witness_address = null;
		
		try
        {
			if(isset($this->_request['first_name']) && trim($this->_request['first_name'])!=null && isset($this->_request['accident_type_id']) && trim($this->_request['accident_type_id'])!=null && isset($this->_request['phone']) && trim($this->_request['phone'])!=null && isset($this->_request['latest_accident_dt']) && trim($this->_request['latest_accident_dt'])!=null && $this->_request['accident_type_id']>0 && $this->_request['accident_type_id']<5 && isset($this->_request['has_witness']) && trim($this->_request['has_witness'])!=null)
			{
				$first_name = isset($this->_request['first_name']) ? $this->_request['first_name']:"";	
				$middle_name = isset($this->_request['middle_name']) ? $this->_request['middle_name']:"";	
				$last_name = isset($this->_request['last_name']) ? $this->_request['last_name']:"";	
				$phone = isset($this->_request['phone']) ? $this->_request['phone']:"";	
				$email = isset($this->_request['email']) ? $this->_request['email']:"";	
				$relative_name = isset($this->_request['relative_name']) ? $this->_request['relative_name']:"";	
				$relative_number = isset($this->_request['relative_number']) ? $this->_request['relative_number']:"";	
				$last_accident_type_id = isset($this->_request['accident_type_id']) ? $this->_request['accident_type_id']:0;	
				$is_driver = isset($this->_request['is_driver']) ? $this->_request['is_driver']:0;	
				$postal_code = isset($this->_request['postal_code']) ? $this->_request['postal_code']:"";	
				$address = isset($this->_request['address']) ? $this->_request['address']:"";	
				$state_name = isset($this->_request['state_name']) ? $this->_request['state_name']:"";	
				$suburb_name = isset($this->_request['suburb_name']) ? $this->_request['suburb_name']:"";	
				$has_witness = isset($this->_request['has_witness']) ? $this->_request['has_witness']:0;	
				$accident_details = isset($this->_request['accident_details']) ? $this->_request['accident_details']:"";	
				$driver_details = isset($this->_request['driver_details']) ? $this->_request['driver_details']:"";	
				$vehicle_details = isset($this->_request['vehicle_details']) ? $this->_request['vehicle_details']:"";	
				$witness_details = isset($this->_request['witness_details']) ? $this->_request['witness_details']:"";	
				
				if(isset($this->_request['accident_details'])){
					$accident_address = isset($accident_details['accident_address']) ? $accident_details['accident_address']:"";	
					$accident_circumstances = isset($accident_details['accident_circumstances']) ? $accident_details['accident_circumstances']:"";	
					$police_event_number = isset($accident_details['police_event_number']) ? $accident_details['police_event_number']:"";	
					$accident_state = isset($accident_details['accident_state']) ? $accident_details['accident_state']:"";	
					$accident_suburb_name = isset($accident_details['accident_suburb_name']) ? $accident_details['accident_suburb_name']:"";	
				}
				
				if($has_witness==1 && isset($this->_request['witness_details']))
				{
					$witness_name = isset($witness_details['witness_name']) ? $witness_details['witness_name']:"";	
					$witness_phone = isset($witness_details['witness_phone']) ? $witness_details['witness_phone']:"";	
					$witness_state = isset($witness_details['witness_state']) ? $witness_details['witness_state']:"";	
					$witness_suburb = isset($witness_details['witness_suburb']) ? $witness_details['witness_suburb']:"";	
					$witness_postal_code = isset($witness_details['witness_postal_code']) ? $witness_details['witness_postal_code']:"";	
					$witness_address = isset($witness_details['witness_address']) ? $witness_details['witness_address']:"";	
				}
				
				
				if($is_driver==0 && isset($this->_request['vehicle_details']) )
				{
					$make = isset($vehicle_details['make']) ? $vehicle_details['make']:"";	
					$model = isset($vehicle_details['model']) ? $vehicle_details['model']:"";	
					$registration = isset($vehicle_details['registration']) ? $vehicle_details['registration']:"";	
					$insurance_company_name = isset($vehicle_details['insurance_company_name']) ? $vehicle_details['insurance_company_name']:"";	
					$insurance_reference_number = isset($vehicle_details['insurance_reference_number']) ? $vehicle_details['insurance_reference_number']:"";	
					$insurance_claim_number = isset($vehicle_details['insurance_claim_number']) ? $vehicle_details['insurance_claim_number']:"";	
					$vehicle_other_info = isset($vehicle_details['vehicle_other_info']) ? $vehicle_details['vehicle_other_info']:"";	
				}
				else
				{
					if(isset($this->_request['driver_details'])){
						$driver_name = isset($driver_details['driver_name']) ? $driver_details['driver_name']:"";	
						$driver_address = isset($driver_details['driver_address']) ? $driver_details['driver_address']:"";	
						$dl_number = isset($driver_details['dl_number']) ? $driver_details['dl_number']:"";	
					}
					if(isset($this->_request['vehicle_details'])){
						$make_d = isset($vehicle_details['make']) ? $vehicle_details['make']:"";	
						$model_d = isset($vehicle_details['model']) ? $vehicle_details['model']:"";	
						$registration_d = isset($vehicle_details['registration']) ? $vehicle_details['registration']:"";	
						$insurance_company_name_d = isset($vehicle_details['insurance_company_name']) ? $vehicle_details['insurance_company_name']:"";	
						$insurance_reference_number_d = isset($vehicle_details['insurance_reference_number']) ? $vehicle_details['insurance_reference_number']:"";	
						$insurance_claim_number_d = isset($vehicle_details['insurance_claim_number']) ? $vehicle_details['insurance_claim_number']:"";	
						$vehicle_other_info_d = isset($vehicle_details['vehicle_other_info']) ? $vehicle_details['vehicle_other_info']:"";	
					}
				}
				
				switch($last_accident_type_id)
				{
					case 1: $last_accident_type = "Motor Vehicle Accident";break;
					case 2: $last_accident_type = "Occupiers Liability";break;
					case 3: $last_accident_type = "Public Liability";break;
					case 4: $last_accident_type = "Accident At Work";break;
					default: $last_accident_type = "";
				}
				
				$police_attended = trim($police_event_number)!=null?1:0;
				$state_id = $this->getStateId($state_name);
				$accident_state_id = $this->getStateId($accident_state);
				$witness_state_id = $this->getStateId($witness_state);
				
				if(trim($state_id)!=null)
				{
					$suburb_id = $this->getSubUrbId($state_id, $suburb_name);
					if(trim($suburb_id)!=null)
					{
						$suburb_name = null;
					}
				}
				
				if(trim($accident_state_id)!=null)
				{
					$accident_suburb_id = $this->getSubUrbId($accident_state_id, $accident_suburb_name);
					if(trim($accident_suburb_id)!=null)
					{
						$accident_suburb_name = null;
					}
				}
				
				if(trim($witness_state_id)!=null)
				{
					$witness_suburb_id = $this->getSubUrbId($witness_state_id, $witness_suburb);
				}
				
				if(trim($this->_request['dob'])!=null)
				{
					$dob = date("Y-m-d",strtotime($this->_request['dob']));
				}
				
				if(trim($this->_request['latest_accident_dt'])!=null)
				{
					$latest_accident_dt = date("Y-m-d H:i:s",strtotime($this->_request['latest_accident_dt']));
				}
				
				//Save to leads table
				$datamaster = $this->mysqli->prepare("INSERT INTO leads(first_name, middle_name, last_name, phone, email, relative_name, relative_number, dob, state_id, suburb_id, suburb_name, postal_code, address, added_by, status, creation_date, updation_date, callcenter_id, master_lead_id, last_accident_type, last_accident_na, latest_accident_dt, reached_admin, assigned_usertype, outcome,outcome_type,assign_n_callcenter,last_comment,latest_comment_usertype_from,latest_comment_usertype_to) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'CCA',(select outcome_title from outcomes where outcome_initials = 'ACL'),'ACL',?,(select outcome_title from outcomes where outcome_initials = 'ACL'),'CCA','CCA')");
				if(!$datamaster)
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				if(!$datamaster->bind_param("ssssssssiisssisssiisisis",$first_name,$middle_name,$last_name, $phone,$email,$relative_name,$relative_number,$dob,$state_id, $suburb_id, $suburb_name,$postal_code,$address, $user_id,$status, $thisdate,$thisdate,$user_id,$master_lead_id,$last_accident_type, $last_accident_na, $latest_accident_dt, $reached_admin,$thisdate))
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				if(!$datamaster->execute())
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				$datamaster->close();
				
				//Get the lead ID
				$copy_lead_id = mysqli_insert_id($this->mysqli);
				
				//Save to comments table
				$datamaster = $this->mysqli->prepare("INSERT INTO lead_comments(lead_id, comment_to, comment_utype, comment_from, comments, status, creation_date, lead_transfer_type) VALUES (?,?,'CCA',?,(select outcome_title from outcomes where outcome_initials = 'ACL'),'PN',?,'LT')");
				if(!$datamaster)
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				if(!$datamaster->bind_param("iiis",$copy_lead_id,$user_id,$user_id,$thisdate))
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				if(!$datamaster->execute())
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				$datamaster->close();
				
				//Save to Lead Accidents table
				$datamaster = $this->mysqli->prepare("insert into  lead_accidents(lead_id, accident_date_time, accident_kind, claim_accident,is_na, is_driver, state_id, suburb_id, suburb_name, address,accident_circumstances,police_attended,event_number) values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
				if(!$datamaster)
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				if(!$datamaster->bind_param("isiiiiiisssis",$copy_lead_id,$latest_accident_dt,$last_accident_type_id, $claim_accident,$last_accident_na, $is_driver, $accident_state_id, $accident_suburb_id, $accident_suburb_name, $accident_address, $accident_circumstances, $police_attended, $police_event_number))
				{					
					throw new Exception(mysqli_error($this->mysqli));
				}
				if(!$datamaster->execute())
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				$datamaster->close();
				
				//Get the Accident ID
				$accident_id = mysqli_insert_id($this->mysqli);
				
				//Save to Lead Vehicle table
				$datamaster = $this->mysqli->prepare("INSERT INTO lead_vehicles(lead_id, make, model, registration, insurance_company, insurance_reference_no, insurance_claim_number, other_info, driver_name, driver_address, dl_number,make_d, model_d, registration_d, insurance_company_d, insurance_reference_no_d, insurance_claim_number_d, other_info_d,lead_accident_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				if(!$datamaster)
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				if(!$datamaster->bind_param("isssssssssssssssssi",$copy_lead_id, $make,$model, $registration, $insurance_company_name, $insurance_reference_number, $insurance_claim_number, $vehicle_other_info, $driver_name, $driver_address, $dl_number, $make_d,$model_d, $registration_d, $insurance_company_name_d, $insurance_reference_number_d, $insurance_claim_number_d, $vehicle_other_info_d, $accident_id))
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				if(!$datamaster->execute())
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				$datamaster->close();
				
				//Save to Lead Witness table
				$datamaster = $this->mysqli->prepare("INSERT INTO lead_witness(lead_id, lead_accident_id, name, phone, state_id, suburb_id, postal_code, address) values(?,?,?,?,?,?,?,?)");
				if(!$datamaster)
				{
					
					throw new Exception(mysqli_error($this->mysqli));
				}
				if(!$datamaster->bind_param("iissiiss",$copy_lead_id,$accident_id,$witness_name, $witness_phone, $witness_state_id, $witness_suburb_id, $witness_postal_code, $witness_address))
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				if(!$datamaster->execute())
				{
					throw new Exception(mysqli_error($this->mysqli));
				}
				$datamaster->close();
				if(trim($message)==null)
				{
					$message = self::Lead_Saved;
					$r=1;
				}
			}
			else
			{
				$message=self::Required_Fields_Empty;
				$r=0;
			}
			
			$this->sendJson($r,$message,$result);
		}
		catch(Exception $e)
        {
			$this->log->error($this->userErrorHandler('save_lead',$e->getTraceAsString(),$e->getMessage(),$e->getFile(),$e->getLine()));
            $this->sendJson(-1,$e->getMessage(),null);
			// $this->exceptionJson();
        }
    }
}

$api = new AuthenticateAPI;
$api->processApi();
?>
