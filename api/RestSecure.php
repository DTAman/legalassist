<?php
require_once('constants.php');

class RestSecure extends myConstants {

    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    public $_userId;

    public function __construct() {
        $this->inputs();
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {

        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() 
	{
        $API_KEY = null;
        $API_SECRET = null;
		if (isset($_SERVER['API_KEY'])) 
		{
            $API_KEY = trim($_SERVER["API_KEY"]);
            $API_SECRET = trim($_SERVER["API_SECRET"]);
        }
        else if (isset($_SERVER['HTTP_API_KEY'])) 
		{
            $API_KEY = trim($_SERVER["HTTP_API_KEY"]);
            $API_SECRET = trim($_SERVER["HTTP_API_SECRET"]);
        } 
		else if (function_exists('apache_request_headers')) 
		{
            $requestHeaders = apache_request_headers();
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
			if (isset($requestHeaders['API_KEY'])) 
			{
                $API_KEY = trim($requestHeaders['API_KEY']);
            }
			if (isset($requestHeaders['API_SECRET'])) 
			{
                $API_SECRET = trim($requestHeaders['API_SECRET']);
            }
        }
		
		$rm = new ArrayObject();
		
        try 
		{  
			$this->dbConnect();

			if(trim($API_KEY)==null)
			{
				throw new Exception('API Key Not Valid.');
			}
			else if(trim($API_SECRET)==null)
			{
				throw new Exception('API Secret Not Valid.');
			}
			else if($this->verifyApiKey($API_KEY,$API_SECRET)==0)
			{
				throw new Exception('User is not authorized.');
			}
			else
			{
				$this->_userId = $this->getCallcenterId();
			}
			
            switch ($this->get_request_method()) 
			{
                case "POST":
                    if ($this->startsWith($_SERVER["CONTENT_TYPE"], 'application/json') == true)
                    {
                        $entityBody = file_get_contents('php://input');
	 
                        $this->_request = json_decode($entityBody, true);
                    } 
                    else
                    {
                        $this->_request = $this->cleanInputs($_POST);
                    }
                    break;
                case "GET":
                case "DELETE":
                    $this->_request = $this->cleanInputs($_GET);
                    break;
                case "PUT":
                    parse_str(file_get_contents("php://input"), $this->_request);
                    $this->_request = $this->cleanInputs($this->_request);
                    break;
                default:
                    $this->response('', 406);
                    break;
            }
        } 
		catch (Exception $exc) 
		{
            $rm['result_code'] = "FAILED";
            $rm['message'] = $exc->getMessage();
            $rm['result_data'] = new ArrayObject();  
			$this->response(json_encode($rm), 403);			
        } 
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
        header("Content-Type:" . $this->_content_type);
    }

}
?>

