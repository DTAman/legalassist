<?php
include('php/Logger.php');
Logger::configure('config.xml');
require 'PHPMailer/PHPMailerAutoload.php';

class myConstants
{
	const DB_SERVER = "127.0.0.1";
	const DB_USER = "root";
	const DB_PASSWORD ="";
	const DB = "legal_live";
	const BASE_URL = "http://127.0.0.1/legal/api/";
	
	const OK='OK';
	const FAILED='FAILED';
	const ERROR='ERROR';
	const Required_Fields_Empty='Required Fields Not Sent.';
	const Lead_Saved='Lead Saved Successfully.';
	
	function userErrorHandler($function,$error_details,$message,$filename,$linenum)
	{
		$err='';
		$timezone='IST';
		$output='';
		
		$err = "FUNCTION=\"".$function."\",";
		$err.= " ERR_MSG=\"".$message."\",";
		$err.= " ERR_TIME=\"".$this->getCurrentDatetTime()."\",";
		$err.= " FILENAME=\"".$filename."\",";
		$err.= " LINE=\"".$linenum."\",";
		$err.= " TIMEZONE=\"".$timezone."\",";
		$err.= " DETAILS=\"".str_replace("\n", ', ', $error_details)."\",";
		$err.= $output;
		$err.= "\n";
		
		return $err;
	}
	
    public function dbConnect() 
	{
		$this->mysqli = new mysqli(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD, self::DB);
		mysqli_set_charset($this->mysqli, "utf8");
		$this->log = Logger::getLogger(__CLASS__);
    }
	
	public function getCurrentDatetTime()
	{
		date_default_timezone_set('UTC');
		return date('Y-m-d H:i:s');
	}
	
	public function sendJson($r,$message,$data)
	{
		switch($r)
		{
			case 1: $r=self::OK;$code=200;
			break;
			case 0: $r=self::FAILED;$code=400;
			break;
			case -1: $r=self::ERROR;$code=500;
			break;
			default: $r=self::FAILED;$code=403;
		}
		$array = array("result_code" => strval($r),"message"=>$message,"result_data" => $data);
		$this->response($this->json($array), $code);
	}
	
	public function exceptionJson()
	{
		$array = array("result_code" =>"FAILED","message"=>"Something Went Wrong","result_data" => null);
		$this->response($this->json($array), 500);
	}
	
    public function json($data) 
	{
        if (is_array($data)) 
		{
            return json_encode($data);
        }
    }
	
	public function getStateId($initials)
	{
		$otpmaster = $this->mysqli->prepare("select id from states where initials = ?");
		$otpmaster->bind_param("s",$initials);
		$otpmaster->execute();
		$otpmaster->bind_result($id);
		$otpmaster->fetch();
		$otpmaster->close();
		
		return $id;
	}
	
	public function getSubUrbId($state_id, $suburb_name)
	{
		$otpmaster = $this->mysqli->prepare("select id from suburbs where suburb_name = ? and state_id = ?");
		$otpmaster->bind_param("si",$suburb_name, $state_id);
		$otpmaster->execute();
		$otpmaster->bind_result($id);
		$otpmaster->fetch();
		$otpmaster->close();
		
		return $id;
	}
	
	public function verifyApiKey($key, $secret)
	{
		$otpmaster = $this->mysqli->prepare("select count(*) from api_master where api_key=? and api_secret = ? and status = 'AC'");
		$otpmaster->bind_param("ss",$key, $secret);
		$otpmaster->execute();
		$otpmaster->bind_result($data_count);
		$otpmaster->fetch();
		$otpmaster->close();
		
		return $data_count;
	}
	
	public function getCallcenterId()
	{
		$otpmaster = $this->mysqli->prepare("select user_id from api_master where status = 'AC'");
		$otpmaster->execute();
		$otpmaster->bind_result($user_id);
		$otpmaster->fetch();
		$otpmaster->close();
		
		return $user_id;
	}
}
?>
