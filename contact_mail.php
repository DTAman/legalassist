<?php
include('getAddress.php');

if(isset($_POST["txtFirstName"]) && trim($_POST["txtFirstName"])!=null && isset($_POST["txtLastName"]) && trim($_POST["txtLastName"])!=null && isset($_POST["txtPhone"]) && trim($_POST["txtPhone"])!=null && isset($_POST["txtEmail"]) && trim($_POST["txtEmail"])!=null && isset($_POST["txtMessage"]) && trim($_POST["txtMessage"])!=null && isset($_POST["ddlState"]) && trim($_POST["ddlState"])!=null && isset($_POST["ddlHear"]) && trim($_POST["ddlHear"])!=null && isset($_POST["ddlClaim"]) && trim($_POST["ddlClaim"])!=null)
{
	$mailSubject="Injury Assist Helpline: Contact Us Query";
	/* $mailBody='<p>Hello Admin, <br/>Following person has contacted you. His details are below:</p>
				<p>
					<strong>Name: </strong>'.$_POST['txtFirstName'].' '.$_POST['txtLastName'].'<br/>
					<strong>Phone: </strong>'.$_POST['txtPhone'].'<br/>
					<strong>Email: </strong>'.$_POST['txtEmail'].'<br/>
					<strong>State: </strong>'.$_POST['ddlState'].'<br/>
					<strong>Hear From: </strong>'.$_POST['ddlHear'].'<br/>
					<strong>Claim For: </strong>'.$_POST['ddlClaim'].'<br/>
					<strong>Message: </strong>'.$_POST['txtMessage'].'<br/>
					<strong>Info: </strong>'.getUserTimezone().'<br/>
				</p>'; */
	
	  $mailBody='<p>Hello Admin, <br/>Following person has contacted you. His details are below:</p>
				<p>
					<strong>Name: </strong>'.$_POST['txtFirstName'].' '.$_POST['txtLastName'].'<br/>
					<strong>Phone: </strong>'.$_POST['txtPhone'].'<br/>
					<strong>Email: </strong>'.$_POST['txtEmail'].'<br/>
					<strong>Hear From: </strong>'.$_POST['ddlHear'].'<br/>
					<strong>Claim For: </strong>'.$_POST['ddlClaim'].'<br/>
					<strong>Message: </strong>'.$_POST['txtMessage'].'<br/>
					<strong>Info: </strong>'.getUserTimezone().'<br/>
				</p>
				<p>I agree to the Terms and Conditions and the Privacy Policy. <br/>I hereby authorize Injury Assist Helpline and other third party companies to contact me by phone, email and post.</p>';
       
	  send_email($admins_email,$mailSubject, $mailBody);
	  
	  echo 'Your message has been successfully sent. We will contact you very soon!';
}
else
{
	echo 'Please retry again by filling all the required fields.';
}

?>