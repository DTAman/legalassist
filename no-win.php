<?php
include("includes/head.php");
?>

<body class="" contenteditable="false">
   <!-- Page-->
   <div class="page">
      <!-- Panel Thumbnail-->
      <!-- Template panel-->
      <div class="layout-panel-wrap">
         <div class="layout-panel">
         </div>
      </div>
      <?php
		include("includes/header.php");
	  ?>

      <section class="detail pt-5">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <h4 class="heading-decorated">No Win No Fee - how does it work?</h4>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-lg-8 order-1 order-lg-0 mt-4">
                  <div class="content">
                     <p>No win no fee, also known as a cost agreement, is an agreement you make with your solicitor so
                        that you can claim compensation without worrying about upfront legal fees. If your compensation
                        claim is unsuccessful, the agreement states you won't have to pay your no win no fee lawyer any
                        money.
                        Because you don't have to pay anything if you lose your case, no win no fee arrangements take
                        the risk out of making an accident claim. You don't have to pay anything upfront, there are no
                        hidden charges and you'll never be left out of pocket.
                        No win no fee is subject to the solicitor firm agreeing to take on your case and appropriate
                        insurance cover being in place. Typically customers pay up to 25% of the amount recovered as a
                        contribution to legal fees. This will be subject to your individual circumstances and the actual
                        fee may be more or less than this.
                     </p>
                     <br>
                     <p><b>Why claim no win no fee with us?</b></p>
                     <p>We know it can sometimes be hard to know who's right for your type of claim, or who will truly
                        listen to what happened to you. You may even feel anxious that contacting somebody about your
                        accident will mean you're pressured or rushed into starting a claim.
                     </p>
                     <p>When you contact us, you do so in your own way, in your own time. Whether that's by calling,
                        requesting a call back or chatting to us online, we'll never rush or pressure you to start a
                        claim. And we'll never share your data without your permission.
                     </p>
                     <p>We're simply here to listen to you and offer free and impartial advice, using our 25 years'
                        experience working with personal injury claims.
                     </p>
                     <p>Starting your claim with us also means:
                     </p>
                     <ol>
                        <li>Free, impartial and jargon-free advice before you decide to claim</li>
                        <li>No stress over picking the right no win no fee accident solicitor - and all of our
                           solicitors are held to high standards of service
                        </li>
                        <li>No upfront or unexpected costs and nothing to pay if your claim is unsuccessful</li>
                        <li>No risk of paying the other side's costs</li>
                     </ol>
                  </div>
               </div>
               <div class="col-12 col-lg-4 order-0 order-lg-1">
                  <div class="imgg"></div>
               </div>
            </div>
         </div>
      </section>
      <!-- Call to Action-->
      <section class="section section-sm context-dark bg-gray-dark section-cta">
         <div class="container">
            <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
               <div class="col-xl-8 text-xl-left">
                  <h4><span class="font-weight-bold">Extrafast</span><span class="font-weight-normal">offers flexible
                        solutions with lots of advantages</span>
                  </h4>
               </div>
               <div class="col-xl-2 text-xl-right"><a class="button button-primary" target="_blank" data-toggle="modal"
                     data-target="#modalLogin">Get in touch</a></div>
            </div>
         </div>
      </section>
        <?php
	  include("includes/footer_one.php");
	  ?>
   </div>
    <?php
	  include("includes/footer_two.php");
	  ?>
</body>
<!-- Google Tag Manager -->

</html>