$(document).ready(function() 
{
	$("#frmSendQuery").validate({
		submitHandler: function(form) {
			 $.ajax({
				  
					url: "contact_mail.php",
					type: 'POST',
					data:  {"txtFirstName":$("#txtFirstName").val(),"txtLastName":$("#txtLastName").val(),"txtEmail":$("#txtEmail").val(),"txtMessage":$("#txtMessage").val(),"ddlClaim":$("#ddlClaim").val(),"ddlHear":$("#ddlHear").val(),"ddlState":$("#ddlState").val(),"txtPhone":$("#txtPhone").val()},
					datatype: 'html',
					success: function(rsp)
					{
		
						$('#contactBtnMsg').html(rsp); 
						$('#contactBtnMsg').addClass('d-block');
						$('#contactBtnMsg').removeClass('d-none');
						
						$('#ddlClaim').val(''); 
						$('#ddlHear').val(''); 
						$('#ddlState').val('State'); 
						$('#txtMessage').val(''); 
						$('#txtPhone').val(''); 
						$('#txtFirstName').val(''); 
						$('#txtLastName').val(''); 
						$('#txtEmail').val(''); 
					
					}
			}); 	
			
			return false;
		},
		rules: {
			txtFirstName: {
				required: true,
				maxlength: 30
			},
			txtLastName: {
				required: true,
				maxlength: 30
			},
			txtPhone: {
				required: true,
				maxlength: 15,
				number: true,
				minlength: 10
			},
			txtMessage: {
				required: true,
				maxlength: 50
			},
			txtEmail: {
				required: true,
				email: true
			},
			ddlClaim: {
				required: true,
			},
			ddlHear: {
				required: true,
			},
			ddlState: {
				required: true,
			}
		},
		messages: {
			chckTC: {
				required: '*',
			},
			chckTCContact: {
				required: '*',
			}
		}
	});
	
	$("#frmSendQueryPopUp").validate({
		submitHandler: function(form) {
			 $.ajax({
				  
					url: "contact_mail_popup.php",
					type: 'POST',
					data:  {"txtPopUpName":$("#txtPopUpName").val(),"txtPopUpEmail":$("#txtPopUpEmail").val(),"txtPopUpMessage":$("#txtPopUpMessage").val(),"txtPopUpNumber":$("#txtPopUpNumber").val()},
					datatype: 'html',
					success: function(rsp)
					{
		
						$('#contactBtnMsgPopUp').html(rsp); 
						$('#contactBtnMsgPopUp').addClass('d-block');
						$('#contactBtnMsgPopUp').removeClass('d-none');
						
						$('#txtPopUpEmail').val(''); 
						$('#txtPopUpMessage').val(''); 
						$('#txtPopUpName').val(''); 
						$('#txtPopUpNumber').val(''); 
					
					}
			}); 	
			
			return false;
		},
		rules: {
			txtPopUpName: {
				required: true,
				maxlength: 30
			},
			txtPopUpMessage: {
				required: true,
				maxlength: 50
			},
			txtPopUpEmail: {
				required: true,
				email: true
			},
			txtPopUpNumber: {
				required: true,
				maxlength: 15,
				number: true,
				minlength: 10
			}
		},
		messages: {
			/* chckTCP: {
				required: '*',
			},
			chckTCPContact: {
				required: '*',
			} */
		}
	});
	
});