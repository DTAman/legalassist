<section class="bg-accent text-center section parallax-container context-dark" id="online-query">
	 <div class="parallax-content p-5">
		<div class="container">
		   <div class="row row-50">
			  <div class="col-12">
				 <h4 class="heading-decorated">READY TO CONTACT AN ADVISOR?</h4>
			  </div>
			  <div class="col-lg-6">
				 <!-- RD Mailform-->
				 <div>
					<p class="alert alert-success contact_btn_msg d-none" id='contactBtnMsg'></p>
				 </div>
				 <form id='frmSendQuery' name='frmSendQuery'>
					<div class="form-wrap_icon wf-50">
					   <input class="form-input" id="txtFirstName" type="text" placeholder="First Name"
						  name="txtFirstName"><span class="form-validation">
					</div>
					<div class="form-wrap_icon  wf-50">
					   <input class="form-input " id="txtLastName" type="text" placeholder="Last Name"
						  name="txtLastName">
					</div>
					<div class="form-wrap_icon  wf-50">
					   <input class="form-input " id="txtEmail" type="email" name="txtEmail" placeholder="E-mail">
					</div>
					<div class="form-wrap_icon  wf-50">
					   <input class="form-input " id="txtPhone" type="text" name="txtPhone"
						  placeholder="Phone Number">
					</div>
					<!-- <div class="form-wrap_icon  wf-50">
						  <select name="ddlState" class="form-input text-white" id="ddlState">
							 <option value=''>Select State</option>
							 <option value='NSW'>NSW</option>
							 <option value='WA'>WA</option>
							 <option value='ACT'>ACT</option>
							 <option value='QLD'>QLD</option>
							 <option value='VIC'>VIC</option>
							 <option value='SA'>SA</option>
						  </select>
						  </div> -->
					<input class="form-input" id="ddlState" type="hidden" name="ddlState" value="State">
					<div class="form-wrap_icon  w-100 float-left">
					   <select name="ddlHear" class="form-input text-white" id="ddlHear">
						  <option value=''>How did you hear about us</option>
						  <option value='Friends'>Friends</option>
						  <option value='Social Network'>Social Network</option>
						  <option value='Website'>Website</option>
						  <option value='Advertisements'>Advertisements</option>
					   </select>
					</div>
					<div class="form-wrap_icon  w-100 float-left">
					   <select name="ddlClaim" class="form-input text-white" id="ddlClaim">
						  <option value=''>Claim For</option>
						  <option value='Motor Vehicle Accidents'>Motor Vehicle Accidents</option>
						  <option value='Work Accidents'>Work Accidents</option>
						  <option value='TPD Claims'>TPD Claims</option>
					   </select>
					</div>
					<div class="form-wrap_icon  w-100 float-left">
					   <textarea class="form-input" id="txtMessage" name="txtMessage"
						  placeholder="Message"></textarea><span class="form-validation"></span>
					</div>
					<div class="form-wrap_icon w-100  float-left" style="text-align: start;">
					   <input type="checkbox" required value='1' name="chckTC" id="chckTC" />
					   &nbsp; I agree to the
					   <a target="_blank" class="link-yellow" href="terms-conditions.php">Terms and Conditions</a> and the <a
						  target="_blank" class="link-yellow" href="privacy-policy.php">Privacy Policy</a>
					   <span class="form-validation"></span>
					</div>
					<div class="form-wrap_icon w-100  float-left" style="text-align: start;">
					   <input type="checkbox" required value='1' name="chckTCContact" id="chckTCContact" />
					   &nbsp; I hereby authorize Injury Assist Helpline and other third party companies to contact me by phone, email and post.
					   <span class="form-validation"></span>
					</div>
					

					<input class="button button-primary float-left contact_btn" type="submit" value='send'
					   id='btnContact' name='btnContact' />
				 </form>
			  </div>
			  <div class="col-lg-6 d-lg-block">
				 <div class="image">
					<img src="images/query.png" alt="" class="img-fluid w-100 h-100 animationn">
				 </div>
			  </div>
		   </div>
		</div>
	 </div>
  </section>
  <section class="pre-footer-corporate bg-image-7 bg-overlay-darkest" style="background-size: cover;">
         <div class="container">
            <div class="row justify-content-sm-center justify-content-lg-start row-30 row-md-60">
                <div class="col-sm-10 col-md-6 col-lg-5 col-xl-6">
                  <img src="images/map-3.jpg">
               </div>
               <!--
			   <div class="col-sm-10 col-md-6 col-lg-10 col-xl-3">

                  <span class="icon">
                     <ion-icon name="call"></ion-icon>
                  </span>
                  <span class="mr-3">0 (7) 304 075 37</span>

               </div>
			   -->
               <div class="col-sm-10 col-md-6 col-lg-3 col-xl-2">
                  <h6>Navigation</h6>
                  <ul class="list-xxs">
                     <li><a href="index.php">Home</a></li>
                     <li><a href="about-us.php">About Us</a></li>
                     <li><a href="motor-vehicle-accident.php">Motor Vehicle Accidents</a></li>
                     <li><a href="work-accidents.php">Work Accidents</a></li>
                     <li><a href="tpd-claim.php">TPD Claims</a></li>
                     <li><a href="no-win.php">No Win No Fee</a></li>
                     <li><a href="contact-us.php">Contact Us</a></li>
                     <li><a href="called-by-us.php">Were you called by us?</a></li>
                  </ul>
               </div>
               <div class="col-sm-10 col-md-6 col-lg-5 col-xl-3">
                  <h6>Testimonials</h6>
                  <ul class="list-xs">
                     <li>
                        <!-- Comment minimal-->
                        <article class="comment-minimal">
                           <p class="comment-minimal__author">Mary</p>
                           <p class="comment-minimal__link">After speaking with the lady, she was really nice and she
                              was very sympathetic.
                           </p>
                        </article>
                     </li>
                     <li>
                        <!-- Comment minimal-->
                        <article class="comment-minimal">
                           <p class="comment-minimal__author">Brian</p>
                           <p class="comment-minimal__link">I couldn't go to work for the first 6-12 weeks because I
                              couldn't drive
                           </p>
                        </article>
                     </li>
                  </ul>
                  <h6 class="list-xs"><a href="dashboard/" class="button button-black" style="letter-spacing: 2px;">Client Login</a></h6>
               </div>
            </div>
         </div>
      </section>
      <footer class="footer-corporate bg-gray-darkest">
         <div class="container">
            <div class="footer-corporate__inner justify-content-center">
               <p class="rights"><span>Injury Assist Helpline</span><span>&nbsp;</span><span
                     class="copyright-year">2019</span>. All Rights Reserved.
                  <a href="privacy-policy.php">Privacy Policy</a>,
                  <a href="terms-conditions.php">Terms &amp; Conditions </a> and
                  <a href="disclaimer.php">Disclaimer</a>
               </p>
            </div>
         </div>
      </footer>