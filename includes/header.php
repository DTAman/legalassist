<header class="page-header">
 <!-- RD Navbar-->
 <div class="rd-navbar-wrap" style="height: 151px;">
	<nav class="rd-navbar rd-navbar-default rd-navbar-original rd-navbar-static" data-layout="rd-navbar-fixed"
	   data-sm-layout="rd-navbar-fixed" data-sm-device-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed"
	   data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed"
	   data-lg-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static"
	   data-xl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static"
	   data-xxl-layout="rd-navbar-static" data-stick-up-clone="false" data-sm-stick-up="true"
	   data-md-stick-up="true" data-lg-stick-up="true" data-md-stick-up-offset="115px"
	   data-lg-stick-up-offset="35px">
	   
	   <div class="rd-navbar-inner rd-navbar-search-wrap">
		  <!-- RD Navbar Panel-->
		  <div class="rd-navbar-panel rd-navbar-search-lg_collapsable">
			 <button class="rd-navbar-toggle toggle-original"
				data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
			 <!-- RD Navbar Brand-->
			 <div><a class="brand-name" href="index.php"><img src="images/logo.png" alt=""></a></div>
		  </div>
		  <!-- RD Navbar Nav-->
		  <div class="rd-navbar-nav-wrap rd-navbar-search_not-collapsable toggle-original-elements">
			 <!-- RD Navbar Nav-->
			 <div class="rd-navbar-search_collapsable">
				<ul class="rd-navbar-nav">
				   <li><a href="index.php">Home</a>
				   </li>
				   <li><a href="about-us.php">About Us</a>
				   </li>
				   <li class="nav-item dropdown">
					  <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button"
						 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						 Our Services
					  </a>
					  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
						 <a class="dropdown-item" href="motor-vehicle-accident.php">Motor Vehicle Accidents</a>
						 <a class="dropdown-item" href="work-accidents.php">Work Accidents</a>
						 <a class="dropdown-item" href="tpd-claim.php">TPD Claims</a>
					  </div>
				   </li>
				   <li><a href="no-win.php">No Win No Fee</a>
				   </li>
				   <li><a href="contact-us.php">Contact Us</a>
				   </li>
				   <li><a href="called-by-us.php">Were you called by us?</a>
					  </li>
					  <li class="nav-item dropdown">
						 <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Company
						 </a>
						 <div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="privacy-policy.php">Privacy Policy</a>
							<a class="dropdown-item" href="terms-conditions.php">Terms &amp; Conditions</a>
							<a class="dropdown-item" href="disclaimer.php">Disclaimer</a>
						 </div>
					  </li>
				</ul>
			 </div>
		  </div>
	   </div>
	</nav>
 </div>
</header>