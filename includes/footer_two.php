<!-- Modal login window-->
   <div class="modal fade" id="modalLogin" role="dialog">
      <div class="modal-dialog modal-dialog_custom">
         <!-- Modal content-->
         <div class="modal-dialog__inner">
            <button class="close" type="button" data-dismiss="modal"></button>
            <div class="modal-dialog__content">
               <h5 class="heading-decorated">Get in Touch</h5>
               <div>
                  <p class="alert alert-success contact_btn_msg d-none" id='contactBtnMsgPopUp'></p>
               </div>
               <!-- RD Mailform-->
               <form id='frmSendQueryPopUp' name='frmSendQueryPopUp'>
                  <div class="form-wrap form-wrap_icon linear-icon-man">
                     <input class="form-input" id="txtPopUpName" type="text" name="txtPopUpName"><span
                        class="form-validation"></span>
                     <label class="form-label rd-input-label" for="txtPopUpName">Your name</label>
                  </div>
                  <div class="form-wrap form-wrap_icon linear-icon-envelope">
                     <input class="form-input" id="txtPopUpEmail" type="email" name="txtPopUpEmail"><span
                        class="form-validation"></span>
                     <label class="form-label rd-input-label" for="txtPopUpEmail">Your e-mail</label>
                  </div>
                  <div class="form-wrap form-wrap_icon linear-icon-man">
                     <input class="form-input" id="txtPopUpNumber" type="text" name="txtPopUpNumber"><span
                        class="form-validation"></span>
                     <label class="form-label rd-input-label" for="txtPopUpNumber">Your mobile</label>
                  </div>
                  <div class="form-wrap form-wrap_icon linear-icon-feather">
                     <textarea class="form-input" id="txtPopUpMessage" name="txtPopUpMessage"></textarea><span
                        class="form-validation"></span>
                     <label class="form-label rd-input-label"  for="txtPopUpMessage">Your Message</label>
                  </div>

                  <div class="form-wrap form-wrap_icon ">
                     <input type="checkbox" required value='1' name="chckTCP" id="chckTCP" />
                     I agree to the <a target="_blank" href="terms-conditions.php">Terms and Conditions</a> and the <a
                        target="_blank" href="privacy-policy.php">Privacy Policy</a>
                  </div>
				 <div class="form-wrap form-wrap_icon ">
                     <input type="checkbox" required value='1' name="chckTCPContact" id="chckTCPContact" />
                     I hereby authorize Injury Assist Helpline and other third party companies to contact me by phone, email and post.
                  </div>
               

                  <input class="button contact_btn_popup" type="submit" value='send' id='btnContactPopUp'
                     name='btnContactPopUp' />
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- Global Mailform Output-->
   <!-- Javascript-->
   
   
   
  
   <!--<script src="js/compressed.js"></script>-->
   <script src="https://unpkg.com/ionicons@4.5.5/dist/ionicons.js"></script>
   <script src="js/core.min.js"></script>
   <script src="js/script.js"></script>
   <script src="js/swiper.min.js"></script>
   <script>
      var swiper = new Swiper('.swiper-container', {
         autoplay: {
            delay: 5000,
            disableOnInteraction: false,
         },
      });
   </script>
   <script src="jquery.validate.js"></script> 
   <script src="js/main.js"></script>
   <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDRQWjiZLVmW6KgbHMST61vz_vw6T1yaI&amp;callback=initGoogleMap"></script>
   <script src="mail.js"></script>
   