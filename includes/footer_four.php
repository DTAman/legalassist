  <section class="pre-footer-corporate bg-image-7 bg-overlay-darkest" style="background-size: cover;">
         <div class="container">
            <div class="row justify-content-sm-center justify-content-lg-start row-30 row-md-60">
               <div class="col-sm-10 col-md-6 col-lg-5 col-xl-6">
                  <img src="images/map-3.jpg">
               </div>
               <!--
			   <div class="col-sm-10 col-md-6 col-lg-10 col-xl-3">

                  <span class="icon">
                     <ion-icon name="call"></ion-icon>
                  </span>
                  <span class="mr-3">0 (7) 304 075 37</span>

               </div>
			   -->
               <div class="col-sm-10 col-md-6 col-lg-3 col-xl-2">
                  <h6>Navigation</h6>
                  <ul class="list-xxs">
                     <li><a href="index.php">Home</a></li>
                     <li><a href="about-us.php">About Us</a></li>
                     <li><a href="motor-vehicle-accident.php">Motor Vehicle Accidents</a></li>
                     <li><a href="work-accidents.php">Work Accidents</a></li>
                     <li><a href="tpd-claim.php">TPD Claims</a></li>
                     <li><a href="no-win.php">No Win No Fee</a></li>
                     <li><a href="contact-us.php">Contact Us</a></li>
                     <li><a href="called-by-us.php">Were you called by us?</a></li>
                  </ul>
               </div>
               <div class="col-sm-10 col-md-6 col-lg-5 col-xl-3">
                  <h6>Testimonials</h6>
                  <ul class="list-xs">
                     <li>
                        <!-- Comment minimal-->
                        <article class="comment-minimal">
                           <p class="comment-minimal__author">Mary</p>
                           <p class="comment-minimal__link">After speaking with the lady, she was really nice and she
                              was very sympathetic.
                           </p>
                        </article>
                     </li>
                     <li>
                        <!-- Comment minimal-->
                        <article class="comment-minimal">
                           <p class="comment-minimal__author">Brian</p>
                           <p class="comment-minimal__link">I couldn't go to work for the first 6-12 weeks because I
                              couldn't drive
                           </p>
                        </article>
                     </li>
                  </ul>
                  <h6 class="list-xs"><a href="dashboard/" class="button button-black" style="letter-spacing: 2px;">Client Login</a></h6>
               </div>
            </div>
         </div>
      </section>
      <footer class="footer-corporate bg-gray-darkest">
         <div class="container">
            <div class="footer-corporate__inner justify-content-center">
               <p class="rights"><span>Injury Assist Helpline</span><span>&nbsp;</span><span
                     class="copyright-year">2019</span>. All Rights Reserved.
                  <a href="privacy-policy.php">Privacy Policy</a>,
                  <a href="terms-conditions.php">Terms &amp; Conditions </a> and
                  <a href="disclaimer.php">Disclaimer</a>
               </p>
            </div>
         </div>
      </footer>