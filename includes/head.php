<!DOCTYPE html>
<html class="wide wow-animation desktop landscape rd-navbar-static-linked" lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <title>Injury Assist Helpline</title>
   <meta name="viewport"
      content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <link rel="icon" href="images/fav_icon.png" type="image/x-icon">
   <!-- Stylesheets-->
   
   <link rel="stylesheet" href="css/bootstrap.css">
   <link rel="stylesheet" href="css/style.css">
   <link rel="stylesheet" href="css/map/font-awesome.css">
   <link rel="stylesheet" href="css/map/main.css">
   <link rel="stylesheet" href="css/fonts.css">
   <style type="text/css">
    /*   ul {
         list-style: circle;
      }

      ul li {
         display: list-item;
      } */
   </style>
   <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-215041881-1">
	</script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-215041881-1');
	</script>
</head>