<?php
include("includes/head.php");
?>

<body class="" contenteditable="false">
   <!-- Page-->
   <div class="page">
      <!-- Panel Thumbnail-->
      <!-- Template panel-->
      <div class="layout-panel-wrap">
         <div class="layout-panel">
         </div>
      </div>
      <?php
		include("includes/header.php");
	  ?>

        <section class="detail pt-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h4 class="heading-decorated">Terms &amp; Conditions</h4>
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-12 col-lg-8 order-1 order-lg-0 mt-4"> -->
                    <div class="col-12 col-lg-12 order-1 order-lg-0 mt-4">
                        <div class="content">
                            <p>In these website terms & conditions, we, us or our means Injury Assist Helpline (“IA”).
                                This website is operated by IA. It is available at www.injuryassisthelpline.com and may
                                be available through other addresses or channels.</p>

                            <h5>Consent</h5>
                            <p>By accessing and/or using our website, you agree to these terms of use and our Privacy
                                Policy (available on our website) (“Terms”). Please read these Terms carefully and
                                immediately cease using our website if you do not agree to them.</p>

                            <h5>Variations</h5>

                            <p>We may, at any time and at our discretion, vary these Terms by publishing the varied
                                terms on our website. We recommend you check our website regularly to ensure you are
                                aware of our current terms. Materials and information on this website (“Content”) are
                                subject to change without notice. We do not undertake to keep our website up-to-date and
                                we are not liable if any Content is inaccurate or out-of-date.</p>

                            <h5>Licence to use our Website</h5>

                            <p>We grant you a non-exclusive, royalty-free, revocable, worldwide, non-transferable
                                licence to use our website in accordance with these Terms. All other uses are prohibited
                                without our prior written consent.</p>

                            <h5>Prohibited conduct</h5>

                            <p>You must not do or attempt to do anything that is prohibited by any laws applicable to
                                our website, which we would consider inappropriate, or which might bring us or our
                                website into disrepute, including (without limitation):</p>

                            <ol>
                                <li>anything that would constitute a breach of an individual’s privacy (including
                                    uploading private or personal information without an individual’s consent) or any
                                    other legal rights;</li>
                                <li> using our website to defame, harass, threaten, menace or offend any person;</li>
                                <li>interfering with any user using our website;</li>
                                <li>tampering with or modifying our website, knowingly transmitting viruses or other
                                    disabling features, or damaging or interfering with our website, including (without
                                    limitation) using trojan horses, viruses or piracy or programming routines that may
                                    damage or interfere with our website;</li>
                                <li>using our website to send unsolicited email messages; or</li>
                                <li>facilitating or assisting a third party to do any of the above acts.</li>
                            </ol>
                            <br />
                            <h5>Exclusion of competitors</h5>
                            <p>You are prohibited from using our website, including the Content, in any way that
                                competes with our business.</p>

                            <h5>Information</h5>
                            <p>The Content is not comprehensive and is for general information purposes only. It does
                                not take into account your specific needs, objectives or circumstances, and it is not
                                advice. While we use reasonable attempts to ensure the accuracy and completeness of the
                                Content, we make no representation or warranty in relation to it, to the maximum extent
                                permitted by law.</p>

                            <h5>Intellectual Property rights</h5>
                            <p>Unless otherwise indicated, we own or licence all rights, title and interest (including
                                intellectual property rights) in our website and all of the Content. Your use of our
                                website and your use of and access to any Content does not grant or transfer to you any
                                rights, title or interest in relation to our website or the Content. You must not:</p>

                            <ol>
                                <li>copy or use, in whole or in part, any Content;</li>
                                <li>reproduce, retransmit, distribute, disseminate, sell, publish, broadcast or
                                    circulate any Content to any third party; or</li>
                                <li>breach any intellectual property rights connected with our website or the Content,
                                    including (without limitation) altering or modifying any of the Content, causing any
                                    of the Content to be framed or embedded in another website or platform, or creating
                                    derivative works from the Content.</li>
                            </ol>
                            <br />
                            <h5>User Content</h5>
                            <p> You may be permitted to post, upload, publish, submit or transmit relevant information
                                and content (“User Content”) on our website. By making available any User Content on or
                                through our website, you grant to us a worldwide, irrevocable, perpetual, non-exclusive,
                                transferable, royalty-free licence to use the User Content, with the right to use, view,
                                copy, adapt, modify, distribute, license, sell, transfer, communicate, publicly display,
                                publicly perform, transmit, stream, broadcast, access, or otherwise exploit such User
                                Content on, through or by means of our website.
                            </p>
                            <p>You agree that you are solely responsible for all User Content that you make available on
                                or through our website. You represent and warrant that:</p>

                            <ol>
                                <li>you are either the sole and exclusive owner of all User Content or you have all
                                    rights, licences, consents and releases that are necessary to grant to us the rights
                                    in such User Content (as contemplated by these Terms); and</li>
                                <li>neither the User Content nor the posting, uploading, publication, submission or
                                    transmission of the User Content or our use of the User Content on, through or by
                                    means of our website will infringe, misappropriate or violate a third party’s
                                    intellectual property rights, or rights of publicity or privacy, or result in the
                                    violation of any applicable law or regulation.</li>
                            </ol>

                            <p>We do not endorse or approve, and are not responsible for, any User Content. We may, at
                                any time (at our sole discretion), remove any User Content.</p>

                            <h5>Third party websites</h5>
                            <p>Our website may contain links to websites operated by third parties. Unless expressly
                                stated otherwise, we do not control, endorse or approve, and are not responsible for,
                                the content on those websites. You should make your own investigations with respect to
                                the suitability of those websites.</p>

                            <h5>Discontinuance</h5>
                            <p>We may, at any time and without notice to you, discontinue our website, in whole or in
                                part. We may also exclude any person from using our website, at any time and at our sole
                                discretion. We are not responsible for any Liability you may suffer arising from or in
                                connection with any such discontinuance or exclusion.</p>

                            <h5>Warranties and disclaimers</h5>
                            <p>To the maximum extent permitted by law, we make no representations or warranties about
                                our website or the Content, including (without limitation) that:</p>
                            <ol>
                                <li>they are complete, accurate, reliable, up-to-date and suitable for any particular
                                    purpose;</li>
                                <li>access will be uninterrupted, error-free or free from viruses; or</li>
                                <li>our website will be secure.</li>
                            </ol>

                            <p>You read, use and act on our website and the Content at your own risk.</p>

                            <h5>Limitation of liability</h5>
                            <p>To the maximum extent permitted by law, we are not responsible for any loss, damage or
                                expense, howsoever arising, whether direct or indirect and/or whether present,
                                unascertained, future or contingent (“Liability”) suffered by you or any third party,
                                arising from or in connection with your use of our website and/or the Content and/or any
                                inaccessibility of, interruption to or outage of our website and/or any loss or
                                corruption of data and/or the fact that the Content is incorrect, incomplete or
                                out-of-date.</p>

                            <h5>Indemnity</h5>
                            <p>To the maximum extent permitted by law, you must indemnify us, and hold us harmless,
                                against any Liability suffered or incurred by us arising from or in connection with your
                                use of our website or any breach of these Terms or any applicable laws by you. This
                                indemnity is a continuing obligation, independent from the other obligations under these
                                Terms, and continues after these Terms end. It is not necessary for us to suffer or
                                incur any Liability before enforcing a right of indemnity under these Terms.</p>

                            <h5>Termination</h5>
                            <p>These Terms are effective until terminated by us, which we may do at any time and without
                                notice to you. In the event of termination, all restrictions imposed on you by these
                                Terms and limitations of liability set out in these Terms will survive.</p>

                            <h5>Disputes</h5>
                            <p>In the event of any dispute arising from, or in connection with, these Terms (Dispute),
                                the party claiming there is a Dispute must give written notice to the other party
                                setting out the details of the Dispute and proposing a resolution. Within 90 days after
                                receiving the notice, the parties must, by their senior executives or senior managers
                                (who have the authority to reach a resolution on behalf of the party), meet at least
                                once to attempt to resolve the Dispute or agree on the method of resolving the Dispute
                                by other means, in good faith. All aspects of every such conference, except the fact of
                                the occurrence of the conference, will be privileged. If the parties do not resolve the
                                Dispute, or (if the Dispute is not resolved) agree on an alternate method to resolve the
                                Dispute, within 120 days after receipt of the notice, the Dispute may be referred by
                                either party (by notice in writing to the other party) to litigation.</p>

                            <h5>Severance</h5>
                            <p>If a provision of these Terms is held to be void, invalid, illegal or unenforceable, that
                                provision must be read down as narrowly as necessary to allow it to be valid or
                                enforceable. If it is not possible to read down a provision (in whole or in part), that
                                provision (or that part of that provision) is severed from these Terms without affecting
                                the validity or enforceability of the remainder of that provision or the other
                                provisions in these Terms.</p>

                            <h5>Jurisdiction</h5>
                            <p>Your use of our website and these Terms are governed by the laws of New South Wales. You
                                irrevocably and unconditionally submit to the exclusive jurisdiction of the courts
                                operating in New South Wales and any courts entitled to hear appeals from those courts
                                and waive any right to object to proceedings being brought in those courts.</p>

                            <p>Our website may be accessed throughout Australia and overseas. We make no representation
                                that our website complies with the laws (including intellectual property laws) of any
                                country outside Australia. If you access our website from outside Australia, you do so
                                at your own risk and are responsible for complying with the laws of the jurisdiction
                                where you access our website from.</p>

                            <h5>For any questions and notices, please contact us at:</h5>
                            Injury Assist Helpline <br />
                            Email: info@injuryassisthelpline.com <br />
                            Last updated: 03 Dec 2021 <br />
                        </div>
                    </div>
                    <!--
					<div class="col-12 col-lg-4 order-0 order-lg-1">
                        <div class="imgg4"></div>
                    </div>
					-->
                </div>
            </div>
        </section>
         <?php
	  include("includes/footer_one.php");
	  ?>
   </div>
    <?php
	  include("includes/footer_two.php");
	  ?>
</body>
<!-- Google Tag Manager -->

</html>